this is line is just for testing live activities  
11
# GeetaDressMain

# Git global setup
* git config --global user.name "kondle mohan"
* git config --global user.email "kondlemohan1@gmail.com"

# Pull a new repository
* git remote add origin git@server.vedamlab.com:kondlemohan/geetadressmain.git
* git pull origin master

# Create a new repository
* git clone git@server.vedamlab.com:kondlemohan/geetadressmain.git
* cd testproject
* touch README.md
* git add README.md
* git commit -m "add README"
* git push -u origin master

# Push an existing folder
* cd existing_folder
* git init
* git remote add origin git@server.vedamlab.com:kondlemohan/geetadressmain.git
* git add .
* git commit -m "Initial commit"
* git push -u origin master

# Push an existing Git repository
* cd existing_repo
* git remote rename origin old-origin
* git remote add origin git@server.vedamlab.com:kondlemohan/geetadressmain.git
* git push -u origin --all
* git push -u origin --tags






