-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 20, 2020 at 04:38 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inv`
--

DELIMITER $$
--
-- Functions
--
CREATE DEFINER=`root`@`localhost` FUNCTION `fn_secqtyOrder` (`order_id` CHAR(20)) RETURNS CHAR(1) CHARSET latin1 BEGIN
DECLARE secqty char(150);
set secqty = (select sum(sma_quotes_products.squantity) AS sec_qty FROM sma_quote_items 
LEFT JOIN sma_quotes_products  ON sma_quotes_products.id = sma_quote_items.product_id 
LEFT JOIN sma_product_variants  ON sma_product_variants.id = sma_quote_items.option_id  
LEFT JOIN sma_tax_rates ON sma_tax_rates.id = sma_quote_items.tax_rate_id 
LEFT JOIN sma_size ON sma_size.id = sma_quotes_products.size 
LEFT JOIN sma_per ON sma_per.id = sma_quotes_products.sunit WHERE quote_id =order_id
GROUP BY sma_quote_items.quote_id 
);
RETURN secqty;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `sma_adjustments`
--

CREATE TABLE `sma_adjustments` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_advance_receipt`
--

CREATE TABLE `sma_advance_receipt` (
  `id` int(11) NOT NULL,
  `trn` varchar(20) DEFAULT NULL,
  `receipt_date` date DEFAULT NULL,
  `customer` varchar(200) DEFAULT NULL,
  `delivery_at` varchar(200) DEFAULT NULL,
  `advance_amt` double(10,2) DEFAULT NULL,
  `merchandiser` varchar(200) DEFAULT NULL,
  `who_took` varchar(300) DEFAULT NULL,
  `design` varchar(100) DEFAULT NULL,
  `color` varchar(100) DEFAULT NULL,
  `size` varchar(20) DEFAULT NULL,
  `delivery_by` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_advance_receipt_items`
--

CREATE TABLE `sma_advance_receipt_items` (
  `id` int(11) NOT NULL,
  `advance_receipt_id` int(11) DEFAULT NULL,
  `design` varchar(100) DEFAULT NULL,
  `color` varchar(100) DEFAULT NULL,
  `size` int(5) DEFAULT NULL,
  `advance_amt` decimal(10,2) NOT NULL,
  `extra_amt` decimal(10,2) DEFAULT NULL,
  `delivery_by` date DEFAULT NULL,
  `po_number` varchar(100) DEFAULT NULL,
  `inv_no` varchar(100) DEFAULT NULL,
  `status` enum('Altr','Ready','New') DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_attributes`
--

CREATE TABLE `sma_attributes` (
  `id` int(11) NOT NULL,
  `department` tinyint(1) DEFAULT NULL,
  `product_items` tinyint(1) DEFAULT NULL,
  `section` tinyint(1) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `brands` tinyint(1) DEFAULT NULL,
  `design` tinyint(1) DEFAULT NULL,
  `style` tinyint(1) DEFAULT NULL,
  `pattern` tinyint(1) DEFAULT NULL,
  `fitting` tinyint(1) DEFAULT NULL,
  `fabric` tinyint(1) DEFAULT NULL,
  `color` tinyint(1) DEFAULT NULL,
  `size` tinyint(1) DEFAULT NULL,
  `per` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_bank`
--

CREATE TABLE `sma_bank` (
  `id` int(20) NOT NULL,
  `bank_code` varchar(200) DEFAULT NULL,
  `bank_name` text,
  `ifsc_code` varchar(255) DEFAULT NULL,
  `address_1` text,
  `address_2` text,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `pin_code` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `fax` varchar(100) DEFAULT NULL,
  `email` text,
  `web_site` text,
  `status` enum('Active','Deactive') DEFAULT NULL,
  `create_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_brands`
--

CREATE TABLE `sma_brands` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `userby` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Deactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_calendar`
--

CREATE TABLE `sma_calendar` (
  `date` date NOT NULL,
  `data` varchar(255) NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_captcha`
--

CREATE TABLE `sma_captcha` (
  `captcha_id` bigint(13) UNSIGNED NOT NULL,
  `captcha_time` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(16) CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `word` varchar(20) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_cashmemo`
--

CREATE TABLE `sma_cashmemo` (
  `id` int(11) NOT NULL,
  `cash_memo_no` varchar(20) DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `items_detail` text,
  `grand_total` varchar(20) DEFAULT NULL,
  `crate_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_categories`
--

CREATE TABLE `sma_categories` (
  `id` int(11) NOT NULL,
  `code` varchar(55) NOT NULL,
  `name` varchar(55) NOT NULL,
  `image` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_color`
--

CREATE TABLE `sma_color` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `userby` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Deactive') DEFAULT NULL,
  `code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_combo_items`
--

CREATE TABLE `sma_combo_items` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `item_code` varchar(20) NOT NULL,
  `quantity` decimal(12,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_companies`
--

CREATE TABLE `sma_companies` (
  `id` int(11) NOT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `group_name` varchar(20) NOT NULL,
  `customer_group_id` int(11) DEFAULT NULL,
  `customer_group_name` varchar(100) DEFAULT NULL,
  `name` varchar(55) NOT NULL,
  `company` varchar(255) NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `vat_no` varchar(100) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `address2` text NOT NULL,
  `city` varchar(55) NOT NULL,
  `state` varchar(55) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `cf1` varchar(100) DEFAULT NULL,
  `cf2` varchar(100) DEFAULT NULL,
  `cf3` varchar(100) DEFAULT NULL,
  `cf4` varchar(100) DEFAULT NULL,
  `cf5` varchar(100) DEFAULT NULL,
  `cf6` varchar(100) DEFAULT NULL,
  `invoice_footer` text,
  `payment_term` int(11) DEFAULT '0',
  `logo` varchar(255) DEFAULT 'logo.png',
  `award_points` int(11) DEFAULT '0',
  `discount` varchar(20) DEFAULT NULL,
  `tax_rate` varchar(255) DEFAULT NULL,
  `tax_method` varchar(255) DEFAULT NULL,
  `barcode_prefix` varchar(255) DEFAULT NULL,
  `conactpname` varchar(255) NOT NULL,
  `panno` varchar(100) NOT NULL,
  `gstno` varchar(100) NOT NULL,
  `tinno` varchar(100) DEFAULT NULL,
  `openingbalance` varchar(100) NOT NULL,
  `website` varchar(100) NOT NULL,
  `telphoneno` varchar(100) NOT NULL,
  `preferredsupplier` varchar(100) NOT NULL,
  `noofdays` varchar(100) NOT NULL,
  `attachments` text NOT NULL,
  `status` enum('Active','Deactive') NOT NULL DEFAULT 'Active',
  `bank_name` text NOT NULL,
  `acc_no` text NOT NULL,
  `neft` text NOT NULL,
  `bank_branch` text NOT NULL,
  `faxno` varchar(250) NOT NULL,
  `refered_by` varchar(255) NOT NULL,
  `igst` enum('1','0') NOT NULL COMMENT '1 yes, 0 no',
  `opening_balance` varchar(255) NOT NULL,
  `store_id` int(50) NOT NULL COMMENT 'This is only for supplier',
  `adhar_no` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_companies`
--

INSERT INTO `sma_companies` (`id`, `group_id`, `group_name`, `customer_group_id`, `customer_group_name`, `name`, `company`, `code`, `vat_no`, `address`, `address2`, `city`, `state`, `postal_code`, `country`, `phone`, `email`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `invoice_footer`, `payment_term`, `logo`, `award_points`, `discount`, `tax_rate`, `tax_method`, `barcode_prefix`, `conactpname`, `panno`, `gstno`, `tinno`, `openingbalance`, `website`, `telphoneno`, `preferredsupplier`, `noofdays`, `attachments`, `status`, `bank_name`, `acc_no`, `neft`, `bank_branch`, `faxno`, `refered_by`, `igst`, `opening_balance`, `store_id`, `adhar_no`) VALUES
(0, NULL, 'biller', NULL, NULL, 'hi', 'Rocket Sales Corp', NULL, 'ABCD1234A1ZS', 'Shop no 99 SMC Complex, navi peth', '', 'Solapur', 'Maharashtra', '413005', 'India', '9911223344', 'info@rocketsales.com', NULL, NULL, NULL, NULL, NULL, NULL, '', 0, 'rocket_1.png', 0, NULL, NULL, '0', 'RSC', '', '', '', NULL, '', '', '', '', '', '', 'Active', '', '', '', '', '', '', '1', '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `sma_contra_vouchers`
--

CREATE TABLE `sma_contra_vouchers` (
  `id` int(50) NOT NULL,
  `payment_no` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `time` varchar(200) DEFAULT NULL,
  `transfer_type` varchar(255) DEFAULT NULL COMMENT 'payment for/ Received for / transfer type',
  `from_acc` varchar(255) DEFAULT NULL COMMENT 'party acc/from account',
  `against_ref_no` varchar(255) DEFAULT NULL,
  `bank_acc_no` varchar(255) DEFAULT NULL,
  `datedd` datetime DEFAULT NULL,
  `amount` varchar(200) DEFAULT NULL,
  `to_acc` varchar(255) DEFAULT NULL COMMENT 'payment_mode / to account',
  `receiver_bank_acc_no` varchar(255) DEFAULT NULL COMMENT 'from acc/ to acc/ bank_acc_no',
  `mode_of_transfer` varchar(255) DEFAULT NULL COMMENT 'pay_type_no / mode of transfer',
  `ref_no` varchar(255) DEFAULT NULL COMMENT 'bank_acc_no/ account_no / ref_no',
  `dated` datetime DEFAULT NULL,
  `balance` varchar(255) DEFAULT NULL,
  `narration` text,
  `status` enum('Active','Deactive') DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `ten` int(20) NOT NULL,
  `hundread` int(20) NOT NULL,
  `fivehundread` int(20) NOT NULL,
  `twothousand` int(20) NOT NULL,
  `coins` int(20) NOT NULL,
  `twenty` int(20) NOT NULL,
  `fifty` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_costing`
--

CREATE TABLE `sma_costing` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `sale_item_id` int(11) NOT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `purchase_item_id` int(11) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `purchase_net_unit_cost` decimal(25,4) DEFAULT NULL,
  `purchase_unit_cost` decimal(25,4) DEFAULT NULL,
  `sale_net_unit_price` decimal(25,4) NOT NULL,
  `sale_unit_price` decimal(25,4) NOT NULL,
  `quantity_balance` decimal(15,4) DEFAULT NULL,
  `inventory` tinyint(1) DEFAULT '0',
  `overselling` tinyint(1) DEFAULT '0',
  `option_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_coupon`
--

CREATE TABLE `sma_coupon` (
  `id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT NULL,
  `offer` int(11) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `description` text,
  `coupon_usages` enum('Use Unlimited Number of Times','Use Fixed Number of Times') DEFAULT NULL,
  `use_fixed_times` decimal(10,0) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `generat_coupon` enum('Specify Number of Coupons','Customer Specify Coupons') DEFAULT NULL,
  `number_of_coupon` decimal(10,0) DEFAULT NULL,
  `status` enum('Active','Deactive') DEFAULT 'Active',
  `coupon_no` varchar(250) DEFAULT NULL,
  `store` int(11) DEFAULT NULL,
  `department` int(11) DEFAULT NULL,
  `section` int(11) DEFAULT NULL,
  `product_item` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `brand` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_currencies`
--

CREATE TABLE `sma_currencies` (
  `id` int(11) NOT NULL,
  `code` varchar(5) NOT NULL,
  `name` varchar(55) NOT NULL,
  `rate` decimal(12,4) NOT NULL,
  `auto_update` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_currencies`
--

INSERT INTO `sma_currencies` (`id`, `code`, `name`, `rate`, `auto_update`) VALUES
(1, 'USD', 'US Dollar', '0.0160', 0),
(2, 'EUR', 'EURO', '0.0130', 1),
(3, 'INR', 'Indian Rupees', '1.0000', 0),
(4, 'Briti', 'British Pound', '0.0120', 1),
(5, 'AUD', 'Australian Dollar', '0.0200', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sma_customerdetails`
--

CREATE TABLE `sma_customerdetails` (
  `id` int(11) NOT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `dateofbirth` datetime DEFAULT NULL,
  `anniversarydate` datetime DEFAULT NULL,
  `isloyatlity` enum('True','False') DEFAULT NULL,
  `loyalityno` varchar(50) DEFAULT NULL,
  `enrolldate` datetime DEFAULT NULL,
  `referby` varchar(50) DEFAULT NULL,
  `ispoint` enum('True','False') DEFAULT NULL,
  `billaddressline1` varchar(255) DEFAULT NULL,
  `billaddressline2` varchar(255) DEFAULT NULL,
  `billcity` varchar(250) DEFAULT NULL,
  `billstate` varchar(250) DEFAULT NULL,
  `billcountry` varchar(250) DEFAULT NULL,
  `billzipcode` varchar(20) DEFAULT NULL,
  `billtelephoneno` varchar(20) DEFAULT NULL,
  `billmobileno` varchar(20) DEFAULT NULL,
  `billemail` varchar(255) DEFAULT NULL,
  `enrolllimit` varchar(250) DEFAULT NULL,
  `creaditlimit` varchar(255) DEFAULT NULL,
  `fabalance` varchar(200) DEFAULT NULL,
  `sincedate` datetime DEFAULT NULL,
  `loyalitypoint` varchar(200) DEFAULT NULL,
  `createdby` int(11) NOT NULL,
  `loyalty_percent` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_customer_groups`
--

CREATE TABLE `sma_customer_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `percent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_damage_sold_return`
--

CREATE TABLE `sma_damage_sold_return` (
  `id` int(11) NOT NULL,
  `reference_no` varchar(55) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delivery_date` datetime DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `supplier` varchar(55) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` varchar(1000) NOT NULL,
  `total` decimal(25,4) DEFAULT NULL,
  `product_discount` decimal(25,4) DEFAULT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `order_discount` decimal(25,4) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT NULL,
  `product_tax` decimal(25,4) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `paid` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `status` varchar(55) DEFAULT '',
  `payment_status` varchar(20) DEFAULT 'pending',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `attachment` varchar(55) DEFAULT NULL,
  `lr_no` varchar(255) DEFAULT NULL,
  `ag_order_no` varchar(255) DEFAULT NULL,
  `purchase_no` varchar(255) DEFAULT NULL,
  `store_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_damage_sold_return_items`
--

CREATE TABLE `sma_damage_sold_return_items` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `transfer_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_cost` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(20) DEFAULT NULL,
  `discount` varchar(20) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `quantity_balance` decimal(15,4) DEFAULT '0.0000',
  `date` date NOT NULL,
  `status` varchar(50) NOT NULL,
  `unit_cost` decimal(25,4) DEFAULT NULL,
  `real_unit_cost` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_date_format`
--

CREATE TABLE `sma_date_format` (
  `id` int(11) NOT NULL,
  `js` varchar(20) NOT NULL,
  `php` varchar(20) NOT NULL,
  `sql` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_date_format`
--

INSERT INTO `sma_date_format` (`id`, `js`, `php`, `sql`) VALUES
(1, 'mm-dd-yyyy', 'm-d-Y', '%m-%d-%Y'),
(2, 'mm/dd/yyyy', 'm/d/Y', '%m/%d/%Y'),
(3, 'mm.dd.yyyy', 'm.d.Y', '%m.%d.%Y'),
(4, 'dd-mm-yyyy', 'd-m-Y', '%d-%m-%Y'),
(5, 'dd/mm/yyyy', 'd/m/Y', '%d/%m/%Y'),
(6, 'dd.mm.yyyy', 'd.m.Y', '%d.%m.%Y');

-- --------------------------------------------------------

--
-- Table structure for table `sma_dc`
--

CREATE TABLE `sma_dc` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT 'no_image.png',
  `design_code` varchar(200) DEFAULT NULL,
  `cost` varchar(20) DEFAULT NULL,
  `price` varchar(20) DEFAULT NULL,
  `markup` varchar(5) NOT NULL,
  `category` int(5) NOT NULL,
  `create_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_deliveries`
--

CREATE TABLE `sma_deliveries` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sale_id` int(11) NOT NULL,
  `do_reference_no` varchar(50) NOT NULL,
  `sale_reference_no` varchar(50) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_department`
--

CREATE TABLE `sma_department` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `store_id` int(11) NOT NULL,
  `userby` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Deactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_design`
--

CREATE TABLE `sma_design` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `brands_id` int(11) NOT NULL,
  `userby` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Deactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_design_code`
--

CREATE TABLE `sma_design_code` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT 'no_image.png',
  `design_code` varchar(200) DEFAULT NULL,
  `cost` varchar(20) DEFAULT NULL,
  `price` varchar(20) DEFAULT NULL,
  `markup` varchar(5) NOT NULL,
  `create_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_employee`
--

CREATE TABLE `sma_employee` (
  `id` int(11) NOT NULL,
  `joining_date` date DEFAULT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `mname` varchar(255) DEFAULT NULL,
  `lname` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `marital_status` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `smreq` enum('True','False') DEFAULT NULL,
  `smsapprove` enum('True','False') DEFAULT NULL,
  `notemp` enum('Ture','False') DEFAULT NULL,
  `invattach` enum('True','False') DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `address2` varchar(250) DEFAULT NULL,
  `city` varchar(250) DEFAULT NULL,
  `state` varchar(250) DEFAULT NULL,
  `country` varchar(250) DEFAULT NULL,
  `zipcode` varchar(250) DEFAULT NULL,
  `tele_ph` varchar(250) DEFAULT NULL,
  `faxno` varchar(250) DEFAULT NULL,
  `mobile` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `website` varchar(250) DEFAULT NULL,
  `scp` varchar(250) DEFAULT NULL,
  `cqp` varchar(250) DEFAULT NULL,
  `hlsda` enum('True','False') DEFAULT NULL,
  `msd` varchar(250) DEFAULT NULL,
  `store` int(11) DEFAULT NULL,
  `department` int(11) DEFAULT NULL,
  `section` int(11) DEFAULT NULL,
  `desigation` varchar(250) DEFAULT NULL,
  `shift` varchar(250) DEFAULT NULL,
  `bank_name` varchar(250) DEFAULT NULL,
  `acc_no` varchar(250) DEFAULT NULL,
  `neft` varchar(50) DEFAULT NULL,
  `bank_branch` varchar(250) DEFAULT NULL,
  `qualification` text,
  `special_training` text,
  `identity_proof` text,
  `references` text,
  `photo` text,
  `create_date` datetime DEFAULT NULL,
  `modify_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Deactive') DEFAULT NULL,
  `refered_by` varchar(250) DEFAULT NULL,
  `opening_balance` varchar(200) DEFAULT NULL,
  `incentive` decimal(10,2) DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_employee_sale_insentive`
--

CREATE TABLE `sma_employee_sale_insentive` (
  `id` int(11) NOT NULL,
  `emp_id` int(11) DEFAULT NULL,
  `bill_no` int(11) DEFAULT NULL,
  `insentive_amount` decimal(10,2) DEFAULT NULL COMMENT 'amount add with employee incentive with sale',
  `datetime` timestamp NULL DEFAULT NULL,
  `pay_incentive` decimal(10,2) DEFAULT NULL COMMENT 'Amount Pay with employee',
  `note` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_expenses`
--

CREATE TABLE `sma_expenses` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference` varchar(50) NOT NULL,
  `amount` decimal(25,4) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `created_by` varchar(55) NOT NULL,
  `attachment` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_fabric`
--

CREATE TABLE `sma_fabric` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `fitting_id` int(11) NOT NULL,
  `userby` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Deactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_fitting`
--

CREATE TABLE `sma_fitting` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `pattern_id` int(11) NOT NULL,
  `userby` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Deactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_gift_cards`
--

CREATE TABLE `sma_gift_cards` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `card_no` varchar(20) NOT NULL,
  `value` decimal(25,4) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `balance` decimal(25,4) NOT NULL,
  `expiry` date DEFAULT NULL,
  `created_by` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_groups`
--

CREATE TABLE `sma_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_groups`
--

INSERT INTO `sma_groups` (`id`, `name`, `description`) VALUES
(1, 'owner', 'Owner'),
(2, 'admin', 'Administrator'),
(3, 'customer', 'Customer'),
(4, 'supplier', 'Supplier'),
(5, 'Sales', 'Sales Staff'),
(6, 'Operator', 'head'),
(8, 'Cashier', 'Cashier staff'),
(9, 'Head', 'Head'),
(13, 'HR', 'Employee Recruitment '),
(14, 'storemanager', 'Store Manager');

-- --------------------------------------------------------

--
-- Table structure for table `sma_gst`
--

CREATE TABLE `sma_gst` (
  `id` int(11) NOT NULL,
  `store_id` int(11) DEFAULT NULL,
  `department` int(11) DEFAULT NULL,
  `section` int(11) DEFAULT NULL,
  `product_items` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `brands` int(11) DEFAULT NULL,
  `design` int(11) DEFAULT NULL,
  `style` int(11) DEFAULT NULL,
  `pattern` int(11) DEFAULT NULL,
  `fitting` int(11) DEFAULT NULL,
  `fabric` int(11) DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mrp` decimal(10,2) DEFAULT NULL,
  `mrpfrom` decimal(10,2) DEFAULT NULL,
  `mrpto` decimal(10,2) DEFAULT NULL,
  `cgst` decimal(10,2) DEFAULT NULL,
  `sgst` decimal(10,2) DEFAULT NULL,
  `hsncode` varchar(255) DEFAULT NULL,
  `cess` decimal(10,2) DEFAULT NULL,
  `addupgst` varchar(20) DEFAULT NULL,
  `status` enum('Active','Deactive') DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_hsncodes`
--

CREATE TABLE `sma_hsncodes` (
  `id` int(11) NOT NULL,
  `particulars` text NOT NULL,
  `hsncode` varchar(255) NOT NULL,
  `taxrate` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_in_stock_damage`
--

CREATE TABLE `sma_in_stock_damage` (
  `id` int(11) NOT NULL,
  `reference_no` varchar(55) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delivery_date` datetime DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `supplier` varchar(55) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` varchar(1000) NOT NULL,
  `total` decimal(25,4) DEFAULT NULL,
  `product_discount` decimal(25,4) DEFAULT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `order_discount` decimal(25,4) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT NULL,
  `product_tax` decimal(25,4) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `paid` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `status` varchar(55) DEFAULT '',
  `payment_status` varchar(20) DEFAULT 'pending',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `attachment` varchar(55) DEFAULT NULL,
  `lr_no` varchar(255) DEFAULT NULL,
  `ag_order_no` varchar(255) DEFAULT NULL,
  `purchase_no` varchar(255) DEFAULT NULL,
  `store_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_in_stock_damage_items`
--

CREATE TABLE `sma_in_stock_damage_items` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `transfer_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_cost` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(20) DEFAULT NULL,
  `discount` varchar(20) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `quantity_balance` decimal(15,4) DEFAULT '0.0000',
  `date` date NOT NULL,
  `status` varchar(50) NOT NULL,
  `unit_cost` decimal(25,4) DEFAULT NULL,
  `real_unit_cost` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_login_attempts`
--

CREATE TABLE `sma_login_attempts` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_login_attempts`
--

INSERT INTO `sma_login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(0, 0x3a3a31, 'admin@gmail.com', 1589937500);

-- --------------------------------------------------------

--
-- Table structure for table `sma_migrations`
--

CREATE TABLE `sma_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_migrations`
--

INSERT INTO `sma_migrations` (`version`) VALUES
(308);

-- --------------------------------------------------------

--
-- Table structure for table `sma_min_qty_lvl`
--

CREATE TABLE `sma_min_qty_lvl` (
  `id` int(11) NOT NULL,
  `store_id` int(11) DEFAULT NULL,
  `department` int(11) DEFAULT NULL,
  `section` int(11) DEFAULT NULL,
  `product_items` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `brands` int(11) DEFAULT NULL,
  `design` int(11) DEFAULT NULL,
  `style` int(11) DEFAULT NULL,
  `pattern` int(11) DEFAULT NULL,
  `fitting` int(11) DEFAULT NULL,
  `fabric` int(11) DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mrp` double(20,2) DEFAULT NULL,
  `mrp_from` double(20,2) DEFAULT NULL,
  `mrp_to` double(20,2) DEFAULT NULL,
  `min_qty_lvl` int(11) DEFAULT NULL,
  `min_qty_lvl_per` int(11) DEFAULT NULL,
  `min_qty_lvl_2` int(11) DEFAULT NULL,
  `min_qty_lvl_2_per` int(11) DEFAULT NULL,
  `reorder_qty` int(11) DEFAULT NULL,
  `reorder_qty_per` int(11) DEFAULT NULL,
  `reorder_qty_2` int(11) DEFAULT NULL,
  `reorder_qty_2_per` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userby` int(11) DEFAULT NULL,
  `status` enum('Active','Deactive') DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_notifications`
--

CREATE TABLE `sma_notifications` (
  `id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `from_date` datetime DEFAULT NULL,
  `till_date` datetime DEFAULT NULL,
  `scope` tinyint(1) NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_offers`
--

CREATE TABLE `sma_offers` (
  `id` int(11) NOT NULL,
  `offertype` varchar(255) DEFAULT NULL,
  `offer_name` varchar(255) DEFAULT NULL,
  `preference` varchar(255) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `is_repeat` enum('True','False') DEFAULT NULL,
  `repeated_days` text,
  `start_time` varchar(255) DEFAULT NULL,
  `end_time` varchar(255) DEFAULT NULL,
  `applicable_store_type` varchar(255) DEFAULT NULL,
  `sotres` varchar(255) DEFAULT NULL,
  `applicable_cust_type` varchar(255) DEFAULT NULL,
  `cust_groups` varchar(255) DEFAULT NULL,
  `categorys` varchar(255) DEFAULT NULL,
  `discount_level` text,
  `if_coupon_applocable` enum('True','False') DEFAULT 'False',
  `department` int(11) DEFAULT NULL,
  `section` int(11) DEFAULT NULL,
  `product_items` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `brands` int(11) DEFAULT NULL,
  `status` enum('Active','Deactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_order_ref`
--

CREATE TABLE `sma_order_ref` (
  `ref_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `br` int(11) NOT NULL DEFAULT '1',
  `so` int(11) NOT NULL DEFAULT '1',
  `qu` int(11) NOT NULL DEFAULT '1',
  `po` int(11) NOT NULL DEFAULT '1',
  `to` int(11) NOT NULL DEFAULT '1',
  `pos` int(11) NOT NULL DEFAULT '1',
  `do` int(11) NOT NULL DEFAULT '1',
  `pay` int(11) NOT NULL DEFAULT '1',
  `re` int(11) NOT NULL DEFAULT '1',
  `ex` int(11) NOT NULL DEFAULT '1',
  `pur` int(11) NOT NULL,
  `ctran` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_parcel_sent_voucher`
--

CREATE TABLE `sma_parcel_sent_voucher` (
  `id` int(11) NOT NULL,
  `receiver` varchar(200) DEFAULT NULL,
  `receiver_address` text,
  `transport` text,
  `veh_no` text,
  `through` text,
  `lr_no` varchar(200) DEFAULT NULL,
  `no_of_bales` int(11) DEFAULT NULL,
  `book_date` datetime DEFAULT NULL,
  `booked_by` int(11) DEFAULT NULL,
  `parcel_type` text,
  `rg_no` text,
  `goods_desc` text,
  `status` enum('Active','Deactive') DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userby` int(11) DEFAULT NULL,
  `store_id` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_pattern`
--

CREATE TABLE `sma_pattern` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `style_id` int(11) NOT NULL,
  `userby` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Deactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_payee`
--

CREATE TABLE `sma_payee` (
  `id` int(55) NOT NULL,
  `store_id` int(55) DEFAULT NULL,
  `bank_id` int(55) DEFAULT NULL,
  `ifsc_code` varchar(200) DEFAULT NULL,
  `account_no` text,
  `opening_balance` varchar(250) NOT NULL,
  `status` enum('Active','Deactive') DEFAULT NULL,
  `create_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_payments`
--

CREATE TABLE `sma_payments` (
  `id` int(11) NOT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sale_id` int(11) DEFAULT NULL,
  `return_id` int(11) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `reference_no` varchar(50) NOT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `paid_by` varchar(20) NOT NULL,
  `cheque_no` varchar(20) DEFAULT NULL,
  `cc_no` varchar(20) DEFAULT NULL,
  `cc_holder` varchar(25) DEFAULT NULL,
  `cc_month` varchar(2) DEFAULT NULL,
  `cc_year` varchar(4) DEFAULT NULL,
  `cc_type` varchar(20) DEFAULT NULL,
  `amount` decimal(25,4) NOT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `pos_paid` decimal(25,4) DEFAULT '0.0000',
  `pos_balance` decimal(25,4) DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_payment_vouchers`
--

CREATE TABLE `sma_payment_vouchers` (
  `id` int(50) NOT NULL,
  `type` enum('payment','receipt','contra') DEFAULT NULL,
  `payment_no` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `time` varchar(200) DEFAULT NULL,
  `for` varchar(255) DEFAULT NULL COMMENT 'payment for/ Received for / transfer type',
  `account` varchar(255) DEFAULT NULL COMMENT 'party acc/from account',
  `against_ref_no` varchar(255) DEFAULT NULL,
  `bank_acc_no` varchar(255) DEFAULT NULL,
  `datedd` datetime DEFAULT NULL,
  `amount` varchar(200) DEFAULT NULL,
  `payment_mode` varchar(255) DEFAULT NULL COMMENT 'payment_mode / to account',
  `from_acc` varchar(255) DEFAULT NULL COMMENT 'from acc/ to acc/ bank_acc_no',
  `pay_type_no` varchar(255) DEFAULT NULL COMMENT 'pay_type_no / mode of transfer',
  `bank_acc_number` varchar(255) DEFAULT NULL COMMENT 'bank_acc_no/ account_no / ref_no',
  `dated` datetime DEFAULT NULL,
  `balance` varchar(255) DEFAULT NULL,
  `narration` text,
  `status` enum('Active','Deactive') DEFAULT NULL,
  `created_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_paypal`
--

CREATE TABLE `sma_paypal` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `account_email` varchar(255) NOT NULL,
  `paypal_currency` varchar(3) NOT NULL DEFAULT 'USD',
  `fixed_charges` decimal(25,4) NOT NULL DEFAULT '2.0000',
  `extra_charges_my` decimal(25,4) NOT NULL DEFAULT '3.9000',
  `extra_charges_other` decimal(25,4) NOT NULL DEFAULT '4.4000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_paypal`
--

INSERT INTO `sma_paypal` (`id`, `active`, `account_email`, `paypal_currency`, `fixed_charges`, `extra_charges_my`, `extra_charges_other`) VALUES
(1, 1, 'mypaypal@paypal.com', 'USD', '122.0000', '1233.0000', '1290.0000');

-- --------------------------------------------------------

--
-- Table structure for table `sma_per`
--

CREATE TABLE `sma_per` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `decimal_req` tinyint(4) NOT NULL DEFAULT '0',
  `userby` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Deactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_permissions`
--

CREATE TABLE `sma_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `products-index` tinyint(1) DEFAULT '0',
  `products-add` tinyint(1) DEFAULT '0',
  `products-edit` tinyint(1) DEFAULT '0',
  `products-delete` tinyint(1) DEFAULT '0',
  `products-cost` tinyint(1) DEFAULT '0',
  `products-price` tinyint(1) DEFAULT '0',
  `quotes-index` tinyint(1) DEFAULT '0',
  `quotes-add` tinyint(1) DEFAULT '0',
  `quotes-edit` tinyint(1) DEFAULT '0',
  `quotes-pdf` tinyint(1) DEFAULT '0',
  `quotes-email` tinyint(1) DEFAULT '0',
  `quotes-delete` tinyint(1) DEFAULT '0',
  `sales-index` tinyint(1) DEFAULT '0',
  `sales-add` tinyint(1) DEFAULT '0',
  `sales-edit` tinyint(1) DEFAULT '0',
  `sales-pdf` tinyint(1) DEFAULT '0',
  `sales-email` tinyint(1) DEFAULT '0',
  `sales-delete` tinyint(1) DEFAULT '0',
  `purchases-index` tinyint(1) DEFAULT '0',
  `purchases-add` tinyint(1) DEFAULT '0',
  `purchases-edit` tinyint(1) DEFAULT '0',
  `purchases-pdf` tinyint(1) DEFAULT '0',
  `purchases-email` tinyint(1) DEFAULT '0',
  `purchases-delete` tinyint(1) DEFAULT '0',
  `transfers-index` tinyint(1) DEFAULT '0',
  `transfers-add` tinyint(1) DEFAULT '0',
  `transfers-edit` tinyint(1) DEFAULT '0',
  `transfers-pdf` tinyint(1) DEFAULT '0',
  `transfers-email` tinyint(1) DEFAULT '0',
  `transfers-delete` tinyint(1) DEFAULT '0',
  `customers-index` tinyint(1) DEFAULT '0',
  `customers-add` tinyint(1) DEFAULT '0',
  `customers-edit` tinyint(1) DEFAULT '0',
  `customers-delete` tinyint(1) DEFAULT '0',
  `suppliers-index` tinyint(1) DEFAULT '0',
  `suppliers-add` tinyint(1) DEFAULT '0',
  `suppliers-edit` tinyint(1) DEFAULT '0',
  `suppliers-delete` tinyint(1) DEFAULT '0',
  `sales-deliveries` tinyint(1) DEFAULT '0',
  `sales-add_delivery` tinyint(1) DEFAULT '0',
  `sales-edit_delivery` tinyint(1) DEFAULT '0',
  `sales-delete_delivery` tinyint(1) DEFAULT '0',
  `sales-email_delivery` tinyint(1) DEFAULT '0',
  `sales-pdf_delivery` tinyint(1) DEFAULT '0',
  `sales-gift_cards` tinyint(1) DEFAULT '0',
  `sales-add_gift_card` tinyint(1) DEFAULT '0',
  `sales-edit_gift_card` tinyint(1) DEFAULT '0',
  `sales-delete_gift_card` tinyint(1) DEFAULT '0',
  `pos-index` tinyint(1) DEFAULT '0',
  `sales-return_sales` tinyint(1) DEFAULT '0',
  `reports-index` tinyint(1) DEFAULT '0',
  `reports-warehouse_stock` tinyint(1) DEFAULT '0',
  `reports-quantity_alerts` tinyint(1) DEFAULT '0',
  `reports-expiry_alerts` tinyint(1) DEFAULT '0',
  `reports-products` tinyint(1) DEFAULT '0',
  `reports-daily_sales` tinyint(1) DEFAULT '0',
  `reports-monthly_sales` tinyint(1) DEFAULT '0',
  `reports-sales` tinyint(1) DEFAULT '0',
  `reports-payments` tinyint(1) DEFAULT '0',
  `reports-purchases` tinyint(1) DEFAULT '0',
  `reports-profit_loss` tinyint(1) DEFAULT '0',
  `reports-customers` tinyint(1) DEFAULT '0',
  `reports-suppliers` tinyint(1) DEFAULT '0',
  `reports-staff` tinyint(1) DEFAULT '0',
  `reports-register` tinyint(1) DEFAULT '0',
  `sales-payments` tinyint(1) DEFAULT '0',
  `purchases-payments` tinyint(1) DEFAULT '0',
  `purchases-expenses` tinyint(1) DEFAULT '0',
  `products-challan` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_permissions`
--

INSERT INTO `sma_permissions` (`id`, `group_id`, `products-index`, `products-add`, `products-edit`, `products-delete`, `products-cost`, `products-price`, `quotes-index`, `quotes-add`, `quotes-edit`, `quotes-pdf`, `quotes-email`, `quotes-delete`, `sales-index`, `sales-add`, `sales-edit`, `sales-pdf`, `sales-email`, `sales-delete`, `purchases-index`, `purchases-add`, `purchases-edit`, `purchases-pdf`, `purchases-email`, `purchases-delete`, `transfers-index`, `transfers-add`, `transfers-edit`, `transfers-pdf`, `transfers-email`, `transfers-delete`, `customers-index`, `customers-add`, `customers-edit`, `customers-delete`, `suppliers-index`, `suppliers-add`, `suppliers-edit`, `suppliers-delete`, `sales-deliveries`, `sales-add_delivery`, `sales-edit_delivery`, `sales-delete_delivery`, `sales-email_delivery`, `sales-pdf_delivery`, `sales-gift_cards`, `sales-add_gift_card`, `sales-edit_gift_card`, `sales-delete_gift_card`, `pos-index`, `sales-return_sales`, `reports-index`, `reports-warehouse_stock`, `reports-quantity_alerts`, `reports-expiry_alerts`, `reports-products`, `reports-daily_sales`, `reports-monthly_sales`, `reports-sales`, `reports-payments`, `reports-purchases`, `reports-profit_loss`, `reports-customers`, `reports-suppliers`, `reports-staff`, `reports-register`, `sales-payments`, `purchases-payments`, `purchases-expenses`, `products-challan`) VALUES
(1, 5, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, 1, NULL, NULL, 0),
(2, 6, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, 1, 1, 1, 1, 1, NULL, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, 1, 1, 0, 0, 1, NULL, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1),
(3, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(4, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, 1, NULL, NULL, 0),
(5, 9, 1, 1, 1, 1, NULL, NULL, 1, 1, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, 0),
(6, 9, 1, 1, 1, 1, NULL, NULL, 1, 1, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, 0),
(7, 10, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, 1, NULL, NULL, 0),
(8, 11, 1, 1, NULL, NULL, NULL, NULL, 1, 1, NULL, 1, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, 1, NULL, 1, 1, NULL, NULL, 1, 1, NULL, 1, 1, NULL, 1, 1, 1, 1, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, 0),
(9, 12, 1, 1, 1, NULL, NULL, NULL, 1, 1, 1, 1, 1, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, 0),
(10, 10, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, 1, NULL, NULL, 0),
(11, 11, 1, 1, NULL, NULL, NULL, NULL, 1, 1, NULL, 1, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, 1, NULL, 1, 1, NULL, NULL, 1, 1, NULL, 1, 1, NULL, 1, 1, 1, 1, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, 0),
(12, 12, 1, 1, 1, NULL, NULL, NULL, 1, 1, 1, 1, 1, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, 0),
(13, 13, 1, 1, 1, NULL, 1, 1, 1, 1, 1, NULL, 1, NULL, 1, 1, 1, 1, 1, NULL, 1, 1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, 1, 1, 1, 1, NULL, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1),
(14, 14, 1, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, 0),
(15, 15, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, 0),
(16, 14, 1, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, 0),
(17, 15, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, 0),
(18, 15, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, 0, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sma_position`
--

CREATE TABLE `sma_position` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `rate` varchar(255) NOT NULL,
  `incentivetype` varchar(255) NOT NULL,
  `usergroup` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` enum('Active','Deactive') NOT NULL,
  `userby` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_pos_cash_transfer`
--

CREATE TABLE `sma_pos_cash_transfer` (
  `id` int(11) NOT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `amount` double(10,4) NOT NULL,
  `empid_to` int(11) NOT NULL,
  `to_type` enum('Cr','Dr') NOT NULL,
  `empid_from` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `note` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_pos_register`
--

CREATE TABLE `sma_pos_register` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `cash_in_hand` decimal(25,4) NOT NULL,
  `status` varchar(10) NOT NULL,
  `total_cash` decimal(25,4) DEFAULT NULL,
  `total_cheques` int(11) DEFAULT NULL,
  `total_cc_slips` int(11) DEFAULT NULL,
  `total_cash_submitted` decimal(25,4) DEFAULT NULL,
  `total_cheques_submitted` int(11) DEFAULT NULL,
  `total_cc_slips_submitted` int(11) DEFAULT NULL,
  `note` text,
  `closed_at` timestamp NULL DEFAULT NULL,
  `transfer_opened_bills` varchar(50) DEFAULT NULL,
  `closed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_pos_settings`
--

CREATE TABLE `sma_pos_settings` (
  `pos_id` int(1) NOT NULL,
  `cat_limit` int(11) NOT NULL,
  `pro_limit` int(11) NOT NULL,
  `default_category` int(11) NOT NULL,
  `default_customer` int(11) NOT NULL,
  `default_biller` int(11) NOT NULL,
  `display_time` varchar(3) NOT NULL DEFAULT 'yes',
  `cf_title1` varchar(255) DEFAULT NULL,
  `cf_title2` varchar(255) DEFAULT NULL,
  `cf_value1` varchar(255) DEFAULT NULL,
  `cf_value2` varchar(255) DEFAULT NULL,
  `receipt_printer` varchar(55) DEFAULT NULL,
  `cash_drawer_codes` varchar(55) DEFAULT NULL,
  `focus_add_item` varchar(55) DEFAULT NULL,
  `add_manual_product` varchar(55) DEFAULT NULL,
  `customer_selection` varchar(55) DEFAULT NULL,
  `add_customer` varchar(55) DEFAULT NULL,
  `toggle_category_slider` varchar(55) DEFAULT NULL,
  `toggle_subcategory_slider` varchar(55) DEFAULT NULL,
  `cancel_sale` varchar(55) DEFAULT NULL,
  `suspend_sale` varchar(55) DEFAULT NULL,
  `print_items_list` varchar(55) DEFAULT NULL,
  `finalize_sale` varchar(55) DEFAULT NULL,
  `today_sale` varchar(55) DEFAULT NULL,
  `open_hold_bills` varchar(55) DEFAULT NULL,
  `close_register` varchar(55) DEFAULT NULL,
  `keyboard` tinyint(1) NOT NULL,
  `pos_printers` varchar(255) DEFAULT NULL,
  `java_applet` tinyint(1) NOT NULL,
  `product_button_color` varchar(20) NOT NULL DEFAULT 'default',
  `tooltips` tinyint(1) DEFAULT '1',
  `paypal_pro` tinyint(1) DEFAULT '0',
  `stripe` tinyint(1) DEFAULT '0',
  `rounding` tinyint(1) DEFAULT '0',
  `char_per_line` tinyint(4) DEFAULT '42',
  `pin_code` varchar(20) DEFAULT NULL,
  `purchase_code` varchar(100) DEFAULT 'purchase_code',
  `envato_username` varchar(50) DEFAULT 'envato_username',
  `version` varchar(10) DEFAULT '3.0.1.21',
  `extra_discount` varchar(55) NOT NULL,
  `config_setup` varchar(50) NOT NULL,
  `log_out` varchar(50) NOT NULL,
  `coupon_voucher` varchar(55) NOT NULL,
  `show_offers` varchar(55) NOT NULL,
  `hold_invoice` varchar(55) NOT NULL,
  `open_cash_drawer` varchar(55) NOT NULL,
  `pic_held_invoice` varchar(20) NOT NULL,
  `exit_bill` varchar(20) NOT NULL,
  `spot_discount` varchar(20) NOT NULL,
  `extra_charges` varchar(20) NOT NULL,
  `free_item` varchar(20) NOT NULL,
  `show_item_image` varchar(20) NOT NULL,
  `cancel_item_no` varchar(20) NOT NULL,
  `new_item_enquiry` varchar(20) NOT NULL,
  `available_item_size` varchar(20) NOT NULL,
  `good_return` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_pos_settings`
--

INSERT INTO `sma_pos_settings` (`pos_id`, `cat_limit`, `pro_limit`, `default_category`, `default_customer`, `default_biller`, `display_time`, `cf_title1`, `cf_title2`, `cf_value1`, `cf_value2`, `receipt_printer`, `cash_drawer_codes`, `focus_add_item`, `add_manual_product`, `customer_selection`, `add_customer`, `toggle_category_slider`, `toggle_subcategory_slider`, `cancel_sale`, `suspend_sale`, `print_items_list`, `finalize_sale`, `today_sale`, `open_hold_bills`, `close_register`, `keyboard`, `pos_printers`, `java_applet`, `product_button_color`, `tooltips`, `paypal_pro`, `stripe`, `rounding`, `char_per_line`, `pin_code`, `purchase_code`, `envato_username`, `version`, `extra_discount`, `config_setup`, `log_out`, `coupon_voucher`, `show_offers`, `hold_invoice`, `open_cash_drawer`, `pic_held_invoice`, `exit_bill`, `spot_discount`, `extra_charges`, `free_item`, `show_item_image`, `cancel_item_no`, `new_item_enquiry`, `available_item_size`, `good_return`) VALUES
(1, 22, 20, 1, 133, 104, '1', 'GST Reg', 'VAT Reg', '123456789', '987654321', 'BIXOLON SRP-350II', 'x1C', 'Ctrl+Shift+F', 'Ctrl+Shift+M', 'Ctrl+Shift+C', 'Ctrl+F2', 'Ctrl+F11', 'Ctrl+F122', 'F8', '-', 'F9', 'F88', 'F7', 'F2', 'Ctrl+F10', 0, 'BIXOLON SRP-350II, BIXOLON SRP-350II', 0, 'default', 1, 0, 0, 0, 42, NULL, 'purchase_code', 'envato_username', '3.0.1.21', 'Ctrl+F6', 'Ctrl+F12', 'F12', 'Ctrl+F9', 'Ctrl+F8', 'F2', 'F7', 'F3', '', 'Ctrl+F6', 'Ctrl+F7', 'F9', 'F5', 'F4', 'Ctrl+F3', 'Ctrl+1', 'F1');

-- --------------------------------------------------------

--
-- Table structure for table `sma_products`
--

CREATE TABLE `sma_products` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` char(255) NOT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `cost` decimal(25,4) DEFAULT NULL,
  `price` decimal(25,4) NOT NULL,
  `cper` int(11) DEFAULT NULL,
  `pper` int(11) DEFAULT NULL,
  `uper` int(11) DEFAULT NULL,
  `rateper` int(11) DEFAULT NULL,
  `ratetype` varchar(255) DEFAULT NULL,
  `singlerate` varchar(100) DEFAULT NULL,
  `mrprate` varchar(100) DEFAULT NULL,
  `mulratef` varchar(100) DEFAULT NULL,
  `mulratet` varchar(100) DEFAULT NULL,
  `alert_quantity` decimal(15,4) DEFAULT '0.0000',
  `image` varchar(255) DEFAULT 'no_image.png',
  `category_id` int(11) DEFAULT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `cf1` varchar(255) DEFAULT NULL,
  `cf2` varchar(255) DEFAULT NULL,
  `cf3` varchar(255) DEFAULT NULL,
  `cf4` varchar(255) DEFAULT NULL,
  `cf5` varchar(255) DEFAULT NULL,
  `cf6` varchar(255) DEFAULT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `tax_rate` int(11) DEFAULT NULL,
  `track_quantity` tinyint(1) DEFAULT '1',
  `details` varchar(1000) DEFAULT NULL,
  `warehouse` int(11) DEFAULT NULL,
  `barcode_symbology` varchar(55) NOT NULL DEFAULT 'code128',
  `file` varchar(100) DEFAULT NULL,
  `product_details` text,
  `tax_method` tinyint(1) DEFAULT '0',
  `type` varchar(55) NOT NULL DEFAULT 'standard',
  `store_id` int(11) DEFAULT NULL,
  `department` int(11) DEFAULT NULL,
  `section` int(11) DEFAULT NULL,
  `product_items` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `brands` int(11) DEFAULT NULL,
  `design` int(11) DEFAULT NULL,
  `style` int(11) DEFAULT NULL,
  `pattern` int(11) DEFAULT NULL,
  `fitting` int(11) DEFAULT NULL,
  `fabric` int(11) DEFAULT NULL,
  `sizeangle` varchar(10) DEFAULT NULL,
  `sizetype` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `colortype` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `colorqty` int(11) DEFAULT NULL,
  `combo_discount` int(11) DEFAULT NULL,
  `batch` varchar(255) DEFAULT NULL,
  `batchdate` datetime NOT NULL,
  `supplier1` int(11) DEFAULT NULL,
  `supplier1price` decimal(25,4) DEFAULT NULL,
  `supplier2` int(11) DEFAULT NULL,
  `supplier2price` decimal(25,4) DEFAULT NULL,
  `supplier3` int(11) DEFAULT NULL,
  `supplier3price` decimal(25,4) DEFAULT NULL,
  `supplier4` int(11) DEFAULT NULL,
  `supplier4price` decimal(25,4) DEFAULT NULL,
  `supplier5` int(11) DEFAULT NULL,
  `supplier5price` decimal(25,4) DEFAULT NULL,
  `status` enum('Active','Deactive') NOT NULL DEFAULT 'Active',
  `squantity` decimal(15,4) DEFAULT NULL,
  `sunit` varchar(50) DEFAULT NULL,
  `colorcode` varchar(255) DEFAULT NULL,
  `billno` varchar(255) DEFAULT NULL,
  `hsn` varchar(255) DEFAULT NULL,
  `gst` varchar(255) DEFAULT NULL,
  `addup` enum('0','1') DEFAULT NULL,
  `adduppercentage` varchar(255) DEFAULT NULL,
  `cess` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_box`
--

CREATE TABLE `sma_product_box` (
  `id` int(11) NOT NULL,
  `store` int(11) DEFAULT NULL,
  `department` int(11) DEFAULT NULL,
  `section` int(11) DEFAULT NULL,
  `product_item` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `brand` int(11) DEFAULT NULL,
  `design` int(11) DEFAULT NULL,
  `style` int(11) DEFAULT NULL,
  `pattern` int(11) DEFAULT NULL,
  `fitting` int(11) DEFAULT NULL,
  `fabric` int(11) DEFAULT NULL,
  `no_of_pic` int(11) DEFAULT NULL,
  `no_of_box` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_challan`
--

CREATE TABLE `sma_product_challan` (
  `challan_no` int(11) NOT NULL,
  `reference_no` varchar(55) NOT NULL,
  `challan_name` varchar(50) NOT NULL,
  `challan_date` datetime NOT NULL,
  `dispatch_date` datetime NOT NULL,
  `customer_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` varchar(1000) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_challan_items`
--

CREATE TABLE `sma_product_challan_items` (
  `id` int(11) NOT NULL,
  `challan_no` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_discount`
--

CREATE TABLE `sma_product_discount` (
  `id` int(11) NOT NULL,
  `store_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `product_item_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `brands_id` int(11) DEFAULT NULL,
  `discount` int(3) DEFAULT NULL,
  `userby` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Deactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_items`
--

CREATE TABLE `sma_product_items` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `section_id` int(11) NOT NULL,
  `userby` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Deactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_margin`
--

CREATE TABLE `sma_product_margin` (
  `id` int(11) NOT NULL,
  `store_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `product_item_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `brands_id` int(11) DEFAULT NULL,
  `margin` int(3) DEFAULT NULL,
  `userby` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Deactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_photos`
--

CREATE TABLE `sma_product_photos` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `photo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_received_voucher`
--

CREATE TABLE `sma_product_received_voucher` (
  `id` int(11) NOT NULL,
  `sender` int(11) DEFAULT NULL,
  `sender_address` text,
  `transport` text,
  `veh_no` text,
  `hamal` text,
  `lr_no` text,
  `no_of_bales` int(11) DEFAULT NULL,
  `bk_date` datetime DEFAULT NULL,
  `receiver` varchar(100) DEFAULT NULL,
  `parcel_type` text,
  `delivery` enum('DOOR','Collect_By_Sales') DEFAULT NULL,
  `goods_desc` text,
  `status` enum('Active','Deactive') DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modified_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userby` int(11) DEFAULT NULL,
  `store_id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_seasonal_discount`
--

CREATE TABLE `sma_product_seasonal_discount` (
  `id` int(11) NOT NULL,
  `store_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `product_item_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `brands_id` int(11) DEFAULT NULL,
  `margin` int(3) DEFAULT NULL,
  `reg_discount` int(3) DEFAULT NULL,
  `extra_discount` int(3) DEFAULT NULL,
  `user_by` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Deactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_special_discount`
--

CREATE TABLE `sma_product_special_discount` (
  `id` int(11) NOT NULL,
  `product_code` varchar(100) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `mrp` double(20,4) DEFAULT NULL,
  `purchase_rate` double(20,4) DEFAULT NULL,
  `margin` int(3) DEFAULT NULL,
  `old_discount` int(3) DEFAULT NULL,
  `special_discount` int(3) DEFAULT NULL,
  `userby` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Deactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_variants`
--

CREATE TABLE `sma_product_variants` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `cost` decimal(25,4) DEFAULT NULL,
  `price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_purchases`
--

CREATE TABLE `sma_purchases` (
  `id` int(11) NOT NULL,
  `reference_no` varchar(55) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delivery_date` datetime DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `supplier` varchar(55) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` varchar(1000) NOT NULL,
  `total` decimal(25,4) DEFAULT NULL,
  `product_discount` decimal(25,4) DEFAULT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `order_discount` decimal(25,4) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT NULL,
  `product_tax` decimal(25,4) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `paid` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `status` varchar(55) DEFAULT '',
  `payment_status` varchar(20) DEFAULT 'pending',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `attachment` varchar(55) DEFAULT NULL,
  `lr_no` varchar(255) DEFAULT NULL,
  `ag_order_no` varchar(255) DEFAULT NULL,
  `purchase_no` varchar(255) DEFAULT NULL,
  `store_id` int(11) NOT NULL,
  `pur_challan` enum('0','1') NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL,
  `sorted_by` int(11) DEFAULT NULL,
  `packing_charges_id` varchar(255) DEFAULT NULL,
  `packingcharges` decimal(24,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_purchases_return`
--

CREATE TABLE `sma_purchases_return` (
  `id` int(11) NOT NULL,
  `reference_no` varchar(55) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `purchase_return_no` varchar(255) DEFAULT NULL,
  `delivery_date` datetime DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `supplier` varchar(55) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` varchar(1000) NOT NULL,
  `total` decimal(25,4) DEFAULT NULL,
  `product_discount` decimal(25,4) DEFAULT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `order_discount` decimal(25,4) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT NULL,
  `product_tax` decimal(25,4) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `paid` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `status` varchar(55) DEFAULT '',
  `payment_status` varchar(20) DEFAULT 'pending',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `attachment` varchar(55) DEFAULT NULL,
  `lr_no` varchar(255) DEFAULT NULL,
  `ag_order_no` varchar(255) DEFAULT NULL,
  `purchase_no` varchar(255) DEFAULT NULL,
  `store_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `challan` int(11) DEFAULT '0',
  `damage_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_purchases_return_without_barcode`
--

CREATE TABLE `sma_purchases_return_without_barcode` (
  `id` int(11) NOT NULL,
  `reference_no` varchar(55) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delivery_date` datetime DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `supplier` varchar(55) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` varchar(1000) NOT NULL,
  `total` decimal(25,4) DEFAULT NULL,
  `product_discount` decimal(25,4) DEFAULT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `order_discount` decimal(25,4) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT NULL,
  `product_tax` decimal(25,4) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `paid` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `status` varchar(55) DEFAULT '',
  `payment_status` varchar(20) DEFAULT 'pending',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `attachment` varchar(55) DEFAULT NULL,
  `lr_no` varchar(255) DEFAULT NULL,
  `ag_order_no` varchar(255) DEFAULT NULL,
  `purchase_no` varchar(255) DEFAULT NULL,
  `store_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL,
  `challan` int(11) DEFAULT '0',
  `damage_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_purchase_items`
--

CREATE TABLE `sma_purchase_items` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `transfer_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_cost` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(20) DEFAULT NULL,
  `discount` varchar(20) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `quantity_balance` decimal(15,4) DEFAULT '0.0000',
  `date` date NOT NULL,
  `status` varchar(50) NOT NULL,
  `unit_cost` decimal(25,4) DEFAULT NULL,
  `real_unit_cost` decimal(25,4) DEFAULT NULL,
  `hsn` varchar(255) DEFAULT NULL,
  `gst` varchar(255) DEFAULT NULL,
  `addup` enum('0','1') DEFAULT NULL,
  `adduppercentage` int(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_purchase_return_items`
--

CREATE TABLE `sma_purchase_return_items` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `transfer_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_cost` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(20) DEFAULT NULL,
  `discount` varchar(20) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `quantity_balance` decimal(15,4) DEFAULT '0.0000',
  `date` date NOT NULL,
  `status` varchar(50) NOT NULL,
  `unit_cost` decimal(25,4) DEFAULT NULL,
  `real_unit_cost` decimal(25,4) DEFAULT NULL,
  `hsn` varchar(255) DEFAULT NULL,
  `gst` varchar(255) DEFAULT NULL,
  `addup` enum('0','1') DEFAULT NULL,
  `adduppercentage` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_purchase_return_items_without_barcode`
--

CREATE TABLE `sma_purchase_return_items_without_barcode` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `transfer_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_cost` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(20) DEFAULT NULL,
  `discount` varchar(20) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `quantity_balance` decimal(15,4) DEFAULT '0.0000',
  `date` date NOT NULL,
  `status` varchar(50) NOT NULL,
  `unit_cost` decimal(25,4) DEFAULT NULL,
  `real_unit_cost` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_purchase_return_product_without_barcode`
--

CREATE TABLE `sma_purchase_return_product_without_barcode` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` char(255) NOT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `cost` decimal(25,4) DEFAULT NULL,
  `price` decimal(25,4) NOT NULL,
  `cper` int(11) DEFAULT NULL,
  `pper` int(11) DEFAULT NULL,
  `uper` int(11) DEFAULT NULL,
  `rateper` int(11) DEFAULT NULL,
  `ratetype` varchar(255) DEFAULT NULL,
  `singlerate` varchar(100) DEFAULT NULL,
  `mrprate` varchar(100) DEFAULT NULL,
  `mulratef` varchar(100) DEFAULT NULL,
  `mulratet` varchar(100) DEFAULT NULL,
  `alert_quantity` decimal(15,4) DEFAULT '0.0000',
  `image` varchar(255) DEFAULT 'no_image.png',
  `category_id` int(11) DEFAULT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `cf1` varchar(255) DEFAULT NULL,
  `cf2` varchar(255) DEFAULT NULL,
  `cf3` varchar(255) DEFAULT NULL,
  `cf4` varchar(255) DEFAULT NULL,
  `cf5` varchar(255) DEFAULT NULL,
  `cf6` varchar(255) DEFAULT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `tax_rate` int(11) DEFAULT NULL,
  `track_quantity` tinyint(1) DEFAULT '1',
  `details` varchar(1000) DEFAULT NULL,
  `warehouse` int(11) DEFAULT NULL,
  `barcode_symbology` varchar(55) NOT NULL DEFAULT 'code128',
  `file` varchar(100) DEFAULT NULL,
  `product_details` text,
  `tax_method` tinyint(1) DEFAULT '0',
  `type` varchar(55) NOT NULL DEFAULT 'standard',
  `store_id` int(11) DEFAULT NULL,
  `department` int(11) DEFAULT NULL,
  `section` int(11) DEFAULT NULL,
  `product_items` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `brands` int(11) DEFAULT NULL,
  `design` int(11) DEFAULT NULL,
  `style` int(11) DEFAULT NULL,
  `pattern` int(11) DEFAULT NULL,
  `fitting` int(11) DEFAULT NULL,
  `fabric` int(11) DEFAULT NULL,
  `sizeangle` varchar(10) DEFAULT NULL,
  `sizetype` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `colortype` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `colorqty` int(11) DEFAULT NULL,
  `combo_discount` int(11) DEFAULT NULL,
  `batch` varchar(255) DEFAULT NULL,
  `batchdate` datetime NOT NULL,
  `supplier1` int(11) DEFAULT NULL,
  `supplier1price` decimal(25,4) DEFAULT NULL,
  `supplier2` int(11) DEFAULT NULL,
  `supplier2price` decimal(25,4) DEFAULT NULL,
  `supplier3` int(11) DEFAULT NULL,
  `supplier3price` decimal(25,4) DEFAULT NULL,
  `supplier4` int(11) DEFAULT NULL,
  `supplier4price` decimal(25,4) DEFAULT NULL,
  `supplier5` int(11) DEFAULT NULL,
  `supplier5price` decimal(25,4) DEFAULT NULL,
  `status` enum('Active','Deactive') NOT NULL DEFAULT 'Active',
  `squantity` decimal(15,4) DEFAULT NULL,
  `sunit` varchar(50) DEFAULT NULL,
  `colorcode` varchar(255) NOT NULL,
  `billno` varchar(255) DEFAULT NULL,
  `hsn` varchar(255) DEFAULT NULL,
  `gst` varchar(255) DEFAULT NULL,
  `addup` enum('0','1') DEFAULT NULL,
  `adduppercentage` varchar(255) DEFAULT NULL,
  `cess` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_quotes`
--

CREATE TABLE `sma_quotes` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `biller_id` int(11) NOT NULL,
  `biller` varchar(55) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `internal_note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount` decimal(25,4) DEFAULT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT '0.0000',
  `product_tax` decimal(25,4) DEFAULT '0.0000',
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT NULL,
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `attachment` varchar(55) DEFAULT NULL,
  `estimate_delievery` datetime NOT NULL,
  `transport` varchar(250) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_quotes_products`
--

CREATE TABLE `sma_quotes_products` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` char(255) NOT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `cost` decimal(25,4) DEFAULT NULL,
  `price` decimal(25,4) NOT NULL,
  `cper` int(11) DEFAULT NULL,
  `pper` int(11) DEFAULT NULL,
  `uper` int(11) DEFAULT NULL,
  `rateper` int(11) DEFAULT NULL,
  `ratetype` varchar(255) DEFAULT NULL,
  `singlerate` varchar(100) DEFAULT NULL,
  `mrprate` varchar(100) DEFAULT NULL,
  `mulratef` varchar(100) DEFAULT NULL,
  `mulratet` varchar(100) DEFAULT NULL,
  `alert_quantity` decimal(15,4) DEFAULT '0.0000',
  `image` varchar(255) DEFAULT 'no_image.png',
  `category_id` int(11) DEFAULT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `cf1` varchar(255) DEFAULT NULL,
  `cf2` varchar(255) DEFAULT NULL,
  `cf3` varchar(255) DEFAULT NULL,
  `cf4` varchar(255) DEFAULT NULL,
  `cf5` varchar(255) DEFAULT NULL,
  `cf6` varchar(255) DEFAULT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `tax_rate` int(11) DEFAULT NULL,
  `track_quantity` tinyint(1) DEFAULT '1',
  `details` varchar(1000) DEFAULT NULL,
  `warehouse` int(11) DEFAULT NULL,
  `barcode_symbology` varchar(55) NOT NULL DEFAULT 'code128',
  `file` varchar(100) DEFAULT NULL,
  `product_details` text,
  `tax_method` tinyint(1) DEFAULT '0',
  `type` varchar(55) DEFAULT 'standard',
  `store_id` int(11) DEFAULT NULL,
  `department` int(11) DEFAULT NULL,
  `section` int(11) DEFAULT NULL,
  `product_items` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `brands` int(11) DEFAULT NULL,
  `design` int(11) DEFAULT NULL,
  `style` int(11) DEFAULT NULL,
  `pattern` int(11) DEFAULT NULL,
  `fitting` int(11) DEFAULT NULL,
  `fabric` int(11) DEFAULT NULL,
  `sizeangle` varchar(10) DEFAULT NULL,
  `sizetype` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `colortype` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `colorqty` int(11) DEFAULT NULL,
  `combo_discount` int(11) DEFAULT NULL,
  `batch` varchar(255) DEFAULT NULL,
  `batchdate` datetime NOT NULL,
  `supplier1` int(11) DEFAULT NULL,
  `supplier1price` decimal(25,4) DEFAULT NULL,
  `supplier2` int(11) DEFAULT NULL,
  `supplier2price` decimal(25,4) DEFAULT NULL,
  `supplier3` int(11) DEFAULT NULL,
  `supplier3price` decimal(25,4) DEFAULT NULL,
  `supplier4` int(11) DEFAULT NULL,
  `supplier4price` decimal(25,4) DEFAULT NULL,
  `supplier5` int(11) DEFAULT NULL,
  `supplier5price` decimal(25,4) DEFAULT NULL,
  `status` enum('Active','Deactive') NOT NULL DEFAULT 'Active',
  `squantity` decimal(15,4) DEFAULT NULL,
  `sunit` varchar(50) DEFAULT NULL,
  `colorcode` varchar(255) NOT NULL,
  `billno` varchar(255) DEFAULT NULL,
  `hsn` varchar(255) DEFAULT NULL,
  `gst` varchar(255) DEFAULT NULL,
  `addup` enum('0','1') DEFAULT NULL,
  `adduppercentage` varchar(255) DEFAULT NULL,
  `cess` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_quote_items`
--

CREATE TABLE `sma_quote_items` (
  `id` int(11) NOT NULL,
  `quote_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_receiptmodule`
--

CREATE TABLE `sma_receiptmodule` (
  `id` int(11) NOT NULL,
  `coupon_code` varchar(20) DEFAULT NULL,
  `coupon_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_receiptmodule_product`
--

CREATE TABLE `sma_receiptmodule_product` (
  `id` int(11) NOT NULL,
  `receiptmodule_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_code` varchar(20) DEFAULT NULL,
  `product_name` varchar(500) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_cost` varchar(20) DEFAULT NULL,
  `unit_cost` varchar(20) DEFAULT NULL,
  `quantity` varchar(20) DEFAULT NULL,
  `quantity_balance` varchar(30) DEFAULT NULL,
  `warehouse_id` int(3) DEFAULT NULL,
  `item_tax` varchar(10) DEFAULT NULL,
  `tax_rate_id` varchar(3) DEFAULT NULL,
  `tax` varchar(5) DEFAULT NULL,
  `discount` varchar(11) DEFAULT NULL,
  `item_discount` varchar(11) DEFAULT NULL,
  `subtotal` varbinary(20) DEFAULT NULL,
  `expiry` varchar(20) DEFAULT NULL,
  `real_unit_cost` varchar(30) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_receipt_vouchers`
--

CREATE TABLE `sma_receipt_vouchers` (
  `id` int(50) NOT NULL,
  `payment_no` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `time` varchar(200) DEFAULT NULL,
  `received_for` varchar(255) DEFAULT NULL COMMENT 'payment for/ Received for / transfer type',
  `party_account` varchar(255) DEFAULT NULL COMMENT 'party acc/from account',
  `against_ref_no` varchar(255) DEFAULT NULL,
  `bank_acc_no` varchar(255) DEFAULT NULL,
  `datedd` datetime DEFAULT NULL,
  `amount` varchar(200) DEFAULT NULL,
  `payment_mode` varchar(255) DEFAULT NULL COMMENT 'payment_mode / to account',
  `to_acc` varchar(255) DEFAULT NULL COMMENT 'from acc/ to acc/ bank_acc_no',
  `pay_type_no` varchar(255) DEFAULT NULL COMMENT 'pay_type_no / mode of transfer',
  `acc_number` varchar(255) DEFAULT NULL COMMENT 'bank_acc_no/ account_no / ref_no',
  `dated` datetime DEFAULT NULL,
  `balance` varchar(255) DEFAULT NULL,
  `narration` text,
  `status` enum('Active','Deactive') DEFAULT NULL,
  `created_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_recive_payments`
--

CREATE TABLE `sma_recive_payments` (
  `id` int(11) NOT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sale_id` int(11) DEFAULT NULL,
  `purchase_return_barcode` int(11) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `reference_no` varchar(50) NOT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `paid_by` varchar(20) NOT NULL,
  `cheque_no` varchar(20) DEFAULT NULL,
  `cc_no` varchar(20) DEFAULT NULL,
  `cc_holder` varchar(25) DEFAULT NULL,
  `cc_month` varchar(2) DEFAULT NULL,
  `cc_year` varchar(4) DEFAULT NULL,
  `cc_type` varchar(20) DEFAULT NULL,
  `amount` decimal(25,4) NOT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `pos_paid` decimal(25,4) DEFAULT '0.0000',
  `pos_balance` decimal(25,4) DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_retailbill`
--

CREATE TABLE `sma_retailbill` (
  `id` int(11) NOT NULL,
  `retail_bill_no` varchar(20) DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `items_detail` text,
  `grand_total` varchar(20) DEFAULT NULL,
  `crate_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_returnp_items`
--

CREATE TABLE `sma_returnp_items` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) UNSIGNED DEFAULT NULL,
  `transfer_id` int(11) DEFAULT NULL,
  `return_id` int(11) UNSIGNED NOT NULL,
  `purchase_item_id` int(11) DEFAULT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `credit_note_inv` varchar(20) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_return_items`
--

CREATE TABLE `sma_return_items` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) UNSIGNED NOT NULL,
  `return_id` int(11) UNSIGNED NOT NULL,
  `sale_item_id` int(11) DEFAULT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_return_purchase`
--

CREATE TABLE `sma_return_purchase` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `supplier` varchar(55) NOT NULL,
  `biller_id` int(11) DEFAULT NULL,
  `biller` varchar(55) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount_id` varchar(20) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount` decimal(25,4) DEFAULT '0.0000',
  `product_tax` decimal(25,4) DEFAULT '0.0000',
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT '0.0000',
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `surcharge` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `credit_note_inv` varchar(20) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_return_sales`
--

CREATE TABLE `sma_return_sales` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `biller_id` int(11) NOT NULL,
  `biller` varchar(55) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount_id` varchar(20) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount` decimal(25,4) DEFAULT '0.0000',
  `product_tax` decimal(25,4) DEFAULT '0.0000',
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT '0.0000',
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `surcharge` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `ret_type` enum('pos','sale') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_sales`
--

CREATE TABLE `sma_sales` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `biller_id` int(11) NOT NULL,
  `biller` varchar(55) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `staff_note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount_id` varchar(20) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount` decimal(25,4) DEFAULT '0.0000',
  `product_tax` decimal(25,4) DEFAULT '0.0000',
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT '0.0000',
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `sale_status` varchar(20) DEFAULT NULL,
  `payment_status` varchar(20) DEFAULT NULL,
  `payment_term` tinyint(4) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_items` tinyint(4) DEFAULT NULL,
  `pos` tinyint(1) NOT NULL DEFAULT '0',
  `paid` decimal(25,4) DEFAULT '0.0000',
  `return_id` int(11) DEFAULT NULL,
  `surcharge` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `attachment` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_sale_incentive`
--

CREATE TABLE `sma_sale_incentive` (
  `id` int(11) NOT NULL,
  `store_id` int(11) DEFAULT NULL,
  `department` int(11) DEFAULT NULL,
  `section` int(11) DEFAULT NULL,
  `product_items` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `brands` int(11) DEFAULT NULL,
  `design` int(11) DEFAULT NULL,
  `style` int(11) DEFAULT NULL,
  `pattern` int(11) DEFAULT NULL,
  `fitting` int(11) DEFAULT NULL,
  `fabric` int(11) DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `mrp` double(20,2) DEFAULT NULL,
  `mrp_from` double(20,2) DEFAULT NULL,
  `mrp_to` double(20,2) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `userby` int(11) DEFAULT NULL,
  `status` enum('Active','Deactive') DEFAULT NULL,
  `sales_incentive` int(11) DEFAULT NULL,
  `per` int(11) DEFAULT NULL,
  `incentive_type` enum('fixed','percentage') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_sale_items`
--

CREATE TABLE `sma_sale_items` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `emp_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_section`
--

CREATE TABLE `sma_section` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `department_id` int(11) NOT NULL,
  `userby` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Deactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_sessions`
--

CREATE TABLE `sma_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_sessions`
--

INSERT INTO `sma_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('2au7d1t1adq4kdt47eij5chb2qu6icoc', '::1', 1589940195, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538393934303139353b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b757365726e616d657c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353839393338343239223b6c6173745f69707c733a333a223a3a31223b6176617461727c733a33363a2232356136353931386530646536363530646637656636333232323164626235342e6a7067223b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c733a313a2230223b62696c6c65725f69647c733a313a2230223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b),
('710lf9lvvq5hvdkerh6p0cf143d1pb7c', '::1', 1589939526, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538393933393532363b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b757365726e616d657c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353839393338343239223b6c6173745f69707c733a333a223a3a31223b6176617461727c733a33363a2232356136353931386530646536363530646637656636333232323164626235342e6a7067223b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c733a313a2230223b62696c6c65725f69647c733a313a2230223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b),
('cpbbm6oig75ichjmmbkp8rlo6tbohfrq', '::1', 1589942304, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538393934323038333b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b757365726e616d657c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353839393338343239223b6c6173745f69707c733a333a223a3a31223b6176617461727c733a33363a2232356136353931386530646536363530646637656636333232323164626235342e6a7067223b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c733a313a2230223b62696c6c65725f69647c733a313a2230223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b),
('o4b3dlnvcefv9fa9k81hep1rmhchhtsn', '::1', 1589938867, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538393933383836373b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b757365726e616d657c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2235223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353839393338313630223b6c6173745f69707c733a333a223a3a31223b6176617461727c733a33363a2232356136353931386530646536363530646637656636333232323164626235342e6a7067223b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c733a313a2230223b62696c6c65725f69647c733a313a2230223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b),
('rhpr4ndbrnh5qn0bnfsdhigbgk83tipf', '::1', 1589942083, 0x5f5f63695f6c6173745f726567656e65726174657c693a313538393934323038333b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b757365726e616d657c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353839393338343239223b6c6173745f69707c733a333a223a3a31223b6176617461727c733a33363a2232356136353931386530646536363530646637656636333232323164626235342e6a7067223b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c733a313a2230223b62696c6c65725f69647c733a313a2230223b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b);

-- --------------------------------------------------------

--
-- Table structure for table `sma_settings`
--

CREATE TABLE `sma_settings` (
  `setting_id` int(1) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `logo2` varchar(255) NOT NULL,
  `site_name` varchar(55) NOT NULL,
  `language` varchar(20) NOT NULL,
  `default_warehouse` int(2) NOT NULL,
  `accounting_method` tinyint(4) NOT NULL DEFAULT '0',
  `default_currency` varchar(3) NOT NULL,
  `default_tax_rate` int(2) NOT NULL,
  `rows_per_page` int(2) NOT NULL,
  `version` varchar(10) NOT NULL DEFAULT '1.0',
  `default_tax_rate2` int(11) NOT NULL DEFAULT '0',
  `dateformat` int(11) NOT NULL,
  `sales_prefix` varchar(20) DEFAULT NULL,
  `quote_prefix` varchar(20) DEFAULT NULL,
  `purchase_prefix` varchar(20) DEFAULT NULL,
  `transfer_prefix` varchar(20) DEFAULT NULL,
  `delivery_prefix` varchar(20) DEFAULT NULL,
  `payment_prefix` varchar(20) DEFAULT NULL,
  `return_prefix` varchar(20) DEFAULT NULL,
  `expense_prefix` varchar(20) DEFAULT NULL,
  `item_addition` tinyint(1) NOT NULL DEFAULT '0',
  `theme` varchar(20) NOT NULL,
  `product_serial` tinyint(4) NOT NULL,
  `default_discount` int(11) NOT NULL,
  `product_discount` tinyint(1) NOT NULL DEFAULT '0',
  `discount_method` tinyint(4) NOT NULL,
  `tax1` tinyint(4) NOT NULL,
  `tax2` tinyint(4) NOT NULL,
  `overselling` tinyint(1) NOT NULL DEFAULT '0',
  `restrict_user` tinyint(4) NOT NULL DEFAULT '0',
  `restrict_calendar` tinyint(4) NOT NULL DEFAULT '0',
  `timezone` varchar(100) DEFAULT NULL,
  `iwidth` int(11) NOT NULL DEFAULT '0',
  `iheight` int(11) NOT NULL,
  `twidth` int(11) NOT NULL,
  `theight` int(11) NOT NULL,
  `watermark` tinyint(1) DEFAULT NULL,
  `reg_ver` tinyint(1) DEFAULT NULL,
  `allow_reg` tinyint(1) DEFAULT NULL,
  `reg_notification` tinyint(1) DEFAULT NULL,
  `auto_reg` tinyint(1) DEFAULT NULL,
  `protocol` varchar(20) NOT NULL DEFAULT 'mail',
  `mailpath` varchar(55) DEFAULT '/usr/sbin/sendmail',
  `smtp_host` varchar(100) DEFAULT NULL,
  `smtp_user` varchar(100) DEFAULT NULL,
  `smtp_pass` varchar(255) DEFAULT NULL,
  `smtp_port` varchar(10) DEFAULT '25',
  `smtp_crypto` varchar(10) DEFAULT NULL,
  `corn` datetime DEFAULT NULL,
  `customer_group` int(11) NOT NULL,
  `default_email` varchar(100) NOT NULL,
  `mmode` tinyint(1) NOT NULL,
  `bc_fix` tinyint(4) NOT NULL DEFAULT '0',
  `auto_detect_barcode` tinyint(1) NOT NULL DEFAULT '0',
  `captcha` tinyint(1) NOT NULL DEFAULT '1',
  `reference_format` tinyint(1) NOT NULL DEFAULT '1',
  `racks` tinyint(1) DEFAULT '0',
  `attributes` tinyint(1) NOT NULL DEFAULT '0',
  `product_expiry` tinyint(1) NOT NULL DEFAULT '0',
  `decimals` tinyint(2) NOT NULL DEFAULT '2',
  `qty_decimals` tinyint(2) NOT NULL DEFAULT '2',
  `decimals_sep` varchar(2) NOT NULL DEFAULT '.',
  `thousands_sep` varchar(2) NOT NULL DEFAULT ',',
  `invoice_view` tinyint(1) DEFAULT '0',
  `default_biller` int(11) DEFAULT NULL,
  `envato_username` varchar(50) DEFAULT NULL,
  `purchase_code` varchar(100) DEFAULT NULL,
  `rtl` tinyint(1) DEFAULT '0',
  `each_spent` decimal(15,4) DEFAULT NULL,
  `ca_point` tinyint(4) DEFAULT NULL,
  `each_sale` decimal(15,4) DEFAULT NULL,
  `sa_point` tinyint(4) DEFAULT NULL,
  `update` tinyint(1) DEFAULT '0',
  `sac` tinyint(1) DEFAULT '0',
  `display_all_products` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_settings`
--

INSERT INTO `sma_settings` (`setting_id`, `logo`, `logo2`, `site_name`, `language`, `default_warehouse`, `accounting_method`, `default_currency`, `default_tax_rate`, `rows_per_page`, `version`, `default_tax_rate2`, `dateformat`, `sales_prefix`, `quote_prefix`, `purchase_prefix`, `transfer_prefix`, `delivery_prefix`, `payment_prefix`, `return_prefix`, `expense_prefix`, `item_addition`, `theme`, `product_serial`, `default_discount`, `product_discount`, `discount_method`, `tax1`, `tax2`, `overselling`, `restrict_user`, `restrict_calendar`, `timezone`, `iwidth`, `iheight`, `twidth`, `theight`, `watermark`, `reg_ver`, `allow_reg`, `reg_notification`, `auto_reg`, `protocol`, `mailpath`, `smtp_host`, `smtp_user`, `smtp_pass`, `smtp_port`, `smtp_crypto`, `corn`, `customer_group`, `default_email`, `mmode`, `bc_fix`, `auto_detect_barcode`, `captcha`, `reference_format`, `racks`, `attributes`, `product_expiry`, `decimals`, `qty_decimals`, `decimals_sep`, `thousands_sep`, `invoice_view`, `default_biller`, `envato_username`, `purchase_code`, `rtl`, `each_spent`, `ca_point`, `each_sale`, `sa_point`, `update`, `sac`, `display_all_products`) VALUES
(1, 'logo2.png', 'logo3.png', 'Geeta Dresses', 'english', 1, 0, 'INR', 0, 10, '3.0.1.21', 1, 5, 'SALE', 'GD01O', 'GD1P', 'TR', 'DO', 'IPAY', 'RETURNSL', '', 1, 'default', 1, 1, 1, 1, 0, 1, 0, 1, 0, 'Asia/Kolkata', 800, 800, 60, 60, 1, 0, 0, 0, NULL, 'mail', '/usr/sbin/sendmail', 'pop.gmail.com', 'contact@tecdiary.com', 'jEFTM4T63AiQ9dsidxhPKt9CIg4HQjCN58n/RW9vmdC/UDXCzRLR469ziZ0jjpFlbOg43LyoSmpJLBkcAHh0Yw==', '25', NULL, NULL, 1, 'pallavi.01sn@gmail.com', 0, 4, 1, 0, 4, 1, 0, 0, 2, 2, '.', ',', 1, 56, '', '', 0, '1000.0000', 100, '2000.0000', 100, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sma_size`
--

CREATE TABLE `sma_size` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `section_id` int(11) DEFAULT NULL,
  `product_items_id` int(11) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `userby` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Deactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_skrill`
--

CREATE TABLE `sma_skrill` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `account_email` varchar(255) NOT NULL DEFAULT 'testaccount2@moneybookers.com',
  `secret_word` varchar(20) NOT NULL DEFAULT 'mbtest',
  `skrill_currency` varchar(3) NOT NULL DEFAULT 'USD',
  `fixed_charges` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `extra_charges_my` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `extra_charges_other` decimal(25,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_skrill`
--

INSERT INTO `sma_skrill` (`id`, `active`, `account_email`, `secret_word`, `skrill_currency`, `fixed_charges`, `extra_charges_my`, `extra_charges_other`) VALUES
(1, 1, 'testaccount2@moneybookers.com', 'mbtest', 'USD', '0.0000', '0.0000', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `sma_stock`
--

CREATE TABLE `sma_stock` (
  `id` int(5) NOT NULL,
  `code` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sma_stock`
--

INSERT INTO `sma_stock` (`id`, `code`) VALUES
(3, '20160330001'),
(12, '20160330010'),
(14, '20160330012'),
(15, '20160330013'),
(17, '20160330015'),
(21, '20160330019'),
(22, '20160330020'),
(23, '20160330021'),
(24, '20160330022'),
(25, '20160330023'),
(27, '20160330025'),
(30, '20160330028'),
(34, '20160330032'),
(35, '20160330033'),
(36, '20160330034'),
(38, '20160330036'),
(44, '20160330042'),
(53, '20160330051'),
(77, '20160330075'),
(78, '20160330076'),
(80, '20160330078'),
(81, '20160330079'),
(88, '20160330086'),
(99, '20160330097'),
(103, '20160330101'),
(115, '20160330113'),
(119, '20160330117'),
(120, '20160330118'),
(122, '20160330120'),
(123, '20160330121'),
(124, '20160330122'),
(125, '20160330123'),
(126, '20160330124'),
(127, '20160330125'),
(128, '20160330126'),
(129, '20160330127'),
(130, '20160330128'),
(131, '20160330129'),
(132, '20160330130'),
(133, '20160330131'),
(136, '20160330134'),
(137, '20160330135'),
(138, '20160330136'),
(144, '20160330142'),
(155, '20160330153'),
(156, '20160330154'),
(164, '20160330162'),
(170, '20160330168'),
(176, '20160330174'),
(180, '20160330178'),
(182, '20160330180'),
(183, '20160330181'),
(186, '20160330184'),
(189, '20160330187'),
(190, '20160330188'),
(191, '20160330189'),
(192, '20160330190'),
(202, '20160330200'),
(221, '20160330219'),
(222, '20160330220'),
(223, '20160330221'),
(224, '20160330222'),
(225, '20160330223'),
(228, '20160330226'),
(229, '20160330227'),
(231, '20160330229'),
(235, '20160330233'),
(237, '20160330235'),
(239, '20160330237'),
(268, '20160330266'),
(273, '20160330271'),
(287, '20160331001'),
(327, '20160331041'),
(328, '20160331042'),
(331, '20160331045'),
(337, '20160331051'),
(367, '20160331081'),
(368, '20160331082'),
(369, '20160331083'),
(370, '20160331084'),
(371, '20160331085'),
(372, '20160331086'),
(374, '20160331088'),
(375, '20160331089'),
(376, '20160331090'),
(377, '20160331091'),
(378, '20160331092'),
(379, '20160331093'),
(380, '20160331094'),
(381, '20160331095'),
(382, '20160331096'),
(384, '20160331098'),
(387, '20160331101'),
(389, '20160331103'),
(397, '20160331111'),
(398, '20160331112'),
(401, '20160331115'),
(402, '20160331116'),
(404, '20160331118'),
(405, '20160331119'),
(407, '20160331121'),
(414, '20160331128'),
(415, '20160331129'),
(418, '20160331132'),
(423, '20160331137'),
(431, '20160331145'),
(447, '20160331161'),
(449, '20160331163'),
(450, '20160331164'),
(454, '20160331168'),
(462, '20160331176'),
(468, '20160331182'),
(469, '20160331183'),
(470, '20160331184'),
(471, '20160331185'),
(472, '20160331186'),
(508, '20160331222'),
(537, '20160331251'),
(540, '20160331254'),
(541, '20160331255'),
(542, '20160331256'),
(543, '20160331257'),
(544, '20160331258'),
(546, '20160331260'),
(547, '20160331261'),
(549, '20160331263'),
(551, '20160331265'),
(553, '20160331267'),
(555, '20160331269'),
(557, '20160331271'),
(558, '20160331272'),
(559, '20160331273'),
(560, '20160331274'),
(561, '20160331275'),
(562, '20160331276'),
(563, '20160331277'),
(564, '20160331278'),
(597, '20160331315'),
(599, '20160331317'),
(600, '20160331318'),
(601, '20160331319'),
(602, '20160331320'),
(603, '20160331321'),
(605, '20160331323'),
(606, '20160331324'),
(643, '20160331361'),
(651, '20160331369'),
(654, '20160331372'),
(658, '20160331376'),
(663, '20160331381'),
(665, '20160331383'),
(666, '20160331384'),
(669, '20160331387'),
(670, '20160331388'),
(671, '20160331389'),
(672, '20160331390'),
(679, '20160331397'),
(681, '20160331399'),
(682, '20160331400'),
(683, '20160331401'),
(693, '20160331411'),
(694, '20160331412'),
(697, '20160331415'),
(717, '20160331435'),
(724, '20160331442'),
(729, '20160331447'),
(730, '20160331448'),
(732, '20160331450'),
(733, '20160331451'),
(734, '20160331452'),
(749, '20160331467'),
(750, '20160331468'),
(754, '20160331472'),
(755, '20160331473'),
(759, '20160331477'),
(790, '20160331508'),
(794, '20160331512'),
(795, '20160331513'),
(796, '20160331514'),
(799, '20160331517'),
(801, '20160331519'),
(802, '20160331520'),
(804, '20160331522'),
(806, '20160331524'),
(808, '20160331526'),
(812, '20160331530'),
(817, '20160331535'),
(818, '20160331536'),
(844, '20160331563'),
(845, '20160331564'),
(846, '20160331565'),
(849, '20160331568'),
(852, '20160331571'),
(853, '20160331572'),
(854, '20160331573'),
(856, '20160331575'),
(860, '20160331579'),
(865, '20160331584'),
(867, '20160331586'),
(874, '20160401001'),
(875, '20160401002'),
(876, '20160401003'),
(877, '20160401004'),
(878, '20160401005'),
(879, '20160401006'),
(882, '20160401009'),
(883, '20160401010'),
(884, '20160401011'),
(886, '20160401013'),
(887, '20160401014'),
(888, '20160401015'),
(889, '20160401016'),
(890, '20160401017'),
(891, '20160401018'),
(892, '20160401019'),
(893, '20160401020'),
(894, '20160401021'),
(898, '20160401025'),
(899, '20160401026'),
(900, '20160401027'),
(901, '20160401028'),
(902, '20160401029'),
(903, '20160401030'),
(904, '20160401031'),
(905, '20160401032'),
(907, '20160401034'),
(908, '20160401035'),
(913, '20160401040'),
(914, '20160401041'),
(916, '20160401043'),
(917, '20160401044'),
(920, '20160401047'),
(921, '20160401048'),
(925, '20160401052'),
(926, '20160401053'),
(939, '20160401066'),
(948, '20160401075'),
(949, '20160401076'),
(950, '20160401077'),
(952, '20160401079'),
(953, '20160401080'),
(954, '20160401081'),
(955, '20160401082'),
(956, '20160401083'),
(957, '20160401084'),
(958, '20160401085'),
(959, '20160401086'),
(962, '20160401089'),
(963, '20160401090'),
(964, '20160401091'),
(965, '20160401092'),
(966, '20160401093'),
(967, '20160401094'),
(969, '20160401096'),
(970, '20160401097'),
(971, '20160401098'),
(972, '20160401099'),
(973, '20160401100'),
(974, '20160401101'),
(975, '20160401102'),
(976, '20160401103'),
(978, '20160401105'),
(979, '20160401106'),
(980, '20160401107'),
(981, '20160401108'),
(984, '20160401111'),
(985, '20160401112'),
(986, '20160401113'),
(987, '20160401114'),
(988, '20160401115'),
(989, '20160401116'),
(990, '20160401117'),
(991, '20160401118'),
(992, '20160401119'),
(993, '20160401120'),
(994, '20160401121'),
(995, '20160401122'),
(997, '20160401124'),
(999, '20160401126'),
(1015, '20160401142'),
(1051, '20160401178'),
(1052, '20160401179'),
(1053, '20160401180'),
(1054, '20160401181'),
(1055, '20160401182'),
(1083, '20160401210'),
(1087, '20160401214'),
(1088, '20160401215'),
(1097, '20160401224'),
(1099, '20160401226'),
(1100, '20160401227'),
(1139, '20160401266'),
(1140, '20160401267'),
(1142, '20160401269'),
(1143, '20160401270'),
(1144, '20160401271'),
(1154, '20160401281'),
(1157, '20160401284'),
(1165, '20160401292'),
(1176, '20160401303'),
(1177, '20160401304'),
(1178, '20160401305'),
(1181, '20160401308'),
(1182, '20160401309'),
(1183, '20160401310'),
(1185, '20160401312'),
(1186, '20160401313'),
(1194, '20160401321'),
(1195, '20160401322'),
(1197, '20160401324'),
(1199, '20160401326'),
(1201, '20160401328'),
(1212, '20160401339'),
(1213, '20160401340'),
(1214, '20160401341'),
(1221, '20160401348'),
(1222, '20160401349'),
(1223, '20160401350'),
(1224, '20160401351'),
(1232, '20160401359'),
(1233, '20160401360'),
(1234, '20160401361'),
(1236, '20160401363'),
(1237, '20160401364'),
(1238, '20160401365'),
(1239, '20160401366'),
(1240, '20160401367'),
(1241, '20160401368'),
(1242, '20160401369'),
(1243, '20160401370'),
(1244, '20160401371'),
(1245, '20160401372'),
(1246, '20160401373'),
(1247, '20160401374'),
(1248, '20160401375'),
(1249, '20160401376'),
(1250, '20160401377'),
(1267, '20160401394'),
(1310, '20160401437'),
(1336, '20160402016'),
(1337, '20160402017'),
(1338, '20160402018'),
(1348, '20160402028'),
(1355, '20160402035'),
(1363, '20160402043'),
(1364, '20160402044'),
(1366, '20160402046'),
(1373, '20160402053'),
(1389, '20160402069'),
(1391, '20160402071'),
(1394, '20160402074'),
(1407, '20160402087'),
(1409, '20160402089'),
(1433, '20160402113'),
(1439, '20160402119'),
(1458, '20160402138'),
(1463, '20160402143'),
(1464, '20160402144'),
(1465, '20160402145'),
(1482, '20160402162'),
(1483, '20160402163'),
(1484, '20160402164'),
(1486, '20160402166'),
(1487, '20160402167'),
(1488, '20160402168'),
(1489, '20160402169'),
(1491, '20160402171'),
(1492, '20160402172'),
(1493, '20160402173'),
(1494, '20160402174'),
(1495, '20160402175'),
(1496, '20160402176'),
(1497, '20160402177'),
(1502, '20160402182'),
(1519, '20160402199'),
(1520, '20160402200'),
(1523, '20160402203'),
(1524, '20160402204'),
(1527, '20160402207'),
(1541, '20160402221'),
(1542, '20160402222'),
(1543, '20160402223'),
(1544, '20160402224'),
(1545, '20160402225'),
(1546, '20160402226'),
(1551, '20160402231'),
(1552, '20160402232'),
(1553, '20160402233'),
(1554, '20160402234'),
(1556, '20160402236'),
(1557, '20160402237'),
(1558, '20160402238'),
(1559, '20160402239'),
(1560, '20160402240'),
(1561, '20160402241'),
(1562, '20160402242'),
(1563, '20160402243'),
(1564, '20160402244'),
(1565, '20160402245'),
(1566, '20160402246'),
(1570, '20160402250'),
(1571, '20160402251'),
(1572, '20160402252'),
(1574, '20160402254'),
(1575, '20160402255'),
(1576, '20160402256'),
(1784, '20160407031'),
(1785, '20160407032'),
(1792, '20160407039'),
(1793, '20160407040'),
(1871, '20160407118'),
(1874, '20160407121'),
(1880, '20160407127'),
(1887, '20160407134'),
(1890, '20160407137'),
(1894, '20160407141'),
(1904, '20160407151'),
(1905, '20160407152'),
(1906, '20160407153'),
(1913, '20160407160'),
(1924, '20160407171'),
(1925, '20160407172'),
(1930, '20160407177'),
(2422, '20160403492'),
(2423, '20160403493'),
(2425, '20160403495'),
(2428, '20160403498'),
(2431, '20160403501'),
(2433, '20160403503'),
(2435, '20160403505'),
(2438, '20160403508'),
(2441, '20160403511'),
(2444, '20160403514'),
(2447, '20160403517'),
(2448, '20160403518'),
(2449, '20160403519'),
(2450, '20160403520'),
(2451, '20160403521'),
(2452, '20160403522'),
(2458, '20160403528'),
(2460, '20160403530'),
(2461, '20160403531'),
(2462, '20160403532'),
(2468, '20160403538'),
(2470, '20160403540'),
(2471, '20160403541'),
(2473, '20160403543'),
(2495, '20160403565'),
(2496, '20160403566'),
(2589, '20160403659'),
(2593, '20160403663'),
(2594, '20160403664'),
(2595, '20160403665'),
(2596, '20160403666'),
(2597, '20160403667'),
(2598, '20160403668'),
(2599, '20160403669'),
(2616, '20160403686'),
(2617, '20160403687'),
(2620, '20160403690'),
(2623, '20160403693'),
(2624, '20160403694'),
(2629, '20160403699'),
(2630, '20160403700'),
(2631, '20160403701'),
(2633, '20160403703'),
(2634, '20160403704'),
(2635, '20160403705'),
(2636, '20160403706'),
(2638, '20160403708'),
(2639, '20160403709'),
(2646, '20160403716'),
(2647, '20160403717'),
(2649, '20160403719'),
(2650, '20160403720'),
(2651, '20160403721'),
(2652, '20160403722'),
(2653, '20160403723'),
(2654, '20160403724'),
(2655, '20160403725'),
(2657, '20160403729'),
(2658, '20160403730'),
(2659, '20160403731'),
(2660, '20160403732'),
(2661, '20160403733'),
(2662, '20160403734'),
(2663, '20160403735'),
(2664, '20160403736'),
(2665, '20160403737'),
(2666, '20160403738'),
(2667, '20160403739'),
(2668, '20160403740'),
(2670, '20160403742'),
(2671, '20160403743'),
(2672, '20160403744'),
(2673, '20160403745'),
(2674, '20160403746'),
(2683, '20160403755'),
(2684, '20160403756'),
(2685, '20160403757'),
(2686, '20160403758'),
(2687, '20160403759'),
(2688, '20160403760'),
(2689, '20160403761'),
(2690, '20160403762'),
(2691, '20160403763'),
(2692, '20160403764'),
(2693, '20160403765'),
(2694, '20160403766'),
(2695, '20160403767'),
(2696, '20160403768'),
(2697, '20160403769'),
(2698, '20160403770'),
(2699, '20160403771'),
(2700, '20160403772'),
(2701, '20160403773'),
(2702, '20160403774'),
(2703, '20160403775'),
(2704, '20160403776'),
(2705, '20160403777'),
(2706, '20160403778'),
(2707, '20160403779'),
(2708, '20160403780'),
(2709, '20160403781'),
(2710, '20160403782'),
(2711, '20160403783'),
(2712, '20160403784'),
(2713, '20160403785'),
(2714, '20160403786'),
(2715, '20160403787'),
(2716, '20160403788'),
(2717, '20160403789'),
(2718, '20160403790'),
(2737, '20160403809'),
(2747, '20160403819'),
(2748, '20160403820'),
(2749, '20160403821'),
(2750, '20160403822'),
(2751, '20160403823'),
(2753, '20160403825'),
(2754, '20160403826'),
(2755, '20160403827'),
(2756, '20160403828'),
(2757, '20160403829'),
(2758, '20160403830'),
(2761, '20160403833'),
(2762, '20160403834'),
(2763, '20160403835'),
(2764, '20160403836'),
(2765, '20160403837'),
(2766, '20160403838'),
(2767, '20160403839'),
(2768, '20160403840'),
(2769, '20160403841'),
(2770, '20160403842'),
(2771, '20160403843'),
(2772, '20160403846'),
(2773, '20160403847'),
(2775, '20160403849'),
(2776, '20160403850'),
(2777, '20160403851'),
(2778, '20160403852'),
(2779, '20160403853'),
(2785, '20160403859'),
(2792, '20160403866'),
(2793, '20160403867'),
(2794, '20160403868'),
(2795, '20160403869'),
(2796, '20160403870'),
(2797, '20160403871'),
(2798, '20160403872'),
(2799, '20160403873'),
(2800, '20160403874'),
(2801, '20160403875'),
(2802, '20160403876'),
(2803, '20160403877'),
(2805, '20160403879'),
(2809, '20160403883'),
(2811, '20160403885'),
(2812, '20160403886'),
(2813, '20160403887'),
(2814, '20160403888'),
(2815, '20160403889'),
(2816, '20160403890'),
(2817, '20160403891'),
(2818, '20160403892'),
(2820, '20160403894'),
(2920, '20160403994'),
(2925, '20160403999'),
(2926, '20160404000'),
(2927, '20160404001'),
(2928, '20160404002'),
(2929, '20160404003'),
(2930, '20160404004'),
(2931, '20160404005'),
(2932, '20160404006'),
(2933, '20160404007'),
(2934, '20160404008'),
(2935, '20160404009'),
(2983, '20160404057'),
(2984, '20160404058'),
(2985, '20160404059'),
(2986, '20160404060'),
(2987, '20160404061'),
(2988, '20160404062'),
(2989, '20160404063'),
(2990, '20160404064'),
(2991, '20160404065'),
(2992, '20160404066'),
(2993, '20160404067'),
(2994, '20160404068'),
(2995, '20160404069'),
(2996, '20160404070'),
(2997, '20160404071'),
(2998, '20160404072'),
(3010, '20160404084'),
(3011, '20160404085'),
(3012, '20160404086'),
(3013, '20160404087'),
(3016, '20160404090'),
(3017, '20160404091'),
(3022, '20160404096'),
(3025, '20160404099'),
(3026, '20160404100'),
(3027, '20160404101'),
(3031, '20160404105'),
(3296, '20160404371'),
(3297, '20160404372'),
(3298, '20160404373'),
(3339, '20160404414'),
(3365, '20160405009'),
(3370, '20160405014'),
(3372, '20160405016'),
(3373, '20160405017'),
(3374, '20160405018'),
(3465, '20160405109'),
(3466, '20160405110'),
(3467, '20160405111'),
(3468, '20160405112'),
(3469, '20160405113'),
(3470, '20160405114'),
(3471, '20160405115'),
(3472, '20160405116'),
(3473, '20160405117'),
(3479, '20160405123'),
(3481, '20160405125'),
(3483, '20160405127'),
(3484, '20160405128'),
(3485, '20160405129'),
(3487, '20160405131'),
(3488, '20160405132'),
(3491, '20160405135'),
(3494, '20160405138'),
(3495, '20160405139'),
(3509, '20160405153'),
(3511, '20160405155'),
(3512, '20160405156'),
(3516, '20160405160'),
(3517, '20160405161'),
(3521, '20160405165'),
(3529, '20160405173'),
(3549, '20160405193'),
(3551, '20160405195'),
(3553, '20160405197'),
(3554, '20160405198'),
(3557, '20160405201'),
(3561, '20160405205'),
(3563, '20160405207'),
(3564, '20160405208'),
(3565, '20160405209'),
(3566, '20160405210'),
(3569, '20160405213'),
(3572, '20160405216'),
(3573, '20160405217'),
(3574, '20160405218'),
(3575, '20160405219'),
(3576, '20160405220'),
(3577, '20160405221'),
(3594, '20160406053'),
(3596, '20160406055'),
(3597, '20160406056'),
(3599, '20160406058'),
(3601, '20160406060'),
(3602, '20160406061'),
(3607, '20160406066'),
(3612, '20160406071'),
(3613, '20160406072'),
(3616, '20160406075'),
(3618, '20160406077'),
(3619, '20160406078'),
(3620, '20160406079'),
(3621, '20160406080'),
(3703, '20160406162'),
(3704, '20160406163'),
(3706, '20160406165'),
(3708, '20160406167'),
(3710, '20160406169'),
(3711, '20160406170'),
(3713, '20160406172'),
(3714, '20160406173'),
(3736, '20160406195'),
(3738, '20160406197'),
(3744, '20160406203'),
(3745, '20160406204'),
(3746, '20160406205'),
(3747, '20160406206'),
(3748, '20160406207'),
(3749, '20160406208'),
(3750, '20160406209'),
(3751, '20160406210'),
(3752, '20160406211'),
(3755, '20160406214'),
(3756, '20160406215'),
(3759, '20160406218'),
(3760, '20160406219'),
(3761, '20160406220'),
(3762, '20160406221'),
(3763, '20160406222'),
(3766, '20160406225'),
(3767, '20160406226'),
(3768, '20160406227'),
(3769, '20160406228'),
(3770, '20160406229'),
(3771, '20160406230'),
(3772, '20160406231'),
(3773, '20160406232'),
(3774, '20160406233'),
(3775, '20160406234'),
(3776, '20160406235'),
(3778, '20160406237'),
(3781, '20160406240'),
(3782, '20160406241'),
(3783, '20160406242'),
(3785, '20160406244'),
(3788, '20160406247'),
(3789, '20160406248'),
(3790, '20160406249'),
(3791, '20160406250'),
(3818, '20160406277'),
(3819, '20160406278'),
(3820, '20160406279'),
(3821, '20160406280'),
(3823, '20160406282'),
(4198, '20160408373'),
(4199, '20160408374'),
(4200, '20160408375'),
(4201, '20160408376'),
(4212, '20160408387'),
(4213, '20160408388'),
(4216, '20160408391'),
(4222, '20160408397'),
(4226, '20160408401'),
(4229, '20160408404'),
(4230, '20160408405'),
(4271, '20160408446'),
(4273, '20160408448'),
(4275, '20160408450'),
(4309, '20160408484'),
(4310, '20160408485'),
(4311, '20160408486'),
(4314, '20160408489'),
(4328, '20160408503'),
(4331, '20160408506'),
(4332, '20160408507'),
(4333, '20160408508'),
(4335, '20160408510'),
(4337, '20160408512'),
(5140, '20160409005'),
(5154, '20160409019'),
(5156, '20160409021'),
(5158, '20160409023'),
(5162, '20160409027'),
(5164, '20160409029'),
(5165, '20160409030'),
(5169, '20160409034'),
(5182, '20160409047'),
(5184, '20160409049'),
(5187, '20160409052'),
(5189, '20160409054'),
(5190, '20160409055'),
(5194, '20160409059'),
(5198, '20160409063'),
(5230, '20160412004'),
(5231, '20160412005'),
(5233, '20160412007'),
(5234, '20160412008'),
(5270, '20160412044'),
(5271, '20160412045'),
(5341, '20160412115'),
(5343, '20160412117'),
(5345, '20160412119'),
(5435, '20160412209'),
(5437, '20160412211'),
(5444, '20160412218'),
(5449, '20160412223'),
(5451, '20160412225'),
(5463, '20160412237'),
(5520, '20160413004'),
(5522, '20160413006'),
(5529, '20160413013'),
(5530, '20160413014'),
(5532, '20160413016'),
(5533, '20160413017'),
(5535, '20160413019'),
(5537, '20160413021'),
(5539, '20160413023'),
(5541, '20160413025'),
(5542, '20160413026'),
(5544, '20160413028'),
(5546, '20160413030'),
(5547, '20160413031'),
(5548, '20160413032'),
(5549, '20160413033'),
(5550, '20160413034'),
(5557, '20160413041'),
(5563, '20160413047'),
(5567, '20160413051'),
(5569, '20160413053'),
(5574, '20160413058'),
(5919, '20160414041'),
(5922, '20160414044'),
(5923, '20160414045'),
(5924, '20160414046'),
(5928, '20160414050'),
(5929, '20160414051'),
(5932, '20160414054'),
(5933, '20160414055'),
(5977, '20160416019'),
(6055, '20160416097'),
(6056, '20160416098'),
(6057, '20160416099'),
(6058, '20160416100'),
(6059, '20160416101'),
(6061, '20160416103'),
(6062, '20160416104'),
(6063, '20160416105'),
(6064, '20160416106'),
(6065, '20160416107'),
(6066, '20160416108'),
(6067, '20160416109'),
(6068, '20160416110'),
(6069, '20160416111'),
(6070, '20160416112'),
(6071, '20160416113'),
(6072, '20160416114'),
(6074, '20160416116'),
(6076, '20160416118'),
(6077, '20160416119'),
(6078, '20160416120'),
(6079, '20160416121'),
(6080, '20160416122'),
(6082, '20160416124'),
(6083, '20160416125'),
(6086, '20160416128'),
(6089, '20160416131'),
(6091, '20160416133'),
(6092, '20160416134'),
(6093, '20160416135'),
(6094, '20160416136'),
(6095, '20160416137'),
(6097, '20160416139'),
(6100, '20160416142'),
(6101, '20160416143'),
(6102, '20160416144'),
(6104, '20160416146'),
(6105, '20160416147'),
(6106, '20160416148'),
(6107, '20160416149'),
(6108, '20160416150'),
(6109, '20160416151'),
(6110, '20160416152'),
(6111, '20160416153'),
(6113, '20160416155'),
(6114, '20160416156'),
(6127, '20160416169'),
(6128, '20160416170'),
(6129, '20160416171'),
(6130, '20160416172'),
(6131, '20160416173'),
(6132, '20160416174'),
(6133, '20160416175'),
(6135, '20160416177'),
(6136, '20160416178'),
(6137, '20160416179'),
(6138, '20160416180'),
(6151, '20160416193'),
(6160, '20160416202'),
(6163, '20160416205'),
(6165, '20160416207'),
(6167, '20160416209'),
(6168, '20160416210'),
(6169, '20160416211'),
(6170, '20160416212'),
(6171, '20160416213'),
(6172, '20160416214'),
(6174, '20160416216'),
(6175, '20160416217'),
(6176, '20160416218'),
(6178, '20160416220'),
(6179, '20160416221'),
(6180, '20160416222'),
(6181, '20160416223'),
(6182, '20160416224'),
(6183, '20160416225'),
(6184, '20160416226'),
(6185, '20160416227'),
(6186, '20160416228'),
(6188, '20160416230'),
(6189, '20160416231'),
(6190, '20160416232'),
(6191, '20160416233'),
(6192, '20160416234'),
(6193, '20160416235'),
(6194, '20160416236'),
(6195, '20160416237'),
(6196, '20160416238'),
(6197, '20160416239'),
(6198, '20160416240'),
(7915, '20160430048'),
(7916, '20160430049'),
(7919, '20160430052'),
(7937, '20160430070'),
(8417, '20160504032'),
(8435, '20160504050'),
(8436, '20160504051'),
(8437, '20160504052'),
(8438, '20160504053'),
(8439, '20160504054'),
(8440, '20160504055'),
(8441, '20160504056'),
(8442, '20160504057'),
(8443, '20160504058'),
(8444, '20160504059'),
(8445, '20160504060'),
(8446, '20160504061'),
(8448, '20160504063'),
(8450, '20160504065'),
(8451, '20160504066'),
(8452, '20160504067'),
(8453, '20160504068'),
(8454, '20160504069'),
(8456, '20160504071'),
(8458, '20160504073'),
(8488, '20160504103'),
(8489, '20160504104'),
(8490, '20160504105'),
(8491, '20160504106'),
(8492, '20160504107'),
(8493, '20160504108'),
(8495, '20160504110'),
(8497, '20160504112'),
(8592, '20160512019'),
(8593, '20160512020'),
(8597, '20160512024'),
(8682, '20160516008'),
(8700, '20160516026'),
(8703, '20160516029'),
(8706, '20160516032'),
(8708, '20160516034'),
(8710, '20160516036'),
(8714, '20160516040'),
(8716, '20160516042'),
(8720, '20160516046'),
(8721, '20160516047'),
(9059, '20160524197'),
(9060, '20160524198'),
(9061, '20160524199'),
(9062, '20160524200'),
(9063, '20160524201'),
(9064, '20160524202'),
(9065, '20160524203'),
(9066, '20160524204'),
(9067, '20160524205'),
(9068, '20160524206'),
(9070, '20160524208'),
(9071, '20160524209'),
(9072, '20160524210'),
(9073, '20160524211'),
(9074, '20160524212'),
(9075, '20160524213'),
(9076, '20160524214'),
(9077, '20160524215'),
(9079, '20160524217'),
(9080, '20160524218'),
(9081, '20160524219'),
(9082, '20160524220'),
(9083, '20160524221'),
(9084, '20160524222'),
(9085, '20160524223'),
(9086, '20160524224'),
(9087, '20160524225'),
(9088, '20160524226'),
(9089, '20160524227'),
(9090, '20160524228'),
(9091, '20160524229'),
(9092, '20160524230'),
(9093, '20160524231'),
(9094, '20160524232'),
(9095, '20160524233'),
(9096, '20160524234'),
(9100, '20160524238'),
(9101, '20160524239'),
(9103, '20160524241'),
(9106, '20160524244'),
(9122, '20160524260'),
(9123, '20160524261'),
(9124, '20160524262'),
(9125, '20160524263'),
(9126, '20160524264'),
(9127, '20160524265'),
(9737, '20160527033'),
(9738, '20160527034'),
(9739, '20160527035'),
(9740, '20160527036'),
(9741, '20160527037'),
(9742, '20160527038'),
(9745, '20160527041'),
(9747, '20160527043'),
(9748, '20160527044'),
(9749, '20160527045'),
(9750, '20160527046'),
(9751, '20160527047'),
(9752, '20160527048'),
(9753, '20160527049'),
(9755, '20160527051'),
(9756, '20160527052'),
(9757, '20160527053'),
(9758, '20160527054'),
(9763, '20160527059'),
(9766, '20160527062'),
(9767, '20160527063'),
(9768, '20160527064'),
(9772, '20160527068'),
(10039, '20160528156'),
(10040, '20160528157'),
(10045, '20160528162'),
(10046, '20160528163'),
(10047, '20160528164'),
(10048, '20160528165'),
(10049, '20160528166'),
(10050, '20160528167'),
(10051, '20160528168'),
(10052, '20160528169'),
(10053, '20160528170'),
(10054, '20160528171'),
(10055, '20160528172'),
(10056, '20160528173'),
(10057, '20160528174'),
(10058, '20160528175'),
(10059, '20160528176'),
(10060, '20160528177'),
(10061, '20160528178'),
(10062, '20160528179'),
(10063, '20160528180'),
(10064, '20160528181'),
(10066, '20160528183'),
(10067, '20160528184'),
(10068, '20160528185'),
(10069, '20160528186'),
(10070, '20160528187'),
(10071, '20160528188'),
(10072, '20160528189'),
(10077, '20160528194'),
(10078, '20160528195'),
(10079, '20160528196'),
(10080, '20160528197'),
(10081, '20160528198'),
(10082, '20160528199'),
(10083, '20160528200'),
(10084, '20160528201'),
(10085, '20160528202'),
(10086, '20160528203'),
(10087, '20160528204'),
(10088, '20160528205'),
(10089, '20160528206'),
(10090, '20160528207'),
(10091, '20160528208'),
(10092, '20160528209'),
(10093, '20160528210'),
(10094, '20160528211'),
(10095, '20160528212'),
(10096, '20160528213'),
(10097, '20160528214'),
(10098, '20160528215'),
(10099, '20160528216'),
(10100, '20160528217'),
(10101, '20160528218'),
(10102, '20160528219'),
(10103, '20160528220'),
(10104, '20160528221'),
(10105, '20160528222'),
(10106, '20160528223'),
(10107, '20160528224'),
(10108, '20160528225'),
(10110, '20160528227'),
(10112, '20160528229'),
(10113, '20160528230'),
(10114, '20160528231'),
(10115, '20160528232'),
(10116, '20160528233'),
(10117, '20160528234'),
(10118, '20160528235'),
(10119, '20160528236'),
(10120, '20160528237'),
(10121, '20160528238'),
(10124, '20160528241'),
(10125, '20160528242'),
(10126, '20160528243'),
(10127, '20160528244'),
(10128, '20160528245'),
(10129, '20160528246'),
(10130, '20160528247'),
(10229, '20160531004'),
(10230, '20160531005'),
(10231, '20160531006'),
(10232, '20160531007'),
(10233, '20160531008'),
(10234, '20160531009'),
(10235, '20160531010'),
(10237, '20160531012'),
(10238, '20160531013'),
(10240, '20160531015'),
(10241, '20160531016'),
(10242, '20160531017'),
(10243, '20160531018'),
(10244, '20160531019'),
(10245, '20160531020'),
(10246, '20160531021'),
(10247, '20160531022'),
(10249, '20160531024'),
(10250, '20160531025'),
(10252, '20160531027'),
(10254, '20160531029'),
(10255, '20160531030'),
(10256, '20160531031'),
(10257, '20160531032'),
(10258, '20160531033'),
(10259, '20160531034'),
(10260, '20160531035'),
(10261, '20160531036'),
(10262, '20160531037'),
(10263, '20160531038'),
(10264, '20160531039'),
(10265, '20160531040'),
(10266, '20160531041'),
(10267, '20160531042'),
(10268, '20160531043'),
(10270, '20160531045'),
(10271, '20160531046'),
(10272, '20160531047'),
(10274, '20160531049'),
(10276, '20160531051'),
(10277, '20160531052'),
(10278, '20160531053'),
(10279, '20160531054'),
(10280, '20160531055'),
(10281, '20160531056'),
(10282, '20160531057'),
(10283, '20160531058'),
(10284, '20160531059'),
(10347, '20160601059'),
(10348, '20160601060'),
(10349, '20160601061'),
(10350, '20160601062'),
(10351, '20160601063'),
(10352, '20160601064'),
(10353, '20160601065'),
(10358, '20160601070'),
(10359, '20160601071'),
(10360, '20160601072'),
(10361, '20160601073'),
(10362, '20160601074'),
(10363, '20160601075'),
(10364, '20160601076'),
(10365, '20160601077'),
(10366, '20160601078'),
(10367, '20160601079'),
(10368, '20160601080'),
(10369, '20160601081'),
(10370, '20160601082'),
(10371, '20160601083'),
(10372, '20160601084'),
(10373, '20160601085'),
(10374, '20160601086'),
(10375, '20160601087'),
(10376, '20160601088'),
(10378, '20160601090'),
(10379, '20160601091'),
(10380, '20160601092'),
(10381, '20160601093'),
(10382, '20160601094'),
(10383, '20160601095'),
(10384, '20160601096'),
(10385, '20160601097'),
(10386, '20160601098'),
(10387, '20160601099'),
(10388, '20160601100'),
(10389, '20160601101'),
(10390, '20160601102'),
(10391, '20160601103'),
(10392, '20160601104'),
(10393, '20160601105'),
(10394, '20160601106'),
(10401, '20160601113'),
(10404, '20160601116'),
(10405, '20160601117'),
(10406, '20160601118'),
(10407, '20160601119'),
(10408, '20160601120'),
(10409, '20160601121'),
(10410, '20160601122'),
(10411, '20160601123'),
(10414, '20160601125'),
(10420, '20160601131'),
(10421, '20160601132'),
(10422, '20160601133'),
(10424, '20160601135'),
(10425, '20160601136'),
(10426, '20160601137'),
(10427, '20160601138'),
(10428, '20160601139'),
(10429, '20160601140'),
(10430, '20160601141'),
(10431, '20160601142'),
(10432, '20160601143'),
(10433, '20160601144'),
(10434, '20160601145'),
(10435, '20160601146'),
(10436, '20160601147'),
(10437, '20160601148'),
(10438, '20160601149'),
(10439, '20160601150'),
(10440, '20160601151'),
(10441, '20160601152'),
(10442, '20160601153'),
(10473, '20160601182'),
(10474, '20160601183'),
(10475, '20160601184'),
(10476, '20160601185'),
(10477, '20160601186'),
(10478, '20160601187'),
(10479, '20160601188'),
(10480, '20160601189'),
(10481, '20160601190'),
(10482, '20160601191'),
(10483, '20160601192'),
(10484, '20160601193'),
(10485, '20160601194'),
(10535, '20160601243'),
(12499, '20160604633'),
(12500, '20160604634'),
(12629, '20160604756'),
(12754, '20160606001'),
(12755, '20160606002'),
(12756, '20160606003'),
(12757, '20160606004'),
(12758, '20160606005'),
(12759, '20160606006'),
(12760, '20160606007'),
(12761, '20160606008'),
(12762, '20160606009'),
(12763, '20160606010'),
(12764, '20160606011'),
(12765, '20160606012'),
(12766, '20160606013'),
(12767, '20160606014'),
(12768, '20160606015'),
(12769, '20160606016'),
(12770, '20160606017'),
(12771, '20160606018'),
(12772, '20160606019'),
(12773, '20160606020'),
(12774, '20160606021'),
(12775, '20160606022'),
(12776, '20160606023'),
(12777, '20160606024'),
(12778, '20160606025'),
(12779, '20160606026'),
(12780, '20160606027'),
(12781, '20160606028'),
(12782, '20160606029'),
(12783, '20160606030'),
(12784, '20160606031'),
(12785, '20160606032'),
(12786, '20160606033'),
(12787, '20160606034'),
(12788, '20160606035'),
(12789, '20160606036'),
(12790, '20160606037'),
(12791, '20160606038'),
(12792, '20160606039'),
(12793, '20160606040'),
(12794, '20160606041'),
(12795, '20160606042'),
(12796, '20160606043'),
(12797, '20160606044'),
(12798, '20160606045'),
(12799, '20160606046'),
(12800, '20160606047'),
(12801, '20160606048'),
(12802, '20160606049'),
(12803, '20160606050'),
(12804, '20160606051'),
(12805, '20160606052'),
(12806, '20160606053'),
(12807, '20160606054'),
(12808, '20160606055'),
(12809, '20160606056'),
(12810, '20160606057'),
(12811, '20160606058'),
(12812, '20160606059'),
(12813, '20160606060'),
(12814, '20160606061'),
(12815, '20160606062'),
(12816, '20160606063'),
(12817, '20160606064'),
(12818, '20160606065'),
(12819, '20160606066'),
(12820, '20160606067'),
(12821, '20160606068'),
(12822, '20160606069'),
(12823, '20160606070'),
(12824, '20160606071'),
(12825, '20160606072'),
(12826, '20160606073'),
(12827, '20160606074'),
(12828, '20160606075'),
(12829, '20160606076'),
(12830, '20160606077'),
(12834, '20160606079'),
(12835, '20160606080'),
(12836, '20160606081'),
(12837, '20160606082'),
(12838, '20160606083'),
(12839, '20160606084'),
(12840, '20160606085'),
(12841, '20160606086'),
(12842, '20160606087'),
(12843, '20160606088'),
(12844, '20160606089'),
(12845, '20160606090'),
(12846, '20160606091'),
(12847, '20160606092'),
(12848, '20160606093'),
(12849, '20160606094'),
(12850, '20160606095'),
(12851, '20160606096'),
(12852, '20160606097'),
(12853, '20160606098'),
(12854, '20160606099'),
(12855, '20160606100'),
(12856, '20160606101'),
(12857, '20160606102'),
(12860, '20160606105'),
(12862, '20160606107'),
(12863, '20160606108'),
(12864, '20160606109'),
(12865, '20160606110'),
(12866, '20160606111'),
(12867, '20160606112'),
(12868, '20160606113'),
(12869, '20160606114'),
(12870, '20160606115'),
(12871, '20160606116'),
(12872, '20160606117'),
(12873, '20160606118'),
(12874, '20160606119'),
(12875, '20160606120'),
(12876, '20160606121'),
(12877, '20160606122'),
(12878, '20160606123'),
(12879, '20160606124'),
(12880, '20160606125'),
(12881, '20160606126'),
(12882, '20160606127'),
(12883, '20160606128'),
(12884, '20160606129'),
(12885, '20160606130'),
(12886, '20160606131'),
(12887, '20160606132'),
(12888, '20160606133'),
(12889, '20160606134'),
(12890, '20160606135'),
(12891, '20160606136'),
(12892, '20160606137'),
(12893, '20160606138'),
(12894, '20160606139'),
(12895, '20160606140'),
(12896, '20160606141'),
(12897, '20160606142'),
(12898, '20160606143'),
(12899, '20160606144'),
(12900, '20160606145'),
(12901, '20160606146'),
(12902, '20160606147'),
(12903, '20160606148'),
(12904, '20160606149'),
(12905, '20160606150'),
(12906, '20160606151'),
(12907, '20160606152'),
(12908, '20160606153'),
(12909, '20160606154'),
(12910, '20160606155'),
(12911, '20160606156'),
(12912, '20160606157'),
(12913, '20160606158'),
(12914, '20160606159'),
(12915, '20160606160'),
(12916, '20160606161'),
(12917, '20160606162'),
(12918, '20160606163'),
(12919, '20160606164'),
(12920, '20160606165'),
(12921, '20160606166'),
(12922, '20160606167'),
(12923, '20160606168'),
(12924, '20160606169'),
(12925, '20160606170'),
(12926, '20160606171'),
(12927, '20160606172'),
(12928, '20160606173'),
(12929, '20160606174'),
(12930, '20160606175'),
(12931, '20160606176'),
(12932, '20160606177'),
(12933, '20160606178'),
(12934, '20160606179'),
(12935, '20160606180'),
(12936, '20160606181'),
(12937, '20160606182'),
(12938, '20160606183'),
(12939, '20160606184'),
(12940, '20160606185'),
(12943, '20160606188'),
(12944, '20160606189'),
(12945, '20160606190'),
(12946, '20160606191'),
(12947, '20160606192'),
(12948, '20160606193'),
(12949, '20160606194'),
(12950, '20160606195'),
(12951, '20160606196'),
(12952, '20160606197'),
(12953, '20160606198'),
(12954, '20160606199'),
(12955, '20160606200'),
(12956, '20160606201'),
(12957, '20160606202'),
(12959, '20160606204'),
(12960, '20160606205'),
(12961, '20160606206'),
(12962, '20160606207'),
(12963, '20160606208'),
(12964, '20160606209'),
(12965, '20160606210'),
(12966, '20160606211'),
(12967, '20160606212'),
(12968, '20160606213'),
(12969, '20160606214'),
(12970, '20160606215'),
(12971, '20160606216'),
(12972, '20160606217'),
(12973, '20160606218'),
(12974, '20160606219'),
(12975, '20160606220'),
(12976, '20160606221'),
(12977, '20160606222'),
(12978, '20160606223'),
(12979, '20160606224'),
(12980, '20160606225'),
(12981, '20160606226'),
(12982, '20160606227'),
(12983, '20160606228'),
(12984, '20160606229'),
(12985, '20160606230'),
(12986, '20160606231'),
(12987, '20160606232'),
(12988, '20160606233'),
(12989, '20160606234'),
(12990, '20160606235'),
(12991, '20160606236'),
(12992, '20160606237'),
(12993, '20160606238'),
(12994, '20160606239'),
(12995, '20160606240'),
(12996, '20160606241'),
(12997, '20160606242'),
(12998, '20160606243'),
(12999, '20160606244'),
(13000, '20160606245'),
(13001, '20160606246'),
(13002, '20160606247'),
(13003, '20160606248'),
(13004, '20160606249'),
(13005, '20160606250'),
(13006, '20160606251'),
(13007, '20160606252'),
(13008, '20160606253'),
(13009, '20160606254'),
(13010, '20160606255'),
(13011, '20160606256'),
(13012, '20160606257'),
(13013, '20160606258'),
(13014, '20160606259'),
(13015, '20160606260'),
(13016, '20160606261'),
(13017, '20160606262'),
(13018, '20160606263'),
(13019, '20160606264'),
(13020, '20160606265'),
(13021, '20160606266'),
(13022, '20160606267'),
(13023, '20160606268'),
(13024, '20160606269'),
(13025, '20160606270'),
(13026, '20160606271'),
(13027, '20160606272'),
(13028, '20160606273'),
(13030, '20160606274'),
(13031, '20160606275'),
(13032, '20160606276'),
(13033, '20160606277'),
(13034, '20160606278'),
(13035, '20160606279'),
(13036, '20160606280'),
(13037, '20160606281'),
(13038, '20160606282'),
(13039, '20160606283'),
(13040, '20160606284'),
(13041, '20160606285'),
(13043, '20160606287'),
(13044, '20160606288'),
(13045, '20160606289'),
(13046, '20160606290'),
(13047, '20160606291'),
(13048, '20160606292'),
(13050, '20160606294'),
(13052, '20160606295'),
(13054, '20160606297'),
(13055, '20160606298'),
(13056, '20160606299'),
(13057, '20160606300'),
(13060, '20160606301'),
(13061, '20160606302'),
(13062, '20160606303'),
(13063, '20160606304'),
(13064, '20160606305'),
(13065, '20160606306'),
(13066, '20160606307'),
(13067, '20160606308'),
(13068, '20160606309'),
(13069, '20160606310'),
(13071, '20160606311'),
(13073, '20160606312'),
(13076, '20160606313'),
(13077, '20160606314'),
(13078, '20160606315'),
(13079, '20160606316'),
(13080, '20160606317'),
(13081, '20160606318'),
(13083, '20160606319'),
(13086, '20160606320'),
(13087, '20160606321'),
(13088, '20160606322'),
(13090, '20160606323'),
(13092, '20160606324'),
(13093, '20160606325'),
(13095, '20160606326'),
(13098, '20160606327'),
(13100, '20160606328'),
(13101, '20160606329'),
(13102, '20160606330'),
(13103, '20160606331'),
(13105, '20160606332'),
(13106, '20160606333'),
(13108, '20160606334'),
(13109, '20160606335'),
(13110, '20160606336'),
(13111, '20160606337'),
(13112, '20160606338'),
(13113, '20160606339'),
(13115, '20160606340'),
(13117, '20160606341'),
(13119, '20160606342'),
(13120, '20160606343'),
(13121, '20160606344'),
(13122, '20160606345'),
(13123, '20160606346'),
(13124, '20160606347'),
(13125, '20160606348'),
(13126, '20160606349'),
(13129, '20160606350'),
(13130, '20160606351'),
(13132, '20160606352'),
(13134, '20160606353'),
(13135, '20160606354'),
(13136, '20160606355'),
(13138, '20160606356'),
(13139, '20160606357'),
(13141, '20160606358'),
(13142, '20160606359'),
(13143, '20160606360'),
(13144, '20160606361'),
(13145, '20160606362'),
(13146, '20160606363'),
(13147, '20160606364'),
(13148, '20160606365'),
(13149, '20160606366'),
(13151, '20160606367'),
(13152, '20160606368'),
(13153, '20160606369'),
(13154, '20160606370'),
(13155, '20160606371'),
(13156, '20160606372'),
(13157, '20160606373'),
(13158, '20160606374'),
(13159, '20160606375'),
(13160, '20160606376'),
(13161, '20160606377'),
(13162, '20160606378'),
(13163, '20160606379'),
(13165, '20160606381'),
(13166, '20160606382'),
(13167, '20160606383'),
(13168, '20160606384'),
(13169, '20160606385'),
(13170, '20160606386'),
(13171, '20160606387'),
(13173, '20160606389'),
(13174, '20160606390'),
(13182, '20160606398'),
(13190, '20160606406'),
(13198, '20160606414'),
(13230, '20160606445'),
(13241, '20160606456'),
(13242, '20160606457'),
(13243, '20160606458'),
(13244, '20160606459'),
(13245, '20160606460'),
(13246, '20160606461'),
(13247, '20160606462'),
(13248, '20160606463'),
(13249, '20160606464'),
(13250, '20160606465'),
(13251, '20160606466'),
(13252, '20160606467'),
(13254, '20160606469'),
(13255, '20160606470'),
(13256, '20160606471'),
(13257, '20160606472'),
(13278, '20160606492'),
(13279, '20160606493'),
(13280, '20160606494'),
(13281, '20160606495'),
(13284, '20160606498'),
(13285, '20160606499'),
(13286, '20160606500'),
(13287, '20160606501'),
(13288, '20160606502'),
(13289, '20160606503'),
(13291, '20160606505'),
(13292, '20160606506'),
(13294, '20160606507'),
(13295, '20160606508'),
(13296, '20160606509'),
(13301, '20160606514'),
(13303, '20160606515'),
(13304, '20160606516'),
(13306, '20160606517'),
(13308, '20160606518'),
(13335, '20160606545'),
(13336, '20160606546'),
(13337, '20160606547'),
(13338, '20160606548'),
(13339, '20160606549'),
(13345, '20160607001'),
(13346, '20160607002'),
(13347, '20160607003'),
(13348, '20160607004'),
(13349, '20160607005'),
(13350, '20160607006'),
(13351, '20160607007'),
(13352, '20160607008'),
(13353, '20160607009'),
(13354, '20160607010'),
(13355, '20160607011'),
(13356, '20160607012'),
(13357, '20160607013'),
(13358, '20160607014'),
(13359, '20160607015'),
(13360, '20160607016'),
(13361, '20160607017'),
(13362, '20160607018'),
(13363, '20160607019'),
(13364, '20160607020'),
(13365, '20160607021'),
(13366, '20160607022'),
(13367, '20160607023'),
(13368, '20160607024'),
(13369, '20160607025'),
(13370, '20160607026'),
(13371, '20160607027'),
(13373, '20160607028'),
(13374, '20160607029'),
(13375, '20160607030'),
(13376, '20160607031'),
(13377, '20160607032'),
(13378, '20160607033'),
(13379, '20160607034'),
(13380, '20160607035'),
(13381, '20160607036'),
(13383, '20160607037'),
(13384, '20160607038'),
(13385, '20160607039'),
(13386, '20160607040'),
(13387, '20160607041'),
(13388, '20160607042'),
(13389, '20160607043'),
(13390, '20160607044'),
(13391, '20160607045'),
(13392, '20160607046'),
(13393, '20160607047'),
(13395, '20160607048'),
(13396, '20160607049'),
(13397, '20160607050'),
(13399, '20160607052'),
(13400, '20160607053'),
(13409, '20160607062'),
(13410, '20160607063'),
(13411, '20160607064'),
(13412, '20160607065'),
(13413, '20160607066'),
(13414, '20160607067'),
(13415, '20160607068'),
(13416, '20160607069'),
(13417, '20160607070'),
(13418, '20160607071'),
(13420, '20160607072'),
(13421, '20160607073'),
(13422, '20160607074'),
(13423, '20160607075'),
(13424, '20160607076'),
(13425, '20160607077'),
(13426, '20160607078'),
(13427, '20160607079'),
(13465, '20160607115'),
(13466, '20160607116'),
(13467, '20160607117'),
(13468, '20160607118'),
(13469, '20160607119'),
(13470, '20160607120'),
(13471, '20160607121'),
(13472, '20160607122'),
(13473, '20160607123'),
(13474, '20160607124'),
(13475, '20160607125'),
(13476, '20160607126'),
(13477, '20160607127'),
(13478, '20160607128'),
(13479, '20160607129'),
(13480, '20160607130'),
(13481, '20160607131'),
(13485, '20160607135'),
(13486, '20160607136'),
(13487, '20160607137'),
(13488, '20160607138'),
(13489, '20160607139'),
(13490, '20160607140'),
(13491, '20160607141'),
(13492, '20160607142'),
(13493, '20160607143'),
(13494, '20160607144'),
(13495, '20160607145'),
(13496, '20160607146'),
(13497, '20160607147'),
(13498, '20160607148'),
(13499, '20160607149'),
(13504, '20160607154'),
(13505, '20160607155'),
(13506, '20160607156'),
(13507, '20160607157'),
(13509, '20160607159'),
(13510, '20160607160'),
(13511, '20160607161'),
(13512, '20160607162'),
(13513, '20160607163'),
(13514, '20160607164'),
(13515, '20160607165'),
(13516, '20160607166'),
(13517, '20160607167'),
(13518, '20160607168'),
(13519, '20160607169'),
(13520, '20160607170'),
(13521, '20160607171'),
(13522, '20160607172'),
(13524, '20160607174'),
(13525, '20160607175'),
(13526, '20160607176'),
(13527, '20160607177'),
(13528, '20160607178'),
(13529, '20160607179'),
(13530, '20160607180'),
(13531, '20160607181'),
(13532, '20160607182'),
(13533, '20160607183'),
(13534, '20160607184'),
(13699, '20160607347'),
(13712, '20160607360'),
(13713, '20160607361'),
(13714, '20160607362'),
(13715, '20160607363'),
(13717, '20160607364'),
(13718, '20160607365'),
(13719, '20160607366'),
(13749, '20160607395'),
(13754, '20160607400'),
(13761, '20160607407'),
(13768, '20160607414'),
(13769, '20160607415'),
(13776, '20160607422'),
(13783, '20160607429'),
(13791, '20160607436'),
(13798, '20160607443'),
(13895, '20160608001'),
(13896, '20160608002'),
(13897, '20160608003'),
(13898, '20160608004'),
(13899, '20160608005'),
(13900, '20160608006'),
(13901, '20160608007'),
(13902, '20160608008'),
(13903, '20160608009'),
(13904, '20160608010'),
(13905, '20160608011'),
(13906, '20160608012'),
(13907, '20160608013'),
(13908, '20160608014'),
(13909, '20160608015'),
(13910, '20160608016'),
(13911, '20160608017'),
(13912, '20160608018'),
(13913, '20160608019'),
(13914, '20160608020'),
(13915, '20160608021'),
(13916, '20160608022'),
(13917, '20160608023'),
(13918, '20160608024'),
(13919, '20160608025'),
(13920, '20160608026'),
(13921, '20160608027'),
(13922, '20160608028'),
(13923, '20160608029'),
(13924, '20160608030'),
(13925, '20160608031'),
(13927, '20160608032'),
(13928, '20160608033'),
(13929, '20160608034'),
(13930, '20160608035'),
(13931, '20160608036'),
(13932, '20160608037'),
(13933, '20160608038'),
(13934, '20160608039'),
(13935, '20160608040'),
(13936, '20160608041'),
(13937, '20160608042'),
(13938, '20160608043'),
(13939, '20160608044'),
(13940, '20160608045'),
(13941, '20160608046'),
(13942, '20160608047'),
(13943, '20160608048'),
(13944, '20160608049'),
(13945, '20160608050'),
(13946, '20160608051'),
(13947, '20160608052'),
(13948, '20160608053'),
(13949, '20160608054'),
(13950, '20160608055'),
(13990, '20160608095'),
(13991, '20160608096'),
(13992, '20160608097'),
(13993, '20160608098'),
(13994, '20160608099'),
(13995, '20160608100'),
(13996, '20160608101'),
(13997, '20160608102'),
(13998, '20160608103'),
(13999, '20160608104'),
(14000, '20160608105'),
(14001, '20160608106'),
(14004, '20160608109'),
(14016, '20160608120'),
(14017, '20160608121'),
(14018, '20160608122'),
(14019, '20160608123'),
(14021, '20160608125'),
(14022, '20160608126'),
(14023, '20160608127'),
(14025, '20160608129'),
(14077, '20160608181'),
(14078, '20160608182'),
(14079, '20160608183'),
(14080, '20160608184'),
(14106, '20160608210'),
(14107, '20160608211'),
(14108, '20160608212'),
(14109, '20160608213'),
(14110, '20160608214'),
(14111, '20160608215'),
(14112, '20160608216'),
(14113, '20160608217'),
(14114, '20160608218'),
(14115, '20160608219'),
(14116, '20160608220'),
(14117, '20160608221'),
(14118, '20160608222'),
(14353, '20160608457'),
(14354, '20160608458'),
(14359, '20160608463'),
(14376, '20160608480'),
(14377, '20160608481'),
(14378, '20160608482'),
(14379, '20160608483'),
(14386, '20160608490'),
(14387, '20160608491'),
(14396, '20160608500'),
(14397, '20160608501'),
(14398, '20160608502'),
(14405, '20160608509'),
(14408, '20160608512'),
(14411, '20160608515'),
(14418, '20160608522'),
(14419, '20160608523'),
(14434, '20160608538'),
(14445, '20160608549'),
(14446, '20160608550'),
(14449, '20160608553'),
(14456, '20160608560'),
(14527, '20160608630'),
(14528, '20160608631'),
(14529, '20160608632'),
(14530, '20160608633'),
(14531, '20160608634'),
(14532, '20160608635'),
(14533, '20160608636'),
(14534, '20160608637'),
(14535, '20160608638'),
(14536, '20160608639'),
(14537, '20160608640'),
(14540, '20160608641'),
(14541, '20160608642'),
(14542, '20160608643'),
(14543, '20160608644'),
(14544, '20160608645'),
(14648, '20160608743'),
(14649, '20160608744'),
(14650, '20160608745'),
(14651, '20160608746'),
(14653, '20160608747'),
(14654, '20160608748'),
(14655, '20160608749'),
(14656, '20160608750'),
(14657, '20160608751'),
(14658, '20160608752'),
(14659, '20160608753'),
(14660, '20160608754'),
(14663, '20160608757'),
(14664, '20160608758'),
(14665, '20160608759'),
(14666, '20160608760'),
(14667, '20160608761'),
(14668, '20160608762'),
(14669, '20160608763'),
(14670, '20160608764'),
(14671, '20160608765'),
(14672, '20160608766'),
(14673, '20160608767'),
(14674, '20160608768'),
(14675, '20160608769'),
(14676, '20160608770'),
(14677, '20160608771'),
(14678, '20160608772'),
(14679, '20160608773'),
(14680, '20160608774'),
(14681, '20160608775'),
(14682, '20160608776'),
(14683, '20160608777'),
(14684, '20160608778'),
(14685, '20160608779'),
(14686, '20160608780'),
(14699, '20160608792'),
(14700, '20160608793'),
(14701, '20160608794'),
(14702, '20160608795'),
(14703, '20160608796'),
(14704, '20160608797'),
(14705, '20160608798'),
(14706, '20160608799'),
(14707, '20160608800'),
(14708, '20160608801'),
(14714, '20160608806'),
(14715, '20160608807'),
(14716, '20160608808'),
(14718, '20160608809'),
(14735, '20160608824'),
(14736, '20160608825'),
(14737, '20160608826'),
(14738, '20160608827'),
(14739, '20160608828'),
(14740, '20160608829'),
(14741, '20160608830'),
(14742, '20160608831'),
(14743, '20160608832'),
(14744, '20160608833'),
(14745, '20160608834'),
(14746, '20160608835'),
(14747, '20160608836'),
(14748, '20160608837'),
(14749, '20160608838'),
(14750, '20160608839'),
(14751, '20160608840'),
(14752, '20160608841'),
(14753, '20160608842'),
(14796, '20160608885'),
(14797, '20160608886'),
(14874, '20160608963'),
(14875, '20160608964'),
(14876, '20160608965'),
(14877, '20160608966'),
(14878, '20160608967'),
(14935, '20160609002'),
(14936, '20160609003'),
(14937, '20160609004'),
(14938, '20160609005'),
(14939, '20160609006'),
(14940, '20160609007'),
(14941, '20160609008'),
(14943, '20160609009'),
(14944, '20160609010'),
(14945, '20160609011'),
(14946, '20160609012'),
(14947, '20160609013'),
(14948, '20160609014'),
(14949, '20160609015'),
(14950, '20160609016'),
(14951, '20160609017'),
(14952, '20160609018'),
(14954, '20160609019'),
(14955, '20160609020'),
(14956, '20160609021'),
(14957, '20160609022'),
(14958, '20160609023'),
(14959, '20160609024'),
(14960, '20160609025'),
(14961, '20160609026'),
(14962, '20160609027'),
(14963, '20160609028'),
(14964, '20160609029'),
(14965, '20160609030'),
(14966, '20160609031'),
(14967, '20160609032'),
(14968, '20160609033'),
(14969, '20160609034'),
(14970, '20160609035'),
(14974, '20160609039'),
(14975, '20160609040'),
(14976, '20160609041'),
(14977, '20160609042'),
(14978, '20160609043'),
(14979, '20160609044'),
(14981, '20160609045'),
(14982, '20160609046'),
(14983, '20160609047'),
(14984, '20160609048'),
(14985, '20160609049'),
(14986, '20160609050'),
(14987, '20160609051'),
(14988, '20160609052'),
(14989, '20160609053'),
(14990, '20160609054'),
(14991, '20160609055'),
(14992, '20160609056'),
(14993, '20160609057'),
(14997, '20160609061'),
(14998, '20160609062'),
(14999, '20160609063'),
(15000, '20160609064'),
(15001, '20160609065'),
(15002, '20160609066'),
(15003, '20160609067'),
(15004, '20160609068'),
(15005, '20160609069'),
(15006, '20160609070'),
(15007, '20160609071'),
(15008, '20160609072'),
(15009, '20160609073'),
(15010, '20160609074'),
(15011, '20160609075'),
(15012, '20160609076'),
(15013, '20160609077'),
(15014, '20160609078'),
(15015, '20160609079'),
(15016, '20160609080'),
(15017, '20160609081'),
(15018, '20160609082'),
(15020, '20160609084'),
(15021, '20160609085'),
(15022, '20160609086'),
(15023, '20160609087'),
(15024, '20160609088'),
(15025, '20160609089'),
(15026, '20160609090'),
(15027, '20160609091'),
(15028, '20160609092'),
(15029, '20160609093'),
(15030, '20160609094'),
(15036, '20160609099'),
(15037, '20160609100'),
(15038, '20160609101'),
(15039, '20160609102'),
(15045, '20160609108'),
(15046, '20160609109'),
(15047, '20160609110'),
(15048, '20160609111'),
(15049, '20160609112'),
(15050, '20160609113'),
(15051, '20160609114'),
(15052, '20160609115'),
(15053, '20160609116'),
(15054, '20160609117'),
(15055, '20160609118'),
(15056, '20160609119'),
(15057, '20160609120'),
(15060, '20160609122'),
(15061, '20160609123'),
(15062, '20160609124'),
(15063, '20160609125'),
(15064, '20160609126'),
(15066, '20160609128'),
(15067, '20160609129'),
(15068, '20160609130'),
(15069, '20160609131'),
(15070, '20160609132'),
(15071, '20160609133'),
(15072, '20160609134'),
(15073, '20160609135'),
(15151, '20160609212'),
(15152, '20160609213'),
(15153, '20160609214'),
(15154, '20160609215'),
(15155, '20160609216'),
(15156, '20160609217'),
(15157, '20160609218'),
(15158, '20160609219'),
(15159, '20160609220'),
(15160, '20160609221'),
(15161, '20160609222'),
(15162, '20160609223'),
(15163, '20160609224'),
(15164, '20160609225'),
(15165, '20160609226'),
(15167, '20160609227'),
(15396, '20160609452'),
(15397, '20160609453'),
(15398, '20160609454'),
(15399, '20160609455'),
(15400, '20160609456'),
(15402, '20160609457'),
(15403, '20160609458'),
(15404, '20160609459'),
(15405, '20160609460'),
(15406, '20160609461'),
(15407, '20160609462'),
(15408, '20160609463'),
(15410, '20160609464'),
(15411, '20160609465'),
(15412, '20160609466'),
(15413, '20160609467'),
(15416, '20160609469'),
(15417, '20160609470'),
(15418, '20160609471'),
(15419, '20160609472'),
(15420, '20160609473'),
(15421, '20160609474'),
(15422, '20160609475'),
(15423, '20160609476'),
(15424, '20160609477'),
(15425, '20160609478'),
(15426, '20160609479'),
(15427, '20160609480'),
(15428, '20160609481'),
(15429, '20160609482'),
(15430, '20160609483'),
(15431, '20160609484'),
(15432, '20160609485'),
(15433, '20160609486'),
(15434, '20160609487'),
(15435, '20160609488'),
(15436, '20160609489'),
(15437, '20160609490'),
(15438, '20160609491'),
(15439, '20160609492'),
(15440, '20160609493'),
(15441, '20160609494'),
(15442, '20160609495'),
(15443, '20160609496'),
(15444, '20160609497'),
(15445, '20160609498'),
(15446, '20160609499'),
(15448, '20160609500'),
(15449, '20160609501'),
(15450, '20160609502'),
(15451, '20160609503'),
(15452, '20160609504'),
(15453, '20160609505'),
(15454, '20160609506'),
(15455, '20160609507'),
(15456, '20160609508'),
(15458, '20160609509'),
(15459, '20160609510'),
(15460, '20160609511'),
(15461, '20160609512'),
(15462, '20160609513'),
(15463, '20160609514'),
(15464, '20160609515'),
(15501, '201606100001'),
(15502, '201606100002'),
(15503, '201606100003'),
(15504, '201606100004'),
(15505, '201606100005'),
(15506, '201606100006'),
(15508, '201606100007'),
(15509, '201606100008'),
(15511, '201606100009'),
(15512, '201606100010'),
(15513, '201606100011'),
(15515, '201606100012'),
(15516, '201606100013'),
(15517, '201606100014'),
(15519, '201606100015'),
(15520, '201606100016'),
(15521, '201606100017'),
(15522, '201606100018'),
(15523, '201606100019'),
(15525, '201606100020'),
(15526, '201606100021'),
(15528, '201606100022'),
(15529, '201606100023'),
(15530, '201606100024'),
(15531, '201606100025'),
(15532, '201606100026'),
(15533, '201606100027'),
(15535, '201606100028'),
(15536, '201606100029'),
(15537, '201606100030'),
(15538, '201606100031'),
(15539, '201606100032'),
(15540, '201606100033'),
(15541, '201606100034'),
(15542, '201606100035'),
(15544, '201606100036'),
(15545, '201606100037'),
(15546, '201606100038'),
(15547, '201606100039'),
(15548, '201606100040'),
(15549, '201606100041'),
(15550, '201606100042'),
(15551, '201606100043'),
(15557, '201606110001'),
(15558, '201606110002'),
(15559, '201606110003'),
(15560, '201606110004'),
(15561, '201606110005'),
(15562, '201606110006'),
(15563, '201606110007'),
(15564, '201606110008'),
(15565, '201606110009'),
(15568, '201606110011'),
(15569, '201606110012'),
(15570, '201606110013'),
(15572, '201606110015'),
(15573, '201606110016'),
(15574, '201606110017'),
(15575, '201606110018'),
(15577, '201606110019'),
(15578, '201606110020'),
(15579, '201606110021'),
(15580, '201606110022'),
(15581, '201606110023'),
(15582, '201606110024'),
(15583, '201606110025'),
(15585, '201606110026'),
(15587, '201606110027'),
(15588, '201606110028'),
(15589, '201606110029'),
(15590, '201606110030'),
(15592, '201606110031'),
(15594, '201606110032'),
(15597, '201606110033'),
(15599, '201606110034'),
(15600, '201606110035'),
(15601, '201606110036'),
(15602, '201606110037'),
(15603, '201606110038'),
(15604, '201606110039'),
(15605, '201606110040'),
(15606, '201606110041'),
(15607, '201606110042'),
(15608, '201606110043'),
(15609, '201606110044'),
(15610, '201606110045'),
(15611, '201606110046'),
(15612, '201606110047'),
(15613, '201606110048'),
(15614, '201606110049'),
(15626, '201606110061'),
(15629, '201606110064'),
(15632, '201606110067'),
(15635, '201606110069'),
(15636, '201606110070'),
(15637, '201606110071'),
(15638, '201606110072'),
(15639, '201606110073'),
(15640, '201606110074'),
(15641, '201606110075'),
(15642, '201606110076');
INSERT INTO `sma_stock` (`id`, `code`) VALUES
(15643, '201606110077'),
(15644, '201606110078'),
(15645, '201606110079'),
(15646, '201606110080'),
(15647, '201606110081'),
(15648, '201606110082'),
(15649, '201606110083'),
(15650, '201606110084'),
(15651, '201606110085'),
(15652, '201606110086'),
(15653, '201606110087'),
(15654, '201606110088'),
(15655, '201606110089'),
(15656, '201606110090'),
(15657, '201606110091'),
(15658, '201606110092'),
(15659, '201606110093'),
(15660, '201606110094'),
(15661, '201606110095'),
(15662, '201606110096'),
(15663, '201606110097'),
(15665, '201606110098'),
(15666, '201606110099'),
(15667, '201606110100'),
(15668, '201606110101'),
(15669, '201606110102'),
(15670, '201606110103'),
(15671, '201606110104'),
(15674, '201606110107'),
(15675, '201606110108'),
(15677, '201606110109'),
(15678, '201606110110'),
(15679, '201606110111'),
(15680, '201606110112'),
(15682, '201606110113'),
(15683, '201606110114'),
(15684, '201606110115'),
(15685, '201606110116'),
(15686, '201606110117'),
(15687, '201606110118'),
(15689, '201606110119'),
(15690, '201606110120'),
(15692, '201606110121'),
(15694, '201606110122'),
(15695, '201606110123'),
(15696, '201606110124'),
(15697, '201606110125'),
(15698, '201606110126'),
(15699, '201606110127'),
(15700, '201606110128'),
(15702, '201606110129'),
(15703, '201606110130'),
(15704, '201606110131'),
(15705, '201606110132'),
(15706, '201606110133'),
(15707, '201606110134'),
(15708, '201606110135'),
(15709, '201606110136'),
(15710, '201606110137'),
(15713, '201606110140'),
(15714, '201606110141'),
(15715, '201606110142'),
(15716, '201606110143'),
(15717, '201606110144'),
(15718, '201606110145'),
(15719, '201606110146'),
(15720, '201606110147'),
(15721, '201606110148'),
(15722, '201606110149'),
(15723, '201606110150'),
(15724, '201606110151'),
(15725, '201606110152'),
(15726, '201606110153'),
(15727, '201606110154'),
(15728, '201606110155'),
(15729, '201606110156'),
(15730, '201606110157'),
(15731, '201606110158'),
(15732, '201606110159'),
(15734, '201606110160'),
(15735, '201606110161'),
(15736, '201606110162'),
(15737, '201606110163'),
(15738, '201606110164'),
(15740, '201606110165'),
(15742, '201606110166'),
(15743, '201606110167'),
(15745, '201606110168'),
(15746, '201606110169'),
(15747, '201606110170'),
(15748, '201606110171'),
(15773, '201606120001'),
(15777, '201606120005'),
(15778, '201606120006'),
(15779, '201606120007'),
(15780, '201606120008'),
(15781, '201606120009'),
(15782, '201606120010'),
(15783, '201606120011'),
(15784, '201606120012'),
(15785, '201606120013'),
(15786, '201606120014'),
(15787, '201606120015'),
(15788, '201606120016'),
(15789, '201606120017'),
(15790, '201606120018'),
(15791, '201606120019'),
(15792, '201606120020'),
(15793, '201606120021'),
(15794, '201606120022'),
(15795, '201606120023'),
(15796, '201606120024'),
(15797, '201606120025'),
(15798, '201606120026'),
(15799, '201606120027'),
(15800, '201606120028'),
(15801, '201606120029'),
(15802, '201606120030'),
(15803, '201606120031'),
(15806, '201606120034'),
(15807, '201606120035'),
(15808, '201606120036'),
(15809, '201606120037'),
(15810, '201606120038'),
(15811, '201606120039'),
(15812, '201606120040'),
(15813, '201606120041'),
(15814, '201606120042'),
(15815, '201606120043'),
(15816, '201606120044'),
(15817, '201606120045'),
(15818, '201606120046'),
(15819, '201606120047'),
(15820, '201606120048'),
(15821, '201606120049'),
(15822, '201606120050'),
(15823, '201606120051'),
(15824, '201606120052'),
(15825, '201606120053'),
(15826, '201606120054'),
(15827, '201606120055'),
(15828, '201606120056'),
(15829, '201606120057'),
(15830, '201606120058'),
(15831, '201606120059'),
(15832, '201606120060'),
(15833, '201606120061'),
(15834, '201606120062'),
(15835, '201606120063'),
(15836, '201606120064'),
(15837, '201606120065'),
(15838, '201606120066'),
(15839, '201606120067'),
(15840, '201606120068'),
(15841, '201606120069'),
(15842, '201606120070'),
(15843, '201606120071'),
(15844, '201606120072'),
(15845, '201606120073'),
(15846, '201606120074'),
(15847, '201606120075'),
(15848, '201606120076'),
(15849, '201606120077'),
(15850, '201606120078'),
(15851, '201606120079'),
(15852, '201606120080'),
(15853, '201606120081'),
(15854, '201606120082'),
(15855, '201606120083'),
(15856, '201606120084'),
(15857, '201606120085'),
(15858, '201606120086'),
(15859, '201606120087'),
(15860, '201606120088'),
(15861, '201606120089'),
(15862, '201606120090'),
(15863, '201606120091'),
(15864, '201606120092'),
(15865, '201606120093'),
(15866, '201606120094'),
(15867, '201606120095'),
(15868, '201606120096'),
(15869, '201606120097'),
(15870, '201606120098'),
(15871, '201606120099'),
(15872, '201606120100'),
(15873, '201606120101'),
(15874, '201606120102'),
(15875, '201606120103'),
(15876, '201606120104'),
(15877, '201606120105'),
(15878, '201606120106'),
(15879, '201606120107'),
(15880, '201606120108'),
(15881, '201606120109'),
(15882, '201606120110'),
(15883, '201606120111'),
(15884, '201606120112'),
(15885, '201606120113'),
(15886, '201606120114'),
(15887, '201606120115'),
(15888, '201606120116'),
(15889, '201606120117'),
(15890, '201606120118'),
(15891, '201606120119'),
(15892, '201606120120'),
(15893, '201606120121'),
(15894, '201606120122'),
(15895, '201606120123'),
(15896, '201606120124'),
(15897, '201606120125'),
(15898, '201606120126'),
(15899, '201606120127'),
(15900, '201606120128'),
(15901, '201606120129'),
(15902, '201606120130'),
(15903, '201606120131'),
(15904, '201606120132'),
(15905, '201606120133'),
(15906, '201606120134'),
(15907, '201606120135'),
(15908, '201606120136'),
(15909, '201606120137'),
(15910, '201606120138'),
(15911, '201606120139'),
(15912, '201606120140'),
(15913, '201606120141'),
(15914, '201606120142'),
(15915, '201606120143'),
(15916, '201606120144'),
(15917, '201606120145'),
(15919, '201606120147'),
(15920, '201606120148'),
(15921, '201606120149'),
(15922, '201606120150'),
(15927, '201606120155'),
(15928, '201606120156'),
(15929, '201606120157'),
(15930, '201606120158'),
(15931, '201606120159'),
(15932, '201606120160'),
(15933, '201606120161'),
(15934, '201606120162'),
(15935, '201606120163'),
(15936, '201606120164'),
(15937, '201606120165'),
(15938, '201606120166'),
(15939, '201606120167'),
(15940, '201606120168'),
(15941, '201606120169'),
(15942, '201606120170'),
(15943, '201606120171'),
(15944, '201606120172'),
(15945, '201606120173'),
(15946, '201606120174'),
(15947, '201606120175'),
(15948, '201606120176'),
(15949, '201606120177'),
(15950, '201606120178'),
(15951, '201606120179'),
(15952, '201606120180'),
(15953, '201606120181'),
(15954, '201606120182'),
(15955, '201606120183'),
(15956, '201606120184'),
(15957, '201606120185'),
(15958, '201606120186'),
(15959, '201606120187'),
(15960, '201606120188'),
(15961, '201606120189'),
(15962, '201606120190'),
(15963, '201606120191'),
(15964, '201606120192'),
(15965, '201606120193'),
(15966, '201606120194'),
(15967, '201606120195'),
(15968, '201606120196'),
(15969, '201606120197'),
(15970, '201606120198'),
(15971, '201606120199'),
(15972, '201606120200'),
(15973, '201606120201'),
(15974, '201606120202'),
(15975, '201606120203'),
(15976, '201606120204'),
(15977, '201606120205'),
(15978, '201606120206'),
(15979, '201606120207'),
(15980, '201606120208'),
(15981, '201606120209'),
(15982, '201606120210'),
(15983, '201606120211'),
(15984, '201606120212'),
(15985, '201606120213'),
(15986, '201606120214'),
(15987, '201606120215'),
(15988, '201606120216'),
(15989, '201606120217'),
(15990, '201606120218'),
(15991, '201606120219'),
(15992, '201606120220'),
(15993, '201606120221'),
(15994, '201606120222'),
(15995, '201606120223'),
(15996, '201606120224'),
(15997, '201606120225'),
(15998, '201606120226'),
(15999, '201606120227'),
(16000, '201606120228'),
(16001, '201606120229'),
(16002, '201606120230'),
(16003, '201606120231'),
(16004, '201606120232'),
(16005, '201606120233'),
(16006, '201606120234'),
(16007, '201606120235'),
(16008, '201606120236'),
(16009, '201606120237'),
(16010, '201606120238'),
(16011, '201606120239'),
(16012, '201606120240'),
(16013, '201606120241'),
(16014, '201606120242'),
(16015, '201606120243'),
(16016, '201606120244'),
(16017, '201606120245'),
(16018, '201606120246'),
(16019, '201606120247'),
(16020, '201606120248'),
(16021, '201606120249'),
(16022, '201606120250'),
(16023, '201606120251'),
(16024, '201606120252'),
(16025, '201606120253'),
(16026, '201606120254'),
(16027, '201606120255'),
(16028, '201606120256'),
(16029, '201606120257'),
(16030, '201606120258'),
(16031, '201606120259'),
(16032, '201606120260'),
(16033, '201606120261'),
(16034, '201606120262'),
(16035, '201606120263'),
(16036, '201606120264'),
(16037, '201606120265'),
(16038, '201606120266'),
(16039, '201606120267'),
(16040, '201606120268'),
(16041, '201606120269'),
(16042, '201606120270'),
(16043, '201606120271'),
(16044, '201606120272'),
(16045, '201606120273'),
(16046, '201606120274'),
(16047, '201606120275'),
(16048, '201606120276'),
(16049, '201606120277'),
(16050, '201606120278'),
(16051, '201606120279'),
(16052, '201606120280'),
(16053, '201606120281'),
(16054, '201606120282'),
(16055, '201606120283'),
(16056, '201606120284'),
(16057, '201606120285'),
(16058, '201606120286'),
(16059, '201606120287'),
(16060, '201606120288'),
(16061, '201606120289'),
(16062, '201606120290'),
(16063, '201606120291'),
(16064, '201606120292'),
(16065, '201606120293'),
(16066, '201606120294'),
(16067, '201606120295'),
(16068, '201606120296'),
(16069, '201606120297'),
(16070, '201606120298'),
(16071, '201606120299'),
(16072, '201606120300'),
(16073, '201606120301'),
(16074, '201606120302'),
(16075, '201606120303'),
(16076, '201606120304'),
(16077, '201606120305'),
(16078, '201606120306'),
(16079, '201606120307'),
(16080, '201606120308'),
(16081, '201606120309'),
(16082, '201606120310'),
(16083, '201606120311'),
(16084, '201606120312'),
(16085, '201606120313'),
(16086, '201606120314'),
(16087, '201606120315'),
(16088, '201606120316'),
(16089, '201606120317'),
(16090, '201606120318'),
(16091, '201606120319'),
(16092, '201606120320'),
(16093, '201606120321'),
(16094, '201606120322'),
(16095, '201606120323'),
(16096, '201606120324'),
(16097, '201606120325'),
(16098, '201606120326'),
(16099, '201606120327'),
(16100, '201606120328'),
(16101, '201606120329'),
(16102, '201606120330'),
(16103, '201606120331'),
(16104, '201606120332'),
(16105, '201606120333'),
(16106, '201606120334'),
(16107, '201606120335'),
(16108, '201606120336'),
(16109, '201606120337'),
(16110, '201606120338'),
(16111, '201606120339'),
(16112, '201606120340'),
(16113, '201606120341'),
(16114, '201606120342'),
(16115, '201606120343'),
(16116, '201606120344'),
(16117, '201606120345'),
(16118, '201606120346'),
(16119, '201606120347'),
(16120, '201606120348'),
(16121, '201606120349'),
(16122, '201606120350'),
(16123, '201606120351'),
(16124, '201606120352'),
(16125, '201606120353'),
(16126, '201606120354'),
(16127, '201606120355'),
(16128, '201606120356'),
(16129, '201606120357'),
(16130, '201606120358'),
(16131, '201606120359'),
(16132, '201606120360'),
(16133, '201606120361'),
(16134, '201606120362'),
(16135, '201606120363'),
(16136, '201606120364'),
(16137, '201606120365'),
(16138, '201606120366'),
(16139, '201606120367'),
(16140, '201606120368'),
(16141, '201606120369'),
(16142, '201606120370'),
(16143, '201606120371'),
(16144, '201606120372'),
(16145, '201606120373'),
(16146, '201606120374'),
(16147, '201606120375'),
(16148, '201606120376'),
(16149, '201606120377'),
(16150, '201606120378'),
(16151, '201606120379'),
(16152, '201606120380'),
(16153, '201606120381'),
(16154, '201606120382'),
(16155, '201606120383'),
(16156, '201606120384'),
(16157, '201606120385'),
(16158, '201606120386'),
(16159, '201606120387'),
(16160, '201606120388'),
(16161, '201606120389'),
(16162, '201606120390'),
(16163, '201606120391'),
(16164, '201606120392'),
(16165, '201606120393'),
(16166, '201606120394'),
(16167, '201606120395'),
(16168, '201606120396'),
(16169, '201606120397'),
(16170, '201606120398'),
(16171, '201606120399'),
(16172, '201606120400'),
(16173, '201606120401'),
(16174, '201606120402'),
(16175, '201606120403'),
(16176, '201606120404'),
(16177, '201606120405'),
(16178, '201606120406'),
(16179, '201606120407'),
(16180, '201606120408'),
(16181, '201606120409'),
(16182, '201606120410'),
(16183, '201606120411'),
(16184, '201606120412'),
(16185, '201606120413'),
(16186, '201606120414'),
(16187, '201606120415'),
(16188, '201606120416'),
(16189, '201606120417'),
(16190, '201606120418'),
(16191, '201606120419'),
(16192, '201606120420'),
(16193, '201606120421'),
(16194, '201606120422'),
(16195, '201606120423'),
(16196, '201606120424'),
(16197, '201606120425'),
(16198, '201606120426'),
(16199, '201606120427'),
(16200, '201606120428'),
(16201, '201606120429'),
(16202, '201606120430'),
(16203, '201606120431'),
(16204, '201606120432'),
(16205, '201606120433'),
(16206, '201606120434'),
(16207, '201606120435'),
(16208, '201606120436'),
(16209, '201606120437'),
(16210, '201606120438'),
(16211, '201606120439'),
(16212, '201606120440'),
(16213, '201606120441'),
(16214, '201606120442'),
(16215, '201606120443'),
(16216, '201606120444'),
(16217, '201606120445'),
(16218, '201606120446'),
(16219, '201606120447'),
(16220, '201606120448'),
(16221, '201606120449'),
(16222, '201606120450'),
(16223, '201606120451'),
(16224, '201606120452'),
(16225, '201606120453'),
(16226, '201606120454'),
(16227, '201606120455'),
(16228, '201606120456'),
(16230, '201606120457'),
(16231, '201606120458'),
(16232, '201606120459'),
(16233, '201606120460'),
(16234, '201606120461'),
(16235, '201606120462'),
(16236, '201606120463'),
(16237, '201606120464'),
(16238, '201606120465'),
(16239, '201606120466'),
(16240, '201606120467'),
(16241, '201606120468'),
(16242, '201606120469'),
(16243, '201606120470'),
(16244, '201606120471'),
(16245, '201606120472'),
(16246, '201606120473'),
(16247, '201606120474'),
(16248, '201606120475'),
(16249, '201606120476'),
(16250, '201606120477'),
(16251, '201606120478'),
(16252, '201606120479'),
(16253, '201606120480'),
(16254, '201606120481'),
(16255, '201606120482'),
(16256, '201606120483'),
(16257, '201606120484'),
(16258, '201606120485'),
(16259, '201606120486'),
(16262, '201606120489'),
(16263, '201606120490'),
(16264, '201606120491'),
(16265, '201606120492'),
(16266, '201606120493'),
(16267, '201606120494'),
(16268, '201606120495'),
(16269, '201606120496'),
(16270, '201606120497'),
(16271, '201606120498'),
(16272, '201606120499'),
(16273, '201606120500'),
(16274, '201606120501'),
(16275, '201606120502'),
(16276, '201606120503'),
(16277, '201606120504'),
(16278, '201606120505'),
(16279, '201606120506'),
(16280, '201606120507'),
(16281, '201606120508'),
(16283, '201606120509'),
(16284, '201606120510'),
(16285, '201606120511'),
(16286, '201606120512'),
(16287, '201606120513'),
(16288, '201606120514'),
(16289, '201606120515'),
(16290, '201606120516'),
(16292, '201606120517'),
(16293, '201606120518'),
(16294, '201606120519'),
(16295, '201606120520'),
(16296, '201606120521'),
(16297, '201606120522'),
(16300, '201606120525'),
(16301, '201606120526'),
(16302, '201606120527'),
(16303, '201606120528'),
(16304, '201606120529'),
(16305, '201606120530'),
(16306, '201606120531'),
(16307, '201606120532'),
(16308, '201606120533'),
(16309, '201606120534'),
(16310, '201606120535'),
(16311, '201606120536'),
(16312, '201606120537'),
(16313, '201606120538'),
(16314, '201606120539'),
(16315, '201606120540'),
(16316, '201606120541'),
(16317, '201606120542'),
(16318, '201606120543'),
(16320, '201606120544'),
(16321, '201606120545'),
(16322, '201606120546'),
(16323, '201606120547'),
(16324, '201606120548'),
(16325, '201606120549'),
(16326, '201606120550'),
(16327, '201606120551'),
(16328, '201606120552'),
(16329, '201606120553'),
(16330, '201606120554'),
(16331, '201606120555'),
(16332, '201606120556'),
(16333, '201606120557'),
(16334, '201606120558'),
(16335, '201606120559'),
(16336, '201606120560'),
(16337, '201606120561'),
(16338, '201606120562'),
(16339, '201606120563'),
(16340, '201606120564'),
(16341, '201606120565'),
(16342, '201606120566'),
(16343, '201606120567'),
(16345, '201606120568'),
(16346, '201606120569'),
(16347, '201606120570'),
(16348, '201606120571'),
(16349, '201606120572'),
(16350, '201606120573'),
(16351, '201606120574'),
(16352, '201606120575'),
(16353, '201606120576'),
(16354, '201606120577'),
(16355, '201606120578'),
(16356, '201606120579'),
(16357, '201606120580'),
(16358, '201606120581'),
(16359, '201606120582'),
(16360, '201606120583'),
(16361, '201606120584'),
(16362, '201606120585'),
(16363, '201606120586'),
(16364, '201606120587'),
(16365, '201606120588'),
(16366, '201606120589'),
(16367, '201606120590'),
(16368, '201606120591'),
(16369, '201606120592'),
(16370, '201606120593'),
(16371, '201606120594'),
(16372, '201606120595'),
(16373, '201606120596'),
(16374, '201606120597'),
(16375, '201606120598'),
(16376, '201606120599'),
(16377, '201606120600'),
(16378, '201606120601'),
(16379, '201606120602'),
(16380, '201606120603'),
(16381, '201606120604'),
(16382, '201606120605'),
(16383, '201606120606'),
(16384, '201606120607'),
(16385, '201606120608'),
(16386, '201606120609'),
(16387, '201606120610'),
(16388, '201606120611'),
(16389, '201606120612'),
(16390, '201606120613'),
(16391, '201606120614'),
(16392, '201606120615'),
(16393, '201606120616'),
(16394, '201606120617'),
(16395, '201606120618'),
(16396, '201606120619'),
(16397, '201606120620'),
(16398, '201606120621'),
(16399, '201606120622'),
(16400, '201606120623'),
(16401, '201606120624'),
(16402, '201606120625'),
(16403, '201606120626'),
(16404, '201606120627'),
(16406, '201606120628'),
(16407, '201606120629'),
(16408, '201606120630'),
(16409, '201606120631'),
(16410, '201606120632'),
(16411, '201606120633'),
(16412, '201606120634'),
(16413, '201606120635'),
(16414, '201606120636'),
(16415, '201606120637'),
(16416, '201606120638'),
(16417, '201606120639'),
(16418, '201606120640'),
(16419, '201606120641'),
(16420, '201606120642'),
(16421, '201606120643'),
(16422, '201606120644'),
(16423, '201606120645'),
(16424, '201606120646'),
(16425, '201606120647'),
(16426, '201606120648'),
(16427, '201606120649'),
(16428, '201606120650'),
(16429, '201606120651'),
(16430, '201606120652'),
(16431, '201606120653'),
(16432, '201606120654'),
(16433, '201606120655'),
(16434, '201606120656'),
(16435, '201606120657'),
(16436, '201606120658'),
(16437, '201606120659'),
(16438, '201606120660'),
(16439, '201606120661'),
(16440, '201606120662'),
(16441, '201606120663'),
(16442, '201606120664'),
(16443, '201606120665'),
(16444, '201606120666'),
(16445, '201606120667'),
(16447, '201606120668'),
(16448, '201606120669'),
(16449, '201606120670'),
(16450, '201606120671'),
(16451, '201606120672'),
(16452, '201606120673'),
(16453, '201606120674'),
(16454, '201606120675'),
(16455, '201606120676'),
(16456, '201606120677'),
(16457, '201606120678'),
(16458, '201606120679'),
(16459, '201606120680'),
(16460, '201606120681'),
(16461, '201606120682'),
(16462, '201606120683'),
(16463, '201606120684'),
(16464, '201606120685'),
(16465, '201606120686'),
(16466, '201606120687'),
(16467, '201606120688'),
(16468, '201606120689'),
(16469, '201606120690'),
(16470, '201606120691'),
(16471, '201606120692'),
(16472, '201606120693'),
(16473, '201606120694'),
(16474, '201606120695'),
(16475, '201606120696'),
(16476, '201606120697'),
(16477, '201606120698'),
(16478, '201606120699'),
(16479, '201606120700'),
(16480, '201606120701'),
(16481, '201606120702'),
(16482, '201606120703'),
(16483, '201606120704'),
(16484, '201606120705'),
(16485, '201606120706'),
(16486, '201606120707'),
(16487, '201606120708'),
(16488, '201606120709'),
(16489, '201606120710'),
(16490, '201606120711'),
(16643, '201606130108'),
(16644, '201606130109'),
(16651, '201606130115'),
(16652, '201606130116'),
(16653, '201606130117'),
(16655, '201606130119'),
(16656, '201606130120'),
(16658, '201606130122');

-- --------------------------------------------------------

--
-- Table structure for table `sma_style`
--

CREATE TABLE `sma_style` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `design_id` int(11) NOT NULL,
  `userby` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Deactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_subcategories`
--

CREATE TABLE `sma_subcategories` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `code` varchar(55) NOT NULL,
  `name` varchar(55) NOT NULL,
  `image` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_sup_damage`
--

CREATE TABLE `sma_sup_damage` (
  `id` int(11) NOT NULL,
  `reference_no` varchar(55) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delivery_date` datetime DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `supplier` varchar(55) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` varchar(1000) NOT NULL,
  `total` decimal(25,4) DEFAULT NULL,
  `product_discount` decimal(25,4) DEFAULT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `order_discount` decimal(25,4) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT NULL,
  `product_tax` decimal(25,4) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `paid` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `status` varchar(55) DEFAULT '',
  `payment_status` varchar(20) DEFAULT 'pending',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `attachment` varchar(55) DEFAULT NULL,
  `lr_no` varchar(255) DEFAULT NULL,
  `ag_order_no` varchar(255) DEFAULT NULL,
  `purchase_no` varchar(255) DEFAULT NULL,
  `store_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_sup_damage_items`
--

CREATE TABLE `sma_sup_damage_items` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `transfer_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_cost` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(20) DEFAULT NULL,
  `discount` varchar(20) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `quantity_balance` decimal(15,4) DEFAULT '0.0000',
  `date` date NOT NULL,
  `status` varchar(50) NOT NULL,
  `unit_cost` decimal(25,4) DEFAULT NULL,
  `real_unit_cost` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_suspended_bills`
--

CREATE TABLE `sma_suspended_bills` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) DEFAULT NULL,
  `count` int(11) NOT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `biller_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `suspend_note` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_suspended_items`
--

CREATE TABLE `sma_suspended_items` (
  `id` int(11) NOT NULL,
  `suspend_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_tax_invoice`
--

CREATE TABLE `sma_tax_invoice` (
  `id` int(11) NOT NULL,
  `tax_invoice_no` varchar(20) DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `items_detail` text,
  `grand_total` varchar(20) DEFAULT NULL,
  `crate_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_tax_rates`
--

CREATE TABLE `sma_tax_rates` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `code` varchar(10) DEFAULT NULL,
  `rate` decimal(12,4) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_transfers`
--

CREATE TABLE `sma_transfers` (
  `id` int(11) NOT NULL,
  `transfer_no` varchar(55) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `from_warehouse_id` int(11) NOT NULL,
  `from_warehouse_code` varchar(55) NOT NULL,
  `from_warehouse_name` varchar(55) NOT NULL,
  `to_warehouse_id` int(11) NOT NULL,
  `to_warehouse_code` varchar(55) NOT NULL,
  `to_warehouse_name` varchar(55) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT NULL,
  `grand_total` decimal(25,4) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `status` varchar(55) NOT NULL DEFAULT 'pending',
  `shipping` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `attachment` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_transfer_items`
--

CREATE TABLE `sma_transfer_items` (
  `id` int(11) NOT NULL,
  `transfer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `net_unit_cost` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) DEFAULT NULL,
  `quantity_balance` decimal(15,4) NOT NULL,
  `unit_cost` decimal(25,4) DEFAULT NULL,
  `real_unit_cost` decimal(25,4) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_transport`
--

CREATE TABLE `sma_transport` (
  `id` int(11) NOT NULL,
  `code` varchar(250) NOT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `email` text,
  `gender` varchar(50) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `transport_name` text,
  `address` text,
  `city` varchar(250) DEFAULT NULL,
  `state` varchar(250) DEFAULT NULL,
  `postal_code` varchar(50) DEFAULT NULL,
  `country` varchar(200) DEFAULT NULL,
  `mobile` text,
  `landline` text,
  `fax` text,
  `status` enum('Active','Deactive') DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `gst_no` varchar(250) NOT NULL,
  `igst` enum('1','0') DEFAULT NULL COMMENT '1 yes 0 no'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_type`
--

CREATE TABLE `sma_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `product_items_id` int(11) NOT NULL,
  `userby` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Deactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_unit`
--

CREATE TABLE `sma_unit` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `userby` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` enum('Active','Deactive') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sma_unlabelled_damage_return`
--

CREATE TABLE `sma_unlabelled_damage_return` (
  `id` int(11) NOT NULL,
  `reference_no` varchar(55) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delivery_date` datetime DEFAULT NULL,
  `supplier_id` int(11) NOT NULL,
  `supplier` varchar(55) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` varchar(1000) NOT NULL,
  `total` decimal(25,4) DEFAULT NULL,
  `product_discount` decimal(25,4) DEFAULT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `order_discount` decimal(25,4) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT NULL,
  `product_tax` decimal(25,4) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `paid` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `status` varchar(55) DEFAULT '',
  `payment_status` varchar(20) DEFAULT 'pending',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `attachment` varchar(55) DEFAULT NULL,
  `lr_no` varchar(255) DEFAULT NULL,
  `ag_order_no` varchar(255) DEFAULT NULL,
  `purchase_no` varchar(255) DEFAULT NULL,
  `store_id` int(11) NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_unlabelled_damage_return_items`
--

CREATE TABLE `sma_unlabelled_damage_return_items` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `transfer_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_cost` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(20) DEFAULT NULL,
  `discount` varchar(20) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `quantity_balance` decimal(15,4) DEFAULT '0.0000',
  `date` date NOT NULL,
  `status` varchar(50) NOT NULL,
  `unit_cost` decimal(25,4) DEFAULT NULL,
  `real_unit_cost` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_unlabelled_products`
--

CREATE TABLE `sma_unlabelled_products` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` char(255) NOT NULL,
  `unit` varchar(50) DEFAULT NULL,
  `cost` decimal(25,4) DEFAULT NULL,
  `price` decimal(25,4) NOT NULL,
  `cper` int(11) DEFAULT NULL,
  `pper` int(11) DEFAULT NULL,
  `uper` int(11) DEFAULT NULL,
  `rateper` int(11) DEFAULT NULL,
  `ratetype` varchar(255) DEFAULT NULL,
  `singlerate` varchar(100) DEFAULT NULL,
  `mrprate` varchar(100) DEFAULT NULL,
  `mulratef` varchar(100) DEFAULT NULL,
  `mulratet` varchar(100) DEFAULT NULL,
  `alert_quantity` decimal(15,4) DEFAULT '0.0000',
  `image` varchar(255) DEFAULT 'no_image.png',
  `category_id` int(11) DEFAULT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `cf1` varchar(255) DEFAULT NULL,
  `cf2` varchar(255) DEFAULT NULL,
  `cf3` varchar(255) DEFAULT NULL,
  `cf4` varchar(255) DEFAULT NULL,
  `cf5` varchar(255) DEFAULT NULL,
  `cf6` varchar(255) DEFAULT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `tax_rate` int(11) DEFAULT NULL,
  `track_quantity` tinyint(1) DEFAULT '1',
  `details` varchar(1000) DEFAULT NULL,
  `warehouse` int(11) DEFAULT NULL,
  `barcode_symbology` varchar(55) NOT NULL DEFAULT 'code128',
  `file` varchar(100) DEFAULT NULL,
  `product_details` text,
  `tax_method` tinyint(1) DEFAULT '0',
  `type` varchar(55) NOT NULL DEFAULT 'standard',
  `store_id` int(11) DEFAULT NULL,
  `department` int(11) DEFAULT NULL,
  `section` int(11) DEFAULT NULL,
  `product_items` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `brands` int(11) DEFAULT NULL,
  `design` int(11) DEFAULT NULL,
  `style` int(11) DEFAULT NULL,
  `pattern` int(11) DEFAULT NULL,
  `fitting` int(11) DEFAULT NULL,
  `fabric` int(11) DEFAULT NULL,
  `sizeangle` varchar(10) DEFAULT NULL,
  `sizetype` varchar(255) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `colortype` varchar(255) DEFAULT NULL,
  `color` varchar(255) DEFAULT NULL,
  `colorqty` int(11) DEFAULT NULL,
  `combo_discount` int(11) DEFAULT NULL,
  `batch` varchar(255) DEFAULT NULL,
  `batchdate` datetime NOT NULL,
  `supplier1` int(11) DEFAULT NULL,
  `supplier1price` decimal(25,4) DEFAULT NULL,
  `supplier2` int(11) DEFAULT NULL,
  `supplier2price` decimal(25,4) DEFAULT NULL,
  `supplier3` int(11) DEFAULT NULL,
  `supplier3price` decimal(25,4) DEFAULT NULL,
  `supplier4` int(11) DEFAULT NULL,
  `supplier4price` decimal(25,4) DEFAULT NULL,
  `supplier5` int(11) DEFAULT NULL,
  `supplier5price` decimal(25,4) DEFAULT NULL,
  `status` enum('Active','Deactive') NOT NULL DEFAULT 'Active',
  `squantity` decimal(15,4) DEFAULT NULL,
  `sunit` varchar(50) DEFAULT NULL,
  `colorcode` varchar(255) DEFAULT NULL,
  `billno` varchar(255) DEFAULT NULL,
  `hsn` varchar(255) DEFAULT NULL,
  `gst` varchar(255) DEFAULT NULL,
  `addup` enum('0','1') DEFAULT NULL,
  `adduppercentage` varchar(255) DEFAULT NULL,
  `cess` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_users`
--

CREATE TABLE `sma_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `last_ip_address` varbinary(45) DEFAULT NULL,
  `ip_address` varbinary(45) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `avatar` varchar(55) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `warehouse_id` int(10) UNSIGNED DEFAULT NULL,
  `biller_id` int(10) UNSIGNED DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `emplyee_id` int(11) DEFAULT NULL,
  `show_cost` tinyint(1) DEFAULT '0',
  `show_price` tinyint(1) DEFAULT '0',
  `award_points` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_users`
--

INSERT INTO `sma_users` (`id`, `last_ip_address`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `avatar`, `gender`, `group_id`, `warehouse_id`, `biller_id`, `company_id`, `emplyee_id`, `show_cost`, `show_price`, `award_points`) VALUES
(1, 0x3a3a31, 0x3137352e3130302e3135392e323136, 'admin@admin.com', 'f8d30071c8b978c30719ed98f774bb3d52700036', NULL, 'admin@admin.com', NULL, '159bce7bde8fe0a51a2cf9de06ad8b90eaa55d3a', 1506149307, 'd78b3389e6c3e96391c6ce0d4ab1d06012d64470', 1492694088, 1589939169, 1, 'admin', 'admin', 'abc', '7507546002', '25a65918e0de6650df7ef632221dbb54.jpg', 'male', 1, 0, 0, NULL, NULL, 0, 0, 4139);

-- --------------------------------------------------------

--
-- Table structure for table `sma_user_logins`
--

CREATE TABLE `sma_user_logins` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_user_logins`
--

INSERT INTO `sma_user_logins` (`id`, `user_id`, `company_id`, `ip_address`, `login`, `time`) VALUES
(0, 22, NULL, 0x3a3a31, 'admin@admin.com', '2020-05-20 01:29:20');

-- --------------------------------------------------------

--
-- Table structure for table `sma_variants`
--

CREATE TABLE `sma_variants` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `sma_vw_emp`
-- (See below for the actual view)
--
CREATE TABLE `sma_vw_emp` (
`id` int(11)
,`emp_id` int(11)
,`photo` text
,`joining_date` varchar(10)
,`emp_name` text
,`mobile` varchar(250)
,`store` varchar(55)
,`dept_name` varchar(255)
,`desigation` varchar(255)
,`msd` varchar(250)
,`refered_by` varchar(255)
,`incentive` decimal(10,2)
,`status` enum('Active','Deactive')
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `sma_vw_orderlist`
-- (See below for the actual view)
--
CREATE TABLE `sma_vw_orderlist` (
`id` int(11)
,`order_date` timestamp
,`sullpier` varchar(313)
,`order_no` varchar(55)
,`date` varchar(24)
,`store` varchar(55)
,`grand_total` decimal(25,4)
,`transport_name` mediumtext
,`totals` decimal(37,4)
,`status` varchar(20)
,`sec_qty` char(1)
,`first_name` varchar(50)
,`user_id` int(11)
,`warehouse_id` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `sma_vw_purchase`
-- (See below for the actual view)
--
CREATE TABLE `sma_vw_purchase` (
`id` int(11)
,`updated_at` varchar(24)
,`supplier` varchar(55)
,`reference_no` varchar(55)
,`dates` varchar(24)
,`lr_no` varchar(255)
,`up_date` varchar(24)
,`comp` varchar(55)
,`grand_total` decimal(25,4)
,`paid` decimal(25,4)
,`balance` decimal(26,4)
,`payment_status` varchar(20)
,`pur_challan` enum('0','1')
,`warehouse_id` int(11)
,`quantity` decimal(37,4)
,`sqty` decimal(37,4)
,`created_by` int(11)
,`supplier_id` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `sma_warehouses`
--

CREATE TABLE `sma_warehouses` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `map` varchar(255) DEFAULT NULL,
  `phone` varchar(55) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_warehouses`
--

INSERT INTO `sma_warehouses` (`id`, `code`, `name`, `address`, `map`, `phone`, `email`) VALUES
(1, 'Default', 'Default', '<p>Pune</p>', NULL, '7507546002', 'akash@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `sma_warehouses_products`
--

CREATE TABLE `sma_warehouses_products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `rack` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_warehouses_products_variants`
--

CREATE TABLE `sma_warehouses_products_variants` (
  `id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `rack` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_waybill`
--

CREATE TABLE `sma_waybill` (
  `id` int(11) NOT NULL,
  `way_bill_no` varchar(20) DEFAULT NULL,
  `cust_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `items_detail` text,
  `grand_total` varchar(20) DEFAULT NULL,
  `crate_date` datetime DEFAULT NULL,
  `modify_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure for view `sma_vw_emp`
--
DROP TABLE IF EXISTS `sma_vw_emp`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sma_vw_emp`  AS  select `sma_employee`.`id` AS `id`,`sma_employee`.`id` AS `emp_id`,`sma_employee`.`photo` AS `photo`,date_format(`sma_employee`.`joining_date`,'%d/%m/%Y') AS `joining_date`,concat(`sma_employee`.`fname`,' ',`sma_employee`.`mname`,' ',`sma_employee`.`lname`) AS `emp_name`,`sma_employee`.`mobile` AS `mobile`,`sma_companies`.`name` AS `store`,`sma_department`.`name` AS `dept_name`,`sma_position`.`name` AS `desigation`,`sma_employee`.`msd` AS `msd`,`em`.`fname` AS `refered_by`,`sma_employee`.`incentive` AS `incentive`,`sma_employee`.`status` AS `status` from ((((`sma_employee` left join `sma_department` on((`sma_employee`.`department` = `sma_department`.`id`))) left join `sma_companies` on((`sma_employee`.`store` = `sma_companies`.`id`))) left join `sma_position` on((`sma_employee`.`desigation` = `sma_position`.`id`))) left join `sma_employee` `em` on((`em`.`id` = `sma_employee`.`refered_by`))) ;

-- --------------------------------------------------------

--
-- Structure for view `sma_vw_orderlist`
--
DROP TABLE IF EXISTS `sma_vw_orderlist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sma_vw_orderlist`  AS  select `sma_quotes`.`id` AS `id`,`sma_quotes`.`updated_at` AS `order_date`,concat(`sma_quotes`.`customer`,' (',`sma_companies`.`code`,')') AS `sullpier`,`sma_quotes`.`reference_no` AS `order_no`,date_format(`sma_quotes`.`date`,'%d/%m/%Y %H:%i:%s') AS `date`,`sma_quotes`.`biller` AS `store`,`sma_quotes`.`grand_total` AS `grand_total`,concat(`sma_transport`.`transport_name`,' (',`sma_transport`.`code`,')') AS `transport_name`,sum(`sma_quote_items`.`quantity`) AS `totals`,`sma_quotes`.`status` AS `status`,`FN_SECQTYORDER`(`sma_quotes`.`id`) AS `sec_qty`,`sma_users`.`first_name` AS `first_name`,`sma_quotes`.`user_id` AS `user_id`,`sma_quotes`.`warehouse_id` AS `warehouse_id` from ((((`sma_quotes` left join `sma_quote_items` on((`sma_quotes`.`id` = `sma_quote_items`.`quote_id`))) left join `sma_transport` on((`sma_quotes`.`transport` = `sma_transport`.`id`))) left join `sma_companies` on((`sma_quotes`.`customer_id` = `sma_companies`.`id`))) left join `sma_users` on((`sma_users`.`id` = `sma_quotes`.`user_id`))) group by `sma_quotes`.`id` order by `sma_quotes`.`id` desc ;

-- --------------------------------------------------------

--
-- Structure for view `sma_vw_purchase`
--
DROP TABLE IF EXISTS `sma_vw_purchase`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sma_vw_purchase`  AS  select `sma_purchases`.`id` AS `id`,date_format(`sma_purchases`.`updated_at`,'%d/%m/%Y %H:%i:%s') AS `updated_at`,`sma_purchases`.`supplier` AS `supplier`,`sma_purchases`.`reference_no` AS `reference_no`,date_format(`sma_purchases`.`date`,'%d/%m/%Y %H:%i:%s') AS `dates`,`sma_purchases`.`lr_no` AS `lr_no`,ifnull(date_format(`sma_quotes`.`updated_at`,'%d/%m/%Y %H:%i:%s'),'00/00/0000') AS `up_date`,`sma_companies`.`name` AS `comp`,`sma_purchases`.`grand_total` AS `grand_total`,`sma_purchases`.`paid` AS `paid`,(`sma_purchases`.`grand_total` - `sma_purchases`.`paid`) AS `balance`,`sma_purchases`.`payment_status` AS `payment_status`,`sma_purchases`.`pur_challan` AS `pur_challan`,`sma_purchases`.`warehouse_id` AS `warehouse_id`,(select sum(`sma_purchase_items`.`quantity`) AS `sqty` from `sma_purchase_items` where (`sma_purchase_items`.`purchase_id` = `sma_purchases`.`id`)) AS `quantity`,(select sum(`sma_products`.`squantity`) AS `sqty` from (`sma_purchase_items` join `sma_products` on((`sma_products`.`id` = `sma_purchase_items`.`product_id`))) where (`sma_purchase_items`.`purchase_id` = `sma_purchases`.`id`)) AS `sqty`,`sma_purchases`.`created_by` AS `created_by`,`sma_purchases`.`supplier_id` AS `supplier_id` from ((`sma_purchases` left join `sma_companies` on((`sma_companies`.`id` = `sma_purchases`.`store_id`))) left join `sma_quotes` on((`sma_quotes`.`reference_no` = `sma_purchases`.`ag_order_no`))) group by `sma_quotes`.`reference_no` order by `sma_purchases`.`id` desc ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sma_adjustments`
--
ALTER TABLE `sma_adjustments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_advance_receipt`
--
ALTER TABLE `sma_advance_receipt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_advance_receipt_items`
--
ALTER TABLE `sma_advance_receipt_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`,`advance_receipt_id`,`po_number`,`inv_no`,`code`),
  ADD KEY `id_2` (`id`),
  ADD KEY `advance_receipt_id` (`advance_receipt_id`),
  ADD KEY `po_number` (`po_number`),
  ADD KEY `inv_no` (`inv_no`),
  ADD KEY `code` (`code`);

--
-- Indexes for table `sma_attributes`
--
ALTER TABLE `sma_attributes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_bank`
--
ALTER TABLE `sma_bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_brands`
--
ALTER TABLE `sma_brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_calendar`
--
ALTER TABLE `sma_calendar`
  ADD PRIMARY KEY (`date`);

--
-- Indexes for table `sma_captcha`
--
ALTER TABLE `sma_captcha`
  ADD PRIMARY KEY (`captcha_id`),
  ADD KEY `word` (`word`);

--
-- Indexes for table `sma_cashmemo`
--
ALTER TABLE `sma_cashmemo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_categories`
--
ALTER TABLE `sma_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_color`
--
ALTER TABLE `sma_color`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_combo_items`
--
ALTER TABLE `sma_combo_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_companies`
--
ALTER TABLE `sma_companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `group_id_2` (`group_id`);

--
-- Indexes for table `sma_contra_vouchers`
--
ALTER TABLE `sma_contra_vouchers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_costing`
--
ALTER TABLE `sma_costing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_coupon`
--
ALTER TABLE `sma_coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_currencies`
--
ALTER TABLE `sma_currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_customerdetails`
--
ALTER TABLE `sma_customerdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_customer_groups`
--
ALTER TABLE `sma_customer_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_damage_sold_return`
--
ALTER TABLE `sma_damage_sold_return`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_damage_sold_return_items`
--
ALTER TABLE `sma_damage_sold_return_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_id` (`purchase_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sma_date_format`
--
ALTER TABLE `sma_date_format`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_dc`
--
ALTER TABLE `sma_dc`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_deliveries`
--
ALTER TABLE `sma_deliveries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_department`
--
ALTER TABLE `sma_department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_design`
--
ALTER TABLE `sma_design`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_design_code`
--
ALTER TABLE `sma_design_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_employee`
--
ALTER TABLE `sma_employee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_employee_sale_insentive`
--
ALTER TABLE `sma_employee_sale_insentive`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_expenses`
--
ALTER TABLE `sma_expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_fabric`
--
ALTER TABLE `sma_fabric`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_fitting`
--
ALTER TABLE `sma_fitting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_gift_cards`
--
ALTER TABLE `sma_gift_cards`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `card_no` (`card_no`);

--
-- Indexes for table `sma_groups`
--
ALTER TABLE `sma_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_gst`
--
ALTER TABLE `sma_gst`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_hsncodes`
--
ALTER TABLE `sma_hsncodes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_in_stock_damage`
--
ALTER TABLE `sma_in_stock_damage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_in_stock_damage_items`
--
ALTER TABLE `sma_in_stock_damage_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_id` (`purchase_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sma_login_attempts`
--
ALTER TABLE `sma_login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_min_qty_lvl`
--
ALTER TABLE `sma_min_qty_lvl`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_notifications`
--
ALTER TABLE `sma_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_offers`
--
ALTER TABLE `sma_offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_order_ref`
--
ALTER TABLE `sma_order_ref`
  ADD PRIMARY KEY (`ref_id`);

--
-- Indexes for table `sma_parcel_sent_voucher`
--
ALTER TABLE `sma_parcel_sent_voucher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_pattern`
--
ALTER TABLE `sma_pattern`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_payee`
--
ALTER TABLE `sma_payee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_payments`
--
ALTER TABLE `sma_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_payment_vouchers`
--
ALTER TABLE `sma_payment_vouchers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_paypal`
--
ALTER TABLE `sma_paypal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_per`
--
ALTER TABLE `sma_per`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_permissions`
--
ALTER TABLE `sma_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_position`
--
ALTER TABLE `sma_position`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_pos_cash_transfer`
--
ALTER TABLE `sma_pos_cash_transfer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_pos_register`
--
ALTER TABLE `sma_pos_register`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_pos_settings`
--
ALTER TABLE `sma_pos_settings`
  ADD PRIMARY KEY (`pos_id`);

--
-- Indexes for table `sma_products`
--
ALTER TABLE `sma_products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`),
  ADD KEY `category_id_2` (`category_id`);

--
-- Indexes for table `sma_product_box`
--
ALTER TABLE `sma_product_box`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_product_challan`
--
ALTER TABLE `sma_product_challan`
  ADD PRIMARY KEY (`challan_no`);

--
-- Indexes for table `sma_product_challan_items`
--
ALTER TABLE `sma_product_challan_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_product_discount`
--
ALTER TABLE `sma_product_discount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_product_items`
--
ALTER TABLE `sma_product_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_product_margin`
--
ALTER TABLE `sma_product_margin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_product_photos`
--
ALTER TABLE `sma_product_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_product_received_voucher`
--
ALTER TABLE `sma_product_received_voucher`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_product_seasonal_discount`
--
ALTER TABLE `sma_product_seasonal_discount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_product_special_discount`
--
ALTER TABLE `sma_product_special_discount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_product_variants`
--
ALTER TABLE `sma_product_variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_purchases`
--
ALTER TABLE `sma_purchases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_purchases_return`
--
ALTER TABLE `sma_purchases_return`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_purchases_return_without_barcode`
--
ALTER TABLE `sma_purchases_return_without_barcode`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_purchase_items`
--
ALTER TABLE `sma_purchase_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_id` (`purchase_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sma_purchase_return_items`
--
ALTER TABLE `sma_purchase_return_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_id` (`purchase_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sma_purchase_return_items_without_barcode`
--
ALTER TABLE `sma_purchase_return_items_without_barcode`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_id` (`purchase_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sma_purchase_return_product_without_barcode`
--
ALTER TABLE `sma_purchase_return_product_without_barcode`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`),
  ADD KEY `category_id_2` (`category_id`);

--
-- Indexes for table `sma_quotes`
--
ALTER TABLE `sma_quotes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_quotes_products`
--
ALTER TABLE `sma_quotes_products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`),
  ADD KEY `category_id_2` (`category_id`);

--
-- Indexes for table `sma_quote_items`
--
ALTER TABLE `sma_quote_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quote_id` (`quote_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sma_receiptmodule`
--
ALTER TABLE `sma_receiptmodule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_receiptmodule_product`
--
ALTER TABLE `sma_receiptmodule_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_receipt_vouchers`
--
ALTER TABLE `sma_receipt_vouchers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_recive_payments`
--
ALTER TABLE `sma_recive_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_retailbill`
--
ALTER TABLE `sma_retailbill`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_returnp_items`
--
ALTER TABLE `sma_returnp_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sale_id` (`purchase_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `product_id_2` (`product_id`,`purchase_id`),
  ADD KEY `sale_id_2` (`purchase_id`,`product_id`);

--
-- Indexes for table `sma_return_items`
--
ALTER TABLE `sma_return_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_return_purchase`
--
ALTER TABLE `sma_return_purchase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_return_sales`
--
ALTER TABLE `sma_return_sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_sales`
--
ALTER TABLE `sma_sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_sale_incentive`
--
ALTER TABLE `sma_sale_incentive`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_sale_items`
--
ALTER TABLE `sma_sale_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_section`
--
ALTER TABLE `sma_section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_sessions`
--
ALTER TABLE `sma_sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_settings`
--
ALTER TABLE `sma_settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `sma_size`
--
ALTER TABLE `sma_size`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_skrill`
--
ALTER TABLE `sma_skrill`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_stock`
--
ALTER TABLE `sma_stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_style`
--
ALTER TABLE `sma_style`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_subcategories`
--
ALTER TABLE `sma_subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_sup_damage`
--
ALTER TABLE `sma_sup_damage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_sup_damage_items`
--
ALTER TABLE `sma_sup_damage_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_suspended_bills`
--
ALTER TABLE `sma_suspended_bills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_suspended_items`
--
ALTER TABLE `sma_suspended_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_tax_invoice`
--
ALTER TABLE `sma_tax_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_tax_rates`
--
ALTER TABLE `sma_tax_rates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_transfers`
--
ALTER TABLE `sma_transfers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_transfer_items`
--
ALTER TABLE `sma_transfer_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_transport`
--
ALTER TABLE `sma_transport`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_type`
--
ALTER TABLE `sma_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_unit`
--
ALTER TABLE `sma_unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_unlabelled_damage_return`
--
ALTER TABLE `sma_unlabelled_damage_return`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_unlabelled_damage_return_items`
--
ALTER TABLE `sma_unlabelled_damage_return_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_unlabelled_products`
--
ALTER TABLE `sma_unlabelled_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_users`
--
ALTER TABLE `sma_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_user_logins`
--
ALTER TABLE `sma_user_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_variants`
--
ALTER TABLE `sma_variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_warehouses`
--
ALTER TABLE `sma_warehouses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_warehouses_products`
--
ALTER TABLE `sma_warehouses_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_warehouses_products_variants`
--
ALTER TABLE `sma_warehouses_products_variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_waybill`
--
ALTER TABLE `sma_waybill`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sma_adjustments`
--
ALTER TABLE `sma_adjustments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
