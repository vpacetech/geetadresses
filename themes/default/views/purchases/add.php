<script type="text/javascript">

<?php
if ($this->session->userdata('remove_pols')) {
    ?>
        if (localStorage.getItem('poitems')) {
        localStorage.removeItem('poitems');
        }
        if (localStorage.getItem('podiscount')) {
        localStorage.removeItem('podiscount');
        }
        if (localStorage.getItem('potax2')) {
        localStorage.removeItem('potax2');
        }
        if (localStorage.getItem('poshipping')) {
        localStorage.removeItem('poshipping');
        }
        if (localStorage.getItem('poref')) {
        localStorage.removeItem('poref');
        }
        if (localStorage.getItem('powarehouse')) {
        localStorage.removeItem('powarehouse');
        }
        if (localStorage.getItem('ponote')) {
        localStorage.removeItem('ponote');
        }
        if (localStorage.getItem('posupplier')) {
        localStorage.removeItem('posupplier');
        }
        if (localStorage.getItem('pocurrency')) {
        localStorage.removeItem('pocurrency');
        }
        if (localStorage.getItem('poextras')) {
        localStorage.removeItem('poextras');
        }
        if (localStorage.getItem('podate')) {
        localStorage.removeItem('podate');
        }
        if (localStorage.getItem('postatus')) {
        localStorage.removeItem('postatus');
        }
    <?php
    $this->sma->unset_data('remove_pols');
}
?>
<?php if ($quote_id) {
    ?>

        localStorage.setItem('powarehouse', '<?= $quote->warehouse_id ?>');
        localStorage.setItem('ponote', '<?= str_replace(array("\r", "\n"), "", $this->sma->decode_html($quote->note)); ?>');
        localStorage.setItem('podiscount', '<?= $quote->order_discount_id ?>');
        localStorage.setItem('potax2', '<?= $quote->order_tax_id ?>');
        localStorage.setItem('poshipping', '<?= $quote->shipping ?>');
        localStorage.setItem('poitems', JSON.stringify(<?= $quote_items; ?>));
        //        loadItems();
        $(document).ready(function () {
        //        loadItems();
        localStorage.setItem('ag_orderid', '<?= $quote->reference_no ?>');
        $('#store').trigger('change');
        $('#list_suppliers').val('<?= $quote->customer_id ?>');
        $('#list_suppliers').trigger('change');
        $('#reference_no').trigger('change');
        });
<?php } ?>

    var count = 1, an = 1, product_variant = 0, DT = <?= $Settings->default_tax_rate ?>, DC = '<?= $default_currency->code ?>', shipping = 0,
            product_tax = 0, invoice_tax = 0, total_discount = 0, total = 0,
            tax_rates = <?php echo json_encode($tax_rates); ?>, poitems = {},
            audio_success = new Audio('<?= $assets ?>sounds/sound2.mp3'),
            audio_error = new Audio('<?= $assets ?>sounds/sound3.mp3');
    $(document).ready(function () {
// copied from purchases.js as it is not working there because of some js error 30 aug 2017
    if (localStorage.getItem('poextras')) {
    $('#extras').iCheck('check');
    $('#extras-con').show();
    }
    $('#extras').on('ifChecked', function () {
    localStorage.setItem('poextras', 1);
    $('#extras-con').slideDown();
    });
    $('#extras').on('ifUnchecked', function () {
    localStorage.removeItem("poextras");
    $('#extras-con').slideUp();
    });
    var old_podiscount;
    $('#podiscount').focus(function () {
    old_podiscount = $(this).val();
    }).change(function () {
    if (is_valid_discount($(this).val())) {
    localStorage.removeItem('podiscount');
    localStorage.setItem('podiscount', $(this).val());
    loadItems();
    return;
    } else {
    $(this).val(old_podiscount);
    bootbox.alert(lang.unexpected_value);
    return;
    }

    });
    var old_packingcharges;
    $('#packingcharges').focus(function () {
    old_packingcharges = $(this).val();
    }).change(function () {
    if (is_valid_discount($(this).val())) {
    localStorage.removeItem('packingcharges');
    localStorage.setItem('packingcharges', $(this).val());
    loadItems();
    return;
    } else {
    $(this).val(old_packingcharges);
    bootbox.alert(lang.unexpected_value);
    return;
    }
    });
// till this.



<?php if ($this->input->get('text')) { ?>
        if (!localStorage.getItem('poitems')) {
        localStorage.setItem('posupplier', <?= $this->input->get('text'); ?>);
        }
<?php } ?>
<?php if ($Owner || $Admin) { ?>
        if (!localStorage.getItem('podate')) {
        $("#podate").datetimepicker({
        format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0
        }).datetimepicker('update', new Date());
        }
        $(document).on('change', '#podate', function (e) {
        localStorage.setItem('podate', $(this).val());
        });
        if (podate = localStorage.getItem('podate')) {
        $('#podate').val(podate);
        }
<?php } ?>
    if (!localStorage.getItem('potax2')) {
    localStorage.setItem('potax2', <?= $Settings->default_tax_rate2; ?>);
    setTimeout(function () {
    $('#extras').iCheck('check');
    }, 1000);
    }
    ItemnTotals();
    $("#add_item").autocomplete({
    source: '<?= site_url('purchases/suggestions'); ?>',
            minLength: 1,
            autoFocus: false,
            delay: 200,
            response: function (event, ui) {
            if ($(this).val().length >= 16 && ui.content[0].id == 0) {
            //audio_error.play();
            bootbox.alert('<?= lang('no_match_found') ?>', function () {
            $('#add_item').focus();
            });
            $(this).removeClass('ui-autocomplete-loading');
            $(this).val('');
            } else if (ui.content.length == 1 && ui.content[0].id != 0) {
            ui.item = ui.content[0];
            $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
            $(this).autocomplete('close');
            $(this).removeClass('ui-autocomplete-loading');
            } else if (ui.content.length == 1 && ui.content[0].id == 0) {
            //audio_error.play();
            bootbox.alert('<?= lang('no_match_found') ?>', function () {
            $('#add_item').focus();
            });
            $(this).removeClass('ui-autocomplete-loading');
            $(this).val('');
            }
            },
            select: function (event, ui) {
            event.preventDefault();
            if (ui.item.id !== 0) {
            var row = add_purchase_item(ui.item);
            if (row)
                    $(this).val('');
            } else {
            //audio_error.play();
            bootbox.alert('<?= lang('no_match_found') ?>');
            }
            }
    });
    $('#add_item').bind('keypress', function (e) {
    if (e.keyCode == 13) {
    e.preventDefault();
    $(this).autocomplete("search");
    }
    });
//        $(document).on('click', '#addItemManually', function (e) {
//            if (!$('#mcode').val()) {
//                $('#mError').text('<?= lang('product_code_is_required') ?>');
//                $('#mError-con').show();
//                return false;
//            }
//            if (!$('#mname').val()) {
//                $('#mError').text('<?= lang('product_name_is_required') ?>');
//                $('#mError-con').show();
//                return false;
//            }
//            if (!$('#mcategory').val()) {
//                $('#mError').text('<?= lang('product_category_is_required') ?>');
//                $('#mError-con').show();
//                return false;
//            }
//            if (!$('#munit').val()) {
//                $('#mError').text('<?= lang('product_unit_is_required') ?>');
//                $('#mError-con').show();
//                return false;
//            }
//            if (!$('#mcost').val()) {
//                $('#mError').text('<?= lang('product_cost_is_required') ?>');
//                $('#mError-con').show();
//                return false;
//            }
//            if (!$('#mprice').val()) {
//                $('#mError').text('<?= lang('product_price_is_required') ?>');
//                $('#mError-con').show();
//                return false;
//            }
//
//            var msg, row = null, product = {
//                type: 'standard',
//                code: $('#mcode').val(),
//                name: $('#mname').val(),
//                tax_rate: $('#mtax').val(),
//                tax_method: $('#mtax_method').val(),
//                category_id: $('#mcategory').val(),
//                unit: $('#munit').val(),
//                cost: $('#mcost').val(),
//                price: $('#mprice').val()
//            };
//
//            $.ajax({
//                type: "get", async: false,
//                url: site.base_url + "products/addByAjax",
//                data: {token: "<?= $csrf; ?>", product: product},
//                dataType: "json",
//                success: function (data) {
//                    if (data.msg == 'success') {
//                        row = add_purchase_item(data.result);
//                    } else {
//                        msg = data.msg;
//                    }
//                }
//            });
//            if (row) {
//                $('#mModal').modal('hide');
//                //audio_success.play();
//            } else {
//                $('#mError').text(msg);
//                $('#mError-con').show();
//            }
//            return false;
//        });
    });</script>
<div class="box" ng-controller="purchaseCode">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?php
            if ($v == 'add_challan') {
                echo lang('add_purchasechallan');
            } else {
                echo lang('add_purchase');
            }
            ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?php echo lang('enter_info'); ?></p>
                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form');
                if ($v == 'add_challan') {
                    echo form_open_multipart("purchases/add_challan?" . http_build_query($_GET), $attrib);
                } else {
                    echo form_open_multipart("purchases/add", $attrib);
                }
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("Store", "store") ?> <label></label>
                                <?php
                                $bl[""] = "";
                                foreach ($billers as $biller) {
                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->company;
                                }
                                echo form_dropdown('store_id', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $quote->biller_id), 'id="store" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("biller") . '" required="required" class="form-control" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("supplier", "posupplier"); ?>
                                <?php
                                $wh[''] = '';
                                foreach ($suppliers as $sup) {
                                    $wh[$sup->id] = $sup->code;
                                }
                                echo form_dropdown('supplier', "", (isset($_POST['supplier']) ? $_POST['supplier'] : $quote->customer_id), 'id="list_suppliers" class="form-control input-tip select get_supplier_bill_no" data-placeholder="' . lang("select") . ' ' . lang("supplier") . '" required="required" style="width:100%;" '); //get_lr_no get_against_order_sup get_supplier_bill_no
                                ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("supplier_date", "podate"); ?>
                                <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="podate" required="required"'); ?>
                            </div>
                        </div>
                        <?php //}         ?>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("supplier_bill_no", "poref"); ?>
                                <?php //echo form_input('reference_no', (isset($_POST['reference_no']) ? $_POST['reference_no'] : ""), 'class="form-control input-tip" id="poref"'); ?>
                                <?php echo form_dropdown('reference_no', '', (isset($_POST['reference_no']) ? $_POST['reference_no'] : ''), 'id="poref" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("purchase_bill_no") . '" required="required" class="form-control get_ag_order_from_bill_no" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("purchase_no", "poref"); ?>
                                <?php echo form_input('purchase_no', $latest_pur, 'class="form-control input-tip" id="purchase_no" readonly'); ?>
                            </div>
                        </div>
                        <!--<div class="col-md-4">
                            <div class="form-group">
                                <?php // lang("warehouse", "powarehouse"); ?>
                                <?php
                                // $whe[''] = '';
                                // foreach ($warehouses as $warehouse) {
                                //     $whe[$warehouse->id] = $warehouse->name;
                                // }
                                // echo "<pre>"; print_r($whe);exit;
                                //echo form_dropdown('warehouse', $whe, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $Settings->default_warehouse), 'id="powarehouse" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("warehouse") . '" required="required" style="width:100%;" ');
                                ?>
                            </div>
                        </div>-->
                        <?php
                                // $whe[''] = '';
                                foreach ($warehouses as $warehouse) {
                                    $whe[$warehouse->id] = $warehouse->name;
                                }
                                // echo form_dropdown('warehouse', $whe, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $Settings->default_warehouse), 'id="powarehouse" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("warehouse") . '" required="required" style="width:100%;" ');
                                ?>
                                <input type="hidden" name="warehouse" value="<?= $whe[$warehouse->id]?>"/>

                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("document", "document") ?>
                                <input id="document" type="file" name="document" data-show-upload="false"
                                       data-show-preview="false" class="form-control file">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("lr_no", "lr_no") ?>
                                <?php
                                $lrno[""] = "";
                                foreach ($lr_no as $lr_n) {
                                    $lrno[$lr_n->lrno] = $lr_n->lrno;
                                }
                                echo form_dropdown('lr_no[]', '', (isset($_POST['lr_no']) ? $_POST['lr_no'] : ''), 'id="lr_no" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("lr_no") . '" multiple required="required" class="form-control" style="width:100%;"');
                                ?>
                            </div>
                        </div>

                        <!-- <div class="row">
                            
                            <div class="col-md-8  colorassorted hide">
                                <div class="row">
                                    <?php
                                    echo form_dropdown('lr_nos[]', [""=>"Select LR.No"], (isset($_POST['lr_no']) ? $_POST['lr_no'] : ($product ? $product->colorassorted : '')), 'class="form-control" multiple select2 id="mullr_nos" ng-model="prod.colorassorted" placeholder="' . lang("select") . " " . lang("color") . '"style="width:100%"');
                                    ?>
                                </div>
                            </div>
                        </div> -->
                        
                        <div class="col-md-4">

                            <div class="form-group">
                                <?= lang("ag_order_no", "ag_order_no") ?>
                                <?php
                                $ag_ord[""] = "";
                                if (!empty($ag_ord_no)) {
                                    foreach ($ag_ord_no as $ag_ord_no) {
                                        $ag_ord[$ag_ord_no->reference_no] = $ag_ord_no->reference_no;
                                    }
                                }
                                echo form_dropdown('ag_order_no[]', '', (isset($_POST['reference_no']) ? $_POST['reference_no'] : ''), 'id="reference_no" placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("reference_no") . '" multiple required="required" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("sorted_by", "sorted_by"); ?>
                                <?php
                                $us[""] = "Select Sorted by";
                                foreach ($user as $usr) {
                                    $us[$usr->id] = $usr->first_name != '-' ? $usr->first_name . ' ' . $usr->last_name : $usr->first_name . ' ' . $usr->last_name;
                                }

                                echo form_dropdown('sorted_by', $us, (isset($_POST['first_name']) ? $_POST['first_name'] : ''), 'id="sorted_by" placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("sorted_by") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("labeler", "labeler"); ?>
                                <?php
                                $us[""] = [];
                                foreach ($user as $usr) {
                                    $us[$usr->id] = $usr->first_name != '-' ? $usr->first_name . ' ' . $usr->last_name : $usr->first_name . ' ' . $usr->last_name;
                                }
                                echo form_dropdown('labeler', $us, (isset($_POST['first_name']) ? $_POST['first_name'] : ''), ' multiple id="labelled_by" placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("labelled_by") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-4 hidden">
                            <div class="form-group">
                                <?= lang("status", "postatus"); ?>
                                <?php
                                $post = array('received' => lang('received'), 'pending' => lang('pending'), 'ordered' => lang('ordered'));
                                echo form_dropdown('status', $post, (isset($_POST['status']) ? $_POST['status'] : ''), 'id="postatus" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("status") . '" required="required" style="width:100%;" ');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-12" id="sticker">
                            <div class="well well-sm">
                                <div class="form-group" style="margin-bottom:0;">
                                    <div class="input-group wide-tip">
                                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                            <i class="fa fa-2x fa-barcode addIcon"></i></a></div>
                                        <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . $this->lang->line("add_product_to_order") . '"'); ?>
                                        <?php if ($Owner || $Admin || $GP['products-add']) { ?>
                                            <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                <a data-toggle="modal" data-target="#mModal" id="addManually" class="tip" title="<?= lang('add_product_manually') ?>">
                                                    <i class="fa fa-2x fa-plus-circle addIcon" id="addIcon"></i></a></div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="control-group table-group">
                                <label class="table-label"><?= lang("order_items"); ?></label>
                                <div class="controls table-controls table-responsive" style="overflow: auto;">
                                    <table id="poTable" class="table items table-striped table-bordered table-condensed table-hover">
                                        <thead>
                                            <tr>
                                                <th class="col-md-4"><?= lang("product_name") . " (" . $this->lang->line("product_code") . ")"; ?></th>

                                                <th class="col-md-1"><?= lang("product_cost"); ?></th>
                                                <th class="col-md-1"><?= lang("primary_quantity"); ?></th>
                                                <th class="col-md-1"><?= lang("secondary_quantity"); ?></th>
                                                <th class="col-md-1"><?= lang("brand"); ?></th>
                                                <th class="col-md-1"><?= lang("design"); ?></th>
                                                <th class="col-md-1"><?= lang("style"); ?></th>
                                                <th class="col-md-1"><?= lang("pattern"); ?></th>
                                                <th class="col-md-1"><?= lang("fitting"); ?></th>
                                                <th class="col-md-1"><?= lang("frabic"); ?></th>
                                                <th class="col-md-1"><?= lang("color"); ?></th>
                                                <th class="col-md-1"><?= lang("mrp"); ?></th>

                                                <th><?= lang("subtotal"); ?> (<span
                                                        class="currency"><?= $default_currency->code ?></span>)
                                                </th>
                                                <th style="width: 30px !important; text-align: center;"><i
                                                        class="fa fa-trash-o"
                                                        style="opacity:0.5; filter:alpha(opacity=50);"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody style="overflow: auto; max-height: 200px"></tbody>
                                        <tfoot></tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <input type="hidden" name="total_items" value="" id="total_items" required="required"/>

                        <div class="col-md-12">
                            <div class="form-group">
                                <input type="checkbox" class="checkbox" id="extras" value=""/>
                                <label for="extras" class="padding05"><?= lang('more_options') ?></label>
                            </div>
                            <div class="row" id="extras-con" style="display: none;">
                                <?php if ($Settings->tax1) { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang('order_tax', 'potax2') ?>
                                            <?php
                                            $tr[""] = "";
                                            foreach ($tax_rates as $tax) {
                                                $tr[$tax->id] = $tax->name;
                                            }
                                            echo form_dropdown('order_tax', $tr, "", 'id="potax2" class="form-control input-tip select" style="width:100%;"');
                                            ?>
                                        </div>
                                    </div>
                                <?php } ?>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("Supplier_Discount", "podiscount"); ?> <label>*</label>
                                        <?php echo form_input('discount', '', 'class="form-control input-tip" id="podiscount" ng-model="pur.discount"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("packingcharges", "packingcharges"); ?>
                                        <?php echo form_input('packingcharges', '', 'class="form-control input-tip" id="packingcharges" ng-model="pur.packingcharges"'); ?>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <!--                                    <div class="form-group" style="margin-bottom:5px;">
                                    <?= lang("shipping", "poshipping"); ?>
                                    <?php echo form_input('shipping', '', 'class="form-control input-tip" id="poshipping"'); ?>
                                    
                                                                        </div>-->
                                </div>
                            </div>
                            <div id="bottom-total" class="well well-sm" style="margin-bottom: 0;">
                                <table class="table table-bordered table-condensed totals gstdetails" style="margin-bottom:0;">

                                </table>

                                <?php
                                if ($v != 'add_challan') {
                                    ?>
                                    <table class="table table-bordered table-condensed totals" style="margin-bottom:0;">
                                        <tr class="warning">

                                            <td colspan="">Taxable Value <span class="totals_val pull-right" id="taxabel">0</span></td>
                                            <td>CGST <span class="totals_val pull-right" id="tcgst">0</span></td>
                                            <td>SGST <span class="totals_val pull-right" id="tsgst">0</span></td>
                                        </tr>
                                    </table>
                                <?php } ?>

                                <table class="table table-bordered table-condensed totals" style="margin-bottom:0;">
                                    <tr class="warning">
                                        <td><?= lang('items') ?> <span class="totals_val pull-right" id="titems">0</span></td>
                                        <td><?= lang('total') ?> <span class="totals_val pull-right" id="total">0.00</span></td>
                                        <td><?= lang('discount') ?> <span class="totals_val pull-right" id="tds">0.00</span></td>
                                        <?php if ($Settings->tax2) { ?>
                                                                                                                                                                                                                <!--<td><?= lang('order_tax') ?> <span class="totals_val pull-right" id="ttax2">0.00</span></td>-->
                                        <?php } ?>
                    <!--<td><?= lang('shipping') ?> <span class="totals_val pull-right" id="tship">0.00</span></td>-->
                                        <td><?= lang('grand_total') ?> <span class="totals_val pull-right" id="gtotal">0.00</span></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <?= lang("note", "ponote"); ?>
                                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="ponote" style="margin-top: 10px; height: 100px;"'); ?>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <div
                                class="from-group"><?php echo form_submit('add_pruchase', $this->lang->line("submit"), 'id="add_pruchase" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                                <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></button>
                            </div>
                        </div>
                    </div>
                </div>


                <?php echo form_close(); ?>

            </div>

        </div>
    </div>
</div>

<div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?= lang('close'); ?></span></button>
                <h4 class="modal-title" id="prModalLabel"></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= lang('product_tax') ?></label>
                            <div class="col-sm-8">
                                <?php
                                $tr[""] = "";
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('ptax', $tr, "", 'id="ptax" class="form-control pos-input-tip" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="pquantity" class="col-sm-4 control-label"><?= lang('quantity') ?></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pquantity">
                        </div>
                    </div>
                    <?php if ($Settings->product_expiry) { ?>
                        <div class="form-group">
                            <label for="pexpiry" class="col-sm-4 control-label"><?= lang('product_expiry') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control date" id="pexpiry">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="poption" class="col-sm-4 control-label"><?= lang('product_option') ?></label>

                        <div class="col-sm-8">
                            <div id="poptions-div"></div>
                        </div>
                    </div>
                    <?php if ($Settings->product_discount) { ?>
                        <div class="form-group">
                            <label for="pdiscount"
                                   class="col-sm-4 control-label"><?= lang('product_discount') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pdiscount">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="pcost" class="col-sm-4 control-label"><?= lang('unit_cost') ?></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pcost">
                        </div>
                    </div>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th style="width:25%;"><?= lang('net_unit_cost'); ?></th>
                            <th style="width:25%;"><span id="net_cost"></span></th>
                            <th style="width:25%;"><?= lang('product_tax'); ?></th>
                            <th style="width:25%;"><span id="pro_tax"></span></th>
                        </tr>
                    </table>
                    <input type="hidden" id="punit_cost" value=""/>
                    <input type="hidden" id="old_tax" value=""/>
                    <input type="hidden" id="old_qty" value=""/>
                    <input type="hidden" id="old_cost" value=""/>
                    <input type="hidden" id="row_id" value=""/>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="editItem"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>


<script>
//    $('#store').change(function () {
//        var v = $(this).val();
//        $('#modal-loading').show();
//        if (v) {
//            $.ajax({
//                type: "get",
//                async: false,
//                data: {store: v},
//                url: "<?= site_url('purchases/getBiller') ?>",
//                dataType: "json",
//                success: function (scdata) {
//                    $('#list_suppliers').html('');
//                    $('#list_suppliers').removeAttr('readonly', '');
////                    $('#list_suppliers').append('<option value="">Please Select Supplier Code</option>');
//                    $.each(scdata, function (k, val) {
//                        var opt = $('<option />');
//                        opt.val(val.id);
//                        opt.text(val.code + ' - ' + val.company);
//                        $('#list_suppliers').append(opt);
//                    })
//                    $('#modal-loading').hide();
//                },
//                error: function () {
//                    $('#list_suppliers').attr('readonly', '');
//                    $('#modal-loading').hide();
//                }
//            });
//        }
//    });

//    $('#reference_no').change(function () {
//        var v = $(this).val();
//        $('#modal-loading').show();
//        if (v) {
//            $.ajax({
//                type: "get",
//                async: false,
//                data: {ref: v},
//                url: "<?= site_url('purchases/getOrderedItems') ?>",
//                dataType: "json",
//                success: function (scdata) {
//                    if (scdata) {
//                        var row = add_purchase_item(scdata);
//                        if (row)
//                            $(this).val('');
//                    } else {
//                        //audio_error.play();
//                        bootbox.alert('<?= lang('no_match_found') ?>');
//                    }
//                },
//                error: function () {
//                    $('#list_suppliers').attr('readonly', '');
//                    $('#modal-loading').hide();
//                }
//            });
//        }
//    });


</script>







<?php
if (!empty($variants)) {
    foreach ($variants as $variant) {
        $vars[] = addslashes($variant->name);
    }
} else {
    $vars = array();
}

$department[''] = "";
$product_items[''] = "";
$section[''] = "";
$type[''] = "";
$brands[''] = "";
$design[''] = "";
$style[''] = "";
$pattern[''] = "";
$fitting[''] = "";
$fabric[''] = "";
$color[''] = "";
$size[''] = "";
$sizes = array();
$per[''] = "";
foreach ($product_para as $k => $v) {
    if ($k == "department" && !empty($v)) {
        foreach ($v as $d) {
            $department[$d->id] = $d->name;
        }
    }
    if ($k == "product_items" && !empty($v)) {
        foreach ($v as $d) {
            $product_items[$d->id] = $d->name;
        }
    }
    if ($k == "section" && !empty($v)) {
        foreach ($v as $d) {
            $section[$d->id] = $d->name;
        }
    }
    if ($k == "type" && !empty($v)) {
        foreach ($v as $d) {
            $type[$d->id] = $d->name;
        }
    }
    if ($k == "brands" && !empty($v)) {
        foreach ($v as $d) {
            $brands[$d->id] = $d->name;
        }
    }
    if ($k == "design" && !empty($v)) {
        foreach ($v as $d) {
            $design[$d->id] = $d->name;
        }
    }
    if ($k == "style" && !empty($v)) {
        foreach ($v as $d) {
            $style[$d->id] = $d->name;
        }
    }
    if ($k == "pattern" && !empty($v)) {
        foreach ($v as $d) {
            $pattern[$d->id] = $d->name;
        }
    }
    if ($k == "fitting" && !empty($v)) {
        foreach ($v as $d) {
            $fitting[$d->id] = $d->name;
        }
    }
    if ($k == "fabric" && !empty($v)) {
        foreach ($v as $d) {
            $fabric[$d->id] = $d->name;
        }
    }
    if ($k == "color" && !empty($v)) {
        foreach ($v as $d) {
            $color[$d->id] = $d->name;
        }
    }
    if ($k == "size" && !empty($v)) {
        foreach ($v as $d) {
            $size[$d->id] = $d->name;
            $sizes[$d->id] = $d;
//            array_push($sizes, $d);
        }
    }
    if ($k == "per" && !empty($v)) {
        foreach ($v as $d) {
            $per[$d->id] = $d->name;
        }
    }
}
?>




<style>
    @media(min-width: 992px) {.modal-lg {width: 80% !important;}}
    .err{
        border-color: red;
    }
</style>


<div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?= lang('close'); ?></span></button>
                <h4 class="modal-title" id="prModalLabel"></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= lang('product_tax') ?></label>
                            <div class="col-sm-8">
                                <?php
                                $tr[""] = "";
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('ptax', $tr, "", 'id="ptax" class="form-control pos-input-tip" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <input type="text" name="from_order" value="from_order" class="form-control hidden"/>
                    <div class="form-group">
                        <label for="pquantity" class="col-sm-4 control-label"><?= lang('quantity') ?></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pquantity">
                        </div>
                    </div>
                    <?php if ($Settings->product_expiry) { ?>
                        <div class="form-group">
                            <label for="pexpiry" class="col-sm-4 control-label"><?= lang('product_expiry') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control date" id="pexpiry">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="poption" class="col-sm-4 control-label"><?= lang('product_option') ?></label>

                        <div class="col-sm-8">
                            <div id="poptions-div"></div>
                        </div>
                    </div>
                    <?php if ($Settings->product_discount) { ?>
                        <div class="form-group">
                            <label for="pdiscount"
                                   class="col-sm-4 control-label"><?= lang('product_discount') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pdiscount">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="pcost" class="col-sm-4 control-label"><?= lang('unit_cost') ?></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pcost">
                        </div>
                    </div>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th style="width:25%;"><?= lang('net_unit_cost'); ?></th>
                            <th style="width:25%;"><span id="net_cost"></span></th>
                            <th style="width:25%;"><?= lang('product_tax'); ?></th>
                            <th style="width:25%;"><span id="pro_tax"></span></th>
                        </tr>
                    </table>
                    <input type="hidden" id="punit_cost" value=""/>
                    <input type="hidden" id="old_tax" value=""/>
                    <input type="hidden" id="old_qty" value=""/>
                    <input type="hidden" id="old_cost" value=""/>
                    <input type="hidden" id="row_id" value=""/>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="editItem"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>



<style>
    @media(min-width: 992px) {.modal-lg {width: 80% !important;}}
    .err{
        border-color: red;
    }
</style>

<div class="modal" id="mModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?= lang('close'); ?></span></button>
                <h4 class="modal-title" id="mModalLabel"><?= lang('add_product') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <div class="box" ng-controller="addProducts">
                    <div class="box-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php
//                                $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'addproduct');
//                                echo form_open_multipart("products/addPurchaseProducts", $attrib)
                                ?>
                                <form method="post" id="addproduct" data-toggle="validator" role="form">
                                    <input type="text" name="from_order" value="from_order" class="form-control hidden"/>


                                    <div class="col-md-5">
                                        <div class="form-group hidden">
                                            <?= lang("product_type", "type1") ?>
                                            <?php
                                            if ($product_id != "") {
                                                $opts = array('standard' => lang('standard'), 'combo' => lang('combo'), 'bundle' => lang('bundle'));
                                            } else {
                                                $opts = array('standard' => lang('standard'));
                                            }
                                            echo form_dropdown('type', $opts, (isset($_POST['type']) ? $_POST['type'] : ($product ? $product->type : 'standard')), 'class="form-control" id="type" required="required" ng-model="prod.protype" ng-change="getProdBarcode()"');
                                            ?>
                                        </div>
                                        <div class="form-group all">
                                            <?= lang("Store", "companies") ?>
                                            <div class="input-group col-md-12">

                                                <?php echo form_input('store_id', (isset($_POST['store_id']) ? $_POST['store_id'] : ($product ? $product->store_id : '')), 'class="form-control" default-attrib data-tab="companies" data-id="0" id="companies" placeholder="' . lang("select") . " " . lang("store") . '" required="required" style="width:100%" ng-model="prod.store_id" ng-change="getProdBarcode();getGstDetails();"'); ?>
                                                <div class="input-group-addon no-print">
                                                    <a href="<?php echo site_url('billers/add'); ?>" data-toggle="modal" data-target="#myModal" class="external">
                                                        <i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group all">
                                            <?= lang("Department", "departmentpur") ?> *
                                            <div class="input-group col-md-12">
                                                <?php
                                                echo form_input('department', (isset($_POST['department']) ? $_POST['department'] : ($product ? $product->department : '')), 'class="form-control" default-attrib data-tab="department" data-key="store_id" data-id="companies" id="department" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ng-model="prod.dept" ng-change="getProdName();getProdBarcode();getProductMargin();getGstDetails();"');
                                                ?>
                                                <div class="input-group-addon no-print">
                                                    <a href="<?php echo site_url('system_settings/AddProduct_para/department'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group all">
                                            <?= lang("Section", "sectionpur") ?> *
                                            <div class="input-group col-md-12">
                                                <?php
                                                echo form_input('section', (isset($_POST['section']) ? $_POST['section'] : ($product ? $product->section : '')), 'class="form-control" default-attrib data-tab="section" id="section" data-key="department_id" data-id="department" placeholder="' . lang("select") . " " . lang("section") . '" ng-model="prod.section_id" ng-change="getProductMargin();" required="required" style="width:100%"');
                                                ?>
                                                <div class="input-group-addon no-print">
                                                    <a href="<?php echo site_url('system_settings/AddProduct_para/section'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group all">
                                            <?= lang("Product_items", "product_itemspur") ?> *
                                            <div class="input-group col-md-12">
                                                <?php
                                                echo form_input('product_items', (isset($_POST['product_items']) ? $_POST['product_items'] : ($product ? $product->product_items : '')), 'class="form-control" default-attrib data-tab="product_items" data-key="section_id" data-id="section" id="product_items" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%" ng-model="prod.product_item" ng-change="getProdName();getProductMargin();getGstDetails();"');
                                                ?>
                                                <div class="input-group-addon no-print">
                                                    <a href="<?php echo site_url('system_settings/AddProduct_para/product_items'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group all">
                                            <?= lang("Type", "typeput") ?> *
                                            <div class="input-group col-md-12">
                                                <?php
                                                echo form_input('type_id', (isset($_POST['type_id']) ? $_POST['type_id'] : ($product ? $product->type_id : '')), 'class="form-control" default-attrib data-tab="type" id="type_id" data-key="product_items_id" data-id="product_items" placeholder="' . lang("select") . " " . lang("type") . '" required="required" style="width:100%" ng-model="prod.type" ng-change="getProdName();getProductMargin();getGstDetails();"');
                                                ?>
                                                <div class="input-group-addon no-print">
                                                    <a href="<?php echo site_url('system_settings/AddProduct_para/type'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group all">
                                            <?= lang("Brands", "brandspur") ?> *
                                            <div class="input-group col-md-12">
                                                <?php
                                                echo form_input('brands', (isset($_POST['brands']) ? $_POST['brands'] : ($product ? $product->brands : '')), 'class="form-control" default-attrib data-tab="brands" id="brands" data-key="type_id" data-id="type_id" placeholder="' . lang("select") . " " . lang("brands") . '" required="required" ng-model="prod.brands_id" ng-change="getProductMargin();getGstDetails();"  style="width:100%"');
                                                ?>
                                                <div class="input-group-addon no-print">
                                                    <a href="<?php echo site_url('system_settings/AddProduct_para/brands'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group standard">
                                            <?= lang("Design", "designpur") ?> *
                                            <div class="input-group col-md-12">
                                                <?php
                                                echo form_input('design', (isset($_POST['design']) ? $_POST['design'] : ($product ? $product->design : '')), 'class="form-control" default-attrib data-tab="design" id="design" data-key="brands_id" data-id="brands" placeholder="' . lang("select") . " " . lang("design") . '" required="required" style="width:100%" ng-model="prod.design" ng-change="getGstDetails();"');
                                                ?>
                                                <div class="input-group-addon no-print">
                                                    <a href="<?php echo site_url('system_settings/AddProduct_para/design'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group standard">
                                            <?= lang("Style", "stylepur") ?> *
                                            <div class="input-group col-md-12">
                                                <?php
                                                echo form_input('style', (isset($_POST['style']) ? $_POST['style'] : ($product ? $product->style : '')), 'class="form-control" default-attrib data-tab="style" id="style" data-key="design_id" data-id="design" placeholder="' . lang("select") . " " . lang("style") . '" required="required" style="width:100%" ng-model="prod.style" ng-change="getGstDetails();"');
                                                ?>
                                                <div class="input-group-addon no-print">
                                                    <a href="<?php echo site_url('system_settings/AddProduct_para/style'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group standard">
                                            <?= lang("Pattern", "patternpur") ?> *
                                            <div class="input-group col-md-12">
                                                <?php
                                                echo form_input('pattern', (isset($_POST['pattern']) ? $_POST['pattern'] : ($product ? $product->pattern : '')), 'class="form-control" default-attrib data-tab="pattern" data-key="style_id" data-id="style" id="pattern" placeholder="' . lang("select") . " " . lang("pattern") . '" required="required" style="width:100%" ng-model="prod.pattern" ng-change="getGstDetails();"');
                                                ?>
                                                <div class="input-group-addon no-print">
                                                    <a href="<?php echo site_url('system_settings/AddProduct_para/pattern'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group standard">
                                            <?= lang("Fitting", "fittingpur") ?> *
                                            <div class="input-group col-md-12">
                                                <?php
                                                echo form_input('fitting', (isset($_POST['fitting']) ? $_POST['fitting'] : ($product ? $product->fitting : '')), 'class="form-control" default-attrib data-tab="fitting" data-key="pattern_id" data-id="pattern" id="fitting" placeholder="' . lang("select") . " " . lang("fitting") . '" required="required" style="width:100%" ng-model="prod.fitting" ng-change="getGstDetails();"');
                                                ?>
                                                <div class="input-group-addon no-print">
                                                    <a href="<?php echo site_url('system_settings/AddProduct_para/fitting'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group standard">
                                            <?= lang("Fabric", "fabricpur") ?> *
                                            <div class="input-group col-md-12">
                                                <?php
                                                echo form_input('fabric', (isset($_POST['fabric']) ? $_POST['fabric'] : ($product ? $product->fabric : '')), 'class="form-control" default-attrib data-tab="fabric" id="fabric" data-key="fitting_id" data-id="fitting" placeholder="' . lang("select") . " " . lang("fabric") . '" required="required" style="width:100%" ng-model="prod.fabric" ng-change="getGstDetails();"');
                                                ?>
                                                <div class="input-group-addon no-print">
                                                    <a href="<?php echo site_url('system_settings/AddProduct_para/fabric'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group standard">
                                            <?= lang("Color", "color") ?>
                                            <div class="row">
                                                <div class="col-md-4"> &nbsp;&nbsp;<input type="radio" name="colortype" ng-model="prod.colortype" icheck value="Single"/>&nbsp;&nbsp;&nbsp;&nbsp;Single</div>
                                                <div class="col-md-3 colorsingle hide">
                                                    <?php echo form_dropdown('colorsingle', $color, (isset($_POST['colorsingle']) ? $_POST['colorsingle'] : ($product ? $product->color : '')), 'class="form-control" select2 id="color" placeholder="' . lang("select") . " " . lang("color") . '" style="width:100%"'); ?>
                                                </div>
                                                <div class="col-md-2 colorsingle hide">
                                                    <input type="text" id="colorqty" name="colorqty" onlyno class="form-control" ng-model="prod.colorqty" ng-change="getQty('#colorqty')"/>
                                                    <!--<p class="label label-danger hide"> Product Qty and color qty must be same.</p>-->
                                                </div>
                                                <div class="col-md-3 colorsingle hide">
                                                    <input type="text" id="colorcode" name="colorcode" onlyno readonly class="form-control" ng-model="prod.colorcode" ng-change="getQty('#colorqty')"/>
                                                    <input type="text" id="codet" name="codet" onlyno class="hidden form-control"/>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-4"> &nbsp;&nbsp;<input type="radio" name="colortype" ng-model="prod.colortype" icheck value="Assorted"/>&nbsp;&nbsp;&nbsp;&nbsp;Assorted</div>
                                                <div class="col-md-8 colorassorted hide">
                                                    <?php echo form_dropdown('colorassorted[]', $color, (isset($_POST['colorassorted']) ? $_POST['colorassorted'] : ($product ? $product->colorassorted : '')), 'class="form-control" multiple select2 id="mulcolor" ng-model="prod.colorassorted" placeholder="' . lang("select") . " " . lang("color") . '" style="width:100%"'); ?>
                                                    <br/>
                                                    <div class="row" style="padding-top: 40px;">
                                                        <div class="col-md-3 from-group" style="padding-right: 0px; margin-bottom: 5px" ng-repeat="n in prod.colorassoarr">
                                                            <input type="text" name="colorqty[]" onlyno ng-model="n.qty" ng-blur="getQtyCal($index, n.qty)" class="form-control"/>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="padding-top: 20px;">
                                                        <div class="col-md-3 from-group" style="padding-right: 0px; margin-bottom: 5px" ng-repeat="n in prod.colorassoarr">
                                                            <input type="text" name="colorcode[]" onlyno ng-model="n.colorcode" readonly class="form-control"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <?= lang("Size", "sizepur") ?> *
                                            <div class="row">
                                                <div class="col-md-4"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="sizeangle" ng-model="prod.sizeangle" ng-change="getProdName()" <?= ($product && $product->sizeangle == "HT" ? "checked" : '') ?> value="HT" id="sizeangle_ht"/>&nbsp;&nbsp;&nbsp;&nbsp;Height</div>
                                                <div class="col-md-4"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="sizeangle" ng-model="prod.sizeangle" ng-change="getProdName()" <?= ($product && $product->sizeangle == "WT" ? "checked" : '') ?> value="WT" id="sizeangle_wt" />&nbsp;&nbsp;&nbsp;&nbsp;Width</div>
                                                <div class="col-md-4"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="sizeangle" ng-model="prod.sizeangle" ng-change="getProdName()" <?= ($product && $product->sizeangle == "NZ" ? "checked" : '') ?> value="NZ" id="sizeangle_nz" />&nbsp;&nbsp;&nbsp;&nbsp;No Size</div>
                                            </div>
                                            <br/>
                                            <div class="row sizeangle_nz">
                                                <div class="col-md-4"> &nbsp;&nbsp;<input icheck type="radio" name="sizetype" ng-model="prod.sizetype" ng-change="getProdName()" checked="" value="Single"/>&nbsp;&nbsp;&nbsp;&nbsp;Single</div>
                                                <div class="col-md-5" ng-init='size =<?= json_encode($sizes) ?>' ng-show="prod.sizetype == 'Single'">
                                                    <div class="col-md-9" style="padding: 0px"><?php
                                                        echo form_input('singlesize', (isset($_POST['singlesize']) ? $_POST['singlesize'] : ($product ? $product->size : '')), 'class="form-control selec_clear " sizesel data-tab="size" data-key="department_id-section_id-product_items_id" data-id="department,section,product_items" id="size" placeholder="' . lang("select") . " " . lang("size") . '" required="required" style="width:100%" ng-model="prod.singlesize" ng-change="getProdName();getGstDetails();"');
                                                        ?>
                                                    </div>
                                                    <div class="col-md-2"> {{size[prod.singlesize].code?size[prod.singlesize].code+'\"':""}}</div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <br/>
                                            <div class="row standard sizeangle_nz">
                                                <div class="col-md-4"> &nbsp;&nbsp;<input icheck type="radio" name="sizetype" ng-model="prod.sizetype" ng-change="getProdName()" value="Multiple"/>&nbsp;&nbsp;&nbsp;&nbsp;Multiple</div>
                                                <div class="col-md-4" ng-init='size1 =<?= json_encode($sizes) ?>' ng-show="prod.sizetype == 'Multiple'" style="padding-left: 0;padding-right: 0;">
                                                    <div class="col-md-9" style="padding: 0px"><?php
                                                        echo form_input('multisizef', (isset($_POST['multisizef']) ? $_POST['multisizef'] : ''), 'class="form-control selec_clear " sizesel data-tab="size" data-key="department_id-section_id-product_items_id" data-id="department,section,product_items" id="multisizef" placeholder="' . lang("select") . " " . lang("size") . '" required="required" style="width:100%" ng-model="prod.multisizef" ng-change="getProdName();getGstDetails();"');
                                                        ?></div>
                                                    <div class="col-md-2"> {{size1[prod.multisizef].code?size1[prod.multisizef].code+'\"':""}}</div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-md-1" ng-show="prod.sizetype == 'Multiple'">To</div>
                                                <div class="col-md-3" ng-init='size2 =<?= json_encode($sizes) ?>' ng-show="prod.sizetype == 'Multiple'" style="padding-left: 0;padding-right: 0;">
                                                    <div class="col-md-9" style="padding: 0px"><?php
                                                        echo form_input('multisizet', (isset($_POST['multisizet']) ? $_POST['multisizet'] : ''), 'class="form-control selec_clear " sizesel data-tab="size" data-key="department_id-section_id-product_items_id" data-id="department,section,product_items" id="multisizet" placeholder="' . lang("select") . " " . lang("size") . '" required="required" style="width:100%" ng-model="prod.multisizet"');
                                                        ?></div>
                                                    <div class="col-md-2"> {{size2[prod.multisizet].code?size2[prod.multisizet].code+'\"':""}}</div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6 col-md-offset-1">
                                        <div class="form-group all">
                                            <?= lang("product_name", "namepur") ?> * 
                                            <?= form_input('name', (isset($_POST['name']) ? $_POST['name'] : ($product ? $product->name : '')), 'class="form-control" id="name" required="required" readonly ng-model="prod.name"'); ?>
                                        </div>
                                        <div class="form-group all hidden">
                                            <?= lang("product_code", "code") ?>
                                            <?= form_input('code', (isset($_POST['code']) ? $_POST['code'] : ($product ? $product->code : '')), 'class="form-control" id="code"  required="required" readonly ng-model="prod.barcode" ') ?>
                                            <span class="help-block"><?= lang('you_scan_your_barcode_too') ?></span>
                                        </div>
                                        <?= form_hidden('barcode_symbology', (isset($_POST['barcode_symbology']) ? $_POST['barcode_symbology'] : ($product ? $product->barcode_symbology : 'code128')), 'class="form-control" id="barcode_symbology"  required="required" readonly ') ?>
                                        <?php if ($Settings->tax1) { ?>
                                            <div class="form-group standard">
                                                <?= lang("product_tax", "tax_rate") ?>
                                                <?php
                                                $tr[""] = "";
                                                foreach ($tax_rates as $tax) {
                                                    $tr[$tax->id] = $tax->name;
                                                }
                                                echo form_dropdown('tax_rate', $tr, (isset($_POST['tax_rate']) ? $_POST['tax_rate'] : ($product ? $product->tax_rate : $Settings->default_tax_rate)), 'class="form-control select" id="tax_rate" placeholder="' . lang("select") . ' ' . lang("product_tax") . '" style="width:100%"');
                                                ?>
                                            </div>
                                            <div class="form-group standard">
                                                <?= lang("tax_method", "tax_method") ?>
                                                <?php
                                                $tm = array('0' => lang('inclusive'), '1' => lang('exclusive'));
                                                echo form_dropdown('tax_method', $tm, (isset($_POST['tax_method']) ? $_POST['tax_method'] : ($product ? $product->tax_method : '')), 'class="form-control select" id="tax_method" placeholder="' . lang("select") . ' ' . lang("tax_method") . '" style="width:100%"');
                                                ?>
                                            </div>
                                        <?php } ?>
                                        <div class="form-group standard">
                                            <?= lang("supplier", "supplier") ?>
                                            <div class="row" id="supplier-con">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <?php echo form_input('supplier', (isset($_POST['supplier']) ? $_POST['supplier'] : ''), 'class="form-control hidden" id="supplier12" placeholder="" style="width:100%;"'); ?>
                                                    <?php echo form_input('supplier_name', (isset($_POST['supplier']) ? $_POST['supplier'] : ''), 'class="form-control" id="supplier_name" readonly placeholder="" style="width:100%;"'); ?>

                                                </div>
                                            </div>
                                        </div>



                                        <div class="form-group standard">
                                            <?= lang("product_image", "product_image") ?> max (<?= byte_format($this->Settings->iwidth * $this->Settings->iheight) ?>)
                                            <input id="product_image" type="file" name="product_image" data-show-upload="false"
                                                   data-show-preview="false" accept="image/*" class="form-control file">
                                        </div>

                                        <div id="img-details"></div>

                                        <div class="row standard">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="control-label" for="unitpur"><?= lang("product_qty") ?> *</label> 
                                                    <?php
                                                    foreach ($warehouses as $warehouse) {
                                                        if ($this->Settings->racks) {
                                                            echo "<div class='row'><div class='col-md-12'>";
                                                            echo form_hidden('wh_' . $warehouse->id, $warehouse->id) . form_input('wh_qty_' . $warehouse->id, (isset($_POST['wh_qty_' . $warehouse->id]) ? $_POST['wh_qty_' . $warehouse->id] : ''), 'class="form-control" id="wh_qtys" placeholder="' . lang('quantity') . '" ng-model="prod.qty" onlyno');
                                                            echo "</div></div>";
                                                        } else {
                                                            echo form_hidden('wh_' . $warehouse->id, $warehouse->id) . form_input('wh_qty_' . $warehouse->id, (isset($_POST['wh_qty_' . $warehouse->id]) ? $_POST['wh_qty_' . $warehouse->id] : ''), 'class="form-control" id="wh_qty_' . $warehouse->id . '" placeholder="' . lang('quantity') . '" ng-model="prod.qty" onlyno');
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("unit_per", "unitpur") ?> *
                                                    <?= form_input('unit', (isset($_POST['unit']) ? $_POST['unit'] : ($product ? $product->uper : '')), 'class="form-control" id="unit" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" required="required" style="width:100%" '); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row standard">
                                            <div class="col-md-1">
                                                <div class="form-group all">
                                                    <input type="checkbox" icheck class="checkbox" ng-model="prod.chksecqty" name="sec_qtychk" id="sec_qtychk">
                                                </div>
                                            </div>
                                            <div class="col-md-7">
                                                <div class="form-group all">
                                                    <?= form_input('squantity', (isset($_POST['price']) ? $_POST['price'] : ($product ? $this->sma->formatDecimal($product->price) : '')), 'class="form-control tip" id="squantity"') ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= form_input('sunit', (isset($_POST['sunit']) ? $_POST['sunit'] : ($product ? $product->uper : '')), 'class="form-control" id="sunit" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" style="width:100%" '); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <script>
                                            $('#squantity').change(function () {
                                            var squantity = $('#squantity').val();
                                            var wh_qtys = $('#wh_qtys').val();
                                            if (wh_qtys % squantity != 0 && squantity != "") {
                                            bootbox.alert('Please Enter Valid Secondary Quentity');
                                            $('#squantity').val("");
                                            }
                                            });
                                        </script>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group all">
                                                    <?= lang("product_cost", "costpur") ?> *
                                                    <?= form_input('cost', (isset($_POST['cost']) ? $_POST['cost'] : ($product ? $this->sma->formatDecimal($product->cost) : '')), 'class="form-control tip" id="cost" required="required" ng-model="prod.cost" ng-blur="getProductMargin()"') ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group all">
                                                    <?= lang("product_per", "cperpur") ?> *
                                                    <?= form_input('cper', (isset($_POST['cper']) ? $_POST['cper'] : ($product ? $product->cper : '')), 'class="form-control" id="cper" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" required="required" style="width:100%" '); ?>
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group all">
                                                    <?= lang("product_price", "pricepur") ?> *
                                                    <?= form_input('price', (isset($_POST['price']) ? $_POST['price'] : ($product ? $this->sma->formatDecimal($product->price) : '')), 'class="form-control tip" id="price" ng-model="prod.price" required="required"') ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group all">
                                                    <?= lang("product_per", "pperpur") ?> *
                                                    <div class="input-group col-md-12">
                                                        <?= form_input('pper', (isset($_POST['pper']) ? $_POST['pper'] : ($product ? $product->pper : '')), 'class="form-control" id="pper" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" required="required" style="width:100%"'); ?>
                                                        <div class="input-group-addon no-print">
                                                            <a href="<?php echo site_url('system_settings/AddProduct_para/per'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group standard ">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" <?= (isset($_POST['ratetype']) && $_POST['ratetype'] == "MRP" ? 'checked' : 'checked') ?> name="ratetype"  value="MRP"/>&nbsp;&nbsp;&nbsp;&nbsp;MRP</div>
                                                        <div class="col-md-3">
                                                            <?php echo form_input('mrprate', (isset($_POST['mrprate']) ? $_POST['mrprate'] : ''), 'class="form-control" id="mrprate" placeholder="" style="width:100%;"'); ?>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="checkbox" icheck class="checkbox"  ng-model="prod.roundup" id="extras" value=""/>
                                                            <label for="extras" class="padding05"><?= lang('roundup') ?></label>
                                                        </div>
                                                        <!--                                                         <div class="col-md-3">
                                                        <?= lang("Rate", "singlerate") ?>   
                                                                                                                </div>
                                                                                                                <div class="col-md-1 hidden">
                                                        <?= lang("Per", "Per") ?> </div>
                                                                                                                <div class="col-md-4"><?= form_input('rateper', (isset($_POST['rateper']) ? $_POST['rateper'] : ($product ? $product->rateper : '')), 'class="form-control" default-attrib data-tab="per" id="rateper" placeholder="' . lang("select") . " " . lang("Unit") . '" required="required" style="width:100%" '); ?>
                                                                                                                </div> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <br/>
                                            <div class="row">
                                                <!--<div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" <?= (isset($_POST['ratetype']) && $_POST['ratetype'] == "Single" ? 'checked' : '') ?> name="ratetype" value="Single"/>&nbsp;&nbsp;&nbsp;&nbsp;Single</div>-->
                                                <!--<div class="col-md-3"><?php echo form_input('singlerate', (isset($_POST['singlerate']) ? $_POST['singlerate'] : ''), 'class="form-control" id="singlerate" placeholder="" style="width:100%;" '); ?></div>-->

                                            </div>
                                            <br/>
                                            <div class="row hidden">
                                                <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" <?= (isset($_POST['ratetype']) && $_POST['ratetype'] == "Multiple" ? 'checked' : '') ?> name="ratetype" value="Multiple"/>&nbsp;&nbsp;&nbsp;&nbsp;Multiple</div>
                                                <div class="col-md-3">
                                                    <div class="col-md-12" style="padding: 0px"><?php echo form_input('mulratef', (isset($_POST['mulratef']) ? $_POST['mulratef'] : ''), 'class="form-control" id="mulratef" placeholder="" style="width:100%;"'); ?></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-md-3">To</div>
                                                <div class="col-md-3" ng-init='size2 =<?= json_encode($sizes) ?>'>
                                                    <div class="col-md-12" style="padding: 0px"><?php echo form_input('mulratet', (isset($_POST['mulratet']) ? $_POST['mulratet'] : ''), 'class="form-control" id="mulratet" placeholder="" style="width:100%;"'); ?></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row ">
                                            <div class="col-md-6 form-group"> 
                                                <?= lang('hsncode', 'hsncode') ?>
                                                <?= form_input('hsnno', (isset($_POST['hsnno']) ? $_POST['hsnno'] : $product->price), 'class="form-control tip" id="hsnno" readonly ng-model="prod.hsnno" ') ?>
                                            </div>

                                            <div class="col-md-4 form-group">
                                                <?= lang('gstno', 'gstnopur') ?> *
                                                <?= form_input('gstno', (isset($_POST['gstno']) ? $_POST['gstno'] : $product->price), 'class="form-control tip" id="gstno" readonly ng-model="prod.gstno"  ng-blur="getProductMargin()"') ?>
                                            </div>
                                            <div class="col-md-2">
                                                <?= lang('cess', 'cess') ?>
                                                <?= form_input('cess', (isset($_POST['cess']) ? $_POST['cess'] : $product->price), 'class="form-control tip" id="cess" readonly ng-model="prod.cess"  ng-blur="getProductMargin()"') ?>
                                            </div>

                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-8"> &nbsp;&nbsp;<input type="checkbox" icheck class="checkbox" id="addupgetcheck" ng-model="prod.addupgst" name="addupgst">&nbsp;&nbsp;&nbsp;&nbsp;Add-up GST if MRP exceeds 1000+ (%)</div>
                                            <div class="col-md-4">
                                                <?= form_input('addupgstmrp', (isset($_POST['addupgstmrp']) ? $_POST['addupgstmrp'] : $product->price), 'class="form-control tip" id="addupgstmrp" ng-model="prod.addupgstmrp" readonly ng-blur="getProductMargin()"') ?>
                                            </div>
                                        </div>
                                        <!--                                        <div class="row form-group">
                                                                                    <div class="col-md-8"> &nbsp;&nbsp;<input icheck type="radio"  name="addupgst" ng-model="prod.addupgst" value="1"/>&nbsp;&nbsp;&nbsp;&nbsp;Add-up GST if MRP exceeds 1000+ (%)</div>
                                                                                    <div class="col-md-4">
                                        <?= form_input('addupgstmrp', (isset($_POST['addupgstmrp']) ? $_POST['addupgstmrp'] : $product->price), 'class="form-control tip" id="addupgstmrp" ng-model="prod.addupgstmrp" ng-blur="getProductMargin()" required="required"') ?>
                                                                                    </div>
                                                                                </div>-->

                                        <div class="combo" style="display:none;">
                                            <div class="form-group">
                                                <?= lang("add_product", "add_item") . ' (' . lang('not_with_variants') . ')'; ?>
                                                <?php echo form_input('add_item', '', 'class="form-control ttip" id="add_item" data-placement="top" data-trigger="focus" data-bv-notEmpty-message="' . lang('please_add_items_below') . '" placeholder="' . $this->lang->line("add_item") . '"'); ?>
                                            </div>
                                            <div class="control-group table-group">
                                                <label class="table-label" for="combo">{{prod.protype}} <?= lang("products"); ?></label>
                                                <div class="controls table-controls">
                                                    <table id="prTable" class="table items table-striped table-bordered table-condensed table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th class="col-md-5 col-sm-5 col-xs-5"><?= lang("product_name") . " (" . $this->lang->line("product_code") . ")"; ?></th>
                                                                <th class="col-md-2 col-sm-2 col-xs-2"><?= lang("quantity"); ?></th>
                                                                <th class="col-md-3 col-sm-3 col-xs-3"><?= lang("unit_price"); ?></th>
                                                                <th class="col-md-1 col-sm-1 col-xs-1 text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="digital" style="display:none;">
                                            <div class="form-group digital">
                                                <?= lang("digital_file", "digital_file") ?>
                                                <input id="digital_file" type="file" name="digital_file" data-show-upload="false"
                                                       data-show-preview="false" class="form-control file">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <?php // echo form_submit('add_product', "Save", 'class="btn btn-primary" ');       ?>
                                        </div>
                                    </div>
                                </form>
                                <?php // echo form_close();        ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="addProductItemManually"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>
<div id="editorderProducts" class="modal fade" role="dialog">

</div>

<script type="text/javascript" src="<?= $assets ?>js/bootstrapValidator.min.js"></script>
<script>
                                                    $(document).ready(function () {
                                                    $('#user_id').append('<option value="" selected="selected">Select Order By</option>');
//                                                        $('#list_suppliers').append('<option value="" selected="selected">Select Supplier Code</option>');

                                                    })
//                                                    $('#store').change(function () {
//
//                                                        var v = $(this).val();
//                                                        $('#modal-loading').show();
//                                                        if (v) {
//                                                            $.ajax({
//                                                                type: "get",
//                                                                async: false,
//                                                                data: {store: v},
//                                                                url: "<?= site_url('purchases/getBiller') ?>",
//                                                                dataType: "json",
//                                                                success: function (scdata) {
//                                                                    $('#list_suppliers').html('');
//                                                                    $('#list_suppliers').removeAttr('readonly', '');
////                                                                    $('#list_suppliers').append('<option value="">Please Select Supplier Code</option>');
//                                                                    $.each(scdata, function (k, val) {
//                                                                        var opt = $('<option />');
//                                                                        opt.val(val.id);
//                                                                        opt.text(val.code + ' - ' + val.company);
//                                                                        $('#list_suppliers').append(opt);
//                                                                    })
//                                                                    $('#modal-loading').hide();
//                                                                },
//                                                                error: function () {
//                                                                    $('#list_suppliers').attr('readonly', '');
//                                                                    $('#modal-loading').hide();
//                                                                }
//                                                            });
//
//                                                            $.ajax({
//                                                                type: "get",
//                                                                async: false,
//                                                                data: {store: v},
//                                                                url: "<?= site_url('purchases/getEmployees') ?>",
//                                                                dataType: "json",
//                                                                success: function (scdata) {
//                                                                    $('#user_id').html('');
//                                                                    $('#user_id').removeAttr('readonly', '');
//                                                                    $('#user_id').append('<option value="">Please Select Order By</option>');
//                                                                    $.each(scdata, function (k, val) {
//                                                                        var opt = $('<option />');
//                                                                        opt.val(val.id);
//                                                                        opt.text(val.fname + ' ' + val.mname + ' ' + val.lname);
//                                                                        $('#user_id').append(opt);
//                                                                    })
//                                                                }, error: function () {
////                    $('#booked_by').addClass('hidden');
//                                                                    $('#user_id').attr('readonly', '');
//                                                                    $('#modal-loading').hide();
//                                                                }
//                                                            })
//                                                        }
//                                                    });


                                                            $('#list_suppliers').change(function () {
                                                    var su = $('#list_suppliers').val();
                                                    var supp_code = $('#list_suppliers option:selected').text();
                                                    $('#supplier_name').attr('placeholder', supp_code);
                                                    $('#supplier12').val(su);
                                                    });</script>

<script type="text/javascript">
    $(document).ready(function () {


    $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
    placeholder: "<?= lang('select_category_to_load') ?>", data: [
    {id: '', text: '<?= lang('select_category_to_load') ?>'}
    ]
    });
    $('#category').change(function () {
    var v = $(this).val();
    $('#modal-loading').show();
    if (v) {
    $.ajax({
    type: "get",
            async: false,
            url: "<?= site_url('products/getSubCategories') ?>/" + v,
            dataType: "json",
            success: function (scdata) {
            if (scdata != null) {
            $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
            placeholder: "<?= lang('select_category_to_load') ?>",
                    data: scdata
            });
            }
            },
            error: function () {
            bootbox.alert('<?= lang('ajax_error') ?>');
            $('#modal-loading').hide();
            }
    });
    } else {
    $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
    placeholder: "<?= lang('select_category_to_load') ?>",
            data: [{id: '', text: '<?= lang('select_category_to_load') ?>'}]
    });
    }
    $('#modal-loading').hide();
    });
    $('#code').bind('keypress', function (e) {
    if (e.keyCode == 13) {
    e.preventDefault();
    return false;
    }
    });
    $('#color').change(function () {
    var v = $(this).val();
    $.ajax({
    type: "get",
            async: false,
            url: "<?= site_url('products/getColorCode') ?>/" + v,
            dataType: "json",
            success: function (scdata) {
//                        alert(scdata);
            if (scdata != null) {
            var code = scdata.code;
            $('#codet').val(code);
            var quantity = $('#colorqty').val();
            $('#colorcode').val(code + quantity);
            $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
            placeholder: "<?= lang('select_category_to_load') ?>",
                    data: scdata
            });
            }
            },
            error: function () {
            bootbox.alert('<?= lang('ajax_error') ?>');
            $('#modal-loading').hide();
            }
    });
    });
    $('#colorqty').change(function () {
    $('#colorcode').attr("value", "");
    var codet = $('#codet').val();
//        var quantity = $('#colorqty').val();
    var qty = $('#colorqty').val();
    $('#colorcode').attr("value", codet + qty);
    })

//            $('#wh_qtys').change(function () {
//    var v = $('#addproduct').serialize();
//    var qty = $('#wh_qtys').val();
//    $('#modal-loading').show();
//    $.ajax({
//    type: "get",
//            data: v,
//            async: false,
//            url: "<?= site_url('products/getProSquentity') ?>?" + v,
//            dataType: "json",
//            success: function (scdata) {
//            if (scdata != 0) {
//            var sqty = parseInt(qty) / parseInt(scdata.no_of_pic);
//            $('#squantity').val(Number(Math.round(sqty)));
//            }
////                else{
////                    $('#squantity').val('');
////                }
//            },
//            error: function () {
//            bootbox.alert('<?= lang('ajax_error') ?>');
//            $('#modal-loading').hide();
//            }
//    });
//    $('#modal-loading').hide();
//    });
//        if (store = localStorage.getItem('store')) {
//            $('#store').value(store);
//        }
    });
    $(document).on('click', '#addManually', function (e) {
    $('#mModal').appendTo("body").modal('show');
    return false;
    });</script>



<script>
//    $('#store').change(function () {
//        var v = $(this).val();
//        $('#modal-loading').show();
//        if (v) {
//            $.ajax({
//                type: "get",
//                async: false,
//                data: {store: v},
//                url: "<?= site_url('purchases/getBiller') ?>",
//                dataType: "json",
//                success: function (scdata) {
//                    $('#list_suppliers').html('');
//                    $('#list_suppliers').removeAttr('readonly', '');
//                    $('#list_suppliers').append('<option value="">Please Select Supplier Code</option>');
//                    $.each(scdata, function (k, val) {
//                        var opt = $('<option />');
//                        opt.val(val.id);
//                        opt.text(val.code + ' - ' + val.company);
//                        $('#list_suppliers').append(opt);
//                    })
//                    $('#modal-loading').hide();
//                },
//                error: function () {
//                    $('#list_suppliers').attr('readonly', '');
//                    $('#modal-loading').hide();
//                }
//            });
//        }
//    });



    $('#addItemManually').click(function () {
    var v = $('#addproduct').serialize();
//        var qty = $('#wh_qtys').val();
    $('#modal-loading').show();
    $.ajax({
    type: "get",
            data: v,
            async: false,
            url: "<?= site_url('products/saveAddOrderProduct') ?>?" + v,
            dataType: "json",
            success: function (scdata) {
            if (scdata.status == 1) {
            var mid = scdata.product_id,
                    mcode = $('#code').val(),
                    mname = $('#name').val(),
                    mqty = parseFloat($('#wh_qtys').val()),
                    sqty = parseFloat($('#squantity').val()),
                    unit_price = parseFloat($('#cost').val())
                    quitems[mid] = {"id": mid, "item_id": mid, "label": mname, "row": {"id": mid, "code": mcode, "page": 0, "cost": unit_price, "name": mname, "quantity": mqty, "squantity": sqty, "real_unit_cost": unit_price, "price": unit_price, "unit_price": unit_price, "real_unit_price": unit_price, "tax_rate": 0, "tax_method": 0, "qty": mqty, "type": "manual", "discount": 0, "serial": "", "option": ""}, "tax_rate": 0, "options": false};
            localStorage.setItem('quitems', JSON.stringify(quitems));
            loadItems();
            $('#mModal').modal('hide');
            $('#mcode').val('');
            $('#mname').val('');
            $('#mtax').val('');
            $('#mquantity').val('');
            $('#mdiscount').val('');
            $('#mprice').val('');
            return false;
            }
            },
    });
    $('#modal-loading').hide();
    });</script>

<script>
    $(document).ready(function () {
    $('#user_id').append('<option value="" selected="selected">Select Order By</option>');
//        $('#list_suppliers').append('<option value="" selected="selected">Select Supplier Code</option>');

    })
            $('#store').change(function () {

    var v = $(this).val();
    $('#modal-loading').show();
    if (v) {
    $.ajax({
    type: "get",
            async: false,
            data: {store: v},
            url: "<?= site_url('purchases/getBiller') ?>",
            dataType: "json",
            success: function (scdata) {
                $('#list_suppliers').html('');
                $('#list_suppliers').removeAttr('readonly', '');
                $('#list_suppliers').append('<option value="">Please Select Supplier Code</option>');
                $.each(scdata, function (k, val) {
                var opt = $('<option />');
                opt.val(val.id);
                opt.text(val.code + ' - ' + val.company);
                $('#list_suppliers').append(opt);
                })
                $('#list_suppliers').select2('open');
                    $('#modal-loading').hide();
            },
            error: function () {
            $('#list_suppliers').attr('readonly', '');
            $('#modal-loading').hide();
            }
    });
    $.ajax({
    type: "get",
            async: false,
            data: {store: v},
            url: "<?= site_url('purchases/getEmployees') ?>",
            dataType: "json",
            success: function (scdata) {
            $('#user_id').html('');
            $('#user_id').removeAttr('readonly', '');
            $('#user_id').append('<option value="">Please Select Order By</option>');
            $.each(scdata, function (k, val) {
            var opt = $('<option />');
            opt.val(val.id);
            opt.text(val.fname + ' ' + val.mname + ' ' + val.lname);
            $('#user_id').append(opt);
            })
            }, error: function () {
//                    $('#booked_by').addClass('hidden');
    $('#user_id').attr('readonly', '');
    $('#modal-loading').hide();
    }
    })
    }
    });
    $('#list_suppliers').change(function () {
    var su = $('#list_suppliers').val();
    var supp_code = $('#list_suppliers option:selected').text();
    $('#supplier_name').attr('placeholder', supp_code);
    $('#supplier12').val(su);
    });</script>

<script type="text/javascript">
    $(document).ready(function () {
    localStorage.clear();
//                location.reload();

    $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
    placeholder: "<?= lang('select_category_to_load') ?>", data: [
    {id: '', text: '<?= lang('select_category_to_load') ?>'}
    ]
    });
    $('#category').change(function () {
    var v = $(this).val();
    $('#modal-loading').show();
    if (v) {
    $.ajax({
    type: "get",
            async: false,
            url: "<?= site_url('products/getSubCategories') ?>/" + v,
            dataType: "json",
            success: function (scdata) {
            if (scdata != null) {
            $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
            placeholder: "<?= lang('select_category_to_load') ?>",
                    data: scdata
            });
            }
            },
            error: function () {
            bootbox.alert('<?= lang('ajax_error') ?>');
            $('#modal-loading').hide();
            }
    });
    } else {
    $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
    placeholder: "<?= lang('select_category_to_load') ?>",
            data: [{id: '', text: '<?= lang('select_category_to_load') ?>'}]
    });
    }
    $('#modal-loading').hide();
    });
    $('#code').bind('keypress', function (e) {
    if (e.keyCode == 13) {
    e.preventDefault();
    return false;
    }
    });
    $('#color').change(function () {
    var v = $(this).val();
    $.ajax({
    type: "get",
            async: false,
            url: "<?= site_url('products/getColorCode') ?>/" + v,
            dataType: "json",
            success: function (scdata) {
//                        alert(scdata);
            if (scdata != null) {
            var code = scdata.code;
            $('#codet').val(code);
            var quantity = $('#colorqty').val();
            $('#colorcode').val(code + quantity);
            $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
            placeholder: "<?= lang('select_category_to_load') ?>",
                    data: scdata
            });
            }
            },
            error: function () {
            bootbox.alert('<?= lang('ajax_error') ?>');
            $('#modal-loading').hide();
            }
    });
    });
    $('#colorqty').change(function () {
    $('#colorcode').attr("value", "");
    var codet = $('#codet').val();
//        var quantity = $('#colorqty').val();
    var qty = $('#colorqty').val();
    $('#colorcode').attr("value", codet + qty);
    })

//            $('#wh_qtys').change(function () {
//    var v = $('#addproduct').serialize();
//    var qty = $('#wh_qtys').val();
//    $('#modal-loading').show();
//    $.ajax({
//    type: "get",
//            data: v,
//            async: false,
//            url: "<?= site_url('products/getProSquentity') ?>?" + v,
//            dataType: "json",
//            success: function (scdata) {
//            if (scdata != 0) {
//            var sqty = parseInt(qty) / parseInt(scdata.no_of_pic);
//            $('#squantity').val(Number(Math.round(sqty)));
//            }
////                else{
////                    $('#squantity').val('');
////                }
//            },
//            error: function () {
//            bootbox.alert('<?= lang('ajax_error') ?>');
//            $('#modal-loading').hide();
//            }
//    });
//    $('#modal-loading').hide();
//    });
//        if (store = localStorage.getItem('store')) {
//            $('#store').value(store);
//        }

            var old_row_qty;
    $(document).on("focus", '.rquantity', function () {
    old_row_qty = $(this).val();
    }).on("change", '.rquantity', function () {
    var row = $(this).closest('tr');
    if (!is_numeric($(this).val())) {
    $(this).val(old_row_qty);
    bootbox.alert(lang.unexpected_value);
    return;
    }
    var new_qty = parseFloat($(this).val()),
            item_id = row.attr('data-item-id');
    poitems[item_id].row.qty = new_qty;
    localStorage.setItem('poitems', JSON.stringify(poitems));
    loadItems();
    });
    });
    $('#addProductItemManually').click(function () {
    var v = $('#addproduct').serialize();
    $('#modal-loading').show();
    $.ajax({
    type: "get",
            //data: v,
            async: false,
            url: "<?= site_url('products/saveAddOrderProduct') ?>?" + v,
            dataType: "json",
            success: function (scdata) {
            if (scdata.status == 1) {
            $.each(scdata.data, function (k, val) {
            var mid = val.id,
                    mcode = val.code,
                    mname = val.name,
                    mqty = val.quantity,
                    sqty = val.squantity,
                    unit_price = val.cost,
                    brandname = val.brandname,
                    colorname = val.colorname,
                    designname = val.designname,
                    stylename = val.stylename,
                    patternname = val.patternname,
                    fittingname = val.fittingname,
                    fabricname = val.fabricname,
                    rate = val.rate,
                    uname = val.uname,
                    gst = val.gst
                    poitems[mid] = {"id": mid, "item_id": mid, "label": mname, "row": {"id": mid, "code": mcode, "page": 0, "cost": unit_price, "name": mname, "quantity": mqty, "squantity": sqty, "real_unit_cost": unit_price, "price": unit_price, "unit_price": unit_price, "real_unit_price": unit_price, "tax_rate": 0, "tax_method": 0, "qty": mqty, "type": "manual", "discount": 0, "serial": "", "option": "", "brandname": brandname, "colorname": colorname, "designname": designname, "stylename": stylename, "patternname": patternname, "fittingname": fittingname, "fabricname": fabricname, "rate": rate, "uname": uname, "gst": gst}, "tax_rate": 0, "options": true};
            localStorage.setItem('poitems', JSON.stringify(poitems));
            loadItems();
            });
            $('#mModal').modal('hide');
            $('#mcode').val('');
            $('#mname').val('');
            $('#mtax').val('');
            $('#mquantity').val('');
            $('#mdiscount').val('');
            $('#mprice').val('');
            return false;
            } else {
            alert('Please Select All required fields');
            }
            },
    });
    $('#modal-loading').hide();
    });


</script>