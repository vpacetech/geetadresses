 <?= $modal_js ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <center><h4 class="modal-title" id="myModalLabel">Edit Parcel Received Voucher</h4></center>
        </div>
        <?php
        $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'addFormx');
        echo form_open_multipart("purchases/updateParcelRev/" . $min->id, $attrib);
        ?>
        <!--<form data-toggle="validator" role="form" id="addFormx">-->
        <div class="modal-body ">
            <p><?= lang('enter_info'); ?></p>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 transport">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="veh_no"><?= lang('select_store') ?></label>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <div class="controls"> 

                            <?php
                            $st = "";
                            foreach ($store as $row) {
                                $st[$row->id] = $row->name;
                            }
                            echo form_dropdown('store_id', $st, (isset($_POST['biller']) ? $_POST['biller'] : $min->store_id), 'data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" id="get_supplier" class="form-control input-tip select" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 transport">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="veh_no">Sender</label>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <div class="controls"> 
                            <?php
                            $bl[""] = "";
                            foreach ($suppliers as $biller) {
                                $bl[$biller->id] = $biller->company != '-' ? $biller->code. ' - ' . $biller->company :  $biller->code. ' - ' . $biller->company;
                                if ($min->sender == $biller->id) {
                                    $id[$biller->id] = $biller->company != '-' ? $biller->code. ' - ' . $biller->company :  $biller->code. ' - ' . $biller->company;
                                }
                            }
                            echo form_dropdown('sender', $id, set_value('sender', $min->sender), 'data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("sender") . '" required="required" class="form-control input-tip select" id="sender"  style="width:100%;height:auto;"');
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 transport">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="veh_no">Sender Address</label>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <div class="controls"> 
                            <?php echo form_input('sender_address', $min->sender_address, 'class="form-control tip" id="sender_address" data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 transport">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="transport">Transport</label>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <div class="controls"> 
                            <?php
                            $trans[""] = "";
                            foreach ($transport as $tr) {
                                $trans[$tr->id] = $tr->code . ' - ' . $tr->transport_name;
                            }
                            echo form_dropdown('transport', $trans, $min->transport, 'id="transport" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("transport") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 billing">
            
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="veh_no">L.R.No</label>
                        <div class="controls"> 
                            <?php echo form_input('lr_no', $min->lr_no, 'class="form-control tip" id="lr_no" data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <!-- <label class="control-label" for="veh_no">Against Order.No</label> -->
                        <div class="controls"> 
                            <?php //echo form_input('ag_order_no', $min->lr_no, 'class="form-control tip" id="ag_order_no" data-bv-notempty="true"'); ?>
                            <?= lang("ag_order_no", "ag_order_no") ?>
                            <?php
                            // $ag_ord[""] = "";
                            $i=0;
                            if (!empty($selected_ag_order_nos)) {
                                foreach ($ag_order_nos as $ag_ord_no) {
                                    foreach ($selected_ag_order_nos as $selord) {
                                        if( $ag_ord_no == $selord->ag_order_no)
                                        {
                                            $ag_ord[] = $i;
                                            break;
                                        }
                                    }
                                }
                            }
                            // echo "<pre>"; print_r($ag_ord);exit;
                            echo form_dropdown('ag_order_no[]', $ag_order_nos, $ag_ord, 'id="reference_no" placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("reference_no") . '" multiple required="required" class="form-control input-tip select get_against_order_sup" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="veh_no">Veh.no</label>
                        <div class="controls"> 
                            <?php echo form_input('veh_no', $min->veh_no, 'class="form-control tip"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="veh_no">Bk. Date</label>
                        <div class="form-group">
                            <?php echo form_input('bk_date', ($min->bk_date != "" ? $min->bk_date : $_POST['bk_date']), 'class="form-control input-tip datetime" id="sldate" required="required"'); ?>
                        </div>
                        <!--                        <div class="controls"> 
                        <?php //echo form_input('bk_date', '', 'class="form-control tip" id="lr_no" data-bv-notempty="true"'); ?>
                                                </div>-->
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="">Parcel Type</label>
                        <div class="controls">
                            <?php echo form_dropdown('parcel_type', array('medium_cartoon' => 'Medium Cartoon', 'small_cartoon' => 'Small Cartoon', 'large_cartoon' => 'Large Cartoon'), $min->parcel_type, 'class="form-control myselect parcel_type" data-tab="per" style="" '); ?>                             
                        </div>
                    </div>

                </div><!--end col 6 --->
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="veh_no">No of Bales</label>
                        <div class="controls"> 
                            <?php echo form_input('no_of_bales', $min->no_of_bales, 'class="form-control tip" id="no_of_bales" data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="veh_no">Supplier Purchase Bill No.</label>
                        <div class="controls"> 
                            <?php echo form_input('purchase_bill_no', $min->purchase_bill_no, 'class="form-control tip" id="purchase_bill_no" data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="veh_no">Hamal</label>
                        <div class="controls"> 
                            <?php echo form_input('hamal', $min->hamal, 'class="form-control tip"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="veh_no">Receiver</label>
                        <div class="controls"> 
                            <?php
                            $us[""] = "";
                            foreach ($emp as $usr) {
                                $us[$usr->id] = $usr->fname;
                                if ($min->receiver == $usr->id) {
                                    $id[$usr->id] = $usr->fname;
                                }
                            }
                            echo form_dropdown('receiver', $id, set_value('receiver', $min->receiver), 'data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" class="form-control input-tip select" id="booked_by"  style="width:100%;height:auto;"');?>



                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="">Delivery</label>
                        <div class="controls">
                            <?php echo form_dropdown('delivery', array('Door' => 'Door', 'Collect_By_Sales' => 'Collect By Sales'), $min->delivery, 'class="form-control myselect incentive_type" data-tab="per" style="" '); ?>                             
                        </div>
                    </div>
                </div>
                <!--end col 6 --->

                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 text-center">
                    <div class="form-group">
                        <label class="control-label" for="veh_no">Goods Description</label>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                    <div class="form-group">
                        <div class="controls"> 
                            <?php echo form_input('goods_desc', $min->goods_desc, 'class="form-control tip" id="goods_desc" data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                </div>


                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 text-center">
                    <div class="form-group">
                        <label class="control-label" for="fight">Fright Charges</label>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                    <div class="form-group">
                        <div class="controls"> 
                        <?php
                            echo form_input('fright', $min->fright, 'class="form-control tip" id="fright"'
                                    . 'data-bv-notempty="true"
                               data-bv-notempty-message="The fright is required and cannot be empty"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The fright chargers can only consist of digits"');
                            ?>
                        </div>
                    </div>
                </div>
                
                <div class="clearfir"></div>


            </div>
            <!---->
        </div><!--end body div-->
        <div class="modal-footer" style="clear:both;">
            <input name="add_user" value="Save" class="btn btn-primary" type="submit"> 
        </div><!--end foter div-->
        </form>

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function (e) {
        $('#addFormx').bootstrapValidator({
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            }, excluded: [':disabled']
        });
        $('select.select').select2({minimumResultsForSearch: 6});
        fields = $('.modal-content').find('.form-control');
        $.each(fields, function () {
            var id = $(this).attr('id');
            var iname = $(this).attr('name');
            var iid = '#' + id;
            if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
//                $("label[for='" + id + "']").append(' *');
                $(document).on('change', iid, function () {
                    $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
                });
            }
        });
    });

    mrp = onlydigits('mrp');
    mrp();
    lr_no = onlydigits('lr_no');
    lr_no();
    no_of_bales = onlydigits('no_of_bales');
    no_of_bales();
    hamal = onlyAlphabet('hamal');
    hamal();
    sldate = onlyKeyPress('sldate');
    sldate();

    $(document).ready(function () {
//        $("#addFormx").unbind().submit(function (e) {
//            e.preventDefault();
//            var form = $("#addFormx");
//            var $data = $(this).serialize();
////            $data += "&name=<?= $this->security->get_csrf_token_name() ?>&value=<?= $this->security->get_csrf_hash() ?>";
//            $.ajax({
//                url: "<?= site_url('system_settings/SaveMin_qty_lvl/?v=1') ?>",
//                type: "get", async: false,
//                data: $data,
//                dataType: 'json',
//                success: function (data, textStatus, jqXHR) {
//                    if (data.s === "true") {
//                        bootbox.alert('Paramenter Added Successfully!');
//                        oTable.fnDraw();
//                        $("#myModal").modal('hide');
//                    } else {
//                        bootbox.alert('Paramenter Adding Failed!');
//                        $("#myModal").modal('hide');
//                    }
//                }
//            });
//            return false;
//        });
        var obj = ['companies', 'department', 'section', 'product_items', 'type', 'brands', 'design', 'style', 'pattern', 'fitting', 'fabric', 'color', 'size', 'per'];
        var objarr = [];
        var i = 0;
        $.each(obj, function (k, v) {
            objarr[i] = myselect2(v);
            objarr[i]();
            i++;
        });
        function myselect2(id) {
            if (id == "per") {
                var pp = $("." + id);
            } else {
                var pp = $("#" + id);
            }
            function get() {
                pp.select2({
                    minimumInputLength: 1,
                    data: [],
                    initSelection: function (element, callback) {
                        $.ajax({
                            type: "get", async: false,
                            url: site_url + "products/getIdAttribute",
                            data: {
                                term: pp.val(),
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                            },
                            dataType: "json",
                            success: function (data) {
                                callback(data[0]);
                            }
                        });
                    },
                    ajax: {
                        url: site_url + "products/getIdAttributes",
                        dataType: 'json',
                        quietMillis: 15,
                        data: function (term, page) {
                            return {
                                term: term,
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                                limit: 10
                            };
                        },
                        results: function (data, page) {
                            if (data.results != null) {
                                return {results: data.results};
                            } else {
                                return {results: [{id: '', text: 'No Match Found'}]};
                            }
                        }
                    }
                });
            }
            return get;
        }
        function getidsx() {
//            alsert($("#selsize").data('id'));
            var $ids = $("#selsize").data('id').split(",");
            var $idvals = "";

            $.each($ids, function (k, v) {
                $idvals += ($("#" + v).val() ? $("#" + v).val() : "") + "-";
            });
            $idvals = $idvals.trim();
            return $idvals;
        }
        $("#selsize").select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site_url + "products/getIdAttribute",
                    data: {
                        term: $(element).val(),
                        tab: $(element).data('tab'),
                        id: getidsx,
                        key: $(element).data('key'),
                    },
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site_url + "products/getIdAttributes",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        tab: $("#selsize").data('tab'),
                        id: getidsx,
                        key: $("#selsize").data('key'),
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'No Match Found'}]};
                    }
                }
            }
        });
    });
    
    
    
//    $('#get_supplier').change(function () {
//        var v = $(this).val();
//        $('#modal-loading').show();
//        if (v) {
//            $.ajax({
//                type: "get",
//                async: false,
//                data: {store: v},
//                url: "<?= site_url('purchases/getBiller') ?>",
//                dataType: "json",
//                success: function (scdata) {
//                    $('#sender').html('');
//                    $('#sender').removeAttr('readonly', '');
//                    $.each(scdata, function (k, val) {
//                        var opt = $('<option />');
//                        opt.val(val.id);
//                        opt.text(val.code + ' - ' + val.name);
//                        $('#sender').append(opt);
//                    })
//                },
//                error: function () {
//                    $('#sender').attr('readonly', '');
//                    $('#modal-loading').hide();
//                }
//            });
//
//            $.ajax({
//                type: "get",
//                async: false,
//                data: {store: v},
//                url: "<?= site_url('purchases/getEmployees') ?>",
//                dataType: "json",
//                success: function (scdata) {
//                    $('#booked_by').html('');
//                    $('#booked_by').removeAttr('readonly', '');
//                    $.each(scdata, function (k, val) {
//                        var opt = $('<option />');
//                        opt.val(val.id);
//                        opt.text(val.fname + ' ' + val.mname + ' ' + val.lname);
//                        $('#booked_by').append(opt);
//                    })
//                }, error: function () {
////                    $('#booked_by').addClass('hidden');
//                    $('#booked_by').attr('readonly', '');
//                    $('#modal-loading').hide();
//                }
//            })
//        }
//    });
    $('#sender').change(function () {
        var v = $(this).val();
        $('#modal-loading').show();
        if (v) {
            $.ajax({
                type: "get",
                async: false,
                url: "<?= site_url('purchases/getSupplierAddress') ?>/" + v,
                dataType: "json",
                success: function (scdata) {
                    if (scdata) {
                        $('#sender_address').val(scdata);
                    } else {
                        bootbox.alert('<?= lang('sender_address_not_avail') ?>');
                        $('#sender_address').val('');
                    }
                },
                error: function () {
                    bootbox.alert('<?= lang('ajax_error') ?>');
                    $('#modal-loading').hide();
                }
            });
        }
    });


$(document).ready(myFunction);
 $('#get_supplier').on('ready change',myFunction);

function myFunction() {
        var v  = $('#get_supplier').val();
//        var v = $(this).val();
        $('#modal-loading').show();
        if (v) {
            $.ajax({
                type: "get",
                async: false,
                data: {store: v},
                url: "<?= site_url('purchases/getBiller') ?>",
                dataType: "json",
                success: function (scdata) {
                    $('#sender').html('');
                    $('#sender').removeAttr('readonly', '');
                    $.each(scdata, function (k, val) {
                        var opt = $('<option />');
                        opt.val(val.id);
                        opt.text(val.code + ' - ' + val.company);
                        $('#sender').append(opt);
                    })
                },
                error: function () {
                    $('#sender').attr('readonly', '');
                    $('#modal-loading').hide();
                }
            });

            $.ajax({
                type: "get",
                async: false,
                data: {store: v},
                url: "<?= site_url('purchases/getEmployees') ?>",
                dataType: "json",
                success: function (scdata) {
                    $('#booked_by').html('');
                    $('#booked_by').removeAttr('readonly', '');
                    $.each(scdata, function (k, val) {
                        var opt = $('<option />');
                        opt.val(val.id);
                        opt.text(val.fname + ' ' + val.mname + ' ' + val.lname);
                        $('#booked_by').append(opt);
                    })
                }, error: function () {
//                    $('#booked_by').addClass('hidden');
                    $('#booked_by').attr('readonly', '');
                    $('#modal-loading').hide();
                }
            })
        }
    };






</script>
