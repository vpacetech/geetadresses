<style>
    .close{
        font-size: 19px !important;
    }
    .heading{
        margin-top:14px !important;
        border-top: 1px solid black;
    }
    .heading1{
        padding-top: 7px;
    }
    .modal-header{
        border-bottom: none !important;
    }
</style>
<?php echo $modal_js ?>
<div class="modal-dialog modal-lg ">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
        </div>
        <?php
        $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'addFormx');
        echo form_open_multipart("purchases/UpdateContraVoucher/" . $voucher->id, $attrib);
        ?>
        <div class="modal-body">
            <div >
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <h2 style="margin:0;padding-top: 10px !important;">
                        <?= lang('contravoucher_edit') ?>
                    </h2>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
                    <b style='font-size: 20px;'><?= $Settings->site_name ?></b>
                    <!--<h4>Kavathe - Mahankal</h4>-->
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 "></div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 heading" >
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 text-right">
                        <button type="button" onclick="enableEdit()"><?= lang('define_my_own_Code') ?></button>
                    </div>

                    <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="joining_date">  <?= lang('contra_no') ?> </label>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 heading1">
                        <input type="text" name="payment_no"  id="contra_code" value="<?php echo $voucher->payment_no ?>" class="form-control" readonly>
                    </div>
                </div>


                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                    <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="joining_date"><?= lang('date') ?> </label>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 heading1">
                        <?php echo form_input('date', date('d/m/Y') . '(' . date('l') . ')', 'class="form-control tip" id="sender_address" data-bv-notempty="true"  readonly'); ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group">
                    <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="joining_date">  <?= lang('user') ?>  </label>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 heading1">
                        <?php
                        $bl = "";
                        $bl[''] = "Select User";
                        foreach ($users as $row) {
                            $bl[$row->id] = $row->first_name . ' ' . $row->last_name;
                        }
                        echo form_dropdown('user_name', $bl, (isset($_POST['user_name']) ? $_POST['user_name'] : $voucher->user_name), 'data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                        ?>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                    <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="joining_date"> <?= lang('time') ?> </label>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 heading1">
                        <?php echo form_input('time', date('h:i a'), 'class="form-control tip" id="sender_address" data-bv-notempty="true" readonly'); ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 heading" style="padding:0;">
                <h4 class="text-center" style="font-size: 20px; padding-bottom:5px;"><span style="text-decoration: underline;">Depositer </span>&nbsp; <span style="text-decoration: underline;">Details </span></h4>
                <!--<h4 class="text-center" style="font-size: 20px; text-decoration: underline;padding-bottom:5px;"><?= lang('depositor_details') ?></h4>-->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="payfor"><?= lang('transfer_type') ?> :</label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">

                            <?php
                            $opt = array('' => 'Select Transfer Type', 'Bank Deposits' => 'Bank Deposits', 'Bank Transfer' => 'Bank Transfer');
                            echo form_dropdown('transfer_type', $opt, (isset($_POST['transfer_type']) ? $_POST['transfer_type'] : $voucher->transfer_type), 'id="transfer_type" data-placeholder="' . lang("select") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                            ?>

                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12"><?= lang('from_account') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php
                            $b2 = array();
//                            $b2['Payee'] = 'Payee';
                            $b2[''] = 'Select From Account';
                            foreach ($bank_acc_no as $row) {
                                $b2[$row->id] = $row->bank_name;
                            }
                            $b2['Cash'] = 'Cash';
                            echo form_dropdown('from_acc', $b2, $voucher->from_acc, 'data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" id="get_acc_no" class="form-control input-tip select" style="width:100%;"');
                            ?>

                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="againstno"><?= lang('ref_no') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php
                            $rf = "";
                            foreach ($ref_no as $row) {
                                $rf[$row->reference_no] = $row->reference_no;
                            }
                            echo form_dropdown('against_ref_no', $rf, (isset($_POST['against_ref_no']) ? $_POST['against_ref_no'] : $voucher->against_ref_no), 'id="qubiller" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="joining_date"><?= lang('bank_acc_no') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <input type="text" name="bank_acc_no" id="bank" value="<?= $voucher->bank_acc_no ?>" class="form-control" <?= $voucher->from_acc == "Cash" ? 'disabled' : '' ?> >
                            <?php
//                            foreach ($bank_acc_no as $row) {
//                                $b[$row->account_no] = $row->account_no;
//                            }
//                            echo form_dropdown('bank_acc_no', $b, (isset($_POST['against_ref_no']) ? $_POST['against_ref_no'] : $voucher->bank_acc_no), 'id="bank" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("bank_acc_no") . '" required="required" class="form-control input-tip select" style="width:100%;"');
////                            
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="dated"><?= lang('dated') ?> </label>

                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php echo form_input('datedd', (isset($_POST['date']) ? $_POST['date'] : $this->sma->hrld($voucher->datedd)), 'class="form-control input-tip datetime" id="podate" required="required"'); ?>                        
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="amount"><?= lang('amount') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">

                            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12" style="padding: 0">
                                <?php echo form_input('amount', (isset($_POST['amount']) ? $_POST['amount'] : $voucher->amount), 'class="form-control input-tip" id="amount" required="required"'); ?>                        
                                <!--<input type="text" name="amount" class="form-control" placeholder="55,689.00" required=""  >-->
                            </div>
                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="padding-right: 0">
                                <input type="text"  class="form-control" placeholder="cr." disabled>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive" id="currency_note" <?= $voucher->from_acc != "Cash" ? 'style="display: none"' : '' ?>>
                    <hr>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                        <h3><b><?= lang("currency_note_details") ?></b></h3>
                    </div>
                    <table class="table table-bordered table-striped" style="width: 100%">
                        <tr class="text-center">
                            <td>10 X</td>
                            <td>20 X</td>
                            <td>50 X</td>
                            <td>100 X</td>
                            <td>500 X</td>
                            <td>2000 X</td>
                            <td>Other(Remaining)</td>
                            <td>Total</td>
                        </tr>
                        <tr class="text-center">
                            <td><input type="text" name="ten" value="<?= $voucher->ten ?>" class="form-control" id="ten" maxlength="8" data-bv-notempty="true" data-bv-stringlength="true" data-bv-regexp="true" data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Only Numbers are allowed"></td>
                            <td><input type="text" name="twenty" value="<?= $voucher->twenty ?>" class="form-control" id="twenty" maxlength="8" data-bv-notempty="true" data-bv-stringlength="true" data-bv-regexp="true" data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Only Numbers are allowed"></td>
                            <td><input type="text" name="fifty" value="<?= $voucher->fifty ?>" class="form-control" id="fifty" maxlength="8" data-bv-notempty="true" data-bv-stringlength="true" data-bv-regexp="true" data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Only Numbers are allowed"></td>
                            <td><input type="text" name="hundread" value="<?= $voucher->hundread ?>" class="form-control" id="hundread"  maxlength="8" data-bv-notempty="true" data-bv-stringlength="true" data-bv-regexp="true" data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Only Numbers are allowed"></td>
                            <td><input type="text" name="fivehundread" value="<?= $voucher->fivehundread ?>" class="form-control" id="fivehundread" maxlength="8"  data-bv-notempty="true" data-bv-stringlength="true" data-bv-regexp="true" data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Only Numbers are allowed"> </td>
                            <td><input type="text" name="twothousand" value="<?= $voucher->twothousand ?>"  class="form-control" id="twothousand" maxlength="8"  data-bv-notempty="true" data-bv-stringlength="true" data-bv-regexp="true" data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Only Numbers are allowed"></td>
                            <td><input type="text" name="coins" value="<?= $voucher->coins ?>"  class="form-control" id="coins" maxlength="8"  data-bv-notempty="true" data-bv-stringlength="true" data-bv-regexp="true" data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Only Numbers are allowed"></td>
                            <td><input type="text" class="form-control" value="<?= $voucher->amount ?>" id="totals"></td>
                        </tr>
                    </table>
                </div>



            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 heading" style="padding:0;">
                <h4 class="text-center" style="font-size: 20px; text-decoration: underline;padding-bottom:5px;"><?= lang('receiver_details') ?></h4>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="from_acc"><?= lang('to_bank_acc') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php
                            $bl = "";
                            $bl[''] = 'Select To Account';
                            foreach ($bank_acc_no as $row) {
                                $bl[$row->id] = $row->bank_name;
                            }
                            echo form_dropdown('to_acc', $bl, (isset($_POST['to_acc']) ? $_POST['to_acc'] : $voucher->to_acc), 'id="to_acc" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="bank_acc_number"><?= lang('acc_no') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php
                            $b = array();
                            foreach ($bank_acc_no as $row) {
                                if ($voucher->to_acc == $row->id) {
                                    $b[$row->account_no] = $row->account_no;
                                }
                            }
                            echo form_dropdown('receiver_bank_acc_no', $b, (isset($_POST['receiver_bank_acc_no']) ? $_POST['receiver_bank_acc_no'] : $voucher->bank_acc_number), 'id="receiver_bank_acc_no" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("acc_no") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                            ?>
                        </div>
                    </div>

                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="payment_mode"><?= lang('mode_of_trans') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php
//                            $opt = array('neft' => lang('neft'));
//                            echo form_dropdown('mode_of_transfer', $opt, (isset($_POST['mode_of_transfer']) ? $_POST['mode_of_transfer'] : $voucher->payment_mode), 'id="mode_of_trans" data-placeholder="' . lang("select") . '" required="required" class="form-control" style="width:100%;"');
                            ?>

                            <select class="form-control col-lg-5 col-md-5 col-sm-5 col-xs-12" id="mode_of_trans" name="mode_of_transfer">
                                <!--                                <option value="NEFT" >NEFT</option>-->
                            </select>
                        </div>
                    </div>

                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="ref_no"><?= lang('ref_no') ?></label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php echo form_input('ref_no', (isset($_POST['ref_no']) ? $_POST['ref_no'] : $voucher->ref_no), 'class="form-control input-tip" required="required"'); ?>                        
                            <!--<input type="text" name="pay_type_no" class="form-control" placeholder="023456" required=""  >-->
                        </div>
                    </div>

                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for=""><?= lang('dated') ?></label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php echo form_input('dated', (isset($_POST['dated']) ? $_POST['dated'] : $this->sma->hrld($voucher->dated)), 'class="form-control input-tip datetime" id="podates" required="required"'); ?>                        
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="joining_date"><?= lang('balance') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12" style="padding: 0">
                                <?php echo form_input('balance', (isset($_POST['balance']) ? $_POST['balance'] : $voucher->balance), 'class="form-control input-tip" id="balance" required="required"'); ?>                        
                                <!--<input type="text" name="balance" class="form-control" placeholder="55,689.00" required=""  >-->
                            </div>
                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="padding-right: 0">
                                <input type="text" class="form-control" placeholder="Dr." required=""  >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer" style="clear:both;text-align: left;">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                        <b>Narration</b> 
                        <input type="text" name="narration" value="<?php echo $voucher->narration; ?>" class="form-control" >
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-5"><?php echo form_submit('add_transfer', $this->lang->line("submit"), 'id="add_transfer" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>

                </div>
            </div>
        </div>


<?php 

                            echo '<pre>';
print_r($voucher);
echo '</pre>';
//die();
?>

        <?php echo form_close(); ?>

        <!---->
    </div><!--end body div-->

    <!--end foter div-->

</div>


<script type="text/javascript">
    $(document).ready(function (e) {
        $('#addFormx').bootstrapValidator({
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            }, excluded: [':disabled']
        });
        $('select.select').select2({minimumResultsForSearch: 6});
        fields = $('.modal-content').find('.form-control');
        $.each(fields, function () {
            var id = $(this).attr('id');
            var iname = $(this).attr('name');
            var iid = '#' + id;
            if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
//                $("label[for='" + id + "']").append(' *');
                $(document).on('change', iid, function () {
                    $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
                });
            }
        });
    });

    $(document).ready(function () {

        var obj = ['companies', 'department', 'section', 'product_items', 'type', 'brands', 'design', 'style', 'pattern', 'fitting', 'fabric', 'color', 'size', 'per'];
        var objarr = [];
        var i = 0;
        $.each(obj, function (k, v) {
            objarr[i] = myselect2(v);
            objarr[i]();
            i++;
        });
        function myselect2(id) {
            if (id == "per") {
                var pp = $("." + id);
            } else {
                var pp = $("#" + id);
            }
            function get() {
                pp.select2({
                    minimumInputLength: 1,
                    data: [],
                    initSelection: function (element, callback) {
                        $.ajax({
                            type: "get", async: false,
                            url: site_url + "products/getIdAttribute",
                            data: {
                                term: pp.val(),
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                            },
                            dataType: "json",
                            success: function (data) {
                                callback(data[0]);
                            }
                        });
                    },
                    ajax: {
                        url: site_url + "products/getIdAttributes",
                        dataType: 'json',
                        quietMillis: 15,
                        data: function (term, page) {
                            return {
                                term: term,
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                                limit: 10
                            };
                        },
                        results: function (data, page) {
                            if (data.results != null) {
                                return {results: data.results};
                            } else {
                                return {results: [{id: '', text: 'No Match Found'}]};
                            }
                        }
                    }
                });
            }
            return get;
        }
        function getidsx() {
//            alsert($("#selsize").data('id'));
            var $ids = $("#selsize").data('id').split(",");
            var $idvals = "";
            $.each($ids, function (k, v) {
                $idvals += ($("#" + v).val() ? $("#" + v).val() : "") + "-";
            });
            $idvals = $idvals.trim();
            return $idvals;
        }
        $("#selsize").select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site_url + "products/getIdAttribute",
                    data: {
                        term: $(element).val(),
                        tab: $(element).data('tab'),
                        id: getidsx,
                        key: $(element).data('key'),
                    },
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site_url + "products/getIdAttributes",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        tab: $("#selsize").data('tab'),
                        id: getidsx,
                        key: $("#selsize").data('key'),
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'No Match Found'}]};
                    }
                }
            }
        });
    });
    $('#get_acc_no').change(function () {
        var v = $(this).val();
        if (v == "Cash") {
            $('#bank').prop('disabled', 'disabled');
            $('#currency_note').css('display', 'block');
        } else {
            $('#bank').prop('disabled', '');
            $('#currency_note').css('display', 'none');

            $.ajax({
                type: "get",
                async: false,
                data: {id: v},
                url: "<?= site_url('purchases/bankAccountNoByBank') ?>",
                dataType: "json",
                success: function (data) {
                    $('#payeeBankDetails').html('');

                    $.each(data, function (k, val) {
                        $('#bank').val(val.account_no);
                        return;
                    })
                },
            });
        }

    });

    $('#ten,#twenty,#fifty,#hundread,#fivehundread,#twothousand,#coins').keypress(function (e) {
        var key = e.keyCode ? e.keyCode : e.which;
        if (isNaN(String.fromCharCode(key)) && key != 8 && key != 46)
            return false;
    });
    $('#ten,#twenty,#fifty,#hundread,#fivehundread,#twothousand,#coins').keyup(function (evt) {

        $('#amount').attr('readonly', '');
        $('#totals').attr('readonly', '');
        var ten = $('#ten').val();
        var twenty = $('#twenty').val();
        var fifty = $('#fifty').val();
        var hundread = $('#hundread').val();
        var fivehundread = $('#fivehundread').val();
        var twothousand = $('#twothousand').val();
        var coins = $('#coins').val();
        var totals = ((ten * 10) + (twenty * 20) + (fifty * 50) + (hundread * 100) + (fivehundread * 500) + (twothousand * 2000) + (coins * 1));
        $('#totals').val(totals);
        $('#amount').val(totals);
    });

    function enableEdit() {
        $('#contra_code').removeAttr('readonly');
    }


    $('#transfer_type').change(function () {
        var type = $(this).val();
        var sv = '<?= $voucher->mode_of_transfer ?>';
        $('#mode_of_trans').html('');
        $("#mode_of_trans").select2("val", "");
        var data = [];
        if (type == 'Bank Deposits') {
            data = [{"mode": "Cash Deposit"}];
        } else {
            var data = [{"mode": "NEFT"}, {"mode": "IMPS"}, {"mode": "RTGS"}, {"mode": "UPI"}, {"mode": "Other"}];
        }
        var opt = $('<option />');
        opt.val('');
        opt.text('Select Mode');
        $('#mode_of_trans').append(opt);
        $('mode_of_trans').trigger('change');
        $.each(data, function (k, val) {
            var opt = $('<option />');
            opt.val(val.mode);
            if (val.mode == sv) {
                opt.prop('selected', 'selected');
            }
            opt.text(val.mode);
            $('#mode_of_trans').append(opt);
        });
        $("#mode_of_trans").select2("val", sv);
        $('#mode_of_trans').trigger('change');
    });

    $('#transfer_type').trigger('change');


    $('#to_acc').change(function () {
        var v = $(this).val();
        $.ajax({
            type: "get",
            async: false,
            data: {id: v},
            url: "<?= site_url('purchases/bankAccountNoByBank') ?>",
            dataType: "json",
            success: function (data) {
                $('#receiver_bank_acc_no').html('');
                var opt = $('<option />');
                opt.val('');
                opt.text('Select Account No.');
                $('#receiver_bank_acc_no').append(opt);
                $('receiver_bank_acc_no').trigger('change');
                $.each(data, function (k, val) {
                    var opt = $('<option />');
                    opt.val(val.account_no);
                    opt.text(val.account_no);
                    $('#receiver_bank_acc_no').append(opt);
                })
            },
        });
    });

</script>
