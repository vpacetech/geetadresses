<?php
if ($challan) {
    $url = 'challan=1';
} else {
    $url = 'challan=0';
}
?>
<script>
    $(document).ready(function () {
    var oTable = $('#POData').dataTable({
    "aaSorting": [[1, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, - 1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('purchases/getPurchasesReturnWithoutBarcode' . ($warehouse_id ? '/' . $warehouse_id : '')) ?>?' + '<?= $url ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
            aoData.push({
            "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
            });
            $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [{
            "bSortable": false,
                    "mRender": checkbox
            }, {"mRender": null},
            {"mRender": null},
            {"mRender": null},
            {"mRender": currencyFormat},
            {"mRender": currencyFormat},
<?php if ($challan) { ?>
                {"mRender": row_status},
<?php } ?>
            {"bSortable": false}],
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
            var oSettings = oTable.fnSettings();
            nRow.id = aData[0];
//                nRow.className = "purchase_link";
            return nRow;
            },
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
            var total = 0, paid = 0, balance = 0;
            for (var i = 0; i < aaData.length; i++) {
            total += parseFloat(aaData[aiDisplay[i]][3]);
            paid += parseFloat(aaData[aiDisplay[i]][4]);
            balance += parseFloat(aaData[aiDisplay[i]][5]);
            }
            var nCells = nRow.getElementsByTagName('th');
//                nCells[3].innerHTML = currencyFormat(total);
            nCells[4].innerHTML = currencyFormat(paid);
            nCells[5].innerHTML = currencyFormat(balance);
            }
    }).fnSetFilteringDelay().dtFilter([
    ], "footer");
<?php if ($this->session->userdata('remove_pols')) { ?>
        if (localStorage.getItem('poitems')) {
        localStorage.removeItem('poitems');
        }
        if (localStorage.getItem('podiscount')) {
        localStorage.removeItem('podiscount');
        }
        if (localStorage.getItem('potax2')) {
        localStorage.removeItem('potax2');
        }
        if (localStorage.getItem('poshipping')) {
        localStorage.removeItem('poshipping');
        }
        if (localStorage.getItem('poref')) {
        localStorage.removeItem('poref');
        }
        if (localStorage.getItem('powarehouse')) {
        localStorage.removeItem('powarehouse');
        }
        if (localStorage.getItem('ponote')) {
        localStorage.removeItem('ponote');
        }
        if (localStorage.getItem('posupplier')) {
        localStorage.removeItem('posupplier');
        }
        if (localStorage.getItem('postore')) {
        localStorage.removeItem('postore');
        }
        if (localStorage.getItem('pocurrency')) {
        localStorage.removeItem('pocurrency');
        }
        if (localStorage.getItem('poextras')) {
        localStorage.removeItem('poextras');
        }
        if (localStorage.getItem('podate')) {
        localStorage.removeItem('podate');
        }
        if (localStorage.getItem('postatus')) {
        localStorage.removeItem('postatus');
        }
    <?php
    $this->sma->unset_data('remove_pols');
}
?>
    });</script>
<style>
    #POData th, #TOData th {
        width: 20% !important;
    }
</style>

<?php
if ($Owner) {
    echo form_open('purchases/purchase_actions', 'id="action-form"');
}
?>
<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-star"></i>
            <?php
            if ($challan) {
                echo lang('purchase_challan_return1') . ' ' . lang('without_barcode');
            } else {
                echo lang('purchase_return') . ' ' . lang('without_barcode');
            }
            ?>
        </h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right" class="tasks-menus" role="menu" aria-labelledby="dLabel">
                        <!--<li> <a href="<?php // echo site_url('purchases/AddReturnWithBarcode');           ?>" data-toggle="modal" data-target="#myModal" style="margin-left: 10px;"><i class="fa fa-plus-circle"></i> <?= lang('add_purchase_return') . ' ' . lang('without_barcode') ?></a></li>-->
                        <?php if ($challan) { ?>
                            <li><a href="<?= site_url('purchases/AddPurchaseChallanReturnWithoutBarcode') ?>"><i class="fa fa-plus-circle"></i> <?= lang('add_purchase_challan_return') . ' ' . lang('without_barcode') ?></a></li>
                        <?php } else { ?>
                            <li><a href="<?= site_url('purchases/AddPurchaseReturnWithoutBarcode') ?>"><i class="fa fa-plus-circle"></i> <?= lang('add_purchase_return') . ' ' . lang('without_barcode') ?></a></li>
                        <?php } ?>
                    </ul>
                </li>
                <?php if (!empty($warehouses)) { ?>
                    <!--                    <li class="dropdown">
                                            <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-building-o tip" data-placement="left" title="<?= lang("warehouses") ?>"></i></a>
                                            <ul class="dropdown-menu pull-right" class="tasks-menus" role="menu" aria-labelledby="dLabel">
                                                <li><a href="<?= site_url('purchases') ?>"><i class="fa fa-building-o"></i> <?= lang('all_warehouses') ?></a></li>
                                                <li class="divider"></li>
                    <?php
                    foreach ($warehouses as $warehouse) {
                        echo '<li ' . ($warehouse_id && $warehouse_id == $warehouse->id ? 'class="active"' : '') . '><a href="' . site_url('purchases/' . $warehouse->id) . '"><i class="fa fa-building"></i>' . $warehouse->name . '</a></li>';
                    }
                    ?>
                                            </ul>
                                        </li>-->
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?= lang('list_results'); ?></p>

                <div class="table-responsive">
                    <table id="POData" cellpadding="0" cellspacing="0" border="0"
                           class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr class="active">
                                <th style="width:10px !important; text-align: center;">
                                    <input class="checkbox checkft" type="checkbox" name="check"/>
                                </th>
                                <th><?php echo $this->lang->line("entry_date"); ?></th>
                                <th><?php echo $this->lang->line("supplier"); ?></th>

                                <th style="width:100px!important;" ><?php echo $this->lang->line("store"); ?></th>
                                <!--<th><?php echo $this->lang->line("purchase_status"); ?></th>-->
                                <th><?php echo $this->lang->line("grand_total"); ?></th>
                                <th><?php echo $this->lang->line("balance"); ?></th>
                                <?php if ($challan) { ?>
                                    <th>Status</th>
                                <?php } ?>
                                <th style="width:100px;"><?php echo $this->lang->line("actions"); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="10" class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                            </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                            <tr class="active">
<!--                                <th style="min-width:30px; width: 30px; text-align: center;">
                                    <input class="checkbox checkft" type="checkbox" name="check"/>
                                </th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th><?php echo $this->lang->line("grand_total"); ?></th>
                                <th><?php echo $this->lang->line("paid"); ?></th>
                                <th><?php echo $this->lang->line("lr_no"); ?></th>
                                <th><?php echo $this->lang->line("order_date"); ?></th>
                                <th><?php echo $this->lang->line("balance"); ?></th>
                                <th></th>
                                <th></th>
                                <th style="width:100px; text-align: center;"><?php echo $this->lang->line("actions"); ?></th>-->

                                <th style="min-width:30px; width: 30px; text-align: center;">
                                    <input class="checkbox checkft" type="checkbox" name="check"/>
                                </th>
                                <th></th>
                                <th></th>

                                <th></th>
                                <th></th>
                                <th></th>
                                <?php if ($challan) { ?>
                                    <th></th>
                                <?php } ?>

                                <th style="width:100px;"></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>










<div class="modal" id="mModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?= lang('close'); ?></span></button>
                <h4 class="modal-title" id="mModalLabel"><?= lang('add_product') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <!--                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                      <label for="mcode" class="col-sm-4 control-label"><?= lang('product_code') ?> *</label>
                
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="mcode" name="mcode">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="mname" class="col-sm-4 control-label"><?= lang('product_name') ?> *</label>
                
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="mname" name="mname" required>
                                        </div>
                                    </div>
                <?php if ($Settings->tax1) { ?>
                                                                                                                                        <div class="form-group">
                                                                                                                                            <label for="mtax" class="col-sm-4 control-label"><?= lang('product_tax') ?> *</label>
                                                                                                                
                                                                                                                                            <div class="col-sm-8">
                    <?php
                    $tr[""] = "";
                    foreach ($tax_rates as $tax) {
                        $tr[$tax->id] = $tax->name;
                    }
                    echo form_dropdown('mtax', $tr, "", 'id="mtax" class="form-control input-tip select" style="width:100%;" required');
                    ?>
                                                                                                                                            </div>
                                                                                                                                        </div>
                <?php } ?>
                                    <div class="form-group">
                                        <label for="mquantity" class="col-sm-4 control-label"><?= lang('quantity') ?> *</label>
                
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="mquantity" required name="mquantity">
                                        </div>
                                    </div>
                <?php if ($Settings->product_discount) { ?>
                                                                                                                                        <div class="form-group">
                                                                                                                                            <label for="mdiscount"
                                                                                                                                                   class="col-sm-4 control-label"><?= lang('product_discount') ?></label>
                                                                                                                
                                                                                                                                            <div class="col-sm-8">
                                                                                                                                                <input type="text" class="form-control" id="mdiscount" required name="mdiscount">
                                                                                                                                            </div>
                                                                                                                                        </div>
                <?php } ?>
                                    <div class="form-group">
                                        <label for="mprice" class="col-sm-4 control-label"><?= lang('unit_price') ?> *</label>
                
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="mprice" required name="mprice">
                                        </div>
                                    </div>
                                    <table class="table table-bordered table-striped">
                                        <tr>
                                            <th style="width:25%;"><?= lang('net_unit_price'); ?></th>
                                            <th style="width:25%;"><span id="mnet_price"></span></th>
                                            <th style="width:25%;"><?= lang('product_tax'); ?></th>
                                            <th style="width:25%;"><span id="mpro_tax"></span></th>
                                        </tr>
                                    </table>
                                </form>-->

                <div class="box" ng-controller="addProducts">
                    <div class="box-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <?php
                                $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'addproduct');
                                echo form_open_multipart("products/add", $attrib)
                                ?>
                                <input type="text" name="from_order" value="from_order" class="form-control hidden"/>


                                <div class="col-md-5">
                                    <div class="form-group hidden">
                                        <?= lang("product_type", "type") ?>
                                        <?php
                                        if ($product_id != "") {
                                            $opts = array('standard' => lang('standard'), 'combo' => lang('combo'), 'bundle' => lang('bundle'));
                                        } else {
                                            $opts = array('standard' => lang('standard'));
                                        }
                                        echo form_dropdown('type', $opts, (isset($_POST['type']) ? $_POST['type'] : ($product ? $product->type : '')), 'class="form-control" id="type" required="required" ng-model="prod.protype" ng-change="getProdBarcode()"');
                                        ?>
                                    </div>
                                    <div class="form-group all">
                                        <?= lang("Store", "companies") ?>
                                        <div class="input-group col-md-12">

                                            <?php echo form_input('store_id', (isset($_POST['store_id']) ? $_POST['store_id'] : ($product ? $product->store_id : '')), 'class="form-control" default-attrib data-tab="companies" data-id="0" id="companies" placeholder="' . lang("select") . " " . lang("store") . '" required="required" style="width:100%" ng-model="prod.store_id" ng-change="getProdBarcode();"'); ?>
                                            <div class="input-group-addon no-print hidden">
                                                <a href="<?php echo site_url('billers/add'); ?>" data-toggle="modal" data-target="#myModal" class="external">
                                                    <i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group all">
                                        <?= lang("Department", "department") ?>
                                        <div class="input-group col-md-12">
                                            <?php
                                            echo form_input('department', (isset($_POST['department']) ? $_POST['department'] : ($product ? $product->department : '')), 'class="form-control" default-attrib data-tab="department" data-key="store_id" data-id="companies" id="department" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ng-model="prod.dept" ng-change="getProdName();getProdBarcode();getProductMargin();"');
                                            ?>
                                            <div class="input-group-addon no-print hidden">
                                                <a href="<?php echo site_url('system_settings/AddProduct_para/department'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group all">
                                        <?= lang("Section", "section") ?>
                                        <div class="input-group col-md-12">
                                            <?php
                                            echo form_input('section', (isset($_POST['section']) ? $_POST['section'] : ($product ? $product->section : '')), 'class="form-control" default-attrib data-tab="section" id="section" data-key="department_id" data-id="department" placeholder="' . lang("select") . " " . lang("section") . '" ng-model="prod.section_id" ng-change="getProductMargin();" required="required" style="width:100%"');
                                            ?>
                                            <div class="input-group-addon no-print hidden">
                                                <a href="<?php echo site_url('system_settings/AddProduct_para/section'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group all">
                                        <?= lang("Product_items", "product_items") ?>
                                        <div class="input-group col-md-12">
                                            <?php
                                            echo form_input('product_items', (isset($_POST['product_items']) ? $_POST['product_items'] : ($product ? $product->product_items : '')), 'class="form-control" default-attrib data-tab="product_items" data-key="section_id" data-id="section" id="product_items" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%" ng-model="prod.product_item" ng-change="getProdName();getProductMargin();"');
                                            ?>
                                            <div class="input-group-addon no-print hidden">
                                                <a href="<?php echo site_url('system_settings/AddProduct_para/product_items'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group all">
                                        <?= lang("Type", "type") ?>
                                        <div class="input-group col-md-12">
                                            <?php
                                            echo form_input('type_id', (isset($_POST['type_id']) ? $_POST['type_id'] : ($product ? $product->type_id : '')), 'class="form-control" default-attrib data-tab="type" id="type_id" data-key="product_items_id" data-id="product_items" placeholder="' . lang("select") . " " . lang("type") . '" required="required" style="width:100%" ng-model="prod.type" ng-change="getProdName();getProductMargin();"');
                                            ?>
                                            <div class="input-group-addon no-print hidden">
                                                <a href="<?php echo site_url('system_settings/AddProduct_para/type'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group all">
                                        <?= lang("Brands", "brands") ?>
                                        <div class="input-group col-md-12">
                                            <?php
                                            echo form_input('brands', (isset($_POST['brands']) ? $_POST['brands'] : ($product ? $product->brands : '')), 'class="form-control" default-attrib data-tab="brands" id="brands" data-key="type_id" data-id="type_id" placeholder="' . lang("select") . " " . lang("brands") . '" required="required" ng-model="prod.brands_id" ng-change="getProductMargin();"  style="width:100%"');
                                            ?>
                                            <div class="input-group-addon no-print hidden">
                                                <a href="<?php echo site_url('system_settings/AddProduct_para/brands'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group standard">
                                        <?= lang("Design", "design") ?>
                                        <div class="input-group col-md-12">
                                            <?php
                                            echo form_input('design', (isset($_POST['design']) ? $_POST['design'] : ($product ? $product->design : '')), 'class="form-control" default-attrib data-tab="design" id="design" data-key="brands_id" data-id="brands" placeholder="' . lang("select") . " " . lang("design") . '" required="required" style="width:100%"');
                                            ?>
                                            <div class="input-group-addon no-print hidden">
                                                <a href="<?php echo site_url('system_settings/AddProduct_para/design'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group standard">
                                        <?= lang("Style", "style") ?>
                                        <div class="input-group col-md-12">
                                            <?php
                                            echo form_input('style', (isset($_POST['style']) ? $_POST['style'] : ($product ? $product->style : '')), 'class="form-control" default-attrib data-tab="style" id="style" data-key="design_id" data-id="design" placeholder="' . lang("select") . " " . lang("style") . '" required="required" style="width:100%"');
                                            ?>
                                            <div class="input-group-addon no-print hidden">
                                                <a href="<?php echo site_url('system_settings/AddProduct_para/style'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group standard">
                                        <?= lang("Pattern", "pattern") ?>
                                        <div class="input-group col-md-12">
                                            <?php
                                            echo form_input('pattern', (isset($_POST['pattern']) ? $_POST['pattern'] : ($product ? $product->pattern : '')), 'class="form-control" default-attrib data-tab="pattern" data-key="style_id" data-id="style" id="pattern" placeholder="' . lang("select") . " " . lang("pattern") . '" required="required" style="width:100%"');
                                            ?>
                                            <div class="input-group-addon no-print hidden">
                                                <a href="<?php echo site_url('system_settings/AddProduct_para/pattern'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group standard">
                                        <?= lang("Fitting", "fitting") ?>
                                        <div class="input-group col-md-12">
                                            <?php
                                            echo form_input('fitting', (isset($_POST['fitting']) ? $_POST['fitting'] : ($product ? $product->fitting : '')), 'class="form-control" default-attrib data-tab="fitting" data-key="pattern_id" data-id="pattern" id="fitting" placeholder="' . lang("select") . " " . lang("fitting") . '" required="required" style="width:100%"');
                                            ?>
                                            <div class="input-group-addon no-print hidden">
                                                <a href="<?php echo site_url('system_settings/AddProduct_para/fitting'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group standard">
                                        <?= lang("Fabric", "fabric") ?>
                                        <div class="input-group col-md-12">
                                            <?php
                                            echo form_input('fabric', (isset($_POST['fabric']) ? $_POST['fabric'] : ($product ? $product->fabric : '')), 'class="form-control" default-attrib data-tab="fabric" id="fabric" data-key="fitting_id" data-id="fitting" placeholder="' . lang("select") . " " . lang("fabric") . '" required="required" style="width:100%"');
                                            ?>
                                            <div class="input-group-addon no-print hidden">
                                                <a href="<?php echo site_url('system_settings/AddProduct_para/fabric'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-md-offset-1">
                                    <div class="form-group all">
                                        <?= lang("product_name", "name") ?>
                                        <?= form_input('name', (isset($_POST['name']) ? $_POST['name'] : ($product ? $product->name : '')), 'class="form-control" id="name" required="required" readonly ng-model="prod.name"'); ?>
                                    </div>
                                    <div class="form-group all hidden">
                                        <?= lang("product_code", "code") ?>
                                        <?= form_input('code', (isset($_POST['code']) ? $_POST['code'] : ($product ? $product->code : '')), 'class="form-control" id="code"  required="required" readonly ng-model="prod.barcode" ') ?>
                                        <span class="help-block"><?= lang('you_scan_your_barcode_too') ?></span>
                                    </div>
                                    <?= form_hidden('barcode_symbology', (isset($_POST['barcode_symbology']) ? $_POST['barcode_symbology'] : ($product ? $product->barcode_symbology : 'code128')), 'class="form-control" id="barcode_symbology"  required="required" readonly ') ?>
                                    <?php if ($Settings->tax1) { ?>
                                        <div class="form-group standard">
                                            <?= lang("product_tax", "tax_rate") ?>
                                            <?php
                                            $tr[""] = "";
                                            foreach ($tax_rates as $tax) {
                                                $tr[$tax->id] = $tax->name;
                                            }
                                            echo form_dropdown('tax_rate', $tr, (isset($_POST['tax_rate']) ? $_POST['tax_rate'] : ($product ? $product->tax_rate : $Settings->default_tax_rate)), 'class="form-control select" id="tax_rate" placeholder="' . lang("select") . ' ' . lang("product_tax") . '" style="width:100%"');
                                            ?>
                                        </div>
                                        <div class="form-group standard">
                                            <?= lang("tax_method", "tax_method") ?>
                                            <?php
                                            $tm = array('0' => lang('inclusive'), '1' => lang('exclusive'));
                                            echo form_dropdown('tax_method', $tm, (isset($_POST['tax_method']) ? $_POST['tax_method'] : ($product ? $product->tax_method : '')), 'class="form-control select" id="tax_method" placeholder="' . lang("select") . ' ' . lang("tax_method") . '" style="width:100%"');
                                            ?>
                                        </div>
                                    <?php } ?>
                                    <div class="form-group standard">
                                        <?= lang("supplier", "supplier") ?>
                                        <div class="row" id="supplier-con">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <?php echo form_input('supplier', (isset($_POST['supplier']) ? $_POST['supplier'] : ''), 'class="form-control hidden" id="supplier12" placeholder="" style="width:100%;"'); ?>
                                                <?php echo form_input('supplier_name', (isset($_POST['supplier']) ? $_POST['supplier'] : ''), 'class="form-control" id="supplier_name" readonly placeholder="" style="width:100%;"'); ?>
                                                <?php // echo form_input('supplier', 'SUP23', 'class="form-control " sizesel data-tab="size" data-key="department_id-section_id-product_items_id" data-id="department,section,product_items" id="" placeholder="' . lang("select") . " " . lang("size") . '" required="required" style="width:100%"');?>
                                                <?php
//                                                $b2[""] = "";
//                                                if (!empty($suppliers)) {
//                                                    foreach ($suppliers as $supplier) {
//                                                        $b2[$supplier->id] = $supplier->code;
//                                                    }
//                                                }
//                                                echo form_dropdown('supplier', $b2, (isset($_POST['text']) ? $_POST['text'] : ''), 'id="supplier12" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("supplier") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                                ?>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="form-group standard">
                                        <?= lang("product_image", "product_image") ?> max (<?= byte_format($this->Settings->iwidth * $this->Settings->iheight) ?>)
                                        <input id="product_image" type="file" name="product_image" data-show-upload="false"
                                               data-show-preview="false" accept="image/*" class="form-control file">
                                    </div>

                                    <div id="img-details"></div>
                                    <div class="form-group standard">
                                        <?= lang("Color", "color") ?>
                                        <div class="row">
                                            <div class="col-md-3"> &nbsp;&nbsp;<input type="radio" name="colortype" ng-model="prod.colortype" icheck value="Single"/>&nbsp;&nbsp;&nbsp;&nbsp;Single</div>
                                            <div class="col-md-3 colorsingle hide">
                                                <?php echo form_dropdown('colorsingle', $color, (isset($_POST['colorsingle']) ? $_POST['colorsingle'] : ($product ? $product->color : '')), 'class="form-control" select2 id="color" placeholder="' . lang("select") . " " . lang("color") . '" style="width:100%"'); ?>
                                            </div>
                                            <div class="col-md-2 colorsingle hide">
                                                <input type="text" id="colorqty" name="colorqty" onlyno class="form-control" ng-model="prod.colorqty" ng-change="getQty('#colorqty')"/>
                                                <!--<p class="label label-danger hide"> Product Qty and color qty must be same.</p>-->
                                            </div>
                                            <div class="col-md-4 colorsingle hide">
                                                <input type="text" id="colorcode" name="colorcode" onlyno readonly class="form-control" ng-model="prod.colorcode" ng-change="getQty('#colorqty')"/>
                                                <input type="text" id="codet" name="codet" onlyno class="hidden form-control"/>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-4"> &nbsp;&nbsp;<input type="radio" name="colortype" ng-model="prod.colortype" icheck value="Assorted"/>&nbsp;&nbsp;&nbsp;&nbsp;Assorted</div>
                                            <div class="col-md-8 colorassorted hide">
                                                <?php
                                                echo form_dropdown('colorassorted[]', $color, (isset($_POST['colorassorted']) ? $_POST['colorassorted'] : ($product ? $product->colorassorted : '')), 'class="form-control" multiple select2 id="mulcolor" ng-model="prod.colorassorted" placeholder="' . lang("select") . " " . lang("color") . '"style="width:100%"');
                                                ?>
                                                <br/>
                                                <!--{{prod.colorassorted}}-->
                                                <div class="row" style="padding-top: 20px;">
                                                    <div class="col-md-2" style="padding-right: 0px;" ng-repeat="n in prod.colorassoarr">
                                                        <input type="text" name="colorqty[]" onlyno ng-model="n.qty" ng-blur="getQtyCal($index, n.qty)" class="form-control"/></br>
                                                        <input type="text" name="colorcode[]" onlyno ng-model="n.colorcode"  class="form-control"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <?= lang("Size", "size") ?>
                                        <div class="row">
                                            <div class="col-md-3"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="sizeangle" ng-model="prod.sizeangle" ng-change="getProdName()" <?= ($product && $product->sizeangle == "HT" ? "checked" : '') ?> value="HT"/>&nbsp;&nbsp;&nbsp;&nbsp;Height</div>
                                            <div class="col-md-3"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="sizeangle" ng-model="prod.sizeangle" ng-change="getProdName()" <?= ($product && $product->sizeangle == "WT" ? "checked" : '') ?> value="WT"/>&nbsp;&nbsp;&nbsp;&nbsp;Width</div>
                                            <div class="col-md-3"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="sizeangle" ng-model="prod.sizeangle" ng-change="getProdName()" <?= ($product && $product->sizeangle == "NZ" ? "checked" : '') ?> value="NZ"/>&nbsp;&nbsp;&nbsp;&nbsp;No Size</div>
                                        </div>
                                        <br/>
                                        <div class="row" >
                                            <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" name="sizetype" ng-model="prod.sizetype" ng-change="getProdName()" checked="" value="Single"/>&nbsp;&nbsp;&nbsp;&nbsp;Single</div>
                                            <div class="col-md-4" ng-init='size =<?= json_encode($sizes) ?>' ng-show="prod.sizetype == 'Single'">
                                                <div class="col-md-9" style="padding: 0px"><?php
                                                    echo form_input('singlesize', (isset($_POST['singlesize']) ? $_POST['singlesize'] : ($product ? $product->size : '')), 'class="form-control " sizesel data-tab="size" data-key="department_id-section_id-product_items_id" data-id="department,section,product_items" id="size" placeholder="' . lang("select") . " " . lang("size") . '" required="required" style="width:100%" ng-model="prod.singlesize" ng-change="getProdName();"');
                                                    ?>
                                                </div>
                                                <div class="col-md-2"> {{size[prod.singlesize].code?size[prod.singlesize].code+'\"':""}}</div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row standard" >
                                            <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" name="sizetype" ng-model="prod.sizetype" ng-change="getProdName()" value="Multiple"/>&nbsp;&nbsp;&nbsp;&nbsp;Multiple</div>
                                            <div class="col-md-4" ng-init='size1 =<?= json_encode($sizes) ?>' ng-show="prod.sizetype == 'Multiple'">
                                                <div class="col-md-9" style="padding: 0px"><?php
                                                    echo form_input('multisizef', (isset($_POST['multisizef']) ? $_POST['multisizef'] : ''), 'class="form-control " sizesel data-tab="size" data-key="department_id-section_id-product_items_id" data-id="department,section,product_items" id="multisizef" placeholder="' . lang("select") . " " . lang("size") . '" required="required" style="width:100%" ng-model="prod.multisizef" ng-change="getProdName();"');
                                                    ?></div>
                                                <div class="col-md-2"> {{size1[prod.multisizef].code?size1[prod.multisizef].code+'\"':""}}</div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-1" ng-show="prod.sizetype == 'Multiple'">To</div>
                                            <div class="col-md-4" ng-init='size2 =<?= json_encode($sizes) ?>' ng-show="prod.sizetype == 'Multiple'">
                                                <div class="col-md-9" style="padding: 0px"><?php
                                                    echo form_input('multisizet', (isset($_POST['multisizet']) ? $_POST['multisizet'] : ''), 'class="form-control " sizesel data-tab="size" data-key="department_id-section_id-product_items_id" data-id="department,section,product_items" id="multisizet" placeholder="' . lang("select") . " " . lang("size") . '" required="required" style="width:100%" ng-model="prod.multisizet"');
                                                    ?></div>
                                                <div class="col-md-2"> {{size2[prod.multisizet].code?size2[prod.multisizet].code+'\"':""}}</div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row standard">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label class="control-label" for="unit"><?= lang("product_qty") ?></label>
                                                <?php
                                                foreach ($warehouses as $warehouse) {
                                                    //$whs[$warehouse->id] = $warehouse->name;
                                                    if ($this->Settings->racks) {
                                                        echo "<div class='row'><div class='col-md-12'>";
                                                        echo form_hidden('wh_' . $warehouse->id, $warehouse->id) . form_input('wh_qty_' . $warehouse->id, (isset($_POST['wh_qty_' . $warehouse->id]) ? $_POST['wh_qty_' . $warehouse->id] : ''), 'class="form-control" id="wh_qtys" placeholder="' . lang('quantity') . '" ng-model="prod.qty" onlyno');
//                                        echo "</div><div class='col-md-6'>";
//                                        echo form_input('rack_' . $warehouse->id, (isset($_POST['rack_' . $warehouse->id]) ? $_POST['rack_' . $warehouse->id] : ''), 'class="form-control" id="rack_' . $warehouse->id . '" placeholder="' . lang('rack') . '"');
                                                        echo "</div></div>";
                                                    } else {
                                                        echo form_hidden('wh_' . $warehouse->id, $warehouse->id) . form_input('wh_qty_' . $warehouse->id, (isset($_POST['wh_qty_' . $warehouse->id]) ? $_POST['wh_qty_' . $warehouse->id] : ''), 'class="form-control" id="wh_qty_' . $warehouse->id . '" placeholder="' . lang('quantity') . '" ng-model="prod.qty" onlyno');
                                                    }


//                                    if ($this->Settings->racks) {
//                                        echo "<div class='row'><div class='col-md-6'>";
//                                        echo form_hidden('wh_' . $warehouse->id, $warehouse->id) . form_input('wh_qty_' . $warehouse->id, (isset($_POST['wh_qty_' . $warehouse->id]) ? $_POST['wh_qty_' . $warehouse->id] : ''), 'class="form-control" id="wh_qty_' . $warehouse->id . '" placeholder="' . lang('quantity') . '" ng-model="prod.qty" onlyno');
//                                        echo "</div><div class='col-md-6'>";
//                                        echo form_input('rack_' . $warehouse->id, (isset($_POST['rack_' . $warehouse->id]) ? $_POST['rack_' . $warehouse->id] : ''), 'class="form-control" id="rack_' . $warehouse->id . '" placeholder="' . lang('rack') . '"');
//                                        echo "</div></div>";
//                                    } else {
//                                        echo form_hidden('wh_' . $warehouse->id, $warehouse->id) . form_input('wh_qty_' . $warehouse->id, (isset($_POST['wh_qty_' . $warehouse->id]) ? $_POST['wh_qty_' . $warehouse->id] : ''), 'class="form-control" id="wh_qty_' . $warehouse->id . '" placeholder="' . lang('quantity') . '" ng-model="prod.qty" onlyno');
//                                    }
//                                    echo '</div>';
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("unit_per", "unit") ?>
                                                <?= form_input('unit', (isset($_POST['unit']) ? $_POST['unit'] : ($product ? $product->uper : '')), 'class="form-control" id="unit" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" required="required" style="width:100%" '); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row standard">
                                        <div class="col-md-8">
                                            <div class="form-group all">
                                                <?= form_input('squantity', (isset($_POST['price']) ? $_POST['price'] : ($product ? $this->sma->formatDecimal($product->price) : '')), 'class="form-control tip" id="squantity"') ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= form_input('sunit', (isset($_POST['sunit']) ? $_POST['sunit'] : ($product ? $product->uper : '')), 'class="form-control" id="sunit" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" style="width:100%" '); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <script>
                                        $('#squantity').change(function () {
                                        var squantity = $('#squantity').val();
                                        var wh_qtys = $('#wh_qtys').val();
                                        if (wh_qtys % squantity != 0 && squantity != "") {
                                        bootbox.alert('Please Enter Valid Secondary Quentity');
                                        $('#squantity').val("");
                                        }
                                        });
                                    </script>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group all">
                                                <?= lang("product_cost", "cost") ?>
                                                <?= form_input('cost', (isset($_POST['cost']) ? $_POST['cost'] : ($product ? $this->sma->formatDecimal($product->cost) : '')), 'class="form-control tip" id="cost" required="required" ng-model="prod.cost" ng-blur="getProductMargin()"') ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group all">
                                                <?= lang("product_per", "cper") ?>
                                                <?= form_input('cper', (isset($_POST['cper']) ? $_POST['cper'] : ($product ? $product->cper : '')), 'class="form-control" id="cper" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" required="required" style="width:100%" '); ?>
                                            </div>
                                        </div>
                                    </div>  
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group all">
                                                <?= lang("product_price", "price") ?>
                                                <?= form_input('price', (isset($_POST['price']) ? $_POST['price'] : ($product ? $this->sma->formatDecimal($product->price) : '')), 'class="form-control tip" id="price" ng-model="prod.price" required="required"') ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group all">
                                                <?= lang("product_per", "pper") ?>
                                                <div class="input-group col-md-12">
                                                    <?= form_input('pper', (isset($_POST['pper']) ? $_POST['pper'] : ($product ? $product->pper : '')), 'class="form-control" id="pper" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" required="required" style="width:100%"'); ?>
                                                    <div class="input-group-addon no-print hidden">
                                                        <a href="<?php echo site_url('system_settings/AddProduct_para/per'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group standard ">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" <?= (isset($_POST['ratetype']) && $_POST['ratetype'] == "MRP" ? 'checked' : 'checked') ?> name="ratetype"  value="MRP"/>&nbsp;&nbsp;&nbsp;&nbsp;MRP</div>
                                                    <div class="col-md-3">
                                                        <?php echo form_input('mrprate', (isset($_POST['mrprate']) ? $_POST['mrprate'] : ''), 'class="form-control" id="mrprate" placeholder="" style="width:100%;"'); ?>
                                                    </div>
                                                    <!-- <div class="col-md-3">
                                                    <?= lang("Rate", "singlerate") ?>   
                                                    </div>
                                                    <div class="col-md-1 hidden">
                                                    <?= lang("Per", "Per") ?> </div>
                                                    <div class="col-md-4"><?= form_input('rateper', (isset($_POST['rateper']) ? $_POST['rateper'] : ($product ? $product->rateper : '')), 'class="form-control" default-attrib data-tab="per" id="rateper" placeholder="' . lang("select") . " " . lang("Unit") . '" required="required" style="width:100%" '); ?>
                                                    </div> -->
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div class="row">
                                            <!--<div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" <?= (isset($_POST['ratetype']) && $_POST['ratetype'] == "Single" ? 'checked' : '') ?> name="ratetype" value="Single"/>&nbsp;&nbsp;&nbsp;&nbsp;Single</div>-->
                                            <!--<div class="col-md-3"><?php echo form_input('singlerate', (isset($_POST['singlerate']) ? $_POST['singlerate'] : ''), 'class="form-control" id="singlerate" placeholder="" style="width:100%;" '); ?></div>-->

                                        </div>
                                        <br/>
                                        <div class="row hidden">
                                            <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" <?= (isset($_POST['ratetype']) && $_POST['ratetype'] == "Multiple" ? 'checked' : '') ?> name="ratetype" value="Multiple"/>&nbsp;&nbsp;&nbsp;&nbsp;Multiple</div>
                                            <div class="col-md-3">
                                                <div class="col-md-12" style="padding: 0px"><?php echo form_input('mulratef', (isset($_POST['mulratef']) ? $_POST['mulratef'] : ''), 'class="form-control" id="mulratef" placeholder="" style="width:100%;"'); ?></div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-md-3">To</div>
                                            <div class="col-md-3" ng-init='size2 =<?= json_encode($sizes) ?>'>
                                                <div class="col-md-12" style="padding: 0px"><?php echo form_input('mulratet', (isset($_POST['mulratet']) ? $_POST['mulratet'] : ''), 'class="form-control" id="mulratet" placeholder="" style="width:100%;"'); ?></div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="combo" style="display:none;">
                                        <div class="form-group">
                                            <?= lang("add_product", "add_item") . ' (' . lang('not_with_variants') . ')'; ?>
                                            <?php echo form_input('add_item', '', 'class="form-control ttip" id="add_item" data-placement="top" data-trigger="focus" data-bv-notEmpty-message="' . lang('please_add_items_below') . '" placeholder="' . $this->lang->line("add_item") . '"'); ?>
                                        </div>
                                        <div class="control-group table-group">
                                            <label class="table-label" for="combo">{{prod.protype}} <?= lang("products"); ?></label>
                                            <div class="controls table-controls">
                                                <table id="prTable" class="table items table-striped table-bordered table-condensed table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th class="col-md-5 col-sm-5 col-xs-5"><?= lang("product_name") . " (" . $this->lang->line("product_code") . ")"; ?></th>
                                                            <th class="col-md-2 col-sm-2 col-xs-2"><?= lang("quantity"); ?></th>
                                                            <th class="col-md-3 col-sm-3 col-xs-3"><?= lang("unit_price"); ?></th>
                                                            <th class="col-md-1 col-sm-1 col-xs-1 text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="digital" style="display:none;">
                                        <div class="form-group digital">
                                            <?= lang("digital_file", "digital_file") ?>
                                            <input id="digital_file" type="file" name="digital_file" data-show-upload="false"
                                                   data-show-preview="false" class="form-control file">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php echo form_submit('add_product', "Save", 'class="btn btn-primary" '); ?>
                                    </div>
                                </div>
                                <?= form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--            <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="addItemManually"><?= lang('submit') ?></button>
                        </div>-->
        </div>
    </div>
</div>