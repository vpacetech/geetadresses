<style>
    .close{
        font-size: 19px !important;
    }
    .heading{
        margin-top:14px !important;
        border-top: 1px solid black;
    }
    .heading1{
        padding-top: 7px;
    }
    .modal-header{
        border-bottom: none !important;
    }
</style>
<?php echo $modal_js ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
        </div>
        <?php
        $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'addFormx');
        echo form_open_multipart("purchases/SaveContraVoucher", $attrib);
        ?>
        <div class="modal-body">
            <div >
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <h2 style="margin:0;padding-top: 10px !important;">
                        <?= lang('contravoucher_ad') ?>
                    </h2>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
                    <b style='font-size: 20px;'><?= $Settings->site_name ?></b>
                    <!--<h4>Kavathe - Mahankal</h4>-->
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 "></div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 heading" >
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 text-right">
                        <button type="button" onclick="enableEdit()"><?= lang('define_my_own_Code') ?></button>
                    </div>
                    <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="joining_date">  <?= lang('contra_no') ?> </label>

                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 heading1">
                        <input type="text" name="payment_no" id="contra_code" value="<?php echo 'GD-CT' . $latest_pay_no ?>" class="form-control" readonly>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                    <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="joining_date"><?= lang('date') ?> </label>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 heading1">
                        <?php echo form_input('date', date('d/m/Y') . '(' . date('l') . ')', 'class="form-control tip" id="sender_address" data-bv-notempty="true"  readonly'); ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group">
                    <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="joining_date">  <?= lang('user') ?>  </label>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 heading1">
                        <?php
                        $bl = "";
                        $bl[''] = "Select User";
                        foreach ($users as $row) {
                            $bl[$row->id] = $row->first_name . ' ' . $row->last_name;
                        }
                        echo form_dropdown('user_name', $bl, set_value('user_name', ''), 'data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                        ?>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                    <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="joining_date"> <?= lang('time') ?> </label>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 heading1">
                        <?php echo form_input('time', date('h:i a'), 'class="form-control tip" id="sender_address" data-bv-notempty="true" readonly'); ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 heading" style="padding:0;">
                <h4 class="text-center" style="font-size: 20px; padding-bottom:5px;"><span style="text-decoration: underline;">Depositer </span>&nbsp; <span style="text-decoration: underline;">Details </span></h4>
                <!--<h4 class="text-center" style="font-size: 20px; text-decoration: underline;padding-bottom:5px;"><?= lang('depositor_details') ?></h4>-->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="payfor">1. <?= lang('transfer_type') ?> :</label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <select class="form-control col-lg-5 col-md-5 col-sm-5 col-xs-12" name="transfer_type" id="transfer_type">
                                <option value="">Select Transfer Type</option>
                                <option value="Bank Deposits">Bank Deposits</option>
                                <option value="Bank Transfer">Bank Transfer</option>
                            </select>                             
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12">2. <?= lang('from_account') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">

                            <?php
//                            $bl = "";
//                            foreach ($store as $row) {
//                                $bl[$row->id] = $row->company;
//                            }
                            $b2 = array();
                            $b2[''] = 'Select From Account';
//                            $b2['Payee'] = 'Payee';
                            foreach ($bank_acc_no as $row) {
                                $b2[$row->id] = $row->bank_name;
                            }
                            $b2['Cash'] = 'Cash';
                            echo form_dropdown('from_acc', $b2, set_value('from_acc', ''), 'data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" id="get_acc_no" class="form-control input-tip select" style="width:100%;"');
                            ?>

                            <?php
//                              $st = array('cash' => lang('cash'), 'bank' => lang('bank'));
//                                echo form_dropdown('from_acc', $st, '', 'class="form-control input-tip"'); 
                            ?>

                            <!--echo form_dropdown('account', $bl, set_value('payfor', ''), 'id="qubiller" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" class="form-control input-tip select" style="width:100%;"');-->

                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="againstno">3. <?= lang('ref_no') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php
                            $rf = "";
                            foreach ($ref_no as $row) {
                                $rf[$row->reference_no] = $row->reference_no;
                            }
                            echo form_dropdown('against_ref_no', $rf, (isset($_POST['against_ref_no']) ? $_POST['against_ref_no'] : $voucher->against_ref_no), 'id="qubiller" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                            ?>
                            <?php
//                                    echo form_input('against_ref_no', set_value('against_ref_no', ''), 'class="form-control" required="required" '
//                                            . 'data-bv-notempty="true"
//                               data-bv-notempty-message="Reference no cannot be empty"
//                               data-bv-stringlength="true"
//                               data-bv-regexp="true"
//                               data-bv-regexp-regexp="^[0-9]+$"
//                               data-bv-regexp-message="Reference no can only consist of digits"');
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="joining_date">4. <?= lang('bank_acc_no') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <input type="text" name="bank_acc_no" id="bank" class="form-control" >
                            <?php
//                                    echo form_input('bank_acc_no', set_value('bank_acc_no', ''), 'class="form-control" required="required" '
//                                            . 'data-bv-notempty="true"
//                               data-bv-notempty-message="Bank Account no. cannot be empty"
//                               data-bv-stringlength="true"
//                               data-bv-regexp="true"
//                               data-bv-regexp-regexp="^[0-9]+$"
//                               data-bv-regexp-message="Bank Account no. can only consist of digits"');
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="dated">5. <?= lang('dated') ?>* </label>

                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php echo form_input('datedd', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="dated" required="required"'); ?>                        
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="amount">6. <?= lang('amount') ?>* </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">

                            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12" style="padding: 0">
                                <input type="text" name="amount" class="form-control" id="amount" required=""  >
                            </div>
                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="padding-right: 0">
                                <input type="text"  class="form-control" placeholder="cr." disabled>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive" id="currency_note" style="display:none">
                    <hr>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                        <h3><b><?= lang("currency_note_details") ?></b></h3>
                    </div>
                    <table class="table table-bordered table-striped" style="width: 100%">
                        <tr class="text-center">
                            <td>10 X</td>
                            <td>20 X</td>
                            <td>50 X</td>
                            <td>100 X</td>
                            <td>500 X</td>
                            <td>2000 X</td>
                            <td>Other(Remaining)</td>
                            <td>Total</td>
                        </tr>
                        <tr class="text-center">
                            <td><input type="text" name="ten" class="form-control" id="ten" maxlength="8"></td>
                            <td><input type="text" name="twenty" class="form-control" id="twenty" maxlength="8"></td>
                            <td><input type="text" name="fifty" class="form-control" id="fifty" maxlength="8"></td>
                            <td><input type="text" name="hundread" class="form-control" id="hundread" maxlength="8"></td>
                            <td><input type="text" name="fivehundread" class="form-control" id="fivehundread" maxlength="8"></td>
                            <td><input type="text" name="twothousand" class="form-control" id="twothousand" maxlength="8"></td>
                            <td><input type="text" name="coins" class="form-control" id="coins" maxlength="8"></td>
                            <td><input type="text" class="form-control" id="totals"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 heading" style="padding:0;">
                <h4 class="text-center" style="font-size: 20px; padding-bottom:5px;"><span style="text-decoration: underline;">Receiver </span>&nbsp; <span style="text-decoration: underline;">Details </span></h4>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">

                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="from_acc">7. <?= lang('to_acc') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php
                            $bl = "";
                            $bl[''] = 'Select To Account';
                            foreach ($bank_acc_no as $row) {
                                $bl[$row->id] = $row->bank_name;
                            }
                            echo form_dropdown('to_acc', $bl, set_value('to_acc', $row->bank_name), 'data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" id="to_acc" class="form-control input-tip select" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="bank_acc_number">8. <?= lang('acc_no') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php
                            $b = array();
                            $b[''] = "Select Account";
//                            foreach ($bank_acc_no as $row) {
//                                $b[$row->account_no] = $row->account_no;
//                            }
                            echo form_dropdown('receiver_bank_acc_no', $b, set_value('payfor', $row->receiver_bank_acc_no), 'id="receiver_bank_acc_no" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("acc_no") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                            ?>
                        </div>
                    </div>


                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="payment_mode">9. <?= lang('mode_of_trans') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <select class="form-control col-lg-5 col-md-5 col-sm-5 col-xs-12" id="mode_of_trans" name="mode_of_transfer">
                                <option value="NEFT">NEFT</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="ref">10. <?= lang('refer_no') ?>*</label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <input type="text" name="ref_no" class="form-control" required="" id="ref" >
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="joining_date">11. <?= lang('dated') ?>*</label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php echo form_input('dated', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="joining_date" required="required"'); ?>                        
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="balance">12. <?= lang('balance') ?>* </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12" style="padding: 0">
                                <input type="text" name="balance" class="form-control" required="" id="balance" >
                            </div>
                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="padding-right: 0">
                                <input type="text" class="form-control" placeholder="Dr." required=""  >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer" style="clear:both;text-align: left;">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12 control-label ">
                            <b>Narration </b>
                        </div>
                        <div class="col-lg-9 col-sm-9 col-md-9 col-xs-12">
                            <input type="text" name="narration" class="form-control" >
                                <!--<text type="text" name="narration">-->
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-5"><?php echo form_submit('add_transfer', $this->lang->line("submit"), 'id="add_transfer" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>

                </div>
            </div>
        </div>




        <?php echo form_close(); ?>

        <!---->
    </div><!--end body div-->

    <!--end foter div-->

</div>


<script type="text/javascript">
    $(document).ready(function (e) {
        $('#addFormx').bootstrapValidator({
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            }, excluded: [':disabled']
        });
        $('select.select').select2({minimumResultsForSearch: 6});
        fields = $('.modal-content').find('.form-control');
        $.each(fields, function () {
            var id = $(this).attr('id');
            var iname = $(this).attr('name');
            var iid = '#' + id;
            if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
//                $("label[for='" + id + "']").append(' *');
                $(document).on('change', iid, function () {
                    $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
                });
            }
        });
    });

    $(document).ready(function () {

        var obj = ['companies', 'department', 'section', 'product_items', 'type', 'brands', 'design', 'style', 'pattern', 'fitting', 'fabric', 'color', 'size', 'per'];
        var objarr = [];
        var i = 0;
        $.each(obj, function (k, v) {
            objarr[i] = myselect2(v);
            objarr[i]();
            i++;
        });
        function myselect2(id) {
            if (id == "per") {
                var pp = $("." + id);
            } else {
                var pp = $("#" + id);
            }
            function get() {
                pp.select2({
                    minimumInputLength: 1,
                    data: [],
                    initSelection: function (element, callback) {
                        $.ajax({
                            type: "get", async: false,
                            url: site_url + "products/getIdAttribute",
                            data: {
                                term: pp.val(),
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                            },
                            dataType: "json",
                            success: function (data) {
                                callback(data[0]);
                            }
                        });
                    },
                    ajax: {
                        url: site_url + "products/getIdAttributes",
                        dataType: 'json',
                        quietMillis: 15,
                        data: function (term, page) {
                            return {
                                term: term,
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                                limit: 10
                            };
                        },
                        results: function (data, page) {
                            if (data.results != null) {
                                return {results: data.results};
                            } else {
                                return {results: [{id: '', text: 'No Match Found'}]};
                            }
                        }
                    }
                });
            }
            return get;
        }
        function getidsx() {
//            alsert($("#selsize").data('id'));
            var $ids = $("#selsize").data('id').split(",");
            var $idvals = "";

            $.each($ids, function (k, v) {
                $idvals += ($("#" + v).val() ? $("#" + v).val() : "") + "-";
            });
            $idvals = $idvals.trim();
            return $idvals;
        }
        $("#selsize").select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site_url + "products/getIdAttribute",
                    data: {
                        term: $(element).val(),
                        tab: $(element).data('tab'),
                        id: getidsx,
                        key: $(element).data('key'),
                    },
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site_url + "products/getIdAttributes",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        tab: $("#selsize").data('tab'),
                        id: getidsx,
                        key: $("#selsize").data('key'),
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'No Match Found'}]};
                    }
                }
            }
        });
    });


    $('#get_acc_no').change(function () {
        var v = $(this).val();
        if (v == "Cash") {
            $('#bank').prop('disabled', 'disabled');
            $('#currency_note').css('display', 'block');
            $('#bank').val('');
        } else {
            $('#bank').prop('disabled', '');
            $('#currency_note').css('display', 'none');
            $.ajax({
                type: "get",
                async: false,
                data: {id: v},
                url: "<?= site_url('purchases/bankAccountNoByBank') ?>",
                dataType: "json",
                success: function (data) {
                    $('#payeeBankDetails').html('');
                    $.each(data, function (k, val) {
                        $('#bank').val(val.account_no);
                        return;
                    })
                },
            });
        }
    });


    $('#transfer_type').change(function () {
        var type = $(this).val();
        $('#mode_of_trans').html('');
        if (type == 'Bank Deposits') {
            $('#mode_of_trans').append('<option value="">Select Mode</option>');
            $('#mode_of_trans').append('<option value="Cash Deposit">Cash Deposit</option>');
        } else {
            $('#mode_of_trans').append('<option value="">Select Mode</option>');
            $('#mode_of_trans').append('<option value="NEFT">NEFT</option>');
            $('#mode_of_trans').append('<option value="IMPS">IMPS</option>');
            $('#mode_of_trans').append('<option value="RTGS">RTGS</option>');
            $('#mode_of_trans').append('<option value="UPI">UPI</option>');
            $('#mode_of_trans').append('<option value="Other">Other</option>');
        }
    });
    $('#ten,#twenty,#fifty,#hundread,#fivehundread,#twothousand,#coins').keypress(function (e) {
        var key = e.keyCode ? e.keyCode : e.which;
        if (isNaN(String.fromCharCode(key)) && key != 8 && key != 46)
            return false;

    });
    $('#ten,#twenty,#fifty,#hundread,#fivehundread,#twothousand,#coins').keyup(function (evt) {
        if (evt.which < 48 || evt.which > 57)
        {
            evt.preventDefault();
        }
        $('#amount').attr('readonly', '');
        $('#totals').attr('readonly', '');

        var ten = $('#ten').val();
        var twenty = $('#twenty').val();
        var fifty = $('#fifty').val();
        var hundread = $('#hundread').val();
        var fivehundread = $('#fivehundread').val();
        var twothousand = $('#twothousand').val();
        var coins = $('#coins').val();

        var totals = ((ten * 10) + (twenty * 20) + (fifty * 50) + (hundread * 100) + (fivehundread * 500) + (twothousand * 2000) + (coins * 1));
        $('#totals').val(totals);
        $('#amount').val(totals);

    });

    function enableEdit() {
        $('#contra_code').removeAttr('readonly');
    }

//    $('#transfer_type').change(function () {
//        var type = $(this).val();
//        if (type == 'Bank Deposits') {
//            
//        } else {
//
//        }
//    });

    $('#to_acc').change(function () {
        var v = $(this).val();
        $.ajax({
            type: "get",
            async: false,
            data: {id: v},
            url: "<?= site_url('purchases/bankAccountNoByBank') ?>",
            dataType: "json",
            success: function (data) {
                $('#receiver_bank_acc_no').html('');
                var opt = $('<option />');
                opt.val('');
                opt.text('Select Account No.');
                $('#receiver_bank_acc_no').append(opt);
                $('receiver_bank_acc_no').trigger('change');
                $.each(data, function (k, val) {
                    var opt = $('<option />');
                    opt.val(val.account_no);
                    opt.text(val.account_no);
                    $('#receiver_bank_acc_no').append(opt);
                })
            },
        });
    });

</script>
