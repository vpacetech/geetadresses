<?= $modal_js ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <center><h4 class="modal-title" id="myModalLabel">View Parcel Sent Voucher</h4></center>
        </div>
        <?php
        $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'addFormx');
        echo form_open_multipart("purchases/updateParcelRev/" . $min->id, $attrib);
        ?>
        <!--<form data-toggle="validator" role="form" id="addFormx">-->
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
<!--            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="Sender">Sender</label>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <div class="form-group"> 
                            <label><?php echo $min->sender; ?></label>
                        </div>
                    </div>
                </div>
            </div>-->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="Receiver">Receiver</label>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <!--<div class="controls">--> 
                            <span> <?php echo $min->receiver;?></span>
                            <?php // echo form_input('sender_address', $min->sender_address, 'class="form-control tip" id="sender_address" data-bv-notempty="true"'); ?>
                        <!--</div>-->
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="Receiver_Address">Receiver Address</label>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <!--<div class="controls">--> 
                            <span> <?php echo $min->receiver_address?></span>
                            <?php // echo form_input('sender_address', $min->sender_address, 'class="form-control tip" id="sender_address" data-bv-notempty="true"'); ?>
                        <!--</div>-->
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="transport">Transport</label>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <!--<div class="controls">--> 
                        <span><?php echo $min->transport;?></span>
                            <?php // echo form_input('transport', $min->transport, 'class="form-control tip" id="transport" data-bv-notempty="true"'); ?>
                        <!--</div>-->
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="veh_no">Veh.no</label> : 
                            <span><?php echo $min->veh_no?></span>
                            
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="section">L.R.No</label> : <span><?php echo $min->lr_no ?></span>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="type">Book Date</label> : <span><?php echo $min->book_date?></span>
                        <div class="form-group">
                            <?php // echo form_input('bk_date', ($min->bk_date != "" ? $min->bk_date : $_POST['bk_date']), 'class="form-control input-tip datetime" id="sldate" required="required"'); ?>
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="brands">Parcel Type</label> :  <span><?php echo $min->parcel_type ?></span>
                    </div>

                </div><!--end col 6 --->
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="department">Hamal</label> : <span><?php echo  $min->through?></span>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="product_items">No of Bales</label> : <span><?php echo $min->no_of_bales ;?></span>
                       
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="brands">Booked By</label> <span><?php echo $min->booked_by ?></span>
                       
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="design">Rg No</label> : <span><?php echo $min->rg_no?></span>

                    </div>
                </div>
                <!--end col 6 --->

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="min_qty_lvl">Goods Description</label>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"> : <?php echo $min->goods_desc?>
                    <div class="form-group">
                    </div>
                </div>



                <div class="clearfir"></div>


            </div>
            <!---->
        </div><!--end body div-->
        <div class="modal-footer" style="clear:both;">
            <!--<input name="add_user" value="Save" class="btn btn-primary" type="submit">--> 
        </div><!--end foter div-->
        </form>

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function (e) {
        $('#addFormx').bootstrapValidator({
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            }, excluded: [':disabled']
        });
        $('select.select').select2({minimumResultsForSearch: 6});
        fields = $('.modal-content').find('.form-control');
        $.each(fields, function () {
            var id = $(this).attr('id');
            var iname = $(this).attr('name');
            var iid = '#' + id;
            if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
//                $("label[for='" + id + "']").append(' *');
                $(document).on('change', iid, function () {
                    $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
                });
            }
        });
    });

    mrp = onlydigits('mrp');
    mrp();
    mrp_from = onlydigits('mrp_from');
    mrp_from();
    mrp_to = onlydigits('mrp_to');
    mrp_to();
    sales_incentive = onlydigits('sales_incentive');
    sales_incentive();
    min_qty_lvl = onlydigits('min_qty_lvl');
    min_qty_lvl();
    min_qty_lvl_2 = onlydigits('min_qty_lvl_2');
    min_qty_lvl_2();
    reorder_qty = onlydigits('reorder_qty');
    reorder_qty();
    reorder_qty_2 = onlydigits('reorder_qty_2');
    reorder_qty_2();
    $(document).ready(function () {
//        $("#addFormx").unbind().submit(function (e) {
//            e.preventDefault();
//            var form = $("#addFormx");
//            var $data = $(this).serialize();
////            $data += "&name=<?= $this->security->get_csrf_token_name() ?>&value=<?= $this->security->get_csrf_hash() ?>";
//            $.ajax({
//                url: "<?= site_url('system_settings/SaveMin_qty_lvl/?v=1') ?>",
//                type: "get", async: false,
//                data: $data,
//                dataType: 'json',
//                success: function (data, textStatus, jqXHR) {
//                    if (data.s === "true") {
//                        bootbox.alert('Paramenter Added Successfully!');
//                        oTable.fnDraw();
//                        $("#myModal").modal('hide');
//                    } else {
//                        bootbox.alert('Paramenter Adding Failed!');
//                        $("#myModal").modal('hide');
//                    }
//                }
//            });
//            return false;
//        });
        var obj = ['companies', 'department', 'section', 'product_items', 'type', 'brands', 'design', 'style', 'pattern', 'fitting', 'fabric', 'color', 'size', 'per'];
        var objarr = [];
        var i = 0;
        $.each(obj, function (k, v) {
            objarr[i] = myselect2(v);
            objarr[i]();
            i++;
        });
        function myselect2(id) {
            if (id == "per") {
                var pp = $("." + id);
            } else {
                var pp = $("#" + id);
            }
            function get() {
                pp.select2({
                    minimumInputLength: 1,
                    data: [],
                    initSelection: function (element, callback) {
                        $.ajax({
                            type: "get", async: false,
                            url: site_url + "products/getIdAttribute",
                            data: {
                                term: pp.val(),
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                            },
                            dataType: "json",
                            success: function (data) {
                                callback(data[0]);
                            }
                        });
                    },
                    ajax: {
                        url: site_url + "products/getIdAttributes",
                        dataType: 'json',
                        quietMillis: 15,
                        data: function (term, page) {
                            return {
                                term: term,
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                                limit: 10
                            };
                        },
                        results: function (data, page) {
                            if (data.results != null) {
                                return {results: data.results};
                            } else {
                                return {results: [{id: '', text: 'No Match Found'}]};
                            }
                        }
                    }
                });
            }
            return get;
        }
        function getidsx() {
//            alsert($("#selsize").data('id'));
            var $ids = $("#selsize").data('id').split(",");
            var $idvals = "";

            $.each($ids, function (k, v) {
                $idvals += ($("#" + v).val() ? $("#" + v).val() : "") + "-";
            });
            $idvals = $idvals.trim();
            return $idvals;
        }
        $("#selsize").select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site_url + "products/getIdAttribute",
                    data: {
                        term: $(element).val(),
                        tab: $(element).data('tab'),
                        id: getidsx,
                        key: $(element).data('key'),
                    },
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site_url + "products/getIdAttributes",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        tab: $("#selsize").data('tab'),
                        id: getidsx,
                        key: $("#selsize").data('key'),
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'No Match Found'}]};
                    }
                }
            }
        });
    });
</script>


