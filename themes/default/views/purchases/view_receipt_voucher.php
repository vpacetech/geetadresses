<style>
    .close{
        font-size: 19px !important;
    }
    .heading{
        margin-top:14px !important;
        border-top: 1px solid black;
    }
    .heading1{
        padding-top: 7px;
    }
    .modal-header{
        border-bottom: none !important;
    }
</style>
<?php echo $modal_js ?>
<div class="modal-dialog modal-lg ">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
        </div>
        <?php
        $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'addFormx');
        echo form_open_multipart("purchases/UpdateVoucher/".$voucher->id, $attrib);
        ?>
        <div class="modal-body">
            <div >
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <h2 style="margin:0;padding-top: 10px !important;">
                        <?= lang('view_receipt_voucher_tag') ?>
                    </h2>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
                    <b style='font-size: 20px;'><?= $Settings->site_name ?></b>
                    <!--<h4>Kavathe - Mahankal</h4>-->
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 "></div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 heading" >
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                    <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="joining_date">  <?= lang('paymentno') ?> </label>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 heading1">
                       : <?php echo $voucher->payment_no ?>
                    </div>
                </div>
                <input type="text" name="type" value="receipt" class="form-control hidden">
                
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                    <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="joining_date"><?= lang('date') ?> </label>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 heading1">
                       : <?php echo $voucher->date ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group">
                    <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="joining_date">  <?= lang('user') ?>  </label>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 heading1">
                      : <?php echo $voucher->first_name ?> <?php echo $voucher->last_name ?>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                    <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="joining_date"> <?= lang('time') ?> </label>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 heading1">
                          : <?php echo $voucher->time ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 heading" style="padding:0;">
                <h4 class="text-center"><span style="text-decoration: underline;font-size: 20px; padding-bottom:5px;"><?= lang('beneficiary') ?></span>&nbsp;<span style="text-decoration: underline;font-size: 20px; padding-bottom:5px;"><?= lang('details') ?></span></h4>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="payfor"><?= lang('received_for') ?>  </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                               : <?php echo $voucher->recivername ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="party_account"><?= lang('party_account') ?> </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                             : <?= $voucher->party_account ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-4" for="againstno"><?= lang('againstno') ?> </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                 : <?php echo $voucher->against_ref_no ?>                 
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="joining_date"><?= lang('bank_acc_no') ?> </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            : <?php echo $voucher->bank_acc_no ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="dated"><?= lang('dated') ?> </label>

                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                      : <?php echo $voucher->datedd ?>                
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="amount"><?= lang('amount') ?> </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">

                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="padding: 0">
                                : <?php echo $voucher->amount ?>
                            </div>
                            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12" style="padding-right: 0">Cr.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 heading" style="padding:0;">
                <h4 class="text-center" ><span style="font-size: 20px; text-decoration: underline;padding-bottom:5px;"><?= lang('payer') ?></span>&nbsp;<span style="font-size: 20px; text-decoration: underline;padding-bottom:5px;"><?= lang('details') ?></span></h4>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="payment_mode"><?= lang('payment_mode') ?> </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                             : <?php echo $voucher->payment_mode ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="from_acc"><?= lang('to_acc') ?> </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                             : <?= $voucher->to_acc ?>
                                                      <?php // echo $voucher->from_acc ?>  
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="pay_type_no"><?= lang('paym_no') ?></label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                              : <?php echo $voucher->pay_type_no ?>            
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="bank_acc_number"><?= lang('account_no') ?> </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            : <?php echo $voucher->acc_number ?> 
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="joining_date"><?= lang('dated') ?></label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                             : <?php echo $voucher->dated ?> 
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="joining_date"><?= lang('balance') ?> </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 " style="padding: 0">
                               : <?php echo $voucher->balance ?> 
                            </div>
                            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12" style="padding-right: 0">Dr.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer" style="clear:both;text-align: left;">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                        <b>Narration</b> : Cheque given by hand
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-5"><?php // echo form_submit('add_transfer', $this->lang->line("submit"), 'id="add_transfer" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div><!--end body div-->
</div>
