
<div class="modal-dialog modal-lg">


    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <center><h4 class="modal-title" id="myModalLabel">Edit Product</h4></center>
        </div>

        <?= $modal_js ?>

        <?php
        if (!empty($variants)) {
            foreach ($variants as $variant) {
                $vars[] = addslashes($variant->name);
            }
        } else {
            $vars = array();
        }

        $store[''] = "";
        $department[''] = "";
        $product_items[''] = "";
        $section[''] = "";
        $type[''] = "";
        $brands[''] = "";
        $design[''] = "";
        $style[''] = "";
        $pattern[''] = "";
        $fitting[''] = "";
        $fabric[''] = "";
        $color[''] = "";
        $size[''] = "";
        $sizes = array();

        foreach ($product_para as $k => $v) {
            if ($k == "companies" && !empty($v)) {
                foreach ($v as $d) {
                    $store[$d->id] = $d->name;
                }
            }
            if ($k == "department" && !empty($v)) {
                foreach ($v as $d) {
                    $department[$d->id] = $d->name;
                }
            }
            if ($k == "product_items" && !empty($v)) {
                foreach ($v as $d) {
                    $product_items[$d->id] = $d->name;
                }
            }
            if ($k == "section" && !empty($v)) {
                foreach ($v as $d) {
                    $section[$d->id] = $d->name;
                }
            }
            if ($k == "type" && !empty($v)) {
                foreach ($v as $d) {
                    $type[$d->id] = $d->name;
                }
            }
            if ($k == "brands" && !empty($v)) {
                foreach ($v as $d) {
                    $brands[$d->id] = $d->name;
                }
            }
            if ($k == "design" && !empty($v)) {
                foreach ($v as $d) {
                    $design[$d->id] = $d->name;
                }
            }
            if ($k == "style" && !empty($v)) {
                foreach ($v as $d) {
                    $style[$d->id] = $d->name;
                }
            }
            if ($k == "pattern" && !empty($v)) {
                foreach ($v as $d) {
                    $pattern[$d->id] = $d->name;
                }
            }
            if ($k == "fitting" && !empty($v)) {
                foreach ($v as $d) {
                    $fitting[$d->id] = $d->name;
                }
            }
            if ($k == "fabric" && !empty($v)) {
                foreach ($v as $d) {
                    $fabric[$d->id] = $d->name;
                }
            }
            if ($k == "color" && !empty($v)) {
                foreach ($v as $d) {
                    $color[$d->id] = $d->name;
                }
            }
            if ($k == "size" && !empty($v)) {
                foreach ($v as $d) {
                    $size[$d->id] = $d->name;
                    $sizes[$d->id] = $d;
                }
            }
            if ($k == "per" && !empty($v)) {
                foreach ($v as $d) {
                    $per[$d->id] = $d->name;
                }
            }
            if ($k == "per" && !empty($v)) {
                foreach ($v as $d) {
                    $units[$d->name] = $d->name;
                }
            }

            if ($k == "per" && !empty($v)) {
                foreach ($v as $d) {
                    $rateper[$d->id] = $d->name;
                }
            }
        }
        ?>
        <form data-toggle="validator" role="form" id="editOrderItmes">
            <div class="modal-body">
                <div class="box" >

                    <div class="box-content" >
                        <div class="row">
                            <div class="col-lg-12">

                                <p class="introtext"><?php echo lang('update_info'); ?></p>

                                <?php
                                $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'editOrderItmes');
//                            echo form_open_multipart("products/edit/" . $product->id, $attrib)
                                ?>
                                <div class="col-md-5">

                                    <input type="hidden" name="procutid"  value="<?= $product->id ?>">
                                    <input type="hidden" name="type" id="standardEdit" value="standard">

                                    <div class="form-group all">
                                        <?= lang("Store", "companiesEdits") ?>
                                        <?php
                                        echo form_input('store_id', (isset($_POST['store_id']) ? $_POST['store_id'] : ($product ? $product->store_id : '')), 'class="form-control companiesEdit" data-tab="companies" id="companiesEdits" placeholder="' . lang("select") . " " . lang("store") . '" required="required" style="width:100%" onchange="editgetSupplier()"');
                                        ?>
                                    </div>
                                    <div class="form-group all">
                                        <?= lang("Department", "departmentEdit") ?>
                                        <?php
                                        echo form_input('dept', (isset($_POST['dept']) ? $_POST['dept'] : ($product ? $product->department : '')), 'class="form-control departmentEdit" data-tab="department" data-key="store_id" data-id="companiesEdits" id="departmentEdit" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%"  onchange="getProdName();getProductMargin(); getGstDetails();"');
                                        ?>
                                    </div>
                                    <div class="form-group all">
                                        <?= lang("Section", "sectionEdit") ?>
                                        <?php
                                        echo form_input('section', (isset($_POST['section']) ? $_POST['section'] : ($product ? $product->section : '')), 'class="form-control sectionEdit" data-tab="section" id="sectionEdit" data-key="department_id" data-id="departmentEdit" placeholder="' . lang("select") . " " . lang("section") . '" onchange="getProductMargin(); getGstDetails();" required="required" style="width:100%"');
                                        ?>
                                    </div>
                                    <div class="form-group all">
                                        <?= lang("Product_items", "product_itemsEdit") ?>
                                        <?php
                                        echo form_input('product_item', (isset($_POST['product_item']) ? $_POST['product_item'] : ($product ? $product->product_items : '')), 'class="form-control product_itemsEdit" data-tab="product_items" data-key="section_id" data-id="sectionEdit" id="product_itemsEdit" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%" onchange="getProdName();getProductMargin();getGstDetails();"');
                                        ?>
                                    </div>
                                    <div class="form-group all">
                                        <?= lang("Type", "type_idEdit") ?>
                                        <?php
                                        echo form_input('type', (isset($_POST['type_id']) ? $_POST['type_id'] : ($product ? $product->type_id : '')), 'class="form-control type_idEdit"  data-tab="type" id="type_idEdit" data-key="product_items_id" data-id="product_itemsEdit" placeholder="' . lang("select") . " " . lang("type") . '" required="required" style="width:100%" onchange="getProdName();getProductMargin();getGstDetails();"');
                                        ?>
                                    </div>
                                    <div class="form-group all">
                                        <?= lang("Brands", "brandsEdit") ?>
                                        <?php
                                        echo form_input('brands_id', (isset($_POST['brands']) ? $_POST['brands'] : ($product ? $product->brands : '')), 'class="form-control brandsEdit"  data-tab="brands" id="brandsEdit" data-key="type_id" data-id="type_idEdit" placeholder="' . lang("select") . " " . lang("brands") . '" required="required" onchange="getProductMargin();getGstDetails();"  style="width:100%"');
                                        ?>
                                    </div>
                                    <div class="form-group standard">
                                        <?= lang("Design", "designEdit") ?>
                                        <?php
                                        echo form_input('design', (isset($_POST['design']) ? $_POST['design'] : ($product ? $product->design : '')), 'class="form-control designEdit"  data-tab="design" id="designEdit" data-key="brands_id" data-id="brandsEdit" placeholder="' . lang("select") . " " . lang("design") . '" required="required" style="width:100%" onchange="getGstDetails();"');
                                        ?>
                                    </div>
                                    <div class="form-group standard">
                                        <?= lang("Style", "styleEdit") ?>
                                        <?php
                                        echo form_input('style', (isset($_POST['style']) ? $_POST['style'] : ($product ? $product->style : '')), 'class="form-control styleEdit"  data-tab="style" id="styleEdit" data-key="design_id" data-id="designEdit" placeholder="' . lang("select") . " " . lang("style") . '" required="required" style="width:100%" onchange="getGstDetails();"');
                                        ?>
                                    </div>
                                    <div class="form-group standard">
                                        <?= lang("Pattern", "patternEdit") ?>
                                        <?php
                                        echo form_input('pattern', (isset($_POST['pattern']) ? $_POST['pattern'] : ($product ? $product->pattern : '')), 'class="form-control patternEdit"  data-tab="pattern" data-key="style_id" data-id="styleEdit" id="patternEdit" placeholder="' . lang("select") . " " . lang("pattern") . '" required="required" style="width:100%" onchange="getGstDetails();"');
                                        ?>
                                    </div>
                                    <div class="form-group standard">
                                        <?= lang("Fitting", "fittingEdit") ?>
                                        <?php
                                        echo form_input('fitting', (isset($_POST['fitting']) ? $_POST['fitting'] : ($product ? $product->fitting : '')), 'class="form-control fittingEdit" data-tab="fitting" data-key="pattern_id" data-id="patternEdit" id="fittingEdit" placeholder="' . lang("select") . " " . lang("fitting") . '" required="required" style="width:100%" onchange="getGstDetails();"');
                                        ?>
                                    </div>
                                    <div class="form-group standard">
                                        <?= lang("Fabric", "fabricEdit") ?>
                                        <?php
                                        echo form_input('fabric', (isset($_POST['fabric']) ? $_POST['fabric'] : ($product ? $product->fabric : '')), 'class="form-control fabricEdit" default-attrib data-tab="fabric" id="fabricEdit" data-key="fitting_id" data-id="fittingEdit" placeholder="' . lang("select") . " " . lang("fabric") . '" required="required" style="width:100%" onchange="getGstDetails();"');
                                        ?>
                                    </div>
                                    <div id="img-details"></div>
                                </div>
                                <div class="col-md-6 col-md-offset-1">

                                    <div class="form-group all">
                                        <?= lang("product_name", "nameEdit") ?>
                                        <?= form_input('name', (isset($_POST['name']) ? $_POST['name'] : ($product ? $product->name : '')), 'class="form-control" id="nameEdit" required="required" readonly'); ?>
                                    </div>
                                    <!--                                <div class="form-group all">
                                    <?= lang("product_code", "codeEdit") ?>
                                    <?= form_input('code', (isset($_POST['code']) ? $_POST['code'] : ($product ? $product->code : '')), 'class="form-control" id="codeEdit"  required="required" readonly') ?>
                                                                        <span class="help-block"><?= lang('you_scan_your_barcode_too') ?></span>
                                                                    </div>-->

                                    <div class="form-group all">
                                        <?= lang("supplier", "supplierEdit") ?> 
                                        </button>
                                        <div class="row" id="supplier-con">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <?php
//                                                echo $product->supplier1;
                                                $b1 = array();
//                                                $bl[""] = "Select ";
                                                foreach ($suppliers as $supplier) {
                                                    $bl[$supplier->id] = $supplier->code . ' - ' . $supplier->name;
                                                }
                                                echo form_dropdown('supplier', $bl, (isset($_POST['text']) ? $_POST['text'] : $product->supplier1), 'id="supplier12Edit" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("supplier") . '" required="required" class="form-control" style="width:100%;"');
                                                ?>
                                                <?php
                                                ?></div>
                                        </div>
                                        <div id="ex-suppliers"></div>
                                    </div>
                                    <div class="form-group standard">
                                        <?php
                                        if (isset($product->image)) {
                                            echo '<a class="img-thumbnail change_img" href="' . base_url() . 'assets/uploads/' . $product->image . '" style="margin-right:5px;"><img class="img-responsive" src="' . base_url() . 'assets/uploads/thumbs/' . $product->image . '" alt="' . $product->image . '" style="width:' . $this->Settings->twidth . 'px; height:' . $this->Settings->theight . 'px;" /></a>';
                                        }
                                        ?><br>
                                        <?= lang("product_image", "product_image") ?>
                                        <input id="product_image" type="file" name="product_image" data-show-upload="false"
                                               data-show-preview="false" accept="image/*" class="form-control file">
                                    </div>
                                    <div class="form-group standard">
                                        <?= lang("product_gallery_images", "images") ?>
                                        <input id="images" type="file" name="userfile[]" multiple="true" data-show-upload="false"
                                               data-show-preview="false" class="form-control file" accept="image/*">
                                    </div>
                                    <div class="form-group standard">
                                        <?= lang("Color", "colorEdit") ?>
                                        <div class="row" >
                                            <div class="col-md-3" ><?php
                                                echo form_dropdown('colorsingle', $color, (isset($_POST['colorsingle']) ? $_POST['colorsingle'] : ($product ? $product->color : '')), 'class="form-control" select2 id="colorEdit" placeholder="' . lang("select") . " " . lang("color") . '" style="width:100%"');
                                                ?></div>
                                            <div class="col-md-2"><input type="text" name="colorqty" value="<?= (isset($_POST['colorqty']) ? $_POST['colorqty'] : ($product ? $product->colorqty : '')) ?>" class="form-control" id='colorqty' onchange="getQty('#colorqty')"/></div>
                                        </div>
                                    </div>
                                    <div class="form-group all">
                                        <?= lang("Size", "sizeEdit") ?>
                                        <div class="row" >
                                            <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" name="sizeangle" <?= (isset($_POST['sizeangle']) && $_POST['sizeangle'] == "HT" ? "checked='checked'" : ($product && $product->sizeangle == "HT" ? "checked='checked'" : '')) ?> value="HT"/>&nbsp;&nbsp;&nbsp;&nbsp;Height</div>
                                            <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" name="sizeangle" <?= (isset($_POST['sizeangle']) && $_POST['sizeangle'] == "WT" ? "checked='checked'" : ($product && $product->sizeangle == "WT" ? "checked='checked'" : '')) ?> value="WT"/>&nbsp;&nbsp;&nbsp;&nbsp;Width</div>
                                            <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" name="sizeangle" <?= (isset($_POST['sizeangle']) && $_POST['sizeangle'] == "NZ" ? "checked='checked'" : ($product && $product->sizeangle == "NZ" ? "checked='checked'" : '')) ?> value="NZ"/>&nbsp;&nbsp;&nbsp;&nbsp;No Size</div>
                                        </div>
                                        <br>
                                        <?php if ($product->sizeangle != "NZ") { ?>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="col-md-9" style="padding: 0px" ><?php
                                                        echo form_input('size', (isset($_POST['singlesize']) ? $_POST['singlesize'] : ($product ? $product->size : '')), 'class="form-control " sizesel data-tab="size" data-key="department_id-section_id-product_items_id" data-id="departmentEdit,sectionEdit,product_itemsEdit" id="selsize" placeholder="' . lang("select") . " " . lang("size") . '" required="required" style="width:100%" onchange="getProdName();"');
                                                        ?>
                                                    </div>
                                                    <!--<div class="col-md-2"> {{size[prod.singlesize].code}}"</div>-->
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="row standard">
                                        <div class="col-md-8">
                                            <div class="form-group all">
                                                <label class="control-label" for="unit"><?= lang("product_qty") ?></label>
                                                <?php
                                                foreach ($warehouses as $warehouse) {

                                                    $whs[$warehouse->id] = $warehouse->name;
                                                    if ($this->Settings->racks) {
                                                        echo "<div class='row'><div class='col-md-12'>";
                                                        echo form_hidden('wh_' . $warehouse->id, $warehouse->id) . form_input('wh_qty_' . $warehouse->id, (isset($_POST['wh_qty_' . $warehouse->id]) ? $_POST['wh_qty_' . $warehouse->id] : $this->sma->formatQuantity($product->quantity)), 'class="form-control" id="wh_qty_' . $warehouse->id . '" placeholder="' . lang('quantity') . '"  onlyno ');
                                                        echo "</div></div>";
                                                    } else {
                                                        echo form_hidden('wh_' . $warehouse->id, $warehouse->id) . form_input('wh_qty_' . $warehouse->id, (isset($_POST['wh_qty_' . $warehouse->id]) ? $_POST['wh_qty_' . $warehouse->id] : $this->sma->formatQuantity($product->quantity)), 'class="form-control" id="wh_qty_' . $warehouse->id . '" placeholder="' . lang('quantity') . '" onlyno ');
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group standard">
                                                <?= lang("unit_per", "unitEdit") ?>
                                                <?= form_dropdown('unit', $units, (isset($_POST['cper']) ? $_POST['cper'] : ($product ? $product->unit : '')), 'class="form-control select" id="unitEdit" placeholder="' . lang("select") . " " . lang("Unit") . '" required="required" style="width:100%"'); ?>
                                                <?php //form_input('unit', (isset($_POST['unit']) ? $_POST['unit'] : ($product ? $product->unit : '')), 'class="form-control pperEdit" id="unitEdit" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" required="required" style="width:100%" '); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row all">
                                        <div class="col-md-8">
                                            <div class="form-group all">
                                                <?= lang("product_cost", "costEdit") ?>
                                                <?= form_input('cost', (isset($_POST['cost']) ? $_POST['cost'] : ($product ? $this->sma->formatDecimal($product->cost) : '')), 'class="form-control tip" id="costEdit" onblur="getProductMargin();" required="required"') ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group all">
                                                <?= lang("product_per", "cperEdit") ?>
                                                <?= form_dropdown('cper', $per, (isset($_POST['cper']) ? $_POST['cper'] : ($product ? $product->cper : '')), 'class="form-control select" id="cperEdit" placeholder="' . lang("select") . " " . lang("Unit") . '" required="required" style="width:100%"'); ?>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group all">
                                                <?= lang("product_price", "priceEdit") ?>
                                                <?= form_input('price', (isset($_POST['price']) ? $_POST['price'] : ($product ? $this->sma->formatDecimal($product->price) : '')), 'class="form-control tip" id="priceEdit" required="required"') ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group all">
                                                <?= lang("product_per", "perEdit") ?> *
                                                <?= form_dropdown('pper', $per, (isset($_POST['pper']) ? $_POST['pper'] : ($product ? $product->pper : '')), 'class="form-control select" id="pperEdit" placeholder="' . lang("select") . " " . lang("Unit") . '" required="required" style="width:100%"'); ?>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group standard">

                                        <br/>
                                        <div class="row">
                                            <div class="" >
    <!--                                            <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" name="ratetype" <?= (isset($_POST['ratetype']) && $_POST['ratetype'] == "Single" ? "checked='checked'" : ($product && $product->ratetype == "Single" ? "checked='checked'" : '')) ?> value="Single"/>&nbsp;&nbsp;&nbsp;&nbsp;Single</div>
                                                <div class="col-md-3"><?php echo form_input('singlerate', (isset($_POST['singlerate']) ? $_POST['singlerate'] : ($product ? $product->singlerate : '')), 'class="form-control" id="singlerate" placeholder="" style="width:100%;" '); ?></div>-->
                                                <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" name="ratetype" <?= (isset($_POST['ratetype']) && $_POST['ratetype'] == "Single" ? "checked='checked'" : ($product && $product->ratetype == "MRP" ? "checked='checked'" : '')) ?> value="MRP"/>&nbsp;&nbsp;&nbsp;&nbsp;MRP</div>
                                                <div class="col-md-3"><?php echo form_input('mrprate', (isset($_POST['mrprate']) ? $_POST['mrprate'] : ($product ? $product->mrprate : '')), 'class="form-control" id="mrprate" placeholder="" style="width:100%;"'); ?></div>
                                                <div class="col-md-4">
                                                    <input type="checkbox" icheck class="checkbox"  id="extrasedit" value=""/>
                                                    <label for="extras" class="padding05"><?= lang('roundup') ?></label>
                                                </div>
                                            </div>
                                        </div>

                                        <br/>

                                    </div>    

                                    <div class="row standard">
                                        <div class="col-md-6 form-group"> 
                                            <?php
//                                                                                                echo '<pre>';
//                                                                                                print_r($product);
//                                                                                                echo '</pre>';
//                                                                                                die();
                                            ?>
                                            <?= lang('hsncode', 'hsncode') ?>
                                            <?= form_input('hsnno', (isset($_POST['hsnno']) ? $_POST['hsnno'] : $product->hsn), 'class="form-control tip" id="hsnnoEdit" readonly ') ?>
                                        </div>

                                        <div class="col-md-4 form-group">
                                            <?= lang('gstno', 'gstnopur') ?> *
                                            <?= form_input('gstno', (isset($_POST['gstno']) ? $_POST['gstno'] : $product->gst), 'class="form-control tip" id="gstnoEdit" readonly ') ?>
                                        </div>
                                        <div class="col-md-2">
                                            <?= lang('cess', 'cess') ?>
                                            <?= form_input('cess', (isset($_POST['cess']) ? $_POST['cess'] : $product->cess), 'class="form-control tip" id="cessEdit" readonly ') ?>
                                        </div>

                                    </div>
                                    <div class="row form-group standard">
                                        <div class="col-md-8"> &nbsp;&nbsp;<input type="checkbox" icheck class="checkbox" id="addupgetcheckedit" name="addupgst">&nbsp;&nbsp;&nbsp;Add-up GST if MRP exceeds 1000+ (%)</div>
                                        <div class="col-md-4">
                                            <?= form_input('addupgstmrp', (isset($_POST['addupgstmrp']) ? $_POST['addupgstmrp'] : $product->adduppercentage), 'class="form-control tip" id="addupgstmrpedit" readonly ') ?>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-12">

                                    <div class="form-group all">
                                        <?= lang("product_details", "product_details") ?>
                                        <?= form_textarea('product_details', '', 'class="form-control" id="details"'); ?>
                                    </div>
                                    <div class="form-group all">
                                        <?= lang("product_details_for_invoice", "details") ?>
                                        <?= form_textarea('details', '', 'class="form-control" id="details"'); ?>
                                    </div>
                                    <div class="form-group">

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end body div-->
            <div class="modal-footer" style="clear:both;">
                <input name="add_hsn" value="Update" class="btn btn-primary" type="submit"> 
            </div>
        </form>

    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

    });
    var addupchk = false;
    var roundup = false;

    function getGstDetails() {
        var data1 = $('#editOrderItmes').serialize();
        $.ajax({
            type: "get", async: false,
            url: site_url + "gst/getGstRate",
            data: data1,
            dataType: "json",
            success: function (data) {
                if (data != "") {
                    $('#hsnnoEdit').val(data.hsncode);
                    $('#gstnoEdit').val(parseFloat(data.cgst) + parseFloat(data.sgst));
                    $('#cessEdit').val(parseFloat(data.cess));
                    if (addupchk) {
                        $('#addupgstmrpedit').val(parseFloat(data.addupgst));
                    } else {
                        $('#addupgstmrpedit').val('0');
                    }
                } else {
                    $('#hsnnoEdit').val('');
                    $('#gstnoEdit').val('');
                    $('#cessEdit').val('');
                    $('#addupgstmrp').val('');
                }
            }
        });
    }

<?php if ($product->gst == "") { ?>
        getGstDetails();
        getProductMargin();
<?php } ?>




    $(document).on('ifChecked', '#addupgetcheckedit', function (e) {
        addupchk = true;
        getProductMargin();
    });
    $(document).on('ifUnchecked', '#addupgetcheckedit', function (e) {
        addupchk = false;
        getProductMargin();
    });

    $(document).on('ifChecked', '#extrasedit', function (e) {
        roundup = true;
        getProductMargin();
    });

    $(document).on('ifUnchecked', '#extrasedit', function (e) {
        roundup = false;
        getProductMargin();
    });

    $(document).on('change', '#companiesEdits', function (e) {
//        alert();
    });

    function getProdName() {
        var data = $('#editOrderItmes').serialize();
        $.ajax({
            type: "get", async: false,
            url: site_url + "Products/getProdName",
            data: data,
            dataType: "json",
            success: function (data) {
                $('#nameEdit').val(data.name);
            }
        });
    }

    function getProductMargin() {
        getGstDetails();
        var data = $('#editOrderItmes').serialize();
        var gstno = $('#gstnoEdit').val();
        var addup = $('#addupgstmrpedit').val();
        var cess = $('#cessEdit').val();

        var gst = 0;
        var cess1 = 0;
        var addgst = 0;
        $.ajax({
            type: "get", async: false,
            url: site_url + "Products/getProdMargin",
            data: data,
            dataType: "json",
            success: function (data) {
//                alert(JSON.stringify(data));
                var margin = data.margin;
                var cost = $('#costEdit').val();

                var price = '';
                if (margin != 0) {
                    margin = (parseFloat(cost) * parseFloat(margin)) / 100;
                    price = parseFloat(cost) + parseFloat(margin);
                }

                if (typeof gstno != 'undefined') {
                    if (gstno != 0) {
                        gst = (parseFloat(cost) * parseFloat(gstno)) / 100;
                    }
                }
                if (typeof cess != 'undefined') {
                    if (cess != 0) {
                        cess1 = (parseFloat(cost) * parseFloat(cess)) / 100;
                    }
                }
                if (typeof addup != 'undefined') {
                    if (addup != 0) {
                        addgst = (parseFloat(cost) * parseFloat(addup)) / 100;
                    }
                }
                price = parseFloat(price) + parseFloat(gst) + parseFloat(addgst) + parseFloat(cess1);
                if (roundup) {
                    price = Math.floor(price);
                    var rem = price % 5;
                    if (rem > 0) {
                        price = price - rem + 5;
                    }
                }
                $('#priceEdit').val(price);
            }
        });
    }

    /**
     * Comment
     */
    function editgetSupplier() {
        var storeid = $('#companiesEdits').val();
        var supid = <?= $product->supplier1 ?>;
        if (storeid != "") {
            $.ajax({
                type: "get",
                async: false,
//                url: "<?= site_url('purchases/getBiller') ?>/" + storeid,
                url: "<?= site_url('products/getsupplierfrompurchase') ?>/" + storeid,
                dataType: "json",
                success: function (scdata) {
//                    alert(JSON.stringify(scdata));
                    $('#supplier12Edit').select2('val', '');
                    $('#supplier12Edit').html('');
                    $.each(scdata, function (k, val) {
                        var opt = $('<option />');
                        opt.val(val.id);
                        if (supid == val.id) {
                            opt.prop('selected', 'selected');
                        }
                        opt.text(val.name + ' (' + val.code + ')');
                        $('#supplier12Edit').append(opt);
                    });
                    
                },
                error: function () {
                    bootbox.alert('<?= lang('ajax_error') ?>');
                    $('#modal-loading').hide();
                }
            });
        }
    }

    function loadorderitems() {
        var v = $('#reference_no').val();
        $.ajax({
            type: "get",
            data: {reference_no: v},
            async: false,
            url: site.base_url + "products/getProductsFromReference",
            dataType: "json",
            success: function (scdata) {
                if (scdata.status == 1) {
                    $.each(scdata.data, function (k, val) {
                        var mid = val.id,
                                mcode = val.code,
                                mname = val.name,
                                mqty = val.quantity,
                                sqty = val.squantity,
                                unit_price = val.cost,
                                brandname = val.brandname,
                                colorname = val.colorname,
                                designname = val.designname,
                                stylename = val.stylename,
                                patternname = val.patternname,
                                fittingname = val.fittingname,
                                fabricname = val.fabricname,
                                rate = val.rate,
                                uname = val.uname,
                                gst = val.gst
                        poitems[mid] = {"id": mid, "item_id": mid, "label": mname, "row": {"id": mid, "code": mcode, "page": 0, "cost": unit_price, "name": mname, "quantity": mqty, "squantity": sqty, "real_unit_cost": unit_price, "price": unit_price, "unit_price": unit_price, "real_unit_price": unit_price, "tax_rate": 0, "tax_method": 0, "qty": mqty, "type": "manual", "discount": 0, "serial": "", "option": "", "brandname": brandname, "colorname": colorname, "designname": designname, "stylename": stylename, "patternname": patternname, "fittingname": fittingname, "fabricname": fabricname, "rate": rate, "uname": uname, "gst": gst}, "tax_rate": 0, "options": true};
                        localStorage.setItem('poitems', JSON.stringify(poitems));
                        loadItems();
                    });
                    return false;
                }
            },
        });
    }
    ;
    $('#companiesEdits').trigger('change');



    $(document).ready(function () {
        $("#editOrderItmes").unbind().submit(function (e) {
            e.preventDefault();
            var form = $("#editOrderItmes");
            var $data = $(this).serialize();
            $.ajax({
                url: "<?= site_url('purchases/updateOrderProduct') ?>",
                type: "get", async: false,
                data: $data,
                dataType: 'json',
                success: function (data) {
                    if (data.s === "true") {
                        bootbox.alert('Product Updated Successfully!');
                        $("#myModal").modal('toggle');
                        loadorderitems();
                    } else {
                        bootbox.alert('Product Updating Failed!');
                        $("#myModal").modal('toggle');
                    }
                }
            });
            return false;
        });

        var obj = ['companiesEdit', 'departmentEdit', 'sectionEdit', 'product_itemsEdit', 'type_idEdit', 'brandsEdit', 'designEdit', 'styleEdit', 'patternEdit', 'fittingEdit', 'fabricEdit', 'colorEdit', 'sizeEdit', 'perEdit', 'unitEdit'];
        var objarr = [];
        var i = 0;
        $.each(obj, function (k, v) {
            objarr[i] = myselect2(v);
            objarr[i]();
            i++;
        });
        function myselect2(id) {
//            if (id == "per") {
            var pp = $("." + id);
//            } else {
//                var pp = $("#" + id);
//            }
            function get() {
                pp.select2({
//                    minimumInputLength: 1,
                    data: [],
                    initSelection: function (element, callback) {
                        $.ajax({
                            type: "get", async: false,
                            url: site_url + "products/getIdAttribute",
                            data: {
                                term: pp.val(),
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                            },
                            dataType: "json",
                            success: function (data) {
                                callback(data[0]);
                            }
                        });
                    },
                    ajax: {
                        url: site_url + "products/getIdAttributes",
                        dataType: 'json',
                        quietMillis: 15,
                        data: function (term, page) {
                            return {
                                term: term,
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                                limit: 10
                            };
                        },
                        results: function (data, page) {
                            if (data.results != null) {
                                return {results: data.results};
                            } else {
                                return {results: [{id: '', text: 'No Match Found'}]};
                            }
                        }
                    }
                });
            }
            return get;
        }

        function getidsx() {
//            alsert($("#selsize").data('id'));
            var $ids = $("#selsize").data('id').split(",");
            var $idvals = "";
            $.each($ids, function (k, v) {
                $idvals += ($("#" + v).val() ? $("#" + v).val() : "") + "-";
            });
            $idvals = $idvals.trim();
            return $idvals;
        }
        $("#selsize").select2({
//            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
//                alert(getidsx);
                $.ajax({
                    type: "get", async: false,
                    url: site_url + "products/getIdAttribute",
                    data: {
                        term: $(element).val(),
                        tab: $(element).data('tab'),
                        id: getidsx,
                        key: $(element).data('key'),
                    },
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site_url + "products/getIdAttributes",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        tab: $("#selsize").data('tab'),
                        id: getidsx,
                        key: $("#selsize").data('key'),
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'No Match Found'}]};
                    }
                }
            }
        });
    });


</script>
