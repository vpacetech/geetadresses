<?php
$v = "contra";
?>
<script>
    var oTable = ['sale_incentive'];
    $(document).ready(function () {
        oTable['sale_incentive'] = $('#contra_voucher').dataTable({
//        oTable = $('#getSaleIncentive').dataTable({
            "aaSorting": [[1, "desc"]],
            "aLengthMenu": [[5, 25, 50, 100, -1], [5, 25, 50, 100, "All"]],
            "iDisplayLength": 5,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('purchases/getContraVoucher') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [{"bSortable": false, "mRender": checkbox}, 
               null,
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                {"bSortable": false},
                 {"bSortable": false}],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                $('td:eq(6) > label', nRow).attr('onclick', "changeStatus('id'," + aData[0] + ",'sale_incentive')");
            },
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
            }
        }).fnSetFilteringDelay().dtFilter([
        ], "footer");
    });
</script>   
<script>
    deleteParcelRev = function (tab, id) {
        var table_design_display = 'sale_incentive';
        bootbox.confirm("Are you sure to delete?", function (r) {
            if (r == true) {
                $.ajax({
                    url: "<?= site_url() ?>" + "purchases/deleteParcelRev/" + tab + "/" + id,
                    type: 'GET',
                    data: {},
                    dataType: 'json',
                    success: function (data, textStatus, jqXHR) {
                        if (data.s == "true") {
                            bootbox.alert('Contra Voucher Deleted Successfully!');
                            oTable[table_design_display].fnDraw();
                        } else {
                            bootbox.alert('Deleting Failed!');
                        }
                    }
                });
            }
        });
    };

</script>

<?php
if ($Owner) {
    echo form_open('purchases/vouchers_action', 'id="action-form"');
//    echo form_open('purchases/vouchers_action' . ($warehouse_id ? '/' . $warehouse_id : ''), 'id="action-form"');
}
?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i
                class="fa-fw fa fa-barcode"></i><?= lang('contra_voucher'); ?>
        </h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right" class="tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li><a href="<?php echo site_url('purchases/AddContraVoucher'); ?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> <?= lang('added_contra_voucher') ?></a></li>
<!--                        <li><a href="<?php echo site_url('purchases/AddParcelSentVoucher'); ?>" data-toggle="modal"
                               data-target="#myModal"><i class="fa fa-plus"></i> <?= lang('add_parcel_sent_voucher') ?></a></li>-->
<!--<li><a href="<?= site_url('products/addChallan') ?>"><i class="fa fa-plus-circle"></i> <?= lang('add_challan') ?></a></li>-->
                        <!--<li><a href="#" id="pdf" data-action="export_pdf"><i class="fa fa-file-pdf-o"></i> <?= lang('export_to_pdf') ?></a></li>-->
                        <!--<li><a href="#" id="excel" data-action="export_excel"><i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?></a></li>-->
                        <li class="divider"></li>
                        <li><a href="#" class="bpo" title="<b><?= $this->lang->line("delete_contra_voucher") ?></b>"
                               data-content="<p><?= lang('r_u_sure') ?></p><button type='button' class='btn btn-danger' id='delete' data-action='delete'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button>" data-html="true" data-placement="left"><i class="fa fa-trash-o"></i> <?= lang('delete_contra_voucher') ?></a></li>
                    </ul>
                </li>
                <?php if (!empty($warehouses)) { ?>
                    <li class="dropdown">
                        <!--<a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-building-o tip" data-placement="left" title="<?= lang("warehouses") ?>"></i></a>-->
                        <ul class="dropdown-menu pull-right" class="tasks-menus" role="menu" aria-labelledby="dLabel">
                            <li><a href="<?= site_url('products') ?>"><i class="fa fa-building-o"></i> <?= lang('all_warehouses') ?></a></li>
                            <li class="divider"></li>
                            <?php
                            foreach ($warehouses as $warehouse) {
                                echo '<li><a href="' . site_url('products/' . $warehouse->id) . '"><i class="fa fa-building"></i>' . $warehouse->name . '</a></li>';
                            }
                            ?>
                        </ul>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?= lang('list_results'); ?></p>

                <div class="table-responsive" style="overflow-y: auto">
                    <table id="contra_voucher" class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                            <tr class="primary">
                                <th style="min-width:30px; width: 30px; text-align: center;">
                                    <input class="checkbox checkth" type="checkbox" name="check"/>
                                </th>
                                <th><?= lang('contra_no');?></th>
                                <th><?= lang('user');?></th>
                                <th><?= lang('transfer_type');?></th>
                                <th><?= lang('from_acc');?></th>
                                <th><?= lang('ref_no');?></th>
                                <th><?= lang('bank_acc_no') ?></th>
                                <th><?= lang('dated') ?></th>
                                <th><?= lang('amount') ?></th>
                                <th><?= lang('to_acc') ?></th>
                                <th><?= lang('rec_bank_acc') ?></th>
                                <th><?= lang('ref_no') ?></th>
                                <th><?= lang('balance') ?></th>
<!--                                <th><?= lang('status') ?></th>-->
                                <th style="width:40px; text-align:center;"><?= lang("actions") ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="6" class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                            </tr>
                        </tbody>

                        <tfoot class="dtFilter">
                            <tr class="active">
                                <th style="min-width:30px; width: 30px; text-align: center;">
                                    <input class="checkbox checkth" type="checkbox" name="check"/>
                                </th>
                                  <th><?= lang('paymentno');?></th>
                                <th><?= lang('user');?></th>
                            
                                <th><?= lang('payfor');?></th>
                                <th><?= lang('party_account');?></th>
                                <th><?= lang('againstno');?></th>
                                <th><?= lang('bank_acc_no') ?></th>
                                <th><?= lang('dated') ?></th>
                                <th><?= lang('amount') ?></th>
                                <th><?= lang('payment_mode') ?></th>
                               
                                <th><?= lang('paym_no') ?></th>
                                <th><?= lang('acc_no') ?></th>
                                <th><?= lang('balance') ?></th>
                                <!--<th><?= lang('status') ?></th>-->
                                <th style="width:40px; text-align:center;"><?= lang("actions") ?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <input type="hidden" name="table_name" value="contra_vouchers"/>
        
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>
