<style>
    .close{
        font-size: 19px !important;
    }
    .heading{
        margin-top:14px !important;
        border-top: 1px solid black;
    }
    .heading1{
        padding-top: 7px;
    }
    .modal-header{
        border-bottom: none !important;
    }
</style>
<?php echo $modal_js ?>
<div class="modal-dialog modal-lg ">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
        </div>
        <div class="modal-body">
            <div >
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <h2 style="margin:0;padding-top: 10px !important;">
                        <?= lang('contravoucher_view') ?>
                    </h2>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
                    <b style='font-size: 20px;'><?= $Settings->site_name ?></b>
                    <!--<h4>Kavathe - Mahankal</h4>-->
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 "></div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 heading" >
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                    <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="joining_date">  <?= lang('contra_no') ?> </label>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 heading1">
                        : <?php echo $voucher->payment_no ?>
                    </div>
                </div>
                <input type="text" name="type" value="payment" class="form-control hidden">
                
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                    <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="joining_date"><?= lang('date') ?> </label>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 heading1">
                         : <?php echo $voucher->date ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group">
                    <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="joining_date">  <?= lang('user') ?>  </label>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 heading1">
                      : <?= $voucher->user ?> <?= $voucher->last_name ?>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                    <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="joining_date"> <?= lang('time') ?> </label>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 heading1">
                         : <?= $voucher->time ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 heading" style="padding:0;">
                <h4 class="text-center" style="font-size: 20px; padding-bottom:5px;"><span style="text-decoration: underline;">Depositor </span>&nbsp; <span style="text-decoration: underline;">Details </span></h4>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="payfor"><?= lang('transfer_type') ?> :</label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                             : <?= $voucher->transfer_type ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="party_account"><?= lang('from_party') ?> </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            
                            
                            : <?php 
                            $r = $this->db->get_where('bank', array('id'=> $voucher->from_acc))->row();
                            echo $r->bank_name;
                             ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="againstno"><?= lang('ref_no') ?> </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                           : <?= $voucher->against_ref_no?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="joining_date"><?= lang('bank_acc_no') ?> </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <?=  $voucher->bank_acc_no?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="dated"><?= lang('dated') ?> </label>

                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                             : <?=  $this->sma->hrld($voucher->datedd) ?>
                            <?php // echo form_input('datedd', (isset($_POST['date']) ? $_POST['date'] : $this->sma->hrld($voucher->datedd)), 'class="form-control input-tip datetime" id="podate" required="required"'); ?>                        
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="amount"><?= lang('amount') ?> </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">

                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="padding: 0">
                                 : <?= $voucher->amount ?>
                                <?php // echo form_input('amount', (isset($_POST['amount']) ? $_POST['amount'] : $voucher->amount), 'class="form-control input-tip" required="required"'); ?>                        
                                <!--<input type="text" name="amount" class="form-control" placeholder="55,689.00" required=""  >-->
                            </div>
                            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12" style="padding-right: 0">CR
                                <!--<input type="text"  class="form-control" placeholder="cr." disabled>-->
                            </div>
                        </div>
                    </div>
                </div>
                
                
               
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 table-responsive"  <?= $voucher->from_acc != "Cash" ? 'style="display: none"': '' ?>>
                     <hr>
                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                         <h3><b><?=  lang("currency_note_details") ?></b></h3>
                        </div>
                    <table class="table table-bordered table-striped" style="width: 100%">
                        <tr class="text-center">
                            <td>10 X</td>
                            <td>20 X</td>
                            <td>50 X</td>
                            <td>100 X</td>
                            <td>500 X</td>
                            <td>2000 X</td>
                            <td>Other(Remaining)</td>
                            <td>Total</td>
                        </tr>
                        <tr class="text-center">
                            <td><?= $voucher->ten ?></td>
                            <td><?= $voucher->twenty ?></td>
                            <td><?= $voucher->fifty ?></td>
                            <td><?= $voucher->hundread ?></td>
                            <td><?= $voucher->fivehundread ?></td>
                            <td><?= $voucher->twothousand ?></td>
                            <td><?= $voucher->coins ?></td>
                            <td><?= $voucher->amount ?></td>
                        </tr>
                    </table>
                </div>

                
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 heading" style="padding:0;">
                <h4 class="text-center" style="font-size: 20px; padding-bottom:5px;"><span style="text-decoration: underline;">Depository </span>&nbsp; <span style="text-decoration: underline;">Details </span></h4>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4  col-xs-12" for="payment_mode"><?= lang('to_acc') ?> </label>
                        <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                             : <?= $voucher->bank_name ?>
<!--                            <select class="form-control col-lg-5 col-md-5 col-sm-5 col-xs-12" id="sel1" name="payment_mode">
                                <option value="cheque">Cheque Payment</option>
                            </select>-->
                        </div>
                    </div>
                      <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="bank_acc_number"><?= lang('acc_no') ?> </label>
                        <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                            : <?= $voucher->receiver_bank_acc_no ?>
                        </div>
                    </div>
                   
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="pay_type_no"><?= lang('mode_of_trans') ?></label>
                        <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                              : <?= $voucher->mode_of_transfer ?>
                        </div>
                    </div>
                   <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="from_acc"><?= lang('ref_no') ?> </label>
                        <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                            : <?= $voucher->ref_no ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="joining_date"><?= lang('dated') ?></label>
                        <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                                       : <?= $this->sma->hrld($voucher->dated) ?>                  
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="joining_date"><?= lang('balance') ?> </label>
                        <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="padding: 0">
                                 : <?= $voucher->balance ?>
                            </div>
                            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12" style="padding-right: 0">Dr.
                                <!--<input type="text" class="form-control" placeholder="Dr." required=""  >-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer" style="clear:both;text-align: left;">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                        <b>Narration</b> : <?php echo $voucher->narration;?>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function (e) {
        $('#addFormx').bootstrapValidator({
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            }, excluded: [':disabled']
        });
        $('select.select').select2({minimumResultsForSearch: 6});
        fields = $('.modal-content').find('.form-control');
        $.each(fields, function () {
            var id = $(this).attr('id');
            var iname = $(this).attr('name');
            var iid = '#' + id;
            if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
//                $("label[for='" + id + "']").append(' *');
                $(document).on('change', iid, function () {
                    $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
                });
            }
        });
    });

//    mrp = onlydigits('mrp');
//    mrp();
//    mrp_from = onlydigits('mrp_from');
//    mrp_from();
//    mrp_to = onlydigits('mrp_to');
//    mrp_to();
//    sales_incentive = onlydigits('sales_incentive');
//    sales_incentive();
//    min_qty_lvl = onlydigits('min_qty_lvl');
//    min_qty_lvl();
//    min_qty_lvl_2 = onlydigits('min_qty_lvl_2');
//    min_qty_lvl_2();
//    reorder_qty = onlydigits('reorder_qty');
//    reorder_qty();
//    reorder_qty_2 = onlydigits('reorder_qty_2');
//    reorder_qty_2();
    $(document).ready(function () {
//        $("#addFormx").unbind().submit(function (e) {
//            e.preventDefault();
//            var form = $("#addFormx");
//            var $data = $(this).serialize();
////            $data += "&name=<?= $this->security->get_csrf_token_name() ?>&value=<?= $this->security->get_csrf_hash() ?>";
//            $.ajax({
//                url: "<?= site_url('system_settings/SaveMin_qty_lvl/?v=1') ?>",
//                type: "get", async: false,
//                data: $data,
//                dataType: 'json',
//                success: function (data, textStatus, jqXHR) {
//                    if (data.s === "true") {
//                        bootbox.alert('Paramenter Added Successfully!');
//                        oTable.fnDraw();
//                        $("#myModal").modal('hide');
//                    } else {
//                        bootbox.alert('Paramenter Adding Failed!');
//                        $("#myModal").modal('hide');
//                    }
//                }
//            });
//            return false;
//        });
        var obj = ['companies', 'department', 'section', 'product_items', 'type', 'brands', 'design', 'style', 'pattern', 'fitting', 'fabric', 'color', 'size', 'per'];
        var objarr = [];
        var i = 0;
        $.each(obj, function (k, v) {
            objarr[i] = myselect2(v);
            objarr[i]();
            i++;
        });
        function myselect2(id) {
            if (id == "per") {
                var pp = $("." + id);
            } else {
                var pp = $("#" + id);
            }
            function get() {
                pp.select2({
                    minimumInputLength: 1,
                    data: [],
                    initSelection: function (element, callback) {
                        $.ajax({
                            type: "get", async: false,
                            url: site_url + "products/getIdAttribute",
                            data: {
                                term: pp.val(),
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                            },
                            dataType: "json",
                            success: function (data) {
                                callback(data[0]);
                            }
                        });
                    },
                    ajax: {
                        url: site_url + "products/getIdAttributes",
                        dataType: 'json',
                        quietMillis: 15,
                        data: function (term, page) {
                            return {
                                term: term,
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                                limit: 10
                            };
                        },
                        results: function (data, page) {
                            if (data.results != null) {
                                return {results: data.results};
                            } else {
                                return {results: [{id: '', text: 'No Match Found'}]};
                            }
                        }
                    }
                });
            }
            return get;
        }
        function getidsx() {
//            alsert($("#selsize").data('id'));
            var $ids = $("#selsize").data('id').split(",");
            var $idvals = "";

            $.each($ids, function (k, v) {
                $idvals += ($("#" + v).val() ? $("#" + v).val() : "") + "-";
            });
            $idvals = $idvals.trim();
            return $idvals;
        }
        $("#selsize").select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site_url + "products/getIdAttribute",
                    data: {
                        term: $(element).val(),
                        tab: $(element).data('tab'),
                        id: getidsx,
                        key: $(element).data('key'),
                    },
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site_url + "products/getIdAttributes",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        tab: $("#selsize").data('tab'),
                        id: getidsx,
                        key: $("#selsize").data('key'),
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'No Match Found'}]};
                    }
                }
            }
        });
    });
</script>
