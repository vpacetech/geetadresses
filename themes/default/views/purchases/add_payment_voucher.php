<style>
    .close{
        font-size: 19px !important;
    }
    .heading{
        margin-top:14px !important;
        border-top: 1px solid black;
    }
    .heading1{
        padding-top: 7px;
    }
    .modal-header{
        border-bottom: none !important;
    }
</style>
<?php echo $modal_js ?>
<div class="modal-dialog modal-lg ">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
        </div>
        <?php
        $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'addFormx');
        echo form_open_multipart("purchases/SavepaymentVoucher", $attrib);
        ?>
        <div class="modal-body">
            <div >
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <h2 style="margin:0;padding-top: 10px !important;">
                        <?= lang('addpaymentvoucher') ?>
                    </h2>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
                    <b style='font-size: 20px;'><?= $Settings->site_name ?></b>
                    <!--                    <h4>Kavathe - Mahankal</h4>-->
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 "></div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 heading" >
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 text-right">
                        <button type="button" onclick="enableEdit()"><?= lang('define_my_own_Code') ?></button>
                    </div>
                    <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="joining_date"> 
                        <?= lang('paymentno') ?> 
                    </label>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 heading1">
                        <input type="text" name="payment_no" value="<?php echo 'GD-PY' . $latest_pay_no ?>" class="form-control remove_readonly" readonly>
                    </div>
                </div>
                <input type="text" name="type" value="payment" class="form-control hidden">

                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                    <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="joining_date"><?= lang('date') ?> </label>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 heading1">
                        <?php echo form_input('date', date('d/m/Y') . '(' . date('l') . ')', 'class="form-control tip" id="sender_address" data-bv-notempty="true"  readonly'); ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group">
                    <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12">  <?= lang('user') ?>  </label>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 heading1">

                        <?php
                        $bl = "";
                        $bl[''] = "Select User";
                        foreach ($users as $row) {
                            $bl[$row->id] = $row->first_name . ' ' . $row->last_name;
                        }
                        echo form_dropdown('user_name', $bl, set_value('user_name', ''), 'data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                        ?>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                    <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="joining_date"> <?= lang('time') ?> </label>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 heading1">
                        <?php echo form_input('time', date('h:i a'), 'class="form-control tip" id="sender_address" data-bv-notempty="true" readonly'); ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 heading" style="padding:0;">
                <h4 class="text-center" style="font-size: 20px; padding-bottom:5px;"><span style="text-decoration: underline;">Beneficiary </span>&nbsp; <span style="text-decoration: underline;">Details </span></h4>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="payfor">1. <?= lang('payfor') ?> :</label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <!--<select class="form-control col-lg-5 col-md-5 col-sm-5 col-xs-12" name="for" id="sel1"  id="payment_for" >-->
                            <select class="form-control col-lg-5 col-md-5 col-sm-5 col-xs-12" name="for" id="payment_for" >
                                <option value="purchase bill">Purchase Bill</option>
                                <option value="on account">On Account</option>
                            </select>                             
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="party_account">2. <?= lang('party_account') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php
                            $bl = "";
                            foreach ($store as $row) {
                                $bl[$row->id] = $row->code;
                            }
                            echo form_dropdown('account', $bl, set_value('account', $data->supplier_id), 'data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" id="get_acc_no" class="form-control input-tip select" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="dated">3. <?= lang('dated') ?> </label>

                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php echo form_input('datedd', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="podate" required="required"'); ?>                        
                        </div>
                    </div>


                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="joining_date">4. <?= lang('bank_acc_no') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <input type="text" name="bank_acc_no" id="bank" class="form-control" readonly>

                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="againstno">5. <?= lang('againstno') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php
                            echo form_dropdown('against_ref_no[]', "", set_value('payfor', ''), 'data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("againstno") . '" required="required" class="form-control input-tip select" multiple id="ref"  style="width:100%;height:auto;"');
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="amount">6. <?= lang('amount') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">

                            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12" style="padding: 0">
                                <?php
                                echo form_input('amount', set_value('amount', $data->grand_total), 'class="form-control" id="ammount" readonly required="required" '
                                        . 'data-bv-notempty="true"
                               data-bv-notempty-message="Amount cannot be empty"');
                                ?>

                                <?php // echo form_input('amount', 'class="form-control input-tip datetime" required="required"');    ?>                        
<!--<input type="text" name="amount" class="form-control" required=""  >-->
                            </div>
                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="padding-right: 0">
                                <input type="text"  class="form-control" placeholder="cr.">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 heading" style="padding:0;">
                <h4 class="text-center" style="font-size: 20px; padding-bottom:5px;"><span style="text-decoration: underline;">Payer </span>&nbsp; <span style="text-decoration: underline;">Details </span></h4>
                <!--<h4 class="text-center" style="font-size: 20px; text-decoration: underline;padding-bottom:5px;">Payer Details</h4>-->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="payment_mode">7. <?= lang('payment_mode') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <select class="form-control col-lg-5 col-md-5 col-sm-5 col-xs-12" id="sel1" name="payment_mode">
                                <option value="cheque">Cheque Payment</option>
                                <option value="rtgs">RTGS</option>
                                <option value="neft">NEFT</option>
                                <option value="imps">IMPS</option>
                                <option value="upi">UPI</option>
                                <option value="other">Other</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="from_acc">8. <?= lang('from_acc') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php
                            $bl = "";
                            $bl[''] = "Select Bank";
                            foreach ($bank_acc_no as $row) {
                                $bl[$row->id] = $row->bank_name;
                            }
                            echo form_dropdown('from_acc', $bl, set_value('payfor', $row->bank_name), 'id="formAccountNo" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="pay_type_no">9. <?= lang('paym_no') ?></label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php
                            echo form_input('pay_type_no', set_value('pay_type_no', ''), 'class="form-control"'
                                    . 'data-bv-stringlength="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="Payment type no can only consist of digits"');
                            ?>
                            
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="bank_acc_number">10. <?= lang('acc_no') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php
                            $b = "";
                            $b[''] = "Select Account No";
//                            foreach ($bank_acc_no as $row) {
//                                $b[$row->account_no] = $row->account_no;
//                            }
//                            
                            echo form_dropdown('bank_acc_number', $b, set_value('payfor', $row->account_no), 'data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("acc_no") . '" required="required" id="payeeBankDetails" class="form-control input-tip select" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="joining_date">11. <?= lang('dated') ?></label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php echo form_input('dated', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="podates" required="required"'); ?>                        
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="joining_date">12. <?= lang('balance') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12" style="padding: 0">
                                <?php
                                echo form_input('balance', set_value('balance', ''), 'class="form-control" required="required" '
                                        . 'data-bv-notempty="true"
                               data-bv-notempty-message="Balance no cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]\d*(\.\d+)?$"
                               data-bv-regexp-message="Balance can only consist of digits"');
                                ?>
                                <!--<input type="text" name="balance" class="form-control"  required=""  >-->
                            </div>
                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="padding-right: 0">
                                <input type="text" class="form-control" placeholder="Dr." required=""  >
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer" style="clear:both;text-align: left;">



            <div class="row">

                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                            <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12 control-label ">
                                <b>Narration </b>
                            </div>
                            <div class="col-lg-9 col-sm-9 col-md-9 col-xs-12">
                                <input type="text" name="narration" class="form-control" >
                                    <!--<text type="text" name="narration">-->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-md-offset-5"><?php echo form_submit('add_transfer', $this->lang->line("submit"), 'id="add_transfer" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>

                    </div>
                </div>
            </div>
        </div>




        <?php echo form_close(); ?>

        <!---->
    </div><!--end body div-->

    <!--end foter div-->

</div>


<script type="text/javascript">
    $(document).ready(function (e) {
        $('#addFormx').bootstrapValidator({
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            }, excluded: [':disabled']
        });
        $('select.select').select2({minimumResultsForSearch: 6});
        fields = $('.modal-content').find('.form-control');
        $.each(fields, function () {
            var id = $(this).attr('id');
            var iname = $(this).attr('name');
            var iid = '#' + id;
            if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
//                $("label[for='" + id + "']").append(' *');
                $(document).on('change', iid, function () {
                    $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
                });
            }
        });
    });

//    mrp = onlydigits('mrp');
//    mrp();
//    mrp_from = onlydigits('mrp_from');
//    mrp_from();
//    mrp_to = onlydigits('mrp_to');
//    mrp_to();
//    sales_incentive = onlydigits('sales_incentive');
//    sales_incentive();
//    min_qty_lvl = onlydigits('min_qty_lvl');
//    min_qty_lvl();
//    min_qty_lvl_2 = onlydigits('min_qty_lvl_2');
//    min_qty_lvl_2();
//    reorder_qty = onlydigits('reorder_qty');
//    reorder_qty();
//    reorder_qty_2 = onlydigits('reorder_qty_2');
//    reorder_qty_2();
    $(document).ready(function () {
//        $("#addFormx").unbind().submit(function (e) {
//            e.preventDefault();
//            var form = $("#addFormx");
//            var $data = $(this).serialize();
////            $data += "&name=<?= $this->security->get_csrf_token_name() ?>&value=<?= $this->security->get_csrf_hash() ?>";
//            $.ajax({
//                url: "<?= site_url('system_settings/SaveMin_qty_lvl/?v=1') ?>",
//                type: "get", async: false,
//                data: $data,
//                dataType: 'json',
//                success: function (data, textStatus, jqXHR) {
//                    if (data.s === "true") {
//                        bootbox.alert('Paramenter Added Successfully!');
//                        oTable.fnDraw();
//                        $("#myModal").modal('hide');
//                    } else {
//                        bootbox.alert('Paramenter Adding Failed!');
//                        $("#myModal").modal('hide');
//                    }
//                }
//            });
//            return false;
//        });
        var obj = ['companies', 'department', 'section', 'product_items', 'type', 'brands', 'design', 'style', 'pattern', 'fitting', 'fabric', 'color', 'size', 'per'];
        var objarr = [];
        var i = 0;
        $.each(obj, function (k, v) {
            objarr[i] = myselect2(v);
            objarr[i]();
            i++;
        });
        function myselect2(id) {
            if (id == "per") {
                var pp = $("." + id);
            } else {
                var pp = $("#" + id);
            }
            function get() {
                pp.select2({
                    minimumInputLength: 1,
                    data: [],
                    initSelection: function (element, callback) {
                        $.ajax({
                            type: "get", async: false,
                            url: site_url + "products/getIdAttribute",
                            data: {
                                term: pp.val(),
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                            },
                            dataType: "json",
                            success: function (data) {
                                callback(data[0]);
                            }
                        });
                    },
                    ajax: {
                        url: site_url + "products/getIdAttributes",
                        dataType: 'json',
                        quietMillis: 15,
                        data: function (term, page) {
                            return {
                                term: term,
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                                limit: 10
                            };
                        },
                        results: function (data, page) {
                            if (data.results != null) {
                                return {results: data.results};
                            } else {
                                return {results: [{id: '', text: 'No Match Found'}]};
                            }
                        }
                    }
                });
            }
            return get;
        }
        function getidsx() {
//            alsert($("#selsize").data('id'));
            var $ids = $("#selsize").data('id').split(",");
            var $idvals = "";

            $.each($ids, function (k, v) {
                $idvals += ($("#" + v).val() ? $("#" + v).val() : "") + "-";
            });
            $idvals = $idvals.trim();
            return $idvals;
        }
        $("#selsize").select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site_url + "products/getIdAttribute",
                    data: {
                        term: $(element).val(),
                        tab: $(element).data('tab'),
                        id: getidsx,
                        key: $(element).data('key'),
                    },
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site_url + "products/getIdAttributes",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        tab: $("#selsize").data('tab'),
                        id: getidsx,
                        key: $("#selsize").data('key'),
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'No Match Found'}]};
                    }
                }
            }
        });
    });
    $('#get_acc_no').change(function () {
        var v = $(this).val();
        $('#modal-loading').show();
        if (v) {
            $.ajax({
                type: "get",
                async: false,
                url: "<?= site_url('purchases/getAccDetails') ?>/" + v,
                dataType: "json",
                success: function (scdata) {

                    if (scdata.acc_no == null) {

                        bootbox.alert('<?= lang('bank_detail_not_avail') ?>');
                        $('#bank').val('');
                    } else {
                        data = scdata.acc_no;
                        $('#bank').val(data);

                    }
                },
                error: function () {
                    bootbox.alert('<?= lang('ajax_error') ?>');
                    $('#modal-loading').hide();
                }
            });
            $.ajax({
                type: "get",
                async: false,
                url: "<?= site_url('purchases/getRefDetails') ?>/" + v,
                dataType: "json",
                success: function (scdata) {
                    var sel = '<?= $data->id ?>';
                    $('#ref').html('');
                    if (scdata == null) {
                        bootbox.alert('<?= lang('ref_detail_not_avail') ?>');

                    } else {
                        $('#ref').html('');

                        $.each(scdata, function (k, val) {
                            var opt = '<option value="' + val.id + '" ';
                            if (sel == val.id) {
                                opt += 'selected ="selected"';
                            }
                            opt += '>' + val.reference_no + '</option>';
                            $('#ref').append(opt);
                        });
                        var sel = '<?= $data->id ?>';
                        $(this).select2('val', sel);
                    }
                },
                error: function () {
                    bootbox.alert('<?= lang('ajax_error') ?>');
                    $('#modal-loading').hide();
                }
            });
        }
    });

    $('#ref').change(function () {
        var v = $(this).val();
        $.ajax({
            type: "get",
            async: false,
            data: {bill_no: v},
            url: "<?= site_url('purchases/getBillAmounts') ?>",
            dataType: "json",
            success: function (data) {
                $('#ammount').val(data.total);
                $('#podate').val(data.dates);
            },
        });
    });

    $('#formAccountNo').change(function () {
        var v = $(this).val();
        $.ajax({
            type: "get",
            async: false,
            data: {id: v},
            url: "<?= site_url('purchases/bankAccountNoByBank') ?>",
            dataType: "json",
            success: function (data) {
                $('#payeeBankDetails').html('');
                
                $('payeeBankDetails').trigger('change');
                $.each(data, function (k, val) {
                    var opt = $('<option />');
                    opt.val(val.account_no);
                    opt.text(val.account_no);
                    $('#payeeBankDetails').append(opt);
                })
            },
        });
    });


    $('#payment_for').change(function () {
        var v = $(this).val();
        if (v == 'on account') {
            $('#ref').attr('readonly', '')
        } else {
            $('#ref').removeAttr('readonly', '')
        }
    });

<?php if ($data->id != "") { ?>
        $('#get_acc_no').trigger('change');
        $('#ref').trigger('change');
<?php }
?>


</script>
