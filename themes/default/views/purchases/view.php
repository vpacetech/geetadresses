<?php


$ag_ordernos_arr = [];
foreach($ag_ordernos as $ord)
$ag_ordernos_arr[] = $ord['ag_order_no'];

$lrno_arr = [];
foreach($lrnos as $lr)
$lrno_arr[] = $lr['lr_no'];

// echo "<pre>"; print_r($ag_ordernos_arr);exit;
?>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-file"></i><?= lang("purchase_no") . '. ' . $inv->reference_no; ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right" class="tasks-menus" role="menu" aria-labelledby="dLabel">
                        <?php if ($inv->attachment) { ?>
                            <li>
                                <a href="<?= site_url('welcome/download/' . $inv->attachment) ?>">
                                    <i class="fa fa-chain"></i> <?= lang('attachment') ?>
                                </a>
                            </li>
                        <?php } ?>
                        <li><a href="<?= site_url('purchases/payments/' . $inv->id) ?>" data-target="#myModal"
                               data-toggle="modal"><i class="fa fa-money"></i> <?= lang('view_payments') ?></a></li>
                        <li><a href="<?= site_url('purchases/AddPaymentVoucher/' . $inv->id) ?>" data-target="#myModal"
                               data-toggle="modal"><i class="fa fa-money"></i> <?= lang('add_payment') ?></a></li>
                        <li><a href="<?= site_url('purchases/edit/' . $inv->id) ?>"><i
                                    class="fa fa-edit"></i> <?= lang('edit_purchase') ?></a></li>
                        <li><a href="<?= site_url('purchases/email/' . $inv->id) ?>"><i
                                    class="fa fa-envelope-o"></i> <?= lang('send_email') ?></a></li>
                        <li><a href="<?= site_url('purchases/pdf/' . $inv->id) ?>"><i
                                    class="fa fa-file-pdf-o"></i> <?= lang('export_to_pdf') ?></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="print-only col-xs-12">
                    <img src="<?= base_url() . 'assets/uploads/logos/' . $Settings->logo; ?>"
                         alt="<?= $Settings->site_name; ?>">
                </div>
                <div class="col-xs-5">
                   <!--<div class="col-xs-2"><i class="fa fa-3x fa-building padding010 text-muted"></i></div>-->
                    <div class="col-xs-10"> 
                        <span style="font-weight:bold; font-size: 16px;"><?= lang("pur_no"); ?>: </span><span><?= $inv->reference_no; ?>  </span>
                        <br>
                        <!-- <span style="font-weight:bold;"><?= lang("lr_no"); ?>:</span><span> <?= $inv->lr_no; ?></span> -->
                        <span style="font-weight:bold;"><?= lang("lr_no"); ?>:</span><span> <?= implode(", ", $lrno_arr) ?></span>
                        <br>
                        <span style="font-weight:bold; font-size: 16px;"><?= lang("supplier"); ?>: </span><span>  <?= $inv->supplier; ?></span>
                        <br>
                        <!-- <span style="font-weight:bold; font-size: 16px;"><?= lang("ag_order_no"); ?>: </span><span>  <?= $ag_ordernos->ag_order_no; ?></span>    -->
                        <span style="font-weight:bold; font-size: 16px;"><?= lang("ag_order_no"); ?>: </span><span>  <?= implode(", ", $ag_ordernos_arr); ?></span>   
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-xs-3"></div>
                <div class="col-xs-4">
                    <div class="col-xs-2"></div>
                    <div class="col-xs-10"> 
                        <!--<h2 class=""><?= lang("user"); ?>: <?= $inv->supplier; ?></h2>-->
                        <span style="font-weight:bold; font-size: 16px;"><?= lang("bill_date"); ?>: </span><span><?= $this->sma->hrld($inv->date); ?></span>   
                        <br>
                        <span style="font-weight:bold; font-size: 16px;"><?= lang("sup_bill_no"); ?>:  </span><span><?= $inv->reference_no; ?></span>
                        <br>
                        <span style="font-weight:bold; font-size: 16px;"><?= lang("sorted_by"); ?>:  </span><span><?= $sorter->first_name  . ' ' . $sorter->last_name; ?></span>
                        <!--<span style="font-weight:bold; font-size: 16px;"><?= lang("invoice_date"); ?>: </span><span><?= $this->sma->hrld($rows[0]->ordered_date); ?></span>-->   
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>


    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="print-only col-xs-12">
                    <img src="<?= base_url() . 'assets/uploads/logos/' . $Settings->logo; ?>"
                         alt="<?= $Settings->site_name; ?>">
                </div>
                <!--                <div class="well well-sm">
                                    <div class="col-xs-4 border-right">
                
                                        <div class="col-xs-2"><i class="fa fa-3x fa-building padding010 text-muted"></i></div>
                                        <div class="col-xs-10">
                                            <h2 class=""><?= $supplier->company ? $supplier->company : $supplier->name; ?></h2>
                <?= $supplier->company ? "" : "Attn: " . $supplier->name ?>
                
                <?php
                echo $supplier->address . "<br />" . $supplier->city . " " . $supplier->postal_code . " " . $supplier->state . "<br />" . $supplier->country;

                echo "<p>";

                if ($supplier->cf1 != "-" && $supplier->cf1 != "") {
                    echo "<br>" . lang("scf1") . ": " . $supplier->cf1;
                }
                if ($supplier->cf2 != "-" && $supplier->cf2 != "") {
                    echo "<br>" . lang("scf2") . ": " . $supplier->cf2;
                }
                if ($supplier->cf3 != "-" && $supplier->cf3 != "") {
                    echo "<br>" . lang("scf3") . ": " . $supplier->cf3;
                }
                if ($supplier->cf4 != "-" && $supplier->cf4 != "") {
                    echo "<br>" . lang("scf4") . ": " . $supplier->cf4;
                }
                if ($supplier->cf5 != "-" && $supplier->cf5 != "") {
                    echo "<br>" . lang("scf5") . ": " . $supplier->cf5;
                }
                if ($supplier->cf6 != "-" && $supplier->cf6 != "") {
                    echo "<br>" . lang("scf6") . ": " . $supplier->cf6;
                }

                echo "</p>";
                echo lang("tel") . ": " . $supplier->phone . "<br />" . lang("email") . ": " . $supplier->email;
                ?>
                                        </div>
                                        <div class="clearfix"></div>
                
                                    </div>
                                    <div class="col-xs-4">
                
                                        <div class="col-xs-2"><i class="fa fa-3x fa-truck padding010 text-muted"></i></div>
                                        <div class="col-xs-10">
                                            <h2 class=""><?= $Settings->site_name; ?></h2>
                <?= $warehouse->name ?>
                
                <?php
                echo $warehouse->address . "<br>";
                echo ($warehouse->phone ? lang("tel") . ": " . $warehouse->phone . "<br>" : '') . ($warehouse->email ? lang("email") . ": " . $warehouse->email : '');
                ?>
                                        </div>
                                        <div class="clearfix"></div>
                
                
                                    </div>
                                    <div class="col-xs-4 border-left">
                
                                        <div class="col-xs-2"><i class="fa fa-3x fa-file-text-o padding010 text-muted"></i></div>
                                        <div class="col-xs-10">
                                            <h2 class=""><?= lang("ref"); ?>: <?= $inv->reference_no; ?></h2>
                
                                            <p style="font-weight:bold;"><?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?></p>
                
                                            <p style="font-weight:bold;"><?= lang("status"); ?>: <?= $inv->status; ?></p>
                                        </div>
                                        <div class="col-xs-12">
                <?php $br = $this->sma->save_barcode($inv->reference_no, 'code39', 35, false); ?>
                                            <img
                                                src="<?= base_url() ?>assets/uploads/barcode<?= $this->session->userdata('user_id') ?>.png"
                                                alt="<?= $inv->reference_no ?>"/>
                <?php $this->sma->qrcode('link', urlencode(site_url('sales/view/' . $inv->id)), 1); ?>
                                            <img
                                                src="<?= base_url() ?>assets/uploads/qrcode<?= $this->session->userdata('user_id') ?>.png"
                                                alt="<?= $inv->reference_no ?>"/>
                                        </div>
                                        <div class="clearfix"></div>
                
                
                                    </div>
                                    <div class="clearfix"></div>
                                </div>-->

                <div class="table-responsive" style="overflow-y: scroll">
                    <table class="table table-bordered table-hover table-striped print-table order-table">
                        <thead>
                            <tr>
                                <th><?= lang("no"); ?></th>
                                <th><?= lang("Dept.") ?>
                                <th><?= lang("Product") ?>
                                <th><?= lang("Type") ?>
                                <th><?= lang("Brands") ?>
                                <th><?= lang("Design") ?>
                                <th><?= lang("Style") ?>
                                <th><?= lang("Fitting") ?>
                                <th><?= lang("Pattern") ?>
                                <th><?= lang("Fabric") ?>
                                <th><?= lang("Pur. Rate") ?>
                                <th><?= lang("MRP") ?>
                                <th><?= lang("Color") ?>
                                <th><?= lang("Size") ?>
                                <th><?= lang("quantity"); ?></th>
                                <th><?= lang("sqty"); ?></th>
                                <?php
                                if ($Settings->tax1) {
                                    echo '<th style="padding-right:20px; text-align:center; vertical-align:middle;">' . lang("tax") . '</th>';
                                }
                                if ($Settings->product_discount != 0) {
                                    echo '<th style="padding-right:20px; text-align:center; vertical-align:middle;">' . lang("discount") . '</th>';
                                }
                                ?>
                                <th style="padding-right:20px;"><?= lang("Rate"); ?></th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $r = 1;
                            $box = 0;
                            $bund_taga = 0;
                            $pcs_mtr = 0;
                            $qty = 0;
                            $gstdetail = array();
                            foreach ($rows as $row) {
                                $qty = $row->quantity + $qty;
                                $pcs_mtr = $row->quantity + $pcs_mtr;
                                $box = $row->squantity;
                                if ($row->sunit == 5) {
                                    $box = $row->squantity + $box;
                                    $pcs_mtr = $row->quantity + $pcs_mtr;
                                }
                                if ($row->sunit == 7 || $row->sunit == 8) {
                                    $bund_taga = $row->squantity + $bund_taga;
                                }
                                if ($row->sunit == 2 || $row->sunit == 3) {
//                                    $pcs_mtr = $row->squantity + $pcs_mtr;
                                    $pcs_mtr = $row->squantity + $pcs_mtr;
                                } else {
//                                    $pcs_mtr = $row->quantity + $pcs_mtr;
                                }
                                if (empty($row->sunit)) {
                                    $box = $row->squantity + $box;
                                    $pcs_mtr = $row->quantity + $pcs_mtr;
                                }
//                               
                                $row->prod = $this->site->getProductByIDOrder($row->product_id, $row->product_code);

                                $gstdetail[] = array('total' => $row->subtotal, 'gst' => $row->prod->gst);

//                                die();
//                                echo "<pre>";
//                                print_r($row->prod);
//                                print_r($row->subtotal);
//                                echo "</pre>";
//                                die();
                                ?>
                                <tr>
                                    <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
    <!--                                    <td style="vertical-align:middle;"><?= $row->product_name . " (" . $row->product_code . ")" . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                    <?= $row->details ? '<br>' . $row->details : ''; ?></td>-->
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->department, 'department')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->product_items, 'product_items')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->type_id, 'type')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->brands, 'brands')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->design, 'design')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->style, 'style')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->fitting, 'fitting')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->pattern, 'pattern')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->fabric, 'fabric')->name ?></td>
                                    <td style="text-align:right; width:120px; padding-right:10px;"><?= $this->sma->formatMoney($row->net_unit_cost); ?></td>
                                    <td ><?= $this->sma->formatMoney($row->prod->price) ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->color, 'color')->name . "(" . $row->prod->colorqty . ")" ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->size, 'size')->name . "(" . $row->prod->sizeangle . ")" ?></td>
                                    <td style="width: 120px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity); ?></td>
                                    <td style="width: 120px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->sqty); ?></td>
                                    <?php
                                    if ($Settings->tax1) {
                                        echo '<td style="width: 120px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 && $row->tax_code ? '<small>(' . $row->tax_code . ')</small> ' : '') . $this->sma->formatMoney($row->item_tax) . '</td>';
                                    }
                                    if ($Settings->product_discount != 0) {
                                        echo '<td style="width: 120px; text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small>' : '') . ' ' . $this->sma->formatMoney($row->item_discount) . '</td>';
                                    }
                                    ?>
                                    <td style="text-align:right; width:120px; padding-right:10px;"><?= $this->sma->formatMoney($row->subtotal); ?></td>

                                </tr>
                                <?php
                                $r++;
                            }

//                            foreach ($gstdetail as $row) {
//                                echo "<pre>";
//                                print_r($row);
//                                echo "</pre>";
////                                die();
//                                if (in_array($row['gst'], $gstdetail)) {
//                                 
//                                    $gstdetails[$row['gst']]['total'] = floatval($row['total']) + floatval($gstdetail[$row->prod->gst]['total']);
//                                } else {
//                                    $gstdetails[$row['gst']] = $row;
//                                }
//                            }
//                            echo "<pre>";
//                            print_r($gstdetails);
//                            echo "</pre>";
//                            die();
                            ?>
                        </tbody>
                        <tfoot>
                            <?php
                            $col = 15;
                            if ($Settings->product_discount) {
                                $col++;
                            }
                            if ($Settings->tax1) {
                                $col++;
                            }
                            if ($Settings->product_discount && $Settings->tax1) {
                                $tcol = $col - 2;
                            } elseif ($Settings->product_discount) {
                                $tcol = $col - 1;
                            } elseif ($Settings->tax1) {
                                $tcol = $col - 1;
                            } else {
                                $tcol = $col;
                            }
                            ?>
                            <tr>
                                <td colspan="6" style="text-align:center; padding-right:10px; font-weight:bold;"><?= lang("sub_total"); ?></td>
                                <!--<td colspan="2" style="text-align:right; padding-right:10px; font-weight:bold;">&nbsp;</td>-->
                                <td colspan="3" style="text-align:right; padding-right:10px; font-weight:bold;">(<?= $default_currency->code; ?>) <?= $this->sma->formatMoney($inv->total + $inv->product_tax); ?></td>
                                <td colspan="3" style="text-align:center; padding-right:10px; font-weight:bold;"><?= lang("tot_pcs_per_m"); ?></td>
                                <td colspan="4" style="text-align:center; padding-right:10px; font-weight:bold;"><?= $qty; ?></td>
                                <td colspan="5" style="text-align:center; padding-right:10px; font-weight:bold;"><?= lang("pcs"); ?></td>
                            </tr>
                            <tr>

                                <?php
                                $col = 6;


                                if ($inv->order_discount != 0) {
                                    echo '  <td colspan="' . $col . '" style="text-align:center; padding-right:10px;;">' . lang("order_discount") . ' (' . $default_currency->code . ')</td>'
                                    . '<td colspan="3"  style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($inv->order_discount) . '</td>';
                                }

                                if ($inv->shipping != 0) {
                                    echo '<td colspan="' . $col . '" style="text-align:center; padding-right:10px;;">' . lang("shipping") . ' (' . $default_currency->code . ')</td>'
                                    . '<td colspan="3" style="text-align:center; padding-right:10px;">' . $this->sma->formatMoney($inv->shipping) . '</td>';
                                }
                                ?>
<!--<td colspan="2" style="text-align:right; padding-right:10px; font-weight:bold;">&nbsp;</td>-->
                                <td colspan="3"  rowspan="2" style="text-align:center; padding-right:10px; font-weight:bold;"><?= lang("box"); ?></td>
                                <td colspan="4" rowspan="2" style="text-align:center; padding-right:10px; font-weight:bold;"><?= $box; ?></td>
                                <td colspan="5"  rowspan="2" style="text-align:center; padding-right:10px; font-weight:bold;"><?= lang("bndl"); ?></td>
                            </tr>

                            <tr><?php
                                $tx_rate = $this->site->getTaxRateByID($inv->order_tax_id);

                                if ($Settings->tax2 && $inv->order_tax != 0) {
                                    echo '<td colspan="' . $col . '"   style="text-align:center; padding-right:10px;;">' . $tx_rate->name . ' (' . $default_currency->code . ')</td>'
                                    . '<td colspan="3"  style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($inv->order_tax) . '</td>';
                                }
                                ?>
                                <!--<td colspan="9" style="text-align:right; padding-right:10px; font-weight:bold;">&nbsp;</td>-->
                                <!--<td colspan="4" style="text-align:right; padding-right:10px; font-weight:bold;">&nbsp;</td>-->
                                <!--<td colspan="5" style="text-align:right; padding-right:10px; font-weight:bold;">&nbsp;</td>-->
                            </tr>
                            <tr>
                                <td colspan="6" style="text-align:center; padding-right:10px; font-weight:bold;"><?= lang("total"); ?></td>
                                <!--<td colspan="2" style="text-align:right; padding-right:10px; font-weight:bold;">&nbsp;</td>-->
                                <td colspan="3" style="text-align:right; padding-right:10px; font-weight:bold;">(<?= $default_currency->code; ?>) <?= $this->sma->formatMoney($inv->grand_total - $inv->paid); ?></td>
                                <td colspan="3" style="text-align:center; padding-right:10px; font-weight:bold;"><?= lang("bndl_taga"); ?></td>
                                <td colspan="4"  style="text-align:center; padding-right:10px; font-weight:bold;"><?= $bund_taga; ?></td>
                                <td colspan="5" style="text-align:center; padding-right:10px; font-weight:bold;"><?= lang("taga"); ?></td>
                            </tr>


<!--                            <tr>
    <td colspan="<?= $col; ?>" style="text-align:right; padding-right:10px; font-weight:bold;"><?= lang("box"); ?></td>
    <td style="text-align:right; padding-right:10px; font-weight:bold;"><?= $box; ?></td>
</tr>
<tr>
    <td colspan="<?= $col; ?>" style="text-align:right; padding-right:10px; font-weight:bold;"><?= lang("bndl_taga"); ?></td>
    <td style="text-align:right; padding-right:10px; font-weight:bold;"><?= $bund_taga; ?></td>
</tr>-->



                        </tfoot>
                    </table>

                </div>

                <div class="row">
                    <div class="col-xs-7">
                        <?php if ($inv->note || $inv->note != "") { ?>
                            <div class="well well-sm">
                                <p class="bold"><?= lang("note"); ?>:</p>

                                <div><?= $this->sma->decode_html($inv->note); ?></div>
                            </div>
                        <?php } ?>
                    </div>

                    <div class="col-xs-4 col-xs-offset-1">
                        <div class="well well-sm">
                            <p><?= lang("created_by"); ?>
                                : <?= $created_by->first_name . ' ' . $created_by->last_name; ?> </p>

                            <p><?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?></p>
                            <?php if ($inv->updated_by) { ?>
                                <p><?= lang("updated_by"); ?>
                                    : <?=
                                    $updated_by->first_name . ' ' . $updated_by->last_name;
                                    ;
                                    ?></p>
                                <p><?= lang("update_at"); ?>: <?= $this->sma->hrld($inv->updated_at); ?></p>
                            <?php } ?>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <?php if (!empty($payments)) { ?>
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th><?= lang('date') ?></th>
                                    <th><?= lang('payment_reference') ?></th>
                                    <th><?= lang('paid_by') ?></th>
                                    <th><?= lang('amount') ?></th>
                                    <th><?= lang('created_by') ?></th>
                                    <th><?= lang('type') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($payments as $payment) { ?>
                                    <tr>
                                        <td><?= $this->sma->hrld($payment->date) ?></td>
                                        <td><?= $payment->reference_no; ?></td>
                                        <td><?= $payment->paid_by; ?></td>
                                        <td><?= $payment->amount; ?></td>
                                        <td><?= $payment->first_name . ' ' . $payment->last_name; ?></td>
                                        <td><?= $payment->type; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        <?php } ?>

        <?php if (!$Supplier || !$Customer) { ?>
            <div class="buttons">
                <?php if ($inv->attachment) { ?>
                    <div class="btn-group">
                        <a href="<?= site_url('welcome/download/' . $inv->attachment) ?>" class="tip btn btn-primary" title="<?= lang('attachment') ?>">
                            <i class="fa fa-chain"></i>
                            <span class="hidden-sm hidden-xs"><?= lang('attachment') ?></span>
                        </a>
                    </div>
                <?php } ?>
                <div class="btn-group btn-group-justified">
                    <div class="btn-group"><a href="<?= site_url('purchases/payments/' . $inv->id) ?>"
                                              data-toggle="modal" data-target="#myModal" class="tip btn btn-primary tip"
                                              title="<?= lang('view_payments') ?>"><i class="fa fa-money"></i> <span
                                class="hidden-sm hidden-xs"><?= lang('view_payments') ?></span></a></div>
                    <div class="btn-group"><a href="<?= site_url('purchases/add_payment/' . $inv->id) ?>"
                                              class="tip btn btn-primary tip" title="<?= lang('add_payment') ?>"
                                              data-target="#myModal" data-toggle="modal"><i class="fa fa-money"></i>
                            <span class="hidden-sm hidden-xs"><?= lang('add_payment') ?></span></a></div>
                    <div class="btn-group"><a href="<?= site_url('purchases/email/' . $inv->id) ?>" data-toggle="modal"
                                              data-target="#myModal" class="tip btn btn-primary tip"
                                              title="<?= lang('email') ?>"><i class="fa fa-envelope-o"></i> <span
                                class="hidden-sm hidden-xs"><?= lang('email') ?></span></a></div>
                    <div class="btn-group"><a href="<?= site_url('purchases/pdf/' . $inv->id) ?>"
                                              class="tip btn btn-primary" title="<?= lang('download_pdf') ?>"><i
                                class="fa fa-download"></i> <span class="hidden-sm hidden-xs"><?= lang('pdf') ?></span></a>
                    </div>
                    <div class="btn-group"><a href="<?= site_url('purchases/edit/' . $inv->id) ?>"
                                              class="tip btn btn-warning tip" title="<?= lang('edit') ?>"><i
                                class="fa fa-edit"></i> <span class="hidden-sm hidden-xs"><?= lang('edit') ?></span></a>
                    </div>
                    <div class="btn-group"><a href="#" class="tip btn btn-danger bpo"
                                              title="<b><?= $this->lang->line("delete_purchase") ?></b>"
                                              data-content="<div style='width:150px;'><p><?= lang('r_u_sure') ?></p><a class='btn btn-danger' href='<?= site_url('purchases/delete/' . $inv->id) ?>'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button></div>"
                                              data-html="true" data-placement="top"><i class="fa fa-trash-o"></i> <span
                                class="hidden-sm hidden-xs"><?= lang('delete') ?></span></a></div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
