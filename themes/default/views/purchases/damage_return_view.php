<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-file"></i><?= lang("damage_return") . ' ' . lang('number') . '. ' . $inv->id; ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="print-only col-xs-12">
                    <img src="<?= base_url() . 'assets/uploads/logos/' . $Settings->logo; ?>"
                         alt="<?= $Settings->site_name; ?>">
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4">
                            <span style="font-weight:bold; font-size: 16px;">
                                <?= lang("damage_return") . ' ' . lang('number') ?>: </span>
                            <span>  <?= $inv->reference_no; ?></span>
                        </div>
                        <div class="col-md-4 text-center">
                            <span class="text-uppercase" style="font-weight:bold; font-size: 16px;"><?= $Settings->site_name; ?></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4 text-center">
                            <span><?= lang('kavathe_mahakal'); ?></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4 text-center">
                            <span style="font-weight:bold; font-size: 16px;"><?= lang('sup_damage_list') ?></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-4 text-center">
                            <span style="font-weight:bold; font-size: 16px;"><?= lang("supplier"); ?>: </span><span>  
                                <?= $inv->supplier; ?> [ <?= $supplier->code ?> ]</span>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <div class="col-xs-8">
                   <!--<div class="col-xs-2"><i class="fa fa-3x fa-building padding010 text-muted"></i></div>-->
                    <div class="col-xs-10"> 
<!--                            <h2 class=""><?= lang("sup_bill_no"); ?>: <?= $inv->reference_no; ?></h2>
                        <span style="font-weight:bold;"><?= lang("lr_no"); ?>:</span><span> <?= $inv->lr_no; ?></span>
                        <br>-->
                          <!--<span style="font-weight:bold; font-size: 16px;"><?= lang("supplier"); ?>: </span><span>  <?= $inv->supplier; ?></span>-->

                        <br>
                        <!--<span style="font-weight:bold; font-size: 16px;"><?= lang("ag_order_no"); ?>: </span><span>  <?= $inv->ag_order_no; ?></span>-->   
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!--<div class="col-xs-4"></div>-->
                <div class="col-xs-4">
                    <div class="col-xs-2"></div>
                    <div class="col-xs-10"> 
<!--                            <h2 class=""><?= lang("user"); ?>: <?= $inv->reference_no; ?></h2>
                        <span style="font-weight:bold;"></span>
                        <br>
                          <span style="font-weight:bold; font-size: 16px;"><?= lang("pur_no"); ?>: </span><span>  <?= $inv->supplier; ?></span>
                          <br>-->
                        <!--<span style="font-weight:bold; font-size: 16px;"><?= lang("return_date"); ?>: </span><span><?= $this->sma->hrld($inv->date); ?></span>-->   
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>


    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="print-only col-xs-12">
                    <img src="<?= base_url() . 'assets/uploads/logos/' . $Settings->logo; ?>"
                         alt="<?= $Settings->site_name; ?>">
                </div>
                <!--                <div class="well well-sm">
                                    <div class="col-xs-4 border-right">
                
                                        <div class="col-xs-2"><i class="fa fa-3x fa-building padding010 text-muted"></i></div>
                                        <div class="col-xs-10">
                                            <h2 class=""><?= $supplier->company ? $supplier->company : $supplier->name; ?></h2>
                <?= $supplier->company ? "" : "Attn: " . $supplier->name ?>
                
                <?php
                echo $supplier->address . "<br />" . $supplier->city . " " . $supplier->postal_code . " " . $supplier->state . "<br />" . $supplier->country;

                echo "<p>";

                if ($supplier->cf1 != "-" && $supplier->cf1 != "") {
                    echo "<br>" . lang("scf1") . ": " . $supplier->cf1;
                }
                if ($supplier->cf2 != "-" && $supplier->cf2 != "") {
                    echo "<br>" . lang("scf2") . ": " . $supplier->cf2;
                }
                if ($supplier->cf3 != "-" && $supplier->cf3 != "") {
                    echo "<br>" . lang("scf3") . ": " . $supplier->cf3;
                }
                if ($supplier->cf4 != "-" && $supplier->cf4 != "") {
                    echo "<br>" . lang("scf4") . ": " . $supplier->cf4;
                }
                if ($supplier->cf5 != "-" && $supplier->cf5 != "") {
                    echo "<br>" . lang("scf5") . ": " . $supplier->cf5;
                }
                if ($supplier->cf6 != "-" && $supplier->cf6 != "") {
                    echo "<br>" . lang("scf6") . ": " . $supplier->cf6;
                }

                echo "</p>";
                echo lang("tel") . ": " . $supplier->phone . "<br />" . lang("email") . ": " . $supplier->email;
                ?>
                                        </div>
                                        <div class="clearfix"></div>
                
                                    </div>
                                    <div class="col-xs-4">
                
                                        <div class="col-xs-2"><i class="fa fa-3x fa-truck padding010 text-muted"></i></div>
                                        <div class="col-xs-10">
                                            <h2 class=""><?= $Settings->site_name; ?></h2>
                <?= $warehouse->name ?>
                
                <?php
                echo $warehouse->address . "<br>";
                echo ($warehouse->phone ? lang("tel") . ": " . $warehouse->phone . "<br>" : '') . ($warehouse->email ? lang("email") . ": " . $warehouse->email : '');
                ?>
                                        </div>
                                        <div class="clearfix"></div>
                
                
                                    </div>
                                    <div class="col-xs-4 border-left">
                
                                        <div class="col-xs-2"><i class="fa fa-3x fa-file-text-o padding010 text-muted"></i></div>
                                        <div class="col-xs-10">
                                            <h2 class=""><?= lang("ref"); ?>: <?= $inv->reference_no; ?></h2>
                
                                            <p style="font-weight:bold;"><?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?></p>
                
                                            <p style="font-weight:bold;"><?= lang("status"); ?>: <?= $inv->status; ?></p>
                                        </div>
                                        <div class="col-xs-12">
                <?php $br = $this->sma->save_barcode($inv->reference_no, 'code39', 35, false); ?>
                                            <img
                                                src="<?= base_url() ?>assets/uploads/barcode<?= $this->session->userdata('user_id') ?>.png"
                                                alt="<?= $inv->reference_no ?>"/>
                <?php $this->sma->qrcode('link', urlencode(site_url('sales/view/' . $inv->id)), 1); ?>
                                            <img
                                                src="<?= base_url() ?>assets/uploads/qrcode<?= $this->session->userdata('user_id') ?>.png"
                                                alt="<?= $inv->reference_no ?>"/>
                                        </div>
                                        <div class="clearfix"></div>
                
                
                                    </div>
                                    <div class="clearfix"></div>
                                </div>-->

                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped print-table order-table">
                        <thead>
                            <tr>
                                <th><?= lang("no"); ?></th>
                                <th><?= lang("barcodes"); ?></th>
                                <th><?= lang("Dept.") ?>
                                <th><?= lang("Product") ?>
                                <th><?= lang("Type") ?>
                                <th><?= lang("Brands") ?>
                                <th><?= lang("Design") ?>
                                <th><?= lang("Style") ?>
                                <th><?= lang("Fitting") ?>
                                <th><?= lang("Pattern") ?>
                                <th><?= lang("Fabric") ?>
                                <th><?= lang("Pur. Rate") ?>
                                <th><?= lang("MRP") ?>
                                <!--<th><?= lang("Color") ?>-->
                                <th><?= lang("Size") ?>
                                <!--<th><?= lang("quantity"); ?></th>-->

 <!--<th style="padding-right:20px;"><?= lang("Rate"); ?></th>-->

                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $r = 1;
                            $box = 0;
                            $bund_taga = 0;
                            $pcs_mtr = 0;
                            $total_qty = 0;
                            $total_amount = 0;
                            foreach ($rows as $row) {
                                if ($row->sunit == 5) {
                                    $box = $row->squantity + $box;
                                }
                                if ($row->sunit == 7 || $row->sunit == 8) {
                                    $bund_taga = $row->squantity + $bund_taga;
                                }
                                if ($row->sunit == 2 || $row->sunit == 3) {
                                    $pcs_mtr = $row->squantity + $pcs_mtr;
                                }

                                $row->prod = $this->site->getProductByIDOrder($row->product_id , $row->product_code);
                                $total_qty = $row->quantity + $total_qty;
                                $total_amount = $row->subtotal + $total_amount;
                                ?>


                                <tr>
                                    <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
    <!--                                    <td style="vertical-align:middle;"><?= $row->product_name . " (" . $row->product_code . ")" . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                    <?= $row->details ? '<br>' . $row->details : ''; ?></td>-->
                                    <td><?= $row->prod->code ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->department, 'department')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->product_items, 'product_items')->name ?></td>

                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->type_id, 'type')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->brands, 'brands')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->design, 'design')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->style, 'style')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->fitting, 'fitting')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->pattern, 'pattern')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->fabric, 'fabric')->name ?></td>
                                    <td style="text-align:right; width:120px; padding-right:10px;"><?= $this->sma->formatMoney($row->net_unit_cost); ?></td>
                                    <td ><?= $this->sma->formatMoney($row->prod->price) ?></td>
                                    <!--<td><?= $this->site->getParaById('name', 'id', $row->prod->color, 'color')->name . "(" . $row->prod->colorqty . ")" ?></td>-->
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->size, 'size')->name . "(" . $row->prod->sizeangle . ")" ?></td>
                                    <!--<td style="width: 120px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity); ?></td>-->

                     <!--<td style="text-align:right; width:120px; padding-right:10px;"><?= $this->sma->formatMoney($row->subtotal); ?></td>-->

                                </tr>
                                <?php
                                $r++;
                            }
                            ?>
                            <tr style="font-weight:bold; font-size: 16px;">
                                <td colspan="2" style="text-align:center; width:40px; vertical-align:middle;">TOTAL</td>
                                <td colspan="2" style="text-align:center; width:40px; vertical-align:middle;">Quantity</td>
                                <td colspan="3" style="text-align:center; width:40px; vertical-align:middle;"><?= $total_qty . ' ' . lang('pices') ?></td>
                                <td colspan="4" style="text-align:center; width:40px; vertical-align:middle;">Amount</td>
                                <td colspan="4" style="text-align:center; width:40px; vertical-align:middle;">Rs <?= $total_amount ?></td>
                            </tr>
                        </tbody>

                    </table>

                </div>

                <div class="row">
                    <div class="col-xs-7">
                        <?php if ($inv->note || $inv->note != "") { ?>
                            <div class="well well-sm">
                                <p class="bold"><?= lang("note"); ?>:</p>

                                <div><?= $this->sma->decode_html($inv->note); ?></div>
                            </div>
                        <?php } ?>
                    </div>

                    <div class="col-xs-4 col-xs-offset-1">
                        <div class="well well-sm">
                            <p><?= lang("created_by"); ?>
                                : <?= $created_by->first_name . ' ' . $created_by->last_name; ?> </p>

                            <p><?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?></p>
                            <?php if ($inv->updated_by) { ?>
                                <p><?= lang("updated_by"); ?>
                                    : <?=
                                    $updated_by->first_name . ' ' . $updated_by->last_name;
                                    ;
                                    ?></p>
                                <p><?= lang("update_at"); ?>: <?= $this->sma->hrld($inv->updated_at); ?></p>
                            <?php } ?>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        <?php // if (!empty($payments)) {  ?>
        <!--            <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-condensed">
                                    <thead>
                                        <tr>
                                            <th><?= lang('date') ?></th>
                                            <th><?= lang('payment_reference') ?></th>
                                            <th><?= lang('paid_by') ?></th>
                                            <th><?= lang('amount') ?></th>
                                            <th><?= lang('created_by') ?></th>
                                            <th><?= lang('type') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
        <?php foreach ($payments as $payment) { ?>
                                                                <tr>
                                                                    <td><?= $this->sma->hrld($payment->date) ?></td>
                                                                    <td><?= $payment->reference_no; ?></td>
                                                                    <td><?= $payment->paid_by; ?></td>
                                                                    <td><?= $payment->amount; ?></td>
                                                                    <td><?= $payment->first_name . ' ' . $payment->last_name; ?></td>
                                                                    <td><?= $payment->type; ?></td>
                                                                </tr>
        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>-->
        <?php //  }  ?>

        <?php if (!$Supplier || !$Customer) { ?>
            <div class="buttons">
                <?php if ($inv->attachment) { ?>
                    <div class="btn-group">
                        <a href="<?= site_url('welcome/download/' . $inv->attachment) ?>" class="tip btn btn-primary" title="<?= lang('attachment') ?>">
                            <i class="fa fa-chain"></i>
                            <span class="hidden-sm hidden-xs"><?= lang('attachment') ?></span>
                        </a>
                    </div>
                <?php } ?>
                <!--                <div class="btn-group btn-group-justified">
                                    <div class="btn-group"><a href="<?= site_url('purchases/payments/' . $inv->id) ?>"
                                                              data-toggle="modal" data-target="#myModal" class="tip btn btn-primary tip"
                                                              title="<?= lang('view_payments') ?>"><i class="fa fa-money"></i> <span
                                                class="hidden-sm hidden-xs"><?= lang('view_payments') ?></span></a></div>
                                    <div class="btn-group"><a href="<?= site_url('purchases/add_payment/' . $inv->id) ?>"
                                                              class="tip btn btn-primary tip" title="<?= lang('add_payment') ?>"
                                                              data-target="#myModal" data-toggle="modal"><i class="fa fa-money"></i>
                                            <span class="hidden-sm hidden-xs"><?= lang('add_payment') ?></span></a></div>
                                    <div class="btn-group"><a href="<?= site_url('purchases/email/' . $inv->id) ?>" data-toggle="modal"
                                                              data-target="#myModal" class="tip btn btn-primary tip"
                                                              title="<?= lang('email') ?>"><i class="fa fa-envelope-o"></i> <span
                                                class="hidden-sm hidden-xs"><?= lang('email') ?></span></a></div>
                                    <div class="btn-group"><a href="<?= site_url('purchases/pdf/' . $inv->id) ?>"
                                                              class="tip btn btn-primary" title="<?= lang('download_pdf') ?>"><i
                                                class="fa fa-download"></i> <span class="hidden-sm hidden-xs"><?= lang('pdf') ?></span></a>
                                    </div>
                                    <div class="btn-group"><a href="<?= site_url('purchases/edit/' . $inv->id) ?>"
                                                              class="tip btn btn-warning tip" title="<?= lang('edit') ?>"><i
                                                class="fa fa-edit"></i> <span class="hidden-sm hidden-xs"><?= lang('edit') ?></span></a>
                                    </div>
                                    <div class="btn-group"><a href="#" class="tip btn btn-danger bpo"
                                                              title="<b><?= $this->lang->line("delete_purchase") ?></b>"
                                                              data-content="<div style='width:150px;'><p><?= lang('r_u_sure') ?></p><a class='btn btn-danger' href='<?= site_url('purchases/delete/' . $inv->id) ?>'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button></div>"
                                                              data-html="true" data-placement="top"><i class="fa fa-trash-o"></i> <span
                                                class="hidden-sm hidden-xs"><?= lang('delete') ?></span></a></div>
                                </div>-->
            </div>
        <?php } ?>
    </div>
</div>
