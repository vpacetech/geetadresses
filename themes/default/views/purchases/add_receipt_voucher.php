<style>
    .close{
        font-size: 19px !important;
    }
    .heading{
        margin-top:14px !important;
        border-top: 1px solid black;
    }
    .heading1{
        padding-top: 7px;
    }
    .modal-header{
        border-bottom: none !important;
    }
</style>
<?php echo $modal_js ?>
<div class="modal-dialog modal-lg ">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
        </div>
        <?php
        $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'addFormx');
        echo form_open_multipart("purchases/SaveReceiptVoucher", $attrib);
        ?>
        <div class="modal-body">
            <div >
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <h2 style="margin:0;padding-top: 10px !important;">
                        <?= lang('add_receipt_voucher_tag') ?>
                    </h2>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
                    <b style='font-size: 20px;'><?= $Settings->site_name ?></b>
                    <!--<h4>Kavathe - Mahankal</h4>-->
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 "></div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 heading" >
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 text-right">
                        <button type="button" onclick="enableEdit()"><?= lang('define_my_own_Code') ?></button>
                    </div>
                    <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12">  <?= lang('paymentno') ?> </label>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 heading1">
                        <input type="text" name="payment_no" value="<?php echo 'GD-RC'.$latest_pay_no ?>" class="form-control remove_readonly" readonly>
                    </div>
                </div>
                
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                    <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12"><?= lang('date') ?> </label>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 heading1">
                        <?php echo form_input('date', date('d/m/Y') . '(' . date('l') . ')', 'class="form-control tip" id="sender_address" data-bv-notempty="true"  readonly'); ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group">
                    <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12">  <?= lang('user') ?>  </label>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 heading1">
                          <?php
                            $bl = "";
                            $bl[''] = "Select User";
                            foreach ($users as $row) {
//                                $bl[$row->id] = $row->username;
                                $bl[$row->id] = $row->first_name .' '. $row->last_name;
                            }
                            echo form_dropdown('user_name', $bl, set_value('account', ''), 'data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                            ?>
                       <!--<input type="text" name="user_name" class="form-control" required="">-->
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                    <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12"> <?= lang('time') ?> </label>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 heading1">
                        <?php echo form_input('time', date('h:i a'), 'class="form-control tip" id="sender_address" data-bv-notempty="true" readonly'); ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 heading" style="padding:0;">
                <h4 class="text-center" style="font-size: 20px; padding-bottom:5px;"><span style="text-decoration: underline;">Sender </span>&nbsp; <span style="text-decoration: underline;">Details </span></h4>
                <!--<h4 class="text-center" style="font-size: 20px; text-decoration: underline;padding-bottom:5px;"><?= lang('sender_details') ?></h4>-->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="received_for">1. <?= lang('received_for') ?> :* </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php
                            $bl = "";
                            $bl[''] = $this->lang->line('select') . " " .$this->lang->line('received_for');
                            foreach ($customer as $row) {
                                $bl[$row->id] = $row->name;
                            }
                            echo form_dropdown('received_for', $bl, set_value('received_for', ''), 'data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" id="received_for" class="form-control input-tip select" style="width:100%;"');
                            ?>
                             <?php
//                                    echo form_input('received_for', set_value('received_for', ''), 'class="form-control" id="received_for" required="required" '
//                                            . 'data-bv-notempty="true"
//                               data-bv-notempty-message="Received for cannot be empty"');
//                               data-bv-stringlength="true"
//                               data-bv-regexp="true"
//                               data-bv-regexp-regexp="^[a-zA-Z]+$"
//                               data-bv-regexp-message="Received for can only consist of alphabets"
                                    ?>
<!--                            <select class="form-control col-lg-5 col-md-5 col-sm-5 col-xs-12" name="received_for" id="sel1">
                                <option value="Pigmi (Deposits)">Pigmi (Deposits)</option>
                            </select>                             -->
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="party_account">2. <?= lang('party_account','party_account') ?>* </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                             <?php
                                    echo form_input('party_account', set_value('party_account', ''), 'class="form-control" id="party_account" required="required" '
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="Party Account cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[a-zA-Z ]*$"
                               data-bv-regexp-message="Party Account can only consist of alphabets only"');
                                    ?>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="againstno">3. <?= lang('againstno') ?>* </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                          <?php
                                    echo form_input('against_ref_no', set_value('against_ref_no', ''), 'class="form-control" id="againstno" required="required" '
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="Bill No cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="Bill no can only consist of digits"');
                                    ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="joining_date">4. <?= lang('bank_acc_no') ?></label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                           <?php
                                    echo form_input('bank_acc_no', set_value('bank_acc_no', ''), 'class="form-control" '
                                            . 'data-bv-stringlength="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="Bank Account no. can only consist of digits"');
                                    ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="dated">5. <?= lang('dated') ?>* </label>

                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php echo form_input('datedd', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="podate" required="required"'); ?>                        
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="amount">6. <?= lang('amount') ?>* </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">

                            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12" style="padding: 0">
                                  <?php
                                    echo form_input('amount', set_value('amount', ''), 'class="form-control" required="required" '
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="Amount cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="Amount can only consist of digits"');
                                    ?>
                                
                                <!--<input type="text" name="amount" class="form-control" required=""  >-->
                            </div>
                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="padding-right: 0">
                                <input type="text"  class="form-control" placeholder="Cr." disabled style="border-color:rgba(0,0,0,.075)!important">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 heading" style="padding:0;">
                <h4 class="text-center" style="font-size: 20px; padding-bottom:5px;"><span style="text-decoration: underline;">Receiving </span>&nbsp; <span style="text-decoration: underline;">Details </span></h4>
                <!--<h4 class="text-center" style="font-size: 20px; text-decoration: underline;padding-bottom:5px;"><?= lang('receiving_detail')?></h4>-->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="payment_mode">7. <?= lang('payment_mode') ?> </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <select class="form-control col-lg-5 col-md-5 col-sm-5 col-xs-12" id="sel1" name="payment_mode">
                                <option value="cash"><?= lang('cash_payment')?></option>
                                <option value="cheque"><?= lang('cheque')?></option>
                                <option value="rtgs"><?= lang('rtgs')?></option>
                                <option value="neft"><?= lang('neft')?></option>
                                <option value="other"><?= lang('other')?></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="from_acc">8. <?= lang('to_acc') ?>* </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <?php echo form_input('to_acc', set_value('to_acc', ''), 'class="form-control" required="required"');?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="pay_type_no">9. <?= lang('paym_no') ?></label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                              <?php
                                    echo form_input('pay_type_no', set_value('pay_type_no', ''), 'class="form-control" '
                                            . 'data-bv-notempty-message="Payment type no cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="Payment type no can only consist of digits"');
                                    ?>
                            <!--<input type="text" name="pay_type_no" class="form-control"  required=""  >-->
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="bank_acc_number">10. <?= lang('account_no') ?></label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                             <?php
                                    echo form_input('acc_number', set_value('acc_number', ''), 'class="form-control"'
                                            . 'data-bv-stringlength="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="Account no. can only consist of digits"');
                                    ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="joining_date">11. <?= lang('dated') ?>* </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                         <?php echo form_input('dated', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="podates" required="required"'); ?>                        
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-5 col-sm-5 col-md-5 col-xs-12" for="joining_date">12. <?= lang('balance') ?>* </label>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12" style="padding: 0">
                                <?php
                                    echo form_input('balance', set_value('balance', ''), 'class="form-control" required="required" '
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="Balance no cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="Balance can only consist of digits"');
                                    ?>
                                <!--<input type="text" name="balance" class="form-control" required=""  >-->
                            </div>
                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="padding-right: 0">
                                <input type="text" class="form-control" value="Dr." placeholder="Dr." readonly style="border-color:rgba(0,0,0,.075)!important">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer" style="clear:both;text-align: left;">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                   <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                          <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                         <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12 control-label ">
                               <b>Narration </b>
                            </div>
                         <div class="col-lg-9 col-sm-9 col-md-9 col-xs-12">
                             <input type="text" name="narration" class="form-control" >
                                 <!--<text type="text" name="narration">-->
                            </div>
                          </div>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-5"><?php echo form_submit('add_transfer', $this->lang->line("submit"), 'id="add_transfer" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>

                </div>
            </div>
        </div>




        <?php echo form_close(); ?>

        <!---->
    </div><!--end body div-->

    <!--end foter div-->

</div>


<script type="text/javascript">
    $(document).ready(function (e) {
        $('#addFormx').bootstrapValidator({
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            }, excluded: [':disabled']
        });
        $('select.select').select2({minimumResultsForSearch: 6});
        fields = $('.modal-content').find('.form-control');
        $.each(fields, function () {
            var id = $(this).attr('id');
            var iname = $(this).attr('name');
            var iid = '#' + id;
            if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
//                $("label[for='" + id + "']").append(' *');
                $(document).on('change', iid, function () {
                    $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
                });
            }
        });
    });

//    mrp = onlydigits('mrp');
//    mrp();
//    mrp_from = onlydigits('mrp_from');
//    mrp_from();
//    mrp_to = onlydigits('mrp_to');
//    mrp_to();
//    sales_incentive = onlydigits('sales_incentive');
//    sales_incentive();
//    min_qty_lvl = onlydigits('min_qty_lvl');
//    min_qty_lvl();
//    min_qty_lvl_2 = onlydigits('min_qty_lvl_2');
//    min_qty_lvl_2();
//    reorder_qty = onlydigits('reorder_qty');
//    reorder_qty();
//    reorder_qty_2 = onlydigits('reorder_qty_2');
//    reorder_qty_2();
    $(document).ready(function () {
//        $("#addFormx").unbind().submit(function (e) {
//            e.preventDefault();
//            var form = $("#addFormx");
//            var $data = $(this).serialize();
////            $data += "&name=<?= $this->security->get_csrf_token_name() ?>&value=<?= $this->security->get_csrf_hash() ?>";
//            $.ajax({
//                url: "<?= site_url('system_settings/SaveMin_qty_lvl/?v=1') ?>",
//                type: "get", async: false,
//                data: $data,
//                dataType: 'json',
//                success: function (data, textStatus, jqXHR) {
//                    if (data.s === "true") {
//                        bootbox.alert('Paramenter Added Successfully!');
//                        oTable.fnDraw();
//                        $("#myModal").modal('hide');
//                    } else {
//                        bootbox.alert('Paramenter Adding Failed!');
//                        $("#myModal").modal('hide');
//                    }
//                }
//            });
//            return false;
//        });
        var obj = ['companies', 'department', 'section', 'product_items', 'type', 'brands', 'design', 'style', 'pattern', 'fitting', 'fabric', 'color', 'size', 'per'];
        var objarr = [];
        var i = 0;
        $.each(obj, function (k, v) {
            objarr[i] = myselect2(v);
            objarr[i]();
            i++;
        });
        function myselect2(id) {
            if (id == "per") {
                var pp = $("." + id);
            } else {
                var pp = $("#" + id);
            }
            function get() {
                pp.select2({
                    minimumInputLength: 1,
                    data: [],
                    initSelection: function (element, callback) {
                        $.ajax({
                            type: "get", async: false,
                            url: site_url + "products/getIdAttribute",
                            data: {
                                term: pp.val(),
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                            },
                            dataType: "json",
                            success: function (data) {
                                callback(data[0]);
                            }
                        });
                    },
                    ajax: {
                        url: site_url + "products/getIdAttributes",
                        dataType: 'json',
                        quietMillis: 15,
                        data: function (term, page) {
                            return {
                                term: term,
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                                limit: 10
                            };
                        },
                        results: function (data, page) {
                            if (data.results != null) {
                                return {results: data.results};
                            } else {
                                return {results: [{id: '', text: 'No Match Found'}]};
                            }
                        }
                    }
                });
            }
            return get;
        }
        function getidsx() {
//            alsert($("#selsize").data('id'));
            var $ids = $("#selsize").data('id').split(",");
            var $idvals = "";

            $.each($ids, function (k, v) {
                $idvals += ($("#" + v).val() ? $("#" + v).val() : "") + "-";
            });
            $idvals = $idvals.trim();
            return $idvals;
        }
        $("#selsize").select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site_url + "products/getIdAttribute",
                    data: {
                        term: $(element).val(),
                        tab: $(element).data('tab'),
                        id: getidsx,
                        key: $(element).data('key'),
                    },
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site_url + "products/getIdAttributes",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        tab: $("#selsize").data('tab'),
                        id: getidsx,
                        key: $("#selsize").data('key'),
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'No Match Found'}]};
                    }
                }
            }
        });
    });
</script>
