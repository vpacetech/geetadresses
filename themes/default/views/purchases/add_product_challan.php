<?= $modal_js ?>
<?php
$lr_no[''] = "";
foreach ($lr_no as $k => $v) {
    if (!empty($v)) {
        foreach ($v as $d) {
            $lr_no[$d->id] = $d->lr_no;
        }
    }
}
?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <center><h4 class="modal-title" id="myModalLabel">Add Parcel Received Voucher</h4></center>
        </div>
        <?php
        $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'addFormx');
        echo form_open_multipart("purchases/SavePurchaseReceivedVoucher", $attrib);
        ?>
        <!--<form data-toggle="validator" role="form" id="addFormx">-->
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 billing">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="veh_no"><?= lang('select_store') ?></label>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <div class="controls"> 
                            <?php
                            $st = [];
                            $st[] = "Select Store";
                            foreach ($store as $row) {
                                // $st[$row->id] = $row->company;
                                $st[$row->id] = $row->name;
                            }
                            echo form_dropdown('store_id', $st, set_value('from_acc', ''), 'data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" id="get_supplier" class="form-control input-tip select" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="veh_no">Sender</label>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                    <div class="form-group">
                        <div class="controls"> 
                            <?php
//                            $bl[""] = "";
//                            foreach ($supplier as $biller) {
//                                $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->company;
//                            }
//                            echo form_dropdown('sender', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $Settings->default_biller), 'id="qubiller" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("sender") . '" required="required" class="form-control input-tip select" id="sender" style="width:100%;"');

                            echo form_dropdown('sender', "", set_value('sender', ''), 'data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("sender") . '" required="required" class="form-control input-tip select " id="sender"  style="width:100%;height:auto;"');
//                            echo form_dropdown('sender', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $Settings->default_biller), 'id="qubiller" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("sender") . '" required="required" class="form-control input-tip select" id="sender" style="width:100%;"');
                            ?>
                        </div>
                    </div>




                    <!--                    <div class="form-group">
                                            <div class="controls"> 
                    <?php echo form_input('sender', '', 'class="form-control myselect" id="supplier"  data-tab="sender"  placeholder="select sender" data-bv-notempty="true"'); ?>
                                            </div>
                                        </div>-->
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="veh_no">Sender Address</label>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <div class="controls"> 
                            <?php echo form_input('sender_address', '', 'class="form-control tip" id="sender_address" data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="transport">Transport</label>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <div class="controls"> 
                            <?php
                            $trans[""] = "";
                            foreach ($transport as $tr) {
                                $trans[$tr->id] = $tr->code . ' - ' . $tr->transport_name;
                            }
                            echo form_dropdown('transport', $trans, (isset($_POST['transport_name']) ? $_POST['transport_name'] : ''), 'id="transport" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("transport") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                   
                    <div class="form-group">
                        <?= lang("lr_no", "lr_no") ?>
                        <div class="controls"> 
                            <?php
                            echo form_input('lr_no', '', 'class="form-control tip" id="lr_no"'
                                    . 'data-bv-notempty="true"
                               data-bv-notempty-message="The Lr no is required and cannot be empty"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The Lr no can only consist of digits"');
                            ?>
                        </div>
                    </div>
					
					<div class="form-group">
                            <?= lang("ag_order_no", "ag_order_no") ?>
                                <?php
                                $ag_ord[""] = "";
                                if (!empty($ag_ord_no)) {
                                    foreach ($ag_ord_no as $ag_ord_no) {
                                        $ag_ord[$ag_ord_no->reference_no] = $ag_ord_no->reference_no;
                                    }
                                }
                                echo form_dropdown('ag_order_no[]', '', (isset($_POST['reference_no']) ? $_POST['reference_no'] : ''), 'id="reference_no" placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("reference_no") . '" multiple required="required" class="form-control input-tip select get_against_order_sup" style="width:100%;"');
                                ?>
                            </div>
					
                    <div class="form-group">
                        <label class="control-label" for="veh_no">Veh.no</label>
                        <div class="controls"> 
                            <?php echo form_input('veh_no', '', 'class="form-control tip"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="sldate">Bk. Date</label>
                        <div class="form-group">
                            <?php echo form_input('bk_date', (isset($_POST['bk_date']) ? $_POST['bk_date'] : ""), 'class="form-control input-tip datetime" id="sldate" required="required"'); ?>
                        </div>
                        <!--                        <div class="controls"> 
                        <?php //echo form_input('bk_date', '', 'class="form-control tip" id="lr_no" data-bv-notempty="true"'); ?>
                                                </div>-->
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parcel_type">Parcel Type</label>
                        <div class="controls">
                            <?php echo form_dropdown('parcel_type', array('medium_cartoon' => 'Medium Cartoon', 'small_cartoon' => 'Small Cartoon', 'large_cartoon' => 'Large Cartoon'), '', 'class="form-control myselect parcel_type" id="parcel_type" data-tab="per" style="" '); ?>                             
                        </div>
                    </div>
                </div>
                <!--end col 6 --->
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    
                    <div class="form-group">
                        <label class="control-label" for="no_of_bales">No of Bales</label>
                        <div class="controls"> 
                            <?php
                            echo form_input('no_of_bales', '', 'class="form-control tip" id="no_of_bales"'
                                    . 'data-bv-notempty="true"
                               data-bv-notempty-message="The No Of Bales is required and cannot be empty"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The No Of Bales can only consist of digits"');
                            ?>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="control-label" for="purchase_bill_no">Supplier’s Purchase bill number :</label>
                        <div class="controls"> 
                            <?php echo form_input('purchase_bill_no', '', 'class="form-control tip"'); ?>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="control-label" for="hamal">Hamal</label>
                        <div class="controls"> 
                            <?php echo form_input('hamal', '', 'class="form-control tip"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="receiver">Receiver</label>
                        <div class="controls"> 
                            <div class="form-group">
                                <?php echo form_dropdown('receiver', "", set_value('receiver', ''), 'data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" class="form-control input-tip select" id="booked_by"  style="width:100%;height:auto;"'); ?>
                                <?php
//                                $us[""] = "";
//                                foreach ($user as $usr) {
//                                    $us[$usr->id] = $usr->first_name != '-' ? $usr->first_name : $usr->first_name;
//                                }
//                                echo form_dropdown('receiver', $us, (isset($_POST['first_name']) ? $_POST['first_name'] : ''), 'id="user_id" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("user") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="delievery">Delivery</label>
                        <div class="controls">
                            <?php echo form_dropdown('delivery', array('Door' => 'Door', 'Collect_By_Sales' => 'Collect By Sales'), '', 'class="form-control myselect incentive_type" id="delievery" data-tab="per" style="" '); ?>                             
                        </div>
                    </div>
                </div><!--end col 6 --->

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="goods_desc">Goods Description</label>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <div class="controls"> 
                            <?php echo form_input('goods_desc', '', 'class="form-control tip" id="goods_desc" data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                </div>

				
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label" for="fright">Fright : </label>
                    </div>
					
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="controls"> 
                            <?php //echo form_input('fright', '', 'class="form-control tip"'); ?>
                            <?php
                            echo form_input('fright', '0', 'class="form-control tip" id="fright"'
                                    . 'data-bv-notempty="true"
                               data-bv-notempty-message="The fright is required and cannot be empty"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The fright chargers can only consist of digits"');
                            ?>
                        </div>
                </div>
            

                <div class="clearfir"></div>
				
				

            </div>
            <!---->
        </div><!--end body div-->
        <div class="modal-footer" style="clear:both;">
            <input name="add_user" value="Save" class="btn btn-primary" type="submit"> 
        </div><!--end foter div-->
        </form>

    </div>
</div>

<script type="text/javascript">
    $(document).ready(function (e) {
        $('#addFormx').bootstrapValidator({
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            }, excluded: [':disabled']
        });
        $('select.select').select2({minimumResultsForSearch: 6});
        fields = $('.modal-content').find('.form-control');
        $.each(fields, function () {
            var id = $(this).attr('id');
            var iname = $(this).attr('name');
            var iid = '#' + id;
            if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
//                $("label[for='" + id + "']").append(' *');
                $(document).on('change', iid, function () {
                    $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
                });
            }
        });
    });

    mrp = onlydigits('mrp');
    mrp();
    mrp_from = onlydigits('mrp_from');
    mrp_from();
    mrp_to = onlydigits('mrp_to');
    mrp_to();
    sales_incentive = onlydigits('sales_incentive');
    sales_incentive();
    min_qty_lvl = onlydigits('min_qty_lvl');
    min_qty_lvl();
    min_qty_lvl_2 = onlydigits('min_qty_lvl_2');
    min_qty_lvl_2();
    reorder_qty = onlydigits('reorder_qty');
    reorder_qty();
    reorder_qty_2 = onlydigits('reorder_qty_2');
    reorder_qty_2();


    mrp = onlydigits('mrp');
    mrp();
    lr_no = onlydigits('lr_no');
    lr_no();

    fright = onlydigits('fright');
    fright();
    
    no_of_bales = onlydigits('no_of_bales');
    no_of_bales();
    hamal = onlyAlphabet('hamal');
    hamal();
    sldate = onlyKeyPress('sldate');
    sldate();








    $(document).ready(function () {
//        $("#addFormx").unbind().submit(function (e) {
//            e.preventDefault();
//            var form = $("#addFormx");
//            var $data = $(this).serialize();
////            $data += "&name=<?= $this->security->get_csrf_token_name() ?>&value=<?= $this->security->get_csrf_hash() ?>";
//            $.ajax({
//                url: "<?= site_url('system_settings/SaveMin_qty_lvl/?v=1') ?>",
//                type: "get", async: false,
//                data: $data,
//                dataType: 'json',
//                success: function (data, textStatus, jqXHR) {
//                    if (data.s === "true") {
//                        bootbox.alert('Paramenter Added Successfully!');
//                        oTable.fnDraw();
//                        $("#myModal").modal('hide');
//                    } else {
//                        bootbox.alert('Paramenter Adding Failed!');
//                        $("#myModal").modal('hide');
//                    }
//                }
//            });
//            return false;
//        });
        var obj = ['companies', 'department', 'section', 'product_items', 'type', 'brands', 'design', 'style', 'pattern', 'fitting', 'fabric', 'color', 'size', 'per'];
        var objarr = [];
        var i = 0;
        $.each(obj, function (k, v) {
            objarr[i] = myselect2(v);
            objarr[i]();
            i++;
        });
        function myselect2(id) {
            if (id == "per") {
                var pp = $("." + id);
            } else {
                var pp = $("#" + id);
            }
            function get() {
                pp.select2({
                    minimumInputLength: 1,
                    data: [],
                    initSelection: function (element, callback) {
                        $.ajax({
                            type: "get", async: false,
                            url: site_url + "products/getIdAttribute",
                            data: {
                                term: pp.val(),
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                            },
                            dataType: "json",
                            success: function (data) {
                                callback(data[0]);
                            }
                        });
                    },
                    ajax: {
                        url: site_url + "products/getIdAttributes",
                        dataType: 'json',
                        quietMillis: 15,
                        data: function (term, page) {
                            return {
                                term: term,
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                                limit: 10
                            };
                        },
                        results: function (data, page) {
                            if (data.results != null) {
                                return {results: data.results};
                            } else {
                                return {results: [{id: '', text: 'No Match Found'}]};
                            }
                        }
                    }
                });
            }
            return get;
        }
        function getidsx() {
//            alsert($("#selsize").data('id'));
            var $ids = $("#selsize").data('id').split(",");
            var $idvals = "";

            $.each($ids, function (k, v) {
                $idvals += ($("#" + v).val() ? $("#" + v).val() : "") + "-";
            });
            $idvals = $idvals.trim();
            return $idvals;
        }
        $("#selsize").select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site_url + "products/getIdAttribute",
                    data: {
                        term: $(element).val(),
                        tab: $(element).data('tab'),
                        id: getidsx,
                        key: $(element).data('key'),
                    },
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site_url + "products/getIdAttributes",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        tab: $("#selsize").data('tab'),
                        id: getidsx,
                        key: $("#selsize").data('key'),
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'No Match Found'}]};
                    }
                }
            }
        });
    });




    $('#get_supplier').change(function () {
        var v = $(this).val();
        $('#modal-loading').show();
        if (v) {
            $.ajax({
                type: "get",
                async: false,
                data: {store: v},
                url: "<?= site_url('purchases/getBiller') ?>",
                dataType: "json",
                success: function (scdata) {
                    $('#sender').html('');
                    $('#sender').removeAttr('readonly', '');
                    $.each(scdata, function (k, val) {
                        var opt = $('<option />');
                        opt.val(val.id);
                        opt.text(val.code + ' - ' + val.company);
                        $('#sender').append(opt);
                    })
				},
                error: function () {
                    $('#sender').attr('readonly', '');
                    $('#modal-loading').hide();
                }

            });

            $.ajax({
                type: "get",
                async: false,
                data: {store: v},
                url: "<?= site_url('purchases/getEmployees') ?>",
                dataType: "json",
                success: function (scdata) {
                    $('#booked_by').html('');
                    $('#booked_by').removeAttr('readonly', '');
                    $.each(scdata, function (k, val) {
                        var opt = $('<option />');
                        opt.val(val.id);
                        opt.text(val.fname + ' ' + val.mname + ' ' + val.lname);
                        $('#booked_by').append(opt);
                    })
                }, error: function () {
//                    $('#booked_by').addClass('hidden');
                    $('#booked_by').attr('readonly', '');
                    $('#modal-loading').hide();
                }
            })
        }
    });


    $('#sender').change(function () {
        var v = $(this).val();
        $('#modal-loading').show();
        if (v) {
            $.ajax({
                type: "get",
                async: false,
                url: "<?= site_url('purchases/getSupplierAddress') ?>/" + v,
                dataType: "json",
                success: function (scdata) {
                    if (scdata) {
                        $('#sender_address').val(scdata);
                    } else {
                        bootbox.alert('<?= lang('sender_address_not_avail') ?>');
                        $('#sender_address').val('');
                    }
                },
                error: function () {
                    bootbox.alert('<?= lang('ajax_error') ?>');
                    $('#modal-loading').hide();
                }
            });

			$.ajax({
                type: "get",
                async: false,
                data:{"supplier_id": v},
                url: "<?= site_url('common/getAgainstOrderNoAccordingSupplier') ?>",
                dataType: "json",
                success: function (scdata) {
                    if (scdata) {
                        //$('#reference_no').val(scdata);
                        $('#reference_no').html('');
                        $('#reference_no').removeAttr('readonly', '');
                        $('#reference_no').append('<option value="">Please Select Against Order No</option>');
                        $.each(scdata, function (k, val) {
    //                        alert(val.reference_no);
                            var opt = $('<option />');
                            opt.val(val.reference_no);
                            // if (orderid == val.reference_no) {
                            //     opt.prop('selected', 'selected');
                            // }
                            opt.text(val.reference_no + ' [ ' + formatNumber(val.total_qty, 0) + ' ]');
                            $('#reference_no').append(opt);
                        })
                    } else {
                        $('#reference_no').html('');
                        $('#reference_no').attr('readonly', '');
                        $('#reference_no').append('<option value="">Against Order no is not available</option>');

                        // bootbox.alert('<?= lang('sender_address_not_avail') ?>');
                        // $('#reference_no').val('');
                    }
                },
                error: function (er) {
					//console.log(er);
                    bootbox.alert('<?= lang('ajax_error') ?>');
                    $('#modal-loading').hide();
                }
            });
        }
    });



//    $('#transport').change(function () {
//        var v = $(this).val();
//
//    });

</script>
