<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('case_transfer'); ?></h4>
        </div>
        <?php
        $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("purchases/edit_cash_transfer/" . $cashtrans->id, $attrib);
        ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <?php if ($Owner || $Admin) { ?>
                <div class="form-group">
                    <?= lang("date", "date"); ?>
                    <?= form_input('date', ($this->sma->hrld($cashtrans->date)), 'class="form-control datetime" id="date" required="required"'); ?>
                </div>
            <?php } ?>
            <div class="form-group">
                <?php $refno_cash = 'CT' . $this->site->getReference('ctran'); ?>
                <?= lang("reference", "reference"); ?>
                <?= form_input('reference', ($cashtrans->reference), 'class="form-control tip" readonly id="reference"'); ?>
            </div>
            <div class="form-group">
                <?= lang("transfer_to", "transfer_to"); ?>
                <?php
                $us = array();
                $us[""] = "Select Casher";
                foreach ($user as $usr) {
                    $us[$usr->id] = $usr->first_name != '-' ? $usr->first_name . ' ' . $usr->last_name : $usr->first_name . ' ' . $usr->last_name;
                }
                echo form_dropdown('transfer_to', $us, $cashtrans->empid_to, 'id="transfer_to" placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("transfer_to") . '" required="required"  disabled class="form-control input-tip select" style="width:100%;"');
                ?>
            </div>
            <div class="form-group">
                <?= lang("total_amount", "total_amount"); ?>
                <?php
                $expense = $expenses ? $expenses->total : 0;
                $transfer = $transfer ? $transfer->total : 0;
                $receive = $receive ? $receive->total : 0;

//                die();
                $totalamount = $cashsales->paid ? $this->sma->formatMoney(($cashsales->paid + ($this->session->userdata('cash_in_hand')) + $receive ) - ($refunds->returned ? $refunds->returned : 0) - $expense - $transfer) : $this->sma->formatMoney($this->session->userdata('cash_in_hand') - $expense - $transfer);
                ?>
                <div class="input-group">
                    <input name="total_amount" readonly  type="text" id="total_amount" value="<?= $totalamount ?>" class="pa form-control kb-pad amount"

                           required="required"/>
                    <div class="input-group-addon no-print ">
                        <label style="width: 50px">Cr.</label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <?= lang("transfer_from", "transfer_from"); ?>
                <?php
                $us = array();
                $us[""] = "Select Casher";
                foreach ($user as $usr) {
                    if ($usr->id == $this->session->userdata('user_id')) {
                        
                    } else {
                        $us[$usr->id] = $usr->first_name != '-' ? $usr->first_name . ' ' . $usr->last_name : $usr->first_name . ' ' . $usr->last_name;
                    }
                }
                echo form_dropdown('transfer_from', $us, ($cashtrans->empid_from), 'id="transfer_from" placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("transfer_from") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                ?>
            </div>

            <div class="form-group">
                <?= lang("amount", "amount"); ?>
                <div class="input-group">
                    <input name="amount" type="text" id="amount" value="<?= $cashtrans->amount ?>" class="pa form-control kb-pad amount"
                           data-bv-regexp="true"
                           data-bv-regexp-regexp="^[1-9]\d*(\.\d+)?$"
                           data-bv-regexp-message="Enter only digit"
                           required="required"/>
                    <div class="input-group-addon no-print ">
                        <label style="width: 50px">Dr.</label>
                    </div>
                </div>
            </div>


            <div class="form-group">
                <?= lang("note", "note"); ?>
                <?php echo form_textarea('note', ($cashtrans->note), 'class="form-control" id="note"'); ?>
            </div>

        </div>
        <div class="modal-footer">
            <?php echo form_submit('edit_transfer_cash', lang('transfer'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?= $dp_lang ?>;
</script>
<?= $modal_js ?>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        $.fn.datetimepicker.dates['sma'] = <?= $dp_lang ?>;
        $("#date").datetimepicker({
            format: site.dateFormats.js_ldate,
            fontAwesome: true,
            language: 'sma',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0
        }).datetimepicker('update', new Date());
    });
</script>
