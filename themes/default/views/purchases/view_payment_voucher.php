<style>
    .close{
        font-size: 19px !important;
    }
    .heading{
        margin-top:14px !important;
        border-top: 1px solid black;
    }
    .heading1{
        padding-top: 7px;
    }
    .modal-header{
        border-bottom: none !important;
    }
</style>
<?php echo $modal_js ?>
<div class="modal-dialog modal-lg ">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
        </div>
        <?php
        $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'addFormx');
        echo form_open_multipart("purchases/UpdateVoucher/".$voucher->id, $attrib);
        ?>
        <div class="modal-body">
            <div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <h2 style="margin:0;padding-top: 10px !important;">
                        <?= lang('viewpaymentvoucher') ?>
                    </h2>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
                    <b style='font-size: 20px;'><?= $Settings->site_name ?></b>
                    <!--<h4>Kavathe - Mahankal</h4>-->
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 "></div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 heading" >
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                    <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="joining_date">  <?= lang('paymentno') ?> </label>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 heading1">
                        : <?php echo $voucher->payment_no ?>
                    </div>
                </div>
                <input type="text" name="type" value="payment" class="form-control hidden">
                
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                    <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="joining_date"><?= lang('date') ?> </label>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 heading1">
                         : <?php echo $voucher->date ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group">
                    <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="joining_date">  <?= lang('user') ?>  </label>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 heading1">
                      : <?= $voucher->first_name ?>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                    <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="joining_date"> <?= lang('time') ?> </label>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 heading1">
                          : <?= $voucher->time ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 heading" style="padding:0;">
                <h4 class="text-center" ><span style="font-size: 20px; text-decoration: underline;padding-bottom:5px;"><?= lang('beneficiary') ?></span>&nbsp;<span style="font-size: 20px; text-decoration: underline;padding-bottom:5px;"><?= lang('details') ?></span></h4>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="payfor"><?= lang('payfor') ?> </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                              : <?= $voucher->for ?>
                               
<!--                            <select class="form-control col-lg-5 col-md-5 col-sm-5 col-xs-12" name="for" id="sel1">
                                
                                <option value="purchase bill">Purchase Bill</option>
                            </select>                             -->
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="party_account"><?= lang('party_account') ?> </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            : <?= $voucher->company_name ?>
                            <?php
//                            $bl = "";
//                            foreach ($store as $row) {
//                                $bl[$row->id] = $row->name;
//                            }
//                            echo form_dropdown('account', $bl, (isset($_POST['account']) ? $_POST['account'] : $voucher->account), 'id="qubiller" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="againstno"><?= lang('againstno') ?> </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            : <?= $voucher->reference_no ?>
                            <?php
//                            $rf = "";
//                            foreach ($ref_no as $row) {
//                                $rf[$row->reference_no] = $row->reference_no;
//                            }
//                            echo form_dropdown('against_ref_no', $rf,(isset($_POST['against_ref_no']) ? $_POST['against_ref_no'] : $voucher->against_ref_no), 'id="qubiller" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="joining_date"><?= lang('bank_acc_no') ?> </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <?php 
//                                echo"<pre>";
//                                print_r($voucher);
//                                echo"</pre>";
                                ?>
                            : <?=  $voucher->bank_acc_no?>
                            
                            <?php
//                            foreach ($bank_acc_no as $row) {
//                                $b[$row->account_no] = $row->account_no;
//                            }
//                            echo form_dropdown('bank_acc_no', $b, (isset($_POST['against_ref_no']) ? $_POST['against_ref_no'] : $voucher->bank_acc_no), 'id="bank" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("bank_acc_no") . '" required="required" class="form-control input-tip select" style="width:100%;"');
//                            
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="dated"><?= lang('dated') ?> </label>

                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                             : <?=  $this->sma->hrld($voucher->datedd) ?>
                            <?php // echo form_input('datedd', (isset($_POST['date']) ? $_POST['date'] : $this->sma->hrld($voucher->datedd)), 'class="form-control input-tip datetime" id="podate" required="required"'); ?>                        
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="amount"><?= lang('amount') ?> </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">

                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 " style="padding: 0">
                                 : <?= $voucher->amount ?>
                                <?php // echo form_input('amount', (isset($_POST['amount']) ? $_POST['amount'] : $voucher->amount), 'class="form-control input-tip" required="required"'); ?>                        
                                <!--<input type="text" name="amount" class="form-control" placeholder="55,689.00" required=""  >-->
                            </div>
                            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12" style="padding-right: 0">CR
                                <!--<input type="text"  class="form-control" placeholder="cr." disabled>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 heading" style="padding:0;">
                <h4 class="text-center" ><span style="font-size: 20px; text-decoration: underline;padding-bottom:5px;"><?= lang('payer') ?></span>&nbsp;<span style="font-size: 20px; text-decoration: underline;padding-bottom:5px;"><?= lang('details') ?></span></h4>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="payment_mode"><?= lang('payment_mode') ?></label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                             : <?= $voucher->payment_mode ?>
<!--                            <select class="form-control col-lg-5 col-md-5 col-sm-5 col-xs-12" id="sel1" name="payment_mode">
                                <option value="cheque">Cheque Payment</option>
                            </select>-->
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="from_acc"><?= lang('from_acc') ?></label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                           <?php 
//                            echo "<pre>";
//print_r($voucher);
//echo "</pre>";
//die();
                           ?>
                            : <?= $voucher->from_acc_bank_name ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="pay_type_no"><?= lang('paym_no') ?></label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                              : <?= $voucher->pay_type_no ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="bank_acc_number"><?= lang('acc_no') ?> </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                              <?php // echo $voucher->bank_acc_number ?>
                             : <?= $voucher->bank_acc_number ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="joining_date"><?= lang('dated') ?></label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                      : <?= $this->sma->hrld($voucher->dated) ?>                  
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 form-group ">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="joining_date"><?= lang('balance') ?> </label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="padding: 0">
                                : <?= $voucher->balance ?>
                            </div>
                            <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12" style="padding-right: 0">Dr.
                                <!--<input type="text" class="form-control" placeholder="Dr." required=""  >-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer" style="clear:both;text-align: left;">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                        <b>Narration</b> : <?= $voucher->narration ?>
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-5">
                    <?php // echo form_submit('add_transfer', $this->lang->line("submit"), 'id="add_transfer" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>

                </div>
            </div>
        </div>




        <?php echo form_close(); ?>

        <!---->
    </div><!--end body div-->

    <!--end foter div-->

</div>


<script type="text/javascript">
    $(document).ready(function (e) {
        $('#addFormx').bootstrapValidator({
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            }, excluded: [':disabled']
        });
        $('select.select').select2({minimumResultsForSearch: 6});
        fields = $('.modal-content').find('.form-control');
        $.each(fields, function () {
            var id = $(this).attr('id');
            var iname = $(this).attr('name');
            var iid = '#' + id;
            if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
//                $("label[for='" + id + "']").append(' *');
                $(document).on('change', iid, function () {
                    $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
                });
            }
        });
    });

//    mrp = onlydigits('mrp');
//    mrp();
//    mrp_from = onlydigits('mrp_from');
//    mrp_from();
//    mrp_to = onlydigits('mrp_to');
//    mrp_to();
//    sales_incentive = onlydigits('sales_incentive');
//    sales_incentive();
//    min_qty_lvl = onlydigits('min_qty_lvl');
//    min_qty_lvl();
//    min_qty_lvl_2 = onlydigits('min_qty_lvl_2');
//    min_qty_lvl_2();
//    reorder_qty = onlydigits('reorder_qty');
//    reorder_qty();
//    reorder_qty_2 = onlydigits('reorder_qty_2');
//    reorder_qty_2();
    $(document).ready(function () {
//        $("#addFormx").unbind().submit(function (e) {
//            e.preventDefault();
//            var form = $("#addFormx");
//            var $data = $(this).serialize();
////            $data += "&name=<?= $this->security->get_csrf_token_name() ?>&value=<?= $this->security->get_csrf_hash() ?>";
//            $.ajax({
//                url: "<?= site_url('system_settings/SaveMin_qty_lvl/?v=1') ?>",
//                type: "get", async: false,
//                data: $data,
//                dataType: 'json',
//                success: function (data, textStatus, jqXHR) {
//                    if (data.s === "true") {
//                        bootbox.alert('Paramenter Added Successfully!');
//                        oTable.fnDraw();
//                        $("#myModal").modal('hide');
//                    } else {
//                        bootbox.alert('Paramenter Adding Failed!');
//                        $("#myModal").modal('hide');
//                    }
//                }
//            });
//            return false;
//        });
        var obj = ['companies', 'department', 'section', 'product_items', 'type', 'brands', 'design', 'style', 'pattern', 'fitting', 'fabric', 'color', 'size', 'per'];
        var objarr = [];
        var i = 0;
        $.each(obj, function (k, v) {
            objarr[i] = myselect2(v);
            objarr[i]();
            i++;
        });
        function myselect2(id) {
            if (id == "per") {
                var pp = $("." + id);
            } else {
                var pp = $("#" + id);
            }
            function get() {
                pp.select2({
                    minimumInputLength: 1,
                    data: [],
                    initSelection: function (element, callback) {
                        $.ajax({
                            type: "get", async: false,
                            url: site_url + "products/getIdAttribute",
                            data: {
                                term: pp.val(),
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                            },
                            dataType: "json",
                            success: function (data) {
                                callback(data[0]);
                            }
                        });
                    },
                    ajax: {
                        url: site_url + "products/getIdAttributes",
                        dataType: 'json',
                        quietMillis: 15,
                        data: function (term, page) {
                            return {
                                term: term,
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                                limit: 10
                            };
                        },
                        results: function (data, page) {
                            if (data.results != null) {
                                return {results: data.results};
                            } else {
                                return {results: [{id: '', text: 'No Match Found'}]};
                            }
                        }
                    }
                });
            }
            return get;
        }
        function getidsx() {
//            alsert($("#selsize").data('id'));
            var $ids = $("#selsize").data('id').split(",");
            var $idvals = "";

            $.each($ids, function (k, v) {
                $idvals += ($("#" + v).val() ? $("#" + v).val() : "") + "-";
            });
            $idvals = $idvals.trim();
            return $idvals;
        }
        $("#selsize").select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site_url + "products/getIdAttribute",
                    data: {
                        term: $(element).val(),
                        tab: $(element).data('tab'),
                        id: getidsx,
                        key: $(element).data('key'),
                    },
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site_url + "products/getIdAttributes",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        tab: $("#selsize").data('tab'),
                        id: getidsx,
                        key: $("#selsize").data('key'),
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'No Match Found'}]};
                    }
                }
            }
        });
    });
</script>
