
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_department'); ?></h4>
        </div>

        <?php
        $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'add_department');
        echo form_open_multipart("departments/add", $attrib);
        ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            
            <div class="row">
                <div class="col-md-6">
                    
                    <div class="form-group">
                        <?= lang("Store", "companies") ?> <label>*</label>
                        <?php
                        $bl[""] = "";
                        foreach ($billers as $biller) {
                            $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->company;
                        }
                        echo form_dropdown('store_id', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $quote->biller_id), 'id="store" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("biller") . '" required="required" class="form-control" style="width:100%;"');
                        ?>
                    </div>
                    <div class="form-group">
                        <?= lang("department", "department_name") ?>
                        <?php echo form_input('department_name', '', 'class="form-control tip" id="department_name" data-bv-notempty="true"'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer" style="margin-top:10px;">
            <div class="text-center">
            <?php echo form_submit('add_department', lang('save'), 'class="btn btn-primary"'); ?>
            </div>
        </div>
   </div>
    <?php echo form_close(); ?>
 
</div>
<?= $modal_js ?>