
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_biller'); ?></h4>
        </div>
        <?php
        $attrib = array('data-toggle' => 'validator', 'role' => 'form', "id" => "edit_department");
        echo form_open_multipart("departments/edit/" . $department->id, $attrib);
        ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            
            
            <div class="row billing">
                <div class="col-md-6">
                    
                    <div class="form-group">
                        <?= lang("Store", "companies") ?> <label>*</label>
                        <?php
                        $bl[""] = "";
                        foreach ($billers as $biller) {
                            $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->company;
                        }
                        
                        echo form_dropdown('store_id', $bl, (isset($_POST['store_id']) ? $_POST['store_id'] : $department->store_id), 'id="store" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("biller") . '" required="required" class="form-control" style="width:100%;"');
                        ?>
                    </div>
                    <div class="form-group person">
                        <?= lang("department", "department_name"); ?>
                        <?php echo form_input('department_name', $department->name, 'class="form-control tip" id="department_name" required="required"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("status", "status") ?> <label>*</label>
                        <?php
                        $bl = ["Active"=>"Active", "Deactive"=>"Deactive"];
                        echo form_dropdown('status', $bl, (isset($_POST['status']) ? $_POST['status'] : $department->status), 'id="status" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("status") . '" required="required" class="form-control" style="width:100%;"');
                        ?>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('edit_department', lang('update'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

