
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_biller'); ?></h4>
        </div>
        <?php
        $attrib = array('data-toggle' => 'validator', 'role' => 'form', "id" => "addstore");
        echo form_open_multipart("billers/edit/" . $biller->id, $attrib);
        ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="row hide" id="on_logo">
                <div class="">
                    <div class="form-group">
                        <?= lang("logo", "biller_logo"); ?>
                        <div class="input-group"  style="width: auto">

                            <?php
//                        echo form_upload('logo', '', 'class="form-control file-loading" id="store_logo_up" required="required" ');
                            ?>
<!--                            <input id="store_logo_up" name="logo" type="file" class="form-control file-loading" data-show-upload="false"
                                   data-show-preview="false" data-bv-file-maxsize="300*100" accept="image/*">-->
                            <div style="width:850px;">
                                <input id="store_logo_up" name="logo" type="file" class="form-control file-loading" data-show-upload="false" data-show-preview="false" accept="image/*">
                            </div>
                            <div class="input-group-addon no-print">
                                <!--<a href="javascript:void(0);" onclick="getLogoff()"><i class="fa fa-minus-circle" style="font-size: 26px;color: #cc0000" id="addIcon"></i></a>-->

                                <a href="javascript:void(0);" onclick="getLogoff()">
                                    <i class="fa fa-minus-circle" style="font-size: 26px;color: #cc0000" id="addIcon"></i>
                                </a>

                                <!--<a href="javascript:void(0);" onclick="getLogoff()"></a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row billing" id="off_logo">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("logo", "biller_logo"); ?>
                        <div class="input-group">
                            <?php
                            $biller_logos[''] = 'Select Logo';
                            foreach ($logos as $key => $value) {
                                $biller_logos[$value] = $value;
                            }
                          
                            echo form_dropdown('logo_name', $biller_logos, $biller->logo, 'class="form-control select" id="biller_logo" required="required" ');
                            ?>
                            <div class="input-group-addon no-print">
                                <a href="javascript:void(0);" onclick="getLogUp()">
                                    <i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i>
                                </a>
                            </div>
                            <div class="input-group-addon no-print">
                                <a href="javascript:void(0);" title="Remove Logo">
                                    <i class="fa fa-minus-circle" style="font-size: 26px" id="removelogo"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="logo-con" class="text-center"><img src="<?= base_url('assets/uploads/logos/' . $biller->logo) ?>"  alt="" style="width:300px; height:200px;"></div>
                </div>
            </div>
            <div class="row billing">
                <div class="col-md-6">
                    <!-- <div class="form-group company">
                        <?= lang("company", "company"); ?>
                        <?php echo form_input('company', $biller->company, 'class="form-control tip" id="company" required="required"'); ?>
                    </div> -->
                    <div class="form-group person">
                        <?= lang("store_name", "store_name"); ?>
                        <?php echo form_input('store_name', $biller->company, 'class="form-control tip" id="store_name" required="required"'); ?>
                    </div>
                    <div class="form-group person">
                        <?= lang("propriter_name", "propriter_name"); ?>
                        <?php echo form_input('propriter_name', $biller->name, 'class="form-control tip" id="propriter_name" required="required"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("gst_no", "gst_no"); ?>
                        <?php
                        echo form_input('vat_no', $biller->vat_no, 'class="form-control" id="gst_no"'
                                . 'data-bv-notempty="true"
                               data-bv-notempty-message="The GST no is required and cannot be empty"
//                               data-bv-stringlength="true"
                               "');
                        ?>
                    </div>
                    <!--<div class="form-group company">
                    <?= lang("contact_person", "contact_person"); ?>
                    <?php //echo form_input('contact_person', $biller->contact_person, 'class="form-control" id="contact_person" required="required"');     ?>
                </div> -->
                    <div class="form-group">
                        <?= lang("email_address", "email_address"); ?>
                        <input type="email" name="email" class="form-control" required="required" id="email_address"
                               value="<?= $biller->email ?>"/>
                    </div>
                    <div class="form-group">
                        <?= lang("phone", "phone"); ?>
                        <input type="tel" name="phone" class="form-control" required="required" id="phone"
                               value="<?= $biller->phone ?>"
                               data-bv-notempty="true"
                               data-bv-notempty-message="The Mobile no is required and cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="10"
                               data-bv-stringlength-max="10"
                               data-bv-stringlength-message="The Mobile no must be exact 10 numbers long"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The Mobile no can only consist of digits"
                               />
                    </div>
                    <div class="form-group">
                        <?= lang("address", "address"); ?>
                        <?php echo form_input('address', $biller->address, 'class="form-control" id="address" required="required"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("city", "city"); ?>
                        <?php echo form_input('city', $biller->city, 'class="form-control" id="city" required="required"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("state", "state"); ?>
                        <?php echo form_input('state', $biller->state, 'class="form-control" id="state"'); ?>
                    </div>
                    <!--                    <div class="form-group">
                    <?= lang("product_tax", "tax_rate") ?>
                    <?php
                    $tr[""] = "";
                    foreach ($tax_rates as $tax) {
                        $tr[$tax->id] = $tax->name;
                    }
                    echo form_dropdown('tax_rate', $tr, (isset($_POST['tax_rate']) ? $_POST['tax_rate'] : $biller->tax_rate ? $biller->tax_rate : $Settings->default_tax_rate), 'class="form-control select" id="tax_rate" placeholder="' . lang("select") . ' ' . lang("product_tax") . '" style="width:100%"');
                    ?>
                                        </div>-->
                    <div class="form-group">
                        <?= lang("tax_method", "tax_method") ?>
                        <?php
                        $tm = array('0' => lang('inclusive'), '1' => lang('exclusive'));
                        echo form_dropdown('tax_method', $tm, (isset($_POST['tax_method']) ? $_POST['tax_method'] : $biller->tax_method), 'class="form-control select" id="tax_method" placeholder="' . lang("select") . ' ' . lang("tax_method") . '" style="width:100%"');
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("postal_code", "postal_code"); ?>
                        <?php
                        echo form_input('postal_code', $biller->postal_code, 'class="form-control" id="postal_code"'
                                . 'data-bv-notempty="true"
                               data-bv-notempty-message="The Zip Code is required and cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="6"
                               data-bv-stringlength-max="6"
                               data-bv-stringlength-message="The Zip Code must be exact 6 numbers long"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The Zip Code can only consist of digits"')
                        ?>
                    </div>
                    <div class="form-group">
                        <?= lang("country", "country"); ?>
                        <?php echo form_input('country', $biller->country, 'class="form-control" id="country"'); ?>
                    </div>
                    <?php echo form_hidden('latitude', '', 'class="form-control" id="auto_address_latitude"'); ?>
                    <?php echo form_hidden('longitude', '', 'class="form-control" id="auto_address_longitude"'); ?>
                    <!--<div class="form-group">
                        <?= lang("bcf1", "cf1"); ?>
                        <?php echo form_input('cf1', $biller->cf1, 'class="form-control" id="cf1"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("bcf2", "cf2"); ?>
                        <?php echo form_input('cf2', $biller->cf2, 'class="form-control" id="cf2"'); ?>

                    </div>
                    <div class="form-group">
                        <?= lang("bcf3", "cf3"); ?>
                        <?php echo form_input('cf3', $biller->cf3, 'class="form-control" id="cf3"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("bcf4", "cf4"); ?>
                        <?php echo form_input('cf4', $biller->cf4, 'class="form-control" id="cf4"'); ?>

                    </div>
                    <div class="form-group">
                        <?= lang("bcf5", "cf5"); ?>
                        <?php echo form_input('cf5', $biller->cf5, 'class="form-control" id="cf5"'); ?>

                    </div>
                    <div class="form-group">
                        <?= lang("bcf6", "cf6"); ?>
                        <?php echo form_input('cf6', $biller->cf6, 'class="form-control" id="cf6"'); ?>
                    </div>-->
                    <div class="form-group">
                        <?= lang("barcode_prefix", "barcode_prefix"); ?>
                        <?php echo form_input('barcode_prefix', $biller->barcode_prefix, 'class="form-control" id="barcode_prefix" data-bv-notempty="true"'); ?>
                    </div>
                </div>
                <div class="col-sm-12" >
                    <?= lang("description", "description"); ?>
                    <?php echo form_textarea('description', $biller->invoice_footer, 'class="form-control skip" id="invoice_footer" style="height:100px;"'); ?>
                </div>
            </div>
            <div class="row  billing">

            </div>

        </div>
        <div class="modal-footer">
            <?php // echo form_submit('edit_biller', lang('edit_biller'), 'class="btn btn-primary"'); ?>
            <?php echo form_submit('edit_biller', lang('update'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#biller_logo').change(function (event) {
            var biller_logo = $(this).val();
            $('#logo-con').html('<img src="<?= base_url('assets/uploads/logos') ?>/' + biller_logo + '" alt="" style="width:300px; height:200px;">');
        });

    });
    /**
     * Comment
     */
    function getLogUp() {
        $("#off_logo").addClass('hide');
        $("#on_logo").removeClass('hide');
    }
    function getLogoff() {
        $("#off_logo").removeClass('hide');
        $("#on_logo").addClass('hide');
    }
    $(function () {
        $('#store_logo_up').fileinput({
//            maxFileSize: <?= (double) ($this->Settings->iwidth * $this->Settings->iheight) ?>,
        });
        $('#addstore').bootstrapValidator({
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            },
//            fields: {
//                logo: {
//                    validators: {
//                        file: {
//                            extension: 'jpeg,png',
//                            type: 'image/jpeg,image/png',
////                            maxSize: 300 * 100,
//                            message: 'Please check image size. it has to be 300X100 Image size.'
//                        }
//                    }
//                },
//                
//            }
        });
    });


    $('#removelogo').click(function () {
        
        var v = $('#biller_logo').val();
        $('#biller_logo').html('');
        $("#biller_logo").select2("val", "");
        if (v) {
            $.ajax({
                data: {
                    logo: v
                },
                type: 'GET',
                url: site.base_url + "billers/getLogoListdelete",
                dataType: "json",
                success: function (scdata) {
                    $('#logo-con').html('<img src="" alt="" >');
                    if (scdata) {

                        $("#removelogo").val("");
                        $('#biller_logo').append("<option value=''>Select Logo</option>");
                        $.each(scdata, function (k, val) {
                            var opt = $('<option />');
                            opt.val(val);
                            opt.text(val);
                            $('#biller_logo').append(opt);
                        })

                        $('#modal-loading').hide();
                    } else {
                        $('#biller_logo').html('');
                    }
                },
                error: function () {
                    $('#modal-loading').hide();
                }
            });
        }
    });
</script>

<script>
        var autocomplete;

var componentForm = {
  locality: 'long_name', // city
  administrative_area_level_1: 'long_name', //state
  country: 'long_name', // country
  postal_code: 'short_name' //pincode
};
var options = {
//   types: ['(cities)'],
    types: ['geocode'],
    componentRestrictions: {country: "in"}
 };

initAutocomplete();

    function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
        document.getElementById('address'), options );
        // autocomplete.setFields(['address_component']);
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        var place = autocomplete.getPlace();
        console.log(place);
        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng();
        $("#auto_address_latitude").val(latitude);
        $("#auto_address_longitude").val(longitude);
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            var val = place.address_components[i][componentForm[addressType]];
            if (addressType == "locality") {
                // console.log(addressType+" " +val);
                $("#city").val(val);

            }
            else if (addressType == "administrative_area_level_1") {
                // console.log(addressType+" " +val);
                $("#state").val(val);

            }
            else if (addressType == "postal_code") {
                // console.log(addressType+" " +val);
                $("#postal_code").val(val);

            }
            else if (addressType == "country") {
                // console.log(addressType+" " +val);
                $("#country").val(val);

            }
        }
    }

    </script>
<?= $modal_js ?>

