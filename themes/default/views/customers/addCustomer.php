<style>.btton{padding: 5px;width:100%;}
    .form-group {margin-bottom: 6px !important;}
    
</style>
<script type="text/javascript" src="<?= $assets ?>js/webcam.js"></script>
<div class="container-fluid" style="background-color: #ffff;padding: 10px">
    <?php
    $attrib = array('data-toggle' => 'validator', 'class' => 'form-horizontal', 'role' => 'form');
    echo form_open_multipart("customers/add", $attrib);
    ?>
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
            <div class="form-group">
                <label class="control-label col-sm-5"><?= lang('customercode', 'customercode') ?></label>
                <div class="col-sm-7">
                    <input type="text" id="customercode" value="<?= set_value('supliercode', ''); ?>" name="supliercode" readonly class="form-control remove_readonly"/>
                </div>
                <div class="row">
                    <button type="button" onclick="enableEdit()"><?= lang('define_my_own_Code') ?></button>
                </div>
            </div>
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Customer Details</legend>
                <div class="form-group">
                    <label class="control-label col-sm-5"><?= lang('fname', 'fname') ?></label>
                    <div class="col-sm-7">
                        <?php echo form_input('customerFname', set_value('customerFname', ''), 'class="form-control tip" id="fname" data-bv-notempty="true" data-bv-regexp="true" data-bv-regexp-regexp="^[a-zA-Z ]+$" data-bv-regexp-message="Allow Only Alphabets"'); ?>
                        <!--<input type="text" class="form-control">-->
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5"><?= lang('mname', 'mname') ?></label>
                    <div class="col-sm-7">
                        <?php echo form_input('customerMname', set_value('customerMname', ''), 'class="form-control tip" id="mname" data-bv-notempty="true" data-bv-regexp="true" data-bv-regexp-regexp="^[a-zA-Z ]+$" data-bv-regexp-message="Allow Only Alphabets"'); ?>
                        <!--<input type="text" class="form-control">-->
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5"><?= lang('lname', 'lname') ?></label>
                    <div class="col-sm-7">
                        <?php echo form_input('customerLname', set_value('customerLname', ''), 'class="form-control tip" id="lname" data-bv-notempty="true" data-bv-regexp="true" data-bv-regexp-regexp="^[a-zA-Z ]+$" data-bv-regexp-message="Allow Only Alphabets"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 pd-0"><?= lang('dob', 'dob') ?></label>
                    <div class="col-sm-7">
                        <?php echo form_input('dateofbirth', set_value('dateofbirth', ''), 'class="form-control input-tip date" id="dob" data-bv-notempty="true"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5"><?= lang('Marital Status', 'marital_status') ?></label>
                    <!-- <div class="col-sm-7"> -->
                        <?php //echo form_input('marital_status', set_value('marital_status', ''), 'class="form-control input-tip" id="marital_status" data-bv-notempty="false"'); ?>
                    <!-- </div> -->
                    <div class="col-sm-7">
                    <?php
                        $marital_status = [];
                        $marital_status[''] = "";
                        // foreach ($user as $usr) {
                            // $us[$usr->id] = $usr->first_name != '-' ? $usr->first_name : $usr->first_name;
                        // }
                        $marital_status["single"] ="Single";
                        $marital_status["married"] ="Married";
                        // $marital_status["divorced"] ="Divorced";
                        echo form_dropdown('marital_status', $marital_status, '', 'class="form-control select" id="marital_status" data-bv-notempty="true" placeholder="' . lang("select") . " " . lang("marital_status") . '"')
                    ?>
                    </div>
                </div>
                <div class="form-group" id="anniversary_date_block" style="display:none">
                    <label class="control-label col-sm-5"><?= lang('anniversarydate', 'anniversarydate') ?></label>
                    <div class="col-sm-7">
                        <?php echo form_input('anniversarydate', set_value('anniversarydate', ''), 'class="form-control input-tip date" id="anniversarydates" data-bv-notempty="false"'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5"><?= lang('Profession', 'profession') ?></label>
                    <div class="col-sm-7">
                        <?php echo form_input('profession', set_value('profession', ''), 'class="form-control input-tip" id="profession" data-bv-notempty="false"'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5"><?= lang('csd', 'csd') ?></label>
                    <div class="col-sm-7">
                        <?php echo form_input('sincedate', set_value('sincedate', ''), 'class="form-control tip date" id="csd" data-bv-notempty="false"'); ?>
                    </div>

                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5"><?= lang('Address (home)', 'address (home)') ?></label>
                    <div class="col-sm-7">
                    <?php echo form_input('addressline1', set_value('addressline1', ''), 'class="form-control tip" id="address1" data-bv-notempty="true"'); ?>
                    <?php echo form_input('addressline2', set_value('addressline2', ''), 'class="form-control tip" id="address" data-bv-notempty="false"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5"><?= lang('Same Shipping Address', 'same_shipping_address') ?></label>  
                    <div class="col-sm-7">
                        <div class="checkbox" >
                            <input type="checkbox" name="issameshippingaddress" value="1">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5"><?= lang('adhar_no', 'adhar_no') ?></label>
                    <div class="col-sm-7">
                        <?php echo form_input('adhar_no', set_value('adhar_no', ''), 'class="form-control tip" id="adhar_no" data-bv-notempty="true"'); ?>
                    </div>
                </div>


                <div class="form-group">
                    <label class="control-label col-sm-5"><?= lang('pan_no', 'pan_no') ?></label>
                    <div class="col-sm-7">
                        <?php echo form_input('pan_no', set_value('pan_no', ''), 'class="form-control tip" id="pan_no" data-bv-notempty="true"'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5"><?= lang("cg", 'cg') ?></label>
                    <div class="col-sm-7">
                        <?php
                        $groupcust[''] = "";
                        foreach ($custgroup as $row) {
                            $groupcust[$row->id] = $row->name;
                        }
                        echo form_dropdown('customer_groups', $groupcust, '', 'class="form-control select" id="cg" placeholder="' . lang("select") . " " . lang("customer_groups") . '"')
                        ?>

                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5"><?= lang('active', 'active') ?></label>  
                    <div class="col-sm-7">
                        <div class="checkbox" >
                            <input type="checkbox" name="isactive" value="1">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-5"><?= lang('Agent', 'agent') ?></label>  
                    <div class="col-sm-7">
                        <div class="checkbox" >
                            <input type="checkbox" name="isagent" value="1">
                        </div>
                    </div>
                </div>

                <!--                <div class="form-group">
                                    <label class="control-label col-sm-5"></label>
                                    <div class="col-sm-7">
                                        
                                    </div>
                                </div>-->


                <!--                 <div class="row">
                                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">                                                                                                                              
                                       <div class="form-group">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label class="control-label col-sm-5" style="padding:0 !important;"><?= lang('adhar_no', 'adhar_no') ?></label>
                                                    <div class="col-sm-7">
                <?php echo form_input('adhar_no', set_value('adhar_no', ''), 'class="form-control tip" id="adhar_no" data-bv-notempty="true"'); ?>
                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                      <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label col-sm-5" style="padding:0 !important;"><?= lang('pan_no', 'pan_no') ?></label>
                                            <div class="col-sm-7">
                <?php echo form_input('pan_no', set_value('pan_no', ''), 'class="form-control tip" id="pan_no" data-bv-notempty="true"'); ?>
                                           </div>
                                        </div>
                                    </div>
                                </div>-->

            </fieldset>
            
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Customer Loyalty</legend>

                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="checkbox">
                            <label><input type="checkbox" name="isloyatlity" id="isloyatlity" value="1">Is Enrolled for loyalty Points</label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 pd-0"><?= lang('loyalityno', 'loyalityno') ?></label>
                    <div class="col-sm-7">
                        <?php echo form_input('loyalityno', set_value('loyalityno', ''), 'class="form-control tip" id="loyalityno" disabled="disabled" data-bv-notempty="true"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5"></label>
                    <div class="col-sm-7">
                        <button id="autogenerate" type="button" style="width: 100%">Auto-Generate Number</button>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 pd-0"><?= lang('enrolldate', 'enrolldate') ?></label>
                    <div class="col-sm-7">
                        <?php echo form_input('enrolldate', set_value('enrolldate', ''), 'class="form-control tip date" disabled="disabled" id="enrolldate" data-bv-notempty="true"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 pd-0"><?= lang('loyalitypoint', 'loyalitypoint') ?></label>
                    <div class="col-sm-7">
                        <?php echo form_input('loyalitypoint', set_value('loyalitypoint', ''), 'class="form-control tip" disabled="disabled" id="loyalitypoint" data-bv-notempty="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="Only numbers are allowed"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 pd-0"><?= lang('loyalty_percent', 'loyalty_percent') ?></label>
                    <div class="col-sm-7">
                        <?php echo form_input('loyalty_percent', set_value('loyalty_percent', ''), 'class="form-control tip" disabled="disabled" id="loyalty_percent" data-bv-notempty="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="Only numbers are allowed"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5"><?= lang('referby', 'referby'); ?>
                        (For Loyalty Points)</label>
                    <div class="col-sm-7">
                        <?php echo form_input('referby', set_value('referby', ''), 'class="form-control tip" disabled="disabled" id="referby" data-bv-notempty="true"'); ?>
                    </div>

                </div>

            </fieldset>
            <!--<fieldset class="scheduler-border">
                <legend class="scheduler-border">Bank Details</legend>
                <div class="form-group">
                    <label class="control-label col-sm-5"><?= lang('bank_name', 'bank_name') ?></label>
                    <div class="col-sm-7">
                        <?php echo form_input('bank_name', set_value('bank_name', ''), 'class="form-control tip" id="bank_name" data-bv-notempty="true"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5 pd-0"><?= lang('acc_no', 'acc_no') ?></label>
                    <div class="col-sm-7">
                        <?php echo form_input('acc_no', set_value('acc_no', ''), 'class="form-control tip" id="acc_no" data-bv-notempty="true"
                               data-bv-notempty-message="Required"
                               data-bv-stringlength="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="Only Numbers Allowed"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5"><?= lang('ifsc', 'ifsc') ?></label>
                    <div class="col-sm-7">
                        <?php echo form_input('neft', set_value('neft', ''), 'class="form-control tip" id="ifsc" data-bv-notempty="true"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5"><?= lang('bank_branch', 'bank_branch') ?></label>
                    <div class="col-sm-7">
                        <?php echo form_input('bank_branch', set_value('bank_branch', ''), 'class="form-control tip" id="bank_branch" data-bv-notempty="true"'); ?>
                    </div>
                </div>
            </fieldset>-->
        </div><!--end first col-4--->

        
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="display: none">
            <br>
            <ul  class="nav nav-pills">
                <li class="active"><a  href="#1a" data-toggle="tab">Shipping Address</a></li>
                <li><a href="#2a" data-toggle="tab">Billing Address</a></li>
            </ul>
            <div class="tab-content clearfix">
                <div class="tab-pane active" id="1a" style="border:1px solid #ccc;padding:0 22px !important;">

                    <div class="form-group">
                        <label><?= lang('address1', 'address1') ?></label>
                        <?php echo form_input('addressline1', set_value('addressline1', ''), 'class="form-control tip" id="address1" data-bv-notempty="true"'); ?>
                    </div>
                    <div class="form-group">
                        <label><?= lang('address2', 'address2') ?></label>
                        <?php echo form_input('addressline2', set_value('addressline2', ''), 'class="form-control tip" id="address" data-bv-notempty="false"'); ?>
                    </div>
                    <div class="form-group">
                        <label><?= lang('city', 'city') ?></label>
                        <?php echo form_input('city', set_value('city', ''), 'class="form-control tip" id="city" data-bv-notempty="true"'); ?>
                    </div>
                    <div class="form-group">
                        <label><?= lang('state', 'state') ?></label>
                        <?php echo form_input('state', set_value('state', ''), 'class="form-control tip" id="state" data-bv-notempty="true"'); ?>
                    </div>
                    <div class="form-group">
                        <label><?= lang('country', 'country') ?></label>
                        <?php echo form_input('country', set_value('country', ''), 'class="form-control tip" id="country" data-bv-notempty="true"'); ?>
                    </div>
                    <div class="form-group">
                        <label><?= lang('zipcode', 'zipcode') ?></label>
                        <?php echo form_input('zipcode', set_value('zipcode', ''), 'class="form-control tip" id="zipcode" data-bv-notempty="true" data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Only Numbers Allowed" 
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="6"
                               data-bv-stringlength-max="6"
                               data-bv-stringlength-message="The Zip Code must be exact 6 numbers long"'); ?>
                    </div>
                    <div class="form-group">
                        <label><?= lang('telephoneno', 'telephoneno') ?></label>
                        <?php echo form_input('telephoneno', set_value('telephoneno', ''), 'class="form-control tip" id="telephonenos" data-bv-notempty="false" data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Only Numbers Allowed" 
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="10"
                               data-bv-stringlength-max="10"
                               data-bv-stringlength-message="The Telephoneno must be exact 10 numbers long"'); ?>
                    </div>
                    <div class="form-group">
                        <label><?= lang('mobileno', 'mobileno') ?></label>
                        <?php echo form_input('mobileno', set_value('mobileno', ''), 'class="form-control tip" id="mobileno" data-bv-notempty="true" data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Only Numbers Allowed" 
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="10"
                               data-bv-stringlength-max="10"
                               data-bv-stringlength-message="The Mobile No must be exact 10 numbers long"'); ?>
                    </div>
                    <div class="form-group">
                        <label><?= lang('email', 'email') ?></label>
                        <?php // echo form_input('email', set_value('email', ''), 'class="form-control tip" id="email" data-bv-notempty="true"'); ?>
                        <input type="email" name="email" class="form-control" value="<?= set_value('email', '') ?>" required="required" id="email"/>
                    </div>

                </div><!--end Shipping address----->
                <div class="tab-pane" id="2a" style="padding:0 22px !important;">

                    <div class="form-group">
                        <label><?= lang('address1', 'billaddressline1') ?></label>
                        <?php echo form_input('billaddressline1', set_value('addressline1', ''), 'class="form-control tip" required="required" id="billaddressline1" data-bv-notempty="true"'); ?>
                    </div>
                    <div class="form-group">
                        <label><?= lang('address2', 'address2') ?></label>
                        <?php echo form_input('billaddressline2', set_value('addressline2', ''), 'class="form-control tip"  id="billaddressline2" data-bv-notempty="false"'); ?>
                    </div>
                    <div class="form-group">
                        <label><?= lang('city', 'city') ?></label>
                        <?php echo form_input('billcity', set_value('city', ''), 'class="form-control tip" id="billcity" required="required" data-bv-notempty="true"'); ?>
                    </div>
                    <div class="form-group">
                        <label><?= lang('state', 'state') ?></label>
                        <?php echo form_input('billstate', set_value('state', ''), 'class="form-control tip" id="billstate" required="required" data-bv-notempty="true"'); ?>
                    </div>
                    <div class="form-group">
                        <label><?= lang('country', 'country') ?></label>
                        <?php echo form_input('billcountry', set_value('country', ''), 'class="form-control tip" id="billcountry" required="required" data-bv-notempty="true"'); ?>
                    </div>
                    <div class="form-group">
                        <label><?= lang('zipcode', 'zipcode') ?></label>
                        <?php echo form_input('billzipcode', set_value('zipcode', ''), 'class="form-control tip" id="billzipcode" required="required" data-bv-notempty="true" 
                            data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Only Numbers Allowed" 
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="6"
                               data-bv-stringlength-max="6"
                               data-bv-stringlength-message="The Zip Code must be exact 6 numbers long"'); ?>
                    </div>
                    <div class="form-group">
                        <label><?= lang('telephoneno', 'telephoneno') ?></label>
                        <?php echo form_input('billtelephoneno', set_value('telephoneno', ''), 'class="form-control tip" required="required" id="billtelephoneno" data-bv-notempty="false"
                                data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Only Numbers Allowed" 
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="10"
                               data-bv-stringlength-max="10"
                               data-bv-stringlength-message="The Telephoneno must be exact 10 numbers long"'); ?>
                    </div>
                    <div class="form-group">
                        <label><?= lang('mobileno', 'mobileno') ?></label>
                        <?php echo form_input('billmobileno', set_value('mobileno', ''), 'class="form-control tip" id="billmobileno" required="required"  data-bv-notempty="true" 
                            data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Only Numbers Allowed" 
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="10"
                               data-bv-stringlength-max="10"
                               data-bv-stringlength-message="The Mobile No must be exact 10 numbers long"'); ?>
                    </div>
                    <div class="form-group">
                        <label><?= lang('email', 'billemail') ?></label>
                        <?php // echo form_input('billemail', set_value('email', ''), 'class="form-control tip" id="billemail" required="required" data-bv-notempty="true"'); ?>
                        <input type="email" name="billemail" class="form-control" value="<?= set_value('billemail', '') ?>" required="required" id="billemail"/>
                    </div>
                </div><!--end billing address-----> 
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border">Credit limit</legend>

                    <div class="col-sm-12">
                        <div class="checkbox">
                            <label><input type="checkbox" name="enrolllimit" id="enrolllimit"  value="1">Use System Default Credit Limit</label>
                        </div>
                        <br>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-6"><?= lang('creditlimit', 'creditlimit') ?></label>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-sm-12">
                            <?php echo form_input('creaditlimit', '0.00', 'class="form-control tip" disabled="disabled" id="creaditlimit" data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-6  pd-0"><?= lang('opening_balance', 'opening_balance') ?></label>
                        <div class="col-sm-6">
                            <?php echo form_input('opening_balance', set_value('opening_balance', ''), 'class="form-control tip" id="opening_balance" data-bv-notempty="true"'); ?>
                        </div>
                    </div>

                    <!--                    <div class="form-group">
                                            <label class="control-label col-sm-4"><?= lang('fbalance', 'fbalance') ?></label>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-sm-12">
                    <?php echo form_input('fabalance', set_value('fabalance', ''), 'class="form-control tip" disabled="disabled" id="fabalance" data-bv-notempty="true"'); ?>
                                            </div>
                                        </div>-->

                </fieldset>



            </div>



        </div><!--end second col-4--->

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
            <div class="row">
                <div class="form-group" style="padding:0 20px;">
                    <?= lang("photo", "biller_logo"); ?> *

                    <input id="store_logo_up" name="photo" type="file"  class="form-control" 
                           data-show-upload="false"
                           data-show-preview="false" 
                           data-bv-file-maxsize="300*100" 
                           />
                </div>
            </div>
            <br>
            <div class="form-group" style="padding:0 16px;">
                <input type="hidden" name="supplierPhoto" id="supplierphoto" value="<?= set_value('supplierPhoto', '') ?>">
                <div id="my_camera" style="border:1px solid #000000;background:#eee; width: 100%; height: 500px; display: none;"  width="100%" height="500px"></div>
                <img src="" id="image_upload_preview" style="border:1px solid #000000;background:#eee" width="100%" height="500px">
                <div id="results"></div>
            </div>
            <br><center><button type="button" id="takepicture" style="">Take Picture with Attached Camera</button>
                <input type=button value="Take Snapshot" id="tacksnaps" onClick="take_snapshot()" style="display: none"></center>
            <br>

            <div class="form-group">
                <label class="control-label col-sm-5"><?= lang("created", "created"); ?></label>
                <div class="col-sm-7">
                    <?php
                    $custstore[''] = "";
                    foreach ($user as $usr) {
                        $us[$usr->id] = $usr->first_name != '-' ? $usr->first_name : $usr->first_name;
                    }
                    echo form_dropdown('store', $us, '', 'class="form-control select" id="created" placeholder="' . lang("select") . " " . lang("username") . '"')
                    ?>
                </div>
            </div>	
            <!--                <div class="form-group">
                                <label class="control-label col-sm-5">Created By :</label>
                                <div class="col-sm-7">
                                    <select class="form-control form-crtl">
                                        <option></option>
                                        <option> 2</option>
                                        <option>3</option>
            
                                    </select>
                                </div>
            
                            </div>-->

            <!-- <div class="form-group">
                <label class="control-label col-sm-5"><?= lang('csd', 'csd') ?></label>
                <div class="col-sm-7">
                    <?php echo form_input('sincedate', set_value('sincedate', ''), 'class="form-control tip date" id="csd" data-bv-notempty="true"'); ?>
                </div>

            </div> -->


            <!-- <fieldset class="scheduler-border">
                <legend class="scheduler-border"><?= lang("cg") ?></legend>
                <label class="control-label col-sm-5"><?= lang("cg", 'cg') ?></label>
                <div class="col-sm-7">
                    <?php
                    $groupcust[''] = "";
                    foreach ($custgroup as $row) {
                        $groupcust[$row->id] = $row->name;
                    }
                    echo form_dropdown('customer_groups', $groupcust, '', 'class="form-control select" id="cg" placeholder="' . lang("select") . " " . lang("customer_groups") . '"')
                    ?>

                </div>
            </fieldset> -->


        </div><!--end third col-4--->



        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <?= lang('attachment', 'attachment') ?>
            <div calss="form-group">
                <input id="attachment" name="attachment[]"  multiple type="file" class="form-control" 
                       data-show-upload="false"
                       data-show-preview="false" 
                       data-bv-file-maxsize="300*100"/>
            </div>	
        </div>




    </div>
    <div class="row text-center">
        <?php echo form_submit('add_customer', lang('save'), 'class="btn btn-primary"'); ?>
        <!--<button type="button" class="btn btn-danger"  onclick="this.form.reset();">Cancel</button>-->  	
        <a href="<?php echo base_url('customers') ?>"><button type="button" class="btn btn-danger">Cancel</button>  	
    </div>
    <?php echo form_close(); ?>
</div><!--end container fluid----->
<script>

    $(document).on('ifUnchecked', '#enrolllimit', function (e) {
        $('#creaditlimit').attr('disabled', "disabled");
        $('#fabalance').attr('disabled', "disabled");
        $('#creaditlimit').attr('required', 'required');
        $('#fabalance').attr('required', 'required');
    });
    $(document).on('ifChecked', '#enrolllimit', function (e) {
        $('#creaditlimit').removeAttr("disabled");
        $('#fabalance').removeAttr("disabled");
        $('#creaditlimit').removeAttr('required');
        $('#fabalance').removeAttr('required');
    });

    $(document).on('ifUnchecked', '#isloyatlity', function (e) {
        $('#loyalityno').attr('disabled', "disabled");
        $('#enrolldate').attr('disabled', "disabled");
        $('#loyalitypoint').attr('disabled', "disabled");
        $('#referby').attr('disabled', "disabled");
        $('#loyalty_percent').attr('disabled', "disabled");

        $('#loyalityno').attr('required', 'required');
        $('#enrolldate').attr('required', 'required');
        $('#loyalitypoint').attr('required', 'required');
        $('#referby').attr('required', 'required');

    });
    $(document).on('ifChecked', '#isloyatlity', function (e) {
        $('#loyalityno').removeAttr("disabled");
        $('#enrolldate').removeAttr("disabled");
        $('#loyalitypoint').removeAttr("disabled");
        $('#referby').removeAttr("disabled");
        $('#creaditlimit').removeAttr('required');
        $('#enrolldate').removeAttr('required');
        $('#loyalty_percent').removeAttr("disabled");

        $('#loyalitypoint').removeAttr('required');
        $('#referby').removeAttr('required');
    });

    $('#store_logo_up').fileinput({
        maxImageWidth: 300,
        maxImageHeight: 100
    });
    $('#attachment').fileinput({
        maxImageWidth: 300,
        maxImageHeight: 100
    });

//    function enableEdit() {
//        $('#customercode').removeAttr('readonly');
//    }
    $(document).ready(function () {
        $.ajax({
            type: "get",
            async: false,
            url: "<?= site_url('suppliers/getNewSupplierCode') ?>",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $('#customercode').val('CUST' + data.id);
                }
            },
            error: function () {
                bootbox.alert('<?= lang('ajax_error') ?>');
                $('#modal-loading').hide();
            }
        });
    });

    $("#autogenerate").click(function () {
        $.ajax({
            type: "get",
            async: false,
            url: "<?= site_url('suppliers/getNewSupplierCode') ?>",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $('#loyalityno').val('GC00' + data.id + '@10');
                }
            },
        });
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            $("#supplierphoto").val('');
        }
    }

    $("#store_logo_up").change(function () {
        readURL(this);
    });
    $("#takepicture").click(function () {
        $('#my_camera').css('display', 'inline-block');
        $('#tacksnaps').css('display', 'inline-block');
        $('#image_upload_preview').css('display', 'none');
        $('#takepicture').css('display', 'none');
    });
</script>

<script language="JavaScript">
    Webcam.set({
        width: 400,
        height: 330,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
    Webcam.attach('#my_camera');
</script>
<script language="JavaScript">
    function take_snapshot() {
        // take snapshot and get image data
        Webcam.snap(function (data_uri) {
            // display results in page

//            document.getElementById('results').innerHTML =
//                    '<h2>Processing:</h2>';
            readURL("input[name='webcam']");

            Webcam.upload(data_uri, 'suppliers/savecamImage', function (code, text) {
                $("#image_upload_preview").attr("src", 'assets/uploads/' + text);
                $("#supplierphoto").val(text);
                $('#my_camera').css('display', 'none');
                $('#image_upload_preview').css('display', 'inline-block');
                $('#takepicture').css('display', 'inline-block');
                $('#tacksnaps').css('display', 'none');
            });
        });
    }
</script>
