<style>
    .pd-l-20{
        padding-left: 10px;
    }
    .pd-l-40{
        padding-left: 20px;
    }
</style>
<div class="modal-dialog">
    <div class="modal-content" style=" overflow-y:scroll;height:700px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h2 class="modal-title text-center" id="myModalLabel"><?php echo $Settings->site_name; ?></h2>
            
            <br>
            <h2 class="modal-title text-center" id="myModalLabel"><?= lang('year_report'); ?></h2>
             <br><p class="text-center">For <?= $date; ?></p>
        </div>

        <div class="modal-body" style="width: 50%;float: left;background-color: #fff;border-right: 1px solid black">
            <table width="100%" class="stable ">
                <?php
                $c = count($report);
                $x = 0;
                for ($i = 0; $i < $c; $i++) {
                    $cd = count($report[$i]);
                    ?>
                    <tr>
                        <!--<td style="border-bottom: 1px solid #EEE;"><h4><?= lang('products_sale'); ?>:</h4></td>-->
                        <td style="border-bottom: 1px solid #EEE;" class="text-uppercase"><h4>
                                <span><b><?php echo $report[$i][0]->depart_name; ?></b></span></h4>
                        </td>
                        <td style="border-bottom: 1px solid #EEE;"><h4>
                                <span style="padding-left: 32px;"><b><?php echo $report[$i][0]->g_total; ?></b></span></h4>
                        </td>
                    </tr>
                    <?php for ($j = 0; $j < $cd; $j++) { ?>
                        <tr>
                            <td style="border-bottom: 1px solid #EEE;padding-left: 32px;"><h4><?= $report[$i][$j]->name; ?>:</h4></td>
                            <td style="border-bottom: 1px solid #EEE;padding-left: 32px;"><h4><?= $report[$i][$j]->subtotal; ?>:</h4></td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </table>
        </div>

        <div class="modal-body" style="width: 50%; float: right;background-color: #fff;height:600px;">
            <?php
                        
            ?>
            <table width="100%" class="stable">
                <tr>
                    <td style="border-bottom: 1px solid #EEE;">
                        <h4><span class="text-uppercase "><b>1. <?= lang('total_sale'); ?>:</b></span></h4>
                    </td>
                    <td style="border-bottom: 1px solid #EEE;" >
                        <h4><span style="padding-left: 32px;float: right"><b><?php if(empty($total_sale)){echo 0;}else{ echo $total_sale;} ?></b></span></h4>
                    </td>
                </tr>
<!--                <tr>
                    <td style="border-bottom: 1px solid #EEE;">
                        <h3><span class="pd-l-20 black font16"><?= lang('sale_on_cash'); ?>:</span></h3>
                    </td>
                    <td style="border-bottom: 1px solid #EEE;" >
                        <h4><span style="padding-left: 32px;float: right"><?php if(empty($sale_type_wise[0][0]->sumamount)){echo 0;}else{echo $sale_type_wise[0][0]->sumamount;} ?></span></h4>
                    </td>
                </tr>         
                <tr>
                    <td style="border-bottom: 1px solid #EEE;">
                        <h3><span class="pd-l-20 black"><?= lang('sale_on_card'); ?>:</span></h3>
                    </td>
                    <td style="border-bottom: 1px solid #EEE;" >
                        <h4><span style="padding-left: 32px;float: right"><?php if(empty($sale_cc_wise['total_card_sale'])){echo 0;}else{ echo $sale_cc_wise['total_card_sale']; }?></span></h4>
                    </td>
                </tr>  -->

                <!--?>-->
<!--                 <tr>
                    <td style="border-bottom: 1px solid #EEE;">
                        <h3><span class="pd-l-20 black"><?= lang('sale_returned'); ?>:</span></h3>
                    </td>
                    <td style="border-bottom: 1px solid #EEE;" >
                        <h4><span style="padding-left: 32px;float: right"><?php if(empty($sale_return)){echo 0;}else{echo $sale_return;} ?></span></h4>
                    </td>
                </tr> 
                      <tr>
                    <td style="border-bottom: 1px solid #EEE;">
                        <h3><span class="pd-l-20 black"><?= lang('sale_on_other'); ?>:</span></h3>
                    </td>
                    <td style="border-bottom: 1px solid #EEE;" >
                        <h4><span style="padding-left: 32px;float: right"><?php if(empty($sale_type_wise[1][0]->sumamount)){echo 0;}else{echo $sale_type_wise[1][0]->sumamount;} ?></span></h4>
                    </td>
                </tr> -->
                 <tr>
                    <td style="border-bottom: 1px solid #EEE;">
                        <h4><span class="text-uppercase "><b>2. <?= lang('total_expences'); ?>:</b></span></h4>
                    </td>
                    <td style="border-bottom: 1px solid #EEE;" >
                        <h4><span style="padding-left: 32px;float: right"><b><?php if(empty($sale_expences)){echo 0;}else{ echo $sale_expences;} ?></b></span></h4>
                    </td>
                </tr>
                      <tr>
                    <td style="border-bottom: 1px solid #EEE;">
                        <h4><span class="text-uppercase "><b>3. <?= lang('total_net_profit'); ?>:</b></span></h4>
                    </td>
                    <td style="border-bottom: 1px solid #EEE;" >
                        <h4><span style="padding-left: 32px;float: right"><b><?= $this->sma->formatMoney($total_sales->total_amount - $total_purchases_pf_loss->total_amount) ?></b></span></h4>
                    </td>
                </tr>

                 <tr>
                    <td style="border-bottom: 1px solid #EEE;">
                        <h4><span class="text-uppercase "><b>4. <?= lang('total_purchase'); ?>:</b></span></h4>
                    </td>
                    <td style="border-bottom: 1px solid #EEE;" >
                        <h4><span style="padding-left: 32px;float: right"><b><?php if(empty($total_purchase->total_amount)){echo 0;}else{ echo $total_purchase->total_amount;} ?></b></span></h4>
                    </td>
                </tr>
                 <tr>
                    <td style="border-bottom: 1px solid #EEE;">
                        <h4><span class="text-uppercase "><b>5. <?= lang('total_payment'); ?>:</b></span></h4>
                    </td>
                    <td style="border-bottom: 1px solid #EEE;" >
                        <h4><span style="padding-left: 32px;float: right"><b><?php if(empty($total_purchase->paid)){echo 0;}else{ echo $total_purchase->paid;} ?></b></span></h4>
                    </td>
                </tr>
                 <tr>
                    <td style="border-bottom: 1px solid #EEE;">
                        <h4><span class="text-uppercase "><b>6. <?= lang('cash_in_hand'); ?>:</b></span></h4>
                    </td>
                    <td style="border-bottom: 1px solid #EEE;" >
                        <h4><span style="padding-left: 32px;float: right"><b><?= $this->sma->formatMoney($this->session->userdata('cash_in_hand')); ?></b></span></h4>
                    </td>
                </tr>
           
            </table>
        </div>

    </div>
</div>
