<style>
    .pd-l-20{
        padding-left: 10px;
    }
    .pd-l-40{
        padding-left: 20px;
    }
</style>
<div class="modal-dialog">
    <div class="modal-content" style=" overflow-y:scroll;height:700px;">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h2 class="modal-title text-center" id="myModalLabel"><?php echo $Settings->site_name; ?></h2>
            <br>
            <h2 class="modal-title text-center"><?= lang('day_report'); ?></h2>
            <br><p class="text-center">For <?= $this->sma->hrsd($date); ?></p>
        </div>

        <div class="modal-body" style="width: 50%;float: left;background-color: #fff;border-right: 1px solid black">
            <table width="100%" class="stable ">
                <?php
                $c = count($report);
                $x = 0;
                for ($i = 0; $i < $c; $i++) {
                    $cd = count($report[$i]);
                    ?>
                    <tr>
                        <!--<td style="border-bottom: 1px solid #EEE;"><h4><?= lang('products_sale'); ?>:</h4></td>-->
                        <td style="border-bottom: 1px solid #EEE;" class="text-uppercase"><h4>
                                <span><b><?php echo $report[$i][0]->depart_name; ?></b></span></h4>
                        </td>
                        <td style="border-bottom: 1px solid #EEE;"><h4>
                                <span style="padding-left: 32px;"><b><?php echo $report[$i][0]->g_total; ?></b></span></h4>
                        </td>
                    </tr>
                    <?php for ($j = 0; $j < $cd; $j++) { ?>
                        <tr>
                            <td style="border-bottom: 1px solid #EEE;padding-left: 32px;"><h4><?= $report[$i][$j]->name; ?>:</h4></td>
                            <td style="border-bottom: 1px solid #EEE;padding-left: 32px;"><h4><?= $report[$i][$j]->subtotal; ?>:</h4></td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </table>
        </div>

        <div class="modal-body" style="width: 50%; float: right;background-color: #fff;">
            <table width="100%" class="stable">
                <tr>
                    <td style="border-bottom: 1px solid #EEE;">
                        <h4><span class="text-uppercase "><b><?= lang('total_sale'); ?>:</b></span></h4>
                    </td>
                    <td style="border-bottom: 1px solid #EEE;" >
                        <h4><span style="padding-left: 32px;float: right"><b><?php if(empty($total_sale)){echo 0;}else{ echo $total_sale;} ?></b></span></h4>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #EEE;">
                        <h3><span class="pd-l-20 black font16"><?= lang('sale_on_cash'); ?>:</span></h3>
                    </td>
                    <td style="border-bottom: 1px solid #EEE;" >
                        <h4><span style="padding-left: 32px;float: right"><?php if(empty($sale_type_wise[0][0]->sumamount)){echo 0;}else{echo $sale_type_wise[0][0]->sumamount;} ?></span></h4>
                    </td>
                </tr>         
                <tr>
                    <td style="border-bottom: 1px solid #EEE;">
                        <h3><span class="pd-l-20 black"><?= lang('sale_on_card'); ?>:</span></h3>
                    </td>
                    <td style="border-bottom: 1px solid #EEE;" >
                        <h4><span style="padding-left: 32px;float: right"><?php if(empty($sale_cc_wise['total_card_sale'])){echo 0;}else{ echo $sale_cc_wise['total_card_sale']; }?></span></h4>
                    </td>
                </tr>  
                <?php
                foreach($sale_cc_wise as $sale_card){
                    ?>
                    <tr>
                    <td style="border-bottom: 1px solid #EEE;">
                   <h3><span class="pd-l-40">- <?php echo $sale_card->cc_no?>:</span></h3>
                    </td>
                    <td style="border-bottom: 1px solid #EEE;" >
                        <h4><span style="padding-left: 32px;float: right">:<?php if(empty($sale_card->amount)){echo 0;}else{echo $sale_card->amount;}?></span></h4>
                    </td>
                </tr>  
               <?php }

?>
                 <tr>
                    <td style="border-bottom: 1px solid #EEE;">
                        <h3><span class="pd-l-20 black"><?= lang('sale_returned'); ?>:</span></h3>
                    </td>
                    <td style="border-bottom: 1px solid #EEE;" >
                        <h4><span style="padding-left: 32px;float: right"><?php if(empty($sale_return)){echo 0;}else{echo $sale_return;} ?></span></h4>
                    </td>
                </tr> 
                      <tr>
                    <td style="border-bottom: 1px solid #EEE;">
                        <h3><span class="pd-l-20 black"><?= lang('sale_on_other'); ?>:</span></h3>
                    </td>
                    <td style="border-bottom: 1px solid #EEE;" >
                        <h4><span style="padding-left: 32px;float: right"><?php if(empty($sale_type_wise[1][0]->sumamount)){echo 0;}else{echo $sale_type_wise[1][0]->sumamount;} ?></span></h4>
                    </td>
                </tr> 
                 <tr>
                    <td style="border-bottom: 1px solid #EEE;">
                        <h4><span class="text-uppercase "><b><?= lang('total_expences'); ?>:</b></span></h4>
                    </td>
                    <td style="border-bottom: 1px solid #EEE;" >
                        <h4><span style="padding-left: 32px;float: right"><b><?php if(empty($sale_expences)){echo 0;}else{ echo $sale_expences;} ?></b></span></h4>
                    </td>
                </tr>

            </table>
        </div>

    </div>
</div>
