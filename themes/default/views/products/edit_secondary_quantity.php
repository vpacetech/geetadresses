<style>.btton{padding: 5px;width:100%;}
    .form-group {margin-bottom: 6px !important;}
</style>

<div class="box" ng-controller="">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('edit_sec_qty'); ?></h2>
    </div>
    <div class="box-content">
        <?php
        $attrib = array('data-toggle' => 'validator', 'class' => 'form-horizontal', 'role' => 'form');
        echo form_open_multipart("products/editsecondaryqty/" . $sec->id, $attrib);
        ?>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                    <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" >1. Store</label>
                    <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                        <?php
                        $soters = array();
                        $soters[''] = "Select Sotre";
                        foreach ($store as $row) {
                            $soters[$row->id] = $row->name;
                        }
                        echo form_dropdown('store', $soters, $sec->store, 'class="form-control select2 commonselect" data-bv-notempty="true" data-tab="department" disabled="disabled" data-key="store_id" data-id="department" id="store" placeholder="' . lang("select") . ' ' . lang("store") . '"')
                        ?>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                    <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" >2. Department</label>
                    <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                        <?php
                        $dept = array();
                        $dept[''] = "Select Department";
                        foreach ($department as $row) {
                            $dept[$row->id] = $row->name;
                        }
                        echo form_dropdown('department', $dept, $sec->department, 'class="form-control select2 commonselect" disabled="disabled" data-bv-notempty="true" data-tab="product_items" data-key="section_id" data-id="product_item" id="department" placeholder="' . lang("select") . ' ' . lang("department") . '"')
                        ?>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                    <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" >3. Produt Item</label>
                    <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                        <?php
                        $soters = array();
                        $soters[''] = "Select Product Item";
                        foreach ($pro_item as $row) {
                            $soters[$row->id] = $row->name;
                        }
                        echo form_dropdown('product_item', $soters, $sec->product_items, 'class="form-control select2 commonselect" disabled="disabled" data-bv-notempty="true" data-tab="type" data-key="product_items_id" data-id="type" id="product_item" placeholder="' . lang("select") . ' ' . lang("product_item") . '"')
                        ?>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                    <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">4. Type</label>
                    <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                        <?php
                        $soters = array();
                        $soters[''] = "Select Type";
                        foreach ($type as $row) {
                            $soters[$row->id] = $row->name;
                        }
                        echo form_dropdown('type', $soters, $sec->type_id, 'class="form-control select2 commonselect" data-bv-notempty="true" disabled="disabled" data-tab="brands" data-key="type_id" data-id="brand" id="type" placeholder="' . lang("select") . ' ' . lang("type") . '"')
                        ?>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                    <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">5. Brand</label>
                    <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                        <?php
                        $soters = array();
                        $soters[''] = "Select Brand";
                        foreach ($brand as $row) {
                            $soters[$row->id] = $row->name;
                        }
                        echo form_dropdown('brand', $soters, $sec->brands, 'class="form-control select2 commonselect" disabled="disabled" data-tab="design" data-key="brands_id" data-id="design" data-bv-notempty="true" id="brand" placeholder="' . lang("select") . ' ' . lang("brand") . '"')
                        ?>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                    <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">6. Design</label>
                    <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                        <?php
                        $soters = array();
                        $soters[''] = "Select Design";
                        foreach ($design as $row) {
                            $soters[$row->id] = $row->name;
                        }
                        echo form_dropdown('design', $soters, $sec->design, 'class="form-control select2 commonselect" disabled="disabled" data-tab="style" data-key="design_id" data-id="style" data-bv-notempty="true" id="design" placeholder="' . lang("select") . ' ' . lang("brand") . '"')
                        ?>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                    <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">7. Style</label>
                    <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                        <?php
                        $soters = array();
                        $soters[''] = "Select Style";
                        foreach ($style as $row) {
                            $soters[$row->id] = $row->name;
                        }
                        echo form_dropdown('style', $soters, $sec->style, 'class="form-control select2 commonselect" disabled="disabled" data-tab="pattern" data-key="style_id" data-id="pattern" data-bv-notempty="true" id="style" placeholder="' . lang("select") . ' ' . lang("brand") . '"')
                        ?>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                    <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">8. Pattern</label>
                    <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                        <?php
                        $soters = array();
                        $soters[''] = "Select Pattern";
                        foreach ($pattern as $row) {
                            $soters[$row->id] = $row->name;
                        }
                        echo form_dropdown('pattern', $soters, $sec->pattern, 'class="form-control select2 commonselect" disabled="disabled" data-tab="fitting" data-key="pattern_id" data-id="fitting" data-bv-notempty="true" id="pattern" placeholder="' . lang("select") . ' ' . lang("brand") . '"')
                        ?>
                    </div>
                </div>
                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                    <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">9. Fitting</label>
                    <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                        <?php
                        $soters = array();
                        $soters[''] = "Select Fitting";
                        foreach ($fitting as $row) {
                            $soters[$row->id] = $row->name;
                        }
                        echo form_dropdown('fitting', $soters, $sec->fitting, 'class="form-control select2 commonselect" disabled="disabled" data-tab="fabric" data-key="fitting_id" data-id="fabric" data-bv-notempty="true" id="fitting" placeholder="' . lang("select") . ' ' . lang("brand") . '"')
                        ?>
                    </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                    <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">10. Fabric</label>
                    <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                        <?php
                        $soters = array();
                        $soters[''] = "Select Fabric";
                        foreach ($fabric as $row) {
                            $soters[$row->id] = $row->name;
                        }
                        echo form_dropdown('fabric', $soters, $sec->fabric, 'class="form-control select2 " data-tab="design" disabled="disabled" data-key="brands_id" data-id="design" data-bv-notempty="true" id="fabric" placeholder="' . lang("select") . ' ' . lang("brand") . '"')
                        ?>
                    </div>
                </div>
                <!--<div class="row">-->
                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                    <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">10. Quantity</label>
                    <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                        <?php
                        echo form_input('qty', $sec->no_of_pic, 'class="form-control select2 " data-bv-notempty="true" data-bv-stringlength="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="Only Numbers Allowed" id="qty" placeholder="Quantity"')
                  
                            
//                        echo form_input('fabric', $sec->no_of_pic, 'class="form-control" data-bv-notempty="true" id="quantity" 
//                               data-bv-regexp="true"
//                               data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Only Numbers Allowed" placeholder="' . lang("select") . ' ' . lang("brand") . '"')
                        ?>
                    </div>
                </div>
            </div>
            
            <div class="row text-center">
                <?php echo form_submit('editqty', lang('save'), 'class="btn btn-primary"'); ?>
                <!--<button type="button" class="btn btn-danger"  onclick="this.form.reset();">Cancel</button>-->  	
                <a href="<?php echo base_url('products/secondary_quantity') ?>"><button type="button" class="btn btn-danger">Cancel</button>  	
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>



    <script>

        $('#department, #product_item').change(function () {

        var department = $('#department').val();
                var pro_item = $('#product_item').val();
                if (department != "" && pro_item != "") {
        $.ajax({
        async: false,
                data: {
                department: department,
                        pro_item: pro_item
                },
                type: 'GET',
                url: site.base_url + "common/getSizebyid",
                dataType: "json",
                success: function (scdata) {
                if (scdata) {
                $('#size').html('');
                        $('#size').append('<option value="">Please select size</option>');
                        $.each(scdata, function (k, val) {
                        var opt = $('<option />');
                                opt.val(val.id);
                                opt.text(val.name);
                                $('#size').append(opt);
                        })
                        $('#modal-loading').hide();
                } else {
                $('#size').html('');
    //                    $('#' + a).append('<option value="">Against Order no is not Department</option>');
                }
                },
                error: function () {
                $('#modal-loading').hide();
                }
        });
        }
        });
    </script>