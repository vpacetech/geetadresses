<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('add_product'); ?></h2>
    </div>
    <div class="box-content" >
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?php echo lang('enter_info'); ?></p>
                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'addproduct');
                echo form_open_multipart("products/addNew", $attrib)
                ?>
                <div class="col-md-5">
                    <!-- <div class="form-group" ng-init="prod.protype = '<?= (isset($_POST['type']) ? $_POST['type'] : ($product ? $product->type : '')) ?>'">
                        <?= lang("product_type", "type") ?>
                        <?php
                        // if ($product_id != "") {
                        //     $opts = array('standard' => lang('standard'), 'combo' => lang('combo'), 'bundle' => lang('bundle'));
                        // } else {
                        //     $opts = array('standard' => lang('standard'));
                        // }
                        // echo form_dropdown('type', $opts, (isset($_POST['type']) ? $_POST['type'] : ($product ? $product->type : '')), 'class="form-control" id="type" required="required"');
                        echo form_dropdown('type', $opts, (isset($_POST['type']) ? $_POST['type'] : ($product ? $product->type : '')), 'prompt="Select Type" class="form-control" id="type" required="required"');
                        ?>
                    </div> -->
                    

                    <div class="form-group all">
                        <?= lang("Store", "companies") ?>
                        <div class="input-group">
                            <?php
                            echo form_hidden('type', "standard");
                            echo form_dropdown('store_id', $product_para['stores'], (isset($_POST['store_id']) ? $_POST['store_id'] : ($product ? $product->store_id : '')), 'class="form-control" required="required"');
                            // echo form_input('store_id', (isset($_POST['store_id']) ? $_POST['store_id'] : ($product ? $product->store_id : '')), 'class="form-control" default-attrib data-tab="companies" data-id="0" id="companies" placeholder="' . lang("select") . " " . lang("store") . '" required="required" style="width:100%"');
                            ?>
                            <div class="input-group-addon no-print">
                                <a href="<?php echo site_url('billers/add'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group all" >
                             <?= lang("Department", "department") ?>
                        <div class="input-group">
                            <?php
                            echo form_dropdown('department', $product_para['departments'], (isset($_POST['store_id']) ? $_POST['store_id'] : ($product ? $product->store_id : '')), 'class="form-control" required="required"');
                            // echo form_input('department', (isset($_POST['department']) ? $_POST['department'] : ($product ? $product->department : '')), 'class="form-control" default-attrib data-tab="department" data-key="store_id" data-id="companies" id="department" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%"');
                            ?>
                            <div class="input-group-addon no-print">
                                <a href="<?php echo site_url('system_settings/AddProduct_para/department'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group all" >
                             <?= lang("Product_items", "product_items") ?>
                        <div class="input-group">
                            <?php
                            echo form_dropdown('product_items', $product_para['product_items'], (isset($_POST['store_id']) ? $_POST['store_id'] : ($product ? $product->store_id : '')), 'class="form-control" required="required"');
                            // echo form_input('product_items', (isset($_POST['product_items']) ? $_POST['product_items'] : ($product ? $product->product_items : '')), 'class="form-control" default-attrib data-tab="product_items" data-key="department_id" data-id="department" id="product_items" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%" ng-model="prod.product_item" ng-change="getProdName();getProductMargin();getGstDetails();"');
                            ?>
                            <div class="input-group-addon no-print">
                                <a href="<?php echo site_url('system_settings/AddProduct_para/product_items'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group all" ng-init="prod.type = '<?= (isset($_POST['type_id']) ? $_POST['type_id'] : ($product ? $product->type_id : '')) ?>';
                                    getProdBarcode()">
                             <?= lang("Type", "type_id") ?>
                        <div class="input-group">
                            <?php
                            echo form_dropdown('type_id', $product_para['types'], "", 'class="form-control" required="required"');
                            // echo form_input('type_id', (isset($_POST['type_id']) ? $_POST['type_id'] : ($product ? $product->type_id : '')), 'class="form-control" default-attrib data-tab="type" id="type_id" data-key="product_items_id" data-id="product_items" placeholder="' . lang("select") . " " . lang("type") . '" required="required" style="width:100%" ng-model="prod.type" ng-change="getProdName();getProductMargin();getGstDetails();"');
                            ?>
                            <div class="input-group-addon no-print">
                                <a href="<?php echo site_url('system_settings/AddProduct_para/type'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group all" ng-init="prod.brands_id = '<?= (isset($_POST['brands']) ? $_POST['brands'] : ($product ? $product->brands : '')) ?>';
                                    getProdBarcode();">
                             <?= lang("Brands", "brands") ?>
                        <div class="input-group">
                            <?php
                            echo form_dropdown('brands', $product_para['brands'], (isset($_POST['store_id']) ? $_POST['store_id'] : ($product ? $product->store_id : '')), 'class="form-control" required="required"');
                            // echo form_input('brands', (isset($_POST['brands']) ? $_POST['brands'] : ($product ? $product->brands : '')), 'class="form-control" default-attrib data-tab="brands" id="brands" data-key="type_id" data-id="type_id" placeholder="' . lang("select") . " " . lang("brands") . '" required="required" ng-model="prod.brands_id" ng-change="getProductMargin();getGstDetails();"  style="width:100%"');
                            ?>
                            <div class="input-group-addon no-print">
                                <a href="<?php echo site_url('system_settings/AddProduct_para/brands'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group standard" >
                        <?= lang("Design", "design") ?>
                        <div class="input-group">
                            <?php
                            echo form_dropdown('design', $product_para['designs'], (isset($_POST['store_id']) ? $_POST['store_id'] : ($product ? $product->store_id : '')), 'class="form-control" required="required"');
                            // echo form_input('design', (isset($_POST['design']) ? $_POST['design'] : ($product ? $product->design : '')), 'class="form-control" default-attrib data-tab="design" id="design" data-key="brands_id" data-id="brands" placeholder="' . lang("select") . " " . lang("design") . '" style="width:100%" ng-model="prod.design" ');
                            ?>
                            <div class="input-group-addon no-print">
                                <a href="<?php echo site_url('system_settings/AddProduct_para/design'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group standard">
                        <?= lang("Style", "style") ?>
                        <div class="input-group">
                            <?php
                            echo form_dropdown('style', $product_para['styles'], (isset($_POST['store_id']) ? $_POST['store_id'] : ($product ? $product->store_id : '')), 'class="form-control" required="required"');
                            // echo form_input('style', (isset($_POST['style']) ? $_POST['style'] : ($product ? $product->style : '')), 'class="form-control" default-attrib data-tab="style" id="style" data-key="design_id" data-id="design" placeholder="' . lang("select") . " " . lang("style") . '" style="width:100%" ng-model="prod.style"');
                            ?>
                            <div class="input-group-addon no-print">
                                <a href="<?php echo site_url('system_settings/AddProduct_para/style'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group standard">
                        <?= lang("Pattern", "pattern") ?>
                        <div class="input-group">
                            <?php
                            echo form_dropdown('pattern', $product_para['patterns'], (isset($_POST['store_id']) ? $_POST['store_id'] : ($product ? $product->store_id : '')), 'class="form-control" required="required"');
                            // echo form_input('pattern', (isset($_POST['pattern']) ? $_POST['pattern'] : ($product ? $product->pattern : '')), 'class="form-control" default-attrib data-tab="pattern" data-key="style_id" data-id="style" id="pattern" placeholder="' . lang("select") . " " . lang("pattern") . '" style="width:100%" ng-model="prod.pattern"');
                            ?>
                            <div class="input-group-addon no-print">
                                <a href="<?php echo site_url('system_settings/AddProduct_para/pattern'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group standard">
                        <?= lang("Fitting", "fitting") ?>
                        <div class="input-group">
                            <?php
                            echo form_dropdown('fitting', $product_para['fittings'], (isset($_POST['store_id']) ? $_POST['store_id'] : ($product ? $product->store_id : '')), 'class="form-control" required="required"');
                            // echo form_input('fitting', (isset($_POST['fitting']) ? $_POST['fitting'] : ($product ? $product->fitting : '')), 'class="form-control" default-attrib data-tab="fitting" data-key="pattern_id" data-id="pattern" id="fitting" placeholder="' . lang("select") . " " . lang("fitting") . '" style="width:100%" ng-model="prod.fitting"');
                            ?>
                            <div class="input-group-addon no-print">
                                <a href="<?php echo site_url('system_settings/AddProduct_para/fitting'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group standard">
                        <?= lang("Fabric", "fabric") ?>
                        <div class="input-group">
                            <?php
                            echo form_dropdown('fabric', $product_para['fabrics'], (isset($_POST['store_id']) ? $_POST['store_id'] : ($product ? $product->store_id : '')), 'class="form-control" required="required"');
                            // echo form_input('fabric', (isset($_POST['fabric']) ? $_POST['fabric'] : ($product ? $product->fabric : '')), 'class="form-control" default-attrib data-tab="fabric" id="fabric" data-key="fitting_id" data-id="fitting" placeholder="' . lang("select") . " " . lang("fabric") . '" style="width:100%" ng-model="prod.fabric"');
                            ?>
                            <div class="input-group-addon no-print">
                                <a href="<?php echo site_url('system_settings/AddProduct_para/fabric'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="form-group standard">
                        <?= lang("Color", "color") ?>
                        <div class="row">
                            <div class="col-md-4"> &nbsp;&nbsp;<input type="radio" name="colortype" ng-model="prod.colortype" icheck value="Single"/>&nbsp;&nbsp;&nbsp;&nbsp;Single</div>
                            <div class="col-md-4  colorsingle hide">
                                <div class="input-group">
                                    <?= form_input('colorsingle', (isset($_POST['colorsingle']) ? $_POST['colorsingle'] : ($product ? $product->color : '')), 'class="form-control" id="color" placeholder="' . lang("select") . " " . lang("color") . '" default-attrib data-tab="color"  style="width:100%" '); ?>
                                    <?php // echo form_dropdown('colorsingle', $color, (isset($_POST['colorsingle']) ? $_POST['colorsingle'] : ($product ? $product->color : '')), 'class="form-control" select2 id="color" placeholder="' . lang("select") . " " . lang("color") . '" style="width:100%"');  ?>
                                    <div class="input-group-addon no-print ">
                                        <a href="<?php echo site_url('system_settings/AddProduct_para/color'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal2" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 colorsingle hide" >
                                <input type="text" id="colorqty" name="colorqty" onlyno class="form-control" ng-model="prod.colorqty" ng-change="getQty('#colorqty')"/>
                            </div>
                            <div class="col-md-2 colorsingle hide" style="padding: 0;">
                                <input type="text" id="colorcode" name="colorcode[]" onlyno readonly class="form-control" ng-model="prod.colorcode" ng-change="getQty('#colorqty')"/>
                                <input type="text" id="codet" name="codet" onlyno class="hidden form-control"/>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-4"> &nbsp;&nbsp;<input type="radio" name="colortype" ng-model="prod.colortype" icheck value="Assorted"/>&nbsp;&nbsp;&nbsp;&nbsp;Assorted</div>
                            <div class="col-md-8  colorassorted hide">
                                <div class="row">
                                    <?php
                                    echo form_dropdown('colorassorted[]', $color, (isset($_POST['colorassorted']) ? $_POST['colorassorted'] : ($product ? $product->colorassorted : '')), 'class="form-control" multiple select2 id="mulcolor" ng-model="prod.colorassorted" placeholder="' . lang("select") . " " . lang("color") . '"style="width:100%"');
                                    ?>
                                </div>

                                <br/>
                                <div class="row" style="padding-top: 40px;">
                                    <div class="col-md-3 from-group" style="padding-right: 0px; margin-bottom: 5px" ng-repeat="n in prod.colorassoarr">
                                        <input type="text" name="colorqty[]" onlyno ng-model="n.qty" ng-blur="getQtyCal($index, n.qty)" class="form-control"/>
                                    </div>
                                </div>
                                <div class="row" style="padding-top: 2s0px;">
                                    <div class="col-md-3 from-group" style="padding-right: 0px; margin-bottom: 5px" ng-repeat="n in prod.colorassoarr">
                                        <input type="text" name="colorcode[]" onlyno ng-model="n.colorcode" readonly class="form-control"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!--{{prod}}-->
                    <div class="form-group">
                        <?= lang("Size", "size") ?>
                        <div class="row">
                            <div class="col-md-4"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="sizeangle" ng-model="prod.sizeangle" ng-change="getProdName()" <?= ($product && $product->sizeangle == "HT" ? "checked='checked'" : '') ?> value="HT" id="sizeangle_ht"/>&nbsp;&nbsp;&nbsp;&nbsp;Height</div>
                            <div class="col-md-4"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="sizeangle" ng-model="prod.sizeangle" ng-change="getProdName()" <?= ($product && $product->sizeangle == "WT" ? "checked='checked'" : '') ?> value="WT" id="sizeangle_wt" />&nbsp;&nbsp;&nbsp;&nbsp;Width</div>
                            <div class="col-md-4"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="sizeangle" ng-model="prod.sizeangle" ng-change="getProdName()" <?= ($product && $product->sizeangle == "NZ" ? "checked='checked'" : '') ?> value="NZ" id="sizeangle_nz" />&nbsp;&nbsp;&nbsp;&nbsp;No Size</div>
                        </div>
                        <script>
                                    $(document).on('ifChecked', '#sizeangle_nz', function (e) {
                                        $('.sizeangle_nz').css('display', 'none');
                                    });
                                    $(document).on('ifUnchecked', '#sizeangle_nz', function (e) {
                                        $('.sizeangle_nz').css('display', 'block');
                                    });
                        </script>
                        <br/>
                        <div class="row sizeangle_nz" >
                            <div class="col-md-4"> &nbsp;&nbsp;<input icheck id="" type="radio" name="sizetype" ng-model="prod.sizetype" ng-change="getProdName()" checked="" value="Single"/>&nbsp;&nbsp;&nbsp;&nbsp;Single</div>
                            <div class="col-md-5" ng-init='size =<?= json_encode($sizes) ?>' ng-show="prod.sizetype == 'Single'">
                                <div class="col-md-9" style="padding: 0px">
                                    <div class="input-group">
                                        <?php
                                        echo form_input('singlesize', (isset($_POST['singlesize']) ? $_POST['singlesize'] : ($product ? $product->size : '')), 'class="form-control " sizesel data-tab="size" data-key="department_id-product_items_id" data-id="department,product_items" id="size" placeholder="' . lang("select") . " " . lang("size") . '" required="required" style="width:100%" ng-model="prod.singlesize" ng-change="getProdName();"');
                                        ?>
                                        <div class="input-group-addon no-print ">
                                            <a href="<?php echo site_url('system_settings/AddProduct_para/size'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"> {{size[prod.singlesize].code?size[prod.singlesize].code+'\"':""}}</div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <br/>
                        <div class="row standard sizeangle_nz" >
                            <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" name="sizetype" ng-model="prod.sizetype" ng-change="getProdName()" value="Multiple"/>&nbsp;&nbsp;&nbsp;&nbsp;<span>Multiple</span></div>
                            <div class="col-md-4" ng-init='size1 =<?= json_encode($sizes) ?>' ng-show="prod.sizetype == 'Multiple'">
                                <div class="col-md-8" style="padding: 0px"><?php
                                    echo form_input('multisizef', (isset($_POST['multisizef']) ? $_POST['multisizef'] : ''), 'class="form-control " sizesel data-tab="size" data-key="department_id-section_id-product_items_id" data-id="department,section,product_items" id="multisizef" placeholder="' . lang("select") . " " . lang("size") . '" required="required" style="width:100%" ng-model="prod.multisizef" ng-change="getProdName();"');
                                    ?></div>
                                <div class="col-md-2"> {{size1[prod.multisizef].code?size1[prod.multisizef].code+'\"':""}}</div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-1" ng-show="prod.sizetype == 'Multiple'">To</div>
                            <div class="col-md-4" ng-init='size2 =<?= json_encode($sizes) ?>' ng-show="prod.sizetype == 'Multiple'">
                                <div class="col-md-8" style="padding: 0px"><?php
                                    echo form_input('multisizet', (isset($_POST['multisizet']) ? $_POST['multisizet'] : ''), 'class="form-control " sizesel data-tab="size" data-key="department_id-section_id-product_items_id" data-id="department,section,product_items" id="multisizet" placeholder="' . lang("select") . " " . lang("size") . '" required="required" style="width:100%" ng-model="prod.multisizet"');
                                    ?></div>
                                <div class="col-md-2"> {{size2[prod.multisizet].code?size2[prod.multisizet].code+'\"':""}}</div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-md-offset-1">
                    <div class="form-group all" >
                        <?= lang("product_name", "name") ?>
                        <?= form_input('name', (isset($_POST['name']) ? $_POST['name'] : ($product ? $product->name : '')), 'class="form-control" id="name" required="required" readonly ng-model="prod.name"'); ?>
                    </div>
                    <div class="form-group all" >
                        <?= lang("product_code", "code") ?> 
                        <?= form_input('code', (isset($_POST['code']) ? $_POST['code'] : ($product ? $product->code : '')), 'class="form-control" id="code" readonly ng-model="prod.barcode" ') ?>
                        <?php // form_input('code', (isset($_POST['code']) ? $_POST['code'] : ($product ? $product->code : '')), 'class="form-control" id="code"  required="required" readonly ng-model="prod.barcode" ')  ?>
                        <span class="help-block"><?= lang('you_scan_your_barcode_too') ?></span>
                    </div>
                    <?= form_hidden('barcode_symbology', (isset($_POST['barcode_symbology']) ? $_POST['barcode_symbology'] : ($product ? $product->barcode_symbology : 'code128')), 'class="form-control" id="barcode_symbology"  required="required" readonly ') ?>

                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group standard">
                            <?= lang("product_tax", "tax_rate") ?>
                            <?php
                            $tr[""] = "";
                            foreach ($tax_rates as $tax) {
                                $tr[$tax->id] = $tax->name;
                            }
                            echo form_dropdown('tax_rate', $tr, (isset($_POST['tax_rate']) ? $_POST['tax_rate'] : ($product ? $product->tax_rate : $Settings->default_tax_rate)), 'class="form-control select" id="tax_rate" placeholder="' . lang("select") . ' ' . lang("product_tax") . '" style="width:100%"');
                            ?>
                        </div>
                        <div class="form-group standard">
                            <?= lang("tax_method", "tax_method") ?>
                            <?php
                            $tm = array('0' => lang('inclusive'), '1' => lang('exclusive'));
                            echo form_dropdown('tax_method', $tm, (isset($_POST['tax_method']) ? $_POST['tax_method'] : ($product ? $product->tax_method : '')), 'class="form-control select" id="tax_method" placeholder="' . lang("select") . ' ' . lang("tax_method") . '" style="width:100%"');
                            ?>
                        </div>
                    <?php } ?>
                    <?php /* ?> <div class="form-group standard">
                      <?= lang("alert_quantity", "alert_quantity") ?>
                      <div class="input-group"> <?= form_input('alert_quantity', (isset($_POST['alert_quantity']) ? $_POST['alert_quantity'] : ($product ? $this->sma->formatQuantity($product->alert_quantity) : '')), 'class="form-control tip" id="alert_quantity"') ?>
                      <span class="input-group-addon">
                      <input type="checkbox" name="track_quantity" id="track_quantity" value="1" <?= ($product ? (isset($product->track_quantity) ? 'checked="checked"' : '') : 'checked="checked"') ?>>
                      </span>
                      </div>
                      </div>
                     * <?php */ ?>
                    <!-- <div class="form-group standard">
                        <?= lang("supplier", "supplier") ?>
                        <div class="row" id="supplier-con">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php
                                $bl[""] = "";
                                echo form_dropdown('supplier', $bl, (isset($_POST['text']) ? $_POST['text'] : ($product ? $product->supplier1 : '')), 'id="supplier12" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("supplier") . '" required="required" ng-model="prod.supplier" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                    </div> -->
                    

                    <div class="form-group standard">
                        <?= lang("product_image", "product_image") ?>(<?= byte_format($this->Settings->iwidth * $this->Settings->iheight) ?>)
                        <!--<div class="input-group">-->
                            <input id="product_image" type="file" name="product_image" data-show-upload="false"
                                   data-show-preview="false" accept="image/*" class="form-control file">
<!--                            <div class="input-group-addon no-print btn btn-primary" style="border: 0px;">
                                <a href="#myModalCam" id="ModalCam" data-toggle="modal"  class="external"><i class="fa fa-camera fa-2x" id="addIcon"></i></a>
                            </div>
                        </div>-->
                    </div>
                    <div class="form-group camera">
                        <div id="my_camera" style="border:1px solid #000000;background:#eee; width: 100%; display: none" height="200px"></div>
                        <img src="" id="image_upload_preview" style="border:1px solid #000000;background:#eee; width: 100px;">
                        <div id="results"></div>
                    </div>
                    <div class="form-group camera">
                        <center><button type="button" id="takepicture" class="btn btn-primary">Take Picture with Attached Camera</button>
                            <input type=button value="Take Snapshot" id="tacksnaps" class="btn btn-primary" onClick="take_snapshot()" style="display: none"></center>
                    </div>

                    <div class="form-group standard">
                        <?= lang("product_gallery_images", "images") ?>(<?= byte_format($this->Settings->iwidth * $this->Settings->iheight) ?>)
                        <input id="images" type="file" name="userfile[]" multiple="true" data-show-upload="false"
                               data-show-preview="false" class="form-control file" accept="image/*">
                    </div>
                    <div id="img-details"></div>

                    <div class="row standard">
                        <div class="col-md-8">
                            <div class="form-group all">
                                <!-- <label class="control-label" for="unit"><?= lang("product_qty") ?></label> -->
                                <?php
                                foreach ($warehouses as $warehouse) {
                                    if ($this->Settings->racks) {
                                        // echo "<div class='row'><div class='col-md-12'>";
                                        echo form_hidden('wh_' . $warehouse->id, $warehouse->id) . form_hidden('wh_qty_' . $warehouse->id, '1');
                                        // echo "</div></div>";
                                    } else {
                                        echo form_hidden('wh_' . $warehouse->id, $warehouse->id) . form_hidden('wh_qty_' . $warehouse->id, '1');
                                    }
                                }
                                ?>
                                <label class="control-label" for="unit"><?= lang("Primary_Unit") ?></label>
                                
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?php // lang("unit_per", "unit") ?>
                                <div class="input-group">
                                    <?= form_input('unit', (isset($_POST['unit']) ? $_POST['unit'] : ($product ? $product->uper : '')), 'class="form-control" id="unit" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" required="required" style="width:100%" '); ?>
                                    <div class="input-group-addon no-print">
                                        <a href="<?php echo site_url('system_settings/AddProduct_para/per'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row standard">
                        <!-- <div class="col-md-1">
                            <div class="form-group all">
                                <input type="checkbox" icheck class="checkbox" ng-model="prod.chksecqty" name="sec_qtychk" id="sec_qtychk">
                            </div>
                        </div> -->
                        <div class="col-md-8">
                            <div class="form-group all">
                                <label class="control-label" for="unit"><?= lang("Secondary_Unit") ?></label>
                                <!-- <?php // form_input('squantity', (isset($_POST['squantity']) ? $_POST['squantity'] : ($product ? $this->sma->formatDecimal($product->squantity) : '')), 'class="form-control tip" id="squantity"') ?> -->
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <?= form_input('sunit', (isset($_POST['sunit']) ? $_POST['sunit'] : ($product ? intval($product->sunit) : '')), 'class="form-control" id="sunit" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" style="width:100%"'); ?>
                                    <div class="input-group-addon no-print">
                                        <a href="<?php echo site_url('system_settings/AddProduct_para/per'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script>
                                $('#squantity').change(function () {
                                    var squantity = $('#squantity').val();
                                    var wh_qtys = $('#wh_qtys').val();

                                    if (wh_qtys % squantity != 0 && squantity != "") {
                                        bootbox.alert('Please Enter Valid Secondary Quentity');
                                        $('#squantity').val("");
                                    }
                                });
                    </script>
                    <!-- <div class="row">
                        <div class="col-md-8">
                            <div class="form-group all" ng-init="prod.cost = '<?= (isset($_POST["cost"]) ? $_POST["cost"] : ($product ? $this->sma->formatDecimal($product->cost) : "")) ?>'">
                                <?= lang("product_cost", "cost") ?>
                                <?= form_input('cost', (isset($_POST['cost']) ? $_POST['cost'] : ($product ? $this->sma->formatDecimal($product->cost) : '')), 'class="form-control tip" id="cost" required="required" ng-model="prod.cost" ng-blur="getProductMargin();"') ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group all">
                                <?= lang("product_per", "cper") ?>
                                <?= form_input('cper', (isset($_POST['cper']) ? $_POST['cper'] : ($product ? $product->cper : '')), 'class="form-control" id="cper" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" required="required" style="width:100%" '); ?>
                            </div>
                        </div>
                    </div>  

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group all" ng-init="prod.price = '<?= (isset($_POST["price"]) ? $_POST["price"] : ($product ? $this->sma->formatDecimal($product->price) : "")) ?>'">
                                <?= lang("product_price", "price") ?>
                                <?= form_input('price', (isset($_POST['price']) ? $_POST['price'] : ($product ? $this->sma->formatDecimal($product->price) : '')), 'class="form-control tip" id="price" ng-model="prod.price" ') ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group all">
                                <?= lang("product_per", "pper") ?>
                                <div class="">
                                    <?= form_input('pper', (isset($_POST['pper']) ? $_POST['pper'] : ($product ? $product->pper : '')), 'class="form-control" id="pper" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" required="required" style="width:100%"'); ?>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!--<div class="form-group standard">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">

                                    <div class="col-md-3">
                                        <?= lang("Rate", "singlerate") ?>    
                                    </div>
                                    <div class="col-md-1">
                                        <?= lang("Per", "Per") ?> </div>

                                    <div class="col-md-4">
                                        <?= form_input('rateper', (isset($_POST['rateper']) ? $_POST['rateper'] : ($product ? $product->rateper : '')), 'class="form-control" default-attrib data-tab="per" id="rateper" placeholder="' . lang("select") . " " . lang("Unit") . '" required="required" style="width:100%" '); ?>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="checkbox" icheck class="checkbox"  ng-model="prod.roundup" id="extras" value=""/>
                                        <label for="extras" class="padding05"><?= lang('roundup') ?></label>
                                    </div>

                                </div>                                
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" <?= (isset($_POST['ratetype']) && $_POST['ratetype'] == "Single" ? 'checked' : ($product->ratetype == 'Single' ? 'checked' : '')) ?> name="ratetype" value="Single"/>&nbsp;&nbsp;&nbsp;&nbsp;Single</div>
                            <div class="col-md-3"><?php echo form_input('singlerate', (isset($_POST['singlerate']) ? $_POST['singlerate'] : ($product ? $product->singlerate : '')), 'class="form-control" id="singlerate" ng-model="prod.singlerate" placeholder="" style="width:100%;" '); ?></div>
                            <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" <?= (isset($_POST['ratetype']) && $_POST['ratetype'] == "MRP" ? 'checked' : ($product->ratetype == 'MRP' ? 'checked' : '')) ?> name="ratetype"  value="MRP"/>&nbsp;&nbsp;&nbsp;&nbsp;MRP</div>
                            <div class="col-md-3"><?php echo form_input('mrprate', (isset($_POST['mrprate']) ? $_POST['mrprate'] : ($product ? $product->mrprate : '')), 'class="form-control" id="mrprate" placeholder="" style="width:100%;"'); ?></div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" <?= (isset($_POST['ratetype']) && $_POST['ratetype'] == "Multiple" ? 'checked' : ($product->ratetype == 'Multiple' ? 'checked' : '')) ?> name="ratetype" value="Multiple"/>&nbsp;&nbsp;&nbsp;&nbsp;Multiple</div>
                            <div class="col-md-3">
                                <div class="col-md-12" style="padding: 0px"><?php echo form_input('mulratef', (isset($_POST['mulratef']) ? $_POST['mulratef'] : ($product ? $product->mulratef : '')), 'class="form-control" id="mulratef" placeholder="" style="width:100%;"'); ?></div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-3">To</div>
                            <div class="col-md-3" ng-init='size2 =<?= json_encode($sizes) ?>'>
                                <div class="col-md-12" style="padding: 0px"><?php echo form_input('mulratet', (isset($_POST['mulratet']) ? $_POST['mulratet'] : ($product ? $product->mulratet : '')), 'class="form-control" id="mulratet" placeholder="" style="width:100%;"'); ?></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>


                    </div>
                    <div class="">
                        <div class="row ">
                            <div class="col-md-6 form-group"> 
                                <?= lang('hsncode', 'hsncode') ?>
                                <div class="input-group">
                                    <?= form_input('hsnno', (isset($_POST['hsnno']) ? $_POST['hsnno'] : $product->price), 'class="form-control tip"  id="hsnno" ng-model="prod.hsnno" readonly') ?>
                                    <div class="input-group-addon no-print">
                                        <a href="<?php echo site_url('gst/add'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <?= lang('gstno', 'gstno') ?>
                                <?= form_input('gstno', (isset($_POST['gstno']) ? $_POST['gstno'] : $product->price), 'class="form-control tip" id="gstno" readonly ng-model="prod.gstno"  ng-blur="getProductMargin()"') ?>
                            </div>
                            <div class="col-md-3">
                                <?= lang('cess', 'cess') ?>
                                <?= form_input('cess', (isset($_POST['cess']) ? $_POST['cess'] : $product->price), 'class="form-control tip" id="cess" readonly ng-model="prod.cess"  ng-blur="getProductMargin()"') ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-8"> &nbsp;&nbsp;<input type="checkbox" icheck class="checkbox" id="addupgetcheck" ng-model="prod.addupgst" name="addupgst">&nbsp;&nbsp;&nbsp;&nbsp;Add-up GST if MRP exceeds 1000+ (%)</div>
                            <div class="col-md-4">
                                <?= form_input('addupgstmrp', (isset($_POST['addupgstmrp']) ? $_POST['addupgstmrp'] : $product->price), 'class="form-control tip" id="addupgstmrp" ng-model="prod.addupgstmrp" readonly ng-blur="getProductMargin()"') ?>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group combod">
                                <?= lang("combo_discount", "combo_discount") ?>
                                <?= form_input('combo_discount', (isset($_POST['combo_discount']) ? $_POST['combo_discount'] : ($product ? $this->sma->formatDecimal($product->combo_discount) : '')), 'class="form-control tip" id="combo_discount" required="required"') ?>
                            </div>
                        </div>
                    </div>
                    <div class="row bundleb">
                        <div class="col-md-12">
                            <div class="form-group ">
                                <?= lang("Batch", "batch") ?>
                                <?= form_input('batch', (isset($_POST['batch']) ? $_POST['batch'] : ($product ? $product->batch : '')), 'class="form-control tip" id="batch" required="required"') ?>
                            </div>
                        </div>
                    </div>
                    <div class="combo" style="display:none;">
                        <div class="form-group">
                            <?= lang("add_product", "add_item") . ' (' . lang('not_with_variants') . ')'; ?>
                            <?php echo form_input('add_item', '', 'class="form-control ttip" id="add_item" data-placement="top" data-trigger="focus" data-bv-notEmpty-message="' . lang('please_add_items_below') . '" placeholder="' . $this->lang->line("add_item") . '"'); ?>
                        </div>
                        <div class="control-group table-group">
                            <label class="table-label" for="combo">{{prod.protype}} <?= lang("products"); ?></label>
                            <div class="controls table-controls">
                                <table id="prTable" class="table items table-striped table-bordered table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th class="col-md-5 col-sm-5 col-xs-5"><?= lang("product_name") . " (" . $this->lang->line("product_code") . ")"; ?></th>
                                            <th class="col-md-2 col-sm-2 col-xs-2"><?= lang("quantity"); ?></th>
                                            <th class="col-md-3 col-sm-3 col-xs-3"><?= lang("unit_price"); ?></th>
                                            <th class="col-md-1 col-sm-1 col-xs-1 text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="digital" style="display:none;">
                        <div class="form-group digital">
                            <?= lang("digital_file", "digital_file") ?>
                            <input id="digital_file" type="file" name="digital_file" data-show-upload="false"
                                   data-show-preview="false" class="form-control file">
                        </div>
                    </div>
                </div>-->
                <div class="form-group">
                    <?= lang("Product_Classification", "product_classification") ?>
                    <div class="row">
                        <div class="col-md-4"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="cf1" <?= ($_POST['cf1'] == "regular" ? "checked='checked'" : '') ?> value="regular" id="prod_classification_regular"/>&nbsp;&nbsp;&nbsp;&nbsp;Regular</div>
                        <div class="col-md-4"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="cf1" <?= ($_POST['cf1'] == "economical" ? "checked='checked'" : '') ?> value="economical" id="prod_classification_economical" />&nbsp;&nbsp;&nbsp;&nbsp;Economical</div>
                        <div class="col-md-4"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="cf1" <?= ($_POST['cf1'] == "premium" ? "checked='checked'" : '') ?> value="premium" id="prod_classification_premium" />&nbsp;&nbsp;&nbsp;&nbsp;Premium</div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <?php echo form_submit('add_product', "Save", 'class="btn btn-primary" '); ?>
                    </div>
                </div>
                <?= form_close(); ?>
            </div>


        </div>
    </div>
</div>