<?php
// echo "<pre>";
// foreach ($dept as $key => $value) {
//     print_r($value->id);
// }exit;
// echo "<pre>";print_r($product);exit;
?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_size'); ?></h4>
        </div>

        <?php
        $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'add_size');
        echo form_open_multipart("products/addSize", $attrib);
        ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            
            <div class="row">
                <div class="col-md-6">
                    
                    <div class="form-group">
                        <?= lang("size_name", "size_name") ?>
                        <?php echo form_input('size_name', '', 'class="form-control tip" id="size_name" data-bv-notempty="true"'); ?>
                    </div>

                    <div class="form-group">
                        <?= lang("size_code", "size_code") ?>
                        <?php echo form_input('size_code', '', 'class="form-control tip" id="size_code" data-bv-notempty="true"'); ?>
                    </div>

                    <div class="form-group">
                        <?= lang("dept_name", "dept_name") ?> <label>*</label>
                        <?php
                        $bl[""] = "";
                        foreach ($dept as $dept) {
                            $bl[$dept->id] = $dept->name;
                        }
                        // echo "<pre>";print_r($bl);
                        echo form_dropdown('dept_name', $bl, (isset($_POST['dept']) ? $_POST['dept'] : $quote->dept_id), 'id="dept" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("dept") . '" required="required" class="form-control" style="width:100%;"');
                        ?>
                    </div>
                    
                    <div class="form-group">
                        <?= lang("product_name", "product_name") ?> 
                        <?php
                        $prod[""] = "";
                        foreach ($product as $product) {
                            $prod[$product->id] = $product->name ;
                        }
                        // echo "<pre>";print_r($bl);
                        echo form_dropdown('product_name', $prod, (isset($_POST['prod']) ? $_POST['prod'] : $quote->dept_id), 'id="product_name" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("product") . '" required="required" class="form-control" style="width:100%;"');
                        ?>
                    </div>

                    <!-- <div class="form-group">
                        <?= lang("section", "section") ?> 
                        <?php
                        $sec[""] = "";
                        foreach ($section as $section) {
                            $sec[$section->id] = $section->name ;
                        }
                        // echo "<pre>";print_r($bl);
                        echo form_dropdown('section', $sec, (isset($_POST['section']) ? $_POST['section'] : $quote->dept_id), 'id="section" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("section") . '" required="required" class="form-control" style="width:100%;"');
                        ?>
                    </div> -->

                    <!-- <div class="form-group">
                        <?= lang("status", "status") ?> 
                        <?php
                        $status = array(
                            'active'   => 'Active',
                            'deactive' => 'Deactive'
                        );
                        // echo "<pre>";print_r($bl);
                        echo form_dropdown('status', $status, (isset($_POST['status']) ? $_POST['status'] : $quote->dept_id), 'id="status" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("status") . '" required="required" class="form-control" style="width:100%;"');
                        ?>
                    </div> -->

                </div>
            </div>
        </div>
        <div class="modal-footer" style="margin-top:10px;">
            <div class="text-center">
            <?php echo form_submit('add_size', lang('save'), 'class="btn btn-primary"'); ?>
            </div>
        </div>
   </div>
    <?php echo form_close(); ?>
 
</div>
<?= $modal_js ?>