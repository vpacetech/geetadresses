<link href="<?= $assets ?>styles/theme.css" rel="stylesheet" media="all"/>
<link href="<?= $assets ?>styles/mystyle.css" rel="stylesheet"/>
<style>
    @media print{

        .tableid > thead > tr > th,
        .tableid > tbody > tr > th,
        .tableid > tfoot > tr > th,
        .tableid > thead > tr > td,
        .tableid > tbody > tr > td,
        .tableid > tfoot > tr > td
        {line-height:10px !important;border:none!important;padding: 0;}
        body{

            line-height: 1px;
            color: #000;
            font-size:8px !important;
            line-height:10px !important;
        }
        h3{font-size:8px !important;}
        .barcodeprint{
            width: 42mm !important;
            height: 42mm !important;
            padding: 15px 5px !important;
            border: 1px solid #999999!important;
            margin: 2px;
        }
        .container{
            /*width: 36mm !important;*/
            /*margin-left: 10px !important;*/
            margin:8px !important;
            font-size: 8px !important;
            /*border:1px solid #000000 !important;*/
        } 
        @page { 
            size: A4;    
        } 
    }
    @media screen{
        .barcodeprint{
            width: 70mm !important;
            /*height: 10mm !important;*/
            padding: 10px !important;
            border: 1px solid #999999!important;
        }

    }
</style>  
<div class="box">
    <div class="box-header no-print">

        <div class="box-icon">
            <ul class="btn-tasks">

                <li class="dropdown"><a href="javascript:void();" onclick="window.print();"  id="print-icon" class="tip" title="<?= lang('print') ?>"><i class="icon fa fa-print"></i></a></li>
                <li class="dropdown"><a href="javascript:void();" onclick="close_window();"  id="print-icon" class="tip" title="Close"><i class="icon fa fa-times-circle-o"></i></a></li>

            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <div class="container"  style="background-color:#ffffff;">
                    <?php
                    if ($r != 1) {
                        echo $html;
                    } else {
                        echo '<h3>' . $this->lang->line('empty_category') . '</h3>';
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
<script>
                    window.print();
//                    var daddy = window.self;
//                    daddy.opener = window.self;
//                    daddy.close();


                    function close_window() {
                        var daddy = window.self;
                        daddy.opener = window.self;
                        daddy.close();
                    }
</script>
