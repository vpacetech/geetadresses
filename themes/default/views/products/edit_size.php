<?php

// echo "<pre>";print_r($dept->name);exit;

?>


<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_size'); ?></h4>
        </div>
        <?php
        $attrib = array('data-toggle' => 'validator', 'role' => 'form', "id" => "edit_size");
        echo form_open_multipart("products/editSize/" . $size->id, $attrib);
        ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            
            
            <div class="row billing">
                <div class="col-md-6">
                <div class="form-group">
                        <?= lang("size_name", "size_name") ?>
                        <?php echo form_input('size_name', $size->id, 'class="form-control tip" id="size_name" data-bv-notempty="true"'); ?>
                    </div>

                    <div class="form-group">
                        <?= lang("size_code", "size_code") ?>
                        <?php echo form_input('size_code', $size->code, 'class="form-control tip" id="size_code" data-bv-notempty="true"'); ?>
                    </div>

                    <div class="form-group">
                        <?= lang("dept_name", "dept_name") ?> <label>*</label>
                        <?php
                        $bl[""] = "";
                        foreach ($dept as $dept) {
                            $bl[$dept->id] = $dept->name;
                        }
                        $data_dept = array($dept_name->id,$dept_name->name);
                        // echo "<pre>";print_r($bl);
                        echo form_dropdown('dept_name', $bl, $data_dept, 'id="dept" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("dept") . '" required="required" class="form-control" style="width:100%;"');
                        ?>
                    </div>
                    
                    <div class="form-group">
                        <?= lang("product_name", "product_name") ?> 
                        <?php
                        $prod[""] = "";
                        foreach ($product as $product) {
                            $prod[$product->id] = $product->name ;
                        }
                        $data_product = array($product_name->id,$product_name->name);
                        // echo "<pre>";print_r($bl);
                        echo form_dropdown('product_name', $prod, $data_product, 'id="product_name" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("product") . '" required="required" class="form-control" style="width:100%;"');
                        ?>
                    </div>


                    
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('edit_department', lang('update'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

