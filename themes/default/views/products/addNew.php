

<?php
//
if (!empty($variants)) {
    foreach ($variants as $variant) {
        $vars[] = addslashes($variant->name);
    }
} else {
    $vars = array();
}

$department[''] = "";
$product_items[''] = "";
$section[''] = "";
$type[''] = "";
$brands[''] = "";
$design[''] = "";
$style[''] = "";
$pattern[''] = "";
$fitting[''] = "";
$fabric[''] = "";
$color[''] = "";
$size[''] = "";
$sizes = array();
$per[''] = "";
foreach ($product_para as $k => $v) {
    if ($k == "department" && !empty($v)) {
        foreach ($v as $d) {
            $department[$d->id] = $d->name;
        }
    }
    if ($k == "product_items" && !empty($v)) {
        foreach ($v as $d) {
            $product_items[$d->id] = $d->name;
        }
    }
    if ($k == "section" && !empty($v)) {
        foreach ($v as $d) {
            $section[$d->id] = $d->name;
        }
    }
    if ($k == "type" && !empty($v)) {
        foreach ($v as $d) {
            $type[$d->id] = $d->name;
        }
    }
    if ($k == "brands" && !empty($v)) {
        foreach ($v as $d) {
            $brands[$d->id] = $d->name;
        }
    }
    if ($k == "design" && !empty($v)) {
        foreach ($v as $d) {
            $design[$d->id] = $d->name;
        }
    }
    if ($k == "style" && !empty($v)) {
        foreach ($v as $d) {
            $style[$d->id] = $d->name;
        }
    }
    if ($k == "pattern" && !empty($v)) {
        foreach ($v as $d) {
            $pattern[$d->id] = $d->name;
        }
    }
    if ($k == "fitting" && !empty($v)) {
        foreach ($v as $d) {
            $fitting[$d->id] = $d->name;
        }
    }
    if ($k == "fabric" && !empty($v)) {
        foreach ($v as $d) {
            $fabric[$d->id] = $d->name;
        }
    }
    if ($k == "color" && !empty($v)) {
        foreach ($v as $d) {
            $color[$d->id] = $d->name;
        }
    }
    if ($k == "size" && !empty($v)) {
        foreach ($v as $d) {
            $size[$d->id] = $d->name;
            $sizes[$d->id] = $d;
        }
    }
    if ($k == "per" && !empty($v)) {
        foreach ($v as $d) {
            $per[$d->id] = $d->name;
        }
    }
}
?>
<script type="text/javascript">
    $(document).ready(function () {
        $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
            placeholder: "<?= lang('select_category_to_load') ?>", data: [
                {id: '', text: '<?= lang('select_category_to_load') ?>'}
            ]
        });
        $('#category').change(function () {
            var v = $(this).val();
            $('#modal-loading').show();
            if (v) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: "<?= site_url('products/getSubCategories') ?>/" + v,
                    dataType: "json",
                    success: function (scdata) {
                        if (scdata != null) {
                            $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                                placeholder: "<?= lang('select_category_to_load') ?>",
                                data: scdata
                            });
                        }
                    },
                    error: function () {
                        bootbox.alert('<?= lang('ajax_error') ?>');
                        $('#modal-loading').hide();
                    }
                });
            } else {
                $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
                    placeholder: "<?= lang('select_category_to_load') ?>",
                    data: [{id: '', text: '<?= lang('select_category_to_load') ?>'}]
                });
            }
            $('#modal-loading').hide();
        });
        $('#code').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                return false;
            }
        });

        $('#color').change(function () {
            var v = $(this).val();
            $.ajax({
                type: "get",
                async: false,
                url: "<?= site_url('products/getColorCode') ?>/" + v,
                dataType: "json",
                success: function (scdata) {
//                        alert(scdata);
                    if (scdata != null) {
                        var code = scdata.code;
                        $('#codet').val(code);
                        var quantity = $('#colorqty').val();
                        $('#colorcode').val(code + quantity);
                        $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                            placeholder: "<?= lang('select_category_to_load') ?>",
                            data: scdata
                        });
                    }
                },
                error: function () {
                    bootbox.alert('<?= lang('ajax_error') ?>');
                    $('#modal-loading').hide();
                }
            });
        });

        $('#colorqty').change(function () {
            $('#colorcode').attr("value", "");
            var codet = $('#codet').val();
//        var quantity = $('#colorqty').val();
            var qty = $('#colorqty').val();
            $('#colorcode').attr("value", codet + qty);
        })

        $('#companies').change(function () {
            var v = $(this).val();
            $.ajax({
                type: "get",
                async: false,
                url: "<?= site_url('products/getsupplierfrompurchase') ?>/" + v,
                dataType: "json",
                success: function (scdata) {
                    $('#supplier12').select2('val', '');
                    $('#supplier12').html('');
                    $.each(scdata, function (k, val) {
                        var opt = $('<option />');
                        opt.val(val.id);
                        opt.text(val.name + ' (' + val.code + ')');
                        $('#supplier12').append(opt);
                    })
                },
                error: function () {
                    bootbox.alert('<?= lang('ajax_error') ?>');
                    $('#modal-loading').hide();
                }
            });
        });


    });




</script>
<div class="box" ng-controller="addProducts">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('add_product'); ?></h2>
    </div>
    <div class="box-content" >
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?php echo lang('enter_info'); ?></p>
                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'addproduct');
                echo form_open_multipart("products/addNew", $attrib)
                ?>
                <div class="col-md-5">
                    <!-- <div class="form-group" ng-init="prod.protype = '<?= (isset($_POST['type']) ? $_POST['type'] : ($product ? $product->type : '')) ?>'">
                        <?= lang("product_type", "type") ?>
                        <?php
                        // if ($product_id != "") {
                        //     $opts = array('standard' => lang('standard'), 'combo' => lang('combo'), 'bundle' => lang('bundle'));
                        // } else {
                        //     $opts = array('standard' => lang('standard'));
                        // }
                        // echo form_dropdown('type', $opts, (isset($_POST['type']) ? $_POST['type'] : ($product ? $product->type : '')), 'class="form-control" id="type" required="required" ng-model="prod.protype" ng-change="getProdBarcode();"');
                        echo form_dropdown('type', $opts, (isset($_POST['type']) ? $_POST['type'] : ($product ? $product->type : '')), 'class="form-control" id="type" required="required" ng-model="prod.protype" ng-change="getProdBarcode();"');
                        ?>
                    </div> -->
                    

                    <div class="form-group all" ng-init="prod.store_id = '<?= (isset($_POST['store_id']) ? $_POST['store_id'] : ($product ? $product->store_id : '')) ?>'">
                        <?= lang("Store", "companies") ?>
                        <div class="input-group">
                            <?php
                            echo form_hidden('type', "standard");
                            echo form_input('store_id', (isset($_POST['store_id']) ? $_POST['store_id'] : ($product ? $product->store_id : '')), 'class="form-control" default-attrib data-tab="companies" data-id="0" id="companies" placeholder="' . lang("select") . " " . lang("store") . '" required="required" style="width:100%" ng-model="prod.store_id" ng-change="getProdBarcode();getGstDetails();"');
                            ?>
                            <div class="input-group-addon no-print">
                                <a href="<?php echo site_url('billers/add'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group all" ng-init="prod.dept = '<?= (isset($_POST['department']) ? $_POST['department'] : ($product ? $product->department : '')) ?>';
                                    getProdBarcode()">
                             <?= lang("Department", "department") ?>
                        <div class="input-group">
                            <?php
                            echo form_input('department', (isset($_POST['department']) ? $_POST['department'] : ($product ? $product->department : '')), 'class="form-control" default-attrib data-tab="department" data-key="store_id" data-id="companies" id="department" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ng-model="prod.dept" ng-change="getProdName();getProdBarcode();getProductMargin();getGstDetails();"');
                            ?>
                            <div class="input-group-addon no-print">
                                <a href="<?php echo site_url('system_settings/AddProduct_para/department'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="form-group all" ng-init="prod.section_id = '<?= (isset($_POST['section']) ? $_POST['section'] : ($product ? $product->section : '')) ?>';
                                    getProdBarcode()">
                             <?= lang("Section", "section") ?>
                        <div class="input-group">
                            <?php
                            echo form_input('section', (isset($_POST['section']) ? $_POST['section'] : ($product ? $product->section : '')), 'class="form-control" default-attrib data-tab="section" id="section" data-key="department_id" data-id="department" placeholder="' . lang("select") . " " . lang("section") . '" ng-model="prod.section_id" ng-change="getProductMargin();getGstDetails();" required="required" style="width:100%"');
                            ?>
                            <div class="input-group-addon no-print">
                                <a href="<?php echo site_url('system_settings/AddProduct_para/section'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                            </div>
                        </div>
                    </div> -->
                    <div class="form-group all"  ng-init="prod.product_item = '<?= (isset($_POST['product_items']) ? $_POST['product_items'] : ($product ? $product->product_items : '')) ?>';
                                    getProdBarcode()">
                             <?= lang("Product_items", "product_items") ?>
                        <div class="input-group">
                            <?php
                            echo form_input('product_items', (isset($_POST['product_items']) ? $_POST['product_items'] : ($product ? $product->product_items : '')), 'class="form-control" default-attrib data-tab="product_items" data-key="department_id" data-id="department" id="product_items" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%" ng-model="prod.product_item" ng-change="getProdName();getProductMargin();getGstDetails();"');
                            ?>
                            <div class="input-group-addon no-print">
                                <a href="<?php echo site_url('system_settings/AddProduct_para/product_items'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group all" ng-init="prod.type = '<?= (isset($_POST['type_id']) ? $_POST['type_id'] : ($product ? $product->type_id : '')) ?>';
                                    getProdBarcode()">
                             <?= lang("Type", "type_id") ?>
                        <div class="input-group">
                            <?php
                            echo form_input('type_id', (isset($_POST['type_id']) ? $_POST['type_id'] : ($product ? $product->type_id : '')), 'class="form-control" default-attrib data-tab="type" id="type_id" data-key="product_items_id" data-id="product_items" placeholder="' . lang("select") . " " . lang("type") . '" required="required" style="width:100%" ng-model="prod.type" ng-change="getProdName();getProductMargin();getGstDetails();"');
                            ?>
                            <div class="input-group-addon no-print">
                                <a href="<?php echo site_url('system_settings/AddProduct_para/type'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group all" ng-init="prod.brands_id = '<?= (isset($_POST['brands']) ? $_POST['brands'] : ($product ? $product->brands : '')) ?>';
                                    getProdBarcode();">
                             <?= lang("Brands", "brands") ?>
                        <div class="input-group">
                            <?php
                            echo form_input('brands', (isset($_POST['brands']) ? $_POST['brands'] : ($product ? $product->brands : '')), 'class="form-control" default-attrib data-tab="brands" id="brands" data-key="type_id" data-id="type_id" placeholder="' . lang("select") . " " . lang("brands") . '" required="required" ng-model="prod.brands_id" ng-change="getProductMargin();getGstDetails();"  style="width:100%"');
                            ?>
                            <div class="input-group-addon no-print">
                                <a href="<?php echo site_url('system_settings/AddProduct_para/brands'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group standard" >
                        <?= lang("Design", "design") ?>
                        <div class="input-group">
                            <?php
                            echo form_input('design', (isset($_POST['design']) ? $_POST['design'] : ($product ? $product->design : '')), 'class="form-control" default-attrib data-tab="design" id="design" data-key="brands_id" data-id="brands" placeholder="' . lang("select") . " " . lang("design") . '" style="width:100%" ng-model="prod.design" ');
                            ?>
                            <div class="input-group-addon no-print">
                                <a href="<?php echo site_url('system_settings/AddProduct_para/design'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group standard">
                        <?= lang("Style", "style") ?>
                        <div class="input-group">
                            <?php
                            echo form_input('style', (isset($_POST['style']) ? $_POST['style'] : ($product ? $product->style : '')), 'class="form-control" default-attrib data-tab="style" id="style" data-key="design_id" data-id="design" placeholder="' . lang("select") . " " . lang("style") . '" style="width:100%" ng-model="prod.style"');
                            ?>
                            <div class="input-group-addon no-print">
                                <a href="<?php echo site_url('system_settings/AddProduct_para/style'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group standard">
                        <?= lang("Pattern", "pattern") ?>
                        <div class="input-group">
                            <?php
                            echo form_input('pattern', (isset($_POST['pattern']) ? $_POST['pattern'] : ($product ? $product->pattern : '')), 'class="form-control" default-attrib data-tab="pattern" data-key="style_id" data-id="style" id="pattern" placeholder="' . lang("select") . " " . lang("pattern") . '" style="width:100%" ng-model="prod.pattern"');
                            ?>
                            <div class="input-group-addon no-print">
                                <a href="<?php echo site_url('system_settings/AddProduct_para/pattern'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group standard">
                        <?= lang("Fitting", "fitting") ?>
                        <div class="input-group">
                            <?php
                            echo form_input('fitting', (isset($_POST['fitting']) ? $_POST['fitting'] : ($product ? $product->fitting : '')), 'class="form-control" default-attrib data-tab="fitting" data-key="pattern_id" data-id="pattern" id="fitting" placeholder="' . lang("select") . " " . lang("fitting") . '" style="width:100%" ng-model="prod.fitting"');
                            ?>
                            <div class="input-group-addon no-print">
                                <a href="<?php echo site_url('system_settings/AddProduct_para/fitting'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group standard">
                        <?= lang("Fabric", "fabric") ?>
                        <div class="input-group">
                            <?php
                            echo form_input('fabric', (isset($_POST['fabric']) ? $_POST['fabric'] : ($product ? $product->fabric : '')), 'class="form-control" default-attrib data-tab="fabric" id="fabric" data-key="fitting_id" data-id="fitting" placeholder="' . lang("select") . " " . lang("fabric") . '" style="width:100%" ng-model="prod.fabric"');
                            ?>
                            <div class="input-group-addon no-print">
                                <a href="<?php echo site_url('system_settings/AddProduct_para/fabric'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="form-group standard">
                        <?= lang("Color", "color") ?>
                        <div class="row">
                            <div class="col-md-4"> &nbsp;&nbsp;<input type="radio" name="colortype" ng-model="prod.colortype" icheck value="Single"/>&nbsp;&nbsp;&nbsp;&nbsp;Single</div>
                            <div class="col-md-4  colorsingle hide">
                                <div class="input-group">
                                    <?= form_input('colorsingle', (isset($_POST['colorsingle']) ? $_POST['colorsingle'] : ($product ? $product->color : '')), 'class="form-control" id="color" placeholder="' . lang("select") . " " . lang("color") . '" default-attrib data-tab="color"  style="width:100%" '); ?>
                                    <?php // echo form_dropdown('colorsingle', $color, (isset($_POST['colorsingle']) ? $_POST['colorsingle'] : ($product ? $product->color : '')), 'class="form-control" select2 id="color" placeholder="' . lang("select") . " " . lang("color") . '" style="width:100%"');  ?>
                                    <div class="input-group-addon no-print ">
                                        <a href="<?php echo site_url('system_settings/AddProduct_para/color'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal2" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2 colorsingle hide" >
                                <input type="text" id="colorqty" name="colorqty" onlyno class="form-control" ng-model="prod.colorqty" ng-change="getQty('#colorqty')"/>
                            </div>
                            <div class="col-md-2 colorsingle hide" style="padding: 0;">
                                <input type="text" id="colorcode" name="colorcode[]" onlyno readonly class="form-control" ng-model="prod.colorcode" ng-change="getQty('#colorqty')"/>
                                <input type="text" id="codet" name="codet" onlyno class="hidden form-control"/>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-4"> &nbsp;&nbsp;<input type="radio" name="colortype" ng-model="prod.colortype" icheck value="Assorted"/>&nbsp;&nbsp;&nbsp;&nbsp;Assorted</div>
                            <div class="col-md-8  colorassorted hide">
                                <div class="row">
                                    <?php
                                    echo form_dropdown('colorassorted[]', $color, (isset($_POST['colorassorted']) ? $_POST['colorassorted'] : ($product ? $product->colorassorted : '')), 'class="form-control" multiple select2 id="mulcolor" ng-model="prod.colorassorted" placeholder="' . lang("select") . " " . lang("color") . '"style="width:100%"');
                                    ?>
                                </div>

                                <br/>
                                <div class="row" style="padding-top: 40px;">
                                    <div class="col-md-3 from-group" style="padding-right: 0px; margin-bottom: 5px" ng-repeat="n in prod.colorassoarr">
                                        <input type="text" name="colorqty[]" onlyno ng-model="n.qty" ng-blur="getQtyCal($index, n.qty)" class="form-control"/>
                                    </div>
                                </div>
                                <div class="row" style="padding-top: 2s0px;">
                                    <div class="col-md-3 from-group" style="padding-right: 0px; margin-bottom: 5px" ng-repeat="n in prod.colorassoarr">
                                        <input type="text" name="colorcode[]" onlyno ng-model="n.colorcode" readonly class="form-control"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!--{{prod}}-->
                    <div class="form-group">
                        <?= lang("Size", "size") ?>
                        <div class="row">
                            <div class="col-md-4"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="sizeangle" ng-model="prod.sizeangle" ng-change="getProdName()" <?= ($product && $product->sizeangle == "HT" ? "checked='checked'" : '') ?> value="HT" id="sizeangle_ht"/>&nbsp;&nbsp;&nbsp;&nbsp;Height</div>
                            <div class="col-md-4"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="sizeangle" ng-model="prod.sizeangle" ng-change="getProdName()" <?= ($product && $product->sizeangle == "WT" ? "checked='checked'" : '') ?> value="WT" id="sizeangle_wt" />&nbsp;&nbsp;&nbsp;&nbsp;Width</div>
                            <div class="col-md-4"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="sizeangle" ng-model="prod.sizeangle" ng-change="getProdName()" <?= ($product && $product->sizeangle == "NZ" ? "checked='checked'" : '') ?> value="NZ" id="sizeangle_nz" />&nbsp;&nbsp;&nbsp;&nbsp;No Size</div>
                        </div>
                        <script>
                                    $(document).on('ifChecked', '#sizeangle_nz', function (e) {
                                        $('.sizeangle_nz').css('display', 'none');
                                    });
                                    $(document).on('ifUnchecked', '#sizeangle_nz', function (e) {
                                        $('.sizeangle_nz').css('display', 'block');
                                    });
                        </script>
                        <br/>
                        <div class="row sizeangle_nz" >
                            <div class="col-md-4"> &nbsp;&nbsp;<input icheck id="" type="radio" name="sizetype" ng-model="prod.sizetype" ng-change="getProdName()" checked="" value="Single"/>&nbsp;&nbsp;&nbsp;&nbsp;Single</div>
                            <div class="col-md-5" ng-init='size =<?= json_encode($sizes) ?>' ng-show="prod.sizetype == 'Single'">
                                <div class="col-md-9" style="padding: 0px">
                                    <div class="input-group">
                                        <?php
                                        echo form_input('singlesize', (isset($_POST['singlesize']) ? $_POST['singlesize'] : ($product ? $product->size : '')), 'class="form-control " sizesel data-tab="size" data-key="department_id-product_items_id" data-id="department,product_items" id="size" placeholder="' . lang("select") . " " . lang("size") . '" required="required" style="width:100%" ng-model="prod.singlesize" ng-change="getProdName();"');
                                        ?>
                                        <div class="input-group-addon no-print ">
                                            <a href="<?php echo site_url('system_settings/AddProduct_para/size'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"> {{size[prod.singlesize].code?size[prod.singlesize].code+'\"':""}}</div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <br/>
                        <div class="row standard sizeangle_nz" >
                            <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" name="sizetype" ng-model="prod.sizetype" ng-change="getProdName()" value="Multiple"/>&nbsp;&nbsp;&nbsp;&nbsp;<span>Multiple</span></div>
                            <div class="col-md-4" ng-init='size1 =<?= json_encode($sizes) ?>' ng-show="prod.sizetype == 'Multiple'">
                                <div class="col-md-8" style="padding: 0px"><?php
                                    echo form_input('multisizef', (isset($_POST['multisizef']) ? $_POST['multisizef'] : ''), 'class="form-control " sizesel data-tab="size" data-key="department_id-section_id-product_items_id" data-id="department,section,product_items" id="multisizef" placeholder="' . lang("select") . " " . lang("size") . '" required="required" style="width:100%" ng-model="prod.multisizef" ng-change="getProdName();"');
                                    ?></div>
                                <div class="col-md-2"> {{size1[prod.multisizef].code?size1[prod.multisizef].code+'\"':""}}</div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-1" ng-show="prod.sizetype == 'Multiple'">To</div>
                            <div class="col-md-4" ng-init='size2 =<?= json_encode($sizes) ?>' ng-show="prod.sizetype == 'Multiple'">
                                <div class="col-md-8" style="padding: 0px"><?php
                                    echo form_input('multisizet', (isset($_POST['multisizet']) ? $_POST['multisizet'] : ''), 'class="form-control " sizesel data-tab="size" data-key="department_id-section_id-product_items_id" data-id="department,section,product_items" id="multisizet" placeholder="' . lang("select") . " " . lang("size") . '" required="required" style="width:100%" ng-model="prod.multisizet"');
                                    ?></div>
                                <div class="col-md-2"> {{size2[prod.multisizet].code?size2[prod.multisizet].code+'\"':""}}</div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-md-offset-1">
                    <div class="form-group all" >
                        <?= lang("product_name", "name") ?>
                        <?= form_input('name', (isset($_POST['name']) ? $_POST['name'] : ($product ? $product->name : '')), 'class="form-control" id="name" required="required" readonly ng-model="prod.name"'); ?>
                    </div>
                    <div class="form-group all" >
                        <?= lang("product_code", "code") ?> 
                        <?= form_input('code', (isset($_POST['code']) ? $_POST['code'] : ($product ? $product->code : '')), 'class="form-control" id="code" readonly ng-model="prod.barcode" ') ?>
                        <?php // form_input('code', (isset($_POST['code']) ? $_POST['code'] : ($product ? $product->code : '')), 'class="form-control" id="code"  required="required" readonly ng-model="prod.barcode" ')  ?>
                        <span class="help-block"><?= lang('you_scan_your_barcode_too') ?></span>
                    </div>
                    <?= form_hidden('barcode_symbology', (isset($_POST['barcode_symbology']) ? $_POST['barcode_symbology'] : ($product ? $product->barcode_symbology : 'code128')), 'class="form-control" id="barcode_symbology"  required="required" readonly ') ?>

                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group standard">
                            <?= lang("product_tax", "tax_rate") ?>
                            <?php
                            $tr[""] = "";
                            foreach ($tax_rates as $tax) {
                                $tr[$tax->id] = $tax->name;
                            }
                            echo form_dropdown('tax_rate', $tr, (isset($_POST['tax_rate']) ? $_POST['tax_rate'] : ($product ? $product->tax_rate : $Settings->default_tax_rate)), 'class="form-control select" id="tax_rate" placeholder="' . lang("select") . ' ' . lang("product_tax") . '" style="width:100%"');
                            ?>
                        </div>
                        <div class="form-group standard">
                            <?= lang("tax_method", "tax_method") ?>
                            <?php
                            $tm = array('0' => lang('inclusive'), '1' => lang('exclusive'));
                            echo form_dropdown('tax_method', $tm, (isset($_POST['tax_method']) ? $_POST['tax_method'] : ($product ? $product->tax_method : '')), 'class="form-control select" id="tax_method" placeholder="' . lang("select") . ' ' . lang("tax_method") . '" style="width:100%"');
                            ?>
                        </div>
                    <?php } ?>
                    <?php /* ?> <div class="form-group standard">
                      <?= lang("alert_quantity", "alert_quantity") ?>
                      <div class="input-group"> <?= form_input('alert_quantity', (isset($_POST['alert_quantity']) ? $_POST['alert_quantity'] : ($product ? $this->sma->formatQuantity($product->alert_quantity) : '')), 'class="form-control tip" id="alert_quantity"') ?>
                      <span class="input-group-addon">
                      <input type="checkbox" name="track_quantity" id="track_quantity" value="1" <?= ($product ? (isset($product->track_quantity) ? 'checked="checked"' : '') : 'checked="checked"') ?>>
                      </span>
                      </div>
                      </div>
                     * <?php */ ?>
                    <!-- <div class="form-group standard">
                        <?= lang("supplier", "supplier") ?>
                        <div class="row" id="supplier-con">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <?php
                                $bl[""] = "";
                                echo form_dropdown('supplier', $bl, (isset($_POST['text']) ? $_POST['text'] : ($product ? $product->supplier1 : '')), 'id="supplier12" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("supplier") . '" required="required" ng-model="prod.supplier" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                    </div> -->
                    

                    <div class="form-group standard">
                        <?= lang("product_image", "product_image") ?>(<?= byte_format($this->Settings->iwidth * $this->Settings->iheight) ?>)
                        <!--<div class="input-group">-->
                            <input id="product_image" type="file" name="product_image" data-show-upload="false"
                                   data-show-preview="false" accept="image/*" class="form-control file">
<!--                            <div class="input-group-addon no-print btn btn-primary" style="border: 0px;">
                                <a href="#myModalCam" id="ModalCam" data-toggle="modal"  class="external"><i class="fa fa-camera fa-2x" id="addIcon"></i></a>
                            </div>
                        </div>-->
                    </div>
                    <div class="form-group camera">
                        <div id="my_camera" style="border:1px solid #000000;background:#eee; width: 100%; display: none" height="200px"></div>
                        <img src="" id="image_upload_preview" style="border:1px solid #000000;background:#eee; width: 100px;">
                        <div id="results"></div>
                    </div>
                    <div class="form-group camera">
                        <center><button type="button" id="takepicture" class="btn btn-primary">Take Picture with Attached Camera</button>
                            <input type=button value="Take Snapshot" id="tacksnaps" class="btn btn-primary" onClick="take_snapshot()" style="display: none"></center>
                    </div>

                    <div class="form-group standard">
                        <?= lang("product_gallery_images", "images") ?>(<?= byte_format($this->Settings->iwidth * $this->Settings->iheight) ?>)
                        <input id="images" type="file" name="userfile[]" multiple="true" data-show-upload="false"
                               data-show-preview="false" class="form-control file" accept="image/*">
                    </div>
                    <div id="img-details"></div>

                    <div class="row standard">
                        <div class="col-md-8">
                            <div class="form-group all">
                                <!-- <label class="control-label" for="unit"><?= lang("product_qty") ?></label> -->
                                <?php
                                foreach ($warehouses as $warehouse) {
                                    if ($this->Settings->racks) {
                                        // echo "<div class='row'><div class='col-md-12'>";
                                        echo form_hidden('wh_' . $warehouse->id, $warehouse->id) . form_hidden('wh_qty_' . $warehouse->id, '1');
                                        // echo "</div></div>";
                                    } else {
                                        echo form_hidden('wh_' . $warehouse->id, $warehouse->id) . form_hidden('wh_qty_' . $warehouse->id, '1');
                                    }
                                }
                                ?>
                                <label class="control-label" for="unit"><?= lang("Primary_Unit") ?></label>
                                
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?php // lang("unit_per", "unit") ?>
                                <div class="input-group">
                                    <?= form_input('unit', (isset($_POST['unit']) ? $_POST['unit'] : ($product ? $product->uper : '')), 'class="form-control" id="unit" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" required="required" style="width:100%" '); ?>
                                    <div class="input-group-addon no-print">
                                        <a href="<?php echo site_url('system_settings/AddProduct_para/per'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row standard">
                        <!-- <div class="col-md-1">
                            <div class="form-group all">
                                <input type="checkbox" icheck class="checkbox" ng-model="prod.chksecqty" name="sec_qtychk" id="sec_qtychk">
                            </div>
                        </div> -->
                        <div class="col-md-8">
                            <div class="form-group all">
                                <label class="control-label" for="unit"><?= lang("Secondary_Unit") ?></label>
                                <!-- <?php // form_input('squantity', (isset($_POST['squantity']) ? $_POST['squantity'] : ($product ? $this->sma->formatDecimal($product->squantity) : '')), 'class="form-control tip" id="squantity"') ?> -->
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <?= form_input('sunit', (isset($_POST['sunit']) ? $_POST['sunit'] : ($product ? intval($product->sunit) : '')), 'class="form-control" id="sunit" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" style="width:100%"'); ?>
                                    <div class="input-group-addon no-print">
                                        <a href="<?php echo site_url('system_settings/AddProduct_para/per'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <script>
                                $('#squantity').change(function () {
                                    var squantity = $('#squantity').val();
                                    var wh_qtys = $('#wh_qtys').val();

                                    if (wh_qtys % squantity != 0 && squantity != "") {
                                        bootbox.alert('Please Enter Valid Secondary Quentity');
                                        $('#squantity').val("");
                                    }
                                });
                    </script>
                    <!-- <div class="row">
                        <div class="col-md-8">
                            <div class="form-group all" ng-init="prod.cost = '<?= (isset($_POST["cost"]) ? $_POST["cost"] : ($product ? $this->sma->formatDecimal($product->cost) : "")) ?>'">
                                <?= lang("product_cost", "cost") ?>
                                <?= form_input('cost', (isset($_POST['cost']) ? $_POST['cost'] : ($product ? $this->sma->formatDecimal($product->cost) : '')), 'class="form-control tip" id="cost" required="required" ng-model="prod.cost" ng-blur="getProductMargin();"') ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group all">
                                <?= lang("product_per", "cper") ?>
                                <?= form_input('cper', (isset($_POST['cper']) ? $_POST['cper'] : ($product ? $product->cper : '')), 'class="form-control" id="cper" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" required="required" style="width:100%" '); ?>
                            </div>
                        </div>
                    </div>  

                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group all" ng-init="prod.price = '<?= (isset($_POST["price"]) ? $_POST["price"] : ($product ? $this->sma->formatDecimal($product->price) : "")) ?>'">
                                <?= lang("product_price", "price") ?>
                                <?= form_input('price', (isset($_POST['price']) ? $_POST['price'] : ($product ? $this->sma->formatDecimal($product->price) : '')), 'class="form-control tip" id="price" ng-model="prod.price" ') ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group all">
                                <?= lang("product_per", "pper") ?>
                                <div class="">
                                    <?= form_input('pper', (isset($_POST['pper']) ? $_POST['pper'] : ($product ? $product->pper : '')), 'class="form-control" id="pper" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" required="required" style="width:100%"'); ?>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!--<div class="form-group standard">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">

                                    <div class="col-md-3">
                                        <?= lang("Rate", "singlerate") ?>    
                                    </div>
                                    <div class="col-md-1">
                                        <?= lang("Per", "Per") ?> </div>

                                    <div class="col-md-4">
                                        <?= form_input('rateper', (isset($_POST['rateper']) ? $_POST['rateper'] : ($product ? $product->rateper : '')), 'class="form-control" default-attrib data-tab="per" id="rateper" placeholder="' . lang("select") . " " . lang("Unit") . '" required="required" style="width:100%" '); ?>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="checkbox" icheck class="checkbox"  ng-model="prod.roundup" id="extras" value=""/>
                                        <label for="extras" class="padding05"><?= lang('roundup') ?></label>
                                    </div>

                                </div>                                
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" <?= (isset($_POST['ratetype']) && $_POST['ratetype'] == "Single" ? 'checked' : ($product->ratetype == 'Single' ? 'checked' : '')) ?> name="ratetype" value="Single"/>&nbsp;&nbsp;&nbsp;&nbsp;Single</div>
                            <div class="col-md-3"><?php echo form_input('singlerate', (isset($_POST['singlerate']) ? $_POST['singlerate'] : ($product ? $product->singlerate : '')), 'class="form-control" id="singlerate" ng-model="prod.singlerate" placeholder="" style="width:100%;" '); ?></div>
                            <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" <?= (isset($_POST['ratetype']) && $_POST['ratetype'] == "MRP" ? 'checked' : ($product->ratetype == 'MRP' ? 'checked' : '')) ?> name="ratetype"  value="MRP"/>&nbsp;&nbsp;&nbsp;&nbsp;MRP</div>
                            <div class="col-md-3"><?php echo form_input('mrprate', (isset($_POST['mrprate']) ? $_POST['mrprate'] : ($product ? $product->mrprate : '')), 'class="form-control" id="mrprate" placeholder="" style="width:100%;"'); ?></div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" <?= (isset($_POST['ratetype']) && $_POST['ratetype'] == "Multiple" ? 'checked' : ($product->ratetype == 'Multiple' ? 'checked' : '')) ?> name="ratetype" value="Multiple"/>&nbsp;&nbsp;&nbsp;&nbsp;Multiple</div>
                            <div class="col-md-3">
                                <div class="col-md-12" style="padding: 0px"><?php echo form_input('mulratef', (isset($_POST['mulratef']) ? $_POST['mulratef'] : ($product ? $product->mulratef : '')), 'class="form-control" id="mulratef" placeholder="" style="width:100%;"'); ?></div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-3">To</div>
                            <div class="col-md-3" ng-init='size2 =<?= json_encode($sizes) ?>'>
                                <div class="col-md-12" style="padding: 0px"><?php echo form_input('mulratet', (isset($_POST['mulratet']) ? $_POST['mulratet'] : ($product ? $product->mulratet : '')), 'class="form-control" id="mulratet" placeholder="" style="width:100%;"'); ?></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>


                    </div>
                    <div class="">
                        <div class="row ">
                            <div class="col-md-6 form-group"> 
                                <?= lang('hsncode', 'hsncode') ?>
                                <div class="input-group">
                                    <?= form_input('hsnno', (isset($_POST['hsnno']) ? $_POST['hsnno'] : $product->price), 'class="form-control tip"  id="hsnno" ng-model="prod.hsnno" readonly') ?>
                                    <div class="input-group-addon no-print">
                                        <a href="<?php echo site_url('gst/add'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 form-group">
                                <?= lang('gstno', 'gstno') ?>
                                <?= form_input('gstno', (isset($_POST['gstno']) ? $_POST['gstno'] : $product->price), 'class="form-control tip" id="gstno" readonly ng-model="prod.gstno"  ng-blur="getProductMargin()"') ?>
                            </div>
                            <div class="col-md-3">
                                <?= lang('cess', 'cess') ?>
                                <?= form_input('cess', (isset($_POST['cess']) ? $_POST['cess'] : $product->price), 'class="form-control tip" id="cess" readonly ng-model="prod.cess"  ng-blur="getProductMargin()"') ?>
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-8"> &nbsp;&nbsp;<input type="checkbox" icheck class="checkbox" id="addupgetcheck" ng-model="prod.addupgst" name="addupgst">&nbsp;&nbsp;&nbsp;&nbsp;Add-up GST if MRP exceeds 1000+ (%)</div>
                            <div class="col-md-4">
                                <?= form_input('addupgstmrp', (isset($_POST['addupgstmrp']) ? $_POST['addupgstmrp'] : $product->price), 'class="form-control tip" id="addupgstmrp" ng-model="prod.addupgstmrp" readonly ng-blur="getProductMargin()"') ?>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group combod">
                                <?= lang("combo_discount", "combo_discount") ?>
                                <?= form_input('combo_discount', (isset($_POST['combo_discount']) ? $_POST['combo_discount'] : ($product ? $this->sma->formatDecimal($product->combo_discount) : '')), 'class="form-control tip" id="combo_discount" required="required"') ?>
                            </div>
                        </div>
                    </div>
                    <div class="row bundleb">
                        <div class="col-md-12">
                            <div class="form-group ">
                                <?= lang("Batch", "batch") ?>
                                <?= form_input('batch', (isset($_POST['batch']) ? $_POST['batch'] : ($product ? $product->batch : '')), 'class="form-control tip" id="batch" required="required"') ?>
                            </div>
                        </div>
                    </div>
                    <div class="combo" style="display:none;">
                        <div class="form-group">
                            <?= lang("add_product", "add_item") . ' (' . lang('not_with_variants') . ')'; ?>
                            <?php echo form_input('add_item', '', 'class="form-control ttip" id="add_item" data-placement="top" data-trigger="focus" data-bv-notEmpty-message="' . lang('please_add_items_below') . '" placeholder="' . $this->lang->line("add_item") . '"'); ?>
                        </div>
                        <div class="control-group table-group">
                            <label class="table-label" for="combo">{{prod.protype}} <?= lang("products"); ?></label>
                            <div class="controls table-controls">
                                <table id="prTable" class="table items table-striped table-bordered table-condensed table-hover">
                                    <thead>
                                        <tr>
                                            <th class="col-md-5 col-sm-5 col-xs-5"><?= lang("product_name") . " (" . $this->lang->line("product_code") . ")"; ?></th>
                                            <th class="col-md-2 col-sm-2 col-xs-2"><?= lang("quantity"); ?></th>
                                            <th class="col-md-3 col-sm-3 col-xs-3"><?= lang("unit_price"); ?></th>
                                            <th class="col-md-1 col-sm-1 col-xs-1 text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="digital" style="display:none;">
                        <div class="form-group digital">
                            <?= lang("digital_file", "digital_file") ?>
                            <input id="digital_file" type="file" name="digital_file" data-show-upload="false"
                                   data-show-preview="false" class="form-control file">
                        </div>
                    </div>
                </div>-->
                <div class="form-group">
                    <?= lang("Product_Classification", "product_classification") ?>
                    <div class="row">
                        <div class="col-md-4"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="cf1" <?= ($_POST['cf1'] == "regular" ? "checked='checked'" : '') ?> value="regular" id="prod_classification_regular"/>&nbsp;&nbsp;&nbsp;&nbsp;Regular</div>
                        <div class="col-md-4"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="cf1" <?= ($_POST['cf1'] == "economical" ? "checked='checked'" : '') ?> value="economical" id="prod_classification_economical" />&nbsp;&nbsp;&nbsp;&nbsp;Economical</div>
                        <div class="col-md-4"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="cf1" <?= ($_POST['cf1'] == "premium" ? "checked='checked'" : '') ?> value="premium" id="prod_classification_premium" />&nbsp;&nbsp;&nbsp;&nbsp;Premium</div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <?php echo form_submit('add_product', "Save", 'class="btn btn-primary" '); ?>
                    </div>
                </div>
                <?= form_close(); ?>
            </div>


        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        var audio_success = new Audio('<?= $assets ?>sounds/sound2.mp3');
        var audio_error = new Audio('<?= $assets ?>sounds/sound3.mp3');
        var items = {};
<?php
if ($combo_items) {
    foreach ($combo_items as $item) {
        //echo 'ietms['.$item->id.'] = '.$item.';';
        if ($item->code) {
            echo 'add_product_item(' . json_encode($item) . ');';
        }
    }
}
?>
<?= isset($_POST['cf']) ? '$("#extras").iCheck("check");' : '' ?>
        $('#extras').on('ifChecked', function () {
            $('#extras-con').slideDown();
        });
        $('#extras').on('ifUnchecked', function () {
            $('#extras-con').slideUp();
        });
        $('.attributes').on('ifChecked', function (event) {
            $('#options_' + $(this).attr('id')).slideDown();
        });
        $('.attributes').on('ifUnchecked', function (event) {
            $('#options_' + $(this).attr('id')).slideUp();
        });
        //$('#cost').removeAttr('required');
        $('#type').change(function () {
            var t = $(this).val();
//            alert(t);
            if (t !== 'standard') {
                $('.standard').slideUp();
                $('#cost').attr('required', 'required');
                $('#track_quantity').iCheck('uncheck');
                $('form[data-toggle="validator"]').bootstrapValidator('addField', 'cost');
            } else {
                $('.standard').slideDown();
                $('#track_quantity').iCheck('check');
                $('#cost').removeAttr('required');
                $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'cost');
            }
            if (t !== 'digital') {
                $('.digital').slideUp();
                $('#digital_file').removeAttr('required');
                $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'digital_file');
            } else {
                $('.digital').slideDown();
                $('#digital_file').attr('required', 'required');
                $('form[data-toggle="validator"]').bootstrapValidator('addField', 'digital_file');
            }
            if (t !== 'combo' && t !== 'bundle') {
                $('.combo').slideUp();
            } else {
                $('.combo').slideDown();
            }
            if (t !== 'combo') {
                $('.combod').slideUp();
            } else {
                $('.combod').slideDown();
            }
            if (t !== 'bundle') {
                $('.bundleb').slideUp();
            } else {
                $('.bundleb').slideDown();
            }
        });
        /* var t = $('#type').val();
        if (t !== 'standard') {
            $('.standard').slideUp();
            $('#cost').attr('required', 'required');
            $('#track_quantity').iCheck('uncheck');
            $('form[data-toggle="validator"]').bootstrapValidator('addField', 'cost');
        } else {
            $('.standard').slideDown();
            $('#track_quantity').iCheck('check');
            $('#cost').removeAttr('required');
            $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'cost');
        }
        if (t !== 'digital') {
            $('.digital').slideUp();
            $('#digital_file').removeAttr('required');
            $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'digital_file');
        } else {
            $('.digital').slideDown();
            $('#digital_file').attr('required', 'required');
            $('form[data-toggle="validator"]').bootstrapValidator('addField', 'digital_file');
        }
        if (t !== 'combo' && t !== 'bundle') {
            $('.combo').slideUp();
        } else {
            $('.combo').slideDown();
        }
        if (t !== 'combo') {
            $('.combod').slideUp();
        } else {
            $('.combod').slideDown();
        }
        if (t !== 'bundle') {
            $('.bundleb').slideUp();
        } else {
            $('.bundleb').slideDown();
            alert();
        } */
        $("#add_item").autocomplete({
            source: function (request, response) {
                $.getJSON("<?= site_url('products/suggestionscombo'); ?>",
                        {
                            store_id: $('#companies').val(),
                            department: $('#department').val(),
                            section: $('#section').val(),
                            product_items: $('#product_items').val(),
                            type_id: $('#type_id').val(),
                            brands: $('#brands').val(),
                            sizeangle: $('input[type=radio][name=sizeangle]:checked').val(),
                            size: $('#size').val(),
                            term: $("#add_item").val()
                        },
                        response);
            },
            minLength: 1,
            autoFocus: false,
            delay: 200,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');
                } else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                } else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var row = add_product_item(ui.item);
                    if (row) {
                        $(this).val('');
                    }
                } else {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>');
                }
            }
        });
        $('#add_item').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                $(this).autocomplete("search");
            }
        });


<?php
if ($this->input->post('type') == 'combo') {
    $c = sizeof($_POST['combo_item_code']);
    for ($r = 0; $r <= $c; $r++) {
        if (isset($_POST['combo_item_code'][$r]) && isset($_POST['combo_item_quantity'][$r]) && isset($_POST['combo_item_price'][$r])) {
            $items[] = array('id' => $_POST['combo_item_id'][$r], 'name' => $_POST['combo_item_name'][$r], 'code' => $_POST['combo_item_code'][$r], 'qty' => $_POST['combo_item_quantity'][$r], 'price' => $_POST['combo_item_price'][$r]);
        }
    }
    echo 'var ci = ' . json_encode($items) . ';
            $.each(ci, function() { add_product_item(this); });
            ';
}
?>
        function add_product_item(item) {
            var pricex = 0;
            if (item == null) {
                return false;
            }
            item_id = item.id;
            if (items[item_id]) {
                items[item_id].qty = (parseFloat(items[item_id].qty) + 1).toFixed(2);
            } else {
                items[item_id] = item;
            }

            $("#prTable tbody").empty();
            $.each(items, function () {
                var row_no = this.id;
                var newTr = $('<tr id="row_' + row_no + '" class="item_' + this.id + '"></tr>');
                tr_html = '<td><input name="combo_item_id[]" type="hidden" value="' + this.id + '"><input name="combo_item_name[]" type="hidden" value="' + this.name + '"><input name="combo_item_code[]" type="hidden" value="' + this.code + '"><span id="name_' + row_no + '">' + this.name + ' (' + this.code + ')</span></td>';
                tr_html += '<td><input class="form-control text-center" name="combo_item_quantity[]" type="text" value="' + formatDecimal(this.qty) + '" data-id="' + row_no + '" data-item="' + this.id + '" id="quantity_' + row_no + '" onClick="this.select();"></td>';
                tr_html += '<td><input class="form-control text-center" name="combo_item_price[]" type="text" value="' + formatDecimal(this.price) + '" data-id="' + row_no + '" data-item="' + this.id + '" id="combo_item_price_' + row_no + '" onClick="this.select();"></td>';
                tr_html += '<td class="text-center"><i class="fa fa-times tip del" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
                pricex += parseFloat(this.price * this.qty);
                newTr.html(tr_html);
                newTr.prependTo("#prTable");
                $('#cost').val(pricex);
                $('#price').val(pricex);
            });
            $('.item_' + item_id).addClass('warning');
            //audio_success.play();
            return true;
        }

        $(document).on('click', '.del', function () {
            var pricex = $('#price').val();
            var id = $(this).attr('id');
            delete items[id];
            pricex -= parseFloat($("#combo_item_price_" + id).val() * $("#quantity_" + id).val());
            $('#cost').val(pricex);
            $('#price').val(pricex);
            $(this).closest('#row_' + id).remove();
        });
        var su = 2;
        $('#deleteSupplier').hide();
        $('#addSupplier').click(function () {
            if (su <= 5) {
                $('#supplier_1').select2('destroy');
                var html = '<div id="list_' + su + '"><div style="clear:both;height:15px;" ></div><div class="row"><div class="col-md-8 col-sm-8 col-xs-8"><input type="hidden" name="supplier_' + su + '", class="form-control" id="supplier_' + su + '" placeholder="<?= lang("select") . ' ' . lang("supplier") ?>" style="width:100%;display: block !important;" /></div><div class="col-md-4 col-sm-4 col-xs-4"><input type="text" name="supplier_' + su + '_price" class="form-control tip" id="supplier_' + su + '_price" placeholder="<?= lang('supplier_price') ?>" /></div></div></div>';
                $('#ex-suppliers').append(html);
                var sup = $('#supplier_' + su);
                $('#deleteSupplier').show();
                suppliers(sup);
                su++;
            } else {
                bootbox.alert('<?= lang('max_reached') ?>');
                return false;
            }
        });
        $('#deleteSupplier').click(function () {
            if (su == 3) {
                $('#deleteSupplier').hide();
            }
            if (su > 2) {
                //                $('#supplier_1').select2('destroy');
                //                var html = '<div style="clear:both;height:15px;"></div><div class="row"><div class="col-md-8 col-sm-8 col-xs-8"><input type="hidden" name="supplier_' + su + '", class="form-control" id="supplier_' + su + '" placeholder="<?= lang("select") . ' ' . lang("supplier") ?>" style="width:100%;display: block !important;" /></div><div class="col-md-4 col-sm-4 col-xs-4"><input type="text" name="supplier_' + su + '_price" class="form-control tip" id="supplier_' + su + '_price" placeholder="<?= lang('supplier_price') ?>" /></div></div>';
                $('#ex-suppliers').find('#list_' + (su - 1)).remove();
                //                var sup = $('#supplier_' + su);
                //                suppliers(sup);
                su--;
                return true;
            } else {
                bootbox.alert('Cant delete!');
                return false;
            }
        });
        var _URL = window.URL || window.webkitURL;
        $("input#images").on('change.bs.fileinput', function () {
            var ele = document.getElementById($(this).attr('id'));
            var result = ele.files;
            $('#img-details').empty();
            for (var x = 0; x < result.length; x++) {
                var fle = result[x];
                for (var i = 0; i <= result.length; i++) {
                    var img = new Image();
                    img.onload = (function (value) {
                        return function () {
                            ctx[value].drawImage(result[value], 0, 0);
                        }
                    })(i);
                    img.src = 'images/' + result[i];
                }
            }
        });
        var variants = <?= json_encode($vars); ?>;
        $(".select-tags").select2({
            tags: variants,
            tokenSeparators: [","],
            multiple: true
        });
        $(document).on('ifChecked', '#attributes', function (e) {
            $('#attr-con').slideDown();
        });
        $(document).on('ifUnchecked', '#attributes', function (e) {
            $(".select-tags").select2("val", "");
            $('.attr-remove-all').trigger('click');
            $('#attr-con').slideUp();
        });
        $('#addAttributes').click(function (e) {
            e.preventDefault();
            var attrs_val = $('#attributesInput').val(), attrs;
            attrs = attrs_val.split(',');
            console.log(attrs);
            for (var i in attrs) {
                if (attrs[i] !== '') {
                    $('#attrTable').show().append('<tr class="attr"><td><input type="hidden" name="attr_name[]" value="' + attrs[i] + '"><span>' + attrs[i] + '</span></td><td class="code text-center"><input type="hidden" name="attr_warehouse[]" value=""><span></span></td><td class="quantity text-center"><input type="hidden" name="attr_quantity[]" value=""><span></span></td><td class="cost text-right"><input type="hidden" name="attr_cost[]" value="0"><span>0</span></td><td class="price text-right"><input type="hidden" name="attr_price[]" value="0"><span>0</span></span></td><td class="text-center"><i class="fa fa-times delAttr"></i></td></tr>');
                }
            }
        });

        //$('#attributesInput').on('select2-blur', function(){
        //    $('#addAttributes').click();
        //});
//        $('#addupgstmrp').prop('disabled', 'disabled');
//        $('#addupgetcheck').prop('disabled', 'disabled');
//        $(document).on('blur, change', '#price', function () {
//            var price = $('#price').val();
//            if (parseFloat(price) <= 1000) {
//                $('#addupgstmrp').prop('disabled', 'disabled');
//            } else {
//                $('#addupgstmrp').prop('disabled', '');
//            }
//        });

        $(document).on('click', '.delAttr', function () {
            $(this).closest("tr").remove();
        });
        $(document).on('click', '.attr-remove-all', function () {
            $('#attrTable tbody').empty();
            $('#attrTable').hide();
        });
        var row, warehouses = <?= json_encode($warehouses); ?>;
        $(document).on('click', '.attr td:not(:last-child)', function () {
            row = $(this).closest("tr");
            $('#aModalLabel').text(row.children().eq(0).find('span').text());
            $('#awarehouse').select2("val", (row.children().eq(1).find('input').val()));
            $('#aquantity').val(row.children().eq(2).find('input').val());
            $('#acost').val(row.children().eq(3).find('span').text());
            $('#aprice').val(row.children().eq(4).find('span').text());
            $('#aModal').appendTo('body').modal('show');
        });
        $(document).on('click', '#updateAttr', function () {
            var wh = $('#awarehouse').val(), wh_name;
            $.each(warehouses, function () {
                if (this.id == wh) {
                    wh_name = this.name;
                }
            });
            row.children().eq(1).html('<input type="hidden" name="attr_warehouse[]" value="' + wh + '"><input type="hidden" name="attr_wh_name[]" value="' + wh_name + '"><span>' + wh_name + '</span>');
            row.children().eq(2).html('<input type="hidden" name="attr_quantity[]" value="' + $('#aquantity').val() + '"><span>' + decimalFormat($('#aquantity').val()) + '</span>');
            row.children().eq(3).html('<input type="hidden" name="attr_cost[]" value="' + $('#acost').val() + '"><span>' + currencyFormat($('#acost').val()) + '</span>');
            row.children().eq(4).html('<input type="hidden" name="attr_price[]" value="' + $('#aprice').val() + '"><span>' + currencyFormat($('#aprice').val()) + '</span>');
            $('#aModal').modal('hide');
        });
    });

<?php if ($product) { ?>
        $(document).ready(function () {
            var t = "<?= $product->type ?>";
            if (t !== 'standard') {
                $('.standard').slideUp();
                $('#cost').attr('required', 'required');
                $('#track_quantity').iCheck('uncheck');
                $('form[data-toggle="validator"]').bootstrapValidator('addField', 'cost');
            } else {
                $('.standard').slideDown();
                $('#track_quantity').iCheck('check');
                $('#cost').removeAttr('required');
                $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'cost');
            }
            if (t !== 'digital') {
                $('.digital').slideUp();
                $('#digital_file').removeAttr('required');
                $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'digital_file');
            } else {
                $('.digital').slideDown();
                $('#digital_file').attr('required', 'required');
                $('form[data-toggle="validator"]').bootstrapValidator('addField', 'digital_file');
            }
            if (t !== 'combo') {
                $('.combo').slideUp();
                //$('#add_item').removeAttr('required');
                //$('form[data-toggle="validator"]').bootstrapValidator('removeField', 'add_item');
            } else {
                $('.combo').slideDown();
                //$('#add_item').attr('required', 'required');
                //$('form[data-toggle="validator"]').bootstrapValidator('addField', 'add_item');
            }
            $("#code").parent('.form-group').addClass("has-error");
            $("#code").focus();
            $("#product_image").parent('.form-group').addClass("text-warning");
            $("#images").parent('.form-group').addClass("text-warning");
            $.ajax({
                type: "get", async: false,
                //                url: "<?= site_url('products/getSubCategories') ?>",
                url: "<?= site_url('products/getSubCategories') ?>/" + <?= $product->category_id ? $product->category_id : 0 ?>,
                dataType: "json",
                success: function (scdata) {
                    if (scdata != null) {
                        $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                            placeholder: "<?= lang('select_category_to_load') ?>",
                            data: scdata
                        });
                    }
                }
            });
    <?php if ($product->supplier1) { ?>
                select_supplier('supplier1', "<?= $product->supplier1; ?>");
                $('#supplier_price').val("<?= $product->supplier1price == 0 ? '' : $this->sma->formatDecimal($product->supplier1price); ?>");
    <?php } ?>
    <?php if ($product->supplier2) { ?>
                $('#addSupplier').click();
                select_supplier('supplier_2', "<?= $product->supplier2; ?>");
                $('#supplier_2_price').val("<?= $product->supplier2price == 0 ? '' : $this->sma->formatDecimal($product->supplier2price); ?>");
    <?php } ?>
    <?php if ($product->supplier3) { ?>
                $('#addSupplier').click();
                select_supplier('supplier_3', "<?= $product->supplier3; ?>");
                $('#supplier_3_price').val("<?= $product->supplier3price == 0 ? '' : $this->sma->formatDecimal($product->supplier3price); ?>");
    <?php } ?>
    <?php if ($product->supplier4) { ?>
                $('#addSupplier').click();
                select_supplier('supplier_4', "<?= $product->supplier4; ?>");
                $('#supplier_4_price').val("<?= $product->supplier4price == 0 ? '' : $this->sma->formatDecimal($product->supplier4price); ?>");
    <?php } ?>
    <?php if ($product->supplier5) { ?>
                $('#addSupplier').click();
                select_supplier('supplier_5', "<?= $product->supplier5; ?>");
                $('#supplier_5_price').val("<?= $product->supplier5price == 0 ? '' : $this->sma->formatDecimal($product->supplier5price); ?>");
    <?php } ?>
            function select_supplier(id, v) {
                $('#' + id).val(v).select2({
                    minimumInputLength: 1,
                    data: [],
                    initSelection: function (element, callback) {
                        $.ajax({
                            type: "get", async: false,
                            url: "<?= site_url('suppliers/getSupplier') ?>/" + $(element).val(),
                            dataType: "json",
                            success: function (data) {
                                callback(data[0]);
                            }
                        });
                    },
                    ajax: {
                        url: site.base_url + "suppliers/suggestions",
                        dataType: 'json',
                        quietMillis: 15,
                        data: function (term, page) {
                            return {
                                term: term,
                                limit: 10
                            };
                        },
                        results: function (data, page) {
                            if (data.results != null) {
                                return {results: data.results};
                            } else {
                                return {results: [{id: '', text: 'No Match Found'}]};
                            }
                        }
                    }
                }); //.select2("val", "<?= $product->supplier1; ?>");
            }

            var whs = $('.wh');
            $.each(whs, function () {
                $(this).val($('#r' + $(this).attr('id')).text());
            });
        });

<?php } ?>

</script>

<div class="modal" id="aModal" tabindex="-1" role="dialog" aria-labelledby="aModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="aModalLabel"><?= lang('add_product_manually') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="awarehouse" class="col-sm-4 control-label"><?= lang('warehouse') ?></label>
                        <div class="col-sm-8">
                            <?php
                            $wh[''] = '';
                            foreach ($warehouses as $warehouse) {
                                $wh[$warehouse->id] = $warehouse->name;
                            }
                            echo form_dropdown('warehouse', $wh, '', 'id="awarehouse" class="form-control"');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="aquantity" class="col-sm-4 control-label"><?= lang('quantity') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="aquantity">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="acost" class="col-sm-4 control-label"><?= lang('cost') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="acost">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="aprice" class="col-sm-4 control-label"><?= lang('price') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="aprice">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="updateAttr"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>
<!-- <script type="text/javascript" src="<?= $assets ?>js/webcam.js"></script> -->
<div class="modal" id="myModalCam" tabindex="-1" role="dialog" aria-labelledby="aModalLabel" aria-hidden="true">

    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="aModalLabel">Capture Photo</h4>
            </div>
            <div class="modal-body" id="pr_popover_content">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
navigator.getMedia = ( navigator.getUserMedia || // use the proper vendor prefix
                       navigator.webkitGetUserMedia ||
                       navigator.mozGetUserMedia ||
                       navigator.msGetUserMedia);

navigator.getMedia({video: true}, function() {
  // webcam is available
  
}, function() {
    // webcam is Not available
 $('.camera').slideUp();
});
</script>


<script language="JavaScript">
    Webcam.set({    
        width: 300,
        height: 400,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
    Webcam.attach('#my_camera');
</script>
<script language="JavaScript">
    function take_snapshot() {
        // take snapshot and get image data
        Webcam.snap(function (data_uri) {
            readURL("input[name='webcam']");
            Webcam.upload(data_uri, 'products/savecamImage', function (code, text) {
                $("#image_upload_preview").attr("src", data_uri);
                $("#productPhoto").val(text);
                $('#my_camera').css('display', 'none');
                $('#image_upload_preview').css('display', 'inline-block');
                $('#takepicture').css('display', 'inline-block');
                $('#tacksnaps').css('display', 'none');
            });
        });
    }
</script>


<script>
//
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            $("#supplierphoto").val('');
        }
    }

    $("#store_logo_up").change(function () {
        readURL(this);
    });
    $("#takepicture").click(function () {
        $('#my_camera').css('display', 'inline-block');
        $('#tacksnaps').css('display', 'inline-block');
        $('#image_upload_preview').css('display', 'none');
        $('#takepicture').css('display', 'none');
    });
    $(document).on('click', '#ModalCam', function (e) {
        $('#myModalCam').appendTo("body").modal('show');
        return false;
    });
//    $(document).on('click', '#ModalCam', function (e) {
//        $('#myModalProductPara').appendTo("body").modal('show');
//        return false;
//    });

    $('#mulcolor').change(function () {
//       $('.select2-search-choice > div').each(function( i ) {
//           alert($(this).html());
//       });

    });


    $('#wh_qtys').change(function () {
        var v = $('#addproduct').serialize();
        var qty = $('#wh_qtys').val();
//        #sec_qtychk
        $('#modal-loading').show();
        $.ajax({
            type: "get",
            data: v,
            async: false,
            url: "<?= site_url('products/getProSquentity') ?>?" + v,
            dataType: "json",
            success: function (scdata) {
                if (scdata != 0) {
                    var sqty = parseInt(qty) / parseInt(scdata.no_of_pic);
                    $('#squantity').val(Number(Math.round(sqty)));
                }
            },
            error: function () {
                bootbox.alert('<?= lang('ajax_error') ?>');
                $('#modal-loading').hide();
            }
        });
        $('#modal-loading').hide();
    });


    $('#multisizet').change(function () {
        var mf = $('#multisizef').val();
        var mt = $('#multisizet').val();
        var l = $("select[name='colorassorted[]'] option:selected").length;
        if (l != 0) {
            $('#modal-loading').show();
            $.ajax({
                type: "get",
                data: {'multisizet': mt, 'multisizef': mf},
                async: false,
                url: "<?= site_url('products/check_multipalsizeVal') ?>",
                dataType: "json",
                success: function (scdata) {
                    if (l != scdata) {
                        bootbox.alert('Please Enter Valid Color Range And Size');
                    }
                },
                error: function () {
                    bootbox.alert('<?= lang('ajax_error') ?>');
                    $('#modal-loading').hide();
                }
            });
            $('#modal-loading').hide();
        }
    });
    $('#mulcolor').change(function () {
        var mf = $('#multisizef').val();
        var mt = $('#multisizet').val();
        var l = $("select[name='colorassorted[]'] option:selected").length;
        if (l != 0) {
            $('#modal-loading').show();
            $.ajax({
                type: "get",
                data: {'multisizet': mt, 'multisizef': mf},
                async: false,
                url: "<?= site_url('products/check_multipalsizeVal') ?>",
                dataType: "json",
                success: function (scdata) {
                    if (scdata != 0) {
                        if (l != scdata) {
                            bootbox.alert('Please Enter Valid Color Range And Size');
//                        $('#multisizet').val('5');
                        }
                    }
                },
                error: function () {
                    bootbox.alert('<?= lang('ajax_error') ?>');
                    $('#modal-loading').hide();
                }
            });
            $('#modal-loading').hide();
        }
    });


    $("#addproduct").submit(function (event) {
        var mf = $('#multisizef').val();
        var mt = $('#multisizet').val();
        var l = $("select[name='colorassorted[]'] option:selected").length;
        if (l != 0) {
            $('#modal-loading').show();
            $.ajax({
                type: "get",
                data: {'multisizet': mt, 'multisizef': mf},
                async: false,
                url: "<?= site_url('products/check_multipalsizeVal') ?>",
                dataType: "json",
                success: function (scdata) {
                    if (scdata != 0) {
                        if (l != scdata) {
                            bootbox.alert('Please Enter Valid Color Range And Size');

                            event.preventDefault();
                            $('input[name="add_product"]').removeAttr('disabled');
                            return false;
//                        $('#multisizet').val('5');
                        }
                    }
                },
                error: function () {
                    bootbox.alert('<?= lang('ajax_error') ?>');
                    $('#modal-loading').hide();
                    $('input[name="add_product"]').removeAttr('disabled');
                    event.preventDefault();
                    return false;
                }
            });
            $('#modal-loading').hide();
        }
    });

    $(document).ready(function () {
        $('#code').parent().removeClass("has-error");
    });
    $(document).ready(function () {
        var storeid = $('#companies').val();
        if (storeid != "") {
            $.ajax({
                type: "get",
                async: false,
                url: "<?= site_url('products/getsupplierfrompurchase') ?>/" + storeid,
                dataType: "json",
                success: function (scdata) {
                    $('#supplier12').select2('val', '');
                    $('#supplier12').html('');
                    $.each(scdata, function (k, val) {
                        var opt = $('<option />');
                        opt.val(val.id);
                        opt.text(val.name + ' (' + val.code + ')');
                        $('#supplier12').append(opt);
                    })

                },
                error: function () {
                    bootbox.alert('<?= lang('ajax_error') ?>');
                    $('#modal-loading').hide();
                }
            });
        }
    });

</script>

