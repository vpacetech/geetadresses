<script type="text/javascript">
    $(document).ready(function () {
//        $('#category').val('<?= $category_id ?>');

        $('#btnsubmit').click(function () {
            var barcode = $('#barcode').val();
            window.location.replace("<?php echo site_url('products/print_labels'); ?>?barcode=" + barcode);
            return false;
        });
    });
</script>
<div class="box">
    <div class="box-header no-print">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('print_labels'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <?php if (isset($print_link)) { ?>
                <li class="dropdown" style="margin-right:10px;"><?= $print_link; ?></li>
                    <?php } ?>
                <a href="<?php echo site_url('products/showlablePrint/' . $category_id . '/' . $type . '/' . $per_page . '?barcode=' . $barcode); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="icon fa fa-print"></i></a>
                <?php if (isset($inputs)) { ?><li class="dropdown" style="margin-right:10px;"><a href="#" id="labelPrinter" class="tip" title="<?= lang('label_printer') ?>"><i class="icon fa fa-print"></i> <?= lang('label_printer') ?></a></li><?php } ?>
                <!--<li class="dropdown"><a href="javascript:void();" onclick="window.print();" id="print-icon" class="tip" title="<?= lang('print') ?>"><i class="icon fa fa-print"></i></a></li>-->
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?php echo lang('print_barcode_heading'); ?></p>
                <div class="well well-sm no-print">
                    <?php
                    $barcode = $this->input->get('barcode'); ?>
                    <div class="col-md-6">
                        <?php
                        echo form_input('barcode', $barcode, 'class="tip form-control" id="barcode" placeholder="Enter Product Barcode" required="required"');
                        ?>
                    </div>
                    <div class="col-md-3">
                        <button type="button" class="btn btn-primary" id="btnsubmit">Search</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <?php if ($r != 1) { ?>

                    <?php
                    if (!empty($links)) {
                        echo '<div class="text-center">' . $links . '</div>';
                    }
                    ?>
                    <?php echo $html; ?>
                    <?php
                    if (!empty($links)) {
                        echo '<div class="text-center">' . $links . '</div>';
                    }
                    ?>
                    <?php
                } else {
                    echo '<h3>' . $this->lang->line('empty_category') . '</h3>';
                }
                ?>
            </div>
        </div>
    </div>
</div>
<?php if (isset($inputs)) { ?>
    <div id="formData" style="display: none;">
        <?= form_open('products/print_labels2', 'id="PrintLabels"'); ?>
        <?= $inputs; ?>
        <?= form_hidden('print_selected', 1); ?>
        <?= form_close(); ?>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#labelPrinter').click(function (e) {
                e.preventDefault();
                //$('#PrintLabels').submit();
                var fdata = $('#formData').html();
                Popup(fdata);
            });
        });
        function Popup(data) {
            var mywindow = window.open('', 'sma_popup', 'height=600,width=900');
            mywindow.document.write('<!DOCTYPE html><html><head><title>Print</title>');
            mywindow.document.write('<link rel="stylesheet" href="<?= $assets; ?>bootstrap/css/bootstrap.min.css" type="text/css" />');
            mywindow.document.write('<style>a {color: #333;} #totaltbl td, #totaltbl th { vertical-align: middle; }</style>');
            mywindow.document.write('</head><body>');
            mywindow.document.write(data);
            mywindow.document.write('<script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"><\/script>');
            mywindow.document.write('<script>setInterval(function(){$(\'#PrintLabels\').submit();}, 10);<\/script>');
            mywindow.document.write('</body></html>');
        }
    </script>
<?php } ?>

