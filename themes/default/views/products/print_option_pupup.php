<style>
    .close{
        font-size: 19px !important;
    }
    .heading{
        margin-top:14px !important;
        border-top: 1px solid black;
    }
    .heading1{
        padding-top: 7px;
    }
    .modal-header{
        border-bottom: none !important;
    }
</style>
<?php echo $modal_js ?>
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
        </div>

        <form data-toggle="validator" role="form" id="printoptinform">
            <div class="modal-body">
                <div >
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                        <h2>Print Barcode </h2>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 form-group">
                        <div class="radio">
                            <label><input type="radio" name="pinttype" id="all" value="all"> &nbsp;&nbsp;All Serial Numbers </label>
                        </div>
                    </div>
                </div>

                <input type="hidden" value="<?= $type ?>" name="type">
                <input type="hidden" value="<?= $ups ?>" name="ups">
                <input type="hidden" value="<?= $per_page ?>" name="perpage">
                <input type="hidden" value="<?= $barcode ?>" name="barcode">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 form-group">
                        <div class="radio">
                            <label><input type="radio" name="pinttype" id="from" value="fromto"> &nbsp;&nbsp;From </label>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 form-group">
                        <input type="text" name="printfrom" placeholder="From" readonly class="form-control" id="printfrom">
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 form-group">
                        <span> - To </span>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 form-group">
                        <input type="text" name="printto" placeholder="To"  readonly class="form-control" id="printto" data-bv-notempty="true" data-bv-regexp="true" data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="only consist of digits">
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 form-group">
                        <div class="radio">
                            <label><input type="radio" name="prints" id="prints"  value="fromto"> &nbsp;&nbsp;Prints </label>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 form-group">
                        <input type="text" name="peritem" placeholder="Per Item" class="form-control" value="1" id="peritem">
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 form-group">
                        <span> Per Item </span>
                    </div>
                </div>

            </div>
            <div class="modal-footer" style="clear:both;text-align: left;">
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                    <div class="col-md-4 col-md-offset-5">
                        <?php echo form_submit('add_coupon', 'OK', 'id="add_transfer" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                    </div>
                </div>
            </div>
        </form>
    </div><!--end body div-->

    <!--end foter div-->

</div>
<script type="text/javascript" src="<?= $assets ?>js/common.js"></script>

<script type="text/javascript">
    $("#printoptinform").submit(function (event) {
        event.preventDefault();
    });
//        offer_description

    $(document).on('ifChecked', '#all', function (e) {
        $('#printfrom').prop("readonly", "readonly");
        $('#printto').prop("readonly", "readonly");

    });
    $(document).on('ifUnchecked', '#all', function (e) {
        $('#printfrom').prop("readonly", "");
        $('#printto').prop("readonly", "");
    });

    $(document).on('ifChecked', '#from', function (e) {
        $('#printfrom').prop("readonly", "");
        $('#printto').prop("readonly", "");
    });
    $(document).on('ifUnchecked', '#from', function (e) {

        $('#printfrom').prop("readonly", "readonly");
        $('#printto').prop("readonly", "readonly");
    });

    $("#printoptinform").unbind().submit(function (e) {
        
        var $data = $(this).serialize();
        var url = "<?= site_url('products/print_serial_barcodes') ?>?" + $data;
        var ter = '';
        e.preventDefault();
        window.open(url, "popupWindow", "width=1024, height=687, scrollbars=yes");
    });


</script>
