
<style>
    .border_left{border-left:0 !important;border-top:0 !important;border-right:0 !important;}
     .border_right{border-left:0 !important;border-top:0 !important;}
    @media print {
        @font-face {
            font-family: 'Ubuntu';
            font-style: normal;
            font-weight: 400;
            src: url('../fonts/Ubuntu-R.ttf');
        }
        body{
            font-family: 'Ubuntu', sans-serif;
            line-height: 1px;
            color: #000;
        }


        @page { 
/*           size: 79mm auto;*/
            size: portrait;  

        }
        .container{
            width: 79mm !important;
            margin-left: 0px !important;
            font-size: 6px !important;
        } 
        #printhtml{width:79mm !important;}
        .table{width:79mm !important;margin-bottom: 0 !important;}
        .small_size{font-size: 4px !important;}
        .small_size{font-size: 4px !important;}
        .border-zero{ border: none !important;font-size:4px !important; } 
         .table-bordered>tfoot>tr>td.border_left{border-left:0 !important;border-top:0 !important;border-right:0 !important;}
     .table-bordered>tfoot>tr>td.border_right{border-left:0 !important;border-top:0 !important;}
           .table > tbody > tr > td.border_left{border-left:0 !important;border-top:0 !important;border-right:0 !important;}
     .table > tbody > tr > td.border_right{border-left:0 !important;border-top:0 !important;}
        .border-zerotop{ border-top: none !important; border-bottom: none !important; } 
        .table > tbody > tr > td.small_size{font-size: 8px !important;}
	.table > tbody > tr.small_size{font-size: 8px !important;}
	.table > tbody > tr > td.border-zero{ border: none !important;font-size:8px !important; } 
	.table > tbody > tr > td.border-zerotop{ border-top: none !important; border-bottom: none !important; } 
        .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td{padding:8px 2px !important;}
    }
</style>  
<div class="container" style="background-color:#ffffff;">
    <div class="row">

        <div class="box">
            <div class="box-header no-print">
                <h2 class="blue"></h2>
                <div class="box-icon">
                    <ul class="btn-tasks">

                        <li class="dropdown"><a  onclick="window.print();" id="print-icon" class="tip" title="<?= lang('print') ?>"><i class="icon fa fa-print"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="printhtml">
            
        
        <table class="table table-bordered" >
            <tr>
                <td style="width:30%;" >
                    <img src="<?= base_url() . 'assets/uploads/logos/' . $biller->logo; ?>"
                         alt="<?= $biller->company != '-' ? $biller->company : $biller->name; ?>">
                    <!--<img src="images/logo.png" alt="Geeta Dresses">-->
                </td>
                <td>
                    <address>
                        <h2 class=""><?= $biller->company != '-' ? $biller->name : $biller->company; ?></h2>
                        <?= $biller->company ? "" : "Attn: " . $biller->name ?>
                        <?php
                        echo $biller->address . "<br>" . $biller->city . " " . $biller->postal_code . " " . $biller->state . "<br>" . $biller->country;

                        echo "<p>";

                        if ($biller->cf1 != "-" && $biller->cf1 != "") {
                            echo "<br>" . lang("scf1") . ": " . $biller->cf1;
                        }
                        if ($biller->cf2 != "-" && $biller->cf2 != "") {
                            echo "<br>" . lang("scf2") . ": " . $biller->cf2;
                        }
                        if ($biller->cf3 != "-" && $biller->cf3 != "") {
                            echo "<br>" . lang("scf3") . ": " . $biller->cf3;
                        }
                        if ($biller->cf4 != "-" && $biller->cf4 != "") {
                            echo "<br>" . lang("scf4") . ": " . $biller->cf4;
                        }
                        if ($biller->cf5 != "-" && $biller->cf5 != "") {
                            echo "<br>" . lang("scf5") . ": " . $biller->cf5;
                        }
                        if ($biller->cf6 != "-" && $biller->cf6 != "") {
                            echo "<br>" . lang("scf6") . ": " . $biller->cf6;
                        }

                        echo "</p>";
                        echo lang("tel") . ": " . $biller->phone . "<br>" . lang("email") . ": " . $biller->email;
                        ?>
                    </address>
                </td>
            </tr>
            <tr>
                <td class="border-zero" ><?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?></td>
             <td class="border-zero text-right" >
<!--                    Time: 20:02 Hrs.-->
                </td>
            </tr>

            <tr>
                <td class="border-zero" ><?= lang("sale_reference"); ?> .: <?= $inv->reference_no; ?></td>
                <td class="border-zero text-right" >
                    Cashier: <?= $user->first_name . ' ' . $user->last_name; ?>
                </td>
            </tr>

        </table>
        <table class="table table-bordered table-hover table-striped order-table">

            <thead>

                <tr>
                    <th><?= lang("no"); ?></th>
                    <th><?= lang("description"); ?> (<?= lang("code"); ?>)</th>
                    <th><?= lang("quantity"); ?></th>
                    <?php
//                    if ($Settings->product_serial) {
                    echo '<th style="text-align:center; vertical-align:middle;">' . lang("mrp") . '</th>';
//                    }
                    ?>
                    <th style="padding-right:20px;"><?= lang("rate"); ?></th>
                    <!--<?php
                    if ($Settings->tax1) {
                        echo '<th style="padding-right:20px; text-align:center; vertical-align:middle;">' . lang("tax") . '</th>';
                    }
                    ?>-->
                    <?php
                    if ($Settings->product_discount) {
                        echo '<th style="padding-right:20px; text-align:center; vertical-align:middle;">' . lang("discount") . '</th>';
                    }
                    ?>
                    <th style="padding-right:20px;"><?= lang("subtotal"); ?></th>
                </tr>

            </thead>

            <tbody>

                <?php
                $r = 1;
                $total_qty = 0;
                $item_discount = 0;
                $$total_unit_price = 0;
                foreach ($rows as $row):
//                    echo"<pre>";
//                    print_r($row);
//                    print_r($Settings);
//                    echo"</pre>";
//                    die();
                    $total_qty = $total_qty + $row->quantity;
                    $item_discount = $item_discount + $row->item_discount;
                    $total_unit_price = $total_unit_price + $row->real_unit_price;
                    $total_net_unit = $total_net_unit + $row->net_unit_price;
                    ?>
                    <tr>
                        <td class="border_left" style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                        <td class="border_left" style="vertical-align:middle;"><?= $row->product_name . " (" . $row->product_code . ")" . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                            <?= $row->details ? '<br>' . $row->details : ''; ?></td>
                        <td class="border_left" style="width: 120px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity); ?></td>
                        <?php
//                        if ($Settings->product_serial) {
                        echo '<td class="border_left">' . $this->sma->formatMoney($row->real_unit_price) . '</td>';
//                        }
                        ?>
                        <td class="border_left" style="text-align:right; width:150px; padding-right:10px;"><?= $this->sma->formatMoney($row->net_unit_price + ($row->item_tax / $row->quantity)); ?></td>
                        <!--<?php
                        if ($Settings->tax1) {
                            echo '<td class="border_left" style="width: 150px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 ? '<small>(' . $row->tax . ')</small>' : '') . ' ' . $this->sma->formatMoney($row->item_tax) . '</td>';
                        }
                        ?>-->
                        <?php
                        if ($Settings->product_discount) {
                            echo '<td class="border_left" style="width: 150px; text-align:right; vertical-align:middle;">' . ($row->item_discount != 0 ? '<small>(' . $row->discount . ')</small>' : '') . ' ' . $this->sma->formatMoney($row->item_discount) . '</td>';
                        }
                        ?>
                        <td class="border_right" style="text-align:right; width:120px; padding-right:10px;"><?= $this->sma->formatMoney($row->subtotal); ?></td>
                    </tr>
                    <?php
                    $r++;
                endforeach;
                ?>
            </tbody>
            <tfoot>
                <tr style="font-weight:bold;">
                    <td class="border_left" ><?php echo count($rows) ?></td>
                    <td class="border_left">Total Qty</td>
                    <td class="border_left"><?php echo $this->sma->formatMoney($total_qty) ?></td>
                    <td class="border_left" colspan="3">Sub Total</td>

                    <td class="border_right"><?= $this->sma->formatMoney($inv->total + $inv->product_tax); ?></td>
                </tr>
                <tr>
                    <td colspan="2">MRP Total</td>
                    <td><?= $this->sma->formatMoney($total_unit_price); ?></td>
                    <td colspan="3">Deduction</td>
                    <td><?= $Deduction = $this->sma->formatMoney($total_net_unit - $inv->total); ?></td>
                </tr>
                <tr class="small_size" style="font-weight:bold;font-size:16px;">
                    <td colspan="2">Total Discount</td>
                    <td><?= $this->sma->formatMoney($item_discount) ?></td>
                    <td colspan="3">Total </td>
                    <td><?= $this->sma->formatMoney($inv->total + $inv->product_tax); ?></td>
                </tr>
                <tr>
                    <td colspan="2"> Discounted Total</td>
                    <td><?= $this->sma->formatMoney($inv->total + $inv->product_tax); ?></td>
                    <td colspan="3">Cash Returned </td>
                    <td colspan="3"><?= $this->sma->formatMoney($inv->total + $inv->product_tax); ?></td>
                </tr>
                <tr style="font-weight:bold;font-size:18px;" class="small_size">
                    <td colspan="7">
            <center>GOODS/DAMAGED RETURNED</center>
            </td>
            </tr>
            <tr>
                <td colspan="7">
                    <h5 class="text-center">माल बदलून  घेते वेळी..... </h5>
                    <p>१. कोणताही खरेदी केलेला माल बदलून घेते वेळी त्याचे खरेदी केलेले बिल असणे आवश्यक आहे , अन्यथा माल बदलून मिळणार नाही . <br/>
                        २. कोणताही खरेदी केलेला माल बदलून घेते वेळी त्याचे खरेदी केलेले बिल असणे आवश्यक आहे , अन्यथा माल बदलून मिळणार नाही . <br/>
                        ३. कोणताही खरेदी केलेला माल बदलून घेते वेळी त्याचे खरेदी केलेले बिल असणे आवश्यक आहे , अन्यथा माल बदलून मिळणार नाही . <br/>
                        ४. कोणताही खरेदी केलेला माल बदलून घेते वेळी त्याचे खरेदी केलेले बिल असणे आवश्यक आहे , अन्यथा माल बदलून मिळणार नाही . <br/>
                        ५. कोणताही खरेदी केलेला माल बदलून घेते वेळी त्याचे खरेदी केलेले बिल असणे आवश्यक आहे , अन्यथा माल बदलून मिळणार नाही . <br/>
                    </p>

                </td>
            </tr>

            <td colspan="7" style="border-top: 0;border-bottom: 0; text-align:center;">
                <img src="<?= base_url() ?>assets/uploads/barcode<?= $this->session->userdata('user_id') ?>.png">
            </td>

            </tfoot>
        </table>
        </div>
<!--        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Sr. No</th>
                    <th>Item Description</th>
                    <th>Qty</th>
                    <th>MRP</th>
                    <th>Rate</th>
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>Mens Formal Shirt - H 38''[G001M000001]<br/> Discount - 10%</td>
                    <td>1</td>
                    <td>599</td>
                    <td>539</td>
                    <td>539</td>

                </tr>
                <tr>
                    <td>2</td>
                    <td>Mens Structure Pant - W 34''[G001M000001]<br/> Discount - 10%</td>
                    <td>1</td>
                    <td>1299</td>
                    <td>1170</td>
                    <td>1170</td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Kids Jacket  Shirt Set - H 8''[G001K000001]<br/> Discount - 10%</td>
                    <td>1</td>
                    <td>1120</td>
                    <td>1008</td>
                    <td>1008</td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>La Saree italian Printed - [G001S000001]<br/> Discount - 10%</td>
                    <td>2</td>
                    <td>900</td>
                    <td>810</td>
                    <td>1620</td>
                </tr>
                <tr style="font-weight:bold;">
                    <td>4</td>
                    <td>Total Qty</td>
                    <td>5</td>
                    <td colspan="2">Sub Total</td>

                    <td>4337</td>
                </tr>

                <tr>
                    <td colspan="2">MRP Total</td>
                    <td>4818</td>
                    <td colspan="2">Deduction</td>
                    <td>217</td>




                </tr>
                <tr class="small_size" style="font-weight:bold;font-size:16px;">
                    <td colspan="2">Total Discount</td>
                    <td>481</td>

                    <td colspan="2">Total </td>

                    <td>4337</td>
                </tr>
                <tr>
                    <td colspan="2"> Discounted Total</td>
                    <td>4337</td>


                    <td colspan="2">Cash Returned </td>

                    <td colspan="3">4120</td>
                </tr>
                <tr style="font-weight:bold;font-size:18px;" class="small_size">
                    <td colspan="6">
            <center>GOODS/DAMAGED RETURNED</center>
            </td>

            </tr>
            <tr>
                <td colspan="6">
                    <h5 class="text-center">माल बदलून  घेते वेळी..... </h5>
                    <p>१. कोणताही खरेदी केलेला माल बदलून घेते वेळी त्याचे खरेदी केलेले बिल असणे आवश्यक आहे , अन्यथा माल बदलून मिळणार नाही . <br/>
                        २. कोणताही खरेदी केलेला माल बदलून घेते वेळी त्याचे खरेदी केलेले बिल असणे आवश्यक आहे , अन्यथा माल बदलून मिळणार नाही . <br/>
                        ३. कोणताही खरेदी केलेला माल बदलून घेते वेळी त्याचे खरेदी केलेले बिल असणे आवश्यक आहे , अन्यथा माल बदलून मिळणार नाही . <br/>
                        ४. कोणताही खरेदी केलेला माल बदलून घेते वेळी त्याचे खरेदी केलेले बिल असणे आवश्यक आहे , अन्यथा माल बदलून मिळणार नाही . <br/>
                        ५. कोणताही खरेदी केलेला माल बदलून घेते वेळी त्याचे खरेदी केलेले बिल असणे आवश्यक आहे , अन्यथा माल बदलून मिळणार नाही . <br/>
                    </p>

                </td>

            </tr>

            <td colspan="6" style="border-top: 0;border-bottom: 0; text-align:center;">
                <img src="images/barcode.png">
            </td>
            </tbody>
        </table>-->
    </div>
</div>
