

<?php

echo "<pre>";print_r($employee);exit;

?>
<style>
    /*    fieldset.scheduler-border {
            border: 1px solid #ddd !important;
            padding: 0 1em 1em 1em !important;
            margin: 0 0 1.1em 0 !important;
            -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
        }
        legend.scheduler-border {
            width:inherit;  Or auto 
            padding:0 10px;  To give a bit of padding on the left and right 
            border-bottom:none;
        }
        .cal 
        {
            position:relative;
            top:-30px;
            left:85%;
        }
        .nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
            color: #6164c0 !important;
            background-color: #fff !important;
            border-radius:0 !important;
        }
    
        .nav-pills>li>a{
            color: #6164c0 !important;
            background-color: #e9ebec !important;
            border-radius:0 !important;
        }
        .tab-content>.active {
            display: block;
            border : 1px solid #ccc;
            padding:5px !important;
        }
        .btton
        {
            padding: 5px;
            width:100%;
        }
    
    
        .stepwizard-step {
            display: table-cell;
            text-align: center;
            position: relative;
        }*/

    fieldset.scheduler-border {
        border: 1px solid #ddd !important;
        padding: 0 1em 1em 1em !important;
        margin: 0 0 1.1em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
        box-shadow:  0px 0px 0px 0px #000;
    }


    legend.scheduler-border {
        width:inherit; /* Or auto */
        padding:0 10px; /* To give a bit of padding on the left and right */
        border-bottom:none;

    }
    .cal 
    {
        position:relative;
        top:-30px;
        left:90%;
    }
    /*
    a {
        color: #000 !important;    
            }*/
    .nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
        color: #6164c0 !important;
        background-color: #fff !important;
        border-radius:0 !important;
    }

    .nav-pills>li>a{
        color: #6164c0 !important;
        background-color: #e9ebec !important;
        border-radius:0 !important;
    }
    .tab-content>.active {
        display: block;
        border : 1px solid #ccc;
        padding:5px !important;
    }
    .btton
    {
        padding: 5px;
        width:100%;
    }

    .stepwizard-step {
        display: table-cell;
        /*text-align: center;*/
        position: relative;
    }
    .form-horizontal .control-label {
        padding-top: 0px;
    }
</style>
<script type="text/javascript" src="<?= $assets ?>js/webcam.js"></script>

<div id="exTab1" class="container-fluid" style="background-color: #ffff;padding: 10px" ng-controller="employee">		

    <div class="stepwizard">
        <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step">
                <a href="#step-1" type="button" class="btn btn-primary ">General Details</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-2" type="button" class="btn btn-default " disabled="disabled">User Fields</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-3" type="button" class="btn btn-default " disabled="disabled">Qualification</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-4" type="button" class="btn btn-default " disabled="disabled">Attachments</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-5" type="button" class="btn btn-default " disabled="disabled">Reset Credentials</a>
            </div>
        </div>
    </div>
    <?php
    $attrib = array('data-toggle' => 'validator', 'class' => 'form-horizontal', 'role' => 'form');
    echo form_open_multipart("employee/edit/" . $employee->id, $attrib);
    ?>
    <div class="tab-pane" style="border: 1px solid #000; padding: 10px">

        <div class="row setup-content" id="step-1">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Employee Details</legend>
                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('fname', 'fname') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('fname', set_value('fname', $employee->fname), 'class="form-control tip" id="fname" required="required" data-bv-notempty="true" data-bv-regexp="true"
                               data-bv-regexp-regexp="^[a-z\s,A-z]+$"
                               data-bv-regexp-message="Name must contain characters only"'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('mname', 'mname') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('mname', set_value('mname', $employee->mname), 'class="form-control tip" id="mname"  data-bv-notempty="true" required="required" data-bv-regexp="true"
                               data-bv-regexp-regexp="^[a-z\s,A-z]+$"
                               data-bv-regexp-message="Name must contain characters only"'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('lname', 'lname') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('lname', set_value('lname', $employee->lname), 'class="form-control tip" id="lname" required="required" data-bv-notempty="true" data-bv-regexp="true"
                               data-bv-regexp-regexp="^[a-z\s,A-z]+$"
                               data-bv-regexp-message="Name must contain characters only"'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('dob', 'dob') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('dob', set_value('dob', $this->sma->hrsd($employee->dob)), 'class="form-control tip date" id="dob" required="required"  data-bv-notempty="true"'); ?><i class="fa fa-calendar cal"></i>
                                </div>
                                <!--                                <div class="col-sm-3">
                                                                    <button type="button" id="dobclear" class="btn btn-default">Clear</button>
                                                                </div>-->
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4"><?= lang('maritalstatus', 'maritalstatus') ?></label>
                                <div class="col-sm-8">
                                    <?php
                                    $mstatus = array(
                                        '' => "Select Marital Status",
                                        'Married' => "Married",
                                        'Unmarried' => 'Unmarried'
                                    );
                                    echo form_dropdown('maritalstatus', $mstatus, $employee->marital_status, 'class="form-control select" id="maritalstatus" required="required" data-bv-notempty="true" placeholder="' . lang("select") . " " . lang("store_name") . '"')
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4"><?= lang('gender', 'gender') ?></label>
                                <div class="col-sm-8">
                                    <?php
                                    $gender = array(
                                        '' => "Select Gender",
                                        'Male' => "Male",
                                        'Female' => 'Female'
                                    );
                                    echo form_dropdown('gender', $gender, $employee->gender, 'class="form-control select" id="gender" required="required" data-bv-notempty="true" placeholder="' . lang("select") . " " . lang("store_name") . '"')
                                    ?>
                                </div>
                            </div>

                        </fieldset>

                        <fieldset class="scheduler-border" style="padding: 0px 25px !important;">
                            <legend class="scheduler-border">Address</legend>

                            <div class="form-group">
                                <label><?= lang('address1', 'address11') ?></label>
                                <?php echo form_input('address1', set_value('address1', $employee->address), 'class="form-control tip" id="address1"  data-bv-notempty="false"'); ?>
                            </div>
                            <div class="form-group">
                                <label><?= lang('address2', 'address2') ?></label>
                                <?php echo form_input('address2', set_value('address21', $employee->address2), 'class="form-control tip" id="address" data-bv-notempty="false"'); ?>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label><?= lang('city', 'city1') ?></label>
                                        <?php echo form_input('city', set_value('city', $employee->city), 'class="form-control tip" id="city"  data-bv-notempty="false"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label><?= lang('state', 'state1') ?></label>
                                        <?php echo form_input('state', set_value('state', $employee->state), 'class="form-control tip" id="state"  data-bv-notempty="false"'); ?>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
<!--                                    <div class="form-group" style="padding-left: 20px !important">
                                        <label><?= lang('country', 'country1') ?></label>
                                        <?php echo form_input('country', set_value('country', $employee->country), 'class="form-control tip" id="country" data-bv-notempty="false"'); ?>
                                    </div>-->
                                    <div class="form-group" style="padding-left: 20px !important">
                                        <label><?= lang('zipcode', 'zipcode1') ?></label>
                                        <?php echo form_input('zipcode', set_value('zipcode', $employee->zipcode), 'class="form-control tip" id="zipcode" data-bv-notempty="false" 
                                        data-bv-notempty-message="Zip code is required and cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="6"
                               data-bv-stringlength-max="6"
                               data-bv-stringlength-message="The Zip code must be exact 6 numbers long"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The Zip code can only consist of digits"'); ?>
                                    </div>
                                </div>
                            </div>

                        </fieldset> 


                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">

                        <fieldset class="scheduler-border" style="padding: 0px 25px !important;">
                            <legend class="scheduler-border">Contact Info</legend>

                            <div class="form-group"> 
                                <label><?= lang('telephoneno', 'telephonenodd') ?></label>
                                <?php echo form_input('telephoneno', set_value('telephoneno', $employee->tele_ph), 'class="form-control tip" id="telephonenos"  data-bv-notempty="flase" pattern="[0-9]{10}" maxlength="10"'); ?>
                            </div>

                            <div class="form-group">
                                <label><?= lang('mobileno', 'mobileno') ?></label>
                                <?php
                                echo form_input('mobileno', set_value('mobileno', $employee->mobile), 'class="form-control tip" id="mobileno" required="required" data-bv-notempty="true"'
                                        . ' data-bv-notempty-message="The Mobile no is required and cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="10"
                               data-bv-stringlength-max="10"
                               data-bv-stringlength-message="The Mobile no must be exact 10 numbers long"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The Mobile no can only consist of digits"');
                                ?>
                            </div>
                            <div class="form-group">
                                <label><?= lang('alternet_mob', 'alternet_mob') ?></label>
                                <?php echo form_input('faxno', set_value('faxno', $employee->faxno), 'class="form-control tip" id="faxnos"  data-bv-notempty="false" pattern="[0-9]{10}" maxlength="10"'); ?>
                            </div>
                            <div class="form-group">
                                <label><?= lang('email', 'email') ?></label>
                                <input type="email" name="email" class="form-control" value="<?= set_value('email', $employee->email) ?>" required="required" data-bv-notempty="true" id="email"/>
                                <?php // echo form_input('email', set_value('email', ''), 'class="form-control tip" id="email" required="required" data-bv-notempty="true"');    ?>
                            </div>
                            <!--                        <div class="form-group">
                                                        <label><?= lang('website', 'website') ?></label>
                                                        <input type="url" name="website" value="<?= set_value('website', $employee->webiste) ?>" class="form-control" id="email_address"/>
                            <?php // echo form_input('website', set_value('website', ''), 'class="form-control tip" id="websites" ="required" data-bv-notempty="true"');    ?>
                                                    </div>-->

                        </fieldset>
                        <!--                        <fieldset class="scheduler-border">
                                                    <legend class="scheduler-border">Sales Commission Details</legend>
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-7"><?= lang('scp', 'scp') ?></label>
                                                        <div class="col-sm-4">
                        <?php
                        echo form_input('scp', set_value('scp', $employee->scp), 'class="form-control tip" id="scp" data-bv-notempty="false"'
                                . 'data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="Only Numbers Allowed"
                                data-bv-stringlength="true"
                               data-bv-stringlength-min="2"
                               data-bv-stringlength-max="2"
                               data-bv-stringlength-message="The Percentage must be exact 2 numbers long"');
                        ?>
                                                        </div>
                                                        <label class="control-label col-sm-1">%</label>
                                                    </div>
                        
                        
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-7"><?= lang('cqp', 'cqp') ?></label>
                                                        <div class="col-sm-4">
                        <?php echo form_input('cqp', set_value('cqp', $employee->cqp), 'class="form-control tip" id="cqp" data-bv-notempty="false"'); ?>
                                                        </div>
                                                        <label class="control-label col-sm-1"></label>
                                                    </div>
                                                </fieldset>-->
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Spot Discount at Invoice</legend>
                            <div class="form-group">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="hlsda" <?= $employee->hlsda == "True" ? "checked" : "" ?> value="1" id="hlsda"><?= lang('hlsda', 'hlsda') ?></label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-7"><?= lang('msd', 'msd1') ?></label>
                                <div class="col-sm-4">
                                    <?php
                                    echo form_input('msd', set_value('msd', $employee->msd), 'class="form-control tip" id="msd"  data-bv-notempty="false"'
                                            . 'data-bv-regexp="false"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="Only Numbers Allowed" data-bv-stringlength="true"
                               data-bv-stringlength-min="2"
                               data-bv-stringlength-max="2"
                               data-bv-stringlength-message="The Percentage must be exact 2 numbers long"');
                                    ?>
                                </div>
                                <label class="control-label col-sm-1">%</label>
                            </div>
                        </fieldset>

                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border"><?= lang('refered_by') ?></legend>
                            <div class="controls"> 
                                <?php
                                $emp[""] = "";
                                foreach ($employees as $biller) {
                                    $emp[$biller->id] = $biller->fname . ' ' . $biller->mname . ' ' . $biller->lname;
                                }
                                echo form_dropdown('refered_by', $emp, (isset($_POST['biller']) ? $_POST['biller'] : $employee->refered_by), ' data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("refered_by") . '"  class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </fieldset>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="row">
                            <div class="form-group" style="padding:0 20px;">
                                <?= lang("photo", "biller_logo"); ?> 
                                <input id="store_logo_up" name="photo" type="file"  class="form-control" 
                                       data-show-upload="false"
                                       data-show-preview="false" 
                                       data-bv-file-maxsize="300*100" 
                                       />
                            </div>
                        </div>  
                        <br>
                        <div class="form-group" style="padding:0 16px;">
                            <input type="hidden" name="supplierPhoto" id="supplierphoto" value="<?= set_value('supplierPhoto', $employee->photo) ?>">
                            <div id="my_camera" style="border:1px solid #000000;background:#eee; width: 100%; height: 500px; display: none;"  width="100%" height="500px"></div>
                            <img  id="image_upload_preview" src="assets/uploads/<?= $employee->photo ?>" style="border:1px solid #000000;background:#eee" width="100%" height="500px">
                            <div id="results"></div>
                        </div>
                        <br><center><button type="button" id="takepicture" style="">Take Picture with Attached Camera</button>
                            <input type=button value="Take Snapshot" id="tacksnaps" onClick="take_snapshot()" style="display: none"></center>
                        <br>



                    </div>

                </div>
                <div class="col-md-12">
                    <!--<button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>-->
                    <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                </div>
            </div>

        </div>

        <div class="row setup-content" id="step-2">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-6 col-md-6"> 
                        <div class="form-group">
                            <label class="control-label col-sm-5"><?= lang('dojoin', 'dojoin') ?></label>
                            <div class="col-sm-7">
                                <?php echo form_input('dojoin', set_value('dojoin', $this->sma->hrsd($employee->joining_date)), 'class="form-control tip date" id="dojoin" data-bv-notempty="true" required="required"'); ?><i class="fa fa-calendar cal"></i>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5"><?= lang('location', 'location') ?></label>
                            <div class="col-sm-7">
                                <?php
                                $us = "";
                                $us[''] = "Select " . lang('location');
                                foreach ($store as $usr) {
                                    $us[$usr->id] = $usr->name;
                                }
                                echo form_dropdown('location', $us, $employee->store, 'class="form-control select" id="location" required="required" data-bv-notempty="true" placeholder="' . lang("select") . " " . lang("username") . '"')
                                ?>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5"><?= lang('department', 'department') ?></label>
                            <div class="col-sm-7">
                                <?php
                                $us = "";
                                foreach ($department as $usr) {
                                    $us[$usr->id] = $usr->name;
                                }
                                echo form_dropdown('department', $us, $employee->department, 'class="form-control select" id="department" required="required" data-bv-notempty="true" placeholder="' . lang("select") . " " . lang("username") . '"')
                                ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-5"><?= lang('section', 'section') ?></label>
                            <div class="col-sm-7">
                                <?php
                                $us = "";
                                foreach ($section as $sec) {
                                    $us[$sec->id] = $sec->name;
                                }
                                echo form_dropdown('section', $us, $employee->section, 'class="form-control select" required="required" data-bv-notempty="true" id="section" placeholder="' . lang("select") . " " . lang("section") . '"')
                                ?>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-sm-5"><?= lang('position', 'position') ?></label>
                            <div class="col-sm-7">
                                <?php
                                $us = "";
                                $us[''] = "Select " . lang('position');
                                foreach ($position as $usr) {
                                    $us[$usr->id] = $usr->name;
                                }
                                echo form_dropdown('desigation', $us, $employee->desigation, 'class="form-control select" id="desigation" required="required" data-bv-notempty="true" placeholder="' . lang("select") . " " . lang("username") . '"')
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5"><?= lang('shift', 'shift') ?></label>
                            <div class="col-sm-7">
                                <?php
                                $us = array(
                                    
                                    '' => 'Select Shift',
                                    'Morning' => 'Morning',
                                    'Evening' => 'Evening'
                                );
                                echo form_dropdown('shift', $us, $employee->shift, 'class="form-control select" id="shift"  required="required" data-bv-notempty="true" placeholder="' . lang("select") . " " . lang("username") . '"')
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6"> 
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Bank Details</legend>
                            <!--                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">-->
                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('bank_name', 'bank_name1') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('bank_name', set_value('bank_name', $employee->bank_name), 'class="form-control tip" id="bank_name" data-bv-notempty="false"'); ?>
            <!--                              <input type="text" name="bank_name" class="form-control">-->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('acc_no', 'acc_no1') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('acc_no', set_value('acc_no', $employee->acc_no), 'class="form-control tip" id="acc_no" data-bv-notempty="false"
                               data-bv-notempty-message="Required"
                               data-bv-stringlength="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="Only Numbers Allowed"'); ?>
                                  <!--<input type="text" name="acc_no" class="form-control">-->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('ifsc', 'ifsc1') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('neft', set_value('neft', $employee->neft), 'class="form-control tip" id="ifsc" data-bv-notempty="false"'); ?>
                                  <!--<input type="text" name="neft" class="form-control">-->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('bank_branch', 'bank_branch1') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('bank_branch', set_value('bank_branch', $employee->bank_branch), 'class="form-control tip" id="bank_branch" data-bv-notempty="false"'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('opening_balance', 'opening_balance1') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('opening_balance', set_value('opening_balance', $employee->opening_balance), 'class="form-control tip" id="opening_balance" data-bv-notempty="false"'); ?>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <div class="col-md-12">
                    <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                    <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                </div>
            </div>
        </div>

        <div class="row setup-content" id="step-3">
            <div class="col-md-12">
                <div class="row">

                    <div class="qualification">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                    <label>Qualification</label>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                    <label>Year of Pasing</label>
                                </div>
                            </div>
                        </div>
                        <?php
                        $special_training = json_decode($employee->qualification, TRUE);
                        $i = 0;
                        foreach ($special_training['quali'] as $row) {
                            $quali[] = array(
                                'quali' => $row,
                                'yr' => $special_training['yr'][$i],
                            );
                            $i++;
                        }
                        if (!empty($special_training)) {
                            foreach ($quali as $row) {
                                ?>
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">

                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

                                            <input type="text" name="quali[]" value="<?= $row['quali'] ?>" class="form-control" id="quali" placeholder="Qualification">
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

                                            <input type="text" name="yr[]" value="<?= $row['yr'] ?>" class="form-control date"  id="yr" placeholder="Year of Pasing">
                                        </div>
                                        <label class="col-lg-2 col-sm-2 col-md-2 col-xs-12 " onclick="addQualifications()">
                                            <i class="fa fa-plus-circle fa-2x"></i></label>
                                    </div>
                                </div>
                                <?php
                            }
                        } else {
                            ?>
                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">

                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

                                        <input type="text" name="quali[]" value="<?= $row['quali'] ?>" class="form-control"  id="quali" placeholder="Qualification">
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

                                        <input type="text" name="yr[]" value="<?= $row['yr'] ?>" class="form-control date" readonly  id="yr" placeholder="">
                                    </div>
                                    <label class="col-lg-2 col-sm-2 col-md-2 col-xs-12 " onclick="addQualifications()">
                                        <i class="fa fa-plus-circle fa-2x"></i></label>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                        <div id="training">
                            <?php
                            $special_training = json_decode($employee->special_training, TRUE);
                            if (!empty($special_training)) {
                                foreach ($special_training as $row) {
                                    ?>
                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">
                                        <label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="special_training">Special Training:</label>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                            <input type="text" name="special_training[]" value="<?= $row ?>" class="form-control"  id="special_training" placeholder="Tally - Accounting Software" style="margin-bottom:10px;">
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                            <i class="fa fa-2x fa-plus-circle" onclick="addTraining()"></i>
                                        </div>
                                    </div>
                                    <?php
                                }
                            } else {
                                ?>
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">
                                    <label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="special_training">Special Training:</label>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                        <input type="text" name="special_training[]" value="<?= $row ?>" class="form-control"  id="special_training" placeholder="Tally - Accounting Software" style="margin-bottom:10px;">
                                    </div>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                        <i class="fa fa-2x fa-plus-circle" onclick="addTraining()"></i>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                    <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                </div>
            </div>
        </div>

        <div class="row setup-content" id="step-4">
            <div class="col-md-12">
                <div class="row">
                    <?php
                    $document = json_decode($employee->identity_proof, TRUE);
//                    if(!$document){
//                        $document = array('docname'=> '');
//                    }
                    ?>
                    <div id="document" ng-init='attachment = <?= json_encode($document) ?>'>
                        <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
                            <label class="control-label" for="document" style="font-weight:bold;">Identify Proof -</label>
                        </div>
                        <div class="col-lg-10 col-sm-10 col-md-10 col-xs-12 ">
                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group "  ng-repeat="attach in attachment">

                                <div class="col-lg-5 col-sm-5 col-md-5 col-xs-5">
                                    <input type="text" name="docname[]" class="form-control" ng-model="attach.docname" id="quali" placeholder="Document Name">
                                    <input type="hidden" name="predocname[]" value="{{attach.file}}">
                                </div>
                                <div class="col-lg-5 col-sm-5 col-md-5 col-xs-5">

                                    <input id="documentfile1" name="document[]" type="file"  class="" 
                                           data-show-upload="false"
                                           data-show-preview="false" 
                                           data-bv-file-maxsize="300*100" 
                                           />
                                    <label>{{attach.file}}</label>
                                </div>
                                <div class="col-lg-1 col-sm-1 col-md-1 col-xs-2">
                                    <button type="button" ng-if="$index < attachment.length - 1" class="btn btn-default" ng-click="attachment.splice($index, 1);" href="">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                    <a href="" ng-if="$index == attachment.length - 1" ng-click='attachment.push({name: ""});' class="btn btn-default">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!--                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">
                                                        <label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="document" style="font-weight:bold;">Identify Proof -</label>
                        
                                                        <div class="col-lg-7 col-sm-7 col-md-7 col-xs-12">
                                                            <input id="documentfile" name="document[]" multiple type="file"  class="form-control" 
                                                                   data-show-upload="false"
                                                                   data-show-preview="false" 
                                                                   data-bv-file-maxsize="300*100" 
                                                                   />
                                                        </div>
                        
                                                    </div>
                                                </div>-->

                    </div>
                    <?php $ref = json_decode($employee->references, true); ?>
                    <div id="references" ng-init='references = <?= json_encode($ref) ?>'>
                        <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
                            <label class="control-label" for="document" style="font-weight:bold;">References -</label>
                        </div>
                        <div class="col-lg-10 col-sm-10 col-md-10 col-xs-12 ">
                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group "  ng-repeat="ref in references">
                                <div class="col-lg-5 col-sm-5 col-md-5 col-xs-5">
                                    <input type="text" name="refname[]" class="form-control" ng-model="ref.name"  id="quali" placeholder="Reference Name">
                                    <input type="hidden" name="prereffile[]" value="{{ref.file}}">
                                </div>
                                <div class="col-lg-5 col-sm-5 col-md-5 col-xs-5">
                                    <input id="referencesfile1" name="references[]" type="file" class="" data-show-upload="false" data-show-preview="false" data-bv-file-maxsize="300*100" />
                                    <label>{{ref.file}}</label>
                                </div>
                                <div class="col-lg-1 col-sm-1 col-md-1 col-xs-2">
                                    <button type="button" ng-if="$index < references.length - 1" class="btn btn-default" ng-click="references.splice($index, 1);" href="">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                    <a href="" ng-if="$index == references.length - 1" ng-click='references.push({referencesname: ""});' class="btn btn-default">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>

                            </div>
                        </div>
                        <!--                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">
                                                        <label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="references" style="font-weight:bold;">References -</label>
                                                                                        <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                                                                            <input type="name" class="form-control" id="email" value="<?= $row ?>" placeholder="0000000000" style="margin-bottom:10px;">
                                                                                        </div>
                                                                                        <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="margin-bottom:10px;">
                                                                                            <label class="btn btn-default btn-file" style="margin:0;">
                                                                                                Browse <input type="file" name="references[]" style="display: none;">
                                                                                            </label>
                                                                                        </div>
                                                                                        <div class="col-lg-2 col-sm-2 col-md-2 col-xs-6">
                                                                                            <i class="fa fa-2x fa-plus-circle" onclick="addReferences()"></i>
                                                                                        </div>
                                                        <div class="col-lg-7 col-sm-7 col-md-7 col-xs-12" style="margin-bottom:10px;">
                                                            <input id="referencesfile" name="references[]" type="file" multiple class="form-control referencesfile" data-show-upload="false" data-show-preview="false" data-bv-file-maxsize="300*100" />
                                                        </div>
                                                    </div>
                                                </div>-->

                    </div>
                </div>
                <div class="col-md-12">
                    <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                    <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                </div>
            </div>
        </div>

        <div class="row setup-content" id="step-5">
            <div class="col-md-12">
                <fieldset class="scheduler-border" style="padding: 0px 25px !important;">
                    <div class="row">
                        <legend class="scheduler-border">Login Info</legend>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?php echo lang('password', 'password'); ?>
                                <?php echo form_password('password', '', 'class="form-control tip" id="password" autocomplete="off"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"'); ?>
                                <span class="help-block">At least 1 capital, 1 lowercase, 1 number and more than 8 characters long</span>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-4">

                            <div class="form-group">
                                <?php echo lang('confirm_password', 'password_confirm'); ?>
                                <?php echo form_password('password_confirm', '', 'class="form-control" id="password_confirm" autocomplete="off"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-bv-identical="true" data-bv-identical-field="password" data-bv-identical-message="' . lang('pw_not_same') . '"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row text-center">
                        <!--<span class="text-center" style="color: red">User name will be same as your email id</span>-->
                    </div>
                </fieldset>
                <div class="col-md-12">
                    <button class="btn btn-primary prevBtn  pull-left" type="button">Previous</button>
                    <?php echo form_submit('edit_employee', lang('save'), 'class="btn btn-primary pull-right"'); ?>
                </div>
            </div>
        </div>

    </div>

    <?php echo form_close(); ?>
</div>

<script>
    $(".removeattachment").click(function (event) {
        event.preventDefault();
        $(this).parents('.attachments').remove();
    });
</script>

<script>
    $(document).ready(function () {
        var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn'),
                allPrevBtn = $('.prevBtn');

        allWells.hide();

        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                    $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-primary').addClass('btn-default');
                $item.addClass('btn-primary');
                allWells.hide();
                $target.show();
//                $target.find('input:eq(0)').focus();
            }
        });

        allPrevBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

            prevStepWizard.removeAttr('disabled').trigger('click');
        });

        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url'],input[type='email'],fieldset > input[type='text']"),
                    isValid = true;

            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid)
                nextStepWizard.removeAttr('disabled').trigger('click');
        });

        $('div.setup-panel div a.btn-primary').trigger('click');
    });
</script>
<script>
    $(document).on('ifUnchecked', '#hlsda', function (e) {
        $('#msd1').attr('disabled', "disabled");
        $('#msd1').attr('required', 'required');
    });
    $(document).on('ifChecked', '#hlsda', function (e) {
        $('#msd1').removeAttr("disabled");
        $('#msd1').removeAttr('required');
    });


    $(document).on('ifUnchecked', '#isloyatlity', function (e) {
        $('#loyalityno').attr('disabled', "disabled");
        $('#enrolldate').attr('disabled', "disabled");
        $('#loyalitypoint').attr('disabled', "disabled");
        $('#referby').attr('disabled', "disabled");
        $('#loyalityno').attr('required', 'required');
        $('#enrolldate').attr('required', 'required');
        $('#loyalitypoint').attr('required', 'required');
        $('#referby').attr('required', 'required');

    });
    $(document).on('ifChecked', '#isloyatlity', function (e) {
        $('#loyalityno').removeAttr("disabled");
        $('#enrolldate').removeAttr("disabled");
        $('#loyalitypoint').removeAttr("disabled");
        $('#referby').removeAttr("disabled");
        $('#creaditlimit').removeAttr('required');
        $('#enrolldate').removeAttr('required');
        $('#loyalitypoint').removeAttr('required');
        $('#referby').removeAttr('required');
    });

    $('#store_logo_up').fileinput({
        maxImageWidth: 300,
        maxImageHeight: 100
    });
    $('#attachment').fileinput({
        maxImageWidth: 300,
        maxImageHeight: 100
    });
    $('#documentfile').fileinput({
        maxImageWidth: 300,
        maxImageHeight: 100
    });
    $('#referencesfile').fileinput({
        maxImageWidth: 300,
        maxImageHeight: 100
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            $("#supplierphoto").val('');
        }
    }

    $("#store_logo_up").change(function () {
        readURL(this);
    });
    $("#takepicture").click(function () {
        $('#my_camera').css('display', 'inline-block');
        $('#tacksnaps').css('display', 'inline-block');
        $('#image_upload_preview').css('display', 'none');
        $('#takepicture').css('display', 'none');
    });
    $("#dobclear").click(function () {
        $('#dob').val('');
    });
</script>

<script language="JavaScript">
    Webcam.set({
        width: 400,
        height: 330,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
    Webcam.attach('#my_camera');
</script>
<script language="JavaScript">
    function take_snapshot() {
        // take snapshot and get image data
        Webcam.snap(function (data_uri) {
            readURL("input[name='webcam']");
            Webcam.upload(data_uri, 'suppliers/savecamImage', function (code, text) {
                $("#image_upload_preview").attr("src", 'assets/uploads/' + text);
                $("#supplierphoto").val(text);
                $('#my_camera').css('display', 'none');
                $('#image_upload_preview').css('display', 'inline-block');
                $('#takepicture').css('display', 'inline-block');
                $('#tacksnaps').css('display', 'none');
            });
        });
    }
</script>

<script>
    function addQualifications() {
        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">' +
                '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">' +
                '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">' +
                '<input type="text" name="quali[]" class="form-control" id="email" placeholder="B.C.A">' +
                '</div>' +
                '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">' +
                '<input type="text" name="yr[]" class="form-control date" id="email" placeholder="Year of Pasing">' +
                '</div>' +
                '<label class="col-lg-2 col-sm-2 col-md-2 col-xs-12" for="email" onclick="addQualifications()"><i class="fa fa-plus-circle fa-2x"></i></label>' +
                '</div>' +
                '</div>';
        $('.qualification').append(data);
    }

    function addTraining() {
        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">' +
                '<label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="email">Special Training:</label>' +
                '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">' +
                '<input type="text" name="special_training[]" class="form-control" id="email" placeholder="Tally - Accounting Software" style="margin-bottom:10px;">' +
                '</div>' +
                '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">' +
                '<i class="fa fa-2x fa-plus-circle" onclick="addTraining()"></i>' +
                '</div>' +
                '</div>';
        $('#training').append(data);
    }
    function addExperience() {
        var data = '<tr>' +
                '<td><input type="text"  class="form-control" id="email"  name="year[]" placeholder="Year"></td>' +
                '<td><input type="text" class="form-control" id="email" name="company[]" placeholder="Company"></td>' +
                '<td><input type="text" class="form-control" id="email" name="position[]" placeholder="Position"></td>' +
                '<td><input type="text" class="form-control" id="email" name="reference[]" placeholder="Reference"></td>' +
                '<td><i class="fa fa-2x fa-plus-circle" onclick="addExperience()"></i></td>' +
                ' </tr>';
        $('#experiance').append(data);
    }
    function addDocument() {
        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">' +
                '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">' +
                '<label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="email" style="font-weight:bold;">Identify Proof -</label>' +
                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">' +
                '<input type="name" class="form-control" id="email" placeholder="0000000000">' +
                '</div>' +
                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">' +
                '<label class="btn btn-default btn-file" style="margin:0;">' +
                'Browse <input type="file" name="document[]" style="display: none;">' +
                '</label>' +
                ' </div>' +
                '<div class="col-lg-2 col-sm-2 col-md-2 col-xs-6">' +
                ' <i class="fa fa-2x fa-plus-circle" onclick="addDocument()"></i>' +
                ' </div>' +
                '</div>' +
                '</div>';
        $('#document').append(data);
    }

    function addReferences() {
        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">' +
                '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">' +
                '<label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="email" style="font-weight:bold;">References -</label>' +
                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">' +
                ' <input type="name" class="form-control" id="email" placeholder="0000000000" style="margin-bottom:10px;">' +
                ' </div>' +
                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="margin-bottom:10px;">' +
                '<label class="btn btn-default btn-file" style="margin:0;">' +
                '  Browse <input type="file" name="references[]" style="display: none;">' +
                '  </label>' +
                '</div>' +
                '<div class="col-lg-2 col-sm-2 col-md-2 col-xs-6">' +
                '<i class="fa fa-2x fa-plus-circle" onclick="addReferences()"></i>' +
                ' </div>' +
                '</div>' +
                '</div>';
        $('#references').append(data);
    }

    $('#location').change(function (event) {
        var store = $('#location').val();
        $.ajax({
            type: "get", async: false,
            url: "<?= site_url('billers/getDepartment') ?>",
            data: {'store': store},
            dataType: "json",
            success: function (data) {
                if (data == '') {
                    bootbox.alert('<?= lang('department_is_not_available') ?>');
                } else {
                    $('#department').html('');
                    $.each(data, function (k, val) {
                        var opt = $('<option />');
                        opt.val(val.id);
                        opt.text(val.name);
                        $('#department').append(opt);
                    })
                }
            }
        });
    });



    $('#department').change(function (event) {
        var section = $(this).val();
        $.ajax({
            type: "get", async: false,
            url: "<?= site_url('billers/getsection') ?>",
            data: {'section': section},
            dataType: "json",
            success: function (data) {
                $('#section').html('');
                if (data == '') {
//                    bootbox.alert('<?= lang('department_is_not_available') ?>');
                } else {
                    $.each(data, function (k, val) {
                        var opt = $('<option />');
                        opt.val(val.id);
                        opt.text(val.name);
                        $('#section').append(opt);
                    })
                }
            }
        });
    });
</script>

