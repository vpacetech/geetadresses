<style>
    .padding-left-0{padding-left: 0;}
    .padding-right-0{padding-right: 0;}
    .form-horizontal .control-label{text-align: left !important;font-size: 12px !important;font-weight: normal;}
    .table-bordered input{border: 0; box-shadow: none;}

    .table-bordered  > thead:first-child > tr:first-child > th,  .table-bordered  > thead:first-child > tr:first-child > td,  .table-bordered-striped thead tr.primary:nth-child(odd) th{    background-color: #6164c0;
                                                                                                                                                                                             text-align: center;}
    h4{font-weight: bold;    text-transform: uppercase;}
    btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }
    .photo{text-align: center;
           border: 1px solid #cccccc;
           padding: 30px 0;}
    </style>
    <div class="modal-dialog modal-lg">
    <div class="modal-content pull-left">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel">Employment Bio Data Form</h4>
        </div>
        <?php
        $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'class' => 'form-horizontal');
        echo form_open_multipart("employee/add", $attrib)
        ?>
        <div class="modal-body">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                        <h4>PERSONAL DETAILS</h4>
                    </div>
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                        <label class="control-label col-lg-3 col-sm-3 col-md-3 col-xs-12" for="joining_date"><?= lang("join_date") ?>:</label>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <input type="text" name="joining_date" class="form-control date" readonly required id="joining_date"  placeholder="Joining Date">
                        </div>
                    </div>
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                        <label class="control-label col-lg-3 col-sm-3 col-md-3 col-xs-12" for="fname"><?= lang("fname") ?>:</label>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <input type="text" name="fname" class="form-control" required id="fname"  placeholder="<?= lang("fname") ?>">
                        </div>
                    </div>
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                        <label class="control-label col-lg-3 col-sm-3 col-md-3 col-xs-12"  for="mname"><?= lang("mname") ?>:</label>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <input type="text" name="mname" class="form-control" id="mname" required requiredplaceholder="<?= lang("mname") ?>">
                        </div>
                    </div>
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                        <label class="control-label col-lg-3 col-sm-3 col-md-3 col-xs-12" for="lname"><?= lang("lname") ?>:</label>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <input type="text" name="lname" class="form-control" id="lname" required placeholder="<?= lang("lname") ?>">
                        </div>
                    </div>
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                        <label class="control-label col-lg-3 col-sm-3 col-md-3 col-xs-12" for="dob"><?= lang("dob") ?>:</label>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <input type="text" name="dob" class="form-control date" required id="dob" readonly placeholder="Date Of Birth">
                        </div>
                    </div>
                </div> 
                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 photo">
                    <h4>PHOTO</h4>
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" style="margin-bottom:10px;">
                        <label class="btn btn-default btn-file" style="margin:0;">
                            Browse <input type="file" name="photo" required style="display: none;">
                        </label>
                    </div>
                </div>
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="marital_status">Marital Status:</label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <select class="form-control" required id="sel1" name="marital_status">
                                <option>Married</option>
                                <option>Unmarried</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                        <label class="control-label col-lg-3 col-sm-3 col-md-3 col-xs-12" for="anniversary">Anniversary:</label>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <input type="text" name="anniversary" class="form-control date" readonly id="anniversary" placeholder="Date Of Anniversary">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                        <label class="control-label col-lg-6 col-sm-6 col-md-6 col-xs-12 " for="no_of_dependents">Number of Dependent:</label>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padding-left-0">
                            <input type="text" name="no_of_dependents" class="form-control" required id="no_of_dependents" placeholder="No of Dependents">
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                        <label class="control-label col-lg-3 col-sm-3 col-md-3 col-xs-12" for="domicile">Domicile:</label>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <input type="text" name="domicile" class="form-control" required id="domicile" placeholder="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                        <label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="address">Address (Home):</label>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <textarea class="form-control" name="address" required></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="tele_ph">Tele.Ph (Home):</label>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <input type="tel" name="tele_ph" class="form-control"  id="email" placeholder="Tele. Ph no">
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
                        <label class="control-label col-lg-3 col-sm-3 col-md-3 col-xs-12" for="mobile">Mobile:</label>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-3 padding-left-0 padding-right-0 ">
                                <input type="text" name="mobile"class="form-control" required id="mobile" placeholder="Mobile">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                        <label class="control-label col-lg-4 col-sm-4 col-md-4 col-xs-12" for="guardian_no">Fathers/Husband's/spouse's Mobile:</label>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding-left-0">
                            <input type="text" name="guardian_no" class="form-control" required id="guardian_no" placeholder="">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padding-left-0">
                            <input type="text" name="guardian_relation" class="form-control" required id="guardian_relation" placeholder="Relation">
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                        <h4>EDUCATIONAL QUALIFICATIONS</h4>
                    </div>
                </div>
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12" style="text-align: center">
                            <span>Qualification</span>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12" style="text-align: center">
                            <span>Year Of Passing</span>
                        </div>
                        <label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12 " for="quali"></label>
                    </div>
                </div>
                <div class="qualification">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                <input type="text" name="quali[]" class="form-control" required id="quali" placeholder="Qualification">
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                <input type="text" name="yr[]" class="form-control date" readonly required id="yr" placeholder="">
                            </div>
                            <label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12 " onclick="addQualifications()" for="email"><i class="fa fa-plus-circle fa-2x"></i></label>
                        </div>
                    </div>
                </div>
                <!--                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                
                                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                            <input type="name" class="form-control" id="email" placeholder="M.B.A">
                                        </div>
                                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                            <input type="name" class="form-control" id="email" placeholder="2010-11">
                                        </div>
                                        <label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12 " for="email"><i class="fa fa-plus-circle fa-2x"></i></label>
                                    </div>
                                </div>-->
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                    <div id="training">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">
                            <label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="special_training">Special Training:</label>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <input type="text" name="special_training[]" class="form-control"  id="special_training" placeholder="Tally - Accounting Software" style="margin-bottom:10px;"><i class="fa fa-2x fa-plus-circle" onclick="addTraining()"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                        <h4 style="text-">Working Experience</h4>
                        <table class="table-bordered">
                            <thead>
                                <tr>
                                    <th>Year</th>
                                    <th>Name of Company</th>
                                    <th>Position</th>
                                    <th>Reference</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="experiance">
                                <tr>
                                    <td><input type="text"  class="form-control" name="year[]" id="email" placeholder="Year"></td>
                                    <td><input type="text" class="form-control" name="company[]" id="email" placeholder="Company"></td>
                                    <td><input type="text" class="form-control" name="position[]" id="email" placeholder="Position"></td>
                                    <td><input type="text" class="form-control" name="reference[]" id="email" placeholder="Reference"></td>
                                    <td><i class="fa fa-2x fa-plus-circle" onclick="addExperience()"></i></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                        <h4 style="text-">Documents</h4>
                    </div>
                    <div id="document">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">
                                <label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="document" style="font-weight:bold;">Identify Proof -</label>
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    <input type="name" class="form-control"  id="email" placeholder="0000000000">
                                </div>
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    <label class="btn btn-default btn-file" style="margin:0;">
                                        Browse <input type="file" name="document[]" required style="display: none;">
                                    </label>
                                </div>
                                <div class="col-lg-2 col-sm-2 col-md-2 col-xs-6">
                                    <i class="fa fa-2x fa-plus-circle" onclick="addDocument()"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="references">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">
                                <label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="references" style="font-weight:bold;">References -</label>
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                                    <input type="name" class="form-control" id="email" placeholder="0000000000" style="margin-bottom:10px;">
                                </div>
                                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="margin-bottom:10px;">
                                    <label class="btn btn-default btn-file" style="margin:0;">
                                        Browse <input type="file" name="references[]" style="display: none;">
                                    </label>
                                </div>
                                <div class="col-lg-2 col-sm-2 col-md-2 col-xs-6">
                                    <i class="fa fa-2x fa-plus-circle" onclick="addReferences()"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary" id="addItemManually"><?= lang('submit') ?></button>
        </div>
        <?= form_close() ?>
    </div>
</div>
<script>
    function addQualifications() {
        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">' +
                '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">' +
                '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">' +
                '<input type="text" name="quali[]" class="form-control" id="email" placeholder="B.C.A">' +
                '</div>' +
                '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">' +
                '<input type="text" name="yr[]" class="form-control date" id="email" placeholder="">' +
                '</div>' +
                '<label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="email" onclick="addQualifications()"><i class="fa fa-plus-circle fa-2x"></i></label>' +
                '</div>' +
                '</div>';
        $('.qualification').append(data);
    }

    function addTraining() {
        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">' +
                '<label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="email">Special Training:</label>' +
                '<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">' +
                '<input type="text" name="special_training[]" class="form-control" id="email" placeholder="Tally - Accounting Software" style="margin-bottom:10px;"><i class="fa fa-2x fa-plus-circle" onclick="addTraining()"></i>' +
                '</div>' +
                '</div>';
        $('#training').append(data);
    }
    function addExperience() {
        var data = '<tr>' +
                '<td><input type="text"  class="form-control" id="email"  name="year[]" placeholder="Year"></td>' +
                '<td><input type="text" class="form-control" id="email" name="company[]" placeholder="Company"></td>' +
                '<td><input type="text" class="form-control" id="email" name="position[]" placeholder="Position"></td>' +
                '<td><input type="text" class="form-control" id="email" name="reference[]" placeholder="Reference"></td>' +
                '<td><i class="fa fa-2x fa-plus-circle" onclick="addExperience()"></i></td>' +
                ' </tr>';
        $('#experiance').append(data);
    }
    function addDocument() {
        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">' +
                '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">' +
                '<label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="email" style="font-weight:bold;">Identify Proof -</label>' +
                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">' +
                '<input type="name" class="form-control" id="email" placeholder="0000000000">' +
                '</div>' +
                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">' +
                '<label class="btn btn-default btn-file" style="margin:0;">' +
                'Browse <input type="file" name="document[]" style="display: none;">' +
                '</label>' +
                ' </div>' +
                '<div class="col-lg-2 col-sm-2 col-md-2 col-xs-6">' +
                ' <i class="fa fa-2x fa-plus-circle" onclick="addDocument()"></i>' +
                ' </div>' +
                '</div>' +
                '</div>';
        $('#document').append(data);
    }

    function addReferences() {
        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">' +
                '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">' +
                '<label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="email" style="font-weight:bold;">References -</label>' +
                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">' +
                ' <input type="name" class="form-control" id="email" placeholder="0000000000" style="margin-bottom:10px;">' +
                ' </div>' +
                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="margin-bottom:10px;">' +
                '<label class="btn btn-default btn-file" style="margin:0;">' +
                '  Browse <input type="file" name="references[]" style="display: none;">' +
                '  </label>' +
                '</div>' +
                '<div class="col-lg-2 col-sm-2 col-md-2 col-xs-6">' +
                '<i class="fa fa-2x fa-plus-circle" onclick="addReferences()"></i>' +
                ' </div>' +
                '</div>' +
                '</div>';
        $('#references').append(data);
    }

</script>
<?= $modal_js ?>
