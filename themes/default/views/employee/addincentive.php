<?= $modal_js ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button"  class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <!--<input type="text"--> 
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('incentive'); ?></h4>
        </div>
        <?php
//        $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'addincentive');
//        echo form_open_multipart("employee/addincentive/" . $empdata->id, $attrib);
        ?>
        <form method="post" id="addincent" data-toggle="validator" role="form">
            <div class="modal-body">
                <p><?= lang('enter_info'); ?></p>
                <div class="form-group">
                    <input type="text" name="emp_id" value="<?= $empdata->id ?>" class="form-control hidden">
                    <?= lang('totalincentive', 'totalincentive'); ?>
                    <?= form_input('totalincentive', $empdata->incentive, 'class="form-control tip" id="totalincentive" readonly  required="required"'); ?>
                </div>
                <div class="form-group">
                    <?= lang('date', 'date'); ?>
                    <?= form_input('date', date('d/m/Y h:i:s'), 'class="form-control tip" id="date" readonly required="required"'); ?>
                </div>
                <div class="form-group">
                    <?= lang('type', 'type'); ?>
                    <?php $opts = array('addition' => 'Add Incentive', 'subtraction' => 'Pay Incentive'); ?>
                    <?= form_dropdown('type', $opts, set_value('type', 'subtraction'), 'class="form-control tip" id="type"  required="required"'); ?>
                </div>
                <div class="form-group">
                    <?= lang('amount', 'amount'); ?>
                    <?= form_input('amount', '0', 'class="form-control tip" id="amount" required="required" data-bv-regexp="true"
                               data-bv-regexp-regexp="^\d+(\.\d{0,2})?$" data-bv-regexp-message="Only Numbers Allowed"'); ?>
                </div>
                <div class="form-group">
                    <label for="note_removed"><?php echo $this->lang->line("note"); ?></label>
                    <div class="controls"> 
                        <!--<input type="text" name="note" class="form-control">-->
                        <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : $this->sma->decode_html($damage->note)), 'class="form-control" id="note"'); ?> 
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" id="sub" class="btn btn-primary"><?= lang('submit') ?></button>
                <?php // echo form_submit('edit_adjustment', 'Add Incentive', 'class="btn btn-primary" id="submit"'); ?>
            </div>
        </form>
    </div>

    <?php // echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>

<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        $.fn.datetimepicker.dates['sma'] = <?= $dp_lang ?>;
    });

//    $("#addincentive").submit(function (event) {
//        var totalincentive = $('#totalincentive').val();
//        var amount = $('#amount').val();
//        var type = $('#type').val();
//        var total = parseFloat(totalincentive);
//        var amt = parseFloat(amount);
//
//        if (type == 'subtraction') {
//            alert(total);
//            alert(amt);
//            if (total < amt) {
//                alert('true');
//                bootbox.alert('Please Enter Valid Amount');
//                $('submit').prop("disabled", "");
//            } else {
//                alert();
//                $('#addincentive').sumbit();
//            }
//        }
//    });




    $("#sub").click(function (e) {
        e.preventDefault();
        var flag = 1;
        var totalincentive = $('#totalincentive').val();
        var amount = $('#amount').val();
        var type = $('#type').val();
        var total = parseFloat(totalincentive);
        var amt = parseFloat(amount);
        
        if (type == 'subtraction') {
            if (total < amt) {
                bootbox.alert('Please Enter Valid Amount');
                flag = 0;
            } else {
                flag = 1;
            }
        }
        if (flag == '1') {
            var v = $('#addincent').serialize();
            $('#modal-loading').show();
            $.ajax({
                type: "get",
                async: false,
                url: "<?= site_url('employee/addincentive') ?>?" + v,
                dataType: "json",
                success: function (scdata) {
                    $('#myModal').modal('hide');
                    oTable['employee'].fnDraw();
                    
                },
                error: function () {
                    $('#modal-loading').hide();
                }
            });
            $('#modal-loading').hide();
        }
    });

</script>



