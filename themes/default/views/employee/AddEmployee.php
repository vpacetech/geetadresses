<?php

if (isset($department)) {
    # code...
    // echo "<pre>";print_r($department);exit;
}

?>


<style>
    fieldset.scheduler-border {
        border: 1px solid #ddd !important;
        padding: 0 1em 1em 1em !important;
        margin: 0 0 1.1em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
        box-shadow:  0px 0px 0px 0px #000;
    }


    legend.scheduler-border {
        width:inherit; /* Or auto */
        padding:0 10px; /* To give a bit of padding on the left and right */
        border-bottom:none;

    }
    .cal 
    {
        position:relative;
        top:-30px;
        left:90%;
    }
    /*
    a {
        color: #000 !important;    
            }*/
    .nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
        color: #6164c0 !important;
        background-color: #fff !important;
        border-radius:0 !important;
    }

    .nav-pills>li>a{
        color: #6164c0 !important;
        background-color: #e9ebec !important;
        border-radius:0 !important;
    }
    .tab-content>.active {
        display: block;
        border : 1px solid #ccc;
        padding:5px !important;
    }
    .btton
    {
        padding: 5px;
        width:100%;
    }

    .stepwizard-step {
        display: table-cell;
        /*text-align: center;*/
        position: relative;
    }
    .form-horizontal .control-label {
        padding-top: 0px;
    }
</style>
<!-- <script type="text/javascript" src="<?= $assets ?>js/webcam.js"></script> -->

<div id="exTab1" class="container-fluid" style="background-color: #ffff;padding: 10px" ng-controller="employee">	

    <!--    <ul  class="nav nav-pills">
            <li class="active"><a  href="#1a" data-toggle="tab">General Details</a></li>
            <li><a href="#2a" data-toggle="tab">User Fields</a></li>
            <li><a href="#3a" data-toggle="tab">Qualification</a></li>
            <li><a href="#4a" data-toggle="tab">Attachments</a></li>
            <li><a href="#login_details" data-toggle="tab">Login Credentials</a></li>
    
        </ul>-->

    <div class="stepwizard">
        <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step">
                <a href="#step-1" type="button"  class="btn btn-primary ">Personal Details</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-2" type="button" class="btn btn-default " disabled="disabled">Educational Qualification</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-3" type="button" class="btn btn-default " disabled="disabled">Work Experience</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-4" type="button" class="btn btn-default " disabled="disabled">Documents</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-5" type="button" class="btn btn-default " disabled="disabled">Join Details</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-6" type="button" class="btn btn-default " disabled="disabled">Employee Reference Income</a>
            </div>
        </div>
    </div>

    <?php
    $attrib = array('data-toggle' => 'validator', 'class' => 'form-horizontal', 'role' => 'form');
    echo form_open_multipart("employee/saveemployee", $attrib);
    ?>

    <div class="tab-pane" style="border: 1px solid #000; padding: 10px">
        <div class="row setup-content" id="step-1">

            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Employee Details</legend>
                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('dojoin', 'dojoin') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('dojoin', set_value('dojoin', ''), 'class="form-control tip date" id="dojoin"  data-bv-notempty="true" required="required"'); ?><i class="fa fa-calendar cal"></i>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('fname', 'fname') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('fname', set_value('fname', ''), 'class="form-control tip" id="fname" required="required" data-bv-notempty="true" data-bv-regexp="true"
                               data-bv-regexp-regexp="^[a-z\s,A-z]+$"
                               data-bv-regexp-message="Name must contain characters only"'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('mname', 'mname') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('mname', set_value('mname', ''), 'class="form-control tip" id="midname" data-bv-regexp="true"
                               data-bv-regexp-regexp="^[a-z\s,A-z]+$"
                               data-bv-regexp-message="Name must contain characters only"'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('lname', 'lname') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('lname', set_value('lname', ''), 'class="form-control tip" id="lname" required="required" data-bv-notempty="true" data-bv-regexp="true"
                               data-bv-regexp-regexp="^[a-z\s,A-z]+$"
                               data-bv-regexp-message="Name must contain characters only"'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('dob', 'dob') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('dob', set_value('dob', ''), 'class="form-control tip date" id="dob"  data-bv-notempty="true" required="required"'); ?><i class="fa fa-calendar cal"></i>
                                </div>
                                <!--                                <div class="col-sm-3">
                                                                    <button type="button" id="dobclear" class="btn btn-default">Clear</button>
                                                                </div>-->
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4"><?= lang('maritalstatus', 'maritalstatus') ?></label>
                                <div class="col-sm-8">
                                    <?php
                                    $mstatus = array(
                                        '' => "Select Marital Status",
                                        'Married' => "Married",
                                        'Unmarried' => 'Unmarried'
                                    );
                                    echo form_dropdown('maritalstatus', $mstatus, "Unmarried", 'class="form-control select" required="required" data-bv-notempty="true" id="maritalstatus" placeholder="' . lang("select") . " " . lang("store_name") . '"')
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('anniversary', 'anniversary') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('anniversary', set_value('anniversary', ''), 'class="form-control tip date" id="anniversary"'); ?><i class="fa fa-calendar cal"></i>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('no_of_dependends', 'no_of_dependends') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('no_of_dependends', set_value('no_of_dependends', ''), 'class="form-control tip" id="no_of_dependends" data-bv-regexp="true"
                                data-bv-regexp-regexp="[0-9]"
                                data-bv-regexp-message="this field must contain digits only"'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('domicile', 'domicile') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('domicile', set_value('domicile', ''), 'class="form-control tip" id="domicil"  data-bv-regexp="true"
                               data-bv-regexp-regexp="^[a-z\s,\.A-z]+$"
                               data-bv-regexp-message="this field must contain characters only"'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-4"><?= lang('gender', 'gender') ?></label>
                                <div class="col-sm-8">
                                    <?php
                                    $gender = array(
                                        '' => "Select Gender",
                                        'Male' => "Male",
                                        'Female' => 'Female'
                                    );
                                    echo form_dropdown('gender', $gender, "Male", 'class="form-control select" id="gender" required="required" data-bv-notempty="true" placeholder="' . lang("select") . " " . lang("store_name") . '"')
                                    ?>
                                </div>
                            </div>

                            <!--                        <div class="form-group">
                                                        <label class="control-label col-sm-3"></label>
                                                        <div class="col-sm-9">
                                                            <div class="checkbox">
                                                                <label><input type="checkbox" value="1" name="smreq" id="smreq"><?= lang('smsreq') ?></label>
                                                                <label><input type="checkbox" value="1" name="smsapprove" id="smsapprove"><?= lang('smsapprove') ?></label>
                                                                <label><input type="checkbox" value="1" name="notemp" id="notemp"><?= lang('notemp') ?></label>
                                                                <label><input type="checkbox" value="1" name="invattach" id="invattach"><?= lang('invattach') ?></label>
                                                            </div>
                                                        </div>
                                                    </div>-->
                        </fieldset>

                        <fieldset class="scheduler-border" style="padding: 0px 25px !important;">
                            <legend class="scheduler-border">Address</legend>

                            <div class="form-group">
                                <label><?= lang('address', 'address') ?></label>
                                <?php echo form_input('address', set_value('address', ''), 'class="form-control tip" id="address" required data-bv-notempty="true"'); ?>
                            </div>
<!--                            <div class="form-group">
                                <label><?= lang('address2', 'address21') ?></label>
                                <?php echo form_input('address2', set_value('address2', ''), 'class="form-control tip" id="address1"  data-bv-notempty="false"'); ?>
                            </div>

                             <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-group" >
                                        <label><?= lang('city', 'city1') ?></label>
                                        <?php echo form_input('city', set_value('city', ''), 'class="form-control tip" id="city" data-bv-notempty="false"'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label><?= lang('state', 'state1') ?></label>
                                        <?php echo form_input('state', set_value('state', ''), 'class="form-control tip" id="state"  data-bv-notempty="false"'); ?>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                                       <div class="form-group" style="padding-left: 20px !important">
                                                                            <label><?= lang('country', 'country1') ?></label>
                                    <?php echo form_input('country', set_value('country', ''), 'class="form-control tip" id="country"  data-bv-notempty="false"'); ?>
                                                                        </div>
                                    <div class="form-group" style="padding-left: 20px !important">
                                        <label><?= lang('zipcode', 'zipcode1') ?></label>
                                        <?php echo form_input('zipcode', set_value('zipcode', ''), 'class="form-control tip" id="zipcode"  data-bv-notempty="false" data-bv-notempty-message="Zip code is required and cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="6"
                               data-bv-stringlength-max="6"
                               data-bv-stringlength-message="The Zip code must be exact 6 numbers long"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The Zip code can only consist of digits"'); ?>
                                    </div>
                                </div>
                            </div> -->

                        </fieldset>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">

                        <fieldset class="scheduler-border" style="padding: 0px 25px !important;">
                            <legend class="scheduler-border">Contact Info</legend>

                            <div class="form-group">
                                <label><?= lang('telephonenos', 'telephonenodd') ?></label>
                                <?php echo form_input('telephoneno', set_value('telephoneno', ''), 'class="form-control tip" id="telephoneno"  data-bv-notempty="false" pattern="[0-9]{10}" maxlength="10"'); ?>
                            </div>

                            <div class="form-group">
                                <label><?= lang('mobileno', 'mobileno') ?></label>
                                <?php
                                echo form_input('mobileno', set_value('mobileno', ''), 'class="form-control tip" id="mobileno" required="required" data-bv-notempty="true"'
                                        . ' data-bv-notempty-message="The Mobile no is required and cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="10"
                               data-bv-stringlength-max="10"
                               data-bv-stringlength-message="The Mobile no must be exact 10 numbers long"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The Mobile no can only consist of digits"');
                                ?>
                            </div>
                            <div class="form-group">
                                <label><?= lang('alternet_mob', 'alternet_mob') ?></label>
                                <?php echo form_input('alternet_mob', set_value('alternet_mob', ''), 'class="form-control tip" id="alternet_mobile"  data-bv-notempty="false" pattern="[0-9]{10}" maxlength="10"'); ?>
                            </div>
<!--                             <div class="form-group">
                                <label><?= lang('email', 'email') ?></label>
                                <input type="email" name="email" class="form-control" value="<?= set_value('email', '') ?>" required="required" id="email"/>
                                <?php // echo form_input('email', set_value('email', ''), 'class="form-control tip" id="email" required="required" data-bv-notempty="true"');   ?>
                            </div> -->
                            <!--                        <div class="form-group">
                                                        <label><?= lang('website', 'website') ?></label>
                                                        <input type="url" name="website" value="<?= set_value('website', '') ?>" class="form-control" id="email_address"/>
                            <?php // echo form_input('website', set_value('website', ''), 'class="form-control tip" id="websites" ="required" data-bv-notempty="true"');   ?>
                                                    </div>-->

                        </fieldset>

                        <!--                        <fieldset class="scheduler-border">
                                                    <legend class="scheduler-border">Sales Commission Details</legend>
                        
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-7"><?= lang('scp', 'scp') ?></label>
                                                        <div class="col-sm-5">
                        <?php
                        echo form_input('scp', set_value('scp', ''), 'class="form-control tip" id="scp"  data-bv-notempty="false"'
                                . 'data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="Only Numbers Allowed" 
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="2"
                               data-bv-stringlength-max="2"
                               data-bv-stringlength-message="The Percentage must be exact 2 numbers long"');
                        ?>
                                                        </div>
                        
                                                    </div>
                        
                        
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-7"><?= lang('cqp', 'cqp1') ?></label>
                                                        <div class="col-sm-5">
                        <?php echo form_input('cqp', set_value('cqp', ''), 'class="form-control tip" id="cqp"  data-bv-notempty="false"'); ?>
                                                        </div>
                        
                                                    </div>
                                                </fieldset>-->

<!--                         <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Spot Discount at Invoice</legend>
                            <div class="form-group">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="hlsda"  value="1" id="hlsda"><?= lang('hlsda', 'hlsda') ?></label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-7"><?= lang('msd', 'msd') ?></label>
                                <div class="col-sm-5">
                                    <?php
                                    echo form_input('msd', set_value('msd', ''), 'class="form-control tip" disabled id="msd1" data-bv-notempty="false"'
                                            . 'data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="Only Numbers Allowed" 
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="2"
                               data-bv-stringlength-max="2"
                               data-bv-stringlength-message="The Percentage must be exact 2 numbers long"');
                                    ?>
                                </div>
                                <label class="control-label col-sm-1">%</label>
                            </div>
                        </fieldset>

                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border"><?= lang('refered_by') ?></legend>
                            <div class="controls"> 
                                <?php
                                $emp[""] = "";
                                foreach ($employees as $biller) {
                                    $emp[$biller->id] = $biller->fname . ' ' . $biller->mname . ' ' . $biller->lname;
                                }
                                echo form_dropdown('refered_by', $emp, (isset($_POST['biller']) ? $_POST['biller'] : $Settings->default_biller), ' data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("refered_by") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </fieldset> -->
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="row">
                            <div class="form-group" style="padding:0 20px;">
                                <?= lang("photo", "biller_logo"); ?> 

                                <input id="store_logo_up" name="photo" type="file"  class="form-control" 
                                       data-show-upload="false"
                                       data-show-preview="false" 
                                       data-bv-file-maxsize="300*100" 
                                       />
                            </div>
                        </div>  
                        <br>
                        <div class="form-group" style="padding:0 16px;">
                            <input type="hidden" name="supplierPhoto" id="supplierphoto" value="<?= set_value('supplierPhoto', '') ?>">
                            <div id="my_camera" style="border:1px solid #000000;background:#eee; width: 100%; height: 500px; display: none;"  width="100%" height="500px"></div>
                            <img src="" id="image_upload_preview" style="border:1px solid #000000;background:#eee" width="100%" height="500px">
                            <div id="results"></div>
                        </div>
                        <br><center><button type="button" id="takepicture" style="">Take Picture with Attached Camera</button>
                            <input type=button value="Take Snapshot" id="tacksnaps" onClick="take_snapshot()" style="display: none"></center>
                        <br>


                    </div>
                </div>

                <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
            </div>

        </div>
        <div class="row setup-content" id="step-2">

            <div class="col-md-12">
                <div class="row">
                    <div class="qualifications">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                    <label for="qualific">Qualification </label>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                    <label for="yr">Year of Pasing</label>
                                </div>
                            </div>
                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

                                    <input type="text" name="quali[]" class="form-control"  id="quali" placeholder="Qualification">
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

                                    <input type="text" name="yr[]" class="form-control" id="yr" placeholder="Year of Pasing">
                                </div>
                                <label class="col-lg-2 col-sm-2 col-md-2 col-xs-12 addquali" onclick="addQuali()">
                                    <i class="fa fa-plus-circle fa-2x"></i>
                                </label>
                                <label class="col-lg-2 col-sm-2 col-md-2 col-xs-12 removequali" style="display:none" onclick="removeQuali(this)">
                                    <i class="fa fa-minus-circle fa-2x"></i>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                        <div id="training">
                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                                <label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12">Special Training:</label>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                    <input type="text" name="special_training[]" class="form-control"  id="special_training" placeholder="Tally - Accounting Software" style="margin-bottom:10px;">
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                    <i class="fa fa-2x fa-plus-circle Addtra" onclick="addTraining()"></i>                                    
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                    <i class="fa fa-2x fa-minus-circle Minustra" style="display:none" onclick="removeTraining(this)"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
            </div>

        </div>

        <div class="row setup-content" id="step-3">

            <div class="col-md-12">
                <div class="row">
                    <div class="qualification">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-8">
                                    <label for="yr">Years</label>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-8">
                                    <label for="comp">Name Of Company</label>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-8">
                                    <label for="pos">position</label>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-8">
                                    <label for="ref">Reference</label>
                                </div>
                            </div>
                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-8">

                                    <input type="text" name="year[]" class="form-control"  id="year" placeholder="Years">
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-8">

                                    <input type="text" name="company[]" class="form-control" id="comp" placeholder="Name Of Company">
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-8">

                                    <input type="text" name="position[]" class="form-control"  id="pos" placeholder="position">
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-8">

                                    <input type="text" name="reference[]" class="form-control"  id="reference" placeholder="reference">
                                </div>
                                <label class="col-lg-3 col-md-3 col-sm-3 col-xs-8 add" onclick="addQualifications()">
                                    <i class="fa fa-plus-circle fa-2x" ></i>
                                </label>
                                <label class="col-lg-3 col-md-3 col-sm-3 col-xs-8 minuss" onclick="removeQualification(this)"  style="display:none">
                                    <i  class="fa fa-minus-circle fa-2x"></i>
                                </label>
                                
                            </div>
                        </div>
                    </div>
                </div>
<!--                 <div class="row">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                        <div id="training">
                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">
                                <label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12">Special Training:</label>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                    <input type="text" name="special_training[]" class="form-control"  id="special_training" placeholder="Tally - Accounting Software" style="margin-bottom:10px;">
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                    <i class="fa fa-2x fa-plus-circle" onclick="addTraining()"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->

                <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
            </div>
        </div>
        <!-- <div class="row setup-content" id="step-4">
            <div class="">
                <div class="col-md-12">
                    <div class="row">

                            <div class="form-group" >
                                <label for="proof" class="col-sm-3 control-label">Identify Proof -</label>
                                <div class="col-sm-5">

                                    <input id="documentfile1" name="document[]" type="file" required data-bv-notempty="true" class="form-control docproof" 
                                        data-show-upload="false"
                                        data-show-preview="false" 
                                    
                                    />
                                </div>
                                <div class="col-lg-1 col-sm-1 col-md-1 col-xs-2">
                                        <button type="button" ng-if="$index < attachment.length - 1" class="btn btn-default" ng-click="attachment.splice($index, 1);" href="">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                        <a href="" ng-if="$index == attachment.length - 1" ng-click='attachment.push({identityprof: ""});' class="btn btn-default">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                </div>
                            </div>

                    </div>
                    <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                    <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                </div>
            </div>
        </div> -->


        <div class="row setup-content" id="step-4">
            <div class="">
                <div class="col-md-12">
                    <div class="row">
                        <div class="identity">
                            <div class="form-group">
                                    <label class="col-sm-3 control-label" for="proof">Identity proof</label>
                                    <div class="col-sm-5">
                                        <input id="proof" type="file" class="form-control docproof"  name="identity[]"  
                                            data-bv-file="true"
                                            data-bv-file-extension = "jpeg,png,pdf"
                                            data-bv-file-type = "image/jpeg,image/png,application/pdf"	
                                            data-bv-file-maxsize = "2146304"	
                                            data-show-upload="false"
                                            data-show-preview="false"
                                        />
                                    </div>
                                    <?php
                                    $i = 0; 
                                    ?>
                                    <label class="col-lg-2 col-sm-2 col-md-2 col-xs-12 adddoc" onclick="addDoc()" >
                                            <i class="fa fa-plus-circle fa-2x"></i>
                                    </label>

                            </div>
                        </div>
                        
                        <div class="references">

                            <div class="form-group">
                                <label class="col-sm-3 control-label" for="references">References</label>
                                <div class="col-sm-5">
                                    <input id="references" type="file" class="form-control docreferences"  name="references[]" 
                                            data-bv-file="true"
                                            data-bv-file-extension = "jpeg,png,pdf"
                                            data-bv-file-type = "image/jpeg,image/png,application/pdf"	
                                            data-bv-file-maxsize = "2146304"	
                                            data-show-upload="false"
                                            data-show-preview="false"
                                    />
                                </div>

                                <label class="col-lg-2 col-sm-2 col-md-2 col-xs-12 addref visible" onclick="addref()" >
                                                <i class="fa fa-plus-circle fa-2x"></i>
                                </label>

                                
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                    <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                </div>
            </div>
        </div>
        <div class="row setup-content" id="step-5">
            <div class="col-md-12">
                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-sm-3"><?= lang('department', 'department') ?></label>
                        <div class="col-sm-5">
                            <select class="form-control" name="department" id="department" required data-bv-notempty="true">
                                <option selected option disabled>select</option>
                                    <?php 
                                foreach ($department as $department) {?>
                                    
                                    <option value="<?php echo $department->id ?>"><?php echo $department->name ?></option>        
                                <?php        
                                }?>
                            
                            </select>                                
                        </div>
                    </div>
                    
                    <div class="form-group">
                            <label class="control-label col-sm-3"><?= lang('position', 'position') ?></label>
                            <div class="col-sm-5">
                                <select id="position" class="form-control" name="position_of_emp">
                                    <option selected option disabled>select</option>
                                        <?php 
                                    foreach ($position as $position) {?>
                                        
                                        <option value="<?php echo $position->id ?>"><?php echo $position->name ?></option>        
                                    <?php        
                                    }?>
                                </select>


                            </div>
                                <!-- <label class="col-sm-3" onclick="addQualifications()">
                                        <i class="fa fa-plus-circle fa-2x" ></i>
                                </label> -->
                                <!-- <button type="button" id="add_position" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">Add Position</button> -->
                                    <!-- Button trigger modal -->
                            <!-- <div class="input-group-addon no-print"> -->
                                    <a href="<?php echo site_url('employee/addPostion'); ?>" data-toggle="modal" data-target="#myModal" class="external"><button class = "btn btn-primary">Add Position</button></a>
                            <!-- </div> -->

                            <!-- Modal -->
                                <!-- <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                    <h5 class="modal-title" id="myModalLabel">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    </div>
                                    <div class="modal-body">
                                    ...
                                    </div>
                                    <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Save changes</button>
                                    </div>
                                </div>
                                </div>
                                </div> -->

                                <!-- <button type="button" id="add_position" class="btn btn-primary">Add Position</button> -->
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3"><?= lang('section', 'section') ?></label>
                        <div class="col-sm-5">
                            <select class="form-control" id="section" name="section" required data-bv-notempty="true">
                                <option selected option disabled>select</option>
                                    <?php 
                                foreach ($section as $section) {?>
                                    
                                    <option value="<?php echo $section->id ?>"><?php echo $section->name ?></option>        
                                <?php        
                                }?>
                            
                            </select>                                
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3"><?= lang('sal', 'sal') ?></label>
                        <div class="col-sm-2">
                                <input id="sal" name="salary" type="text" class="form-control" required data-bv-notempty="true" />
                        </div>
                        <div class="col-sm-5">
                            <b>&#8360; / Month</b>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3"><?= lang('shift', 'shift') ?></label>
                        <div class="col-sm-5">
                            <select class="form-control" name="shift">
                                <option selected option disabled>select</option>
                                <option value="Morning">Morning</option>        
                                <option value="Evening">Evening</option>        
                            </select>                                
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3"><?= lang('refbyemp', 'refbyemp') ?></label>
                        <div class="col-sm-5">
                            <select class="form-control" id="refbyempl" name="refered" >
                                <option selected option disabled>select</option>
                                    <?php 
                                foreach ($employees as $employees) {?>
                                    
                                    <option value="<?php echo $employees->id ?>"><?php echo $employees->fname ?></option>        
                                <?php        
                                }?>
                            
                            </select>                                
                        </div>
                    </div>
<!--                     <div class="form-group">
                        <label class="control-label col-sm-5"><?= lang('location', 'location') ?></label>
                        <div class="col-sm-7">
                            <?php
                            $us = "";
                            $us[''] = "Select " . lang('location');
                            foreach ($store as $usr) {
                                $us[$usr->id] = $usr->name;
                            }
                            echo form_dropdown('location', $us, '', 'class="form-control select" id="location" data-bv-notempty="true" required="required" placeholder="' . lang("select") . " " . lang("location") . '"')
                            ?>

                            <?php
                           echo form_dropdown('against_ref_no',$get_ref_no,(isset($_POST['against_ref_no']) ? $_POST['against_ref_no'] : $voucher->against_ref_no),'data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" class="form-control input-tip select" id="ref" style="width:100%;"');
                            ?>


                        </div>

                    </div>

                        <div class="form-group">
                            <label class="control-label col-sm-5"><?= lang('section', 'section') ?></label>
                            <div class="col-sm-7">
                                <?php
                                $us = "";
                                echo form_dropdown('section', $us, (isset($_POST['section']) ? $_POST['section'] : $us->name), 'data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("section") . '" class="form-control input-tip select" data-bv-notempty="true" required="required" id="section" style="width:100%;"');
                                ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-4"><?= lang('desigation', 'desigation') ?></label>
                            <label class="control-label col-sm-5"><?= lang('position', 'position') ?></label>
                            <div class="col-sm-7">
                                <?php
                                $us = "";
                                $us[''] = "Select " . lang('position');
                                foreach ($position as $usr) {
                                    $us[$usr->id] = $usr->name;
                                }

                                echo form_dropdown('desigation', $us, '', 'class="form-control select" data-bv-notempty="true" required="required" id="position" placeholder="' . lang("select") . " " . lang("username") . '"')
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5"><?= lang('shift', 'shift') ?></label>
                            <div class="col-sm-7">
                                <?php
                                $us = array(
                                    '' => 'Select Shift',
                                    'Morning' => 'Morning',
                                    'Evening' => 'Evening'
                                );
                                echo form_dropdown('shift', $us, '', 'class="form-control select" id="shift" placeholder="' . lang("select") . " " . lang("username") . '"')
                                ?>
                            </div>
                        </div> -->
                </div>
                <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
            </div>
        </div>



        <div class="row setup-content" id="step-6">
            <div class="col-md-12">
                <div class="row">

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="ref_emp">Reffered Employee</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" style="width:200px" id="refemp" name="ref_emp_income"
                            
                                data-bv-integer="true"
                                data-bv-integer-message="The value is not an integer" 
                            
                             />
                        </div>
                        <div class="col-sm-1" style="width:40px">
                            <b>in</b>
                        </div>
                        <div class="col-sm-2">
                            <!-- <b>in</b> -->
                            <input type="text" class="form-control" style="width:200px" id="refemp1" name="ref_emp_income_days"

                                data-bv-integer="true"
                                data-bv-integer-message="The value is not an integer" 
                            
                             />
                        </div>
                        <div class="com-sm-1">
                            <b>&emsp;days</b>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="customer">Customer</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" style="width:200px" id="cust" name="customer_income"
                                data-bv-integer="true"
                                data-bv-integer-message="The value is not an integer" 
                            />
                        </div>
                        <div class="col-sm-1" style="width:40px">
                            <b>in</b>
                        </div>
                        <div class="col-sm-2">
                            <!-- <b>in</b> -->
                            <input type="text" class="form-control" style="width:200px" id="cust1" name="customer_income_days" 

                                data-bv-integer="true"
                                data-bv-integer-message="The value is not an integer" 

                            />
                        </div>
                        <div class="com-sm-1">
                            <b>&emsp;days</b>
                        </div>
                    </div>
                    
                </div>
                <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                <button type="submit" class="btn btn-primary nextBtn pull-right" type="button">save</button>
            </div>
        </div>
    </div>

    <!--end tab-content----->
    <br>
    <!--<div class="row text-center">-->
    <?php // echo form_submit('add_employee', lang('save'), 'class="btn btn-primary"'); ?>
        <!--<a class="btn btn-danger" href="<?= site_url('employee'); ?>" onclick="this.form.reset();">Cancel</a>-->
    <!--</div>-->
    <?php echo form_close(); ?>
<!--     <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div> -->

</div>





<script>
    $(function(){
        $('#store_logo_up').fileinput({
        maxImageWidth: 300,
        maxImageHeight: 100
    });
    $('.docproof').fileinput({
        maxImageWidth: 300,
        maxImageHeight: 100,

    });

    
    $('.docreferences').fileinput({
        maxImageWidth: 300,
        maxImageHeight: 100
    });
    
    


    })

    $(document).ready(function () {
        var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn'),
                allPrevBtn = $('.prevBtn');

        allWells.hide();

        // console.log($('div.setup-panel div a'));

        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                    $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-primary').addClass('btn-default');
                $item.addClass('btn-primary');
                allWells.hide();
                $target.show();
//          $target.find('input:eq(0)').focus();
            }
        });

        allPrevBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

            prevStepWizard.removeAttr('disabled').trigger('click');
        });

        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url'],input[type='email'],input[type='file'],fieldset > input[type='text']"),
                    isValid = true;
            
                    console.log(curInputs);
                    console.log(curStepBtn);
            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid)
                nextStepWizard.removeAttr('disabled').trigger('click');
        });

        $('div.setup-panel div a.btn-primary').trigger('click');
    });
</script>

<script>




    $(document).on('ifUnchecked', '#hlsda', function (e) {
        $('#msd1').attr('disabled', "disabled");
        $('#msd1').attr('required', 'required');
    });
    $(document).on('ifChecked', '#hlsda', function (e) {
        $('#msd1').removeAttr("disabled");
        $('#msd1').removeAttr('required');
    });

    $(document).on('ifUnchecked', '#isloyatlity', function (e) {
        $('#loyalityno').attr('disabled', "disabled");
        $('#enrolldate').attr('disabled', "disabled");
        $('#loyalitypoint').attr('disabled', "disabled");
        $('#referby').attr('disabled', "disabled");
        $('#loyalityno').attr('required', 'required');
        $('#enrolldate').attr('required', 'required');
        $('#loyalitypoint').attr('required', 'required');
        $('#referby').attr('required', 'required');

    });
    $(document).on('ifChecked', '#isloyatlity', function (e) {
        $('#loyalityno').removeAttr("disabled");
        $('#enrolldate').removeAttr("disabled");
        $('#loyalitypoint').removeAttr("disabled");
        $('#referby').removeAttr("disabled");
        $('#creaditlimit').removeAttr('required');
        $('#enrolldate').removeAttr('required');
        $('#loyalitypoint').removeAttr('required');
        $('#referby').removeAttr('required');
    });

/*     $('#store_logo_up').fileinput({
        maxImageWidth: 300,
        maxImageHeight: 100
    }); */
    // $('#documentfile1').fileinput({
    //     maxImageWidth: 300,
    //     maxImageHeight: 100
    // });
    // $('#referencesfile').fileinput({
    //     maxImageWidth: 300,
    //     maxImageHeight: 100
    // });

    // $('#document').fileinput({
    //     maxImageWidth: 300,
    //     maxImageHeight: 100
    // });
    $('#photo').fileinput({
        maxImageWidth: 300,
        maxImageHeight: 100
    });

    $('#documentfile').fileinput({
        maxImageWidth: 300,
        maxImageHeight: 100
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            $("#supplierphoto").val('');
        }
    }

    $("#store_logo_up").change(function () {
        readURL(this);
        // alert(this);
    });
    $("#takepicture").click(function () {
        $('#my_camera').css('display', 'inline-block');
        $('#tacksnaps').css('display', 'inline-block');
        $('#image_upload_preview').css('display', 'none');
        $('#takepicture').css('display', 'none');
    });
    $("#dobclear").click(function () {

        $('#dob').val('');
    });
</script>

<script language="JavaScript">
    Webcam.set({
        width: 400,
        height: 330,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
    Webcam.attach('#my_camera');
</script>

<script language="JavaScript">
    function take_snapshot() {
        // take snapshot and get image data
        Webcam.snap(function (data_uri) {
            readURL("input[name='webcam']");
            Webcam.upload(data_uri, 'suppliers/savecamImage', function (code, text) {
                $("#image_upload_preview").attr("src", 'assets/uploads/' + text);
                $("#supplierphoto").val(text);
                $('#my_camera').css('display', 'none');
                $('#image_upload_preview').css('display', 'inline-block');
                $('#takepicture').css('display', 'inline-block');
                $('#tacksnaps').css('display', 'none');
            });
        });
    }
</script>

<script>


    function addQuali() {

        $('.addquali').hide();
        $('.removequali').show(); 


        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">' +
                '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">' +
                '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">' +
                '<input type="text" name="quali[]" class="form-control" id="email" placeholder="B.C.A">' +
                '</div>' +
                '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">' +
                '<input type="text" name="yr[]" class="form-control" id="email" placeholder="Year of Passing">' +
                '</div>' +
                '<label class="col-lg-2 col-sm-2 col-md-2 col-xs-12 addquali" for="email" onclick="addQuali()"><i class="fa fa-plus-circle fa-2x"></i></label>' +
                '<label class="col-lg-2 col-sm-2 col-md-2 col-xs-12 removequali" for="email" style="display:none" onclick="removeQuali(this)"><i class="fa fa-minus-circle fa-2x"></i></label>' +
                '</div>' +
                '</div>';
        $('.qualifications').append(data);
    }


    // $('.visible').click(function(){
    //     console.log($(this).next('label'));
    //     $(this).next('label').hide();
    //     // $(this).hide();
    //     // alert('hi');
    // });
    function addDoc()
    {
        // $('.adddoc').hide();
        // $('.removedoc').show(); 
        
        var data = 
                '<div class="form-group doc">' +
                // '<div class="delete">' +
                '<label class="col-sm-3 control-label" style="visibility:hidden" for="proof1">Identity proof</label>' +
                '<div class="col-sm-5">' +
                    '<input id="proof1"  type="file" class="form-control docproof1"  name="identity[]"  required data-bv-notempty="true"' +  
                        'data-bv-file="true"' +
                        'data-bv-file-extension = "jpeg,png,pdf"' +
                        'data-bv-file-type = "image/jpeg,image/png,application/pdf"' +
                        'data-bv-file-maxsize = "2146304"' +	
                        'data-show-upload="false"' +
                        'data-show-preview="false"' +
                     '/>' +
                '</div>' +
                            
                // '<label class="col-lg-2 col-sm-2 col-md-2 col-xs-12 adddoc" onclick="addDoc()">' +
                //     '<i class="fa fa-plus-circle fa-2x"></i>' +
                // '</label>' + 
                
                '<label class="col-lg-2 col-sm-2 col-md-2 col-xs-12 removedoc" style="" onclick="removeDoc(this)">' +
                    '<i class="fa fa-minus-circle fa-2x"></i>' +
                '</label>' +
                '</div>';

                $('.identity').append(data);
                $('.docproof1').fileinput({
                    maxImageWidth: 300,
                    maxImageHeight: 100
                });
    }

    function removeDoc(val)
    {
        alert("remove");
        console.log(val);
        console.log($(val).closest('div'));
        $(val).closest('div').remove();
    }
    
    function addref()
    {
        var data = 
        '<div class="form-group">' +

            '<label class="col-sm-3 control-label" style="visibility:hidden" for="references">References</label>' +
            '<div class="col-sm-5">' +
                '<input id="references" type="file" class="form-control docreferences"  name="references[]"  required data-bv-notempty="true"' +
                        'data-bv-file="true"' +
                        'data-bv-file-extension = "jpeg,png,pdf"' +
                        'data-bv-file-type = "image/jpeg,image/png,application/pdf"' +
                        'data-bv-file-maxsize = "2146304"' +	
                        'data-show-upload="false"' +
                        'data-show-preview="false"' +
                '/>' +
            '</div>' +

            '<label class="col-lg-2 col-sm-2 col-md-2 col-xs-12 removeref"  onclick="removeref(this)">' +
                    '<i class="fa fa-minus-circle fa-2x"></i>' +
            '</label>' +

                                
        '</div>';

        $('.references').append(data);
        $('.docreferences').fileinput({
            maxImageWidth: 300,
            maxImageHeight: 100
        });
    }

    function removeref(val)
    {
        alert("remove");
        console.log(val);
        console.log($(val).closest('div'));
        $(val).closest('div').remove();
    }

    function setfilename(This)
    {
        // var fileName = val.split('\\').pop();
        // $('#documentfile1').val(fileName);
        alert("hi");
        alert($(This).val());
        var fieldVal = $(This).val();

        // Change the node's value by removing the fake path (Chrome)
        fieldVal = fieldVal.replace("C:\\fakepath\\", "");
        alert(fieldVal);
        if (fieldVal != undefined || fieldVal != "") {
            $(This).next(".custom-file-control").attr('data-content', fieldVal);
        }


        // alert(fileName);
    }

    function addQualifications() {
        alert('hi');

        $('.minuss').show();
        $('.add').hide(); 

        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">' +
                '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">' +
                '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-8">' +
                '<input type="text" name="year[]" class="form-control" id="year" placeholder="Years">' +
                '</div>' +
                '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-8">' +
                '<input type="text" name="company[]" class="form-control" id="comp" placeholder="Name Of Company">' +
                '</div>' +
                '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-8">' +
                '<input type="text" name="position[]" class="form-control" id="pos" placeholder="position">' +
                '</div>' + 
                '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-8">' +
                '<input type="text" name="reference[]" class="form-control" id="reference" placeholder="reference">' +
                '</div>' +
                '<label class="col-lg-3 col-md-3 col-sm-3 col-xs-8 add" onclick="addQualifications()"><i class="fa fa-plus-circle fa-2x" ></i></label>' +
                '<label class="col-lg-3 col-md-3 col-sm-3 col-xs-8 minuss" onclick="removeQualification(this)"  style="display:none"><i  class="fa fa-minus-circle fa-2x"></i></label>' +
                '</div>' +
                '</div>';

                
                $('.qualification').append(data);


                // $('.minuss').click(removeQualification);
/*                 $('.minuss').click(function(){
                    alert("removed");
                    console.log($(this));
                    console.log($(this).closest('div').parent());
                    $(this).closest('div').parent().remove();
                }); */

    }
    function removeQualification(val)
    {
        alert("removed");
        console.log($(val));
        console.log($(val).closest('div'));
        $(val).closest('div').remove();
    }

    function removeTraining(val)
    {
        alert("removed");
        console.log($(val));
        console.log($(val).closest('div').parent());
        $(val).closest('div').parent().remove();
    }
    

    function removeQuali(val)
    {
        alert("removed");
        console.log($(val));
        console.log($(val).closest('div'));
        $(val).closest('div').remove();
    }

    function addTraining() {
        alert("hi");
        $('.Minustra').show();
        $('.Addtra').hide(); 

        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">' +
                '<label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="email">Special Training:</label>' +
                '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">' +
                '<input type="text" name="special_training[]" class="form-control" id="email" placeholder="Tally - Accounting Software" style="margin-bottom:10px;">' +
                '</div>' +
                '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">' +
                '<i class="fa fa-2x fa-plus-circle Addtra" onclick="addTraining()"></i>' +
                '</div>' +
                '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">' +
                '<i class="fa fa-2x fa-minus-circle Minustra" style="display:none" onclick="removeTraining(this)"></i>' +
                '</div>'+
                '</div>';
        $('#training').append(data);
    }
    function addExperience() {
        var data = '<tr>' +
                '<td><input type="text"  class="form-control" id="email"  name="year[]" placeholder="Year"></td>' +
                '<td><input type="text" class="form-control" id="email" name="company[]" placeholder="Company"></td>' +
                '<td><input type="text" class="form-control" id="email" name="position[]" placeholder="Position"></td>' +
                '<td><input type="text" class="form-control" id="email" name="reference[]" placeholder="Reference"></td>' +
                '<td><i class="fa fa-2x fa-plus-circle" onclick="addExperience()"></i></td>' +
                ' </tr>';
        $('#experiance').append(data);
    }
    function addDocument() {
                var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">' +
                '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">' +
                '<label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="email" style="font-weight:bold;">Identify Proof -</label>' +
                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">' +
                '<input type="name" class="form-control" id="email" placeholder="0000000000">' +
                '</div>' +
                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">' +
                '<label class="btn btn-default btn-file" style="margin:0;">' +
                'Browse <input type="file" name="document[]" style="display: none;">' +
                '</label>' +
                ' </div>' +
                '<div class="col-lg-2 col-sm-2 col-md-2 col-xs-6">' +
                ' <i class="fa fa-2x fa-plus-circle" onclick="addDocument()"></i>' +
                ' </div>' +
                '</div>' +
                '</div>';
        $('#document').append(data);
    }
//     $("#documentfile1").change(function (e) {
//         var path = this.value;
//         this.form.field.value = "..." + path.substring(11, path.length);
//    });
    // $('#documentfile1').click(function(e){
    //     alert("hi");
    //     var fileName = e.target.files[0].name;
    //     $('.custom-file-label').html(fileName);
    // });
    function addReferences() {
        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">' +
                '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">' +
                '<label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="email" style="font-weight:bold;">References -</label>' +
                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="margin-bottom:10px;">' +
                '<input id="referencesfile" name="references[]" type="file"  class="form-control referencesfile" data-show-upload="false" data-show-preview="false" data-bv-file-maxsize="300*100" />' +
                '</div>' +
                '<div class="col-lg-2 col-sm-2 col-md-2 col-xs-6">' +
                '<i class="fa fa-2x fa-plus-circle" onclick="addReferences()"></i>' +
                ' </div>' +
                '</div>' +
                '</div>';
        $('#references').append(data);
    }
//    function addReferences() {
//        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">' +
//                '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">' +
//                '<label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="email" style="font-weight:bold;">References -</label>' +
//                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">' +
//                ' <input type="name" class="form-control" id="email" placeholder="0000000000" style="margin-bottom:10px;">' +
//                ' </div>' +
//                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="margin-bottom:10px;">' +
//                '<label class="btn btn-default btn-file" style="margin:0;">' +
//                '  Browse <input type="file" name="references[]" style="display: none;">' +
//                '  </label>' +
//                '</div>' +
//                '<div class="col-lg-2 col-sm-2 col-md-2 col-xs-6">' +
//                '<i class="fa fa-2x fa-plus-circle" onclick="addReferences()"></i>' +
//                ' </div>' +
//                '</div>' +
//                '</div>';
//        $('#references').append(data);
//    }

    $('#location').change(function (event) {
        var store = $('#location').val();
        $.ajax({
            type: "get", async: false,
            url: "<?= site_url('billers/getDepartment') ?>",
            data: {'store': store},
            dataType: "json",
            success: function (data) {
                $('#department').html('');
                if (data == '') {
//                    bootbox.alert('<?= lang('department_is_not_available') ?>');
                } else {
                    $.each(data, function (k, val) {
                        var opt = $('<option />');
                        opt.val(val.id);
                        opt.text(val.name);
                        $('#department').append(opt);
                    })
                }
            }
        });
    });


    $('#department').change(function (event) {
        var section = $(this).val();
        $.ajax({
            type: "get", async: false,
            url: "<?= site_url('billers/getsection') ?>",
            data: {'section': section},
            dataType: "json",
            success: function (data) {
                $('#section').html('');
                if (data == '') {
//                    bootbox.alert('<?= lang('department_is_not_available') ?>');
                } else {
                    $.each(data, function (k, val) {
                        var opt = $('<option />');
                        opt.val(val.id);
                        opt.text(val.name);
                        $('#section').append(opt);
                    })
                }
            }
        });
    });

    $('#email').blur(function (event) {
        var email = $(this).val();
        $.ajax({
            type: "get",
            async: false,
            url: "<?= site_url('employee/checkuserEmail') ?>",
            data: {'email': email},
            dataType: "json",
            success: function (data) {
                if (data == '1') {
                    $('#email').val('');
                    bootbox.alert('Email Already Exists. Try Another One');
                }
            }
        });
    });

</script>

