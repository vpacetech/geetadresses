<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-users"></i><?= lang('add_transport'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <!--<p class="introtext"><?php // echo lang('add_transport');        ?></p>-->

                <?php
                $attrib = array('class' => 'form-horizontal', 'data-toggle' => 'validator', 'role' => 'form');
                echo form_open("transport/Add", $attrib);
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-5">
                            <div class="form-group">

                                <?php echo lang('t_code', 't_code'); ?> <button style="float: right;padding:3px" type="button" onclick="enableEdits()"><?= lang('define_my_own_Code') ?></button>
                                <div class="controls">
                                    <?php
                                    $latest_transport = "Trans" . ' ' . $latest_transport;
                                    ?>
                                    <input type="text" id="transport_code" value="<?= set_value('code', $latest_transport); ?>" name="code" required readonly class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('trans_name', 'trans_name'); ?>
                                <div class="controls">
                                    <input type="text" id="trans_name" value="<?php echo set_value('username', '') ?>" name="trans_name" class="form-control"
                                           required="required"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('Manager_Name', 'manager_name'); ?>
                                <div class="controls">
                                    <?php
                                    echo form_input('first_name', set_value('first_name', ''), 'class="form-control" id="first_name" required="required"'
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="Please write your first name"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[a-z\s,A-z]+$"
                               data-bv-regexp-message="Name must contain characters only"');
                                    ?>
                                </div>
                            </div>

                            <!-- <div class="form-group">
                                <?php echo lang('last_name', 'last_name'); ?>
                                <div class="controls">
                                    <?php
                                    echo form_input('last_name', set_value('last_name', ''), 'class="form-control" id="last_name" required="required" pattern="^[a-z\s,A-z]+$"'
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="Please write your last name"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[a-z\s,A-z]+$"
                               data-bv-regexp-message="Name must contain characters only"');
                                    ?>

                                </div>
                            </div>
                            <div class="form-group">
                                <?= lang('gender', 'genders'); ?>
                                <?php
                                $ge = array('male' => lang('male'), 'female' => lang('female'));
                                echo form_dropdown('gender', $ge, (isset($_POST['gender']) ? $_POST['gender'] : ''), 'class="tip form-control" id="gender" data-placeholder="' . lang("select") . ' ' . lang("gender") . '" required="required"');
                                ?>
                            </div>-->
                            <!--                            <div class="form-group">
                            <?php echo lang('company', 'company'); ?>
                                                            <div class="controls">
                            <?php echo form_input('company', '', 'class="form-control" id="company" required="required"'); ?>
                                                            </div>
                                                        </div>-->
                            <div class="form-group">
                                <?php echo lang('Office no', 'phone'); ?>
                                <div class="controls">
                                    <?php
                                    echo form_input('phone', set_value('phone', ''), 'class="form-control" id="phone" required="required"'
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="The Mobile no is required and cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="10"
                               data-bv-stringlength-max="10"
                               data-bv-stringlength-message="The Mobile no must be exact 10 numbers long"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The Mobile no can only consist of digits"');
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('email', 'email'); ?>
                                <div class="controls">
                                    <input type="email" value="<?php echo set_value('email', '') ?>" id="email" name="email" class="form-control"/>
                                    <span id="error"></span>
                                </div>
                            </div>
                            
                            <!--                            <div class="form-group">
                            <?php echo lang('password', 'password'); ?>
                                                            <div class="controls">
                            <?php echo form_password('password', set_value('password', ''), 'class="form-control tip" id="password" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"'); ?>
                                                                <span class="help-block"><?= lang('pasword_hint') ?></span>
                                                            </div>
                                                        </div>
                            
                                                        <div class="form-group">
                            <?php echo lang('confirm_password', 'confirm_password'); ?>
                                                            <div class="controls">
                            <?php echo form_password('confirm_password', set_value('confirm_password', ''), 'class="form-control" id="confirm_password" required="required" data-bv-identical="true" data-bv-identical-field="password" data-bv-identical-message="' . lang('pw_not_came') . '"'); ?>
                                                            </div>
                                                        </div>-->
                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            <!-- <div class="form-group">
                                <?= lang('status', 'statuss'); ?>
                                <?php
                                $opt = array('' => '', 'Active' => lang('active'), 'Deactive' => lang('inactive'));
                                echo form_dropdown('status', $opt, (isset($_POST['status']) ? $_POST['status'] : 'Active'), 'id="status" data-placeholder="' . lang("select") . ' ' . lang("status") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div> -->

                            <div class="form-group">
                                <?php echo lang('address', 'address'); ?>
                                <div class="controls">
                                    <input type="text" value="<?php echo set_value('address', '') ?>" id="address" name="address" class="form-control" required="required"/>
                                    <span id="error"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('city', 'city'); ?>
                                <div class="controls">
                                    <?php
                                    echo form_input('city', set_value('city', ''), 'class="form-control" id="city" required="required"'
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="Please write your city name"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[a-z\s,A-z]+$"
                               data-bv-regexp-message="city must contain characters only"');
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('state', 'state'); ?>
                                <div class="controls">
                                    <?php
                                    echo form_input('state', set_value('state', ''), 'class="form-control" id="state" required="required"'
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="Please write your state name"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[a-z\s,A-z]+$"
                               data-bv-regexp-message="state must contain characters only"');
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('postal_code', 'postal_code'); ?>
                                <div class="controls">
                                    <?php
                                    echo form_input('postal_code', set_value('postal_code', ''), 'class="form-control" id="postal_code" required="required"'
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="Postal code is required and cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="6"
                               data-bv-stringlength-max="6"
                               data-bv-stringlength-message="The Postal code must be exact 6 numbers long"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The Postal code can only consist of digits"');
                                    ?>
                                </div>
                            </div>  
                            <div class="form-group">
                                <?php echo lang('country', 'country'); ?>
                                <div class="controls">
                                    <?php
                                    echo form_input('country', set_value('country', ''), 'class="form-control" id="country" required="required"'
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="Please write your country name"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[a-z\s,A-z]+$"
                               data-bv-regexp-message="country name must contain characters only"');
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('gst_no', 'gst_no'); ?>
                                <div class="controls">
                                    <input type="text" value="<?= set_value('gst_no', ''); ?>" name="gst_no" class="form-control"/>
                                </div>
                            </div>

                            <!-- <div class="form-group"> 
                                <div class=" col-sm-1 col-xs-1 col-lg-1 col-md-1" style="padding:0 !important"><label><?= lang('igst') ?></label></div>
                                <div class="col-sm-8 col-xs-8 col-lg-8 col-md-8" style="padding-right: 5px">
                                    <div class="checkbox">
                                        <input type="checkbox" name="igst" value="1" id="igst">
                                    </div>
                                </div>
                            </div> -->


                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>  
                <!--<div class="row">

                    <div class="col-md-12">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Contact Details</legend>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <?php echo lang('mobile', 'mobile'); ?>
                                    <?php
                                    echo form_input('mobile', set_value('mobile', ''), 'class="form-control" id="mobile" required="required"'
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="The Mobile no is required and cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="10"
                               data-bv-stringlength-max="10"
                               data-bv-stringlength-message="The Mobile no must be exact 10 numbers long"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The Mobile no can only consist of digits"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-3 col-md-offset-1">
                                <div class="form-group">
                                    <?php echo lang('landline', 'landline'); ?>
                                    <div class="controls">
                                        <?php
                                        echo form_input('landline', set_value('landline', ''), 'class="form-control" id="landline_no"'
                                                . 'data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The landline no can only consist of digits"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-md-offset-1">
                                <div class="form-group">
                                    <?php echo lang('fax', 'fax'); ?>
                                    <div class="controls">
                                        <?php
                                        echo form_input('fax', set_value('fax', ''), 'class="form-control" id="fax_no"'
                                                . 'data-bv-stringlength-message="The fax no must be exact 10 numbers long"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The fax no can only consist of digits"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                    </div>
                    </fieldset>
                </div>-->
                <p><?php echo form_submit('add_transport', "Save", 'class="btn btn-primary"'); ?></p>

                <?php echo form_close(); ?>
            </div>



        </div>
    </div>
</div>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#group').change(function (event) {
            var group = $(this).val();
            if (group == 1 || group == 2) {
                $('.no').slideUp();
                $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'biller');
                $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'warehouse');
            } else {
                $('.no').slideDown();
                $('form[data-toggle="validator"]').bootstrapValidator('addField', 'biller');
                $('form[data-toggle="validator"]').bootstrapValidator('addField', 'warehouse');
            }
        });

        $('#email').change(function (event) {
            var emails = $('#email').val();
            $.ajax({
                type: "get", async: false,
                url: "<?= site_url('auth/check_email') ?>/",
                data: {'email': emails},
                dataType: "json",
                success: function (data) {
                    if (data == 1) {
                        $('#email').addClass('has-error');
                        $('#error').addClass('label label-danger');
                        $('#error').html('Email Already Exists');
                        setTimeout(function () {
                            $('#error').html('');
                        }, 4000);
                        $('#email').val('');
                    } else {
                    }
                }
            });
        });
        $('#username').change(function (event) {
            var username = $('#username').val();
            $.ajax({
                type: "get", async: false,
                url: "<?= site_url('auth/check_username') ?>/",
                data: {'username': username},
                dataType: "json",
                success: function (data) {
                    if (data == 1) {
                        $('#username').addClass('has-error');
                        $('#error_uname').addClass('label label-danger');
                        $('#error_uname').html('User Name Already Exists');
                        setTimeout(function () {
                            $('#error_uname').html('');
                        }, 4000);
                        $('#username').val('');
                    } else {
                    }
                }
            });
        });
    });
    function enableEdits() {
        $("#transport_code").removeAttr('readonly');
    }
</script>
<script>
        var autocomplete;

var componentForm = {
  locality: 'long_name', // city
  administrative_area_level_1: 'long_name', //state
  administrative_area_level_2: 'long_name', //state
  country: 'long_name', // country
  postal_code: 'short_name' //pincode
};
var options = {
  types: ['(cities)'],
    // types: ['geocode'],
    componentRestrictions: {country: "in"}
 };

initAutocomplete();

    function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
        document.getElementById('city'), options );
        // autocomplete.setFields(['address_component']);
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        var place = autocomplete.getPlace();
        console.log(place);
        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng();
        $("#auto_address_latitude").val(latitude);
        $("#auto_address_longitude").val(longitude);
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            var val = place.address_components[i][componentForm[addressType]];
            if (addressType == "administrative_area_level_2") {
                // console.log(addressType+" " +val);
                $("#city").val(val);

            }
            else if (addressType == "administrative_area_level_1") {
                // console.log(addressType+" " +val);
                $("#state").val(val);

            }
            else if (addressType == "postal_code") {
                // console.log(addressType+" " +val);
                $("#postal_code").val(val);

            }
            else if (addressType == "country") {
                // console.log(addressType+" " +val);
                $("#country").val(val);

            }
        }
    }

    </script>