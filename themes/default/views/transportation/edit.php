<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-users"></i><?= lang('edit_transport'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <!--<p class="introtext"><?php // echo lang('edit_transport');    ?></p>-->

                <?php
                $attrib = array('class' => 'form-horizontal', 'data-toggle' => 'validator', 'role' => 'form');
                echo form_open("transport/update_transport/" . $ed_tr->id, $attrib);
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-5">
                            <div class="form-group">
                                <?php echo lang('t_code', 't_code'); ?> <button style="float: right;padding:3px" type="button" onclick="enableEdits()"><?= lang('define_my_own_Code') ?></button>
                                <div class="controls">
                                    <input type="text" id="transport_code" value="<?= set_value('code', $ed_tr->code); ?>" name="code" readonly required class="form-control"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('trans_name', 'trans_name'); ?>
                                <div class="controls">
                                    <input type="text" id="trans_name" value="<?php echo $ed_tr->transport_name ?>" name="trans_name" class="form-control"
                                           required="required"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('manager_name', 'manager_name'); ?>
                                <div class="controls">
                                    <?php // echo form_input('sender_address', $min->sender_address, 'class="form-control tip" id="sender_address" data-bv-notempty="true"'); ?>

                                    <?php
                                    echo form_input('first_name', $ed_tr->first_name, 'class="form-control" id="first_name" required="required"'
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="Please write your first name"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[a-z\s,A-z]+$"
                               data-bv-regexp-message="Name must contain characters only"');
                                    ?>
                                </div>
                            </div>

                            <!-- <div class="form-group">
                                <?php echo lang('last_name', 'last_name'); ?>
                                <div class="controls">
                                    <?php
                                    echo form_input('last_name', $ed_tr->last_name, 'class="form-control" id="last_name" required="required" pattern="^[a-z\s,A-z]+$"'
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="Please write your last name"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[a-z\s,A-z]+$"
                               data-bv-regexp-message="Name must contain characters only"');
                                    ?>

                                </div>
                            </div>
                            <div class="form-group">
                                <?= lang('gender', 'genders'); ?>
                                <?php
                                $ge = array('male' => lang('male'), 'female' => lang('female'));
                                echo form_dropdown('gender', $ge, (isset($_POST['gender']) ? $_POST['gender'] : $ed_tr->gender), 'class="tip form-control" id="gender" data-placeholder="' . lang("select") . ' ' . lang("gender") . '" required="required"');
                                ?>
                            </div> -->

                            <!--                            <div class="form-group">
                            <?php echo lang('company', 'company'); ?>
                                                            <div class="controls">
                            <?php echo form_input('company', '', 'class="form-control" id="company" required="required"'); ?>
                                                            </div>
                                                        </div>-->

                            <div class="form-group">
                                <?php echo lang('office no', 'phone'); ?>
                                <div class="controls">
                                    <?php
                                    echo form_input('phone', $ed_tr->phone, 'class="form-control" id="phone" required="required"'
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="The Mobile no is required and cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="10"
                               data-bv-stringlength-max="10"
                               data-bv-stringlength-message="The Mobile no must be exact 10 numbers long"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The Mobile no can only consist of digits"');
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('email', 'email'); ?>
                                <div class="controls">
                                    <input type="email" value="<?php echo $ed_tr->email ?>" id="email" name="email" class="form-control"/>
                                    <span id="error"></span>

                                </div>
                            </div>
                            
                            <!--                            <div class="form-group">
                            <?php echo lang('password', 'password'); ?>
                                                            <div class="controls">
                            <?php echo form_password('password', set_value('password', ''), 'class="form-control tip" id="password" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"'); ?>
                                                                <span class="help-block"><?= lang('pasword_hint') ?></span>
                                                            </div>
                                                        </div>
                            
                                                        <div class="form-group">
                            <?php echo lang('confirm_password', 'confirm_password'); ?>
                                                            <div class="controls">
                            <?php echo form_password('confirm_password', set_value('confirm_password', ''), 'class="form-control" id="confirm_password" required="required" data-bv-identical="true" data-bv-identical-field="password" data-bv-identical-message="' . lang('pw_not_came') . '"'); ?>
                                                            </div>
                                                        </div>-->

                        </div>
                        <div class="col-md-5 col-md-offset-1">
                            <div class="form-group">
                                <?= lang('status', 'statuss'); ?>
                                <?php
                                $opt = array('' => '', 'Active' => lang('active'), 'Deactive' => lang('deactive'));
                                echo form_dropdown('status', $opt, (isset($_POST['status']) ? $_POST['status'] : $ed_tr->status), 'id="status" data-placeholder="' . lang("select") . ' ' . lang("status") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>

                            <div class="form-group">
                                <?php echo lang('address', 'address'); ?>
                                <div class="controls">
                                    <input type="text" value="<?php echo$ed_tr->address ?>" id="address" name="address" class="form-control" required="required"/>
                                    <span id="error"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('city', 'city'); ?>
                                <div class="controls">
                                    <?php
                                    echo form_input('city', $ed_tr->city, 'class="form-control" id="city" required="required"'
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="Please write your city name"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[a-z\s,A-z]+$"
                               data-bv-regexp-message="city must contain characters only"');
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('state', 'state'); ?>
                                <div class="controls">
                                    <?php
                                    echo form_input('state', $ed_tr->state, 'class="form-control" id="state" required="required"'
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="Please write your state name"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[a-z\s,A-z]+$"
                               data-bv-regexp-message="state must contain characters only"');
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('postal_code', 'postal_code'); ?>
                                <div class="controls">
                                    <?php
                                    echo form_input('postal_code', $ed_tr->postal_code, 'class="form-control" id="postal_code" required="required"'
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="Postal code is required and cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="6"
                               data-bv-stringlength-max="6"
                               data-bv-stringlength-message="The Postal code must be exact 6 numbers long"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The Postal code can only consist of digits"');
                                    ?>
                                </div>
                            </div>  
                            <div class="form-group">
                                <?php echo lang('country', 'country'); ?>
                                <div class="controls">
                                    <?php
                                    echo form_input('country', $ed_tr->country, 'class="form-control" id="country" required="required"'
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="Please write your country name"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[a-z\s,A-z]+$"
                               data-bv-regexp-message="country name must contain characters only"');
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('gst_no', 'gst_no'); ?>
                                <div class="controls">
                                    <input type="text" value="<?= set_value('gst_no', $ed_tr->gst_no); ?>" name="gst_no" class="form-control"/>
                                </div>
                            </div>

                            <!-- <div class="form-group"> 
                                <div class=" col-sm-1 col-xs-1 col-lg-1 col-md-1" style="padding:0 !important"><label><?= lang('igst') ?></label></div>
                                <div class="col-sm-8 col-xs-8 col-lg-8 col-md-8" style="padding-right: 5px">
                                    <div class="checkbox">
                                      <input type="checkbox" name="igst" value="1" id="igst" <?php if ($ed_tr->igst == '1') {
                                    echo "checked";
                                } ?>>
                                    </div>
                                </div>
                            </div> -->

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>  
                <!-- <div class="row">

                    <div class="col-md-12">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Contact Details</legend>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <?php echo lang('mobile', 'mobile'); ?>
                                    <?php
                                    echo form_input('mobile', $ed_tr->mobile, 'class="form-control" id="mobile" required="required"'
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="The Mobile no is required and cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="10"
                               data-bv-stringlength-max="10"
                               data-bv-stringlength-message="The Mobile no must be exact 10 numbers long"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The Mobile no can only consist of digits"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-3 col-md-offset-1">
                                <div class="form-group">
                                    <?php echo lang('landline', 'landline'); ?>
                                    <div class="controls">
                                        <?php
                                        echo form_input('landline', $ed_tr->landline, 'class="form-control" id="landline_no"'
                                                . 'data-bv-notempty-message="The landline no is required and cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The landline no can only consist of digits"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-md-offset-1">
                                <div class="form-group">
                                    <?php echo lang('fax', 'fax'); ?>
                                    <div class="controls">
                                        <?php
                                        echo form_input('fax', $ed_tr->fax, 'class="form-control" id="fax_no"'
                                                . 'data-bv-stringlength="true"
                                                           data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The fax no can only consist of digits"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                    </div>
                    </fieldset>
                </div> -->
                <p><?php echo form_submit('add_transport', "Save", 'class="btn btn-primary"'); ?></p>

                <?php echo form_close(); ?>
            </div>



        </div>
    </div>
</div>
</div>
<script>
    function enableEdits() {
        $("#transport_code").removeAttr('readonly');
    }
</script>