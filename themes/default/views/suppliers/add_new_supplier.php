<style> 

.tab-content .tab-pane {
  display: none !important;
}

.tab-content  .active {
  display: block !important;
}

  #code
  {
    position: absolute;
    top: 18.8%;
    left: 65%;
  } 

  #check
  {
    position: absolute;
    top: 30%;
    left: 33%;
  }

  #Choose
  {
    position: absolute;
    top: 75%;
    left: 26.5%;
    width: 38%;
  }

  #check1
  {
    position: absolute;
    top: 63.5%;
    left: 26.5%;
  }

    #prev
    {
        position: absolute;
        top: 85%;
        left: 5%;
    }
    #next
    {
        position: absolute;
        top: 85%;
        left: 85%;
    }  
    fieldset.scheduler-border {
        border: 1px solid #ddd !important;
        padding: 0 1em 1em 1em !important;
        margin: 0 0 1.1em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
        box-shadow:  0px 0px 0px 0px #000;
    }


    legend.scheduler-border {
        width:inherit; /* Or auto */
        padding:0 10px; /* To give a bit of padding on the left and right */
        border-bottom:none;

    }
    .cal 
    {
        position:relative;
        top:-30px;
        left:90%;
    }
    /*
    a {
        color: #000 !important;    
            }*/
    .nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
        color: #6164c0 !important;
        background-color: #fff !important;
        border-radius:0 !important;
    }

    .nav-pills>li>a{
        color: #6164c0 !important;
        background-color: #e9ebec !important;
        border-radius:0 !important;
    }
    .tab-content>.active {
        display: block;
        border : 1px solid #ccc;
        padding:5px !important;
    }
    .btton
    {
        padding: 5px;
        width:100%;
    }

    .stepwizard-step {
        display: table-cell;
        /*text-align: center;*/
        position: relative;
    }
    .form-horizontal .control-label {
        padding-top: 0px;
  
    }
</style> 

<ul class="nav nav-pills mb-3 red" id="pills-tab" role="tablist">
      <li class="nav-item active">
      <a class="nav-link active in" id="pills-supplier_info-tab" data-toggle="pill" href="#pills-supplier_info" role="tab" aria-controls="pills-supplier_info" aria-selected="false">supplier_info
  </a>
    </li>
      <li class="nav-item disabled">
      <a class="nav-link disabled" id="pills-General-tab" data-toggle="pill" href="#"  role="tab" aria-controls="pills-General" aria-selected="false">General
  </a> 
    </li>
    <li class="nav-item disabled">
      <a class="nav-link" id="pills-Address-tab" data-toggle="pill" href="#" role="tab" aria-controls="pills-Address" aria-selected="false">Address</a>
    </li>
<!--     <li class="nav-item disabled">
      <a class="nav-link" id="pills-Bank-tab" data-toggle="pill" href="#" role="tab" aria-controls="pills-Bank" aria-selected="true">Bank Details</a>
    </li> -->
    <li class="nav-item disabled">
      <a class="nav-link" id="pills-Payment_Terms-tab" data-toggle="pill" href="#" role="tab" aria-controls="pills-Payment_Terms" aria-selected="false">Payment Terms</a>
    </li>
    </li>
      <li class="nav-item disabled">
      <a class="nav-link" id="pills-Contact-tab" data-toggle="pill" href="#" role="tab" aria-controls="pills-Contact" aria-selected="false">contact info
  </a>
    </li>

      <li class="nav-item disabled">
      <a class="nav-link" id="pills-Refered_By-tab" data-toggle="pill" href="#" role="tab" aria-controls="pills-Refered_By" aria-selected="false">reffered by
  </a>
    </li>

      <li class="nav-item disabled">
      <a class="nav-link" id="pills-Photo-tab" data-toggle="pill" href="#" role="tab" aria-controls="pills-Photo" aria-selected="false">Photo
  </a>
    </li>
</ul>

<div id="exTab1" class="container-fluid" style="background-color: #ffff;padding: 10px" ng-controller="employee">	

<div class="tab-content" id="pills-tabContent" style="padding: 50px;padding-left: 20px">



    <div class="tab-pane fade show active in" style="padding-left: 20px" id="pills-supplier_info" role="tabpanel" aria-labelledby="pills-supplier_info-tab">

      <form id="supplierinfo_form" class="form-horizontal" action="<?php echo site_url().'suppliers/savenewsupplier'?>" enctype="multipart/form-data" method="post">
      

            <br>
            <div class="form-group">
                <label class="col-sm-3 control-label" for="Supplier_Code">Supplier Code</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" readonly id="Supplier_Code" name="Supplier_Code" data-bv-notempty="true" />
                </div>
            </div>          


            <br>
            <br>
            <!-- <br>
            <br> -->
            <button type="button" id="code" class="btn btn-primary" onclick="edit()">Define my own code</button>

            <div class="form-group" id="check">
                <input id="Preferred_Suppliers" data-type='res' name='Preferred_Supplier' type='checkbox'>&emsp;
                <label for="Preferred_Suppliers">Preferred Suppliers&emsp;</label>&emsp;
                <input id="Is_Active" data-type='com' name='Is_Active' type='checkbox'>&emsp;
                <label for="Is_Active">Is Active&emsp;</label>
            </div>          

         <br>
         <br>
         <br>
         <br>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="Select_Store">Select Store</label>
                <div class="col-sm-5">
                  <select class="form-control tip" id="Select_Store" name="store">
                  <option selected option disabled>select store</option>
                      <?php 
                      foreach ($store as $biller) {?>
                          
                          <option value="<?php echo $biller->id?>"><?php echo $biller->name ?></option>        
                      <?php        
                      }?>
            
                </select>             
             </div>
            </div>          


         <br>   


            <div class="form-group">
                <label class="col-sm-3 control-label" for="Supplier_Name">Supplier Name</label>
                <div class="col-sm-5">
                    <input type="text"  class="form-control"  id="Supplier_Name" name="Supplier_Name" data-bv-notempty="true" />
                </div>
            </div>          

         <br>        

            <div class="form-group">
                <label class="col-sm-3 control-label" for="Supplier_Discount1">Supplier Discount(%)</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control"  id="Supplier_Discount" name="Supplier_Discount"  />
                </div>
            </div>          


         <br>

            <div class="form-group">

                <label class="col-sm-3 control-label" for="aadhar_card">aadhar card</label>
                     <div class="col-sm-5">
                        <input id="attachment1" name="aadhar" type="file" class="form-control" 
                            data-show-upload="false"
                            data-show-preview="false" 
                            
                        />
                     </div>
            </div>

            <br>

            <div class="form-group">

                <label class="col-sm-3 control-label" for="Pan">Pan card</label>
                    <div class="col-sm-5">
                        <input id="attachment2" name="pan"  type="file" class="form-control" 
                        data-show-upload="false"
                        data-show-preview="false" 
                        
                        />
                    </div>
            </div>

         <br>
                      <!-- <button type="button" id="id" class="id1 id2">hello</button> -->
<!--           <div align="center">
            <button type="submit" class="btn btn-primary but">save</button>
            <button type="button" class="btn btn-danger">cancel</button>
          </div> -->
          <div class="form-group">
                <!-- <button id="prev" class="btn btn-info btn-lg previous">Previous</button> -->
                <button id="next" class="btn btn-info btn-lg next">Next</button>
          </div>
      </form>
    </div>    
    <div class="tab-pane fade" style="padding-left: 20px" id="pills-General" role="tabpanel" aria-labelledby="pills-General-tab">

    
      <!-- <form id="General_form" class="form-horizontal" action="<?php echo site_url().'suppliers/savenewsupplier_general'?>" method="post" > -->


            <div class="form-group">
                <label class="col-sm-3 control-label" for="gstno">GST No</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="gstno" name="gstno" />
                </div>
            </div>
            <br>
            <div class="form-group">
                <label class="col-sm-3 control-label">PAN</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="panno"/>
                </div>
            </div>
            <br>
            <div class="form-group">
              <label class="col-sm-3 control-label" for="IGST">IGST</label>
              <div class="col-sm-5" id="check1">
                <input type="checkbox" class="form-control" id="IGST" name="IGST"> 
              </div>
            </div>
            <br>
            <div align="center">
              <button type="submit" class="btn btn-primary">save</button>
              <button type="button" class="btn btn-danger">cancel</button>
            </div>
            <div class="form-group">
                <button id="prev" class="btn btn-info btn-lg previous">Previous</button>
                <button id="next" class="btn btn-info btn-lg next">Next</button>
          </div>
        </form>
    </div>

    <div class="tab-pane fade" id="pills-Address" style="padding-left: 20px" role="tabpanel" aria-labelledby="pills-Address-tab">

      <!-- <form id="Address_form" class="form-horizontal" action="<?php echo site_url().'suppliers/savenewsupplier_address'?>" method="post"> -->


            <div class="form-group">
                <label class="col-sm-3 control-label" for="Address1">Address Line1</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="Address1" name="Address1" data-bv-notempty="true"/>
                </div>
            </div>

          <br>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="Address2">Address Line 2</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" name="Address2"/>
                </div>
            </div>

          <br>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="city">City</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control tip" id="city" name="city" data-bv-notempty="true"/>
                </div>
            </div>

          <br>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="state">State</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control tip" id="state" name="state" data-bv-notempty="true"/>
                </div>
            </div>

          <br>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="country">Country</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control tip" id="country" name="country" data-bv-notempty="true" />
                </div>
            </div>

          <br>

            <div class="form-group">
                <label class="col-sm-3 control-label" for="Zip_Code">Zip Code</label>
                <div class="col-sm-5">
                    <input type="text" class="form-control" id="Zip_Code" name="Zip_Code" data-bv-notempty="true" />
                </div>
            </div>

          <br>  

<!--           <div align="center">
            <button type="submit" class="btn btn-primary">save</button>
            <button type="button" class="btn btn-danger">cancel</button>
          </div> -->
          <div class="form-group">
                <button id="prev" class="btn btn-info btn-lg previous">Previous</button>
                <button id="next" class="btn btn-info btn-lg next">Next</button>
          </div>
      </form>
    </div>


<!--     <div class="tab-pane fade" id="pills-Bank"  style="padding-left: 20px" role="tabpanel" aria-labelledby="pills-Bank-tab">



        <form id="Bank_form" class="form-horizontal" action="<?php echo site_url().'suppliers/savenewsupplier_bank'?>" method="post">

            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Supplier Banks</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Add Bank</a>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
            
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                      
                    <table id="bankstable" class="table">
                            <thead>
                                <tr>
                                    <th>Sr.no</th>
                                    <th>Bank Name</th>
                                    <th>Branch</th>
                                    <th>IFSC</th>
                                    <th>Acc. No.</th>
                                    <th>Is Primary Acc.</th>
                                </tr>
                            </thead>

                       <tbody>
                            <?php 
                            $i = 1;
                            foreach($banks as $bank): ?>
                                <tr>
                                    <td><?= $i++; ?></td>
                                    <td><?= $bank->name; ?></td>
                                    <td><?= $bank->branch; ?></td>
                                    <td><?= $bank->ifsc; ?></td>
                                    <td><?= $bank->account_number; ?></td>
                                    <td><?= $bank->isprimary; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>

                </div>
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                
                        <div class="form-group">
                        <label class="col-sm-3 control-label" for="Bank_Name">Bank Name</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="bank_Name" name="Bank_Name"  />
                        </div>
                    </div>

                    <br>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="Account_Number">Account Number</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="account_Number" name="Account_Number"  />
                        </div>
                    </div>

                    <br>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="Opening_Balance">Opening Balance</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="opening_Balance" name="Opening_Balance"  />
                        </div>
                    </div>

                    <br>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="IFSC">IFSC</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="ifsc" name="IFSC"  />
                        </div>
                    </div>    

                    <br>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="Bank_Branch">Bank Branch</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="bank_Branch" name="Bank_Branch"  />
                        </div>
                    </div>    
                    <br>

              <div align="center">
                <button type="submit" class="btn btn-primary">save</button>
                <button type="button" class="btn btn-danger">cancel</button>
                <button type="button" class="btn btn-primary">Add</button>
             </div>
                
                </div>
            </div>


          <br>


          <div class="form-group">
                <button id="prev" class="btn btn-info btn-lg previous">Previous</button>
                <button id="next" class="btn btn-info btn-lg next">Next</button>
          </div>

        </form>
    </div> -->


    <div class="tab-pane fade" id="pills-Contact" style="padding-left: 20px" role="tabpanel" aria-labelledby="pills-Contact-tab">

        <!-- <form id="Contact_form" class="form-horizontal" action="<?php echo site_url().'suppliers/savenewsupplier_contact'?>" method="post"> -->

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="first_Name">first Name</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="first_Name" name="first_Name" />
                    </div>
                </div>    

            <br>

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="last_Name">last Name</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="last_Name" name="last_Name" />
                    </div>
                </div> 
                
            <br>   
                <div class="form-group">
                    <label class="col-sm-3 control-label" for="telephone">telephone No</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="telephone" name="telephone_no" 
                        
                        
                        />
                    </div>
                </div> 
            
            <br> 

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="fax_no">fax No</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="fax_no" name="fax_no" />
                    </div>
                </div> 

            <br>  

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="mobile_No">mobile No</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="mobile_No" name="mobile_No" data-bv-notempty="true" />
                    </div>
                </div>          

            <br> 

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="website">website</label>
                    <div class="col-sm-5">
                        <input type="url" class="form-control" id="website" name="website" />
                    </div>
                </div> 

            <br>   

                <div class="form-group">
                    <label class="col-sm-3 control-label" for="mail">E-mail</label>
                    <div class="col-sm-5">
                        <input type="email" class="form-control" id="mail" name="mail"  />
                    </div>
                </div>          

            <br>                

<!--                 <div align="center">
                <button type="submit" class="btn btn-primary">save</button>
                <button type="button" class="btn btn-danger">cancel</button>
                </div>   -->
        <div class="form-group">
            <button id="prev" class="btn btn-info btn-lg previous">Previous</button>
            <button id="next" class="btn btn-info btn-lg next">Next</button>
        </div>
      </form>
    </div>




    <div class="tab-pane fade" style="padding-left: 20px" id="pills-Refered_By" role="tabpanel" aria-labelledby="pills-Refered_By-tab">
        <br>
        <!-- <form id="refered_by" class="form-horizontal" action="<?php echo site_url().'suppliers/savenewsupplier_referedby'?>" method="post"> -->
           
            <div class="form-group">
                <label class="col-sm-3 control-label" for="Select_reffered">Select Refered By</label>
                <div class="col-sm-5">
                    <select class="form-control" name="refered">
                        <option selected option disabled>select</option>
                            <?php 
                        foreach ($suppliers as $biller) {?>
                            
                            <option value="<?php echo $biller->id ?>"><?php echo $biller->company ?></option>        
                        <?php        
                        }?>
                    
                    </select>
                </div>
            </div>  

            <div class="form-group">
                
                  <label class="col-sm-3 control-label" for="percentage_commision">percentage Commision</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control"  name="percentage_commision">   
                  </div>
            </div>

            <br>  
            <br>  

<!--                 <div align="center">
                <button type="submit" class="btn btn-primary">save</button>
                <button type="button" class="btn btn-danger">cancel</button>
                </div>        -->       
            <div class="form-group">
                <button id="prev" class="btn btn-info btn-lg previous">Previous</button>
                <button id="next" class="btn btn-info btn-lg next">Next</button>
            </div>
        </form> 
    </div>


    <div class="tab-pane fade" style="padding-left: 20px" id="pills-Payment_Terms" role="tabpanel" aria-labelledby="pills-Payment_Terms-tab">


        <!-- <form id="PaymentTerms" class="form-horizontal" action="<?php echo site_url().'suppliers/savenewsupplier_payment'?>" method="post"> -->

            <div class="form-group">
                
                  <label for="payment" class="col-sm-3 control-label">Payment needs to be settled within</label>
            
                <div class="col-sm-5">
                  <input type="text" id="payment" data-bv-notempty="true" class="form-control" name="payment"/> <!-- <span><b>sdays</b></span> -->
                </div>
<!--                 <div class="col-sm-6">
                  <label>days</label>
                </div> -->
                
            </div>        

            <br> 

            <br>  
            <br>

<!--                 <div align="center">
                    <button type="submit" class="btn btn-primary">save</button>
                    <button type="button" class="btn btn-danger">cancel</button>
                </div>  -->
          <div class="form-group">
                <button id="prev" class="btn btn-info btn-lg previous">Previous</button>
                <button id="next" class="btn btn-info btn-lg next">Next</button>
          </div>
        </form>     
    </div>

    <div class="tab-pane fade" style="padding-left: 20px" id="pills-Photo" role="tabpanel" aria-labelledby="pills-Photo-tab">

         <!-- <form id="savenewsupplier_photo" class="form-horizontal" enctype="multipart/form-data" action="<?php echo site_url().'suppliers/savenewsupplier_photo'?>" method="post"> -->

            <div class="form-group">

                <label class="col-sm-3 control-label" for="photo">photo</label>
                <div class="col-sm-5">
                <input id="photo" name="photo" id="photo" type="file" data-bv-notempty="true" class="form-control" 
                    data-show-upload="false"
                    data-show-preview="false" 
                    data-bv-file-maxsize="300*100" 
                    />
                </div>
            </div>
            
            <button align="center" type="submit" id="finish" class="btn btn-primary">save</button>

<!--                     <div>
                    <img id="blah" src="#" alt="your image" />       

                    </div>

 -->
                 <br>


<!--             <div align="center">
              <button type="submit" class="btn btn-primary">save<button>
              <button type="button" class="btn btn-danger">cancel</button>
            </div>            -->        
            <div class="form-group">
                <button id="prev" class="btn btn-info btn-lg previous">Previous</button>
                <!-- <button id="next" class="btn btn-info btn-lg next">Next</button> -->
          </div>
        </form>


    </div>

  </div>

  


 <script>

/*     var i = "1";

    if( i == "" )
    {
        alert("g");
    }
 */
     var autocomplete;
    
    var componentForm = {
      locality: 'long_name', // city
      administrative_area_level_1: 'long_name', //state
      administrative_area_level_2: 'long_name', //state
      country: 'long_name', // country
      Zip_Code: 'short_name' //pincode
    };
    var options = {
      types: ['(cities)'],
        // types: ['geocode'],
        componentRestrictions: {country: "in"}
     };
    
    initAutocomplete();
    
        function initAutocomplete() {
            autocomplete = new google.maps.places.Autocomplete(
            document.getElementById('city'), options );
            // autocomplete.setFields(['address_component']);
            autocomplete.addListener('place_changed', fillInAddress);
        }
    
        function fillInAddress() {
            var place = autocomplete.getPlace();
            console.log(place);
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            $("#auto_address_latitude").val(latitude);
            $("#auto_address_longitude").val(longitude);
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var val = place.address_components[i][componentForm[addressType]];
                if (addressType == "administrative_area_level_2") {
                    // console.log(addressType+" " +val);
                    $("#city").val(val);
    
                }
                else if (addressType == "administrative_area_level_1") {
                    // console.log(addressType+" " +val);
                    $("#state").val(val);
    
                }
                else if (addressType == "country") {
                    // console.log(addressType+" " +val);
                    $("#country").val(val);
                    
                }
                else if (addressType == "Zip_Code") {
                    // console.log(val);
                    $("#Zip_Code").val(val);
    
                }
            }
        }


/*  var s = "1234567890 ";
 alert(s.length);
 */
/* if( s.length == 10 )
{
    alert("tr");
}
else{
    alert("el");
}  */

function isValid(value)
{
  var fieldNum = /^[a-z]+$/i;
//   alert("hi");
  if(value == "")
  {
    return true;
  }
  if ((value.match(fieldNum))) {
      return true;
  }
  else
  {
      return false;
  }

}

function isempty(value)
{
  alert(value);
  if ( value == "" ) {
      return false;
  }
  else
  {
      return true;
  }

}

function check_mob(value)
{
//   var fieldNum = /^[a-z]+$/i;
    // console.log(digits_count(value));
// alert(value);
    len = value.length;

    if( len == 10 )
    {
        return true;
        // alert("tr");
    }
    else
    {
        return false;
        // alert("el");
    }

}

function check_zip(value)
{
//   var fieldNum = /^[a-z]+$/i;
    // console.log(digits_count(value));

    len = value.length;

    if( len == 6 )
    {
        return true;
        // alert("tr");
    }
    else
    {
        return false;
        // alert("el");
    }

}

    $(function()
    {
        $('#attachment1').fileinput({
            maxImageWidth: 300,
            maxImageHeight: 100
        });
        $('#attachment2').fileinput({
            maxImageWidth: 300,
            maxImageHeight: 100
        });

        $('#photo').fileinput({
            maxImageWidth: 300,
            maxImageHeight: 100
        });
    }
    
    )
      function edit() 
        {
            $('#Supplier_Code').prop('readonly',false);
        }

function bs_input_file() {
  $(".input-file").before(
    function() {
      if ( ! $(this).prev().hasClass('input-ghost') ) {
        var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
        element.attr("name",$(this).attr("name"));
        element.change(function(){
          element.next(element).find('input').val((element.val()).split('\\').pop());
        });
        $(this).find("button.btn-choose").click(function(){
          element.click();
        });
        $(this).find("button.btn-reset").click(function(){
          element.val(null);
          $(this).parents(".input-file").find('input').val('');
        });
        $(this).find('input').css("cursor","pointer");
        $(this).find('input').mousedown(function() {
          $(this).parents('.input-file').prev().click();
          return false;
        });
        return element;
      }
    }
  );
}
$(function() {
  bs_input_file();
});



     $(document).ready(function () {

        $("#bankstable").DataTable();
        var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn'),
                allPrevBtn = $('.prevBtn');

        allWells.hide();

        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                    $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-primary').addClass('btn-default');
                $item.addClass('btn-primary');
                allWells.hide();
                $target.show();
//          $target.find('input:eq(0)').focus();
            }
        });

        allPrevBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

            prevStepWizard.removeAttr('disabled').trigger('click');
        });

        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url'],input[type='email'],fieldset > input[type='text']"),
                    isValid = true;

            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid)
                nextStepWizard.removeAttr('disabled').trigger('click');
        });

        $('div.setup-panel div a.btn-primary').trigger('click');

/*         $("#id").click(function(){

            alert("hello");
            $(this).removeClass('id2');
            // alert()
            $(this).addClass('id3');
            alert($(this).attr('class'));

        }) */

        // alert(s.length);


        $.ajax({
            type: "get",
            async: false,
            url: "<?= site_url('suppliers/getNewSupplierCode') ?>",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                        placeholder: "<?= lang('select_category_to_load') ?>",
                        data: data
                    });
                    // alert(data.id);
                    $('#Supplier_Code').val('SUP' + data.id);
                }
            },
            error: function () {
                bootbox.alert('<?= lang('ajax_error') ?>');
                $('#modal-loading').hide();
            }
        });

         function readURL1(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imgInp").change(function(){
      alert("hello");
        window.readURL1(this);
    });
    $("#registrationForm").bootstrapValidator({
                        // To use feedback icons, ensure that you use Bootstrap v3.1.0 or later
                feedbackIcons: {
                  valid: 'fa fa-check',
                  invalid: 'fa fa-times',
                  validating: 'fa fa-refresh'
                },
                fields: {
                    username: {
                        message: 'The username is not valid',
                        validators: {
                            notEmpty: {
                                message: 'The username is required and cannot be empty'
                            },
                            stringLength: {
                                min: 6,
                                max: 30,
                                message: 'The username must be more than 6 and less than 30 characters long'
                            },
                            regexp: {
                                regexp: /^[a-zA-Z0-9]+$/,
                                message: 'The username can only consist of alphabetical and number'
                            },
                            different: {
                                field: 'password',
                                message: 'The username and password cannot be the same as each other'
                            }
                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'The email address is required and cannot be empty'
                            },
                            emailAddress: {
                                message: 'The email address is not a valid'
                            }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'The password is required and cannot be empty'
                            },
                            different: {
                                field: 'username',
                                message: 'The password cannot be the same as username'
                            },
                            stringLength: {
                                min: 8,
                                message: 'The password must have at least 8 characters'
                            }
                        }
                    },
                    birthday: {
                        validators: {
                            notEmpty: {
                                message: 'The date of birth is required'
                            },
                            date: {
                                format: 'YYYY/MM/DD',
                                message: 'The date of birth is not valid'
                            }
                        }
                    },
                    gender: {
                        validators: {
                            notEmpty: {
                                message: 'The gender is required'
                            }
                        }
                    }
                }
            });

    $('#General_form').bootstrapValidator({
        feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-times',
            validating: 'fa fa-refresh'
        },
        fields: {
            gstno: {
                validators: {
                    integer: {
                        message: 'The gst is int'
                    }
                }
            }
        }
    });

    $('#Address_form').bootstrapValidator({
        feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-times',
            validating: 'fa fa-refresh'
        },
        fields: {
          Address1: {
                validators: {
                    notEmpty: {
                        message: 'The Address1 is required'
                    }
                }
            },
            City: {
                validators: {
                    notEmpty: {
                        message: 'The City is required'
                    }
                }
            },
            State: {
                validators: {
                    notEmpty: {
                        message: 'The State is required'
                    }
                }
            },
            Country: {
                validators: {
                    notEmpty: {
                        message: 'The Country is required'
                    }
                }
            },
            Zip_Code: {
                validators: {
                    notEmpty: {
                        message: 'The Zip_Code is required'
                    },
                    integer: {
                        message: 'The value is not an integer'
                    },
                    callback: {
                        message: 'The Zip Code must be exact 6 numbers long',
                        callback: function(value, validator, $field) {
                            if (!check_zip(value)) {
                              return {
                                valid: false,
                              };
                            }
                            else
                            {
                              return {
                                valid: true,
                              };    
                            }

                        }
                    }

                }
            }
        }
    });
    $('#Bank_form').bootstrapValidator({
        feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-times',
            validating: 'fa fa-refresh'
        },
        fields: {
            Bank_Name: {
                validators: {
                    callback: {
                        message: 'please enter only letters',
                        callback: function(value, validator, $field) {
                            if (!isValid(value)) {
                              return {
                                valid: false,
                              };
                            }
                            else
                            {
                              return {
                                valid: true,
                              };    
                            }

                        }
                    }
                }
            },
            Account_Number: {
                validators: {
                    integer: {
                        message: 'The Account_Number contains only numbers'
                    }
                }
            },
            Opening_Balance: {
                validators: {
                    integer: {
                        message: 'The Opening_Balance contains only numbers'
                    }
                }
            }
/*             IFSC: {
                validators: {
                    notEmpty: {
                        message: 'The IFSC is required'
                    }
                }
            }, */
/*             Bank_Branch: {
                validators: {
                    notEmpty: {
                        message: 'The Bank_Branch is required'
                    }
                }
            } */           
        }
    });
    $('#Contact_form').bootstrapValidator({
        feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-times',
            validating: 'fa fa-refresh'
        },
        fields: {
          first_Name: {
                validators: {
/*                     notEmpty: {
                        message: 'The First_Name is required'
                    }, */
                    callback: {
                        message: 'please enter only letters',
                        callback: function(value, validator, $field) {
                            if (!isValid(value)) {
                              return {
                                valid: false,
                              };
                            }
                            else
                            {
                              return {
                                valid: true,
                              };    
                            }

                        }
                    }

                }
            },
            last_Name: {
                validators: {
/*                     notEmpty: {
                        message: 'The Last_Name is required'
                    }, */
                    callback: {
                        message: 'please enter only letters',
                        callback: function(value, validator, $field) {
                            if (!isValid(value)) {
                              return {
                                valid: false,
                              };
                            }
                            else
                            {
                              return {
                                valid: true,
                              };    
                            }

                        }
                    }


                }

            },
            Telephone_no: {
                validators: {
                    integer: {
                        message: 'The value is not an integer'
                    },
                    callback: {
                        message: 'The Telephone_no must be exactly 10 digits',
                        callback: function(value, validator, $field) {
                            if (!check_mob(value)) {
                              return {
                                valid: false,
                              };
                            }
                            else
                            {
                              return {
                                valid: true,
                              };    
                            }
                        }
                    }

                }
            },      
            Fax_no: {
                validators: {
                    integer: {
                        message: 'The value is not an integer'
                    }
                }
            },        
            mobile_No: {
                validators: {
                    notEmpty: {
                        message: 'The Mobile_No is required'
                    },
                    integer: {
                        message: 'The value is not an integer'
                    },
/*                     options: {
                        country: 'IN',
                        message: 'The value is not a valid phone number'
                    } */
/*                     stringLength: {
                        max: 10,
                        message: 'The Mobile_No must be max than 10 digits'
                    },
                    stringLength: {
                        min: 10,
                        message: 'The Mobile_No must be min than 10 digits'
                    } */
                    callback: {
                        message: 'The Mobile_No must be exactly 10 digits',
                        callback: function(value, validator, $field) {
                            if (!check_mob(value)) {
                              return {
                                valid: false,
                              };
                            }
                            else
                            {
                              return {
                                valid: true,
                              };    
                            }

                        }
                    }
                }
            },
            mail: {
                validators: {
/*                     notEmpty: {
                        message: 'The email address is required and cannot be empty'
                    }, */
                    emailAddress: {
                        message: 'The email address is not a valid'
                    }
                }
            }        
        }
    });

    $('#supplierinfo_form')
        .bootstrapValidator({
        feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-times',
            validating: 'fa fa-refresh'
        },
            // ignore: [],
        fields: {
            store: {
                
                validators: {
                     notEmpty: {
                        message: 'The Select_Store is required'
                    }
/*                     ,

                    callback: {
                        message: 'please enter only letters',
                        callback: function(value, validator, $field) {
                            if( value == "" )
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                    }*/
                } 
            },
            Supplier_Code: {
                validators: {
                    notEmpty: {
                        message: 'The Supplier_Code is required'
                    }
                }
            },
            Supplier_Name: {
                validators: {
                    notEmpty: {
                        message: 'The Supplier_Name is required'
                    },
                    callback: {
                        message: 'please enter only letters',
                        callback: function(value, validator, $field) {
                            if (!isValid(value)) {
                              return {
                                valid: false,
                              };
                            }
                            else
                            {
                              return {
                                valid: true,
                              };    
                            }

                        }
                    }
                }
            },


            Supplier_Discount: {
                validators: {
/*                     notEmpty: {
                        message: 'The Supplier_Discount is required'
                    },
 */                    integer: {
                        message: 'The value is not an integer'
                    },
                    between: {
                        min: 0,
                        max: 100,
                        message: 'The Supplier_Discount must be between 0 and 100'
                    }
                }
            },
           /*  'attachment[]': {
                validators: {
                    file: {
                        extension: 'jpeg,png',
                        type: 'image/jpeg,image/png,application/pdf,application/zip,application/x-rar-compressed,application/vnd.ms-powerpoint,text/plain,application/vnd.ms-excel,application/msword,image/tiff,image/gif',
                        maxSize: 2048 * 1024,
                        message: 'The selected file is not valid'
                    }
                }
            }  */
            
            aadhar: {
                validators: {
                    file: {
                        extension: 'jpeg,png,pdf,zip,rar,ppt,txt,xls,doc,tif,gif',
                        type: 'image/jpeg,image/png,application/pdf,application/zip,application/x-rar-compressed,application/vnd.ms-powerpoint,text/plain,application/vnd.ms-excel,application/msword,image/tiff,image/gif',
                        maxSize: 4096 * 4096,
                        message: 'The selected file is not valid'
                    }
                }
            },
            pan: {
                validators: {
                    file: {
                        extension: 'jpeg,png,pdf,zip,rar,ppt,txt,xls,doc,tif,gif',
                        type: 'image/jpeg,image/png,application/pdf,application/zip,application/x-rar-compressed,application/vnd.ms-powerpoint,text/plain,application/vnd.ms-excel,application/msword,image/tiff,image/gif',
                        maxSize: 4096 * 4096,
                        message: 'The selected file is not valid'
                    }
                }
            }

        }

    });

    $('#savenewsupplier_photo').bootstrapValidator({
        feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-times',
            validating: 'fa fa-refresh'
        },
        fields: {
            photo: {
                validators: {
                    notEmpty: {
                        message: 'The Photo is required'
                    },
                    file: {
                        extension: 'jpeg,png,pdf,zip,rar,ppt,txt,xls,doc,tif,gif',
                        type: 'image/jpeg,image/png,application/pdf,application/zip,application/x-rar-compressed,application/vnd.ms-powerpoint,text/plain,application/vnd.ms-excel,application/msword,image/tiff,image/gif',
                        maxSize: 4096 * 4096,
                        message: 'The selected file is not valid'
                    }

                }
            }
        }
    
     });
     $('#refered_by').bootstrapValidator({
        feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-times',
            validating: 'fa fa-refresh'
        },
        fields: {
            percentage_commision: {
                validators: {
                    integer: {
                        message: 'The value is not an integer'
                    } 
                }
            }
        }
    
     });
     
     $('#PaymentTerms').bootstrapValidator({
        feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-times',
            validating: 'fa fa-refresh'
        },
        fields: {
            payment: {
                validators: {
                    notEmpty: {
                        message: 'The payment is required'
                    },
                       integer: {
                        message: 'The value is not an integer'
                    }
                }
            }
        }
    
     });
/*       $('#Address_form').on('keyup', function(e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) { 
                e.preventDefault();
                return false;
            }
        });
 */ 

// var v;
        $('.next').click(function (e) 
        {

            id_of_a = $(this).closest('.tab-pane').attr('aria-labelledby');

            // id_of_div = id_of_div + "-tab";
            // alert(id_of_a);
              // e.preventDefault();
            //   alert("1st");
            id = $(this).closest('form').attr('id');

            // alert(id)

            action = $(this).closest('form').attr('action');

            // alert(action);
            // alert(id);

            var bootstrapValidator = $('#'+id).data('bootstrapValidator');
            
            var result = bootstrapValidator.isValid();
            // debugger;

            // alert(result);

            var bool = result.toString().localeCompare('true');

            // alert(bool);

            // alert(result);

            if( bool == 0 )
            {
                // var data = $('#'+id).serialize('input,select');

                var myform = document.getElementById(id);
                var fd = new FormData(myform);

                console.log(fd);

                // console.log(myform);
/*                 var myform = document.getElementById(id);
                var fd = new FormData(myform); */

                // console.log(fd);

                // alert('hi');
                $.ajax(
                {
                    type:"POST",
                    processData: false,
                    contentType: false,
                    // crossDomain: true,
                    data: fd,
                    url:action,
                    success:function(data)
                    {
                        //  alert(data);
                         console.log(data);
                        // alert(data);
                         
                    /* var url = "<?php //echo site_url().'/Customer_show'?>"
                    window.location.href = url;*/
                    // window.location("www.example.com");
                    }
               /*      error : function(request, status, error) {
                        alert("helo");
                    },
                */ });
                // alert(id_of_a);
                $('.nav-pills > .nav-item > #'+id_of_a).parent().next('li').removeClass('disabled');
                var aria = $('.nav-pills > .nav-item > #'+id_of_a).parent().next('li').find('a').attr('aria-controls');
                // alert(aria+" tab");
                var a = $('.nav-pills > .nav-item > #'+id_of_a).parent().next('li').find('a');
                // alert(a.attr('href')+" next");
                // alert(a.attr('href'));
                // a.removeClass()
                if( a.attr('href') == "#" )
                {
                    $('.nav-pills > .nav-item #'+id_of_a).parent().next('li').find('a').attr('href',a.attr('href')+aria);
                }
                $('.nav-pills > .nav-item #'+id_of_a).parent().next('li').find('a').trigger('click');
            }
            else
            {
                // alert("false");
            }
            
            /* console.log('#'+v);
            // id = '#'+v;
            alert();
            $('#'+v).find('div').each(function(){
                console.log($(this).printElement());
            }); */
            // });
        });

        $('.previous').click(function () {

            id_of_a = $(this).closest('.tab-pane').attr('aria-labelledby');

// id_of_div = id_of_div + "-tab";
// alert(id_of_a);
  // e.preventDefault();
//   alert("1st");
                id = $(this).closest('form').attr('id');

                $('.nav-pills > .nav-item > #'+id_of_a).parent().prev('li').removeClass('disabled');
                var aria = $('.nav-pills > .nav-item > #'+id_of_a).parent().prev('li').find('a').attr('aria-controls');
                alert(aria+" tab");
                var a = $('.nav-pills > .nav-item > #'+id_of_a).parent().prev('li').find('a');
                alert(a.attr('href'));
                // alert(a.attr('href'));
                // a.removeClass()
                // $('.nav-pills > .nav-item #'+id_of_a).parent().prev('li').find('a').attr('href',a.attr('href')+aria);
                $('.nav-pills > .nav-item > #'+id_of_a).parent().prev('li').find('a').trigger('click');
        });

        // console.log(v);

/*         function form_validate(attr_id){
            var result = true;
            $('#'+attr_id).validator('validate');
            $('#'+attr_id+' .form-group').each(function(){
                if($(this).hasClass('has-error')){
                    result = false;
                    return false;
                }
            });
            return result;
        } */


        $('.but').click(function(e)
         {  
            // console.log(valid.validity);
            // e.preventDefault();
            // var val = $("#supplierinfo_form");
            // alert(val.valid());
            var bootstrapValidator = $('#supplierinfo_form').data('bootstrapValidator');

            alert(bootstrapValidator.isValid());

            // alert(form_validate('supplierinfo_form'));

/*             console.log($('#supplierinfo_form').validate());
            // alert($('#supplierinfo_form').validator('validate').has('.has-error').length);
            v = $(this).closest('form').attr('id'); */
            // alert($('#supplierinfo_form').valid());
/*             if ($('#supplierinfo_form').validate('validate')) {
                alert('SOMETHING WRONG');
            } else {
                //$("#form2").submit();
                alert('EVERYTHING IS GOOD');
            } */

            // alert(val.Valid());
            // $('#'+v).find('div').each(function(){
                // console.log($(this).printElement());
                // input = ($(this).find("input[type='text']"));
                // alert($(input).valid());
                // console.log(input);
                // debugger;
              /*   if ( input.validity.valid )
                {
                    alert("er");
                } */
            // });            
            // console.log($(this).closest('form').html());
        });
    });
    
 </script>
