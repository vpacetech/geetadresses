<style>
    fieldset.scheduler-border {
        border: 1px solid #ddd !important;
        padding: 0 1em 1em 1em !important;
        margin: 0 0 1.1em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
        box-shadow:  0px 0px 0px 0px #000;
    }


    legend.scheduler-border {
        width:inherit; /* Or auto */
        padding:0 10px; /* To give a bit of padding on the left and right */
        border-bottom:none;

    }
    .cal 
    {
        position:relative;
        top:-30px;
        left:90%;
    }
    /*
    a {
        color: #000 !important;    
            }*/
    .nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
        color: #6164c0 !important;
        background-color: #fff !important;
        border-radius:0 !important;
    }

    .nav-pills>li>a{
        color: #6164c0 !important;
        background-color: #e9ebec !important;
        border-radius:0 !important;
    }
    .tab-content>.active {
        display: block;
        border : 1px solid #ccc;
        padding:5px !important;
    }
    .btton
    {
        padding: 5px;
        width:100%;
    }

    .stepwizard-step {
        display: table-cell;
        /*text-align: center;*/
        position: relative;
    }
    .form-horizontal .control-label {
        padding-top: 0px;
    }
</style>
<!-- <script type="text/javascript" src="<?= $assets ?>js/webcam.js"></script> -->

<div id="exTab1" class="container-fluid" style="background-color: #ffff;padding: 10px" ng-controller="employee">	

    <!--    <ul  class="nav nav-pills">
            <li class="active"><a  href="#1a" data-toggle="tab">General Details</a></li>
            <li><a href="#2a" data-toggle="tab">User Fields</a></li>
            <li><a href="#3a" data-toggle="tab">Qualification</a></li>
            <li><a href="#4a" data-toggle="tab">Attachments</a></li>
            <li><a href="#login_details" data-toggle="tab">Login Credentials</a></li>
    
        </ul>-->

    <div class="stepwizard">
        <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step">
                <a href="#step-1" type="button"  class="btn btn-primary ">Supplier_info</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-2" type="button" class="btn btn-default " disabled="disabled">Adress</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-3" type="button" class="btn btn-default " disabled="disabled">General and Payment</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-4" type="button" class="btn btn-default " disabled="disabled">Attachments and photos</a>
            </div>
            <!-- <div class="stepwizard-step">
                <a href="#step-5" type="button" class="btn btn-default " disabled="disabled">Login Credentials</a>
            </div> -->
        </div>
    </div>

    <?php

    echo validation_errors();

    $attrib = array('data-toggle' => 'validator', 'class' => 'form-horizontal' , 'id'=>'form', 'role' => 'form');
    echo form_open_multipart("suppliers/savesupplier", $attrib);
    ?>

    <div class="tab-pane" style="border: 1px solid #000; padding: 10px">

        <div class="row setup-content" id="step-1">

  
            <div class="col-md-12">
                <div class="row">
                        <div class="form-group">
                        <label class="col-sm-3 control-label" for="Supplier_Code">Supplier Code</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" readonly id="Supplier_Code" name="Supplier_Code" required data-bv-notempty="true" />
                        </div>
                    <button type="button" id="code" class="btn btn-primary" onclick="edit()">Define my own code</button>
                    </div>          

                    
                    <div class="form-group" id="check">
                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                        <input id="Preferred_Suppliers" data-type='res' name='Preferred_Supplier' type='checkbox'>&emsp;
                        <label for="Preferred_Suppliers">Preferred Suppliers&emsp;</label>&emsp;
                        <input id="Is_Active" data-type='com' name='Is_Active' checked type='checkbox'>&emsp;
                        <label for="Is_Active">Is Active&emsp;</label>
                    </div>  
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="Select_Store">Select Store</label>
                        <div class="col-sm-5">
                                <select class="form-control tip" id="Select_Store" name="store" required data-bv-notempty="true">
                                <option selected option disabled>select store</option>
                                <?php 
                                foreach ($store as $biller) {?>
                                    
                                    <option value="<?php echo $biller->id?>"><?php echo $biller->name ?></option>        
                                <?php        
                                }?>
                    
                            </select>             
                        </div>
                    </div>                
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="Supplier_Name">Firm Name</label>
                        <div class="col-sm-5">
                            <input type="text"  class="form-control"  id="Supplier_Name" name="Supplier_Name" required="required" data-bv-notempty-message="this field is required"	 data-bv-notempty="true" 
                            data-bv-regexp="true" data-bv-regexp-regexp="^[a-zA-Z]+[-\s]?[a-zA-Z ]+$" data-bv-regexp-message="Allow Only Alphabets"/>
                        </div>
                    </div>    

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="first_Name">first Name</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="first_Name" name="first_Name" data-bv-regexp="true" data-bv-regexp-regexp="^[a-zA-Z]+[-\s]?[a-zA-Z ]+$" data-bv-regexp-message="Allow Only Alphabets"/>
                        </div>
                    </div>    

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="last_Name">last Name</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="last_Name" name="last_Name" data-bv-regexp="true" data-bv-regexp-regexp="^[a-zA-Z]+[-\s]?[a-zA-Z ]+$" data-bv-regexp-message="Allow Only Alphabets"/>
                        </div>
                    </div>  

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="telephone">Telephone No</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="telephone" name="telephone_no" 

                            data-bv-regexp-regexp="^[0-9]+$"
                            data-bv-regexp-message="The Telephone No can only consist of digits"
                            data-bv-stringlength="true"
                            data-bv-stringlength-min="10"
                            data-bv-stringlength-max="10"
                            data-bv-stringlength-message="The Telephone no must be exact 10 numbers long"
                            
                            />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="fax_no">fax No</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="fax_no" name="fax_no" />
                        </div>
                    </div> 

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="mobile_No">mobile No</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="mobile_No" name="mobile_No" required data-bv-notempty="true" 
                            
                            data-bv-regexp-regexp="^[0-9]+$"
                            data-bv-regexp-message="The mobile No can only consist of digits"
                            data-bv-stringlength="true"
                            data-bv-stringlength-min="10"
                            data-bv-stringlength-max="10"
                            data-bv-stringlength-message="The mobile no must be exact 10 numbers long"

                            
                            />
                        </div>
                    </div>  

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="website">website</label>
                        <div class="col-sm-5">
                            <input type="url" class="form-control" id="website" name="website" />
                        </div>
                    </div> 


                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="mail">E-mail</label>
                        <div class="col-sm-5">
                            <input type="email" class="form-control" id="mail" name="mail"  />
                        </div>
                    </div>          


                </div>
                 

                <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
            </div>

        </div>
        <div class="row setup-content" id="step-2">
            <div class="col-md-12">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="Address1">Address Line1</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="Address1" name="Address1" 
                            
                            required
                            data-bv-notempty="true"/>
                        </div>
                    </div>
                     
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="Address2">Address Line 2</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="Address2"/>
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="city">City</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control tip" id="city" name="city" 
                            required
                            
                            data-bv-notempty="true"/>
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="state">State</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control tip" id="state" name="state" required data-bv-notempty="true"/>
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="country1">Country</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control tip" id="country" name="country" data-bv-notempty="true" />
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="Zip_Code">Zip Code</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="Zip_Code" name="Zip_Code" 
                            required
                            data-bv-notempty="true" 
                            data-bv-notempty-message="The Zip Code is required and cannot be empty"
                            data-bv-stringlength="true"
                            data-bv-stringlength-min="6"
                            data-bv-stringlength-max="6"
                            data-bv-stringlength-message="The Zip Code must be exact 6 numbers long"
                            data-bv-regexp="true"
                            data-bv-regexp-regexp="^[0-9]+$"
                            data-bv-regexp-message="The Zip Code can only consist of digits"
                            
                            />
                        </div>
                    </div>
                    <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                    <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                </div>
            </div>  
        </div>

        <div class="row setup-content" id="step-3">

            <div class="col-md-12">
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="gstno">GST No</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="gstno" name="gstno" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">PAN</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="panno"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="IGST">IGST</label>
                        <div class="col-sm-5" id="check1">
                            <input type="checkbox" class="form-control" id="IGST" name="IGST"> 
                        </div>
                    </div>      
                    <div class="form-group">
                
                        <label for="payment" class="col-sm-3 control-label">Payment needs to be settled within</label>
            
                        <div class="col-sm-5">
                            <input type="text" id="payment"  class="form-control" name="payment"
                            required 
                            data-bv-notempty="true"                 
                            data-bv-integer="true"
                            data-bv-integer-message="The value is not an integer" /> <!-- <span><b>sdays</b></span> -->
                        </div>
                    </div>      
                    
                    <div class="form-group">
                        
                        <label class="col-sm-3 control-label" for="percentage_commision">percentage Commision</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="percentage_commision"  name="percentage_commision" required data-bv-notempty="true" 
                                data-bv-integer="true"
                                data-bv-integer-message="The value is not an integer"
                            
                            >   
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="Select_reffered">Select Refered By</label>
                        <div class="col-sm-5">
                            <select class="form-control" name="refered">
                                <option selected option disabled>select</option>
                                    <?php 
                                foreach ($suppliers as $biller) {?>
                                    
                                    <option value="<?php echo $biller->id ?>"><?php echo $biller->company ?></option>        
                                <?php        
                                }?>
                            
                            </select>
                        </div>
                    </div>  

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="Supplier_Discount1">Supplier Discount(%)</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control"  id="Supplier_Discount" name="Supplier_Discount"  
                            data-bv-integer="true"
                            data-bv-integer-message="The value is not an integer"
                            />
                        </div>
                    </div>          



                    <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                    <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                </div>
            </div>
        </div>

        <div class="row setup-content" id="step-4">
            <!-- <div class=""> -->
                <div class="col-md-12">
                    <div class="row">
                        <div class="form-group">

                            <label class="col-sm-3 control-label" for="aadhar_card">aadhar card</label>
                            <div class="col-sm-5">
                                <input id="attachment1" name="aadhar" type="file" class="form-control align"
                                    data-bv-file="true"
                                    data-bv-file-extension = "jpeg,png,pdf"
                                    data-bv-file-type = "image/jpeg,image/png,application/pdf"	
                                    data-bv-file-maxsize = "2146304"	
                                    data-show-upload="false"
                                    data-show-preview="false"
                                />
                            </div>
                        </div>


                        <div class="form-group">

                            <label class="col-sm-3 control-label" for="Pan">Pan card</label>
                                <div class="col-sm-5">
                                    <input id="attachment2" name="pan"  type="file" class="form-control align" 
                                    data-bv-file="true"
                                    data-show-upload="false"
                                    data-show-preview="false" 

                                    data-bv-file-extension="jpeg,png,pdf"
                                    data-bv-file-type="image/jpeg,image/png,application/pdf"	
                                    data-bv-file-maxsize = "2146304"

                                    
                                    />
                                </div>
                        </div>

                        <div class="form-group">

                            <label class="col-sm-3 control-label" for="photo">photo</label>
                            <div class="col-sm-5">
                            <input id="photo" name="photo" id="photo" type="file" required data-bv-notempty="true" class="form-control align" 
                                data-bv-file="true"
                                data-show-upload="false"
                                data-show-preview="false" 
                                data-bv-file-extension="jpeg,png,pdf"
                                data-bv-file-type="image/jpeg,image/png,application/pdf"	
                                data-bv-file-maxsize = "2146304"
                                />
                            </div>
                        </div>
                        
                    </div>
                    <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                    <button id="save" type="button" class="btn btn-primary nextBtn pull-right" type="button">save</button>
                </div>
            <!-- </div> -->
        </div>


    </div>

    <!--end tab-content----->

    <?php echo form_close(); ?>
</div>





<script>


var autocomplete;
    
    var componentForm = {
      locality: 'long_name', // city
      administrative_area_level_1: 'long_name', //state
      administrative_area_level_2: 'long_name', //state
      country: 'long_name', // country
      Zip_Code: 'short_name' //pincode
    };
    var options = {
      types: ['(cities)'],
        // types: ['geocode'],
        componentRestrictions: {country: "in"}
     };
    
    initAutocomplete();
    
        function initAutocomplete() {
            autocomplete = new google.maps.places.Autocomplete(
            document.getElementById('city'), options );
            // autocomplete.setFields(['address_component']);
            autocomplete.addListener('place_changed', fillInAddress);
        }
        function fillInAddress() {
            var place = autocomplete.getPlace();
            console.log(place);
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            $("#auto_address_latitude").val(latitude);
            $("#auto_address_longitude").val(longitude);
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var val = place.address_components[i][componentForm[addressType]];
                if (addressType == "administrative_area_level_2") {
                    // console.log(addressType+" " +val);
                    $("#city").val(val);
    
                }
                else if (addressType == "administrative_area_level_1") {
                    // console.log(addressType+" " +val);
                    $("#state").val(val);
    
                }
                else if (addressType == "country") {
                    // console.log(addressType+" " +val);
                    $("#country").val(val);
                    
                }
                else if (addressType == "Zip_Code") {
                    console.log(val);
                    $("#Zip_Code").val(val);
    
                }
            }
        }


    $(document).ready(function () {


        /* $('#save').click(function(e){

            e.preventDefault();
                return false;

            var bootstrapValidator = $('#form').data('bootstrapValidator');
            
            var result = bootstrapValidator.isValid();

            alert(result);

            if( result == false )
            {s
                alert('false');
                e.preventDefault();
                return false;
            }

        }); */

        $('.align').fileinput({
            maxImageWidth: 300,
            maxImageHeight: 100
        });


        $.ajax({
            type: "get",
            async: false,
            url: "<?= site_url('suppliers/getNewSupplierCode') ?>",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                        placeholder: "<?= lang('select_category_to_load') ?>",
                        data: data
                    });
                    // alert(data.id);
                    $('#Supplier_Code').val('SUP' + data.id);
                }
            },
            error: function () {
                bootbox.alert('<?= lang('ajax_error') ?>');
                $('#modal-loading').hide();
            }
        });





        var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn'),
                allPrevBtn = $('.prevBtn');

        allWells.hide();

        // console.log($('div.setup-panel div a'));

        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                    $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-primary').addClass('btn-default');
                $item.addClass('btn-primary');
                allWells.hide();
                $target.show();
//          $target.find('input:eq(0)').focus();
            }
        });

        allPrevBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

            prevStepWizard.removeAttr('disabled').trigger('click');
        });

        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='file'],input[type='url'],input[type='email'],fieldset > input[type='text']"),
                    isValid = true;
            
                    console.log(curInputs);
                    console.log(curStepBtn);
            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid)
            {

                nextStepWizard.removeAttr('disabled').trigger('click');
                if(curStepBtn == "step-4")
                {
                    // $("#form").submit();
                    document.getElementById("form").submit()

                }
            }
        });

        $('div.setup-panel div a.btn-primary').trigger('click');
    });
</script>
<script>

    $(document).on('ifUnchecked', '#hlsda', function (e) {
        $('#msd1').attr('disabled', "disabled");
        $('#msd1').attr('required', 'required');
    });
    $(document).on('ifChecked', '#hlsda', function (e) {
        $('#msd1').removeAttr("disabled");
        $('#msd1').removeAttr('required');
    });

    $(document).on('ifUnchecked', '#isloyatlity', function (e) {
        $('#loyalityno').attr('disabled', "disabled");
        $('#enrolldate').attr('disabled', "disabled");
        $('#loyalitypoint').attr('disabled', "disabled");
        $('#referby').attr('disabled', "disabled");
        $('#loyalityno').attr('required', 'required');
        $('#enrolldate').attr('required', 'required');
        $('#loyalitypoint').attr('required', 'required');
        $('#referby').attr('required', 'required');

    });
    $(document).on('ifChecked', '#isloyatlity', function (e) {
        $('#loyalityno').removeAttr("disabled");
        $('#enrolldate').removeAttr("disabled");
        $('#loyalitypoint').removeAttr("disabled");
        $('#referby').removeAttr("disabled");
        $('#creaditlimit').removeAttr('required');
        $('#enrolldate').removeAttr('required');
        $('#loyalitypoint').removeAttr('required');
        $('#referby').removeAttr('required');
    });

    $('#store_logo_up').fileinput({
        maxImageWidth: 300,
        maxImageHeight: 100
    });
    $('#attachment').fileinput({
        maxImageWidth: 300,
        maxImageHeight: 100
    });
    $('#referencesfile').fileinput({
        maxImageWidth: 300,
        maxImageHeight: 100
    });

    $('#document').fileinput({
        maxImageWidth: 300,
        maxImageHeight: 100
    });
    $('#documentfile').fileinput({
        maxImageWidth: 300,
        maxImageHeight: 100
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            $("#supplierphoto").val('');
        }
    }

    $("#store_logo_up").change(function () {
        readURL(this);
    });
    $("#takepicture").click(function () {
        $('#my_camera').css('display', 'inline-block');
        $('#tacksnaps').css('display', 'inline-block');
        $('#image_upload_preview').css('display', 'none');
        $('#takepicture').css('display', 'none');
    });
    $("#dobclear").click(function () {

        $('#dob').val('');
    });
</script>

<script language="JavaScript">
    Webcam.set({
        width: 400,
        height: 330,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
    Webcam.attach('#my_camera');
</script>
<script language="JavaScript">
    function take_snapshot() {
        // take snapshot and get image data
        Webcam.snap(function (data_uri) {
            readURL("input[name='webcam']");
            Webcam.upload(data_uri, 'suppliers/savecamImage', function (code, text) {
                $("#image_upload_preview").attr("src", 'assets/uploads/' + text);
                $("#supplierphoto").val(text);
                $('#my_camera').css('display', 'none');
                $('#image_upload_preview').css('display', 'inline-block');
                $('#takepicture').css('display', 'inline-block');
                $('#tacksnaps').css('display', 'none');
            });
        });
    }
</script>

<script>
    function addQualifications() {
        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">' +
                '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">' +
                '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">' +
                '<input type="text" name="quali[]" class="form-control" id="email" placeholder="B.C.A">' +
                '</div>' +
                '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">' +
                '<input type="text" name="yr[]" class="form-control date" id="email" placeholder="Year of Pasing">' +
                '</div>' +
                '<label class="col-lg-2 col-sm-2 col-md-2 col-xs-12" for="email" onclick="addQualifications()"><i class="fa fa-plus-circle fa-2x"></i></label>' +
                '</div>' +
                '</div>';
        $('.qualification').append(data);
    }

    function addTraining() {
        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">' +
                '<label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="email">Special Training:</label>' +
                '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">' +
                '<input type="text" name="special_training[]" class="form-control" id="email" placeholder="Tally - Accounting Software" style="margin-bottom:10px;">' +
                '</div>' +
                '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">' +
                '<i class="fa fa-2x fa-plus-circle" onclick="addTraining()"></i>' +
                '</div>' +
                '</div>';
        $('#training').append(data);
    }
    function addExperience() {
        var data = '<tr>' +
                '<td><input type="text"  class="form-control" id="email"  name="year[]" placeholder="Year"></td>' +
                '<td><input type="text" class="form-control" id="email" name="company[]" placeholder="Company"></td>' +
                '<td><input type="text" class="form-control" id="email" name="position[]" placeholder="Position"></td>' +
                '<td><input type="text" class="form-control" id="email" name="reference[]" placeholder="Reference"></td>' +
                '<td><i class="fa fa-2x fa-plus-circle" onclick="addExperience()"></i></td>' +
                ' </tr>';
        $('#experiance').append(data);
    }
    function addDocument() {
        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">' +
                '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">' +
                '<label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="email" style="font-weight:bold;">Identify Proof -</label>' +
                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">' +
                '<input type="name" class="form-control" id="email" placeholder="0000000000">' +
                '</div>' +
                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">' +
                '<label class="btn btn-default btn-file" style="margin:0;">' +
                'Browse <input type="file" name="document[]" style="display: none;">' +
                '</label>' +
                ' </div>' +
                '<div class="col-lg-2 col-sm-2 col-md-2 col-xs-6">' +
                ' <i class="fa fa-2x fa-plus-circle" onclick="addDocument()"></i>' +
                ' </div>' +
                '</div>' +
                '</div>';
        $('#document').append(data);
    }

    function addReferences() {
        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">' +
                '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">' +
                '<label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="email" style="font-weight:bold;">References -</label>' +
                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="margin-bottom:10px;">' +
                '<input id="referencesfile" name="references[]" type="file"  class="form-control referencesfile" data-show-upload="false" data-show-preview="false" data-bv-file-maxsize="300*100" />' +
                '</div>' +
                '<div class="col-lg-2 col-sm-2 col-md-2 col-xs-6">' +
                '<i class="fa fa-2x fa-plus-circle" onclick="addReferences()"></i>' +
                ' </div>' +
                '</div>' +
                '</div>';
        $('#references').append(data);
    }
//    function addReferences() {
//        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">' +
//                '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">' +
//                '<label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="email" style="font-weight:bold;">References -</label>' +
//                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">' +
//                ' <input type="name" class="form-control" id="email" placeholder="0000000000" style="margin-bottom:10px;">' +
//                ' </div>' +
//                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="margin-bottom:10px;">' +
//                '<label class="btn btn-default btn-file" style="margin:0;">' +
//                '  Browse <input type="file" name="references[]" style="display: none;">' +
//                '  </label>' +
//                '</div>' +
//                '<div class="col-lg-2 col-sm-2 col-md-2 col-xs-6">' +
//                '<i class="fa fa-2x fa-plus-circle" onclick="addReferences()"></i>' +
//                ' </div>' +
//                '</div>' +
//                '</div>';
//        $('#references').append(data);
//    }



    $('#location').change(function (event) {
        var store = $('#location').val();
        $.ajax({
            type: "get", async: false,
            url: "<?= site_url('billers/getDepartment') ?>",
            data: {'store': store},
            dataType: "json",
            success: function (data) {
                $('#department').html('');
                if (data == '') {
//                    bootbox.alert('<?= lang('department_is_not_available') ?>');
                } else {
                    $.each(data, function (k, val) {
                        var opt = $('<option />');
                        opt.val(val.id);
                        opt.text(val.name);
                        $('#department').append(opt);
                    })
                }
            }
        });
    });


    $('#department').change(function (event) {
        var section = $(this).val();
        $.ajax({
            type: "get", async: false,
            url: "<?= site_url('billers/getsection') ?>",
            data: {'section': section},
            dataType: "json",
            success: function (data) {
                $('#section').html('');
                if (data == '') {
//                    bootbox.alert('<?= lang('department_is_not_available') ?>');
                } else {
                    $.each(data, function (k, val) {
                        var opt = $('<option />');
                        opt.val(val.id);
                        opt.text(val.name);
                        $('#section').append(opt);
                    })
                }
            }
        });
    });

    $('#email').blur(function (event) {
        var email = $(this).val();
        $.ajax({
            type: "get",
            async: false,
            url: "<?= site_url('employee/checkuserEmail') ?>",
            data: {'email': email},
            dataType: "json",
            success: function (data) {
                if (data == '1') {
                    $('#email').val('');
                    bootbox.alert('Email Already Exists. Try Another One');
                }
            }
        });
    });

    function edit() 
        {
            $('#Supplier_Code').prop('readonly',false);
        }

</script>

