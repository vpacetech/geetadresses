<style>
    fieldset.scheduler-border {
        border: 1px groove #ddd !important;
        padding: 0 1.4em 1.4em 1.4em !important;
        margin: 0 0 1.5em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
        box-shadow:  0px 0px 0px 0px #000;
    }

    legend.scheduler-border {
        width:inherit; /* Or auto */
        padding:0 10px; /* To give a bit of padding on the left and right */
        border-bottom:none;
    }
    .mainblock
    {
        margin:20px 15px;
    }
    .form-group {
        margin-bottom: 6px !important;
    }
    .btton
    {
        padding: 5px;
        width:100%;
    }
</style>
<script type="text/javascript" src="<?= $assets ?>js/webcam.js"></script>
<div>
    <div class="container-fluid" style="background-color: #ffff;padding: 10px">

        <?php
        $attrib = array('data-toggle' => 'validator', 'class' => 'form-horizontal', 'role' => 'form');
        echo form_open_multipart("suppliers/edit/" . $supplier->id, $attrib);
        ?>
        <div class="row">
            <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12">

                <div class="form-group">
                    <div class="control-label col-sm-5">
                        <?= lang("supliercode", "supliercode"); ?></div>
                    <div class="col-sm-7">
                        <?php echo form_input('supliercode', $supplier->code, 'class="form-control readonly tip remove_readonly" id="supliercode" readonly data-bv-notempty="true" '); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="control-label col-sm-5">
                        <?= lang("select_store", "select_store"); ?></div>
                    <div class="col-sm-7">
                        <?php
                        $bl[""] = "";
                        foreach ($store as $biller) {
                            $bl[$biller->id] = $biller->name;
                        }
                        echo form_dropdown('store_id', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $supplier->store_id), ' data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("store") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                        ?>
                    </div>
                </div>

                <div class="form-group">
                    <div class="control-label col-sm-5"><?= lang('suppliername', 'suppliername') ?></div>
                    <div class="col-sm-7">
                        <?php echo form_input('suppliername', $supplier->company, 'class="form-control tip" id="suppliername" data-bv-notempty="true" data-bv-regexp="true" data-bv-regexp-regexp="^[a-zA-Z]+[-\s]?[a-zA-Z ]+$" data-bv-regexp-message="Allow Only Alphabets"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="control-label col-sm-5"><?= lang('supp_discount', 'supp_discount') ?></div>
                    <div class="col-sm-7">
                        <?php echo form_input('discount', set_value('discount', $supplier->discount), 'class="form-control tip" id="supp_discount" data-bv-notempty="true" data-bv-regexp="true" data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Supplier Discount can only consist of digits"'); ?>
                    </div>
                </div>

                <fieldset class="scheduler-border">
                    <legend class="scheduler-border"><?= lang('general') ?></legend>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <!--                        <div class="form-group">
                                                    <div class="col-sm-4 col-xs-4 col-lg-4 col-md-4" style="padding:0 !important"><?= lang('vat_no', 'vat_no') ?>  </div>
                                                    <div class="col-sm-8 col-xs-8 col-lg-8 col-md-8" style="padding-right: 5px">
                        <?php echo form_input('vat_no', $supplier->vat_no, 'class="form-control tip" id="vat_no" data-bv-notempty="true"'); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group" >
                                                    <div class="col-sm-4 col-xs-4 col-lg-4 col-md-4" style="padding:0 !important"><?= lang('tin_no', 'tin_no') ?> </div>
                                                    <div class="col-sm-8 col-xs-8 col-lg-8 col-md-8" style="padding-right: 5px">
                        <?php echo form_input('tin_no', $supplier->tinno, 'class="form-control tip" id="tin_no" data-bv-notempty="true"'); ?>
                                                    </div>
                                                </div>-->
                        <div class="form-group">
                            <div class="col-sm-4 col-xs-4 col-lg-4 col-md-4" style="padding:0 !important"><?= lang('gst_no', 'gst_no1') ?></div>
                            <div class="col-sm-8 col-xs-8 col-lg-8 col-md-8" style="padding-right: 5px">
                                <?php echo form_input('gstno', $supplier->gstno, 'class="form-control tip" id="gst_no" data-bv-notempty="false"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <div class=" col-sm-4 col-xs-4 col-lg-4 col-md-4" style="padding:0 !important""><?= lang('pan_no', 'pan_no') ?> </div>
                            <div class="col-sm-8 col-xs-8 col-lg-8 col-md-8" style="padding-right: 5px">
                                <?php echo form_input('pan_no', $supplier->panno, 'class="form-control tip" id="pan_no"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group"> 
                            <div class=" col-sm-4 col-xs-4 col-lg-4 col-md-4" style="padding:0 !important"><label><?= lang('igst') ?></label></div>
                            <div class="col-sm-8 col-xs-8 col-lg-8 col-md-8" style="padding-right: 5px">
                                <div class="checkbox">
                                    <input type="checkbox" name="igst" value="1" id="igst" <?php
                                    if ($supplier->igst == '1') {
                                        echo "checked";
                                    }
                                    ?>>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="col-sm-4 col-xs-4 col-lg-4 col-md-4" style="padding:0 !important"><?= lang('discount', 'discount') ?></div>
                                                <div class="col-sm-8 col-xs-8 col-lg-8 col-md-8" style="padding-right: 5px">
                    <?php echo form_input('discount', '', 'class="form-control tip" id="discounts" data-bv-notempty="true"'); ?>
                                                </div>
                                            </div>
                                        </div>-->
                </fieldset>


                <fieldset class="scheduler-border">
                    <legend class="scheduler-border"><?= lang('address') ?></legend>
                    <div class="form-group" style='padding:0 20px;'>
                        <?= lang('address1', 'address1') ?>
                        <?php echo form_input('address1', $supplier->address, 'class="form-control tip" id="address1" data-bv-notempty="true"'); ?>
                    </div>
                    <div class="form-group" style='padding:0 20px;'>
                        <?= lang('address2', 'address2') ?>
                        <?php echo form_input('address2', $supplier->address2, 'class="form-control tip" id="address2_optional"'); ?>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group" style='padding:0 8px;'>
                            <?= lang('city', 'city') ?> 
                            <?php echo form_input('city', $supplier->city, 'class="form-control tip" id="city" data-bv-notempty="true"'); ?>
                        </div>
                        <div class="form-group" style='padding:0 8px;'> 
                            <?= lang('country', 'country') ?>
                            <?php echo form_input('country', $supplier->country, 'class="form-control tip" id="country" data-bv-notempty="true"'); ?>
                        </div>
                        <!--Payment needs-->
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group" style='padding:0 8px;'>
                            <?= lang('state', 'state') ?>
                            <?php echo form_input('state', $supplier->state, 'class="form-control tip" id="state" data-bv-notempty="true"'); ?>
                        </div>
                        <div class="form-group" style='padding:0 8px;'>
                            <?= lang('zip_code', 'zip_code') ?>
                            <?php echo form_input('zip_code', $supplier->postal_code, 'class="form-control tip" id="zip_code" data-bv-notempty="true" data-bv-notempty-message="The Zip Code is required and cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="6"
                               data-bv-stringlength-max="6"
                               data-bv-stringlength-message="The Zip Code must be exact 6 numbers long"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The Zip Code can only consist of digits"'); ?>
                        </div>
                    </div>
                </fieldset>


                <!--<fieldset class="scheduler-border">
                    <legend class="scheduler-border">Bank Details</legend>
                    <div class="form-group">
                        <label class="control-label col-sm-5"><?= lang('bank_name', 'bank_name') ?></label>
                        <div class="col-sm-7">
                            <?php echo form_input('bank_name', $supplier->bank_name, 'class="form-control tip" id="bank_name" data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5"><?= lang('acc_no', 'acc_no') ?></label>
                        <div class="col-sm-7">
                            <?php echo form_input('acc_no', $supplier->acc_no, 'class="form-control tip" id="acc_no" data-bv-notempty="true"
                               data-bv-notempty-message="Required"
                               data-bv-stringlength="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="Only Numbers Allowed"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5"><?= lang('opening_balance', 'opening_balance') ?></label>
                        <div class="col-sm-7">
                            <?php echo form_input('opening_balance', $supplier->opening_balance, 'class="form-control tip" id="opening_balance" data-bv-notempty="true"
                               data-bv-notempty-message="Required"
                               data-bv-stringlength="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="Only Numbers Allowed"'); ?>
                            <?php // echo form_input('opening_balance', $supplier->opening_balance, 'class="form-control tip" id="opening_balance" data-bv-notempty="true"');   ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5"><?= lang('ifsc', 'ifsc') ?></label>
                        <div class="col-sm-7">
                            <?php echo form_input('neft', $supplier->neft, 'class="form-control tip" id="ifsc" data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-5"><?= lang('bank_branch', 'bank_branch') ?></label>
                        <div class="col-sm-7">
                            <?php echo form_input('bank_branch', $supplier->bank_branch, 'class="form-control tip" id="bank_branch" data-bv-notempty="true"'); ?>
                        </div>
                    </div>


                </fieldset>-->
                <?php
                $attachment = json_decode($supplier->attachments);
                ?>
                <?= lang('attachment', 'attachment') ?>
                <div calss="form-group">
                    <input id="attachment" name="attachment[]" multiple type="file" class="form-control" 
                           data-show-upload="false"
                           data-show-preview="false" 
                           data-bv-file-maxsize="300*100" 
                           />

                </div>
                <?php
                if (!empty($attachment)) {
                    foreach ($attachment as $row) {
                        ?>
                        <div class="row attachments form-group" style="border: 1px solid #fff; margin: 6px auto; background-color: #eaeaea;">
                            <div class="col-lg-10 col-sm-10 col-md-10 col-xs-10">
                                <input type="hidden" name='prefiles[]' class="form-control" value="<?= $row ?>"> 
                                <span class="col-lg-12 col-sm-12 col-md-12 col-xs-12"><?= $row ?></span>
                            </div>
                            <div class="col-lg-2 col-sm-2 col-md-2 col-xs-2">
                                <a href="" class='removeattachment'><i class="fa fa-close"></i></a>
                            </div>
                        </div>

                        <?php
                    }
                }
                ?>

            </div>
            <script>
                $(".removeattachment").click(function (event) {
                    event.preventDefault();
                    $(this).parents('.attachments').remove();
                });
            </script>


            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                    <button type="button" onclick="enableEdit()"><?= lang('define_my_own_Code') ?></button>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group"> 
                            <div class="checkbox" style="margin-left:12px !important">
                                <label><input type="checkbox" name="preferredsupplier" value="1" id="preferredsupplier" <?php
                                    if ($supplier->preferredsupplier == '1') {
                                        echo "checked";
                                    }
                                    ?>><?= lang('preferred_supplier') ?></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group"> 
                            <div class="checkbox" >
                                <label><input type="checkbox" name="isactive" value="1" id="isactive" <?php
                                    if ($supplier->status == 'Active') {
                                        echo "checked";
                                    }
                                    ?>><?= lang('is_active') ?></label>
                            </div>
                        </div>
                    </div>
                </div>
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border">Payment Terms</legend>

                    <label>Payment needs to be settled within <?php echo form_input('noofdays', $supplier->noofdays, 'class="tip" id="noofdays" data-bv-notempty="false"'); ?> number of days</label>	
                    <!--<label class="col-md-12">Payment needs to be settled within <input type="text" name="bank_branch" style="width:30px;" class="form-control"> number of days</label>-->	
                </fieldset>

                <fieldset class="scheduler-border">
                    <legend class="scheduler-border">Contact info</legend>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" >
                        <div class="form-group" style='padding-right:16px !important;'>
                            <?= lang('fname', 'fname') ?>
                            <?php
                            $name = explode(' ', $supplier->conactpname);
                            echo form_input('fname', $name[0], 'class="form-control tip" id="fname"');
                            ?>
                        </div>
                        <div class="form-group"  style='padding-right:16px !important;'> 
                            <?= lang('telephoneno', 'telephoneno_optional') ?>
                            <?php echo form_input('telephoneno', $supplier->telphoneno, 'class="form-control tip"   data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The Telephone No can only consist of digits"
                                data-bv-stringlength="true"
                               data-bv-stringlength-min="10"
                               data-bv-stringlength-max="10"
                               data-bv-stringlength-message="The Mobile no must be exact 10 numbers long"
                                id="telephoneno"'); ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <?= lang('lname', 'lname') ?>
                            <?php echo form_input('lname', $name[1], 'class="form-control tip" id="lname"'); ?>
                        </div>
                        <div class="form-group">
                            <?= lang('faxnumber', 'faxnumber_optional') ?>
                            <?php echo form_input('faxno', $supplier->faxno, 'class="form-control tip" id="faxnumber" pattern="[0-9]{10}" maxlength="10"'); ?>
                        </div>
                    </div>
                    <div class="form-group" style="padding:0 16px;">
                        <?= lang('mobileno', 'mobileno') ?>
                        <?php echo form_input('mobileno', $supplier->phone, 'class="form-control tip" id="mobileno" data-bv-notempty="true"data-bv-notempty="true"
                               data-bv-notempty-message="The Mobile no is required and cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="10"
                               data-bv-stringlength-max="10"
                               data-bv-stringlength-message="The Mobile no must be exact 10 numbers long"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The Mobile no can only consist of digits"'); ?>
                    </div>
                    <div class="form-group" style="padding:0 16px;">
                        <?= lang('website', 'website') ?>
                        <input type="url" name="website" value="<?= $supplier->website ?>" class="form-control" id="website"/>
                        <?php // echo form_input('website', '', 'class="form-control tip" id="website" data-bv-notempty="true"');      ?>
                    </div>
                    <div class="form-group" style="padding:0 16px;">
                        <?= lang('email', 'email') ?>
                        <input type="email" name="email" value="<?= $supplier->email ?>" class="form-control" id="email_address"/>
                        <?php // echo form_input('email', '', 'class="form-control tip" id="email" data-bv-notempty="true"');       ?>
                    </div>

                </fieldset>

                <fieldset class="scheduler-border">
                    <legend class="scheduler-border"><?= lang('refered_by') ?></legend>
                    <div class="controls"> 
                        <?php
                        $sup[""] = "";
                        foreach ($suppliers as $biller) {
                            $sup[$biller->id] = $biller->company;
                        }
                        
                        echo form_dropdown('refered_by', $sup, (isset($_POST['refered_by']) ? $_POST['refered_by'] : $supplier->refered_by), ' data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("refered_by") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                        ?>
                    </div>
                </fieldset>
            </div>


            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <?= lang("phtot", "biller_logo"); ?> *

                <input id="store_logo_up" name="photo" type="file" class="form-control" 
                       data-bv-notempty="false"
                       data-show-upload="false"
                       data-show-preview="false" 
                       data-bv-file-maxsize="300*100" 
                       />
                <br>
                <!--                <video id="video" width="100%" height="500px" style="border:1px solid #000000;background:#eee" autoplay></video>
                                <br><center><button>Tack Picture with<br> Attached Camera</button></center>-->

                <input type="hidden" name="supplierPhoto" id="supplierphoto" value="<?= set_value('supplierPhoto', $supplier->logo) ?>">
                <div id="my_camera" style="border:1px solid #000000;background:#eee; width: 100%; height: 500px; display: none;"  width="100%" height="500px"></div>
                <img src="assets/uploads/<?= $supplier->logo ?>" id="image_upload_preview" style="border:1px solid #000000;background:#eee" width="100%" height="500px">
                <div id="results"></div>
                <br><center><button type="button" id="takepicture" style="">Take Picture with Attached Camera</button>
                    <input type=button value="Take Snapshot" id="tacksnaps" onClick="take_snapshot()" style="display: none"></center>
            </div>


        </div>

        <div class="row">
            <div class="text-center">

                <?php echo form_submit('edit_supplier', lang('save'), 'class="btn btn-primary"'); ?>
                <a href="<?= site_url('suppliers') ?>" class="btn btn-danger">Cancel</a> 	
                <!--<button type="submit" class="btn btn-primary"><?php echo lang('save'); ?></button>--> 	
            </div>
            <?php echo form_close(); ?>
        </div><!--end row--->
    </div><!--end container-fluid-->
</div><!--end main-block--->

<script>
    $(function () {
        $('#store_logo_up').fileinput({
            maxImageWidth: 300,
            maxImageHeight: 100
        });
        $('#attachment').fileinput({
            maxImageWidth: 300,
            maxImageHeight: 100
        });
        $('#addstore').bootstrapValidator({
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            },
//            fields: {
//                logo: {
//                    validators: {
//                        file: {
//                            extension: 'jpeg,png',
//                            type: 'image/jpeg,image/png',
//                            maxSize: 300 * 100,
//                            message: 'Please check image size. it has to be 300X100asdasd Image size.'
//                        }
//                    }
//                }
//            }
        });
    });
//    function enableEdit() {
//        $('#supliercode').removeAttr('readonly');
//    }
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            $("#supplierphoto").val('');
        }
    }

    $("#store_logo_up").change(function () {
        readURL(this);
    });
    $("#takepicture").click(function () {
        $('#my_camera').css('display', 'inline-block');
        $('#tacksnaps').css('display', 'inline-block');
        $('#image_upload_preview').css('display', 'none');
        $('#takepicture').css('display', 'none');
    });
</script>



<script language="JavaScript">
    Webcam.set({
        width: 400,
        height: 330,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
    Webcam.attach('#my_camera');
</script>
<script language="JavaScript">
    function take_snapshot() {
        // take snapshot and get image data
        Webcam.snap(function (data_uri) {
            // display results in page

            document.getElementById('results').innerHTML =
//                    '<h2>Processing:</h2>';
                    readURL("input[name='webcam']");

            Webcam.upload(data_uri, 'suppliers/savecamImage', function (code, text) {
                $("#image_upload_preview").attr("src", 'assets/uploads/' + text);
                $("#supplierphoto").val(text);
                $('#my_camera').css('display', 'none');
                $('#image_upload_preview').css('display', 'inline-block');
                $('#takepicture').css('display', 'inline-block');
                $('#tacksnaps').css('display', 'none');
            });
        });
    }
</script>


<script>
        var autocomplete;

var componentForm = {
  locality: 'long_name', // city
  administrative_area_level_1: 'long_name', //state
  administrative_area_level_2: 'long_name', //state
  country: 'long_name', // country
  postal_code: 'short_name' //pincode
};
var options = {
  types: ['(cities)'],
    // types: ['geocode'],
    componentRestrictions: {country: "in"}
 };

initAutocomplete();

    function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
        document.getElementById('city'), options );
        // autocomplete.setFields(['address_component']);
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        var place = autocomplete.getPlace();
        console.log(place);
        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng();
        $("#auto_address_latitude").val(latitude);
        $("#auto_address_longitude").val(longitude);
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            var val = place.address_components[i][componentForm[addressType]];
            if (addressType == "administrative_area_level_2") {
                // console.log(addressType+" " +val);
                $("#city").val(val);

            }
            else if (addressType == "administrative_area_level_1") {
                // console.log(addressType+" " +val);
                $("#state").val(val);

            }
            else if (addressType == "postal_code") {
                // console.log(addressType+" " +val);
                $("#postal_code").val(val);

            }
            else if (addressType == "country") {
                // console.log(addressType+" " +val);
                $("#country").val(val);

            }
        }
    }

    </script>