<script type="text/javascript" src="<?= $assets ?>js/webcam.js"></script>
<div class="container-fluid" style="background-color: #ffff;padding: 10px">
    <?php
    $attrib = array('data-toggle' => 'validator', 'class' => 'form-horizontal', 'role' => 'form');
    echo form_open_multipart("suppliers/savesupplier", $attrib);
    ?>

    <div class="row">
        <!--<form class="form-horizontal" id="addSupplierform" method="POST" action="">-->

        <div class="col-lg-4 col-md-3 col-sm-4 col-xs-12">

            <div class="form-group">
                <div class="control-label col-sm-5">
                    <?= lang("supliercode", "supliercode"); ?>*</div>
                <div class="col-sm-7">
                    <input type="text" id="supliercode" value="<?= set_value('supliercode', ''); ?>" name="supliercode" readonly class="remove_readonly form-control"/>
                    <?php // echo form_input('supliercode', '', 'class="form-control tip" id="supliercode" disabled data-bv-notempty="true"'); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="control-label col-sm-5">
                    <?= lang("select_store", "select_store"); ?></div>
                <div class="col-sm-7">
                    <?php
                    $bl[""] = "";
//                    $bl[] = 'Select Store Name';
                    foreach ($store as $biller) {
                        $bl[$biller->id] = $biller->name;
                    }
                    echo form_dropdown('store_id', $bl, (isset($_POST['biller']) ? $_POST['biller'] : ''),  ' data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("store") . '" required data-bv-notempty class="form-control input-tip select" id="select_store"  style="width:100%;"');
                    ?>
                </div>
            </div>
            <div class="form-group">
                <div class="control-label col-sm-5"><?= lang('suppliername', 'suppliername') ?></div>
                <div class="col-sm-7">
                    <?php echo form_input('suppliername', set_value('suppliername', ''), 'class="form-control tip" id="suppliername" data-bv-notempty="true" data-bv-regexp="true" data-bv-regexp-regexp="^[a-zA-Z]+[-\s]?[a-zA-Z ]+$" data-bv-regexp-message="Allow Only Alphabets"'); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="control-label col-sm-5"><?= lang('supp_discount', 'supp_discount') ?></div>
                <div class="col-sm-7">
                    <?php echo form_input('discount', set_value('discount', ''), 'class="form-control tip" id="supplier_discount" data-bv-regexp="true" data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Supplier Discount can only consist of digits"'); ?>
                </div>
            </div>
            <!--                <div class="form-group">
                                <div class=" col-sm-4"><?= lang('searchcode', "searchcode") ?> </div>
                                <div class="col-sm-8">
            <?php echo form_input('searchcode', '', 'class="form-control tip" id="searchcode" data-bv-notempty="true"'); ?>
                                </div>
                            </div>				-->
            <fieldset class="scheduler-border">
                <legend class="scheduler-border"><?= lang('general') ?></legend>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <!--                    <div class="form-group">
                                            <div class="col-sm-4 col-xs-4 col-lg-4 col-md-4" style="padding:0 !important"><?= lang('vat_no', 'vat_no') ?>  </div>
                                            <div class="col-sm-8 col-xs-8 col-lg-8 col-md-8" style="padding-right: 5px">
                    <?php echo form_input('vat_no', set_value('vat_no', ''), 'class="form-control tip" id="vat_no" data-bv-notempty="true"
                               data-bv-notempty-message="Required"
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="11"
                               data-bv-stringlength-max="11"
                               data-bv-stringlength-message="Must be exact 11 numbers"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message=" "'); ?>
                                            </div>
                                        </div>-->
                    <!--                    <div class="form-group" >
                                            <div class="col-sm-4 col-xs-4 col-lg-4 col-md-4" style="padding:0 !important"><?= lang('tin_no', 'tin_no') ?> </div>
                                            <div class="col-sm-8 col-xs-8 col-lg-8 col-md-8" style="padding-right: 5px">
                    <?php echo form_input('tin_no', set_value('tin_no', ''), 'class="form-control tip" id="tin_no" data-bv-notempty="true"'); ?>
                                            </div>
                                        </div>-->
                    <div class="form-group">
                        <div class="col-sm-4 col-xs-4 col-lg-4 col-md-4" style="padding:0 !important"><?= lang('gst_no', 'gst_no1') ?></div>
                        <div class="col-sm-8 col-xs-8 col-lg-8 col-md-8" style="padding-right: 5px">
                             <?php echo form_input('gstno', set_value('gstno', ''), 'class="form-control tip" id="gst_no" data-bv-notempty="false"  data-bv-notempty-message="Required"'); ?>
                         </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <div class=" col-sm-4 col-xs-4 col-lg-4 col-md-4" style="padding:0 !important"><?= lang('pan_no', 'pan_no') ?> </div>
                        <div class="col-sm-8 col-xs-8 col-lg-8 col-md-8" style="padding-right: 5px">
                            <?php echo form_input('pan_no', set_value('pan_no', ''), 'class="form-control tip" id="panno" '); ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group"> 
                        <div class=" col-sm-4 col-xs-4 col-lg-4 col-md-4" style="padding:0 !important"><label><?= lang('igst') ?></label></div>
                        <div class="col-sm-8 col-xs-8 col-lg-8 col-md-8" style="padding-right: 5px">
                            <div class="checkbox">
                                <input type="checkbox" name="igst" value="1" id="igst">
                            </div>
                        </div>
                    </div>
                </div>


                <!--                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="col-sm-4 col-xs-4 col-lg-4 col-md-4" style="padding:0 !important"><?= lang('discount', 'discount') ?></div>
                                            <div class="col-sm-8 col-xs-8 col-lg-8 col-md-8" style="padding-right: 5px">
                <?php echo form_input('discount', '', 'class="form-control tip" id="discounts" data-bv-notempty="true"'); ?>
                                            </div>
                                        </div>
                                    </div>-->
            </fieldset>
            <fieldset class="scheduler-border">
                <legend class="scheduler-border"><?= lang('address') ?></legend>

                <div class="form-group" style='padding:0 20px;'>
                    <?= lang('address1', 'address1') ?>
                    <?php echo form_input('address1', set_value('address1', ''), 'class="form-control tip" id="address1" data-bv-notempty="true"'); ?>
                </div>
                <div class="form-group" style='padding:0 20px;'>
                    <?= lang('address2', 'address2') ?>
                    <?php echo form_input('address2', set_value('address2', ''), 'class="form-control tip" id="address2_optional"'); ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group" style='padding:0 8px;'>
                        <?= lang('city', 'city') ?> 
                        <?php echo form_input('city', set_value('city', ''), 'class="form-control tip" id="city" data-bv-notempty="true"'); ?>
                    </div>
                    <div class="form-group" style='padding:0 8px;'> 
                        <?= lang('country', 'country') ?>
                        <?php echo form_input('country', set_value('country', ''), 'class="form-control tip" id="country" data-bv-notempty="true"'); ?>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group" style='padding:0 8px;'>
                        <?= lang('state', 'state') ?>
                        <?php echo form_input('state', set_value('state', ''), 'class="form-control tip" id="state" data-bv-notempty="true"'); ?>
                    </div>
                    <div class="form-group" style='padding:0 8px;'>
                        <?= lang('zip_code', 'zip_code') ?>
                        <?php echo form_input('zip_code', set_value('zip_code', ''), 'class="form-control tip" id="zip_code" data-bv-notempty="true" data-bv-notempty-message="The Zip Code is required and cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="6"
                               data-bv-stringlength-max="6"
                               data-bv-stringlength-message="The Zip Code must be exact 6 numbers long"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The Zip Code can only consist of digits"'); ?>
                    </div>
                </div>
            </fieldset>
            <!--<fieldset class="scheduler-border">
                <legend class="scheduler-border">Bank Details</legend>
                <div class="form-group">
                    <label class="control-label col-sm-5"><?= lang('bank_name', 'bank_name') ?></label>
                    <div class="col-sm-7">
                        <?php echo form_input('bank_name', set_value('bank_name', ''), 'class="form-control tip" id="bank_name" data-bv-notempty="true"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5"><?= lang('acc_no', 'acc_no') ?></label>
                    <div class="col-sm-7">
                        <?php echo form_input('acc_no', set_value('acc_no', ''), 'class="form-control tip" id="acc_no" data-bv-notempty="true"
                               data-bv-notempty-message="Required"
                               data-bv-stringlength="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="Only Numbers Allowed"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5"><?= lang('opening_balance', 'opening_balance') ?></label>
                    <div class="col-sm-7">
                        <?php echo form_input('opening_balance', set_value('opening_balance', ''), 'class="form-control tip" id="opening_balance" data-bv-notempty="true"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    /AKASH- renaming neft to ifsc as clients requirment but not changing in
                    <label class="control-label col-sm-5"><?= lang('ifsc', 'ifsc') ?></label>
                    <div class="col-sm-7">
                        <?php echo form_input('neft', set_value('neft', ''), 'class="form-control tip" id="ifsc" data-bv-notempty="true"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-5"><?= lang('bank_branch', 'bank_branch') ?></label>
                    <div class="col-sm-7">
                        <?php echo form_input('bank_branch', set_value('bank_branch', ''), 'class="form-control tip" id="bank_branch" data-bv-notempty="true"'); ?>
                    </div>
                </div>
            </fieldset>-->
            <?= lang('attachment', 'attachment') ?>
            <div calss="form-group">
                <input id="attachment" name="attachment[]" multiple type="file" class="form-control" 
                       data-show-upload="false"
                       data-show-preview="false" 
                       data-bv-file-maxsize="300*100" 
                       />

            </div>
        </div> 

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <button type="button" onclick="enableEdit()"><?= lang('define_my_own_Code') ?></button>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group"> 
                        <div class="checkbox" style="margin-left:12px !important">
                            <label><input type="checkbox" name="preferredsupplier" value="1" id="preferredsupplier"><?= lang('preferred_supplier') ?></label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group"> 
                        <div class="checkbox" >
                            <label><input type="checkbox" name="isactive" value="1" id="isactive"><?= lang('is_active') ?></label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <fieldset class="scheduler-border">
                <legend class="scheduler-border">Payment Terms</legend>

                <p>Payment needs to be settled within 
                <?php //echo form_input('noofdays', set_value('noofdays', ''), 'class="form-control" data-bv-regexp="true" data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Payment terms requires no. of days"'); ?> number of days</p>	
            </fieldset> -->
            <fieldset class="scheduler-border">
            <legend class="scheduler-border">Payment Terms</legend>
                <div class="form-group">
                    <div class="control-label col-sm-12">
                    <?= lang('Payment needs to be settled within', 'noofdays') ?>
                    
                        <?php echo form_input('noofdays', set_value('noofdays', ''), 'placeholder="No.of days" class="form-control tip" id="noofdays" data-bv-notempty="true" data-bv-regexp="true" data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Allow Only numbers"'); ?>
                    </div>
                </div>
            </fieldset>

            <fieldset class="scheduler-border">
                <legend class="scheduler-border">Contact info</legend>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group" style='padding-right:16px !important;'>
                        <?= lang('fname', 'fname') ?>
                        <?php echo form_input('fname', set_value('fname', ''), 'class="form-control tip" id="fname"'); ?>
                    </div>
                    <div class="form-group"  style='padding-right:16px !important;'> 
                        <?= lang('telephoneno', 'telephoneno_optional') ?>
                        <?php
                        echo form_input('telephoneno', set_value('telephoneno', ''), 'class="form-control tip" id="telephoneno"'
                                . ' data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The Telephone No can only consist of digits"
                                data-bv-stringlength="true"
                               data-bv-stringlength-min="10"
                               data-bv-stringlength-max="10"
                               data-bv-stringlength-message="The Mobile no must be exact 10 numbers long"');
                        ?>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <?= lang('lname', 'lname') ?>
                        <?php echo form_input('lname', set_value('lname', ''), 'class="form-control tip"  id="lname"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang('faxnumber', 'faxnumber_optional') ?>
                        <?php echo form_input('faxnumber', set_value('faxnumber', ''), 'class="form-control tip" id="faxnumber" pattern="[0-9]{10}" maxlength="10"'); ?>
                    </div>
                </div>
                <div class="form-group" style="padding:0 16px;">
                    <?= lang('mobileno', 'mobileno') ?>
                    <?php echo form_input('mobileno', set_value('mobileno', ''), 'class="form-control tip" id="mobileno" data-bv-notempty="true"
                               data-bv-notempty-message="The Mobile no is required and cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="10"
                               data-bv-stringlength-max="10"
                               data-bv-stringlength-message="The Mobile no must be exact 10 numbers long"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The Mobile no can only consist of digits"'); ?>
                </div>
                <div class="form-group" style="padding:0 16px;">
                    <?= lang('website', 'website') ?>
                    <input type="url" name="website" value="<?= set_value('website', '') ?>" class="form-control" id="website"/>
                    <?php // echo form_input('website', '', 'class="form-control tip" id="website" data-bv-notempty="true"');   ?>
                </div>
                <div class="form-group" style="padding:0 16px;">
                    <?= lang('email', 'email') ?>
                    <input type="email" name="email" class="form-control" value="<?= set_value('email', '') ?>" id="email_address"/>
                    <?php // echo form_input('email', '', 'class="form-control tip" id="email" data-bv-notempty="true"');   ?>
                </div>
                <br><br><br>
            </fieldset>

            <fieldset class="scheduler-border">
                <legend class="scheduler-border"><?= lang('refered_by') ?></legend>
                <div class="controls"> 
                    <?php
                    $sup[""] = "";
                    foreach ($suppliers as $biller) {
                        $sup[$biller->id] = $biller->company;
                    }
                    echo form_dropdown('refered_by', $sup, (isset($_POST['biller']) ? $_POST['biller'] : $Settings->default_biller), ' data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("refered_by") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                    ?>
                </div>
            </fieldset>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <div class="form-group" style="padding:0 16px;">
                <?= lang("phtot", "biller_logo"); ?> *

                <input id="store_logo_up" name="photo" type="file" class="form-control" 
                       data-show-upload="false"
                       data-show-preview="false" 
                       data-bv-file-maxsize="300*100" 
                       />
            </div>
            <div class="form-group" style="padding:0 16px;">
                <input type="hidden" name="supplierPhoto" id="supplierphoto" value="<?= set_value('supplierPhoto', '') ?>">
                <div id="my_camera" style="border:1px solid #000000;background:#eee; width: 100%; height: 500px; display: none;"  width="100%" height="500px"></div>
                <img src="" id="image_upload_preview" style="border:1px solid #000000;background:#eee" width="100%" height="500px">
                <div id="results"></div>
            </div>
            <br><center><button type="button" id="takepicture" style="">Take Picture with Attached Camera</button>
                <input type=button value="Take Snapshot" id="tacksnaps" onClick="take_snapshot()" style="display: none"></center>
            <br> <br> <br> <br>
            <br> <br>        
        </div>
    </div>
    <div class="row">
        <div class="text-center">
            <?php echo form_submit('add_biller', lang('save'), 'class="btn btn-primary"'); ?>
            <a href="<?= site_url('suppliers') ?>" class="btn btn-danger">Cancel</a> 	
        </div>
    </div>
    <?php echo form_close(); ?>

</div>
<script>

    $(function () {
        $('#store_logo_up').fileinput({
            maxImageWidth: 300,
            maxImageHeight: 100
        });
        $('#attachment').fileinput({
            maxImageWidth: 300,
            maxImageHeight: 100
        });
        $('#addstore').bootstrapValidator({
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            },
//            fields: {
//                logo: {
//                    validators: {
//                        file: {
//                            extension: 'jpeg,png',
//                            type: 'image/jpeg,image/png',
//                            maxSize: 300 * 100,
//                            message: 'Please check image size. it has to be 300X100asdasd Image size.'
//                        }
//                    }
//                }
//            }
        });
    });

    $(document).ready(function () {
        $.ajax({
            type: "get",
            async: false,
            url: "<?= site_url('suppliers/getNewSupplierCode') ?>",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                        placeholder: "<?= lang('select_category_to_load') ?>",
                        data: data
                    });
                    $('#supliercode').val('SUP' + data.id);
                }
            },
            error: function () {
                bootbox.alert('<?= lang('ajax_error') ?>');
                $('#modal-loading').hide();
            }
        });
    });

//
//    function enableEdit() {
//        $('#supliercode').removeAttr('readonly');
//    }

// $('#supliercode').change(function(){
//      $.ajax({
//                    type: "get",
//                    async: false,
//                    url: "<?= site_url('suppliers/CheckCode') ?>/"+v,
//                    dataType: "json",
//                    success: function (data) {
//                        if (data != null) {
//                            $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
//                                placeholder: "<?= lang('select_category_to_load') ?>",
//                                data: data
//                            });
//                            $('#supliercode').val('SUP'+data.id);
//                        }
//                    },
//                    error: function () {
//                        bootbox.alert('<?= lang('ajax_error') ?>');
//                        $('#modal-loading').hide();
//                    }
//                });
// });

var autocomplete;
    
    var componentForm = {
      locality: 'long_name', // city
      administrative_area_level_1: 'long_name', //state
      administrative_area_level_2: 'long_name', //state
      country: 'long_name', // country
      Zip_Code: 'short_name' //pincode
    };
    var options = {
      types: ['(cities)'],
        // types: ['geocode'],
        componentRestrictions: {country: "in"}
     };
    
    initAutocomplete();
    
        function initAutocomplete() {
            autocomplete = new google.maps.places.Autocomplete(
            document.getElementById('city'), options );
            // autocomplete.setFields(['address_component']);
            autocomplete.addListener('place_changed', fillInAddress);
        }
    
        function fillInAddress() {
            var place = autocomplete.getPlace();
            console.log(place);
            var latitude = place.geometry.location.lat();
            var longitude = place.geometry.location.lng();
            $("#auto_address_latitude").val(latitude);
            $("#auto_address_longitude").val(longitude);
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                var val = place.address_components[i][componentForm[addressType]];
                if (addressType == "administrative_area_level_2") {
                    // console.log(addressType+" " +val);
                    $("#city").val(val);
    
                }
                else if (addressType == "administrative_area_level_1") {
                    // console.log(addressType+" " +val);
                    $("#state").val(val);
    
                }
                else if (addressType == "country") {
                    // console.log(addressType+" " +val);
                    $("#country").val(val);
                    
                }
                else if (addressType == "Zip_Code") {
                    // console.log(val);
                    $("#Zip_Code").val(val);
    
                }
            }
        }


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            $("#supplierphoto").val('');
        }
    }

    $("#store_logo_up").change(function () {
        readURL(this);
    });
    $("#takepicture").click(function () {
        $('#my_camera').css('display', 'inline-block');
        $('#tacksnaps').css('display', 'inline-block');
        $('#image_upload_preview').css('display', 'none');
        $('#takepicture').css('display', 'none');
    });
</script>
<script language="JavaScript">
/*     Webcam.set({
        width: 400,
        height: 330,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
    Webcam.attach('#my_camera');
 */</script>
<script language="JavaScript">
    function take_snapshot() {
        // take snapshot and get image data
        Webcam.snap(function (data_uri) {
            // display results in page

//            document.getElementById('results').innerHTML =
//                    '<h2>Processing:</h2>';
            readURL("input[name='webcam']");

            Webcam.upload(data_uri, 'suppliers/savecamImage', function (code, text) {
                $("#image_upload_preview").attr("src", 'assets/uploads/' + text);
                $("#supplierphoto").val(text);
                $('#my_camera').css('display', 'none');
                $('#image_upload_preview').css('display', 'inline-block');
                $('#takepicture').css('display', 'inline-block');
                $('#tacksnaps').css('display', 'none');
            });
        });
    }
</script>

<script>
        var autocomplete;

var componentForm = {
  locality: 'long_name', // city
  administrative_area_level_1: 'long_name', //state
  administrative_area_level_2: 'long_name', //state
  country: 'long_name', // country
  postal_code: 'short_name' //pincode
};
var options = {
  types: ['(cities)'],
    // types: ['geocode'],
    componentRestrictions: {country: "in"}
 };

initAutocomplete();

    function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
        document.getElementById('city'), options );
        // autocomplete.setFields(['address_component']);
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        var place = autocomplete.getPlace();
        console.log(place);
        var latitude = place.geometry.location.lat();
        var longitude = place.geometry.location.lng();
        $("#auto_address_latitude").val(latitude);
        $("#auto_address_longitude").val(longitude);
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            var val = place.address_components[i][componentForm[addressType]];
            if (addressType == "administrative_area_level_2") {
                // console.log(addressType+" " +val);
                $("#city").val(val);

            }
            else if (addressType == "administrative_area_level_1") {
                // console.log(addressType+" " +val);
                $("#state").val(val);

            }
            else if (addressType == "postal_code") {
                // console.log(addressType+" " +val);
                $("#postal_code").val(val);

            }
            else if (addressType == "country") {
                // console.log(addressType+" " +val);
                $("#country").val(val);

            }
        }
    }

    </script>