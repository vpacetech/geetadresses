 <style>
    fieldset.scheduler-border {
        border: 1px solid #ddd !important;
        padding: 0 1em 1em 1em !important;
        margin: 0 0 1.1em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
        box-shadow:  0px 0px 0px 0px #000;
    }


    legend.scheduler-border {
        width:inherit; /* Or auto */
        padding:0 10px; /* To give a bit of padding on the left and right */
        border-bottom:none;

    }
    .cal 
    {
        position:relative;
        top:-30px;
        left:90%;
    }
    /*
    a {
        color: #000 !important;    
            }*/
    .nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
        color: #6164c0 !important;
        background-color: #fff !important;
        border-radius:0 !important;
    }

    .nav-pills>li>a{
        color: #6164c0 !important;
        background-color: #e9ebec !important;
        border-radius:0 !important;
    }
    .tab-content>.active {
        display: block;
        border : 1px solid #ccc;
        padding:5px !important;
    }
    .btton
    {
        padding: 5px;
        width:100%;
    }

    .stepwizard-step {
        display: table-cell;
        /*text-align: center;*/
        position: relative;
    }
    .form-horizontal .control-label {
        padding-top: 0px;
    }
</style>
<!-- <script type="text/javascript" src="<?= $assets ?>js/webcam.js"></script> -->

<div id="exTab1" class="container-fluid" style="background-color: #ffff;padding: 10px" ng-controller="employee">	

    <!--    <ul  class="nav nav-pills">
            <li class="active"><a  href="#1a" data-toggle="tab">General Details</a></li>
            <li><a href="#2a" data-toggle="tab">User Fields</a></li>
            <li><a href="#3a" data-toggle="tab">Qualification</a></li>
            <li><a href="#4a" data-toggle="tab">Attachments</a></li>
            <li><a href="#login_details" data-toggle="tab">Login Credentials</a></li>
    
        </ul>-->

    <div class="stepwizard">
        <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step">
                <a href="#step-1" type="button"  class="btn btn-primary ">supplier info</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-2" type="button" class="btn btn-default " disabled="disabled">General</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-3" type="button" class="btn btn-default " disabled="disabled">Address</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-4" type="button" class="btn btn-default " disabled="disabled">Bank Details</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-5" type="button" class="btn btn-default " disabled="disabled">Payment Terms</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-5" type="button" class="btn btn-default " disabled="disabled">Contact info</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-5" type="button" class="btn btn-default " disabled="disabled">Refered By</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-5" type="button" class="btn btn-default " disabled="disabled">Photo</a>
            </div>


        </div>
    </div>



    <div class="tab-pane" style="border: 1px solid #000; padding: 10px">
        <div class="row setup-content" id="step-1">

        <form id="supplierinfo_form" class="form-horizontal" action="<?php echo site_url().'/suppliers/savenewsupplier_supplierinfo'?>" enctype="multipart/form-data" data-toggle="validator" method="post">
      

      <br>
      <div class="form-group">
          <label class="col-sm-3 control-label" for="Supplier_Code">Supplier Code</label>
          <div class="col-sm-5">
              <input type="text" class="form-control" readonly id="Supplier_Code" name="Supplier_Code" data-bv-notempty="true" />
          </div>
      </div>          


      <br>
      <br>
      <!-- <br>
      <br> -->
      <button type="button" id="code" class="btn btn-primary" onclick="edit()">Define my own code</button>

      <div class="form-group" id="check">
          <input id="Preferred_Suppliers" data-type='res' name='Preferred_Supplier' type='checkbox'>&emsp;
          <label for="Preferred_Suppliers">Preferred Suppliers&emsp;</label>&emsp;
          <input id="Is_Active" data-type='com' name='Is_Active' type='checkbox'>&emsp;
          <label for="Is_Active">Is Active&emsp;</label>
      </div>          

   <br>
   <br>
   <br>
   <br>

      <div class="form-group">
          <label class="col-sm-3 control-label" for="Select_Store">Select Store *</label>
          <div class="col-sm-5">
            <select class="selectpicker" id="Select_Store" name="Store" style="width: 500px" data-bv-notempty="true">
            <option value="" selected option disabled>select store</option>
                <?php 
                foreach ($store as $biller) {?>
                    
                    <option value="<?php echo $biller->names?>" id="<?php echo $biller->id ?> "><?php echo $biller->name ?></option>        
                <?php        
                }?>
      
          </select>             
       </div>
      </div>          


   <br>   


      <div class="form-group">
          <label class="col-sm-3 control-label" for="Supplier_Name">Supplier Name</label>
          <div class="col-sm-5">
              <input type="text"  class="form-control"  id="Supplier_Name" name="Supplier_Name" data-bv-notempty="true" />
          </div>
      </div>          

   <br>        

      <div class="form-group">
          <label class="col-sm-3 control-label" for="Supplier_Discount1">Supplier Discount(%)</label>
          <div class="col-sm-5">
              <input type="text" class="form-control"  id="Supplier_Discount" name="Supplier_Discount"  />
          </div>
      </div>          


   <br>

      <div class="form-group">

          <label class="col-sm-3 control-label" for="aadhar_card">aadhar card</label>
               <div class="col-sm-5">
                  <input id="attachment1" name="aadhar"  type="file" class="form-control" 
                      data-show-upload="false"
                      data-show-preview="false" 
                      data-bv-file-maxsize="300*100" 
                      />
               </div>
      </div>

      <br>

      <div class="form-group">

          <label class="col-sm-3 control-label" for="Pan">Pan card</label>
              <div class="col-sm-5">
                  <input id="attachment2" name="pan"  type="file" class="form-control" 
                  data-show-upload="false"
                  data-show-preview="false" 
                  data-bv-file-maxsize="300*100" 
                  />
              </div>
      </div>

   <br>
   
    <div align="center">
      <button type="submit" class="btn btn-primary">save</button>
      <button type="button" class="btn btn-danger">cancel</button>
    </div>
</form>
            <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>

        </div>
        <div class="row setup-content" id="step-2">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-4 col-md-4"> 
                        <div class="form-group">
                            <label class="control-label col-sm-5"><?= lang('dojoin', 'dojoin') ?></label>
                            <div class="col-sm-7">
                                <?php echo form_input('dojoin', set_value('dojoin', ''), 'class="form-control tip date" id="dojoin"  data-bv-notempty="true" required="required"'); ?><i class="fa fa-calendar cal"></i>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5"><?= lang('location', 'location') ?></label>
                            <div class="col-sm-7">
                                <?php
                                $us = "";
                                $us[''] = "Select " . lang('location');
                                foreach ($store as $usr) {
                                    $us[$usr->id] = $usr->name;
                                }
                                echo form_dropdown('location', $us, '', 'class="form-control select" id="location" data-bv-notempty="true" required="required" placeholder="' . lang("select") . " " . lang("location") . '"')
                                ?>

                                <?php
//                            echo form_dropdown('against_ref_no',$get_ref_no,(isset($_POST['against_ref_no']) ? $_POST['against_ref_no'] : $voucher->against_ref_no),'data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" class="form-control input-tip select" id="ref" style="width:100%;"');
                                ?>


                            </div>

                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5"><?= lang('department', 'department') ?></label>
                            <div class="col-sm-7">
                                <?php
                                $us = "";
//                            foreach ($department as $usr) {
//                                $us[$usr->id] = $usr->name;
//                            }
                                echo form_dropdown('department', $us, (isset($_POST['department']) ? $_POST['department'] : $us->name), 'data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("department") . '" class="form-control input-tip select" id="department" style="width:100%;" data-bv-notempty="true" required="required"');
//                            echo form_dropdown('department', $us, '', 'class="form-control select" id="department" placeholder="' . lang("select") . " " . lang("department") . '"')
                                ?>
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-5"><?= lang('section', 'section') ?></label>
                            <div class="col-sm-7">
                                <?php
                                $us = "";
                                echo form_dropdown('section', $us, (isset($_POST['section']) ? $_POST['section'] : $us->name), 'data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("section") . '" class="form-control input-tip select" data-bv-notempty="true" required="required" id="section" style="width:100%;"');
                                ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <!--<label class="control-label col-sm-4"><?= lang('desigation', 'desigation') ?></label>-->
                            <label class="control-label col-sm-5"><?= lang('position', 'position') ?></label>
                            <div class="col-sm-7">
                                <?php
                                $us = "";
                                $us[''] = "Select " . lang('position');
                                foreach ($position as $usr) {
                                    $us[$usr->id] = $usr->name;
                                }

                                echo form_dropdown('desigation', $us, '', 'class="form-control select" data-bv-notempty="true" required="required" id="position" placeholder="' . lang("select") . " " . lang("username") . '"')
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5"><?= lang('shift', 'shift') ?></label>
                            <div class="col-sm-7">
                                <?php
                                $us = array(
                                    '' => 'Select Shift',
                                    'Morning' => 'Morning',
                                    'Evening' => 'Evening'
                                );
                                echo form_dropdown('shift', $us, '', 'class="form-control select" id="shift" placeholder="' . lang("select") . " " . lang("username") . '"')
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6"> 
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Bank Details</legend>
                            <!--                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">-->
                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('bank_name', 'bank_name1') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('bank_name', set_value('bank_name', ''), 'class="form-control tip"  id="bank_name" data-bv-notempty="false"'); ?>
            <!--                              <input type="text" name="bank_name" class="form-control">-->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('acc_no', 'acc_no1') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('acc_no', set_value('acc_no', ''), 'class="form-control tip"  id="acc_no" data-bv-notempty="false"
                               data-bv-notempty-message="Required"
                               data-bv-stringlength="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="Only Numbers Allowed"'); ?>
                                  <!--<input type="text" name="acc_no" class="form-control">-->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('ifsc', 'ifsc1') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('neft', set_value('neft', ''), 'class="form-control tip" id="ifsc" data-bv-notempty="false"'); ?>
                                  <!--<input type="text" name="neft" class="form-control">-->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('bank_branch', 'bank_branch1') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('bank_branch', set_value('bank_branch', ''), 'class="form-control  tip"   id="bank_branch" data-bv-notempty="false"'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('opening_balance', 'opening_balance1') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('opening_balance', set_value('opening_balance', ''), 'class="form-control tip" id="opening_balance" data-bv-notempty="false"'); ?>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
            </div>
        </div>
        <div class="row setup-content" id="step-3">

            <div class="col-md-12">
                <div class="row">
                    <div class="qualification">
                        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                    <label for="qualific">Qualification </label>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                                    <label for="yr">Year of Pasing</label>
                                </div>
                            </div>
                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

                                    <input type="text" name="quali[]" class="form-control"  id="quali" placeholder="Qualification">
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

                                    <input type="text" name="yr[]" class="form-control year" id="yr" placeholder="Year of Pasing">
                                </div>
                                <label class="col-lg-2 col-sm-2 col-md-2 col-xs-12 " onclick="addQualifications()">
                                    <i class="fa fa-plus-circle fa-2x"></i></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                        <div id="training">
                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">
                                <label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12">Special Training:</label>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">
                                    <input type="text" name="special_training[]" class="form-control"  id="special_training" placeholder="Tally - Accounting Software" style="margin-bottom:10px;">
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                    <i class="fa fa-2x fa-plus-circle" onclick="addTraining()"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
            </div>

        </div>
        <div class="row setup-content" id="step-4">
            <div class="">
                <div class="col-md-12">
                    <div class="row">
                        <!--                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                            <h4 style="text-">Documents</h4>
                                        </div>-->
                        <div id="document">
                            <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
                                <label class="control-label" for="document" style="font-weight:bold;">Identify Proof -</label>
                            </div>
                            <div class="col-lg-10 col-sm-10 col-md-10 col-xs-12 ">
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group "  ng-repeat="attach in attachment">
                                    <div class="col-lg-5 col-sm-5 col-md-5 col-xs-5">
                                        <input type="text" name="docname[]" class="form-control" id="quali" placeholder="Document Name">
                                    </div>
                                    <div class="col-lg-5 col-sm-5 col-md-5 col-xs-5">

                                        <input id="documentfile1" name="document[]" multiple type="file"  class="" 
                                               data-show-upload="false"
                                               data-show-preview="false" 
                                               data-bv-file-maxsize="300*100" 
                                               />
                                    </div>
                                    <div class="col-lg-1 col-sm-1 col-md-1 col-xs-2">
                                        <button type="button" ng-if="$index < attachment.length - 1" class="btn btn-default" ng-click="attachment.splice($index, 1);" href="">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                        <a href="" ng-if="$index == attachment.length - 1" ng-click='attachment.push({identityprof: ""});' class="btn btn-default">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div id="references">
                            <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
                                <label class="control-label" for="document" style="font-weight:bold;">References -</label>
                            </div>
                            <div class="col-lg-10 col-sm-10 col-md-10 col-xs-12 ">
                                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group "  ng-repeat="ref in references">
                                    <div class="col-lg-5 col-sm-5 col-md-5 col-xs-5">
                                        <input type="text" name="refname[]" class="form-control" id="quali" placeholder="Reference Name">
                                    </div>
                                    <div class="col-lg-5 col-sm-5 col-md-5 col-xs-5">
                                        <input id="referencesfile1" name="references[]" type="file" multiple class="" data-show-upload="false" data-show-preview="false" data-bv-file-maxsize="300*100" />
                                    </div>
                                    <div class="col-lg-1 col-sm-1 col-md-1 col-xs-2">
                                        <button type="button" ng-if="$index < references.length - 1" class="btn btn-default" ng-click="references.splice($index, 1);" href="">
                                            <i class="fa fa-minus"></i>
                                        </button>
                                        <a href="" ng-if="$index == references.length - 1" ng-click='references.push({referencesname: ""});' class="btn btn-default">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>

                                </div>
                            </div>


                            <!--                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                                                            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">
                                                                <label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" style="font-weight:bold;" for="referencesfile">References -</label>
                                                                <div class="col-lg-7 col-sm-7 col-md-7 col-xs-12" style="margin-bottom:10px;">
                                                                    <input id="referencesfile" name="references[]" type="file" multiple class="form-control referencesfile" data-show-upload="false" data-show-preview="false" data-bv-file-maxsize="300*100" />
                                                                </div>
                                                                <label class="col-lg-7 col-sm-7 col-md-7 col-xs-12">Note : Select one or more file</label>
                                                               
                                                            </div>
                                                        </div>-->
                        </div>
                    </div>
                    <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                    <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                </div>
            </div>
        </div>

        <div class="row setup-content" id="step-5">
            <div class="">
                <div class="col-md-12">
                    <fieldset class="scheduler-border" style="padding: 0px 25px !important;">
                        <div class="row">
                            <legend class="scheduler-border">Login Info</legend>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?php echo lang('password', 'password'); ?>
                                    <?php echo form_password('password', '', 'class="form-control tip" id="password" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"'); ?>
                                    <span class="help-block">At least 1 capital, 1 lowercase, 1 number and more than 8 characters long</span>
                                </div>
                            </div>
                            <div class="col-md-1"></div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?php echo lang('confirm_password', 'password_confirm'); ?>
                                    <?php echo form_password('password_confirm', '', 'class="form-control" id="password_confirm" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-bv-identical="true" data-bv-identical-field="password" data-bv-identical-message="' . lang('pw_not_same') . '"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row text-center">
                            <span class="text-center" style="font-weight: bold">Note :- User name will be same as your email id</span>
                        </div>
                    </fieldset>
                    <button class="btn btn-primary prevBtn  pull-left" type="button">Previous</button>
                    <!--<button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>-->
                    <?php echo form_submit('add_employee', lang('save'), 'class="btn btn-primary pull-right"'); ?>
                </div>
            </div>
        </div>
    </div>

    <!--end tab-content----->
    <br>
    <!--<div class="row text-center">-->
    <?php // echo form_submit('add_employee', lang('save'), 'class="btn btn-primary"'); ?>
        <!--<a class="btn btn-danger" href="<?= site_url('employee'); ?>" onclick="this.form.reset();">Cancel</a>-->
    <!--</div>-->
    <?php echo form_close(); ?>
</div>





<script>
    function isValid(value)
{
  var fieldNum = /^[a-z]+$/i;

  if ((value.match(fieldNum))) {
      return true;
  }
  else
  {
      return false;
  }

}

function isempty(value)
{
  alert(value);
  if ( value == "" ) {
      return false;
  }
  else
  {
      return true;
  }

}

function check_mob(value)
{
//   var fieldNum = /^[a-z]+$/i;
    // console.log(digits_count(value));
// alert(value);
    len = value.length;

    if( len == 10 )
    {
        return true;
        // alert("tr");
    }
    else
    {
        return false;
        // alert("el");
    }

}

function check_zip(value)
{
//   var fieldNum = /^[a-z]+$/i;
    // console.log(digits_count(value));

    len = value.length;

    if( len == 6 )
    {
        return true;
        // alert("tr");
    }
    else
    {
        return false;
        // alert("el");
    }

}

   /*  $(function()
    {
        $('#attachment1').fileinput({
            maxImageWidth: 300,
            maxImageHeight: 100
        });
        $('#attachment2').fileinput({
            maxImageWidth: 300,
            maxImageHeight: 100
        });

    }
 */    $(document).ready(function () {
        var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn'),
                allPrevBtn = $('.prevBtn');

        allWells.hide();

        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                    $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-primary').addClass('btn-default');
                $item.addClass('btn-primary');
                allWells.hide();
                $target.show();
//          $target.find('input:eq(0)').focus();
            }
        });

        allPrevBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

            prevStepWizard.removeAttr('disabled').trigger('click');
        });

        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url'],input[type='email'],fieldset > input[type='text']"),
                    isValid = true;

            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid)
                nextStepWizard.removeAttr('disabled').trigger('click');
        });

        $('div.setup-panel div a.btn-primary').trigger('click');
    });
</script>
<script>

    $(document).on('ifUnchecked', '#hlsda', function (e) {
        $('#msd1').attr('disabled', "disabled");
        $('#msd1').attr('required', 'required');
    });
    $(document).on('ifChecked', '#hlsda', function (e) {
        $('#msd1').removeAttr("disabled");
        $('#msd1').removeAttr('required');
    });

    $(document).on('ifUnchecked', '#isloyatlity', function (e) {
        $('#loyalityno').attr('disabled', "disabled");
        $('#enrolldate').attr('disabled', "disabled");
        $('#loyalitypoint').attr('disabled', "disabled");
        $('#referby').attr('disabled', "disabled");
        $('#loyalityno').attr('required', 'required');
        $('#enrolldate').attr('required', 'required');
        $('#loyalitypoint').attr('required', 'required');
        $('#referby').attr('required', 'required');

    });
    $(document).on('ifChecked', '#isloyatlity', function (e) {
        $('#loyalityno').removeAttr("disabled");
        $('#enrolldate').removeAttr("disabled");
        $('#loyalitypoint').removeAttr("disabled");
        $('#referby').removeAttr("disabled");
        $('#creaditlimit').removeAttr('required');
        $('#enrolldate').removeAttr('required');
        $('#loyalitypoint').removeAttr('required');
        $('#referby').removeAttr('required');
    });

    $('#store_logo_up').fileinput({
        maxImageWidth: 300,
        maxImageHeight: 100
    });
    $('#attachment').fileinput({
        maxImageWidth: 300,
        maxImageHeight: 100
    });
    $('#referencesfile').fileinput({
        maxImageWidth: 300,
        maxImageHeight: 100
    });

    $('#document').fileinput({
        maxImageWidth: 300,
        maxImageHeight: 100
    });
    $('#documentfile').fileinput({
        maxImageWidth: 300,
        maxImageHeight: 100
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_upload_preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            $("#supplierphoto").val('');
        }
    }

    $("#store_logo_up").change(function () {
        readURL(this);
    });
    $("#takepicture").click(function () {
        $('#my_camera').css('display', 'inline-block');
        $('#tacksnaps').css('display', 'inline-block');
        $('#image_upload_preview').css('display', 'none');
        $('#takepicture').css('display', 'none');
    });
    $("#dobclear").click(function () {

        $('#dob').val('');
    });
</script>

<script language="JavaScript">
/*     Webcam.set({
        width: 400,
        height: 330,
        image_format: 'jpeg',
        jpeg_quality: 90
    });
    Webcam.attach('#my_camera');
 */</script>
<script language="JavaScript">
    function take_snapshot() {
        // take snapshot and get image data
        Webcam.snap(function (data_uri) {
            readURL("input[name='webcam']");
            Webcam.upload(data_uri, 'suppliers/savecamImage', function (code, text) {
                $("#image_upload_preview").attr("src", 'assets/uploads/' + text);
                $("#supplierphoto").val(text);
                $('#my_camera').css('display', 'none');
                $('#image_upload_preview').css('display', 'inline-block');
                $('#takepicture').css('display', 'inline-block');
                $('#tacksnaps').css('display', 'none');
            });
        });
    }
</script>

<script>
    
    function addQualifications() {
        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">' +
                '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group">' +
                '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">' +
                '<input type="text" name="quali[]" class="form-control" id="email" placeholder="B.C.A">' +
                '</div>' +
                '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">' +
                '<input type="text" name="yr[]" class="form-control date" id="email" placeholder="Year of Pasing">' +
                '</div>' +
                '<label class="col-lg-2 col-sm-2 col-md-2 col-xs-12" for="email" onclick="addQualifications()"><i class="fa fa-plus-circle fa-2x"></i></label>' +
                '</div>' +
                '</div>';
        $('.qualification').append(data);
    }

    function addTraining() {
        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">' +
                '<label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="email">Special Training:</label>' +
                '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-10">' +
                '<input type="text" name="special_training[]" class="form-control" id="email" placeholder="Tally - Accounting Software" style="margin-bottom:10px;">' +
                '</div>' +
                '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">' +
                '<i class="fa fa-2x fa-plus-circle" onclick="addTraining()"></i>' +
                '</div>' +
                '</div>';
        $('#training').append(data);
    }
    function addExperience() {
        var data = '<tr>' +
                '<td><input type="text"  class="form-control" id="email"  name="year[]" placeholder="Year"></td>' +
                '<td><input type="text" class="form-control" id="email" name="company[]" placeholder="Company"></td>' +
                '<td><input type="text" class="form-control" id="email" name="position[]" placeholder="Position"></td>' +
                '<td><input type="text" class="form-control" id="email" name="reference[]" placeholder="Reference"></td>' +
                '<td><i class="fa fa-2x fa-plus-circle" onclick="addExperience()"></i></td>' +
                ' </tr>';
        $('#experiance').append(data);
    }
    function addDocument() {
        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">' +
                '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">' +
                '<label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="email" style="font-weight:bold;">Identify Proof -</label>' +
                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">' +
                '<input type="name" class="form-control" id="email" placeholder="0000000000">' +
                '</div>' +
                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">' +
                '<label class="btn btn-default btn-file" style="margin:0;">' +
                'Browse <input type="file" name="document[]" style="display: none;">' +
                '</label>' +
                ' </div>' +
                '<div class="col-lg-2 col-sm-2 col-md-2 col-xs-6">' +
                ' <i class="fa fa-2x fa-plus-circle" onclick="addDocument()"></i>' +
                ' </div>' +
                '</div>' +
                '</div>';
        $('#document').append(data);
    }

    function addReferences() {
        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">' +
                '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">' +
                '<label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="email" style="font-weight:bold;">References -</label>' +
                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="margin-bottom:10px;">' +
                '<input id="referencesfile" name="references[]" type="file"  class="form-control referencesfile" data-show-upload="false" data-show-preview="false" data-bv-file-maxsize="300*100" />' +
                '</div>' +
                '<div class="col-lg-2 col-sm-2 col-md-2 col-xs-6">' +
                '<i class="fa fa-2x fa-plus-circle" onclick="addReferences()"></i>' +
                ' </div>' +
                '</div>' +
                '</div>';
        $('#references').append(data);
    }
//    function addReferences() {
//        var data = '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">' +
//                '<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 form-group ">' +
//                '<label class="control-label col-lg-2 col-sm-2 col-md-2 col-xs-12" for="email" style="font-weight:bold;">References -</label>' +
//                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">' +
//                ' <input type="name" class="form-control" id="email" placeholder="0000000000" style="margin-bottom:10px;">' +
//                ' </div>' +
//                '<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12" style="margin-bottom:10px;">' +
//                '<label class="btn btn-default btn-file" style="margin:0;">' +
//                '  Browse <input type="file" name="references[]" style="display: none;">' +
//                '  </label>' +
//                '</div>' +
//                '<div class="col-lg-2 col-sm-2 col-md-2 col-xs-6">' +
//                '<i class="fa fa-2x fa-plus-circle" onclick="addReferences()"></i>' +
//                ' </div>' +
//                '</div>' +
//                '</div>';
//        $('#references').append(data);
//    }

    $('#location').change(function (event) {
        var store = $('#location').val();
        $.ajax({
            type: "get", async: false,
            url: "<?= site_url('billers/getDepartment') ?>",
            data: {'store': store},
            dataType: "json",
            success: function (data) {
                $('#department').html('');
                if (data == '') {
//                    bootbox.alert('<?= lang('department_is_not_available') ?>');
                } else {
                    $.each(data, function (k, val) {
                        var opt = $('<option />');
                        opt.val(val.id);
                        opt.text(val.name);
                        $('#department').append(opt);
                    })
                }
            }
        });
    });


    $('#department').change(function (event) {
        var section = $(this).val();
        $.ajax({
            type: "get", async: false,
            url: "<?= site_url('billers/getsection') ?>",
            data: {'section': section},
            dataType: "json",
            success: function (data) {
                $('#section').html('');
                if (data == '') {
//                    bootbox.alert('<?= lang('department_is_not_available') ?>');
                } else {
                    $.each(data, function (k, val) {
                        var opt = $('<option />');
                        opt.val(val.id);
                        opt.text(val.name);
                        $('#section').append(opt);
                    })
                }
            }
        });
    });

    $('#email').blur(function (event) {
        var email = $(this).val();
        $.ajax({
            type: "get",
            async: false,
            url: "<?= site_url('employee/checkuserEmail') ?>",
            data: {'email': email},
            dataType: "json",
            success: function (data) {
                if (data == '1') {
                    $('#email').val('');
                    bootbox.alert('Email Already Exists. Try Another One');
                }
            }
        });
    });

    $(document).ready(function(){

$('#supplierinfo_form').bootstrapValidator({
feedbackIcons: {
    valid: 'fa fa-check',
    invalid: 'fa fa-times',
    validating: 'fa fa-refresh'
},
fields: {

    Select_Store: {
        validators: {
             notEmpty: {
                message: 'The Select_Store is required'
            }
            /* ,

            callback: {
                message: 'please enter only letters',
                callback: function(value, validator, $field) {
                    if( value == "" )
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            } */
        }
    },
    Supplier_Code: {
        validators: {
            notEmpty: {
                message: 'The Supplier_Code is required'
            }
        }
    },
    Supplier_Name: {
        validators: {
            notEmpty: {
                message: 'The Supplier_Name is required'
            },
            callback: {
                message: 'please enter only letters',
                callback: function(value, validator, $field) {
                    if (!isValid(value)) {
                      return {
                        valid: false,
                      };
                    }
                    else
                    {
                      return {
                        valid: true,
                      };    
                    }

                }
            }
        }
    },


    Supplier_Discount: {
        validators: {
/*                     notEmpty: {
                message: 'The Supplier_Discount is required'
            },
*/                    integer: {
                message: 'The value is not an integer'
            },
            between: {
                min: 0,
                max: 100,
                message: 'The Supplier_Discount must be between 0 and 100'
            }
        }
    },
   /*  'attachment[]': {
        validators: {
            file: {
                extension: 'jpeg,png',
                type: 'image/jpeg,image/png,application/pdf,application/zip,application/x-rar-compressed,application/vnd.ms-powerpoint,text/plain,application/vnd.ms-excel,application/msword,image/tiff,image/gif',
                maxSize: 2048 * 1024,
                message: 'The selected file is not valid'
            }
        }
    }  */
    aadhar: {
        validators: {
            file: {
                extension: 'jpeg,png',
                type: 'image/jpeg,image/png,application/pdf,application/zip,application/x-rar-compressed,application/vnd.ms-powerpoint,text/plain,application/vnd.ms-excel,application/msword,image/tiff,image/gif',
                maxSize: 2048 * 1024,
                message: 'The selected file is not valid'
            }
        }
    },
    pan: {
        validators: {
            file: {
                extension: 'jpeg,png',
                type: 'image/jpeg,image/png,application/pdf,application/zip,application/x-rar-compressed,application/vnd.ms-powerpoint,text/plain,application/vnd.ms-excel,application/msword,image/tiff,image/gif',
                maxSize: 2048 * 1024,
                message: 'The selected file is not valid'
            }
        }
    } 

}

});
});

</script>

