<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-users"></i><?= lang('add_bank'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <?php
            $attrib = array('class' => 'form-horizontal', 'data-toggle' => 'validator', 'role' => 'form');
            echo form_open("bank/Add", $attrib);
            ?>
            <div class="col-lg-12">
                <!--<p class="introtext"><?php // echo lang('add_bank');      ?></p>-->


                <div class="row billing">
                    <div class="col-md-12">
                        <div class="col-md-5">
                            <div class="form-group">
                                <div class="col-md-6">
                                <?php echo lang('bank_code', 'bank_code'); ?>
                                </div>
                                <div class="col-md-6 text-right pd-0">
                                    <button type="button" onclick="enableEdits()"><?= lang('define_my_own_code') ?></button>
                                </div>
                                <div class="controls">
                                    <?php
                                    echo form_input('bank_code', set_value('bank_code', 'B' . $bank_code), 'class="form-control remove_readonly" id="bank_c" readonly');
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('bank_name', 'bank_name'); ?>
                                <div class="controls">
                                    <?php
                                    echo form_input('bank_name', set_value('bank_name', ''), 'class="form-control" id="bank_name" required="required"'
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="Please write your Bank Name"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[a-z\s,A-z]+$"
                               data-bv-regexp-message="Name must contain characters only"');
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('ifsc_code', 'ifsc_code'); ?>
                                <div class="controls">
                                    <?php echo form_input('ifsc_code', set_value('ifsc_code', ''), 'class="form-control" id="ifsc_code" required="required" maxlength="20"'); ?>

                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('address_1', 'address_1'); ?>
                                <div class="controls">
                                    <input type="text" value="<?php echo set_value('address_1', '') ?>" id="address_1" name="address_1" class="form-control" required="required"/>

                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('address_2', 'address_2'); ?>
                                <div class="controls">
                                    <input type="text" value="<?php echo set_value('address_2', '') ?>" id="address_2" name="address_2" class="form-control"/>

                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('city', 'city'); ?>
                                <div class="controls">
                                    <?php
                                    echo form_input('city', set_value('city', ''), 'class="form-control" id="city" required="required" maxlength="30"'
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="Please write your city name"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[a-z\s,A-z]+$"
                               data-bv-regexp-message="city must contain characters only"');
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('state', 'state'); ?>
                                <div class="controls">
                                    <?php
                                    echo form_input('state', set_value('state', ''), 'class="form-control" id="state" required="required" maxlength="30"'
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="Please write your state name"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[a-z\s,A-z]+$"
                               data-bv-regexp-message="state must contain characters only"');
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('country', 'country'); ?>
                                <div class="controls">
                                    <?php
                                    echo form_input('country', set_value('country', ''), 'class="form-control" id="country" required="required" maxlength="30"'
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="Please write your country name"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[a-z\s,A-z]+$"
                               data-bv-regexp-message="country name must contain characters only"');
                                    ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-5 col-md-offset-1">
                            <div class="form-group">
                                <?php echo lang('pin_code', 'pin_code'); ?>
                                <div class="controls">
                                    <?php
                                    echo form_input('pin_code', set_value('pin_code', ''), 'class="form-control" id="pin_code" required="required" maxlength="6"'
                                            . 'data-bv-notempty="true"
                               data-bv-notempty-message="Pin code is required and cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="6"
                               data-bv-stringlength-max="6"
                               data-bv-stringlength-message="The Pin code must be exact 6 numbers long"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The Pin code can only consist of digits"');
                                    ?>
                                </div>
                            </div>  

                            <div class="form-group">
                                <?php echo lang('email', 'email'); ?>
                                <div class="controls">
                                    <input type="email" value="<?php echo set_value('email', '') ?>" id="email" name="email" class="form-control" required="required"/>
                                    <span id="error"></span>

                                </div>
                            </div>
                            <div class="form-group">
                                <?php echo lang('mobile', 'mobile'); ?>
                                <?php
                                echo form_input('mobile', set_value('mobile', ''), 'class="form-control" id="mobile" required="required" maxlength="10"'
                                        . 'data-bv-notempty="true"
                               data-bv-notempty-message="The Mobile no is required and cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="10"
                               data-bv-stringlength-max="10"
                               data-bv-stringlength-message="The Mobile no must be exact 10 numbers long"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The Mobile no can only consist of digits"');
                                ?>
                            </div>

                            <div class="form-group">
                                <?php echo lang('phone', 'phone'); ?>
                                <div class="controls">
                                    <?php
                                    echo form_input('phone', set_value('phone', ''), 'class="form-control" id="phone" maxlength="15"'
                                            . 'data-bv-notempty-message="Phone no is required and cannot be empty"
                               data-bv-stringlength="true"
                               data-bv-stringlength-min="10"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The Phone no can only consist of digits"');
                                    ?>
                                </div>
                            </div>




                            <div class="form-group">

                                <?php echo lang('fax', 'fax'); ?>
                                <div class="controls">
                                    <?php
                                    echo form_input('fax', set_value('fax', ''), 'class="form-control" id="fax_no" maxlength="15"'
                                            . 'data-bv-stringlength-message="The fax no must be exact 10 numbers long"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$"
                               data-bv-regexp-message="The fax no can only consist of digits"');
                                    ?>
                                </div>
                            </div>


                            <div class="form-group">
                                <?php echo lang('web_site', 'web_site'); ?>
                                <div class="controls">
                                    <input type="url" value="<?php echo set_value('web_site', '') ?>" id="address" name="web_site" class="form-control" placeholder="Ex: http://inncrotech.site "/>
                                    <span id="error"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <?= lang('status', 'statuss'); ?>
                                <?php
                                $opt = array('Active' => lang('active'), 'Deactive' => lang('inactive'));
                                echo form_dropdown('status', $opt, (isset($_POST['status']) ? $_POST['status'] : 'Active'), 'id="status" data-placeholder="' . lang("select") . ' ' . lang("status") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>  
            <p class="text-center"><?php echo form_submit('add_bank', "Save", 'class="btn btn-primary"'); ?></p>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#group').change(function (event) {
            var group = $(this).val();
            if (group == 1 || group == 2) {
                $('.no').slideUp();
                $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'biller');
                $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'warehouse');
            } else {
                $('.no').slideDown();
                $('form[data-toggle="validator"]').bootstrapValidator('addField', 'biller');
                $('form[data-toggle="validator"]').bootstrapValidator('addField', 'warehouse');
            }
        });

        $('#email').change(function (event) {
            var emails = $('#email').val();
            $.ajax({
                type: "get", async: false,
                url: "<?= site_url('auth/check_email') ?>/",
                data: {'email': emails},
                dataType: "json",
                success: function (data) {
                    if (data == 1) {
                        $('#email').addClass('has-error');
                        $('#error').addClass('label label-danger');
                        $('#error').html('Email Already Exists');
                        setTimeout(function () {
                            $('#error').html('');
                        }, 4000);
                        $('#email').val('');
                    } else {
                    }
                }
            });
        });
        $('#username').change(function (event) {
            var username = $('#username').val();
            $.ajax({
                type: "get", async: false,
                url: "<?= site_url('auth/check_username') ?>/",
                data: {'username': username},
                dataType: "json",
                success: function (data) {
                    if (data == 1) {
                        $('#username').addClass('has-error');
                        $('#error_uname').addClass('label label-danger');
                        $('#error_uname').html('User Name Already Exists');
                        setTimeout(function () {
                            $('#error_uname').html('');
                        }, 4000);
                        $('#username').val('');
                    } else {
                    }
                }
            });
        });



    });
    
    function enableEdits() {
        $("#bank_c").removeAttr('readonly');
    }
</script>