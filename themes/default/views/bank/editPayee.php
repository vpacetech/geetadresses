<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-users"></i><?= lang('edit_payee'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">

            <?php
            $attrib = array('class' => 'form-horizontal', 'data-toggle' => 'validator', 'role' => 'form');
            echo form_open("bank/update_payee/".$ed_payee->id, $attrib);
            ?>
            <div class="col-lg-12">
                <!--<p class="introtext"><?php // echo lang('add_bank');       ?></p>-->


                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-5">
                            <div class="form-group">
                                <?php echo lang('select_store', 'select_store'); ?>
                                <div class="controls">
                                    <?php
                                    $bl = "";
                                    foreach ($store as $row) {
                                        $bl[$row->id] = $row->name;
                                    }
                                    echo form_dropdown('store_id', $bl, set_value('store_id', $ed_payee->store_id), 'id="qubiller" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("store_name") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('bank_name', 'ifsc_code'); ?>
                                <div class="controls">
                                    <?php
                                    $b = "";
                                    foreach ($bank as $row) {
                                        $b[$row->id] = $row->bank_name;
                                    }
                                    echo form_dropdown('bank_id', $b, set_value('bank_id', $ed_payee->bank_id), 'id="bank_id" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("bank_name") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <?php echo lang('ifsc_code', 'ifsc_code'); ?>
                                <div class="controls">
                                    <?php
                                    $b = "";
                                    $b[''] = 'Select IFSC Code';
                                    foreach ($ifsc as $row) {
                                        $b[$row->ifsc_code] = $row->ifsc_code;
                                    }
                                    echo form_dropdown('ifsc_code', $b, set_value('ifsc_code', $ed_payee->ifsc_code), 'id="ifsc_codes" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("receiver") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-5 col-md-offset-1">
                                                        <div class="form-group">
                                <?php echo lang('opening_balance', 'opening_balance'); ?>
                                <div class="controls">
                                    <input type="text" value="<?php echo set_value('opening_balance', $ed_payee->opening_balance) ?>" id="opening_balance" name="opening_balance" class="form-control" required="required"/>
                                </div>
                            </div>
                                                        <div class="form-group">
                                <?php echo lang('account_no', 'account_no'); ?>
                                <div class="controls">
                                    <input type="text" value="<?php echo set_value('account_no', $ed_payee->account_no) ?>" id="account_no" name="account_no" class="form-control" required="required"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <?= lang('status', 'statuss'); ?>
                                <?php
                                $opt = array('Active' => lang('active'), 'Deactive' => lang('inactive'));
                                echo form_dropdown('status', $opt, (isset($_POST['status']) ? $_POST['status'] : $ed_payee->status), 'id="status" data-placeholder="' . lang("select") . ' ' . lang("status") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
           
            <p class="text-center"><?php echo form_submit('add_bank', "Save", 'class="btn btn-primary"'); ?></p>
            <?php echo form_close(); ?>
        </div>
         </div>  
    </div>


<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#group').change(function (event) {
            var group = $(this).val();
            if (group == 1 || group == 2) {
                $('.no').slideUp();
                $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'biller');
                $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'warehouse');
            } else {
                $('.no').slideDown();
                $('form[data-toggle="validator"]').bootstrapValidator('addField', 'biller');
                $('form[data-toggle="validator"]').bootstrapValidator('addField', 'warehouse');
            }
        });

        $('#email').change(function (event) {
            var emails = $('#email').val();
            $.ajax({
                type: "get", async: false,
                url: "<?= site_url('auth/check_email') ?>/",
                data: {'email': emails},
                dataType: "json",
                success: function (data) {
                    if (data == 1) {
                        $('#email').addClass('has-error');
                        $('#error').addClass('label label-danger');
                        $('#error').html('Email Already Exists');
                        setTimeout(function () {
                            $('#error').html('');
                        }, 4000);
                        $('#email').val('');
                    } else {
                    }
                }
            });
        });
        $('#username').change(function (event) {
            var username = $('#username').val();
            $.ajax({
                type: "get", async: false,
                url: "<?= site_url('auth/check_username') ?>/",
                data: {'username': username},
                dataType: "json",
                success: function (data) {
                    if (data == 1) {
                        $('#username').addClass('has-error');
                        $('#error_uname').addClass('label label-danger');
                        $('#error_uname').html('User Name Already Exists');
                        setTimeout(function () {
                            $('#error_uname').html('');
                        }, 4000);
                        $('#username').val('');
                    } else {
                    }
                }
            });
        });
        
        $('#bank_id').change(function (event) {
            var bankid = $('#bank_id').val();
            $.ajax({
                type: "get", async: false,
                url: "<?= site_url('bank/getifsc') ?>",
                data: {'bankid': bankid},
                dataType: "json",
                success: function (data) {
                    $('#ifsc_codes').html('');
                     $('#ifsc_codes').select2('val', "");
                    //$('#ifsc_codes').html('');
                    $('#ifsc_codes').append('<option value="">Select IFSC Code</option>');
                    if (data != "") {
                        $.each(data, function (k, val) {
                            var opt = $('<option />');
                            opt.val(val.ifsc_code);
                            opt.text(val.ifsc_code);
                            $('#ifsc_codes').append(opt);
                        })
                    } else {
                    }
                }
            });
        });


    });
</script>

