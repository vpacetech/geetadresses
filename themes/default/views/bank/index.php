<script>
    var oTable = ['bank'];
    $(document).ready(function () {
        oTable['bank'] = $('#TOData').dataTable({
//        var oTable = $('#TOData').dataTable({
            "aaSorting": [[1, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('bank/getBank') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [{
                    "bSortable": false,
                    "mRender": checkbox
                }, null, {"bSortable": false}, {"bSortable": false}, {"bSortable": false},{"bSortable": false},{"bSortable": false},{"bSortable": false},{"bSortable": false},{"bSortable": false}, {"mRender": setStatus}, {"bSortable": false}],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                $('td:eq(10) > label', nRow).attr('onclick', "changeStatus('id'," + aData[0] + ",'bank')");
            },
        })
    });
</script>
<?php
if ($Owner) {
//    echo form_open('transfers/transfer_actions', 'id="action-form"');
    echo form_open('bank/bank_actions', 'id="action-form"');
}
?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa fa-bank"></i><?= lang('bank'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right" class="tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li><a href="<?= site_url('bank/add') ?>"><i class="fa fa-plus-circle"></i> <?= lang('add_bank') ?></a></li>
                        <li><a href="#" id="excel" data-action="export_excel"><i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?></a></li>
                        <li><a href="#" id="pdf" data-action="export_pdf"><i class="fa fa-file-pdf-o"></i> <?= lang('export_to_pdf') ?></a></li>
                        <li class="divider"></li>
                        <li><a href="#" class="bpo" title="<b><?= $this->lang->line("delete_bank") ?></b>"
                               data-content="<p><?= lang('r_u_sure') ?></p><button type='button' class='btn btn-danger' id='delete' data-action='delete'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button>"
                               data-html="true" data-placement="left">
                                <i class="fa fa-trash-o"></i> <?= lang('delete_bank') ?></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?= lang('list_results'); ?></p>
                <div class="table-responsive">
                    <table id="TOData" cellpadding="0" cellspacing="0" border="0"
                           class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                            <tr class="active">
                                <th style="min-width:30px; width: 30px; text-align: center;">
                                    <input class="checkbox checkft" type="checkbox" name="check"/>
                                </th>
                                <th><?= lang("bank_code"); ?></th>
                                <th><?= lang("bank_name"); ?></th>
                                <th><?= lang("ifsc"); ?></th>
                                <th><?= lang("address_1") ?></th>
                                <!--<th><?= lang("address_2"); ?></th>-->
                                <th><?= lang("city"); ?></th>
                                <th><?= lang("state"); ?></th>
                                <!--<th><?= lang("country"); ?></th>-->
                                <th><?= lang("pin_code"); ?></th>
                                <!--<th><?= lang("phone") ?></th>-->
                                <th><?= lang("mobile"); ?></th>
                                <!--<th><?= lang("fax"); ?></th>-->
                                <th><?= lang("email") ?></th>
                                <!--<th><?= lang("web_site") ?></th>-->
                                <th><?= lang("status"); ?></th>
                                <th style="width:100px;"><?= lang("actions"); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="10" class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                            </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                            <tr class="active">
                                <th style="min-width:30px; width: 30px; text-align: center;">
                                    <input class="checkbox checkft" type="checkbox" name="check"/>
                                </th>
                                 <th><?= lang("bank_code"); ?></th>
                                <th><?= lang("bank_name"); ?></th>
                                <th><?= lang("ifsc"); ?></th>
                                <th><?= lang("address_1") ?></th>
                                <!--<th><?= lang("address_2"); ?></th>-->
                                <th><?= lang("city"); ?></th>
                                <th><?= lang("state"); ?></th>
                                <!--<th><?= lang("country"); ?></th>-->
                                <th><?= lang("pin_code"); ?></th>
                                <!--<th><?= lang("phone") ?></th>-->
                                <th><?= lang("mobile"); ?></th>
                                <!--<th><?= lang("fax"); ?></th>-->
                                <th><?= lang("email") ?></th>
                                <!--<th><?= lang("web_site") ?></th>-->
                                <th><?= lang("status"); ?></th>
                                <th style="width:100px;"><?= lang("actions"); ?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>