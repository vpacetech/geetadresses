<style>
    fieldset.scheduler-border {
        border: 1px solid #ddd !important;
        padding: 0 1em 1em 1em !important;
        margin: 0 0 1.1em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
        box-shadow:  0px 0px 0px 0px #000;
    }


    legend.scheduler-border {
        width:inherit; /* Or auto */
        padding:0 10px; /* To give a bit of padding on the left and right */
        border-bottom:none;

    }
    .cal 
    {
        position:relative;
        top:-30px;
        left:90%;
    }
    /*
    a {
        color: #000 !important;    
            }*/
    .nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
        color: #6164c0 !important;
        background-color: #fff !important;
        border-radius:0 !important;
    }

    .nav-pills>li>a{
        color: #6164c0 !important;
        background-color: #e9ebec !important;
        border-radius:0 !important;
    }
    .tab-content>.active {
        display: block;
        border : 1px solid #ccc;
        padding:5px !important;
    }
    .btton
    {
        padding: 5px;
        width:100%;
    }

    .stepwizard-step {
        display: table-cell;
        /*text-align: center;*/
        position: relative;
    }
    .form-horizontal .control-label {
        padding-top: 0px;
    }
</style>

<div id="exTab1" class="container-fluid" style="background-color: #ffff;padding: 10px">
    <div class="stepwizard">
        <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step">
                <a href="#step-1" type="button"  class="btn btn-primary ">User Banks</a>
            </div>
            <div class="stepwizard-step">
                <a href="#step-2" type="button" class="btn btn-default ">Add New Bank</a>
            </div>
        </div>
    </div>

    <div class="tab-content">
        <div id="step-1" class="row setup-content">
            <table id="bankstable" class="table">
            <thead>
                <tr>
                    <th>Sr.no</th>
                    <th>Bank Name</th>
                    <th>Branch</th>
                    <th>IFSC</th>
                    <th>Acc. No.</th>
                    <th>Is Primary Acc.</th>
                </tr>
            </thead>

            <tbody>
            <?php 
            $i = 1;
            foreach($banks as $bank): ?>
                <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $bank->name; ?></td>
                    <td><?= $bank->branch; ?></td>
                    <td><?= $bank->ifsc; ?></td>
                    <td><?= $bank->account_number; ?></td>
                    <td><?= $bank->isprimary; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
            </table>
        </div>

        <div class="row setup-content" id="step-2">
            
            <div class="col-md-12">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <fieldset class="scheduler-border">
                        <?php
                $attrib = array('data-toggle' => 'validator', 'class' => 'form-horizontal', 'role' => 'form');
                echo form_open("suppliers/savesupplierbank", $attrib);
                ?>
                            <legend class="scheduler-border">Bank Account Details</legend>
                            
                            <?php echo form_hidden('user_id', set_value('user_id', $uid), 'class="form-control tip"  id="user_id" data-bv-notempty="true"'); ?>

                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('bank_name', 'bank_name') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('bank_name', set_value('bank_name', ''), 'class="form-control tip"  id="bank_name" data-bv-notempty="true"'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('acc_no', 'acc_no') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('acc_no', set_value('acc_no', ''), 'class="form-control tip"  id="acc_no" data-bv-notempty="true"
                                data-bv-notempty-message="Required"
                                data-bv-stringlength="true"
                                data-bv-regexp="true"
                                data-bv-regexp-regexp="^[0-9]+$"
                                data-bv-regexp-message="Only Numbers Allowed"'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('ifsc', 'ifsc') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('ifsc', set_value('ifsc', ''), 'class="form-control tip" id="ifsc" data-bv-notempty="true"'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('bank_branch', 'bank_branch') ?></label>
                                <div class="col-sm-7">
                                    <?php echo form_input('bank_branch', set_value('bank_branch', ''), 'class="form-control tip"   id="bank_branch" data-bv-notempty="true"'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-sm-5"><?= lang('is_primary (set default)', 'is_primary') ?></label>
                                <div class="col-sm-7">
                                    <?php 
                                    $dat = ['name'=>'isprimary','value'=> 1, 'checked' => false, 'id'=> 'is_primary',"class"=>"form-control  tip"];
                                    echo form_checkbox($dat);
                                    // echo form_checkbox('isprimary', set_value('isprimary', ''), 'checked="false" class="form-control tip" id="is_primary" data-bv-notempty="false"'); ?>
                                </div>
                            </div>
                            <?php echo form_submit('add_bank', lang('save'), 'class="btn btn-primary pull-right"'); ?>
                            <?php echo form_close(); ?>
                            
                        </fieldset>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {

        $("#bankstable").DataTable();
        var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn'),
                allPrevBtn = $('.prevBtn');

        allWells.hide();

        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                    $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-primary').addClass('btn-default');
                $item.addClass('btn-primary');
                allWells.hide();
                $target.show();
//          $target.find('input:eq(0)').focus();
            }
        });

        allPrevBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

            prevStepWizard.removeAttr('disabled').trigger('click');
        });

        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url'],input[type='email'],fieldset > input[type='text']"),
                    isValid = true;

            $(".form-group").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".form-group").addClass("has-error");
                }
            }

            if (isValid)
                nextStepWizard.removeAttr('disabled').trigger('click');
        });

        $('div.setup-panel div a.btn-primary').trigger('click');
    });
</script>