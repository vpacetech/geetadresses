<style>.btton{padding: 5px;width:100%;}
    .form-group {margin-bottom: 6px !important;}
</style>

<div class="box" ng-controller="">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('edit_gst'); ?></h2>
    </div>
    <div class="box-content">
        <?php
        $attrib = array('data-toggle' => 'validator', 'class' => 'form-horizontal', 'role' => 'form');
        echo form_open_multipart("gst/edit/" . $gst->id, $attrib);
        ?>
        <div class="row">
            <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                        <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="store">1. Store</label>
                        <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                            <?php
                            $soters = array();
                            $soters[''] = "Select Sotre";
                            foreach ($store as $row) {
                                $soters[$row->id] = $row->name;
                            }
                            echo form_dropdown('store', $soters, $gst->store_id, 'class="form-control select2 commonselect" data-bv-notempty="true" data-tab="department" data-key="store_id" data-id="department" id="store" placeholder="' . lang("select") . ' ' . lang("store") . '"')
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                        <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="product_item">2. Department</label>
                        <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                            <?php
                            $soters = array();
                            $soters[''] = "Select Department";
                            foreach ($departments as $row) {
                                $soters[$row->id] = $row->name;
                            }
                            echo form_dropdown('department', $soters, $gst->department, 'class="form-control select2 commonselect" data-bv-notempty="true" data-tab="section" data-key="department_id" data-id="section" id="department" id="department" placeholder="' . lang("select") . ' ' . lang("department") . '"')
                            ?>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                        <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="product_item">3. Section</label>
                        <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                            <?php
                            $soters[''] = "Select section";
                            foreach ($sections as $row) {
                                $soters[$row->id] = $row->name;
                            }
                            echo form_dropdown('section', $soters, $gst->section, 'class="form-control select2 commonselect" data-bv-notempty="true" data-tab="product_items" data-key="section_id" data-id="product_item" id="section" placeholder="' . lang("select") . ' ' . lang("department") . '"')
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                        <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="product_item">4. Product Item</label>
                        <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                            <?php
                            $soters = array();
                            $soters[''] = "Select Product Item";
                            foreach ($pro_item as $row) {
                                $soters[$row->id] = $row->name;
                            }
                            echo form_dropdown('product_item', $soters, $gst->product_items, 'class="form-control select2 commonselect" data-bv-notempty="true" data-tab="type" data-key="product_items_id" data-id="type" id="product_item" placeholder="' . lang("select") . ' ' . lang("product_item") . '"')
                            ?>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                        <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="type">5. Type</label>
                        <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                            <?php
                            $soters = array();
                            $soters[''] = "Select Type";
                            foreach ($type as $row) {
                                $soters[$row->id] = $row->name;
                            }
                            echo form_dropdown('type', $soters, $gst->type_id, 'class="form-control select2 commonselect" data-bv-notempty="true" data-tab="brands" data-key="type_id" data-id="brand" id="type" placeholder="' . lang("select") . ' ' . lang("type") . '"')
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                        <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="brand">6. Brand</label>
                        <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                            <?php
                            $soters = array();
                            $soters[''] = "Select Brand";
                            foreach ($brand as $row) {
                                $soters[$row->id] = $row->name;
                            }
                            echo form_dropdown('brand', $soters, $gst->brands, 'class="form-control select2 commonselect" data-tab="design" data-key="brands_id" data-id="design" data-bv-notempty="true" id="brand" placeholder="' . lang("select") . ' ' . lang("brand") . '"')
                            ?>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                        <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="design">7. Design</label>
                        <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                            <?php
                            $soters = array();
                            $soters[''] = "Select Design";
                            foreach ($design as $row) {
                                $soters[$row->id] = $row->name;
                            }
                            echo form_dropdown('design', $soters, $gst->design, 'class="form-control select2 commonselect" data-tab="style" data-key="design_id" data-id="style" data-bv-notempty="true" id="design" placeholder="' . lang("select") . ' ' . lang("brand") . '"')
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                        <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">8. Style</label>
                        <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                            <?php
                            $soters = array();
                            $soters[''] = "Select Style";
                            foreach ($style as $row) {
                                $soters[$row->id] = $row->name;
                            }
                            echo form_dropdown('style', $soters, $gst->style, 'class="form-control select2 commonselect" data-tab="pattern" data-key="style_id" data-id="pattern" data-bv-notempty="false" id="style" placeholder="' . lang("select") . ' ' . lang("brand") . '"')
                            ?>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                        <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">9. Pattern</label>
                        <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                            <?php
                            $soters = array();
                            $soters[''] = "Select Pattern";
                            foreach ($pattern as $row) {
                                $soters[$row->id] = $row->name;
                            }
                            echo form_dropdown('pattern', $soters, $gst->pattern, 'class="form-control select2 commonselect" data-tab="fitting" data-key="pattern_id" data-id="fitting" data-bv-notempty="false" id="pattern" placeholder="' . lang("select") . ' ' . lang("brand") . '"')
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                        <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">10. Fitting</label>
                        <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                            <?php
                            $soters = array();
                            $soters[''] = "Select Fitting";
                            foreach ($fitting as $row) {
                                $soters[$row->id] = $row->name;
                            }
                            echo form_dropdown('fitting', $soters, $gst->fitting, 'class="form-control select2 commonselect" data-tab="fabric" data-key="fitting_id" data-id="fabric" data-bv-notempty="false" id="fitting" placeholder="' . lang("select") . ' ' . lang("brand") . '"')
                            ?>
                        </div>
                    </div>

                    
                </div>
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                        <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">11. Fabric</label>
                        <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                            <?php
                            $soters = array();
                            $soters[''] = "Select Fabric";
                            foreach ($fabric as $row) {
                                $soters[$row->id] = $row->name;
                            }
                            echo form_dropdown('fabric', $soters, $gst->fabric, 'class="form-control select2 " data-tab="design" data-key="brands_id" data-id="design" data-bv-notempty="false" id="fabric" placeholder="' . lang("select") . ' ' . lang("brand") . '"')
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                        <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">12. Color</label>
                        <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                            <?php
                            $color = array();
                            $color[''] = 'Select Color';
                            foreach ($product_para['color'] as $row) {
                                $color[$row->id] = $row->name;
                            }
                            echo form_dropdown('color', $color, $gst->color, 'class="form-control select2 "  data-bv-notempty="false" id="color" placeholder="' . lang("select") . ' ' . lang("brand") . '"')
                            ?>
                        </div>
                    </div>
                    
                </div>
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                        <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">13. Size</label>
                        <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                            <?php
                            $soters = array();
                            $soters[''] = "Select Size";
                            foreach ($size as $row) {
                                $soters[$row->id] = $row->name;
                            }
                            echo form_dropdown('size', $soters, $gst->size, 'class="form-control select2" data-bv-notempty="false" id="size" placeholder="' . lang("select") . ' ' . lang("size") . '"')
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                        <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">14. MRP</label>
                        <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                            <?php
                            echo form_input('mrp', $gst->mrp, 'class="form-control" data-bv-notempty="false"  data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]\d*(\.\d+)?$"
                               data-bv-regexp-message="Only Numbers Allowed" id="fitting" placeholder="MRP"')
                            ?>
                        </div>
                    </div>                    
                </div>
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                        <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">15. MRP - From</label>
                        <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                            <?php
                            echo form_input('mrpfrom', $gst->mrpfrom, 'class="form-control"  data-bv-notempty="false"  data-bv-regexp="false"
                               data-bv-regexp-regexp="^[0-9]\d*(\.\d+)?$"
                               data-bv-regexp-message="Only Numbers Allowed" id="fitting" placeholder="MRP - From"')
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                        <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">MRP - Up to</label>
                        <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                            <?php
                            echo form_input('mrpupto', $gst->mrpto, 'class="form-control " data-bv-notempty="false"  data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]\d*(\.\d+)?$"
                               data-bv-regexp-message="Only Numbers Allowed" id="fitting" placeholder="MRP - Up to"')
                            ?>
                        </div>
                        <!--<label class="col-md-2 col-lg-2 col-xs-2 col-sm-2 ">%</label>-->
                    </div>
                    
                </div>
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                        <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="cgst">16. CGST</label>
                        <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                            <?php
                            echo form_input('cgst', $gst->cgst, 'class="form-control "  data-bv-notempty="true"  data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]\d*(\.\d+)?$"
                               data-bv-regexp-message="Only Numbers Allowed" id="cgst" placeholder="CGST"')
                            ?>
                        </div>
                        <label class="col-md-2 col-lg-2 col-xs-2 col-sm-2 ">%</label>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                        <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="sgst">17. SGST</label>
                        <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                            <?php
                            echo form_input('sgst', $gst->sgst, 'class="form-control "  data-bv-notempty="true"  data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]\d*(\.\d+)?$"
                               data-bv-regexp-message="Only Numbers Allowed" id="sgst" placeholder="SGST"')
                            ?>
                        </div>
                        <label class="col-md-2 col-lg-2 col-xs-2 col-sm-2 ">%</label>
                    </div>
                    
                </div>
                <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                        <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="hsncode">18. HSN Code</label>
                        <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                            <?php
                            echo form_input('hsncode', $gst->hsncode, 'class="form-control " data-bv-notempty="true"   id="hsncode" placeholder="HSN Code"')
                            ?>
                        </div>
                        <!--<label class="col-md-2 col-lg-2 col-xs-2 col-sm-2 ">%</label>-->
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                        <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="cess">19. Cess</label>
                        <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                            <?php
                            echo form_input('cess', $gst->cess, 'class="form-control " data-bv-notempty="true"  data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]\d*(\.\d+)?$"
                               data-bv-regexp-message="Only Numbers Allowed" id="cess" placeholder="Cess"')
                            ?>
                        </div>
                        <label class="col-md-2 col-lg-2 col-xs-2 col-sm-2 ">%</label>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                        <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="addupgst">20. Add up GST</label>
                        <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                            <?php
                            echo form_input('addupgst', $gst->addupgst, 'class="form-control  " data-bv-notempty="true" data-bv-stringlength="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]\d*(\.\d+)?$"
                               data-bv-regexp-message="Only Numbers Allowed" id="addupgst" placeholder="Add up GST"')
                            ?>
                        </div>
                        <label class="col-md-2 col-lg-2 col-xs-2 col-sm-2 ">%</label>
                    </div>
                </div>

            </div>


        </div>
        <div class="row text-center">
            <?php echo form_submit('add_gst', lang('save'), 'class="btn btn-primary"'); ?>
            <!--<button type="button" class="btn btn-danger"  onclick="this.form.reset();">Cancel</button>-->  	
            <a href="<?php echo base_url('gst') ?>"><button type="button" class="btn btn-danger">Cancel</button>  	
        </div>
        <?php echo form_close(); ?>
    </div>
</div>



<script>

    $('#department, #product_item').change(function () {

        var department = $('#department').val();
        var pro_item = $('#product_item').val();

        if (department != "" && pro_item != "") {
            $.ajax({
                async: false,
                data: {
                    department: department,
                    pro_item: pro_item
                },
                type: 'GET',
                url: site.base_url + "common/getSizebyid",
                dataType: "json",
                success: function (scdata) {
                    if (scdata) {
                        $('#size').html('');
                        $('#size').append('<option value="">Please select size</option>');
                        $.each(scdata, function (k, val) {
                            var opt = $('<option />');
                            opt.val(val.id);
                            opt.text(val.name);
                            $('#size').append(opt);
                        })
                        $('#modal-loading').hide();
                    } else {
                        $('#size').html('');
//                    $('#' + a).append('<option value="">Against Order no is not Department</option>');
                    }
                },
                error: function () {
                    $('#modal-loading').hide();
                }
            });
        }
    });
    
   
</script>