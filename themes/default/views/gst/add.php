
<style>
    .modal-lg {
        width: 1000px;
    }
</style>
<?= $modal_js ?>

<div class="modal-dialog modal-lg">
    <div class="modal-content ">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_gst'); ?></h4>
        </div>
        <?php
        ?>
        <form data-toggle= "validator" class ="form-horizontal" role="form" id="addFormx" onsubmit="return false;">
            <div class="modal-body">
                <div class="">
                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                                <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="storegst">1. Store *</label>
                                <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8"> 
                                    <?php echo form_input('store_idgst', $data['store_id'], 'class="form-control myselect" id="companiesgst"  data-tab="companies"  placeholder="Select Store" data-bv-notempty="true" required'); ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                                <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="departmentgst">2. Department *</label>
                                <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                                    <?php echo form_input('departmentgst', $data['dept'], 'class="form-control myselect"  data-tab="department" data-key="store_id" data-id="companiesgst" id="departmentgst" placeholder="Select Department" data-bv-notempty="true"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                                <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="product_itemgst">3. Section *</label>
                                <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                                    <?php echo form_input('sectiongst', $data['section_id'], 'class="form-control myselect" data-key="department_id" data-id="departmentgst" id="sectiongst" data-tab="section" placeholder="Select Section" data-bv-notempty="true"'); ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                                <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="product_itemgst">4. Product Item *</label>
                                <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                                    <?php echo form_input('product_itemsgst', $data['product_item'], 'class="form-control myselect"  data-key="section_id" data-id="sectiongst" id="product_itemsgst" data-tab="product_items" placeholder="Select Product Items" data-bv-notempty="true"'); ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                                <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="typefgst">5. Type *</label>
                                <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                                    <?php echo form_input('type_idgst', $data['type'], 'class="form-control myselect" data-key="product_items_id" data-id="product_itemsgst" id="typegst" data-tab="type" placeholder="Select Type" data-bv-notempty="true"'); ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                                <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="brandgst">6. Brand *</label>
                                <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                                    <?php echo form_input('brandsgst', $data['brands_id'], 'class="form-control myselect" data-key="type_id" data-id="typegst" id="brandsgst"  data-tab="brands" placeholder="Select Brands" data-bv-notempty="true"'); ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                                <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="designgst">7. Design *</label>
                                <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                                    <?php echo form_input('designgst', $data['design'], 'class="form-control myselect" data-key="brands_id" data-id="brandsgst" id="designgst" data-tab="design" placeholder="Select Design" '); ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                                <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">8. Style</label>
                                <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                                    <?php echo form_input('stylegst', $data['style'], 'class="form-control myselect" data-key="design_id" data-id="designgst" id="stylegst" data-tab="style" placeholder="Select Style" '); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                                <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">9. Pattern</label>
                                <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                                    <?php echo form_input('patterngst', $data['pattern'], 'class="form-control myselect" data-key="style_id" data-id="stylegst" id="patterngst" data-tab="pattern" placeholder="Select Pattern" '); ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                                <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">10. Fitting</label>
                                <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                                    <?php echo form_input('fittinggst', $data['fitting'], 'class="form-control myselect" data-key="pattern_id" data-id="patterngst" id="fittinggst" data-tab="fitting" placeholder="Select Fitting" '); ?>
                                </div>
                            </div>


                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                                <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">11. Fabric</label>
                                <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                                    <?php echo form_input('fabricgst', $data['fabric'], 'class="form-control myselect" data-key="fitting_id" data-id="fittinggst" id="fabricgst" data-tab="fabric" placeholder="Select Fabric" '); ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                                <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">12. Color</label>
                                <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                                    <?php echo form_input('colorgst', '', 'class="form-control myselect" id="colorgst"  data-tab="color" placeholder="Select Color" data-bv-notempty="true"'); ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                                <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">13. Size *</label>
                                <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                                    <?php echo form_input('sizegst', '', 'class="form-control sizesel" data-tab="size" data-key="department_id-section_id-product_items_id" data-id="departmentgst,sectiongst,product_itemsgst" id="selsizegst" placeholder="select Size" data-bv-notempty="true"'); ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                                <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">14. MRP</label>
                                <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                                    <?php
                                    echo form_input('mrpgst', '', 'class="form-control " data-bv-notempty="false" data-bv-stringlength="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]\d*(\.\d+)?$"
                               data-bv-regexp-message="Only Numbers Allowed" id="fitting" placeholder="MRP"')
                                    ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                                <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">15. MRP - From</label>
                                <div class="col-md-8 col-lg-8 col-xs-8 col-sm-8">
                                    <?php
                                    echo form_input('mrpfromgst', '', 'class="form-control " data-bv-stringlength="true"
                              data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]\d*(\.\d+)?$"
                               data-bv-regexp-message="Only Numbers Allowed" data-bv-notempty="false" id="fitting" placeholder="MRP From"')
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                                <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4">MRP - Up to</label>
                                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                    <?php
                                    echo form_input('mrpuptogst', '', 'class="form-control  " data-bv-notempty="false" data-bv-stringlength="true"
                               
                                data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]\d*(\.\d+)?$"
                               data-bv-regexp-message="Only Numbers Allowed" id="fitting" placeholder="MRP Up to"')
                                    ?>
                                </div>
                                <!--<label class="col-md-2 col-lg-2 col-xs-2 col-sm-2 ">%</label>-->
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                                <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="cgst">16. CGST *</label>
                                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                    <?php
                                    echo form_input('cgst', '', 'class="form-control  "  data-bv-notempty="true" data-bv-stringlength="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]\d*(\.\d+)?$"
                               data-bv-regexp-message="Only Numbers Allowed" id="cgst" placeholder="CGST"')
                                    ?>
                                </div>
                                <label class="col-md-2 col-lg-2 col-xs-2 col-sm-2 ">%</label>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                                <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="sgst">17. SGST *</label>
                                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                    <?php
                                    echo form_input('sgst', '', 'class="form-control  "  data-bv-notempty="true" data-bv-stringlength="true"
                                data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]\d*(\.\d+)?$"
                               data-bv-regexp-message="Only Numbers Allowed" id="sgst" placeholder="SGST"')
                                    ?>
                                </div>
                                <label class="col-md-2 col-lg-2 col-xs-2 col-sm-2 ">%</label>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                                <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="hsncode">18. HSN Code *</label>
                                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                    <?php
                                    echo form_input('hsncodegst', '', 'class="form-control  "  data-bv-notempty="true" data-bv-stringlength="true"
                                id="hsncode" placeholder="HSN Code"')
                                    ?>
                                </div>
                                <!--<label class="col-md-2 col-lg-2 col-xs-2 col-sm-2 ">%</label>-->
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                                <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="cess">19. Cess</label>
                                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                    <?php
                                    echo form_input('cessgst', '', 'class="form-control  " data-bv-notempty="true" data-bv-stringlength="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]\d*(\.\d+)?$"
                               data-bv-regexp-message="Only Numbers Allowed" id="cess" placeholder="Cess"')
                                    ?>
                                </div>
                                <label class="col-md-2 col-lg-2 col-xs-2 col-sm-2 ">%</label>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6 form-group">
                                <label class="col-md-4 col-lg-4 col-xs-4 col-sm-4" for="addupgst">20. Add up GST</label>
                                <div class="col-md-6 col-lg-6 col-xs-6 col-sm-6">
                                    <?php
                                    echo form_input('addupgst', '', 'class="form-control  " data-bv-notempty="true" data-bv-stringlength="true"
                               data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]\d*(\.\d+)?$"
                               data-bv-regexp-message="Only Numbers Allowed" id="addupgst" placeholder="Add up GST"')
                                    ?>
                                </div>
                                <label class="col-md-2 col-lg-2 col-xs-2 col-sm-2 ">%</label>
                            </div>
                        </div>
                    </div>
                    <div class="row text-center">

                        <!--<button type="button" class="btn btn-danger"  onclick="this.form.reset();">Cancel</button>-->  	
                        <!--<a href="<?php echo base_url('gst') ?>"><button type="button" class="btn btn-danger">Cancel</button>  </a>-->	
                    </div>
                    <?php // echo form_close();  ?>
                </div>
            </div>
            <div class="modal-footer">
                <?php echo form_submit('add_gst', lang('save'), 'class="btn btn-primary"'); ?>
                <?php // echo form_submit('add_currency', lang('add_currency'), 'class="btn btn-primary"'); ?>
            </div>
        </form>
    </div>
    <?php // echo form_close();  ?>
</div>

<script type="text/javascript">
    $(document).ready(function (e) {
        $('#addFormx').bootstrapValidator({
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            }, excluded: [':disabled']
        });
        $('select.select').select2({minimumResultsForSearch: 6});
        fields = $('.modal-content').find('.form-control');
        $.each(fields, function () {
            var id = $(this).attr('id');
            var iname = $(this).attr('name');
            var iid = '#' + id;
            if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
//                $("label[for='" + id + "']").append(' *');
                $(document).on('change', iid, function () {
                    $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
                });
            }
        });
    });
    /**
     * Comment
     */
//    mrp = onlydigits('mrp');
//    mrp();
//    mrp_from = onlydigits('mrp_from');
//    mrp_from();
//    mrp_to = onlydigits('mrp_to');
//    mrp_to();
//    min_qty_lvl = onlydigits('min_qty_lvl');
//    min_qty_lvl();
//    min_qty_lvl_2 = onlydigits('min_qty_lvl_2');
//    min_qty_lvl_2();
//    reorder_qty = onlydigits('reorder_qty');
//    reorder_qty();
//    reorder_qty_2 = onlydigits('reorder_qty_2');
//    reorder_qty_2();
    $(document).ready(function () {
        $("#addFormx").unbind().submit(function (e) {
            var valid = true;
            if ($('#companiesgst').val() == "") {
                valid = false;
            } else if ($('#departmentgst').val() == "") {
                valid = false;
            } else if ($('#sectiongst').val() == "") {
                valid = false;
            } else if ($('#product_itemsgst').val() == "") {
                valid = false;
            } else if ($('#typegst').val() == "") {
                valid = false;
            } else if ($('#brandsgst').val() == "") {
                valid = false;
            } else if ($('#designgst').val() == "") {
                valid = false;
            } else if ($('#colorgst').val() == "") {
                valid = false;
            } else if ($('#sizegst').val() == "") {
                valid = false;
            } else if ($('#selsizegst').val() == "") {
                valid = false;
            } else if ($('#cgst').val() == "") {
                valid = false;
            } else if ($('#hsncodegst').val() == "") {
                valid = false;
            } else if ($('#cessgst').val() == "") {
                valid = false;
            }
            if (valid == false) {
                bootbox.alert('The field labels marked with * are required input fields.!');
                return false;
            }
            e.preventDefault();
            var form = $("#addFormx");
            var $data = form.serialize();
            $.ajax({
                url: "<?= site_url('gst/savepopup') ?>",
                type: "get",
                async: false,
                data: $data,
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    if (data.s === "true") {
                        bootbox.alert('GST Added Successfully!');
                        $('#type_id').trigger('change');
//                        if (window.location.href != '//<?= site_url("products/add") ?>') {
//                            
//                            oTable.fnDraw();
//                        }
                        $("#myModal").modal('hide');
                    } else if (data.ex == "true") {
                        bootbox.alert('HST Code Allready Exist!');
                        $("#myModal").modal('hide');
                    } else {
                        bootbox.alert('GST Adding Failed!');
                        $("#myModal").modal('hide');
                    }
                }
            });
            return false;
        });
        var obj = ['companiesgst', 'departmentgst', 'sectiongst', 'product_itemsgst', 'typegst', 'brandsgst', 'designgst', 'stylegst', 'patterngst', 'fittinggst', 'fabricgst', 'colorgst', 'sizegst', 'pergst'];
        var objarr = [];
        var i = 0;
        $.each(obj, function (k, v) {
            objarr[i] = myselect2(v);
            objarr[i]();
            i++;
        });
        function myselect2(id) {
            if (id == "pergst") {
                var pp = $("." + id);
            } else {
                var pp = $("#" + id);
            }
            function get() {
                pp.select2({
//                    minimumInputLength: 1,
                    data: [],
                    initSelection: function (element, callback) {
                        $.ajax({
                            type: "get", async: false,
                            url: site_url + "products/getIdAttribute",
                            data: {
                                term: pp.val(),
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                            },
                            dataType: "json",
                            success: function (data) {
                                callback(data[0]);
                            }
                        });
                    },
                    ajax: {
                        url: site_url + "products/getIdAttributes",
                        dataType: 'json',
                        quietMillis: 15,
                        data: function (term, page) {
                            return {
                                term: term,
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                                limit: 10
                            };
                        },
                        results: function (data, page) {
                            if (data.results != null) {
                                return {results: data.results};
                            } else {
                                return {results: [{id: '', text: 'No Match Found'}]};
                            }
                        }
                    }
                });
            }
            return get;
        }
        function getidsx() {
//            alsert($("#selsize").data('id'));
            var $ids = $("#selsizegst").data('id').split(",");
            var $idvals = "";

            $.each($ids, function (k, v) {
                $idvals += ($("#" + v).val() ? $("#" + v).val() : "") + "-";
            });
            $idvals = $idvals.trim();
            return $idvals;
        }
        $("#selsizegst").select2({
//            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site_url + "products/getIdAttribute",
                    data: {
                        term: $(element).val(),
                        tab: $(element).data('tab'),
                        id: getidsx,
                        key: $(element).data('key'),
                    },
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site_url + "products/getIdAttributes",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        tab: $("#selsizegst").data('tab'),
                        id: getidsx,
                        key: $("#selsizegst").data('key'),
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'No Match Found'}]};
                    }
                }
            }
        });
    });
</script>