<script type="text/javascript">
    var count = 1, an = 1, DT = <?= $Settings->default_tax_rate ?>,
            product_tax = 0, invoice_tax = 0, total_discount = 0, total = 0, shipping = 0,
            tax_rates = <?php echo json_encode($tax_rates); ?>;
    var audio_success = new Audio('<?= $assets ?>sounds/sound2.mp3');
    var audio_error = new Audio('<?= $assets ?>sounds/sound3.mp3');
    $(document).ready(function () {
<?php if ($inv) {
    ?>
            localStorage.setItem('qudate', '<?= date($dateFormats['php_ldate'], strtotime($inv->date)) ?>');
            localStorage.setItem('estimate_delievery', '<?= $this->sma->hrsd($quote_data->estimate_delievery) ?>');
            localStorage.setItem('qucustomer', '<?= $inv->customer_id ?>');
            localStorage.setItem('user_id', '<?= $inv->user_id ?>');
            localStorage.setItem('qubiller', '<?= $inv->biller_id ?>');
            localStorage.setItem('quref', '<?= $inv->reference_no ?>');
            localStorage.setItem('quwarehouse', '<?= $inv->warehouse_id ?>');
            localStorage.setItem('qustatus', '<?= $inv->status ?>');
            localStorage.setItem('qunote', '<?= str_replace(array("\r", "\n"), "", $this->sma->decode_html($inv->note)); ?>');
            localStorage.setItem('qudiscount', '<?= $inv->order_discount_id ?>');
            localStorage.setItem('qutax2', '<?= $inv->order_tax_id ?>');
            localStorage.setItem('qushipping', '<?= $inv->shipping ?>');
            localStorage.setItem('quitems', JSON.stringify(<?= $inv_items; ?>));
<?php } ?>
<?php if ($Owner || $Admin) { ?>
            $(document).on('change', '#qudate', function (e) {
                localStorage.setItem('qudate', $(this).val());
            });
            if (qudate = localStorage.getItem('qudate')) {
                $('#qudate').val(qudate);
            }
            $(document).on('change', '#estimate_delievery', function (e) {
                localStorage.setItem('estimate_delievery', $(this).val());
            });
            if (estimate_delievery = localStorage.getItem('estimate_delievery')) {
                $('#estimate_delievery').val(estimate_delievery);
            }
            $(document).on('change', '#qubiller', function (e) {
                localStorage.setItem('qubiller', $(this).val());
            });
            if (qubiller = localStorage.getItem('qubiller')) {
                $('#qubiller').val(qubiller);
            }
<?php } ?>
        ItemnTotals();
        $("#add_item").autocomplete({
            source: function (request, response) {
                if (!$('#list_suppliers').val()) {
                    $('#add_item').val('').removeClass('ui-autocomplete-loading');
                    bootbox.alert('<?= lang('select_above'); ?>');
                    //response('');
                    $('#add_item').focus();
                    return false;
                }
                $.ajax({
                    type: 'get',
                    url: '<?= site_url('quotes/suggestions'); ?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        warehouse_id: $("#quwarehouse").val(),
                        customer_id: $("#list_suppliers").val(),
                        store: $("#store").val()
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 200,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                } else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                } else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var row = add_invoice_item(ui.item);
                    if (row)
                        $(this).val('');
                } else {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });
        $('#add_item').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                $(this).autocomplete("search");
            }
        });
        $(window).bind('beforeunload', function (e) {
            $.get('<?= site_url('welcome/set_data/remove_quls/1'); ?>');
            if (count > 1) {
                var message = "You will loss data!";
                return message;
            }
        });
        $('#reset').click(function (e) {
            $(window).unbind('beforeunload');
        });
        $('#edit_quote').click(function () {
            $(window).unbind('beforeunload');
            $('form.edit-qu-form').submit();
        });
    });</script>


<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-edit"></i><?= lang('edit_quote'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?php echo lang('enter_info'); ?></p>
                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'class' => 'edit-qu-form');
                echo form_open_multipart("quotes/edit/" . $id, $attrib)
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($Owner || $Admin) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("biller", "qubiller"); ?>
                                    <?php
                                    $bl[""] = "";
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->name;
                                    }
                                    echo form_dropdown('biller', $bl, set_value('biller', $quote_data->biller_id), 'id="store" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("biller") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                            <?php
                        } else {
                            $biller_input = array(
                                'type' => 'hidden',
                                'name' => 'biller',
                                'id' => 'qubiller',
                                'value' => $this->session->userdata('biller_id'),
                            );
                            echo form_input($biller_input);
                        }
                        ?>


                        <?php if ($Owner || $Admin) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("order_date", "qudatewa"); ?>
                                    <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="qudate" required="required"'); ?>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?php echo lang("supplier_code", "qucustomer"); ?>
                                <?php
                                $res[''] = '';
                                foreach ($suppliers as $sup) {
                                    $res[$sup->id] = $sup->code;
                                    if ($inv->customer_id == $sup->id) {
                                        $cust_sup[$sup->id] = $sup->code . ' - ' . $inv->customer;
                                    }
                                }
                                echo form_dropdown('customer', $cust_sup, set_value('customer', $inv->customer_id), 'id="list_suppliers" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("supplier_code") . '" required="required" class="form-control input-tip" style="width:100%;"');
                                ?>

                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("pur_ord_num", "quref"); ?>
                                <?php echo form_input('reference_no', (isset($_POST['reference_no']) ? $_POST['reference_no'] : ''), 'class="form-control input-tip"  id="quref" required="required"'); ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("status", "status"); ?>
                                <!--$st = array('pending' => lang('pending'), 'sent' => lang('sent'), 'completed' => lang('completed'));-->
                                <?php
                                $st = array('pending' => lang('pending'), 'sent' => lang('sent'), 'received' => lang('reveived'));
//                                $st = array('pending' => lang('pending'));
                                echo form_dropdown('status', $st, $quote_data->status, 'class="form-control input-tip" id="st"');
                                ?>

                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("estimate_dilievery_date", "esd"); ?> *
                                <?php echo form_input('estimate_delievery', (isset($_POST['estimate_delievery']) ? $_POST['estimate_delievery'] : ''), 'class="form-control input-tip date" id="estimate_delievery" required="required"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("through_transport", "through_transport"); ?>
                                <?php
                                $trans[""] = "";
                                foreach ($transport as $tr) {
//                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                    $trans[$tr->id] = $tr->code . ' - ' . $tr->transport_name;
                                }
                                echo form_dropdown('transport', $trans, (isset($_POST['transport_name']) ? $_POST['transport_name'] : $transp->transp_id), 'id="transport" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("through_transport") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("order_by", "order_by"); ?>
                                <?php
                                $us = array();
                                $us[""] = "";
                                foreach ($user as $usr) {
                                    $us[$usr->id] = $usr->first_name != '-' ? $usr->first_name . ' ' . $usr->last_name : $usr->first_name . ' ' . $usr->last_name;
                                }
                                echo form_dropdown('user_id', $us, (isset($_POST['first_name']) ? $_POST['first_name'] : $inv->user_id), 'id="user_id" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("user") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("document", "document") ?>
                                <input id="document" type="file" name="document" data-show-upload="false"
                                       data-show-preview="false" class="form-control file">
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="panel panel-warning">
                                <div
                                    class="panel-heading"><?= lang('please_select_these_before_adding_product') ?></div>
                                <div class="panel-body" style="padding: 5px;">

                                    <?php if ($Owner || $Admin) { ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("warehouse", "quwarehouse"); ?>
                                                <?php
                                                $wh[''] = '';
                                                foreach ($warehouses as $warehouse) {
                                                    $wh[$warehouse->id] = $warehouse->name;
                                                }
                                                echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $Settings->default_warehouse), 'id="quwarehouse" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("warehouse") . '" required="required" style="width:100%;" ');
                                                ?>
                                            </div>
                                        </div>
                                        <?php
                                    } else {
                                        echo form_hidden('warehouse', $user->warehouse_id, 'id="quwarehouse"');
                                    }
                                    ?>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-12" id="sticker">
                            <div class="well well-sm">
                                <div class="form-group" style="margin-bottom:0;">
                                    <div class="input-group wide-tip">
                                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                            <i class="fa fa-2x fa-barcode addIcon"></i></a></div>
                                        <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . $this->lang->line("add_product_to_order") . '"'); ?>
                                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                            <a data-toggle="modal" data-target="#mModal" id="addManually" class="tip" title="<?= lang('add_product_manually') ?>">
                                                <i class="fa fa-2x fa-plus-circle addIcon" id="addIcon"></i></a></div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <div class="control-group table-group">
                                <label class="table-label"><?= lang("order_items"); ?> *</label>

                                <div class="controls table-controls">
                                    <table id="quTable" class="table items table-striped table-bordered table-condensed table-hover">
                                        <thead>
                                            <tr>
                                              <!--<th class="col-md-4"><?= lang("product_name") . " (" . $this->lang->line("product_code") . ")"; ?></th>-->
                                                <th class="col-md-4"><?= lang("product_name"); ?></th>
                                                <!--<th class="col-md-1"><?= lang("net_unit_price"); ?></th>-->
                                                <th class="col-md-1"><?= lang("purchase_price"); ?></th>
                                                <th class="col-md-1"><?= lang("primary_quantity"); ?></th>
                                                <th class="col-md-1"><?= lang("squantity"); ?></th>
                                                <th class="col-md-1"><?= lang("design"); ?></th>
                                                <th class="col-md-1"><?= lang("style"); ?></th>
                                                <th class="col-md-1"><?= lang("pattern"); ?></th>
                                                <th class="col-md-1"><?= lang("fitting"); ?></th>
                                                <th class="col-md-1"><?= lang("fabric"); ?></th>
                                                <th class="col-md-1"><?= lang("color"); ?></th>
                                                <th class="col-md-1"><?= lang("mrp"); ?></th>
                                                <?php /*
                                                  if ($Settings->product_serial) {
                                                  echo '<th class="col-md-2">' . $this->lang->line("serial_no") . '</th>';
                                                  } */
                                                ?>
                                                <?php
                                                if ($Settings->product_discount) {
//                                                    echo '<th class="col-md-1">' . $this->lang->line("discount") . '</th>';
                                                }
                                                ?>
                                                <?php
                                                if ($Settings->tax1) {
//                                                    echo '<th class="col-md-2">' . $this->lang->line("product_tax") . '</th>';
                                                }
                                                ?>
                                                <!--<th class="col-md-1"><?= lang("design"); ?></th>-->
                                                <th><?= lang("subtotal"); ?> (<span
                                                        class="currency"><?= $default_currency->code ?></span>)
                                                </th>
                                                <th style="width: 30px !important; text-align: center;"><i
                                                        class="fa fa-trash-o"
                                                        style="opacity:0.5; filter:alpha(opacity=50);"></i></th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                        <tfoot></tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="total_items" value="" id="total_items" required="required"/>
                        <div id="bottom-total" class="well well-sm" style="margin-bottom: 0;">
                            <table class="table table-bordered table-condensed totals" style="margin-bottom:0;">
                                <tr class="warning">
                                    <td><?= lang('items') ?> <span class="totals_val pull-right" id="titems">0</span></td>
                                    <td><?= lang('total') ?> <span class="totals_val pull-right" id="total">0.00</span></td>
                                    <!--<td><?= lang('order_discount') ?> <span class="totals_val pull-right" id="tds">0.00</span></td>-->
                                    <?php if ($Settings->tax2) { ?>
                                                                                                                                    <!--<td><?= lang('order_tax') ?> <span class="totals_val pull-right" id="ttax2">0.00</span></td>-->
                                    <?php } ?>
                                    <!--<td><?= lang('shipping') ?> <span class="totals_val pull-right" id="tship">0.00</span></td>-->
                                    <td><?= lang('grand_total') ?> <span class="totals_val pull-right" id="gtotal">0.00</span></td>
                                </tr>
                            </table>
                        </div>
                        <div class="row" id="bt">
                            <div class="col-sm-12">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <?= lang("note", "qunote"); ?>
                                        <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="qunote" style="margin-top: 10px; height: 100px;"'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div
                                class="fprom-group"><?php echo form_submit('edit_quote', $this->lang->line("submit"), 'id="edit_quote" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                                <!--<button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></div>-->
                            </div>
                        </div>
                    </div>


                    <?php echo form_close(); ?>

                </div>

            </div>
        </div>
    </div>

    <div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                                class="fa fa-2x">&times;</i></span><span class="sr-only"><?= lang('close'); ?></span></button>
                    <h4 class="modal-title" id="prModalLabel"></h4>
                </div>
                <div class="modal-body" id="pr_popover_content">
                    <form class="form-horizontal" role="form" >
                        <?php if ($Settings->tax1) { ?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?= lang('product_tax') ?></label>
                                <div class="col-sm-8">
                                    <?php
                                    $tr[""] = "";
                                    foreach ($tax_rates as $tax) {
                                        $tr[$tax->id] = $tax->name;
                                    }
                                    echo form_dropdown('ptax', $tr, "", 'id="ptax" class="form-control pos-input-tip" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($Settings->product_serial) { ?>
                            <div class="form-group">
                                <label for="pserial" class="col-sm-4 control-label"><?= lang('serial_no') ?></label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="pserial">
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <label for="pquantity" class="col-sm-4 control-label"><?= lang('quantity') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pquantity">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="poption" class="col-sm-4 control-label"><?= lang('product_option') ?></label>

                            <div class="col-sm-8">
                                <div id="poptions-div"></div>
                            </div>
                        </div>
                        <?php if ($Settings->product_discount) { ?>
                            <div class="form-group">
                                <label for="pdiscount"
                                       class="col-sm-4 control-label"><?= lang('product_discount') ?></label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="pdiscount">
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <label for="pprice" class="col-sm-4 control-label"><?= lang('unit_price') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pprice">
                            </div>
                        </div>
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th style="width:25%;"><?= lang('net_unit_price'); ?></th>
                                <th style="width:25%;"><span id="net_price"></span></th>
                                <th style="width:25%;"><?= lang('product_tax'); ?></th>
                                <th style="width:25%;"><span id="pro_tax"></span></th>
                            </tr>
                        </table>
                        <input type="hidden" id="punit_price" value=""/>
                        <input type="hidden" id="old_tax" value=""/>
                        <input type="hidden" id="old_qty" value=""/>
                        <input type="hidden" id="old_price" value=""/>
                        <input type="hidden" id="row_id" value=""/>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="editItem"><?= lang('submit') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(myFunction);
    $('#store').on('ready change', myFunction);
    function myFunction() {
//    $("#user_id").select2("val", "");
        var v = $('#store').val();
        if (v) {
            $.ajax({
                type: "get",
                async: false,
                data: {store: v},
                url: "<?= site_url('purchases/getBiller') ?>",
                dataType: "json",
                success: function (scdata) {
                    var inv_sup_id = '<?= $inv->customer_id; ?>';
                    $('#list_suppliers').html('');
                    $('#list_suppliers').removeAttr('readonly', '');
                    $('#modal-loading').hide();
                    $.each(scdata, function (k, val) {
                        var opt = $('<option />');
                        opt.val(val.id);
                        if (inv_sup_id == val.id) {
                            $(opt).attr('selected', 'selected');
                        }
                        opt.text(val.code + ' - ' + val.company);
                        $('#list_suppliers').append(opt);
                    })
                },
                error: function () {
                    $('#list_suppliers').attr('readonly', '');
                    $('#modal-loading').hide();
                }
            });
//    $.ajax({
//    type: "get",
//            async: false,
//            data: {store: v},
//            url: "<?= site_url('purchases/getEmployees') ?>",
//            dataType: "json",
//            success: function (scdata) {
//            var inv_c_id = '<?= $inv->user_id; ?>';
//            $('#user_id').html('');
//            $('#user_id').removeAttr('readonly', '');
//            $.each(scdata, function (k, val) {
//            var opt = $('<option />');
//            opt.val(val.id);
//            if (inv_c_id == val.id) {
//            $(opt).attr('selected', 'selected');
//            }
//            opt.text(val.fname + ' ' + val.mname + ' ' + val.lname);
//            $('#user_id').append(opt);
//            })
//            }, error: function () {
//    //                    $('#booked_by').addClass('hidden');
//    $('#user_id').attr('readonly', '');
//    $('#modal-loading').hide();
//    }
//    })

        }
    }


</script>




<?php
if (!empty($variants)) {
    foreach ($variants as $variant) {
        $vars[] = addslashes($variant->name);
    }
} else {
    $vars = array();
}

$department[''] = "";
$product_items[''] = "";
$section[''] = "";
$type[''] = "";
$brands[''] = "";
$design[''] = "";
$style[''] = "";
$pattern[''] = "";
$fitting[''] = "";
$fabric[''] = "";
$color[''] = "";
$size[''] = "";
$sizes = array();
$per[''] = "";
foreach ($product_para as $k => $v) {
    if ($k == "department" && !empty($v)) {
        foreach ($v as $d) {
            $department[$d->id] = $d->name;
        }
    }
    if ($k == "product_items" && !empty($v)) {
        foreach ($v as $d) {
            $product_items[$d->id] = $d->name;
        }
    }
    if ($k == "section" && !empty($v)) {
        foreach ($v as $d) {
            $section[$d->id] = $d->name;
        }
    }
    if ($k == "type" && !empty($v)) {
        foreach ($v as $d) {
            $type[$d->id] = $d->name;
        }
    }
    if ($k == "brands" && !empty($v)) {
        foreach ($v as $d) {
            $brands[$d->id] = $d->name;
        }
    }
    if ($k == "design" && !empty($v)) {
        foreach ($v as $d) {
            $design[$d->id] = $d->name;
        }
    }
    if ($k == "style" && !empty($v)) {
        foreach ($v as $d) {
            $style[$d->id] = $d->name;
        }
    }
    if ($k == "pattern" && !empty($v)) {
        foreach ($v as $d) {
            $pattern[$d->id] = $d->name;
        }
    }
    if ($k == "fitting" && !empty($v)) {
        foreach ($v as $d) {
            $fitting[$d->id] = $d->name;
        }
    }
    if ($k == "fabric" && !empty($v)) {
        foreach ($v as $d) {
            $fabric[$d->id] = $d->name;
        }
    }
    if ($k == "color" && !empty($v)) {
        foreach ($v as $d) {
            $color[$d->id] = $d->name;
        }
    }
    if ($k == "size" && !empty($v)) {
        foreach ($v as $d) {
            $size[$d->id] = $d->name;
            $sizes[$d->id] = $d;
//            array_push($sizes, $d);
        }
    }
    if ($k == "per" && !empty($v)) {
        foreach ($v as $d) {
            $per[$d->id] = $d->name;
        }
    }
}
?>




<style>
    @media(min-width: 992px) {.modal-lg {width: 80% !important;}}
    .err{
        border-color: red;
    }
</style>


<div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?= lang('close'); ?></span></button>
                <h4 class="modal-title" id="prModalLabel"></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= lang('product_tax') ?></label>
                            <div class="col-sm-8">
                                <?php
                                $tr[""] = "";
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('ptax', $tr, "", 'id="ptax" class="form-control pos-input-tip" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <input type="text" name="from_order" value="from_order" class="form-control hidden"/>
                    <div class="form-group">
                        <label for="pquantity" class="col-sm-4 control-label"><?= lang('quantity') ?></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pquantity">
                        </div>
                    </div>
                    <?php if ($Settings->product_expiry) { ?>
                        <div class="form-group">
                            <label for="pexpiry" class="col-sm-4 control-label"><?= lang('product_expiry') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control date" id="pexpiry">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="poption" class="col-sm-4 control-label"><?= lang('product_option') ?></label>

                        <div class="col-sm-8">
                            <div id="poptions-div"></div>
                        </div>
                    </div>
                    <?php if ($Settings->product_discount) { ?>
                        <div class="form-group">
                            <label for="pdiscount"
                                   class="col-sm-4 control-label"><?= lang('product_discount') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pdiscount">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="pcost" class="col-sm-4 control-label"><?= lang('unit_cost') ?></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pcost">
                        </div>
                    </div>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th style="width:25%;"><?= lang('net_unit_cost'); ?></th>
                            <th style="width:25%;"><span id="net_cost"></span></th>
                            <th style="width:25%;"><?= lang('product_tax'); ?></th>
                            <th style="width:25%;"><span id="pro_tax"></span></th>
                        </tr>
                    </table>
                    <input type="hidden" id="punit_cost" value=""/>
                    <input type="hidden" id="old_tax" value=""/>
                    <input type="hidden" id="old_qty" value=""/>
                    <input type="hidden" id="old_cost" value=""/>
                    <input type="hidden" id="row_id" value=""/>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="editItem"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>

<!--<div class="modal" id="mModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?= lang('close'); ?></span></button>
                <h4 class="modal-title" id="mModalLabel"><?= lang('add_standard_product') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <div class="alert alert-danger" id="mError-con" style="display: none;">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <span id="mError"></span>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
<?= lang('product_code', 'mcode') ?> *
                            <input type="text" class="form-control" id="mcode">
                        </div>
                        <div class="form-group">
<?= lang('product_name', 'mname') ?> *
                            <input type="text" class="form-control" id="mname">
                        </div>
                        <div class="form-group">
<?= lang('category', 'mcategory') ?> *
<?php
$cat[''] = "";
foreach ($categories as $category) {
    $cat[$category->id] = $category->name;
}
echo form_dropdown('category', $cat, '', 'class="form-control select" id="mcategory" placeholder="' . lang("select") . " " . lang("category") . '" style="width:100%"')
?>
                        </div>
                        <div class="form-group">
<?= lang('unit', 'munit') ?> *
                            <input type="text" class="form-control" id="munit">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
<?= lang('cost', 'mcost') ?> *
                            <input type="text" class="form-control" id="mcost">
                        </div>
                        <div class="form-group">
<?= lang('price', 'mprice') ?> *
                            <input type="text" class="form-control" id="mprice">
                        </div>

<?php if ($Settings->tax1) { ?>
                                                                                                                            <div class="form-group">
    <?= lang('product_tax', 'mtax') ?>
    <?php
    $tr[""] = "";
    foreach ($tax_rates as $tax) {
        $tr[$tax->id] = $tax->name;
    }
    echo form_dropdown('mtax', $tr, "", 'id="mtax" class="form-control input-tip select" style="width:100%;"');
    ?>
                                                                                                                            </div>
                                                                                                                            <div class="form-group all">
    <?= lang("tax_method", "mtax_method") ?>
    <?php
    $tm = array('0' => lang('inclusive'), '1' => lang('exclusive'));
    echo form_dropdown('tax_method', $tm, '', 'class="form-control select" id="mtax_method" placeholder="' . lang("select") . ' ' . lang("tax_method") . '" style="width:100%"')
    ?>
                                                                                                                            </div>
<?php } ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="addUnlabelledItemManually"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>-->

<style>
    @media(min-width: 992px) {.modal-lg {width: 80% !important;}}
    .err{
        border-color: red;
    }
</style>

<div class="modal" id="mModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <script type="text/javascript" src="<?= $assets ?>bower_components/angular/angular.js"></script>
    <script type="text/javascript" src="<?= $assets ?>bower_components/angular-ui-select2/src/select2.js"></script>
    <script src="<?= $assets ?>js/angular.control.js" type="text/javascript"></script>
    <script src="<?= $assets ?>js/services.js" type="text/javascript"></script>
    <!--<script src="<?= $assets ?>js/ui-bootstrap.min.js" type="text/javascript"></script>-->



    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">
                        <i class="fa fa-2x">&times;</i></span><span class="sr-only"><?= lang('close'); ?></span></button>
                <h4 class="modal-title" id="mModalLabel"><?= lang('add_product') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <!--                <form class="form-horizontal" role="form">
                                    <div class="form-group">
                                      <label for="mcode" class="col-sm-4 control-label"><?= lang('product_code') ?> *</label>
                
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control selec_clear" id="mcode" name="mcode">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="mname" class="col-sm-4 control-label"><?= lang('product_name') ?> *</label>
                
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control selec_clear" id="mname" name="mname" required>
                                        </div>
                                    </div>
                <?php if ($Settings->tax1) { ?>
                                                                                                                                                                                                                                <div class="form-group">
                                                                                                                                                                                                                                    <label for="mtax" class="col-sm-4 control-label"><?= lang('product_tax') ?> *</label>
                                                                                                                                                                                                        
                                                                                                                                                                                                                                    <div class="col-sm-8">
                    <?php
                    $tr[""] = "";
                    foreach ($tax_rates as $tax) {
                        $tr[$tax->id] = $tax->name;
                    }
                    echo form_dropdown('mtax', $tr, "", 'id="mtax" class="form-control selec_clear input-tip select" style="width:100%;" required');
                    ?>
                                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                                </div>
                <?php } ?>
                                    <div class="form-group">
                                        <label for="mquantity" class="col-sm-4 control-label"><?= lang('quantity') ?> *</label>
                
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control selec_clear" id="mquantity" required name="mquantity">
                                        </div>
                                    </div>
                <?php if ($Settings->product_discount) { ?>
                                                                                                                                                                                                                                <div class="form-group">
                                                                                                                                                                                                                                    <label for="mdiscount"
                                                                                                                                                                                                                                           class="col-sm-4 control-label"><?= lang('product_discount') ?></label>
                                                                                                                                                                                                        
                                                                                                                                                                                                                                    <div class="col-sm-8">
                                                                                                                                                                                                                                        <input type="text" class="form-control selec_clear" id="mdiscount" required name="mdiscount">
                                                                                                                                                                                                                                    </div>
                                                                                                                                                                                                                                </div>
                <?php } ?>
                                    <div class="form-group">
                                        <label for="mprice" class="col-sm-4 control-label"><?= lang('unit_price') ?> *</label>
                
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control selec_clear" id="mprice" required name="mprice">
                                        </div>
                                    </div>
                                    <table class="table table-bordered table-striped">
                                        <tr>
                                            <th style="width:25%;"><?= lang('net_unit_price'); ?></th>
                                            <th style="width:25%;"><span id="mnet_price"></span></th>
                                            <th style="width:25%;"><?= lang('product_tax'); ?></th>
                                            <th style="width:25%;"><span id="mpro_tax"></span></th>
                                        </tr>
                                    </table>
                                </form>-->

                <div class="box" ng-controller="addProducts">
                    <div class="box-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <form method="post" id="addproduct" data-toggle="validator" role="form">
                                    <input type="text" name="from_order" value="from_order" class="form-control selec_clear hidden"/>
                                    <div class="col-md-5">
                                        <div class="form-group hidden">
                                            <?= lang("product_type", "type") ?>
                                            <?php
                                            if ($product_id != "") {
                                                $opts = array('standard' => lang('standard'), 'combo' => lang('combo'), 'bundle' => lang('bundle'));
                                            } else {
                                                $opts = array('standard' => lang('standard'));
                                            }
                                            echo form_dropdown('type', $opts, (isset($_POST['type']) ? $_POST['type'] : ($product ? $product->type : '')), 'class="form-control selec_clear" id="type" required="required" ng-model="prod.protype" ng-change="getProdBarcode()"');
                                            ?>
                                        </div>
                                        <input type="text" value="standard" name="type" class="hidden form-control selec_clear"/>
                                        <div class="form-group all">


                                            <?= lang("Store", "companies") ?>
                                            <div class="input-group col-md-12">
                                                <?php echo form_input('store_id', (isset($_POST['store_id']) ? $_POST['store_id'] : ($product ? $product->store_id : '')), 'class="form-control selec_clear get_barcode" default-attrib data-tab="companies" data-id="0" id="companies" placeholder="' . lang("select") . " " . lang("store") . '" required="required" style="width:100%"  ng-model="prod.store_id" ng-change="getProdBarcode();getGstDetails();"'); ?>
                                                <div class="input-group-addon no-print hidden">
                                                    <a href="<?php echo site_url('billers/add'); ?>" data-toggle="modal" data-target="#myModal" class="external">
                                                        <i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group all">
                                            <?= lang("Department", "department") ?>
                                            <div class="input-group col-md-12">
                                                <?php
                                                echo form_input('department', (isset($_POST['department']) ? $_POST['department'] : ($product ? $product->department : '')), 'class="form-control selec_clear" default-attrib data-tab="department" data-key="store_id" data-id="companies" id="department" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ng-model="prod.dept" ng-change="getProdName();getProdBarcode();getProductMargin();getGstDetails();"');
                                                ?>
                                                <div class="input-group-addon no-print hidden">
                                                    <a href="<?php echo site_url('system_settings/AddProduct_para/department'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group all">
                                            <?= lang("Section", "section") ?>
                                            <div class="input-group col-md-12">
                                                <?php
                                                echo form_input('section', (isset($_POST['section']) ? $_POST['section'] : ($product ? $product->section : '')), 'class="form-control selec_clear" default-attrib data-tab="section" id="section" data-key="department_id" data-id="department" placeholder="' . lang("select") . " " . lang("section") . '" ng-model="prod.section_id" ng-change="getProductMargin();getGstDetails();" required="required" style="width:100%"');
                                                ?>
                                                <div class="input-group-addon no-print hidden">
                                                    <a href="<?php echo site_url('system_settings/AddProduct_para/section'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group all">
                                            <?= lang("Product_items", "product_items") ?>
                                            <div class="input-group col-md-12">
                                                <?php
                                                echo form_input('product_items', (isset($_POST['product_items']) ? $_POST['product_items'] : ($product ? $product->product_items : '')), 'class="form-control selec_clear" default-attrib data-tab="product_items" data-key="section_id" data-id="section" id="product_items" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%" ng-model="prod.product_item" ng-change="getProdName();getProductMargin();getGstDetails();"');
                                                ?>
                                                <div class="input-group-addon no-print hidden">
                                                    <a href="<?php echo site_url('system_settings/AddProduct_para/product_items'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group all">
                                            <?= lang("Type", "type") ?>
                                            <div class="input-group col-md-12">
                                                <?php
                                                echo form_input('type_id', (isset($_POST['type_id']) ? $_POST['type_id'] : ($product ? $product->type_id : '')), 'class="form-control selec_clear" default-attrib data-tab="type" id="type_id" data-key="product_items_id" data-id="product_items" placeholder="' . lang("select") . " " . lang("type") . '" required="required" style="width:100%" ng-model="prod.type" ng-change="getProdName();getProductMargin();getGstDetails();"');
                                                ?>
                                                <div class="input-group-addon no-print hidden">
                                                    <a href="<?php echo site_url('system_settings/AddProduct_para/type'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group all">
                                            <?= lang("Brands", "brands") ?>
                                            <div class="input-group col-md-12">
                                                <?php
                                                echo form_input('brands', (isset($_POST['brands']) ? $_POST['brands'] : ($product ? $product->brands : '')), 'class="form-control selec_clear" default-attrib data-tab="brands" id="brands" data-key="type_id" data-id="type_id" placeholder="' . lang("select") . " " . lang("brands") . '" required="required" ng-model="prod.brands_id" ng-change="getProductMargin();getGstDetails();"  style="width:100%"');
                                                ?>
                                                <div class="input-group-addon no-print hidden">
                                                    <a href="<?php echo site_url('system_settings/AddProduct_para/brands'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group standard">
                                            <?= lang("Design", "design") ?>
                                            <div class="input-group col-md-12">
                                                <?php
                                                echo form_input('design', (isset($_POST['design']) ? $_POST['design'] : ($product ? $product->design : '')), 'class="form-control selec_clear" default-attrib data-tab="design" id="design" data-key="brands_id" data-id="brands" placeholder="' . lang("select") . " " . lang("design") . '" required="required"  style="width:100%" ng-model="prod.design" ng-change="getGstDetails();"');
                                                ?>
                                                <div class="input-group-addon no-print hidden">
                                                    <a href="<?php echo site_url('system_settings/AddProduct_para/design'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group standard">
                                            <?= lang("Style", "styles") ?>
                                            <div class="input-group col-md-12">
                                                <?php
                                                echo form_input('style', (isset($_POST['style']) ? $_POST['style'] : ($product ? $product->style : '')), 'class="form-control selec_clear" default-attrib data-tab="style" id="style" data-key="design_id" data-id="design" placeholder="' . lang("select") . " " . lang("style") . '" required="required" style="width:100%" ng-model="prod.style" ng-change="getGstDetails();"');
                                                ?>
                                                <div class="input-group-addon no-print hidden">
                                                    <a href="<?php echo site_url('system_settings/AddProduct_para/style'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group standard">
                                            <?= lang("Pattern", "patterns") ?>
                                            <div class="input-group col-md-12">
                                                <?php
                                                echo form_input('pattern', (isset($_POST['pattern']) ? $_POST['pattern'] : ($product ? $product->pattern : '')), 'class="form-control selec_clear" default-attrib data-tab="pattern" data-key="style_id" data-id="style" id="pattern" placeholder="' . lang("select") . " " . lang("pattern") . '" required="required" style="width:100%" ng-model="prod.pattern" ng-change="getGstDetails();"');
                                                ?>
                                                <div class="input-group-addon no-print hidden">
                                                    <a href="<?php echo site_url('system_settings/AddProduct_para/pattern'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group standard">
                                            <?= lang("Fitting", "fittings") ?>
                                            <div class="input-group col-md-12">
                                                <?php
                                                echo form_input('fitting', (isset($_POST['fitting']) ? $_POST['fitting'] : ($product ? $product->fitting : '')), 'class="form-control selec_clear" default-attrib data-tab="fitting" data-key="pattern_id" data-id="pattern" id="fitting" placeholder="' . lang("select") . " " . lang("fitting") . '" required="required" style="width:100%" ng-model="prod.fitting"  ng-change="getGstDetails();"');
                                                ?>
                                                <div class="input-group-addon no-print hidden">
                                                    <a href="<?php echo site_url('system_settings/AddProduct_para/fitting'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group standard">
                                            <?= lang("Fabric", "fabrics") ?>
                                            <div class="input-group col-md-12">
                                                <?php
                                                echo form_input('fabric', (isset($_POST['fabric']) ? $_POST['fabric'] : ($product ? $product->fabric : '')), 'class="form-control selec_clear" default-attrib data-tab="fabric" id="fabric" data-key="fitting_id" data-id="fitting" placeholder="' . lang("select") . " " . lang("fabric") . '" required="required" style="width:100%" ng-model="prod.fabric" ng-change="getGstDetails();"');
                                                ?>
                                                <div class="input-group-addon no-print hidden">
                                                    <a href="<?php echo site_url('system_settings/AddProduct_para/fabric'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-md-offset-1">
                                        <div class="form-group all hidden">
                                            <?= lang("product_name", "name") ?>
                                            <?= form_input('name', (isset($_POST['name']) ? $_POST['name'] : ($product ? $product->name : '')), 'class="form-control selec_clear" id="name" required="required" readonly ng-model="prod.name"'); ?>
                                        </div>
                                        <div class="form-group all hidden">
                                            <?= lang("product_code", "code") ?>
                                            <?= form_input('code', (isset($_POST['code']) ? $_POST['code'] : ($product ? $product->code : '')), 'class="form-control selec_clear" id="code"  required="required" readonly ng-model="prod.barcode" ') ?>
                                            <!--<span class="help-block"><?= lang('you_scan_your_barcode_too') ?></span>-->
                                        </div>
                                        <?= form_hidden('barcode_symbology', (isset($_POST['barcode_symbology']) ? $_POST['barcode_symbology'] : ($product ? $product->barcode_symbology : 'code128')), 'class="form-control selec_clear" id="barcode_symbology"  required="required" readonly ') ?>
                                        <?php if ($Settings->tax1) { ?>
                                            <div class="form-group standard">
                                                <?= lang("product_tax", "tax_rate") ?>
                                                <?php
                                                $tr[""] = "";
                                                foreach ($tax_rates as $tax) {
                                                    $tr[$tax->id] = $tax->name;
                                                }
                                                echo form_dropdown('tax_rate', $tr, (isset($_POST['tax_rate']) ? $_POST['tax_rate'] : ($product ? $product->tax_rate : $Settings->default_tax_rate)), 'class="form-control selec_clear select" id="tax_rate" placeholder="' . lang("select") . ' ' . lang("product_tax") . '" style="width:100%"');
                                                ?>
                                            </div>
                                            <div class="form-group standard">
                                                <?= lang("tax_method", "tax_method") ?>
                                                <?php
                                                $tm = array('0' => lang('inclusive'), '1' => lang('exclusive'));
                                                echo form_dropdown('tax_method', $tm, (isset($_POST['tax_method']) ? $_POST['tax_method'] : ($product ? $product->tax_method : '')), 'class="form-control selec_clear select" id="tax_method" placeholder="' . lang("select") . ' ' . lang("tax_method") . '" style="width:100%"');
                                                ?>
                                            </div>
                                        <?php } ?>
                                        <div class="form-group standard">
                                            <?= lang("supplier", "supplier") ?>
                                            <div class="row" id="supplier-con">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <?php echo form_input('supplier', (isset($_POST['supplier']) ? $_POST['supplier'] : ''), 'class="form-control selec_clear hidden" id="supplier12" placeholder="" style="width:100%;"'); ?>
                                                    <?php echo form_input('supplier_name', (isset($_POST['supplier']) ? $_POST['supplier'] : ''), 'class="form-control selec_clear" id="supplier_name" readonly placeholder="" style="width:100%;"'); ?>
                                                </div>
                                            </div>
                                        </div>



                                        <div class="form-group standard">
                                            <?= lang("product_image", "product_image") ?> max (<?= byte_format($this->Settings->iwidth * $this->Settings->iheight) ?>)
                                            <input id="product_image" type="file" name="product_image" data-show-upload="false"
                                                   data-show-preview="false" accept="image/*" class="form-control selec_clear file">
                                        </div>

                                        <div id="img-details"></div>
                                        <div class="form-group standard">
                                            <?= lang("Color", "color") ?>
                                            <div class="row">
                                                <div class="col-md-3"> &nbsp;&nbsp;<input type="radio" name="colortype" ng-model="prod.colortype" icheck value="Single"/>&nbsp;&nbsp;&nbsp;&nbsp;Single</div>
                                                <div class="col-md-3 colorsingle hide">
                                                    <?php echo form_dropdown('colorsingle', $color, (isset($_POST['colorsingle']) ? $_POST['colorsingle'] : ($product ? $product->color : '')), 'class="form-control" select2 id="color" placeholder="' . lang("select") . " " . lang("color") . '" style="width:100%"'); ?>
                                                </div>
                                                <div class="col-md-2 colorsingle hide">
                                                    <input type="text" id="colorqty" name="colorqty" onlyno class="form-control" ng-model="prod.colorqty" ng-change="getQty('#colorqty')"/>
                                                    <!--<p class="label label-danger hide"> Product Qty and color qty must be same.</p>-->
                                                </div>
                                                <div class="col-md-4 colorsingle hide">
                                                    <input type="text" id="colorcode" name="colorcode[]" onlyno readonly class="form-control" ng-model="prod.colorcode" ng-change="getQty('#colorqty')"/>
                                                    <input type="text" id="codet" name="codet" onlyno class="hidden form-control"/>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-4"> &nbsp;&nbsp;<input type="radio" name="colortype" ng-model="prod.colortype" icheck value="Assorted"/>&nbsp;&nbsp;&nbsp;&nbsp;Assorted</div>
                                                <div class="col-md-8 colorassorted hide">
                                                    <?php
                                                    echo form_dropdown('colorassorted[]', $color, (isset($_POST['colorassorted']) ? $_POST['colorassorted'] : ($product ? $product->colorassorted : '')), 'class="form-control" multiple select2 id="mulcolor" ng-model="prod.colorassorted" placeholder="' . lang("select") . " " . lang("color") . '"style="width:100%"');
                                                    ?>
                                                    <br/>
                                                    <!--                                {{prod.assortedcolorcode}}
                                                                                    {{prod.colorassoarr}}-->
                                                    <div class="row" style="padding-top: 40px;">
                                                        <div class="col-md-3 from-group" style="padding-right: 0px; margin-bottom: 5px" ng-repeat="n in prod.colorassoarr">
                                                            <input type="text" name="colorqty[]" onlyno ng-model="n.qty" ng-blur="getQtyCal($index, n.qty)" class="form-control"/>

<!--<input type="text" name="colorcode[]" onlyno ng-model="n.colorcode" ng-blur="getQtyCal($index, n.qty)" class="form-control"/>-->
                                                        </div>
                                                    </div>
                                                    <div class="row" style="padding-top: 2s0px;">
                                                        <div class="col-md-3 from-group" style="padding-right: 0px; margin-bottom: 5px" ng-repeat="n in prod.colorassoarr">
                                                            <input type="text" name="colorcode[]" onlyno ng-model="n.colorcode" readonly class="form-control"/>
                                                            <!--<input type="text" name="colorcode[]" onlyno ng-model="n.colorcode" ng-blur="getQtyCal($index, n.qty)" class="form-control"/>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <?= lang("Size", "size") ?>
                                            <div class="row">
                                                <div class="col-md-4"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="sizeangle" ng-model="prod.sizeangle" ng-change="getProdName()" <?= ($product && $product->sizeangle == "HT" ? "checked" : '') ?> value="HT"  id="sizeangle_ht"/>&nbsp;&nbsp;&nbsp;&nbsp;Height</div>
                                                <div class="col-md-4"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="sizeangle" ng-model="prod.sizeangle" ng-change="getProdName()" <?= ($product && $product->sizeangle == "WT" ? "checked" : '') ?> value="WT" id="sizeangle_wt"/>&nbsp;&nbsp;&nbsp;&nbsp;Width</div>
                                                <div class="col-md-4"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="sizeangle" ng-model="prod.sizeangle" ng-change="getProdName()" <?= ($product && $product->sizeangle == "NZ" ? "checked" : '') ?> value="NZ" id="sizeangle_nz"/>&nbsp;&nbsp;&nbsp;&nbsp;No Size</div>
                                            </div>
                                            <br/>
                                            <div class="row sizeangle_nz" >
                                                <div class="col-md-4"> &nbsp;&nbsp;<input icheck type="radio" name="sizetype" ng-model="prod.sizetype" ng-change="getProdName()" checked="" value="Single"/>&nbsp;&nbsp;&nbsp;&nbsp;Single</div>
                                                <div class="col-md-4" ng-init='size =<?= json_encode($sizes) ?>' ng-show="prod.sizetype == 'Single'">
                                                    <div class="col-md-9" style="padding: 0px"><?php
                                                        echo form_input('singlesize', (isset($_POST['singlesize']) ? $_POST['singlesize'] : ($product ? $product->size : '')), 'class="form-control selec_clear " sizesel data-tab="size" data-key="department_id-section_id-product_items_id" data-id="department,section,product_items" id="size" placeholder="' . lang("select") . " " . lang("size") . '" required="required" style="width:100%" ng-model="prod.singlesize" ng-change="getProdName();"');
                                                        ?>
                                                    </div>
                                                    <div class="col-md-2"> {{size[prod.singlesize].code?size[prod.singlesize].code+'\"':""}}</div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <br/>
                                            <div class="row standard sizeangle_nz">
                                                <div class="col-md-4"> &nbsp;&nbsp;<input icheck type="radio" name="sizetype" ng-model="prod.sizetype" ng-change="getProdName()" value="Multiple"/>&nbsp;&nbsp;&nbsp;&nbsp;Multiple</div>
                                                <div class="col-md-4" ng-init='size1 =<?= json_encode($sizes) ?>' ng-show="prod.sizetype == 'Multiple'">
                                                    <div class="col-md-9" style="padding: 0px"><?php
                                                        echo form_input('multisizef', (isset($_POST['multisizef']) ? $_POST['multisizef'] : ''), 'class="form-control selec_clear " sizesel data-tab="size" data-key="department_id-section_id-product_items_id" data-id="department,section,product_items" id="multisizef" placeholder="' . lang("select") . " " . lang("size") . '" required="required" style="width:100%" ng-model="prod.multisizef" ng-change="getProdName();"');
                                                        ?></div>
                                                    <div class="col-md-2"> {{size1[prod.multisizef].code?size1[prod.multisizef].code+'\"':""}}</div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-md-1" ng-show="prod.sizetype == 'Multiple'">To</div>
                                                <div class="col-md-3" ng-init='size2 =<?= json_encode($sizes) ?>' ng-show="prod.sizetype == 'Multiple'">
                                                    <div class="col-md-9" style="padding: 0px"><?php
                                                        echo form_input('multisizet', (isset($_POST['multisizet']) ? $_POST['multisizet'] : ''), 'class="form-control selec_clear " sizesel data-tab="size" data-key="department_id-section_id-product_items_id" data-id="department,section,product_items" id="multisizet" placeholder="' . lang("select") . " " . lang("size") . '" required="required" style="width:100%" ng-model="prod.multisizet"');
                                                        ?></div>
                                                    <div class="col-md-1"> {{size2[prod.multisizet].code?size2[prod.multisizet].code+'\"':""}}</div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row standard">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label class="control-label" for="unit"><?= lang("product_qty") ?></label>
                                                    <?php
                                                    foreach ($warehouses as $warehouse) {
                                                        //$whs[$warehouse->id] = $warehouse->name;
                                                        if ($this->Settings->racks) {
                                                            echo "<div class='row'><div class='col-md-12'>";
                                                            echo form_hidden('wh_' . $warehouse->id, $warehouse->id) . form_input('wh_qty_' . $warehouse->id, (isset($_POST['wh_qty_' . $warehouse->id]) ? $_POST['wh_qty_' . $warehouse->id] : ''), 'class="form-control selec_clear" id="wh_qtys" placeholder="' . lang('quantity') . '" ng-model="prod.qty" ng-change="getProdBarcode();" onlyno');
//                                        echo "</div><div class='col-md-6'>";
//                                        echo form_input('rack_' . $warehouse->id, (isset($_POST['rack_' . $warehouse->id]) ? $_POST['rack_' . $warehouse->id] : ''), 'class="form-control selec_clear" id="rack_' . $warehouse->id . '" placeholder="' . lang('rack') . '"');
                                                            echo "</div></div>";
                                                        } else {
                                                            echo form_hidden('wh_' . $warehouse->id, $warehouse->id) . form_input('wh_qty_' . $warehouse->id, (isset($_POST['wh_qty_' . $warehouse->id]) ? $_POST['wh_qty_' . $warehouse->id] : ''), 'class="form-control selec_clear" id="wh_qtys" placeholder="' . lang('quantity') . '" ng-model="prod.qty" ng-change="getProdBarcode();" onlyno');
//                                                        echo form_hidden('wh_' . $warehouse->id, $warehouse->id) . form_input('wh_qty_' . $warehouse->id, (isset($_POST['wh_qty_' . $warehouse->id]) ? $_POST['wh_qty_' . $warehouse->id] : ''), 'class="form-control selec_clear" id="wh_qty_' . $warehouse->id . '" placeholder="' . lang('quantity') . '" ng-change="getProdBarcode();" ng-model="prod.qty" onlyno');
                                                        }


//                                    
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("unit_per", "unit") ?>
                                                    <?= form_input('unit', (isset($_POST['unit']) ? $_POST['unit'] : ($product ? $product->uper : '')), 'class="form-control selec_clear" id="unit" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" required="required" style="width:100%" '); ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row standard">
                                            <div class="col-md-8">
                                                <div class="form-group all">
                                                    <?= form_input('squantity', (isset($_POST['price']) ? $_POST['price'] : ($product ? $this->sma->formatDecimal($product->price) : '')), 'class="form-control selec_clear tip" id="squantity"') ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= form_input('sunit', (isset($_POST['sunit']) ? $_POST['sunit'] : ($product ? $product->uper : '')), 'class="form-control selec_clear" id="sunit" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" style="width:100%" '); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <script>
                                            $('#squantity').change(function () {
                                                var squantity = $('#squantity').val();
                                                var wh_qtys = $('#wh_qtys').val();
                                                if (wh_qtys % squantity != 0 && squantity != "") {
                                                    bootbox.alert('Please Enter Valid Secondary Quentity');
                                                    $('#squantity').val("");
                                                }
                                            });
                                        </script>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group all">
                                                    <?= lang("product_cost", "cost") ?>
                                                    <?= form_input('cost', (isset($_POST['cost']) ? $_POST['cost'] : ($product ? $this->sma->formatDecimal($product->cost) : '')), 'class="form-control selec_clear tip" id="cost" required="required" ng-model="prod.cost" ng-blur="getProductMargin()" ng-change="getProdBarcode();"') ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group all">
                                                    <?= lang("product_per", "cper") ?>
                                                    <?= form_input('cper', (isset($_POST['cper']) ? $_POST['cper'] : ($product ? $product->cper : '')), 'class="form-control selec_clear" id="cper" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" required="required" style="width:100%" '); ?>
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group all">
                                                    <?= lang("product_price", "price") ?>
                                                    <?= form_input('price', (isset($_POST['price']) ? $_POST['price'] : ($product ? $this->sma->formatDecimal($product->price) : '')), 'class="form-control selec_clear tip" id="price" ng-model="prod.price" ng-change="getProdBarcode();" ') ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group all">
                                                    <?= lang("product_per", "pper") ?>
                                                    <div class="input-group col-md-12">
                                                        <?= form_input('pper', (isset($_POST['pper']) ? $_POST['pper'] : ($product ? $product->pper : '')), 'class="form-control selec_clear" id="pper" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per"  style="width:100%"'); ?>
                                                        <div class="input-group-addon no-print hidden">
                                                            <a href="<?php echo site_url('system_settings/AddProduct_para/per'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=" standard ">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" <?= (isset($_POST['ratetype']) && $_POST['ratetype'] == "MRP" ? 'checked' : 'checked') ?> name="ratetype"  value="MRP"/>&nbsp;&nbsp;&nbsp;&nbsp;MRP</div>
                                                        <div class="col-md-3 form-group">
                                                            <?php echo form_input('mrprate', (isset($_POST['mrprate']) ? $_POST['mrprate'] : ''), 'class="form-control selec_clear" id="mrprate" placeholder="" style="width:100%;"'); ?>
                                                        </div>
                                                        <!-- <div class="col-md-3">
                                                        <?= lang("Rate", "singlerate") ?>   
                                                        </div>
                                                        <div class="col-md-1 hidden">
                                                        <?= lang("Per", "Per") ?> </div>
                                                        <div class="col-md-4"><?= form_input('rateper', (isset($_POST['rateper']) ? $_POST['rateper'] : ($product ? $product->rateper : '')), 'class="form-control selec_clear" default-attrib data-tab="per" id="rateper" placeholder="' . lang("select") . " " . lang("Unit") . '" required="required" style="width:100%" '); ?>
                                                        </div> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="hidden">
                                                <div class="row ">
                                                    <div class="col-md-6 form-group"> 
                                                        <?= lang('hsncode', 'hsncode') ?>
                                                        <div class="input-group">
                                                            <?= form_input('hsnno', (isset($_POST['hsnno']) ? $_POST['hsnno'] : $product->price), 'class="form-control tip" id="hsnno" ng-model="prod.hsnno" readonly') ?>
                                                            <div class="input-group-addon no-print">
                                                                <a href="<?php echo site_url('gst/add'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 form-group">
                                                        <?= lang('gstno', 'gstno') ?>
                                                        <?= form_input('gstno', (isset($_POST['gstno']) ? $_POST['gstno'] : $product->price), 'class="form-control tip" id="gstno" readonly ng-model="prod.gstno"  ng-blur="getProductMargin()"') ?>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <?= lang('cess', 'cess') ?>
                                                        <?= form_input('cess', (isset($_POST['cess']) ? $_POST['cess'] : $product->price), 'class="form-control tip" id="cess" readonly ng-model="prod.cess"  ng-blur="getProductMargin()"') ?>
                                                    </div>
                                                </div>

                                                <div class="row form-group">
                                                    <div class="col-md-8"> &nbsp;&nbsp;<input type="checkbox" icheck class="checkbox" id="addupgetcheck" ng-model="prod.addupgst" name="addupgst">&nbsp;&nbsp;&nbsp;&nbsp;Add-up GST if MRP exceeds 1000+ (%)</div>
                                                    <div class="col-md-4">
                                                        <?= form_input('addupgstmrp', (isset($_POST['addupgstmrp']) ? $_POST['addupgstmrp'] : $product->price), 'class="form-control tip" id="addupgstmrp" ng-model="prod.addupgstmrp" readonly ng-blur="getProductMargin()"') ?>
                                                    </div>
                                                </div>

                                            </div>


                                            <br/>
                                            <div class="row">
                                                <!--<div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" <?= (isset($_POST['ratetype']) && $_POST['ratetype'] == "Single" ? 'checked' : '') ?> name="ratetype" value="Single"/>&nbsp;&nbsp;&nbsp;&nbsp;Single</div>-->
                                                <!--<div class="col-md-3"><?php echo form_input('singlerate', (isset($_POST['singlerate']) ? $_POST['singlerate'] : ''), 'class="form-control selec_clear" id="singlerate" placeholder="" style="width:100%;" '); ?></div>-->

                                            </div>
                                            <br/>
                                            <div class="row hidden">
                                                <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" <?= (isset($_POST['ratetype']) && $_POST['ratetype'] == "Multiple" ? 'checked' : '') ?> name="ratetype" value="Multiple"/>&nbsp;&nbsp;&nbsp;&nbsp;Multiple</div>
                                                <div class="col-md-3">
                                                    <div class="col-md-12" style="padding: 0px"><?php echo form_input('mulratef', (isset($_POST['mulratef']) ? $_POST['mulratef'] : ''), 'class="form-control selec_clear" id="mulratef" placeholder="" style="width:100%;"'); ?></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-md-3">To</div>
                                                <div class="col-md-3" ng-init='size2 =<?= json_encode($sizes) ?>'>
                                                    <div class="col-md-12" style="padding: 0px"><?php echo form_input('mulratet', (isset($_POST['mulratet']) ? $_POST['mulratet'] : ''), 'class="form-control selec_clear" id="mulratet" placeholder="" style="width:100%;"'); ?></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="combo" style="display:none;">
                                            <div class="form-group">
                                                <?= lang("add_product", "add_item") . ' (' . lang('not_with_variants') . ')'; ?>
                                                <?php echo form_input('add_item', '', 'class="form-control selec_clear ttip" id="add_item" data-placement="top" data-trigger="focus" data-bv-notEmpty-message="' . lang('please_add_items_below') . '" placeholder="' . $this->lang->line("add_item") . '"'); ?>
                                            </div>
                                            <div class="control-group table-group">
                                                <label class="table-label" for="combo">{{prod.protype}} <?= lang("products"); ?></label>
                                                <div class="controls table-controls">
                                                    <table id="prTable" class="table items table-striped table-bordered table-condensed table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th class="col-md-5 col-sm-5 col-xs-5"><?= lang("product_name") . " (" . $this->lang->line("product_code") . ")"; ?></th>
                                                                <th class="col-md-2 col-sm-2 col-xs-2"><?= lang("quantity"); ?></th>
                                                                <th class="col-md-3 col-sm-3 col-xs-3"><?= lang("unit_price"); ?></th>
                                                                <th class="col-md-1 col-sm-1 col-xs-1 text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="digital" style="display:none;">
                                            <div class="form-group digital">
                                                <?= lang("digital_file", "digital_file") ?>
                                                <input id="digital_file" type="file" name="digital_file" data-show-upload="false"
                                                       data-show-preview="false" class="form-control selec_clear file">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <?php // echo form_submit('add_product', "Save", 'class="btn btn-primary" ');     ?>
                                        </div>
                                    </div>
                                </form>
                                <?php // echo form_close();     ?>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" id="addItemManually"><?= lang('submit') ?></button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!--            <div class="modal-footer">
                            <button type="button" class="btn btn-primary" id="addUnlabelledItemManually"><?= lang('submit') ?></button>
                        </div>-->
        </div>
    </div>

</div>
<script type="text/javascript" src="<?= $assets ?>js/bootstrapValidator.min.js"></script>


<script type="text/javascript">
                                                    $(document).ready(function () {


                                                        $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
                                                            placeholder: "<?= lang('select_category_to_load') ?>", data: [
                                                                {id: '', text: '<?= lang('select_category_to_load') ?>'}
                                                            ]
                                                        });
                                                        $('#category').change(function () {
                                                            var v = $(this).val();
                                                            $('#modal-loading').show();
                                                            if (v) {
                                                                $.ajax({
                                                                    type: "get",
                                                                    async: false,
                                                                    url: "<?= site_url('products/getSubCategories') ?>/" + v,
                                                                    dataType: "json",
                                                                    success: function (scdata) {
                                                                        if (scdata != null) {
                                                                            $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                                                                                placeholder: "<?= lang('select_category_to_load') ?>",
                                                                                data: scdata
                                                                            });
                                                                        }
                                                                    },
                                                                    error: function () {
                                                                        bootbox.alert('<?= lang('ajax_error') ?>');
                                                                        $('#modal-loading').hide();
                                                                    }
                                                                });
                                                            } else {
                                                                $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
                                                                    placeholder: "<?= lang('select_category_to_load') ?>",
                                                                    data: [{id: '', text: '<?= lang('select_category_to_load') ?>'}]
                                                                });
                                                            }
                                                            $('#modal-loading').hide();
                                                        });
                                                        $('#code').bind('keypress', function (e) {
                                                            if (e.keyCode == 13) {
                                                                e.preventDefault();
                                                                return false;
                                                            }
                                                        });
                                                        $('#color').change(function () {
                                                            var v = $(this).val();
                                                            $.ajax({
                                                                type: "get",
                                                                async: false,
                                                                url: "<?= site_url('products/getColorCode') ?>/" + v,
                                                                dataType: "json",
                                                                success: function (scdata) {
                                                                    //                        alert(scdata);
                                                                    if (scdata != null) {
                                                                        var code = scdata.code;
                                                                        $('#codet').val(code);
                                                                        var quantity = $('#colorqty').val();
                                                                        $('#colorcode').val(code + quantity);
                                                                        $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                                                                            placeholder: "<?= lang('select_category_to_load') ?>",
                                                                            data: scdata
                                                                        });
                                                                    }
                                                                },
                                                                error: function () {
                                                                    bootbox.alert('<?= lang('ajax_error') ?>');
                                                                    $('#modal-loading').hide();
                                                                }
                                                            });
                                                        });
                                                        $('#colorqty').change(function () {
                                                            $('#colorcode').attr("value", "");
                                                            var codet = $('#codet').val();
                                                            //        var quantity = $('#colorqty').val();
                                                            var qty = $('#colorqty').val();
                                                            $('#colorcode').attr("value", codet + qty);
                                                        })

                                                        $('#wh_qtys').change(function () {

                                                            var v = $('#addproduct').serialize();
                                                            var qty = $('#wh_qtys').val();
                                                            $('#modal-loading').show();
                                                            $.ajax({
                                                                type: "get",
                                                                data: v,
                                                                async: false,
                                                                url: "<?= site_url('products/getProSquentity') ?>?" + v,
                                                                dataType: "json",
                                                                success: function (scdata) {
                                                                    if (scdata != 0) {
                                                                        var sqty = parseInt(qty) / parseInt(scdata.no_of_pic);
                                                                        $('#squantity').val(Number(Math.round(sqty)));
                                                                    }
                                                                    //                else{
                                                                    //                    $('#squantity').val('');
                                                                    //                }
                                                                },
                                                                error: function () {
                                                                    bootbox.alert('<?= lang('ajax_error') ?>');
                                                                    $('#modal-loading').hide();
                                                                }
                                                            });
                                                            $('#modal-loading').hide();
                                                        });
                                                        //        if (store = localStorage.getItem('store')) {
                                                        //            $('#store').value(store);
                                                        //        }
                                                    });
                                                    $(document).on('click', '#addManually', function (e) {
                                                        $(".selec_clear").select2("val", "");
                                                        $('#mModal').appendTo("body").modal('show');
                                                        return false;
                                                    });</script>



<script>
    $('#store').change(function () {
        var v = $(this).val();
        $('#modal-loading').show();
        if (v) {
            $.ajax({
                type: "get",
                async: false,
                data: {store: v},
                url: "<?= site_url('purchases/getBiller') ?>",
                dataType: "json",
                success: function (scdata) {
                    $('#list_suppliers').html('');
                    $('#list_suppliers').removeAttr('readonly', '');
                    $('#list_suppliers').append('<option value="">Please Select Supplier Code</option>');
                    $.each(scdata, function (k, val) {
                        var opt = $('<option />');
                        opt.val(val.id);
                        opt.text(val.code + ' - ' + val.company);
                        $('#list_suppliers').append(opt);
                    })
                    $('#modal-loading').hide();
                },
                error: function () {
                    $('#list_suppliers').attr('readonly', '');
                    $('#modal-loading').hide();
                }
            });
        }
    });
    $('#addItemManually').click(function () {
//         add_product_form.submit();
        var v = $('#addproduct').serialize();
        $('#modal-loading').show();
        $.ajax({
            type: "get",
            async: false,
            url: "<?= site_url('products/saveAddOrderProduct') ?>?" + v,
            dataType: "json",
            success: function (scdata) {
                if (scdata.status == 1) {
                    $.each(scdata.data, function (k, val) {
                        var mid = val.id,
                                mcode = val.code,
                                mname = val.name,
                                mqty = val.quantity,
                                sqty = val.squantity,
                                unit_price = val.cost,
                                design = val.design_name,
                                brandname = val.brandname,
                                colorname = val.colorname,
                                designname = val.designname,
                                stylename = val.stylename,
                                patternname = val.patternname,
                                fittingname = val.fittingname,
                                fabricname = val.fabricname,
                                rate = val.rate,
                                uname = val.uname;
                        quitems[mid] = {"id": mid, "item_id": mid, "label": mname, "row": {"id": mid, "code": mcode, "page": 0, "cost": unit_price, "name": mname, "quantity": mqty, "squantity": sqty, "design_name": design, "real_unit_cost": unit_price, "price": unit_price, "unit_price": unit_price, "real_unit_price": unit_price, "tax_rate": 0, "tax_method": 0, "qty": mqty, "type": "manual", "discount": 0, "serial": "", "option": "", "brandname": brandname, "colorname": colorname, "designname": designname, "stylename": stylename, "patternname": patternname, "fittingname": fittingname, "fabricname": fabricname, "rate": rate, "uname": uname}, "tax_rate": 0, "options": false};
                        localStorage.setItem('quitems', JSON.stringify(quitems));
                        loadItems();
                    });
//                    $.each(scdata.data, function (k, val) {
//                        var mid = val.id,
//                                mcode = val.code,
//                                mname = val.name,
//                                mqty = val.quantity,
//                                sqty = val.squantity,
//                                unit_price = val.cost
//                        quitems[mid] = {"id": mid, "item_id": mid, "label": mname, "row": {"id": mid, "code": mcode, "page": 0, "cost": unit_price, "name": mname, "quantity": mqty, "squantity": sqty, "real_unit_cost": unit_price, "price": unit_price, "unit_price": unit_price, "real_unit_price": unit_price, "tax_rate": 0, "tax_method": 0, "qty": mqty, "type": "manual", "discount": 0, "serial": "", "option": ""}, "tax_rate": 0, "options": false};
//                        localStorage.setItem('quitems', JSON.stringify(quitems));
//                        loadItems();
//                    });
                    $('#mModal').modal('hide');
                    $('#mcode').val('');
                    $('#mname').val('');
                    $('#mtax').val('');
                    $('#mquantity').val('');
                    $('#mdiscount').val('');
                    $('#mprice').val('');
                    return false;
                } else {
                    alert('Please Select All required fields');
                }
            },
        });
        $('#modal-loading').hide();
    });</script>

<!--<script>
    $(document).ready(function () {
//        $('#user_id').append('<option value="" selected="selected">Select Order By</option>');
        $('#list_suppliers').append('<option value="" selected="selected">Select Supplier Code</option>');

    })
    $('#store').change(function () {

        var v = $(this).val();
        $('#modal-loading').show();
        if (v) {
            $.ajax({
                type: "get",
                async: false,
                data: {store: v},
                url: "<?= site_url('purchases/getBiller') ?>",
                dataType: "json",
                success: function (scdata) {
                    $('#list_suppliers').html('');
                    $('#list_suppliers').removeAttr('readonly', '');
                    $('#list_suppliers').append('<option value="">Please Select Supplier Code</option>');
                    $.each(scdata, function (k, val) {
                        var opt = $('<option />');
                        opt.val(val.id);
                        opt.text(val.code + ' - ' + val.company);
                        $('#list_suppliers').append(opt);
                    })
                    $('#modal-loading').hide();
                },
                error: function () {
                    $('#list_suppliers').attr('readonly', '');
                    $('#modal-loading').hide();
                }
            });

            $.ajax({
                type: "get",
                async: false,
                data: {store: v},
                url: "<?= site_url('purchases/getEmployees') ?>",
                dataType: "json",
                success: function (scdata) {
                    
                    $('#user_id').html('');
                    $('#user_id').removeAttr('readonly', '');
                    $('#user_id').append('<option value="">Please Select Order By</option>');
                    $.each(scdata, function (k, val) {
                        var opt = $('<option />');
                        opt.val(val.id);
                        opt.text(val.fname + ' ' + val.mname + ' ' + val.lname);
                        $('#user_id').append(opt);
                    })
                }, error: function () {
//                    $('#booked_by').addClass('hidden');
                    $('#user_id').attr('readonly', '');
                    $('#modal-loading').hide();
                }
            })
        }
    });


    $('#list_suppliers').change(function () {
        var su = $('#list_suppliers').val();
        var supp_code = $('#list_suppliers option:selected').text();
        $('#supplier_name').attr('placeholder', supp_code);
        $('#supplier12').val(su);
    });
</script>-->

<script type="text/javascript">
    $(document).ready(function () {

        var su = $('#list_suppliers').val();
        var supp_code = $('#list_suppliers option:selected').text();
        $('#supplier_name').attr('placeholder', supp_code);
        $('#supplier12').val(su);
        $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
            placeholder: "<?= lang('select_category_to_load') ?>", data: [
                {id: '', text: '<?= lang('select_category_to_load') ?>'}
            ]
        });
        $('#category').change(function () {
            var v = $(this).val();
            $('#modal-loading').show();
            if (v) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: "<?= site_url('products/getSubCategories') ?>/" + v,
                    dataType: "json",
                    success: function (scdata) {
                        if (scdata != null) {
                            $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                                placeholder: "<?= lang('select_category_to_load') ?>",
                                data: scdata
                            });
                        }
                    },
                    error: function () {
                        bootbox.alert('<?= lang('ajax_error') ?>');
                        $('#modal-loading').hide();
                    }
                });
            } else {
                $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
                    placeholder: "<?= lang('select_category_to_load') ?>",
                    data: [{id: '', text: '<?= lang('select_category_to_load') ?>'}]
                });
            }
            $('#modal-loading').hide();
        });
        $('#code').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                return false;
            }
        });
        $('#color').change(function () {
            var v = $(this).val();
            $.ajax({
                type: "get",
                async: false,
                url: "<?= site_url('products/getColorCode') ?>/" + v,
                dataType: "json",
                success: function (scdata) {
                    //                        alert(scdata);
                    if (scdata != null) {
                        var code = scdata.code;
                        $('#codet').val(code);
                        var quantity = $('#colorqty').val();
                        $('#colorcode').val(code + quantity);
                        $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                            placeholder: "<?= lang('select_category_to_load') ?>",
                            data: scdata
                        });
                    }
                },
                error: function () {
                    bootbox.alert('<?= lang('ajax_error') ?>');
                    $('#modal-loading').hide();
                }
            });
        });
        $('#colorqty').change(function () {
            $('#colorcode').attr("value", "");
            var codet = $('#codet').val();
            //        var quantity = $('#colorqty').val();
            var qty = $('#colorqty').val();
            $('#colorcode').attr("value", codet + qty);
        })

        $('#wh_qtys').change(function () {
            var v = $('#addproduct').serialize();
            var qty = $('#wh_qtys').val();
            $('#modal-loading').show();
            $.ajax({
                type: "get",
                data: v,
                async: false,
                url: "<?= site_url('products/getProSquentity') ?>?" + v,
                dataType: "json",
                success: function (scdata) {
                    if (scdata != 0) {
                        var sqty = parseInt(qty) / parseInt(scdata.no_of_pic);
                        $('#squantity').val(Number(Math.round(sqty)));
                    }
                    //                else{
                    //                    $('#squantity').val('');
                    //                }
                },
                error: function () {
                    bootbox.alert('<?= lang('ajax_error') ?>');
                    $('#modal-loading').hide();
                }
            });
            $('#modal-loading').hide();
        });
        //        if (store = localStorage.getItem('store')) {
        //            $('#store').value(store);
        //        }
    });
    $('#list_suppliers').change(function () {
        var su = $('#list_suppliers').val();
        var supp_code = $('#list_suppliers option:selected').text();
        $('#supplier_name').attr('placeholder', supp_code);
        $('#supplier12').val(su);
    });
</script>