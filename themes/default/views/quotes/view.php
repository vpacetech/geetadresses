<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-file"></i><?= lang("quote_no") . '. ' . $inv->id; ?> </h2>

        <!--        <div class="box-icon">
                    <ul class="btn-tasks">
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip"
                                                                                          data-placement="left"
                                                                                          title="<?= lang("actions") ?>"></i></a>
                            <ul class="dropdown-menu pull-right" class="tasks-menus" role="menu" aria-labelledby="dLabel">
        <?php if ($inv->attachment) { ?>
                                                <li>
                                                    <a href="<?= site_url('welcome/download/' . $inv->attachment) ?>">
                                                        <i class="fa fa-chain"></i> <?= lang('attachment') ?>
                                                    </a>
                                                </li>
        <?php } ?>
                                <li><a href="<?= site_url('quotes/edit/' . $inv->id) ?>"><i
                                            class="fa fa-edit"></i> <?= lang('edit_quote') ?></a></li>
                                <li><a href="<?= site_url('sales/add/' . $inv->id) ?>"><i
                                            class="fa fa-plus-circle"></i> <?= lang('create_invoice') ?></a></li>
                                <li><a href="<?= site_url('quotes/email/' . $inv->id) ?>" data-target="#myModal"
                                       data-toggle="modal"><i class="fa fa-envelope-o"></i> <?= lang('send_email') ?></a></li>
                                <li><a href="<?= site_url('quotes/pdf/' . $inv->id) ?>"><i
                                            class="fa fa-file-pdf-o"></i> <?= lang('export_to_pdf') ?></a></li>
                                <li><a href="<?= site_url('quotes/excel/' . $inv->id) ?>"><i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>-->
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="print-only col-xs-12">
                    <img src="<?= base_url() . 'assets/uploads/logos/' . $biller->logo; ?>"
                         alt="<?= $biller->company != '-' ? $biller->company : $biller->name; ?>">
                </div>
                <!--                <div class="well well-sm">
                                    <div class="col-xs-4 border-right">
                
                                        <div class="col-xs-2"><i class="fa fa-3x fa-building padding010 text-muted"></i></div>
                                        <div class="col-xs-10">
                                            <h2 class=""><?= $biller->company != '-' ? $biller->company : $biller->name; ?></h2>
                <?= $biller->company ? "" : "Attn: " . $biller->name ?>
                <?php
                echo $biller->address . "<br>" . $biller->city . " " . $biller->postal_code . " " . $biller->state . "<br>" . $biller->country;
                echo "<p>";
                if ($biller->cf1 != "-" && $biller->cf1 != "") {
                    echo "<br>" . lang("bcf1") . ": " . $biller->cf1;
                }
                if ($biller->cf2 != "-" && $biller->cf2 != "") {
                    echo "<br>" . lang("bcf2") . ": " . $biller->cf2;
                }
                if ($biller->cf3 != "-" && $biller->cf3 != "") {
                    echo "<br>" . lang("bcf3") . ": " . $biller->cf3;
                }
                if ($biller->cf4 != "-" && $biller->cf4 != "") {
                    echo "<br>" . lang("bcf4") . ": " . $biller->cf4;
                }
                if ($biller->cf5 != "-" && $biller->cf5 != "") {
                    echo "<br>" . lang("bcf5") . ": " . $biller->cf5;
                }
                if ($biller->cf6 != "-" && $biller->cf6 != "") {
                    echo "<br>" . lang("bcf6") . ": " . $biller->cf6;
                }
                echo "</p>";
                echo lang("tel") . ": " . $biller->phone . "<br>" . lang("email") . ": " . $biller->email;
                ?>
                                        </div>
                                        <div class="clearfix"></div>
                
                                    </div>
                                    <div class="col-xs-4 border-right">
                
                                        <div class="col-xs-2"><i class="fa fa-3x fa-user padding010 text-muted"></i></div>
                                        <div class="col-xs-10">
                                            <h2 class=""><?= $customer->company ? $customer->company : $customer->name; ?></h2>
                <?= $customer->company ? "" : "Attn: " . $customer->name ?>
                <?php
                echo $customer->address . "<br>" . $customer->city . " " . $customer->postal_code . " " . $customer->state . "<br>" . $customer->country;
                echo "<p>";
                if ($customer->cf1 != "-" && $customer->cf1 != "") {
                    echo "<br>" . lang("ccf1") . ": " . $customer->cf1;
                }
                if ($customer->cf2 != "-" && $customer->cf2 != "") {
                    echo "<br>" . lang("ccf2") . ": " . $customer->cf2;
                }
                if ($customer->cf3 != "-" && $customer->cf3 != "") {
                    echo "<br>" . lang("ccf3") . ": " . $customer->cf3;
                }
                if ($customer->cf4 != "-" && $customer->cf4 != "") {
                    echo "<br>" . lang("ccf4") . ": " . $customer->cf4;
                }
                if ($customer->cf5 != "-" && $customer->cf5 != "") {
                    echo "<br>" . lang("ccf5") . ": " . $customer->cf5;
                }
                if ($customer->cf6 != "-" && $customer->cf6 != "") {
                    echo "<br>" . lang("ccf6") . ": " . $customer->cf6;
                }
                echo "</p>";
                echo lang("tel") . ": " . $customer->phone . "<br>" . lang("email") . ": " . $customer->email;
                ?>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="col-xs-2"><i class="fa fa-3x fa-building-o padding010 text-muted"></i></div>
                                        <div class="col-xs-10">
                                            <h2 class=""><?= $Settings->site_name; ?></h2>
                <?= $warehouse->name ?>
                <?php
                echo $warehouse->address . "<br>";
                echo ($warehouse->phone ? lang("tel") . ": " . $warehouse->phone . "<br>" : '') . ($warehouse->email ? lang("email") . ": " . $warehouse->email : '');
                ?>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>-->
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-4 col-md-offset-5 text-uppercase" style="font-size: 20px; font-weight: bold"><?= $Settings->site_name ?></div>
                </div>
                <div class="clearfix"></div>

                <div class="col-xs-6 pull-right">
                    <div class="col-xs-12 text-right">
                        <!--                        <?php $br = $this->sma->save_barcode($inv->reference_no, 'code39', 70, false); ?>
                                                <img src="<?= base_url() ?>assets/uploads/barcode<?= $this->session->userdata('user_id') ?>.png"
                                                     alt="<?= $inv->reference_no ?>"/>
                        <?php $this->sma->qrcode('link', urlencode(site_url('quotes/view/' . $inv->id)), 2); ?>
                                                <img src="<?= base_url() ?>assets/uploads/qrcode<?= $this->session->userdata('user_id') ?>.png"
                                                     alt="<?= $inv->reference_no ?>"/>-->
                        <p style="font-weight:bold;"><?= lang("user"); ?>
                            : <?php echo $user->fname . ' ' . $user->lname; ?>

                        <p style="font-weight:bold;"><?= lang("order_date"); ?>
                            : <?= $this->sma->hrld($inv->date); ?>
                            <?php if ($inv->estimate_delievery) { ?>
                            <p style="font-weight:bold;"><?= lang("est_delievery"); ?>
                                : <?=
                                $this->sma->hrsd($inv->estimate_delievery);
                            }
                            ?>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="col-xs-6">
                    <div class="col-xs-2"><i class="fa fa-3x fa-file-text-o padding010 text-muted"></i></div>
                    <div class="col-xs-10">
                        <h2 class=""><?= lang("pur_ord_num"); ?>: <?= $inv->reference_no; ?></h2>
                        <p style="font-weight:bold;"><?= lang("supplier"); ?>
                            : <?= $customer->company != '-' ? $customer->company : $customer->company; ?></p>
                        <p style="font-weight:bold;"><?= lang("thrugh_transport"); ?>
                            : <?= $transport->text ?></p>
<!--                        <p style="font-weight:bold;"><?= lang("date"); ?>
                            : <?= $this->sma->hrld($inv->date); ?></p>-->
                        <!--<p style="font-weight:bold;"><?= lang("status"); ?>: <?= $inv->status; ?></p>-->
                        <p>&nbsp;</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <div class="table-responsive" style="overflow-x:auto;">
                    <table class="table table-bordered table-hover table-striped print-table order-table">
                        <thead>
                            <tr>
                                <th><?= lang("no"); ?></th>
                                <th><?= lang("Dept.") ?>
                                <th><?= lang("Product") ?>
                                <th><?= lang("Type") ?>
                                <th><?= lang("Brands") ?>
                                <th><?= lang("Design") ?>
                                <th><?= lang("Style") ?>
                                <th><?= lang("Fitting") ?>
                                <th><?= lang("Pattern") ?>
                                <th><?= lang("Fabric") ?>
                                <th><?= lang("Pur. Rate") ?>
                                <th><?= lang("MRP") ?> </th>
                                <th><?= lang('from_size') ?></th>
                                <th><?= lang('to_size') ?></th>
                                <th><?= lang('no_of_colors') ?></th>
                                <th><?= lang("quantity"); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $r = 1;
                            $sq = 0;
                           $total_quantity = 0;
                            foreach ($rows as $row):
                                $unit_name = $row->unit_name;

                                $total_quantity = $row->total_quantity + $total_quantity;
                                $sq = intval($row->sec_qty+$sq);
//                                $sq = intval($row->sec_qty);
                                $row->prod = $this->site->getProductByIDOrder($row->product_id, $row->product_code);
//                                $row->prod = $this->site->getProductByID($row->product_id, $row->product_code);
                                
                                if ($row->sunit == '5') {
                                    $sunit = '5';
                                } else if ($row->sunit == '1') {
                                    $sunit = 'Dozen';
                                } else if ($row->sunit == '7') {
                                    $sunit = 'Bundles';
                                }
                                ?>
                                <tr>
                                    <td style="text-align:center;vertical-align:middle;"><?= $r; ?></td>

                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->department, 'department')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->product_items, 'product_items')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->type_id, 'type')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->brands, 'brands')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->design, 'design')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->style, 'style')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->fitting, 'fitting')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->pattern, 'pattern')->name ?></td>
                                    <td><?= $this->site->getParaById('name', 'id', $row->prod->fabric, 'fabric')->name ?></td>
                                    <td style="text-align:right;padding-right:10px;"><?= $this->sma->formatMoney($row->net_unit_price); ?></td>
                                    <td><?= $this->sma->formatMoney($row->prod->price) ?></td>
                                    <td><?= $row->from_size.' '.$row->prod->sizeangle ?></td>
                                    <td><?= $row->to_size.' '.$row->prod->sizeangle ?></td>
                                    <td><?= $row->total_colors ?></td>
    
                                    <td style="text-align:center; vertical-align:middle;"><?= $row->total_quantity ?></td>
                                    <?php
                                    if ($Settings->tax1) {
                                        echo '<td style="text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 && $row->tax_code ? '<small>(' . $row->tax_code . ')</small> ' : '') . $this->sma->formatMoney($row->item_tax) . '</td>';
                                    }
                                    if ($Settings->product_discount && $inv->product_discount != 0) {
                                        echo '<td style="text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($row->item_discount) . '</td>';
                                    }
                                    ?>
                                </tr>
                                <?php
                                $r++;
                            endforeach;
                            ?>


                        </tbody>

                        <tfoot>
                            <?php
                            $col = 16;
                            if ($Settings->product_discount && $inv->product_discount != 0) {
                                $col++;
                            }
                            if ($Settings->tax1) {
                                $col++;
                            }
                            if ($Settings->product_discount && $inv->product_discount != 0 && $Settings->tax1) {
                                $tcol = $col - 2;
                            } elseif ($Settings->product_discount && $inv->product_discount != 0) {
                                $tcol = $col - 1;
                            } elseif ($Settings->tax1) {
                                $tcol = $col - 1;
                            } else {
                                $tcol = $col;
                            }
                            ?>
<!--                        <tr>
                            <td colspan="<?= $tcol; ?>" style="text-align:right; padding-right:10px;"><?= lang("total"); ?>
                                (<?= $default_currency->code; ?>)
                            </td>
                            <?php
                            if ($Settings->tax1) {
                                echo '<td style="text-align:right;">' . $this->sma->formatMoney($inv->product_tax) . '</td>';
                            }
                            if ($Settings->product_discount && $inv->product_discount != 0) {
                                echo '<td style="text-align:right;">' . $this->sma->formatMoney($inv->product_discount) . '</td>';
                            }
                            ?>
                            <td style="text-align:right; padding-right:10px;"><?= $this->sma->formatMoney($inv->total + $inv->product_tax); ?></td>
                            
                        </tr>-->
                            <?php
                            if ($inv->order_discount != 0) {
                                echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("order_discount") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($inv->order_discount) . '</td></tr>';
                            }
                            if ($Settings->tax2 && $inv->order_tax != 0) {
                                echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("order_tax") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($inv->order_tax) . '</td></tr>';
                            }
                            if ($inv->shipping != 0) {
                                echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("shipping") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($inv->shipping) . '</td></tr>';
                            }
                            ?>
<!--                        <tr>
                            <td colspan="<?= $col; ?>"
                                style="text-align:right; padding-right:10px; font-weight:bold;"><?= lang("total_amount"); ?>
                                (<?= $default_currency->code; ?>)
                            </td>
                            <td style="text-align:right; padding-right:10px; font-weight:bold;"><?= $this->sma->formatMoney($inv->grand_total); ?></td>
                        </tr>-->

                            <tr>
                                <td colspan="<?= $col - 11; ?>" style="padding-right:10px; font-weight:bold;" class="text-center">&nbsp;</td>
                                <td colspan="3" style="padding-right:10px; font-weight:bold;" class="text-center">&nbsp;</td>
    <!--                            <td colspan="3" style="text-align:right; padding-right:10px; font-weight:bold;"><?= lang("total_quantity"); ?></td>
                                <td colspan="2" style="text-align:right; padding-right:10px; font-weight:bold;"><?= $tq; ?></td>-->
                                <td colspan="4" style="padding-right:10px; font-weight:bold;" class="text-center"><?= lang("total_pcs"); ?></td>
                                <td colspan="3" style="padding-right:10px; font-weight:bold;" class="text-center"><?= $total_quantity; ?></td>
                                <td colspan="3" style="padding-right:10px; font-weight:bold;" class="text-center"><?= lang("pcs"); ?></td>
                            </tr>

<!--                        <tr>
    <td colspan="<?= $col; ?>"
        style="text-align:right; padding-right:10px; font-weight:bold;"><?= lang("total_quantity"); ?>
    </td>
    <td style="text-align:right; padding-right:10px;"><?= $tq; ?></td>
</tr>-->
                            <tr>
                                <td colspan="<?= $col - 11; ?>" style="padding-right:10px; font-weight:bold;" class="text-center"><?= lang("total_ord"); ?></td>
                                <td colspan="3" style="padding-right:10px; font-weight:bold;" class="text-center"> (<?= $default_currency->code; ?>) <?= $this->sma->formatMoney($inv->grand_total); ?></td>
    <!--                            <td colspan="3" style="text-align:right; padding-right:10px; font-weight:bold;"><?= lang("total_quantity"); ?></td>
                                <td colspan="2" style="text-align:right; padding-right:10px; font-weight:bold;"><?= $tq; ?></td>-->
                                <td colspan="4" style="padding-right:10px; font-weight:bold;" class="text-center"><?= lang('sec_qty'); ?></td>
                                <td colspan="3" style="padding-right:10px; font-weight:bold;" class="text-center"><?= $sq; ?></td>
                                <td colspan="3" style="padding-right:10px; font-weight:bold;" class="text-center"><?= lang($unit_name); ?></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>

                <div class="row">
                    <div class="col-xs-7">
<?php if ($inv->note || $inv->note != "") { ?>
                            <div class="well well-sm">
                                <p class="bold"><?= lang("note"); ?>:</p>

                                <div><?= $this->sma->decode_html($inv->note); ?></div>
                            </div>
<?php } ?>  
                    </div>

                    <div class="col-xs-4 col-xs-offset-1">
                        <div class="well well-sm">
                            <p><?= lang("created_by"); ?>
                                : <?= $created_by->first_name . ' ' . $created_by->last_name; ?> </p>

                            <p><?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?></p>
<?php if ($inv->updated_by) { ?>
                                <p><?= lang("updated_by"); ?>
                                    : <?=
                                    $updated_by->first_name . ' ' . $updated_by->last_name;
                                    ;
                                    ?></p>
                                <p><?= lang("update_at"); ?>: <?= $this->sma->hrld($inv->updated_at); ?></p>
<?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <?php if (!$Supplier || !$Customer) { ?>
            <div class="buttons">
    <?php if ($inv->attachment) { ?>
                    <div class="btn-group">
                        <a href="<?= site_url('welcome/download/' . $inv->attachment) ?>" class="tip btn btn-primary" title="<?= lang('attachment') ?>">
                            <i class="fa fa-chain"></i>
                            <span class="hidden-sm hidden-xs"><?= lang('attachment') ?></span>
                        </a>
                    </div>
    <?php } ?>
                <div class="btn-group btn-group-justified">
                    <div class="btn-group"><a href="<?= site_url('sales/add/' . $inv->id) ?>"
                                              class="tip btn btn-primary" title="<?= lang('create_invoice') ?>"><i
                                class="fa fa-plus-circle"></i> <span
                                class="hidden-sm hidden-xs"><?= lang('create_invoice') ?></span></a></div>
                    <div class="btn-group"><a href="<?= site_url('quotes/pdf/' . $inv->id) ?>"
                                              class="tip btn btn-primary" title="<?= lang('download_pdf') ?>"><i
                                class="fa fa-download"></i> <span class="hidden-sm hidden-xs"><?= lang('pdf') ?></span></a>
                    </div>
                    <div class="btn-group"><a href="<?= site_url('quotes/email/' . $inv->id) ?>" data-toggle="modal"
                                              data-target="#myModal" class="tip btn btn-info tip"
                                              title="<?= lang('email') ?>"><i class="fa fa-envelope-o"></i> <span
                                class="hidden-sm hidden-xs"><?= lang('email') ?></span></a></div>
                    <div class="btn-group"><a href="<?= site_url('quotes/edit/' . $inv->id) ?>"
                                              class="tip btn btn-warning tip" title="<?= lang('edit') ?>"><i
                                class="fa fa-edit"></i> <span class="hidden-sm hidden-xs"><?= lang('edit') ?></span></a>
                    </div>
                    <div class="btn-group"><a href="#" class="tip btn btn-danger bpo"
                                              title="<b><?= $this->lang->line("delete_quote") ?></b>"
                                              data-content="<div style='width:150px;'><p><?= lang('r_u_sure') ?></p><a class='btn btn-danger' href='<?= site_url('quotes/delete/' . $inv->id) ?>'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button></div>"
                                              data-html="true" data-placement="top"><i class="fa fa-trash-o"></i> <span
                                class="hidden-sm hidden-xs"><?= lang('delete') ?></span></a></div>
                </div>
            </div>
<?php } ?>
    </div>
</div>
