<?= $modal_js ?>
<!-- <script src="<?= $assets; ?>js/dropzone.min.js"></script> -->
<style>
   .has-error{
   border: 2px solid red;
   }
</style>
<style>
   fieldset.scheduler-border {
   border: 1px solid #ddd !important;
   padding: 0 1em 1em 1em !important;
   margin: 0 0 1.1em 0 !important;
   -webkit-box-shadow:  0px 0px 0px 0px #000;
   box-shadow:  0px 0px 0px 0px #000;
   }
   legend.scheduler-border {
   width:inherit; /* Or auto */
   padding:0 10px; /* To give a bit of padding on the left and right */
   border-bottom:none;
   }
   .cal 
   {
   position:relative;
   top:-30px;
   left:90%;
   }
   /*
   a {
   color: #000 !important;    
   }*/
   .nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
   color: #6164c0 !important;
   background-color: #fff !important;
   border-radius:0 !important;
   }
   .nav-pills>li>a{
   color: #6164c0 !important;
   background-color: #e9ebec !important;
   border-radius:0 !important;
   }
   .tab-content>.active {
   display: block;
   border : 1px solid #ccc;
   padding:5px !important;
   }
   .btton
   {
   padding: 5px;
   width:100%;
   }
   .stepwizard-step {
   display: table-cell;
   /*text-align: center;*/
   position: relative;
   }
   .form-horizontal .control-label {
   padding-top: 0px;
   }
</style>
<script type="text/javascript">
   var count = 1, an = 1, product_variant = 0, DT = <?= $Settings->default_tax_rate ?>,
           product_tax = 0, invoice_tax = 0, total_discount = 0, total = 0, shipping = 0,
           tax_rates = <?php echo json_encode($tax_rates); ?>;
   var audio_success = new Audio('<?= $assets ?>sounds/sound2.mp3');
   var audio_error = new Audio('<?= $assets ?>sounds/sound3.mp3');
   $(document).ready(function () {
   <?php if ($this->input->get('customer')) { ?>
           if (!localStorage.getItem('quitems')) {
               localStorage.setItem('list_suppliers', <?= $this->input->get('customer'); ?>);
           }
   <?php } ?>
   <?php if ($Owner || $Admin) { ?>
           if (!localStorage.getItem('qudate')) {
               $("#qudate").datetimepicker({
                   format: site.dateFormats.js_ldate,
                   fontAwesome: true,
                   language: 'sma',
                   weekStart: 1,
                   todayBtn: 1,
                   autoclose: 1,
                   todayHighlight: 1,
                   startView: 2,
                   forceParse: 0
               }).datetimepicker('update', new Date());
           }
           $(document).on('change', '#qudate', function (e) {
               localStorage.setItem('qudate', $(this).val());
           });
           if (qudate = localStorage.getItem('qudate')) {
               $('#qudate').val(qudate);
           }
           $(document).on('change', '#store', function (e) {
               localStorage.setItem('store', $(this).val());
           });
           if (store = localStorage.getItem('store')) {
               $('#store').val(store);
           }
           $(document).on('change', '#list_suppliers', function (e) {
               localStorage.setItem('list_suppliers', $(this).val());
           });
           if (list_suppliers = localStorage.getItem('list_suppliers')) {
   
               $('#list_suppliers').val(list_suppliers);
           }
           //            $(document).on('change', '#user_id', function (e) {
           //                localStorage.setItem('user_id', $(this).val());
           //            });
           //            if (user_id = localStorage.getItem('user_id')) {
           //                $('#user_id').val(user_id);
           //            }
           $(document).on('change', '#pur_ord_num', function (e) {
               var pr = $('#pur_ord_num').val();
               localStorage.setItem('pur_ord_num', pr);
           });
           if (pur_ord_num = localStorage.getItem('pur_ord_num')) {
               $('#pur_ord_num').val(pur_ord_num);
           }
           $(document).on('change', '#esd', function (e) {
               localStorage.setItem('esd', $(this).val());
           });
           $(document).on('change', '#through_transport', function (e) {
               var test = $('#through_transport').val();
               localStorage.setItem('through_transport', test);
           });
           if (through_transport = localStorage.getItem('through_transport')) {
               $('#through_transport').val(through_transport);
           }
           if (esd = localStorage.getItem('esd')) {
               $('#esd').val(esd);
               setTimeout(function () {
                   localStorage.setItem('pur_ord_num', '');
                   localStorage.setItem('esd', '');
                   localStorage.setItem('through_transport', '');
                   localStorage.setItem('user_id', '');
               }, 400000);
           }
   <?php } ?>
       if (!localStorage.getItem('qutax2')) {
           localStorage.setItem('qutax2', <?= $Settings->default_tax_rate2; ?>);
       }
       ItemnTotals();
       $("#add_item").autocomplete({
           source: function (request, response) {
               if (!$('#list_suppliers').val()) {
                   $('#add_item').val('').removeClass('ui-autocomplete-loading');
                   bootbox.alert('<?= lang('select_above'); ?>');
                   $('#add_item').focus();
                   return false;
               }
               $.ajax({
                   type: 'get',
                   url: '<?= site_url('quotes/suggestions'); ?>',
                   dataType: "json",
                   data: {
                       term: request.term,
                       warehouse_id: $("#quwarehouse").val(),
                       customer_id: $("#list_suppliers").val(),
                       store: $("#store").val()
                   },
                   success: function (data) {
                       $('#add_item').val('').removeClass('ui-autocomplete-loading');
                       response(data);
                   }
               });
           },
           minLength: 1,
           autoFocus: false,
           delay: 200,
           response: function (event, ui) {
               if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                   //audio_error.play();
                   bootbox.alert('<?= lang('no_match_found') ?>', function () {
                       $('#add_item').focus();
                   });
                   $(this).removeClass('ui-autocomplete-loading');
                   $(this).val('');
               } else if (ui.content.length == 1 && ui.content[0].id != 0) {
                   ui.item = ui.content[0];
                   $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                   $(this).autocomplete('close');
                   $(this).removeClass('ui-autocomplete-loading');
               } else if (ui.content.length == 1 && ui.content[0].id == 0) {
   //                    audio_error.play();
   
                   bootbox.alert('<?= lang('no_match_found') ?>', function () {
                       $('#add_item').focus();
                   });
                   $(this).removeClass('ui-autocomplete-loading');
                   $(this).val('');
               }
           },
           select: function (event, ui) {
   
               event.preventDefault();
               if (ui.item.id !== 0) {
   
                   var row = add_invoice_item(ui.item);
                   if (row)
                       $(this).val('');
               } else {
                   //audio_error.play();
                   bootbox.alert('<?= lang('no_match_found') ?>');
               }
           }
       });
       $('#add_item').bind('keypress', function (e) {
   //        audio_error.play();
           if (e.keyCode == 13) {
               e.preventDefault();
               $(this).autocomplete("search");
           }
       });
   });
</script>
<div class="box">
<div class="box-header">
   <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('add_new_quote'); ?></h2>
</div>
<div class="box-content">
   <div class="row">
      <div class="col-lg-12">
         <p class="introtext"><?php echo lang('enter_info'); ?></p>
         <?php
            $attrib = array('data-toggle' => 'validator', 'role' => 'form');
            echo form_open_multipart("quotes/add", $attrib)
            ?>
         <div class="row">
            <div class="col-lg-12">
               <?php if ($Owner || $Admin) { ?>
               <div class="col-md-4">
                  <div class="form-group">
                     <?= lang("biller", "qubiller"); ?>
                     <?php
                        $bl[""] = "";
                        foreach ($billers as $biller) {
                            // $bl[$biller->id] = $biller->name != '-' ? $biller->name : $biller->name;
                            $bl[$biller->id] = $biller->company;
                        }
                        echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : ''), 'id="store" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("biller") . '" required="required" class="form-control" style="width:100%;"');
                        ?>
                  </div>
               </div>
               <?php
                  } else {
                      $biller_input = array(
                          'type' => 'hidden',
                          'name' => 'biller',
                          'id' => 'store',
                          'value' => $this->session->userdata('biller_id'),
                      );
                  
                      echo form_input($biller_input);
                  }
                  ?>
               <?php if ($Owner || $Admin) { ?>
               <div class="col-md-4">
                  <div class="form-group">
                     <?= lang("order_date", "qudated"); ?>
                     <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="qudate" required="required"'); ?>
                  </div>
               </div>
               <?php } ?>
               <div class="col-md-4">
                  <div class="form-group">
                     <?= lang("supplier", "list_suppliers"); ?>
                     <?php
                        $wh[''] = '';
                        foreach ($suppliers as $sup) {
                            $wh[$sup->id] = $sup->code;
                        }
                        echo form_dropdown('supplier', "", (isset($_POST['supplier']) ? $_POST['supplier'] : ''), 'id="list_suppliers" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("supplier") . '" required="required" style="width:100%;" ');
                        ?>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <?= lang("pur_ord_num", "pur_ord_num"); ?>
                     <?php echo form_input('reference_no', (isset($_POST['reference_no']) ? $_POST['reference_no'] : $orderno), 'class="form-control input-tip" placeholder="' . $this->lang->line("pur_ord_num") . '" id="pur_ord_num"  required="required" readonly'); ?>
                  </div>
               </div>
               <?php if ($Settings->tax2) { ?>
               <!--                            <div class="col-md-4">
                  <div class="form-group">
                  <?= lang("order_tax", "qutax2"); ?>
                  <?php
                     $tr[""] = "";
                     foreach ($tax_rates as $tax) {
                         $tr[$tax->id] = $tax->name;
                     }
                     echo form_dropdown('order_tax', $tr, (isset($_POST['tax2']) ? $_POST['tax2'] : $Settings->default_tax_rate2), 'id="qutax2" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("order_tax") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                     ?>
                  </div>
                  </div>-->
               <?php } ?>
               <!--                        <div class="col-md-4">
                  <div class="form-group">
                  <?= lang("discount", "qudiscount"); ?>
                  <?php echo form_input('discount', '', 'class="form-control input-tip" id="qudiscount"'); ?>
                  </div>
                  </div>
                  
                  <div class="col-md-4">
                  <div class="form-group">
                  <?= lang("shipping", "qushipping"); ?>
                  <?php echo form_input('shipping', '', 'class="form-control input-tip" id="qushipping"'); ?>
                  
                  </div>
                  </div>-->
               <!--<div class="col-md-4">
                  <div class="form-group">
                      <?= lang("status", "qustatus"); ?>
                      <?php
                     $st = array('pending' => lang('pending'));
                     echo form_dropdown('status', $st, '', 'class="form-control input-tip" id="qustatus"');
                     ?>
                  
                  </div>
                  </div>-->
               <div class="col-md-4">
                  <div class="form-group">
                     <?= lang("estimate_dilievery_date", "esd"); ?>
                     <?php echo form_input('estimate_delievery', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip date"  placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("estimate_dilievery_date") . '" id="esd" required="required"'); ?>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <?= lang("through_transport", "through_transport"); ?>
                     <?php
                        $trans[""] = "";
                        foreach ($transport as $tr) {
                            $trans[$tr->id] = $tr->code . ' - ' . $tr->transport_name;
                        }
                        echo form_dropdown('transport', $trans, (isset($_POST['transport_name']) ? $_POST['transport_name'] : ''), 'id="through_transport"  data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("through_transport") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                        ?>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <?= lang("order_by", "order_by"); ?>
                     <?php
                        $us[""] = "Select Order By";
                        foreach ($user as $usr) {
                            $us[$usr->id] = $usr->first_name != '-' ? $usr->first_name . ' ' . $usr->last_name : $usr->first_name . ' ' . $usr->last_name;
                        }
                        
                        echo form_dropdown('user_id', $us, (isset($_POST['first_name']) ? $_POST['first_name'] : ''), 'id="user_id" placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("order_by") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                        ?>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <?= lang("document", "document") ?>
                     <input id="document" type="file" name="document" data-show-upload="false"
                        data-show-preview="false" class="form-control file">
                  </div>
               </div>
               <?php
                  $warehouse_input = array(
                      'type' => 'hidden',
                      'name' => 'warehouse',
                      'id' => 'quwarehouse',
                      'value' => 1,
                  );
                  echo form_input($warehouse_input);
                  ?>
               <!--<div class="col-md-12">
                  <div class="panel panel-warning">
                      <div
                          class="panel-heading"><?= lang('please_select_these_before_adding_product') ?></div>
                      <div class="panel-body" style="padding: 5px;">
                  
                          <?php if ($Owner || $Admin) { ?>
                              <div class="col-md-4">
                                  <div class="form-group">
                                      <?= lang("warehouse", "quwarehouse"); ?>
                                      <?php
                     $wh = array();
                     $wh[''] = '';
                     foreach ($warehouses as $warehouse) {
                         $wh[$warehouse->id] = $warehouse->name;
                     }
                     echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $Settings->default_warehouse), 'id="quwarehouse" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("warehouse") . '" required="required" style="width:100%;" ');
                     ?>
                                  </div>
                              </div>
                              <?php
                     } else {
                         $warehouse_input = array(
                             'type' => 'hidden',
                             'name' => 'warehouse',
                             'id' => 'quwarehouse',
                             'value' => 1,
                         );
                         echo form_input($warehouse_input);
                     }
                     ?>
                  
                      </div>
                  </div>
                  </div>-->
               <div class="col-md-12" id="sticker">
                  <div class="well well-sm">
                     <div class="form-group" style="margin-bottom:0;">
                        <div class="input-group wide-tip">
                           <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                              <i class="fa fa-2x fa-barcode addIcon"></i></a>
                           </div>
                           <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . $this->lang->line("add_product_to_order") . '"'); ?>
                           <?php if ($Owner || $Admin || $GP['products-add']) { ?>
                           <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;" ng-controller="addProducts">
                              <a  id="addManually" ng-click="resetProducts()" class="tip" title="<?= lang('add_product_manually') ?>">
                              <i class="fa fa-2x fa-plus-circle addIcon" id="addIcon"></i></a>
                           </div>
                           <?php } ?>
                        </div>
                     </div>
                     <div class="clearfix"></div>
                  </div>
               </div>
               <div class="clearfix"></div>
               <div class="col-md-12">
                  <div class="control-group table-group">
                     <label class="table-label"><?= lang("order_items"); ?> *</label>
                     <div class="controls table-controls">
                        <table id="quTable"
                           class="table items table-striped table-bordered table-condensed table-hover">
                           <thead>
                              <tr>
                                 <!--<th class="col-md-4"><?= lang("product_name") . " (" . $this->lang->line("product_code") . ")"; ?></th>-->
                                 <th class="col-md-4"><?= lang("product_name"); ?></th>
                                 <!--<th class="col-md-1"><?= lang("net_unit_price"); ?></th>-->
                                 <th class="col-md-1"><?= lang("purchase_price"); ?></th>
                                 <th class="col-md-1"><?= lang("primary_quantity"); ?></th>
                                 <th class="col-md-1"><?= lang("squantity"); ?></th>
                                 <!--<th class="col-md-1"><?= lang("brand"); ?></th>-->
                                 <th class="col-md-1"><?= lang("design"); ?></th>
                                 <th class="col-md-1"><?= lang("style"); ?></th>
                                 <th class="col-md-1"><?= lang("pattern"); ?></th>
                                 <th class="col-md-1"><?= lang("fitting"); ?></th>
                                 <th class="col-md-1"><?= lang("fabric"); ?></th>
                                 <th class="col-md-1"><?= lang("color"); ?></th>
                                 <th class="col-md-1"><?= lang("mrp"); ?></th>
                                 <th><?= lang("subtotal"); ?> (<span
                                    class="currency"><?= $default_currency->code ?></span>)
                                 </th>
                                 <th style="width: 30px !important; text-align: center;"><i
                                    class="fa fa-trash-o"
                                    style="opacity:0.5; filter:alpha(opacity=50);"></i></th>
                              </tr>
                           </thead>
                           <tbody></tbody>
                           <tfoot></tfoot>
                        </table>
                     </div>
                  </div>
               </div>
               <input type="hidden" name="total_items" value="" id="total_items" required="required"/>
               <div id="bottom-total" class="well well-sm" style="margin-bottom: 0;">
                  <table class="table table-bordered table-condensed totals" style="margin-bottom:0;">
                     <tr class="warning">
                        <td><?= lang('items') ?> <span class="totals_val pull-right" id="titems">0</span></td>
                        <td><?= lang('total') ?> <span class="totals_val pull-right" id="total">0.00</span></td>
                        <!--<td><?= lang('order_discount') ?> <span class="totals_val pull-right" id="tds">0.00</span></td>-->
                        <?php if ($Settings->tax2) { ?>
                        <!--<td><?= lang('order_tax') ?> <span class="totals_val pull-right" id="ttax2">0.00</span></td>-->
                        <?php } ?>
                        <!--<td><?= lang('shipping') ?> <span class="totals_val pull-right" id="tship">0.00</span></td>-->
                        <td><?= lang('grand_total') ?> <span class="totals_val pull-right" id="gtotal">0.00</span></td>
                     </tr>
                  </table>
               </div>
               <div class="row" id="bt">
                  <div class="col-sm-12">
                     <div class="col-sm-12">
                        <div class="form-group">
                           <?= lang("note", "qunote"); ?>
                           <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="qunote" style="margin-top: 10px; height: 100px;"'); ?>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-sm-12">
                  <div
                     class="fprom-group">
                     <?php echo form_submit('add_quote', $this->lang->line("submit"), 'id="add_quote" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                     <!--<button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></div>-->
                  </div>
               </div>
            </div>
            <?php echo form_close(); ?>
         </div>
      </div>
   </div>
</div>
<div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
               class="fa fa-2x">&times;</i></span><span class="sr-only"><?= lang('close'); ?></span></button>
            <h4 class="modal-title" id="prModalLabel"></h4>
         </div>
         <div class="modal-body" id="pr_popover_content">
            <form class="form-horizontal" role="form">
               <?php if ($Settings->tax1) { ?>
               <div class="form-group">
                  <label class="col-sm-4 control-label"><?= lang('product_tax') ?></label>
                  <div class="col-sm-8">
                     <?php
                        $tr[""] = "";
                        foreach ($tax_rates as $tax) {
                            $tr[$tax->id] = $tax->name;
                        }
                        echo form_dropdown('ptax', $tr, "", 'id="ptax" class="form-control pos-input-tip" style="width:100%;"');
                        ?>
                  </div>
               </div>
               <?php } ?>
               <div class="form-group">
                  <label for="pquantity" class="col-sm-4 control-label"><?= lang('quantity') ?></label>
                  <div class="col-sm-8">
                     <input type="text" class="form-control" id="pquantity">
                  </div>
               </div>
               <div class="form-group">
                  <label for="poption" class="col-sm-4 control-label"><?= lang('product_option') ?></label>
                  <div class="col-sm-8">
                     <div id="poptions-div"></div>
                  </div>
               </div>
               <?php if ($Settings->product_discount) { ?>
               <div class="form-group">
                  <label for="pdiscount"
                     class="col-sm-4 control-label"><?= lang('product_discount') ?></label>
                  <div class="col-sm-8">
                     <input type="text" class="form-control" id="pdiscount">
                  </div>
               </div>
               <?php } ?>
               <div class="form-group">
                  <label for="pprice" class="col-sm-4 control-label"><?= lang('unit_price') ?></label>
                  <div class="col-sm-8">
                     <input type="text" class="form-control" id="pprice">
                  </div>
               </div>
               <table class="table table-bordered table-striped">
                  <tr>
                     <th style="width:25%;"><?= lang('net_unit_price'); ?></th>
                     <th style="width:25%;"><span id="net_price"></span></th>
                     <th style="width:25%;"><?= lang('product_tax'); ?></th>
                     <th style="width:25%;"><span id="pro_tax"></span></th>
                  </tr>
               </table>
               <input type="hidden" id="punit_price" value=""/>
               <input type="hidden" id="old_tax" value=""/>
               <input type="hidden" id="old_qty" value=""/>
               <input type="hidden" id="old_price" value=""/>
               <input type="hidden" id="row_id" value=""/>
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="editItem"><?= lang('submit') ?></button>
         </div>
      </div>
   </div>
</div>
<?php
   if (!empty($variants)) {
       foreach ($variants as $variant) {
           $vars[] = addslashes($variant->name);
       }
   } else {
       $vars = array();
   }
   
   $department[''] = "";
   $product_items[''] = "";
   $section[''] = "";
   $type[''] = "";
   $brands[''] = "";
   $design[''] = "";
   $style[''] = "";
   $pattern[''] = "";
   $fitting[''] = "";
   $fabric[''] = "";
   $color[''] = "";
   $size[''] = "";
   $sizes = array();
   $per[''] = "";
   
   
   foreach ($product_para as $k => $v) {
       if ($k == "department" && !empty($v)) {
           foreach ($v as $d) {
               $department[$d->id] = $d->name;
           }
       }
       if ($k == "product_items" && !empty($v)) {
           foreach ($v as $d) {
               $product_items[$d->id] = $d->name;
           }
       }
       if ($k == "section" && !empty($v)) {
           foreach ($v as $d) {
               $section[$d->id] = $d->name;
           }
       }
       if ($k == "type" && !empty($v)) {
           foreach ($v as $d) {
               $type[$d->id] = $d->name;
           }
       }
       if ($k == "brands" && !empty($v)) {
           foreach ($v as $d) {
               $brands[$d->id] = $d->name;
           }
       }
       if ($k == "design" && !empty($v)) {
           foreach ($v as $d) {
               $design[$d->id] = $d->name;
           }
       }
       if ($k == "style" && !empty($v)) {
           foreach ($v as $d) {
               $style[$d->id] = $d->name;
           }
       }
       if ($k == "pattern" && !empty($v)) {
           foreach ($v as $d) {
               $pattern[$d->id] = $d->name;
           }
       }
       if ($k == "fitting" && !empty($v)) {
           foreach ($v as $d) {
               $fitting[$d->id] = $d->name;
           }
       }
       if ($k == "fabric" && !empty($v)) {
           foreach ($v as $d) {
               $fabric[$d->id] = $d->name;
           }
       }
       if ($k == "color" && !empty($v)) {
           foreach ($v as $d) {
               $color[$d->id] = $d->name;
           }
       }
       if ($k == "size" && !empty($v)) {
           foreach ($v as $d) {
               $size[$d->id] = $d->name;
               $sizes[$d->id] = $d;
   //            array_push($sizes, $d);
           }
       }
       if ($k == "per" && !empty($v)) {
           foreach ($v as $d) {
               $per[$d->id] = $d->name;
           }
       }
   }
   ?>
<style>
   @media(min-width: 992px) {.modal-lg {width: 80% !important;}}
   .err{
   border-color: red;
   }
</style>
<div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
               class="fa fa-2x">&times;</i></span><span class="sr-only"><?= lang('close'); ?></span></button>
            <h4 class="modal-title" id="prModalLabel"></h4>
         </div>
         <div class="modal-body" id="pr_popover_content">
            <form class="form-horizontal" role="form">
               <?php if ($Settings->tax1) { ?>
               <div class="form-group">
                  <label class="col-sm-4 control-label"><?= lang('product_tax') ?></label>
                  <div class="col-sm-8">
                     <?php
                        $tr[""] = "";
                        foreach ($tax_rates as $tax) {
                            $tr[$tax->id] = $tax->name;
                        }
                        echo form_dropdown('ptax', $tr, "", 'id="ptax" class="form-control pos-input-tip" style="width:100%;"');
                        ?>
                  </div>
               </div>
               <?php } ?>
               <input type="text" name="from_order" value="from_order" class="form-control hidden"/>
               <div class="form-group">
                  <label for="pquantity" class="col-sm-4 control-label"><?= lang('quantity') ?></label>
                  <div class="col-sm-8">
                     <input type="text" class="form-control" id="pquantity">
                  </div>
               </div>
               <?php if ($Settings->product_expiry) { ?>
               <div class="form-group">
                  <label for="pexpiry" class="col-sm-4 control-label"><?= lang('product_expiry') ?></label>
                  <div class="col-sm-8">
                     <input type="text" class="form-control date" id="pexpiry">
                  </div>
               </div>
               <?php } ?>
               <div class="form-group">
                  <label for="poption" class="col-sm-4 control-label"><?= lang('product_option') ?></label>
                  <div class="col-sm-8">
                     <div id="poptions-div"></div>
                  </div>
               </div>
               <?php if ($Settings->product_discount) { ?>
               <div class="form-group">
                  <label for="pdiscount"
                     class="col-sm-4 control-label"><?= lang('product_discount') ?></label>
                  <div class="col-sm-8">
                     <input type="text" class="form-control" id="pdiscount">
                  </div>
               </div>
               <?php } ?>
               <div class="form-group">
                  <label for="pcost" class="col-sm-4 control-label"><?= lang('unit_cost') ?></label>
                  <div class="col-sm-8">
                     <input type="text" class="form-control" id="pcost">
                  </div>
               </div>
               <table class="table table-bordered table-striped">
                  <tr>
                     <th style="width:25%;"><?= lang('net_unit_cost'); ?></th>
                     <th style="width:25%;"><span id="net_cost"></span></th>
                     <th style="width:25%;"><?= lang('product_tax'); ?></th>
                     <th style="width:25%;"><span id="pro_tax"></span></th>
                  </tr>
               </table>
               <input type="hidden" id="punit_cost" value=""/>
               <input type="hidden" id="old_tax" value=""/>
               <input type="hidden" id="old_qty" value=""/>
               <input type="hidden" id="old_cost" value=""/>
               <input type="hidden" id="row_id" value=""/>
            </form>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="editItem"><?= lang('submit') ?></button>
         </div>
      </div>
   </div>
</div>
<style>
   @media(min-width: 992px) {.modal-lg {width: 80% !important;}}
   .err{
   border-color: red;
   }
   #myModal{
   z-index: 1500;
   }
   #myModal2{
   z-index: 1500;
   }
</style>
<div class="modal" id="mModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
<script type="text/javascript" src="<?= $assets ?>bower_components/angular/angular.js"></script>
<script type="text/javascript" src="<?= $assets ?>bower_components/angular-ui-select2/src/select2.js"></script>
<!--<script src="<?= $assets ?>js/angular.control.js" type="text/javascript"></script>-->
<!--<script src="<?= $assets ?>js/services.js" type="text/javascript"></script>-->
<!--<script src="<?= $assets ?>js/ui-bootstrap.min.js" type="text/javascript"></script>-->
<div class="modal-dialog modal-lg" style="overflow-y: scroll; height: 100%">
   <div class="modal-content">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">
         <i class="fa fa-2x">&times;</i></span><span class="sr-only"><?= lang('close'); ?></span></button>
         <h4 class="modal-title" id="mModalLabel"><?= lang('add_product') ?></h4>
      </div>
      <div class="modal-body" id="pr_popover_content">
         <div id="exTab1" class="container-fluid" style="background-color: #ffff;padding: 10px" ng-controller="employee">
            <!-- <form method="post" id="addproduct" data-toggle="validator" role="form">
               <input type="text" name="from_order" value="from_order" class="form-control selec_clear hidden"/>
               <div class="col-md-5">
                   <div class="form-group hidden">
                       <?= lang("product_type", "type") ?>
                       <?php
                  $opts = array('standard' => lang('standard'));
                  
                  echo form_dropdown('type', $opts, (isset($_POST['type']) ? $_POST['type'] : ($product ? $product->type : '')), 'class="form-control selec_clear" id="type" required="required" ng-model="prod.protype" ng-change="getProdBarcode()"');
                  ?>
                   </div>
                   <input type="text" value="standard" name="type" class="hidden form-control selec_clear"/>
                   <div class="form-group all">
               
               
                       <?= lang("Store", "companies") ?>
                       <div class="input-group col-md-12">
                           <?php echo form_input('store_id', (isset($_POST['store_id']) ? $_POST['store_id'] : ($product ? $product->store_id : '')), 'class="form-control selec_clear get_barcode" default-attrib data-tab="companies" data-id="0" id="companies" placeholder="' . lang("select") . " " . lang("store") . '" required="required" style="width:100%" ng-model="prod.store_id" ng-change="getProdBarcode();getGstDetails();"'); ?>
                           <div class="input-group-addon no-print hidden">
                               <a href="<?php echo site_url('billers/add'); ?>" data-toggle="modal" data-target="#myModal2" class="external">
                                   <i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i>
                               </a>
                           </div>
                       </div>
                   </div>
                   <div class="form-group all">
                       <?= lang("Department", "department") ?>
                       <div class="input-group col-md-12">
                           <?php
                  echo form_input('department', (isset($_POST['department']) ? $_POST['department'] : ($product ? $product->department : '')), 'class="form-control selec_clear" default-attrib data-tab="department" data-key="store_id" data-id="companies" id="department" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ng-model="prod.dept" ng-change="getProdName();getProdBarcode();getProductMargin();getGstDetails();"');
                  ?> 
                           <div class="input-group-addon no-print hidden">
                               <a href="<?php echo site_url('system_settings/AddProduct_para/department'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal2" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                           </div>
                       </div>
                   </div>
                   <div class="form-group all">
                       <?= lang("Section", "section") ?>
                       <div class="input-group col-md-12">
                           <?php
                  echo form_input('section', (isset($_POST['section']) ? $_POST['section'] : ($product ? $product->section : '')), 'class="form-control selec_clear" default-attrib data-tab="section" id="section" data-key="department_id" data-id="department" placeholder="' . lang("select") . " " . lang("section") . '" ng-model="prod.section_id" ng-change="getProductMargin();getGstDetails();" required="required" style="width:100%"');
                  ?>
                           <div class="input-group-addon no-print hidden">
                               <a href="<?php echo site_url('system_settings/AddProduct_para/section'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal2" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                           </div>
                       </div>
                   </div>
                   <div class="form-group all">
                       <?= lang("Product_items", "product_items") ?>
                       <div class="input-group col-md-12">
                           <?php
                  echo form_input('product_items', (isset($_POST['product_items']) ? $_POST['product_items'] : ($product ? $product->product_items : '')), 'class="form-control selec_clear" default-attrib data-tab="product_items" data-key="section_id" data-id="section" id="product_items" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%" ng-model="prod.product_item" ng-change="getProdName();getProductMargin();getGstDetails();"');
                  ?>
                           <div class="input-group-addon no-print hidden">
                               <a href="<?php echo site_url('system_settings/AddProduct_para/product_items'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                           </div>
                       </div>
                   </div>
                   <div class="form-group all">
                       <?= lang("Type", "type") ?>
                       <div class="input-group col-md-12">
                           <?php
                  echo form_input('type_id', (isset($_POST['type_id']) ? $_POST['type_id'] : ($product ? $product->type_id : '')), 'class="form-control selec_clear" default-attrib data-tab="type" id="type_id" data-key="product_items_id" data-id="product_items" placeholder="' . lang("select") . " " . lang("type") . '" required="required" style="width:100%" ng-model="prod.type" ng-change="getProdName();getProductMargin();getGstDetails();"');
                  ?>
                           <div class="input-group-addon no-print hidden">
                               <a href="<?php echo site_url('system_settings/AddProduct_para/type'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                           </div>
                       </div>
                   </div>
                   <div class="form-group all">
                       <?= lang("Brands", "brands") ?>
                       <div class="input-group col-md-12">
                           <?php
                  echo form_input('brands', (isset($_POST['brands']) ? $_POST['brands'] : ($product ? $product->brands : '')), 'class="form-control selec_clear" default-attrib data-tab="brands" id="brands" data-key="type_id" data-id="type_id" placeholder="' . lang("select") . " " . lang("brands") . '" required="required" ng-model="prod.brands_id" ng-change="getProductMargin();getGstDetails();"  style="width:100%"');
                  ?>
                           <div class="input-group-addon no-print hidden">
                               <a href="<?php echo site_url('system_settings/AddProduct_para/brands'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                           </div>
                       </div>
                   </div>
                   <div class="form-group standard">
                       <?= lang("Design", "design") ?>
                       <div class="input-group col-md-12">
                           <?php
                  echo form_input('design', (isset($_POST['design']) ? $_POST['design'] : ($product ? $product->design : '')), 'class="form-control selec_clear" default-attrib data-tab="design" id="design" data-key="brands_id" data-id="brands" placeholder="' . lang("select") . " " . lang("design") . '" required="required" style="width:100%" ng-model="prod.design" ng-change="getGstDetails();"');
                  ?>
                           <div class="input-group-addon no-print hidden">
                               <a href="<?php echo site_url('system_settings/AddProduct_para/design'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                           </div>
                       </div>
                   </div>
                   <div class="form-group standard">
                       <?= lang("Style", "style_changed") ?>
                       <div class="input-group col-md-12">
                           <?php
                  echo form_input('style', (isset($_POST['style']) ? $_POST['style'] : ($product ? $product->style : '')), 'class="form-control selec_clear" default-attrib data-tab="style" id="style" data-key="design_id" data-id="design" placeholder="' . lang("select") . " " . lang("style") . '" required="required" style="width:100%" ng-model="prod.style" ng-change="getGstDetails();"');
                  ?>
                           <div class="input-group-addon no-print hidden">
                               <a href="<?php echo site_url('system_settings/AddProduct_para/style'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                           </div>
                       </div>
                   </div>
                   <div class="form-group standard">
                       <?= lang("Pattern", "pattern_changed") ?>
                       <div class="input-group col-md-12">
                           <?php
                  echo form_input('pattern', (isset($_POST['pattern']) ? $_POST['pattern'] : ($product ? $product->pattern : '')), 'class="form-control selec_clear" default-attrib data-tab="pattern" data-key="style_id" data-id="style" id="pattern" placeholder="' . lang("select") . " " . lang("pattern") . '" required="required" style="width:100%" ng-model="prod.pattern" ng-change="getGstDetails();"');
                  ?>
                           <div class="input-group-addon no-print hidden">
                               <a href="<?php echo site_url('system_settings/AddProduct_para/pattern'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                           </div>
                       </div>
                   </div>
                   <div class="form-group standard">
                       <?= lang("Fitting", "fitting_changed") ?>
                       <div class="input-group col-md-12">
                           <?php
                  echo form_input('fitting', (isset($_POST['fitting']) ? $_POST['fitting'] : ($product ? $product->fitting : '')), 'class="form-control selec_clear" default-attrib data-tab="fitting" data-key="pattern_id" data-id="pattern" id="fitting" placeholder="' . lang("select") . " " . lang("fitting") . '" required="required" style="width:100%"  ng-model="prod.fitting"  ng-change="getGstDetails();"');
                  ?>
                           <div class="input-group-addon no-print hidden">
                               <a href="<?php echo site_url('system_settings/AddProduct_para/fitting'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                           </div>
                       </div>
                   </div>
                   <div class="form-group standard">
                       <?= lang("Fabric", "fabric_changed") ?>
                       <div class="input-group col-md-12">
                           <?php
                  echo form_input('fabric', (isset($_POST['fabric']) ? $_POST['fabric'] : ($product ? $product->fabric : '')), 'class="form-control selec_clear" default-attrib data-tab="fabric" id="fabric" data-key="fitting_id" data-id="fitting" placeholder="' . lang("select") . " " . lang("fabric") . '" required="required" style="width:100%" ng-model="prod.fabric" ng-change="getGstDetails();" ');
                  ?>
                           <div class="input-group-addon no-print hidden">
                               <a href="<?php echo site_url('system_settings/AddProduct_para/fabric'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                           </div>
                       </div>
                   </div>
               </div>
               <div class="col-md-6 col-md-offset-1">
                   <div class="form-group all">
                       <?= lang("product_name", "name") ?>
                       <?= form_input('name', (isset($_POST['name']) ? $_POST['name'] : ($product ? $product->name : '')), 'class="form-control selec_clear" id="name" required="required" readonly ng-model="prod.name"'); ?>
                   </div>
                   <div class="form-group all">
                       <?= lang("product_code", "code") ?>
                       <?= form_input('code', (isset($_POST['code']) ? $_POST['code'] : ($product ? $product->code : '')), 'class="form-control selec_clear" id="code"  required="required" readonly ng-model="prod.barcode" ') ?>
                   </div>
                   <?= form_hidden('barcode_symbology', (isset($_POST['barcode_symbology']) ? $_POST['barcode_symbology'] : ($product ? $product->barcode_symbology : 'code128')), 'class="form-control selec_clear" id="barcode_symbology"  required="required" readonly ') ?>
                   <?php if ($Settings->tax1) { ?>
                       <div class="form-group standard">
                           <?= lang("product_tax", "tax_rate") ?>
                           <?php
                  $tr[""] = "";
                  foreach ($tax_rates as $tax) {
                      $tr[$tax->id] = $tax->name;
                  }
                  echo form_dropdown('tax_rate', $tr, (isset($_POST['tax_rate']) ? $_POST['tax_rate'] : ($product ? $product->tax_rate : $Settings->default_tax_rate)), 'class="form-control selec_clear select" id="tax_rate" placeholder="' . lang("select") . ' ' . lang("product_tax") . '" style="width:100%"');
                  ?>
                       </div>
                       <div class="form-group standard">
                           <?= lang("tax_method", "tax_method") ?>
                           <?php
                  $tm = array('0' => lang('inclusive'), '1' => lang('exclusive'));
                  echo form_dropdown('tax_method', $tm, (isset($_POST['tax_method']) ? $_POST['tax_method'] : ($product ? $product->tax_method : '')), 'class="form-control selec_clear select" id="tax_method" placeholder="' . lang("select") . ' ' . lang("tax_method") . '" style="width:100%"');
                  ?>
                       </div>
                   <?php } ?>
                   <div class="form-group standard">
                       <?= lang("supplier", "supplier") ?>
                       <div class="row" id="supplier-con">
                           <div class="col-md-12 col-sm-12 col-xs-12">
                               <?php echo form_input('supplier', (isset($_POST['supplier']) ? $_POST['supplier'] : ''), 'class="form-control selec_clear hidden" id="supplier12" placeholder="" style="width:100%;"'); ?>
                               <?php echo form_input('supplier_name', (isset($_POST['supplier']) ? $_POST['supplier'] : ''), 'class="form-control selec_clear" id="supplier_name" readonly placeholder="" style="width:100%;"'); ?>
                           </div>
                       </div>
                   </div>
               
               
               
                   <div class="form-group standard">
                       <?= lang("product_image", "product_image") ?> max (<?= byte_format($this->Settings->iwidth * $this->Settings->iheight) ?>)
                       <input id="product_image" type="file" name="product_image" data-show-upload="false"
                              data-show-preview="false" accept="image/*" class="form-control selec_clear file">
                   </div>
               
                   <div id="img-details"></div>
                   <div class="form-group standard">
                       <?= lang("Color", "color") ?> *
                       <div class="row">
                           <div class="col-md-3"> &nbsp;&nbsp;<input type="radio" name="colortype" ng-model="prod.colortype" icheck value="Single"/>&nbsp;&nbsp;&nbsp;&nbsp;Single</div>
                           <div class="col-md-3 colorsingle hide">
                               <?php echo form_dropdown('colorsingle', $color, (isset($_POST['colorsingle']) ? $_POST['colorsingle'] : ($product ? $product->color : '')), 'class="form-control" select2 id="color" placeholder="' . lang("select") . " " . lang("color") . '" style="width:100%"'); ?>
               
                           </div>
               
                           <div class="col-md-2 colorsingle hide">
                               <input type="text" id="colorqty" name="colorqty" onlyno class="form-control" ng-model="prod.colorqty" ng-change="getQty('#colorqty')"/>
                           </div>
                           <div class="col-md-4 colorsingle hide">
                               <input type="text" id="colorcode" name="colorcode[]" onlyno readonly class="form-control" ng-model="prod.colorcode" ng-change="getQty('#colorqty')"/>
                               <input type="text" id="codet" name="codet" onlyno class="hidden form-control"/>
                           </div>
                       </div>
                       <br>
                       <div class="row">
                           <div class="col-md-4"> &nbsp;&nbsp;<input type="radio" name="colortype" ng-model="prod.colortype" icheck value="Assorted"/>&nbsp;&nbsp;&nbsp;&nbsp;Assorted</div>
                           <div class="col-md-8 colorassorted hide">
                               <?php
                  echo form_dropdown('colorassorted[]', $color, (isset($_POST['colorassorted']) ? $_POST['colorassorted'] : ($product ? $product->colorassorted : '')), 'class="form-control" multiple select2 id="mulcolor" ng-model="prod.colorassorted" placeholder="' . lang("select") . " " . lang("color") . '"style="width:100%"');
                  ?>
                               <br/>
                               <div class="row" style="padding-top: 40px;">
                                   <div class="col-md-3 from-group" style="padding-right: 0px; margin-bottom: 5px" ng-repeat="n in prod.colorassoarr">
                                       <input type="text" name="colorqty[]" onlyno ng-model="n.qty" ng-blur="getQtyCal($index, n.qty)" class="form-control"/>
               
                                   </div>
                               </div>
                               <div class="row" style="padding-top: 2s0px;">
                                   <div class="col-md-3 from-group" style="padding-right: 0px; margin-bottom: 5px" ng-repeat="n in prod.colorassoarr">
                                       <input type="text" name="colorcode[]" onlyno ng-model="n.colorcode" readonly class="form-control"/>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="form-group ">
                       <?= lang("Size", "size") ?>
                       <div class="row">
                           <div class="col-md-3"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="sizeangle" ng-model="prod.sizeangle" ng-change="getProdName()" <?= ($product && $product->sizeangle == "HT" ? "checked" : '') ?> value="HT" id="sizeangle_ht"/>&nbsp;&nbsp;&nbsp;&nbsp;Height</div>
                           <div class="col-md-3"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="sizeangle" ng-model="prod.sizeangle" ng-change="getProdName()" <?= ($product && $product->sizeangle == "WT" ? "checked" : '') ?> value="WT" id="sizeangle_wt" />&nbsp;&nbsp;&nbsp;&nbsp;Width</div>
                           <div class="col-md-3"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="sizeangle" ng-model="prod.sizeangle" ng-change="getProdName()" <?= ($product && $product->sizeangle == "NZ" ? "checked" : '') ?> value="NZ" id="sizeangle_nz" />&nbsp;&nbsp;&nbsp;&nbsp;No Size</div>
                       </div>
                       <br/>
                       <div class="row sizeangle_nz">
                           <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" name="sizetype" ng-model="prod.sizetype" ng-change="getProdName()" checked="" value="Single"/>&nbsp;&nbsp;&nbsp;&nbsp;Single</div>
                           <div class="col-md-4" ng-init='size =<?= json_encode($sizes) ?>' ng-show="prod.sizetype == 'Single'">
                               <div class="col-md-9" style="padding: 0px"><?php
                  echo form_input('singlesize', (isset($_POST['singlesize']) ? $_POST['singlesize'] : ($product ? $product->size : '')), 'class="form-control selec_clear " sizesel data-tab="size" data-key="department_id-section_id-product_items_id" data-id="department,section,product_items" id="size" placeholder="' . lang("select") . " " . lang("size") . '" required="required" style="width:100%" ng-model="prod.singlesize" ng-change="getProdName();"');
                  ?>
                               </div>
                               <div class="col-md-2"> {{size[prod.singlesize].code?size[prod.singlesize].code+'\"':""}}</div>
                               <div class="clearfix"></div>
                           </div>
                       </div>
                       <br/>
                       <div class="row standard sizeangle_nz">
                           <div class="col-md-4"> &nbsp;&nbsp;<input icheck type="radio" name="sizetype" ng-model="prod.sizetype" ng-change="getProdName()" value="Multiple"/>&nbsp;&nbsp;&nbsp;&nbsp;Multiple</div>
                           <div class="col-md-4" ng-init='size1 =<?= json_encode($sizes) ?>' ng-show="prod.sizetype == 'Multiple'">
                               <div class="col-md-9" style="padding: 0px"><?php
                  echo form_input('multisizef', (isset($_POST['multisizef']) ? $_POST['multisizef'] : ''), 'class="form-control selec_clear " sizesel data-tab="size" data-key="department_id-section_id-product_items_id" data-id="department,section,product_items" id="multisizef" placeholder="' . lang("select") . " " . lang("size") . '" required="required" style="width:100%" ng-model="prod.multisizef" ng-change="getProdName();"');
                  ?></div>
                               <div class="col-md-2"> {{size1[prod.multisizef].code?size1[prod.multisizef].code+'\"':""}}</div>
                               <div class="clearfix"></div>
                           </div>
                           <div class="col-md-1" ng-show="prod.sizetype == 'Multiple'">To</div>
                           <div class="col-md-3" ng-init='size2 =<?= json_encode($sizes) ?>' ng-show="prod.sizetype == 'Multiple'">
                               <div class="col-md-9" style="padding: 0px"><?php
                  echo form_input('multisizet', (isset($_POST['multisizet']) ? $_POST['multisizet'] : ''), 'class="form-control selec_clear " sizesel data-tab="size" data-key="department_id-section_id-product_items_id" data-id="department,section,product_items" id="multisizet" placeholder="' . lang("select") . " " . lang("size") . '" required="required" style="width:100%" ng-model="prod.multisizet"');
                  ?></div>
                               <div class="col-md-2"> {{size2[prod.multisizet].code?size2[prod.multisizet].code+'\"':""}}</div>
                               <div class="clearfix"></div>
                           </div>
                       </div>
                   </div>
                   <div class="row standard">
                       <div class="col-md-8">
                           <div class="form-group">
                               <label class="control-label" for="unit"><?= lang("product_qty") ?></label>
                               <?php
                  foreach ($warehouses as $warehouse) {
                      //$whs[$warehouse->id] = $warehouse->name;
                      if ($this->Settings->racks) {
                          echo "<div class='row'><div class='col-md-12'>";
                          echo form_hidden('wh_' . $warehouse->id, $warehouse->id) . form_input('wh_qty_' . $warehouse->id, (isset($_POST['wh_qty_' . $warehouse->id]) ? $_POST['wh_qty_' . $warehouse->id] : ''), 'class="form-control selec_clear" id="wh_qtys" placeholder="' . lang('quantity') . '" ng-model="prod.qty" ng-change="getProdBarcode();" onlyno');
                          echo "</div></div>";
                      } else {
                          echo form_hidden('wh_' . $warehouse->id, $warehouse->id) . form_input('wh_qty_' . $warehouse->id, (isset($_POST['wh_qty_' . $warehouse->id]) ? $_POST['wh_qty_' . $warehouse->id] : ''), 'class="form-control selec_clear" id="wh_qtys" placeholder="' . lang('quantity') . '" ng-model="prod.qty" ng-change="getProdBarcode();" onlyno');
                      }
                  }
                  ?>
                           </div>
                       </div>
                       <div class="col-md-4">
                           <div class="form-group">
                               <?= lang("unit_per", "unit") ?>
                               <?= form_input('unit', (isset($_POST['unit']) ? $_POST['unit'] : ($product ? $product->uper : '')), 'class="form-control selec_clear" id="unit" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" required="required" style="width:100%" '); ?>
                           </div>
                       </div>
                   </div>
                   <div class="row standard">
                       <div class="col-md-8">
                           <div class="form-group all">
                               <?= form_input('squantity', (isset($_POST['price']) ? $_POST['price'] : ($product ? $this->sma->formatDecimal($product->price) : '')), 'class="form-control selec_clear tip" id="squantity"') ?>
                           </div>
                       </div>
                       <div class="col-md-4">
                           <div class="form-group">
                               <?= form_input('sunit', (isset($_POST['sunit']) ? $_POST['sunit'] : ($product ? $product->uper : '')), 'class="form-control selec_clear" id="sunit" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" style="width:100%" '); ?>
                           </div>
                       </div>
                   </div>
                   <script>
                       $('#squantity').change(function () {
                           var squantity = $('#squantity').val();
                           var wh_qtys = $('#wh_qtys').val();
                           if (wh_qtys % squantity != 0 && squantity != "") {
                               bootbox.alert('Please Enter Valid Secondary Quentity');
                               $('#squantity').val("");
                           }
                       });
                   </script>
                   <div class="row">
                       <div class="col-md-8">
                           <div class="form-group all">
                               <?= lang("product_cost", "cost") ?>
                               <?= form_input('cost', (isset($_POST['cost']) ? $_POST['cost'] : ($product ? $this->sma->formatDecimal($product->cost) : '')), 'class="form-control selec_clear tip" id="cost" required="required" ng-model="prod.cost" ng-blur="getProductMargin()" ng-change="getProdBarcode();"') ?>
                           </div>
                       </div>
                       <div class="col-md-4">
                           <div class="form-group all">
                               <?= lang("product_per", "cper") ?>
                               <?= form_input('cper', (isset($_POST['cper']) ? $_POST['cper'] : ($product ? $product->cper : '')), 'class="form-control selec_clear" id="cper" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" required="required" style="width:100%" '); ?>
                           </div>
                       </div>
                   </div>  
                   <div class="row">
                       <div class="col-md-8">
                           <div class="form-group all">
                               <?= lang("product_price", "price") ?>
                               <?= form_input('price', (isset($_POST['price']) ? $_POST['price'] : ($product ? $this->sma->formatDecimal($product->price) : '')), 'class="form-control selec_clear tip" id="price" ng-model="prod.price" ng-change="getProdBarcode();"') ?>
                           </div>
                       </div>
                       <div class="col-md-4">
                           <div class="form-group all">
                               <?= lang("product_per", "pper") ?>
                               <div class="input-group col-md-12">
                                   <?= form_input('pper', (isset($_POST['pper']) ? $_POST['pper'] : ($product ? $product->pper : '')), 'class="form-control selec_clear" id="pper" placeholder="' . lang("select") . " " . lang("Unit") . '" default-attrib data-tab="per" style="width:100%"'); ?>
                                   <div class="input-group-addon no-print hidden">
                                       <a href="<?php echo site_url('system_settings/AddProduct_para/per'); ?>" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class=" standard ">
                       <div class="row">
                           <div class="col-md-12">
                               <div class="row">
                                   <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" <?= (isset($_POST['ratetype']) && $_POST['ratetype'] == "MRP" ? 'checked' : 'checked') ?> name="ratetype"  value="MRP"/>&nbsp;&nbsp;&nbsp;&nbsp;MRP</div>
                                   <div class="col-md-3 form-group">
                                       <?php echo form_input('mrprate', (isset($_POST['mrprate']) ? $_POST['mrprate'] : ''), 'class="form-control selec_clear" id="mrprate" placeholder="" style="width:100%;"'); ?>
                                   </div>
               
                                   
                               </div>
                               <div class="row">
                                   <div class="form-group">
                                       <?= lang("Product_Classification", "product_classification") ?>
                                       <div class="row">
                                           <div class="col-md-4"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="cf1" <?= ($_POST['cf1'] == "regular" ? "checked='checked'" : '') ?> value="regular" id="prod_classification_regular"/>&nbsp;&nbsp;&nbsp;&nbsp;Regular</div>
                                           <div class="col-md-4"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="cf1" <?= ($_POST['cf1'] == "economical" ? "checked='checked'" : '') ?> value="economical" id="prod_classification_economical" />&nbsp;&nbsp;&nbsp;&nbsp;Economical</div>
                                           <div class="col-md-4"> &nbsp;&nbsp;<input class="sizeangle" icheck type="radio" name="cf1" <?= ($_POST['cf1'] == "premium" ? "checked='checked'" : '') ?> value="premium" id="prod_classification_premium" />&nbsp;&nbsp;&nbsp;&nbsp;Premium</div>
                                       </div>
                                   </div>
                               </div>
               
               
                           </div>
                       </div>
                       <div class="hidden">
                           <div class="row ">
                               <div class="col-md-6 form-group"> 
                                   <?= lang('hsncode', 'hsncode') ?>
                                   <div class="input-group">
                                       <?= form_input('hsnno', (isset($_POST['hsnno']) ? $_POST['hsnno'] : $product->price), 'class="form-control tip" id="hsnno" ng-model="prod.hsnno" readonly') ?>
                                       <div class="input-group-addon no-print">
                                           <a href="<?php echo site_url('gst/add'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-3 form-group">
                                   <?= lang('gstno', 'gstno') ?>
                                   <?= form_input('gstno', (isset($_POST['gstno']) ? $_POST['gstno'] : $product->price), 'class="form-control tip" id="gstno" readonly ng-model="prod.gstno"  ng-blur="getProductMargin()"') ?>
                               </div>
                               <div class="col-md-3">
                                   <?= lang('cess', 'cess') ?>
                                   <?= form_input('cess', (isset($_POST['cess']) ? $_POST['cess'] : $product->price), 'class="form-control tip" id="cess" readonly ng-model="prod.cess"  ng-blur="getProductMargin()"') ?>
                               </div>
                           </div>
               
                           <div class="row form-group">
                               <div class="col-md-8"> &nbsp;&nbsp;<input type="checkbox" icheck class="checkbox" id="addupgetcheck" ng-model="prod.addupgst" name="addupgst">&nbsp;&nbsp;&nbsp;&nbsp;Add-up GST if MRP exceeds 1000+ (%)</div>
                               <div class="col-md-4">
                                   <?= form_input('addupgstmrp', (isset($_POST['addupgstmrp']) ? $_POST['addupgstmrp'] : $product->price), 'class="form-control tip" id="addupgstmrp" ng-model="prod.addupgstmrp" readonly ng-blur="getProductMargin()"') ?>
                               </div>
                           </div>
               
                       </div>
               
               
                       <br/>
                       <div class="row hidden">
                           <div class="col-md-3"> &nbsp;&nbsp;<input icheck type="radio" <?= (isset($_POST['ratetype']) && $_POST['ratetype'] == "Multiple" ? 'checked' : '') ?> name="ratetype" value="Multiple"/>&nbsp;&nbsp;&nbsp;&nbsp;Multiple</div>
                           <div class="col-md-3">
                               <div class="col-md-12" style="padding: 0px"><?php echo form_input('mulratef', (isset($_POST['mulratef']) ? $_POST['mulratef'] : ''), 'class="form-control selec_clear" id="mulratef" placeholder="" style="width:100%;"'); ?></div>
                               <div class="clearfix"></div>
                           </div>
                           <div class="col-md-3">To</div>
                           <div class="col-md-3" ng-init='size2 =<?= json_encode($sizes) ?>'>
                               <div class="col-md-12" style="padding: 0px"><?php echo form_input('mulratet', (isset($_POST['mulratet']) ? $_POST['mulratet'] : ''), 'class="form-control selec_clear" id="mulratet" placeholder="" style="width:100%;"'); ?></div>
                               <div class="clearfix"></div>
                           </div>
                       </div>
                   </div>
               
                   <div class="combo" style="display:none;">
                       <div class="form-group">
                           <?= lang("add_product", "add_item") . ' (' . lang('not_with_variants') . ')'; ?>
                           <?php echo form_input('add_item', '', 'class="form-control selec_clear ttip" id="add_item" data-placement="top" data-trigger="focus" data-bv-notEmpty-message="' . lang('please_add_items_below') . '" placeholder="' . $this->lang->line("add_item") . '"'); ?>
                       </div>
                       <div class="control-group table-group">
                           <label class="table-label" for="combo">{{prod.protype}} <?= lang("products"); ?></label>
                           <div class="controls table-controls">
                               <table id="prTable" class="table items table-striped table-bordered table-condensed table-hover">
                                   <thead>
                                       <tr>
                                           <th class="col-md-5 col-sm-5 col-xs-5"><?= lang("product_name") . " (" . $this->lang->line("product_code") . ")"; ?></th>
                                           <th class="col-md-2 col-sm-2 col-xs-2"><?= lang("quantity"); ?></th>
                                           <th class="col-md-3 col-sm-3 col-xs-3"><?= lang("unit_price"); ?></th>
                                           <th class="col-md-1 col-sm-1 col-xs-1 text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                           </th>
                                       </tr>
                                   </thead>
                                   <tbody></tbody>
                               </table>
                           </div>
                       </div>
                   </div>
               
                   <div class="digital" style="display:none;">
                       <div class="form-group digital">
                           <?= lang("digital_file", "digital_file") ?>
                           <input id="digital_file" type="file" name="digital_file" data-show-upload="false"
                                  data-show-preview="false" class="form-control selec_clear file">
                       </div>
                   </div>
               </div>
               <div class="col-md-12">
                   <div class="form-group">
                       <?php // echo form_submit('add_product', "Save", 'class="btn btn-primary" ');    ?>
                   </div>
               </div>
               </form> -->
            <div class="stepwizard">
               <div class="stepwizard-row setup-panel">
                  <div class="stepwizard-step">
                     <a href="#step-1" type="button"  class="btn btn-primary ">Product Info</a>
                  </div>
                  <div class="stepwizard-step">
                     <a href="#step-2" type="button" class="btn btn-default ">Product Variations</a>
                  </div>
                  <!-- <div class="stepwizard-step">
                     <a href="#step-3" type="button" class="btn btn-default ">Product Variants</a>
                  </div> -->
               </div>
            </div>
            <form method="post" id="addproduct" data-toggle="validator" role="form">
                <div class="tab-pane" style="border: 1px solid #000; padding: 10px">
               <div class="row setup-content" id="step-1">
                  <div class="col-md-12">
                     <div class="row">
                        <input type="text" name="from_order" value="from_order" class="form-control selec_clear hidden"/>
                        <div class="col-md-5 mx-auto">
                           <div class="form-group hidden">
                              <?= lang("product_type", "type") ?>
                              <?php
                                 $opts = array('standard' => lang('standard'));
                                 
                                 echo form_dropdown('type', $opts, (isset($_POST['type']) ? $_POST['type'] : ($product ? $product->type : '')), 'class="form-control selec_clear" id="type" required="required" ng-model="prod.protype" ng-change="getProdBarcode()"');
                                 ?>
                           </div>
                           <input type="text" value="standard" name="type" class="hidden form-control selec_clear"/>
                           <div class="form-group all">
                              <?= lang("Store", "companies") ?>
                              <div class="input-group col-md-12">
                                 <?php echo form_input('store_id', (isset($_POST['store_id']) ? $_POST['store_id'] : ($product ? $product->store_id : '')), 'class="form-control selec_clear get_barcode" default-attrib data-tab="companies" data-id="0" id="companies" placeholder="' . lang("select") . " " . lang("store") . '" required="required" style="width:100%" ng-model="prod.store_id" ng-change="getProdBarcode();getGstDetails();"'); ?>
                                 <div class="input-group-addon no-print hidden">
                                    <a href="<?php echo site_url('billers/add'); ?>" data-toggle="modal" data-target="#myModal2" class="external">
                                    <i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i>
                                    </a>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group all">
                              <?= lang("Department", "department") ?>
                              <div class="input-group col-md-12">
                                 <?php
                                    echo form_input('department', (isset($_POST['department']) ? $_POST['department'] : ($product ? $product->department : '')), 'class="form-control selec_clear" default-attrib data-tab="department" data-key="store_id" data-id="companies" id="department" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ng-model="prod.dept" ng-change="getProdName();getProdBarcode();getProductMargin();getGstDetails();"');
                                    ?> 
                                 <div class="input-group-addon no-print hidden">
                                    <a href="<?php echo site_url('system_settings/AddProduct_para/department'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal2" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group all">
                              <?= lang("Section", "section") ?>
                              <div class="input-group col-md-12">
                                 <?php
                                    echo form_input('section', (isset($_POST['section']) ? $_POST['section'] : ($product ? $product->section : '')), 'class="form-control selec_clear" default-attrib data-tab="section" id="section" data-key="department_id" data-id="department" placeholder="' . lang("select") . " " . lang("section") . '" ng-model="prod.section_id" ng-change="getProductMargin();getGstDetails();" required="required" style="width:100%"');
                                    ?>
                                 <div class="input-group-addon no-print hidden">
                                    <a href="<?php echo site_url('system_settings/AddProduct_para/section'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal2" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group all">
                              <?= lang("Product_items", "product_items") ?>
                              <div class="input-group col-md-12">
                                 <?php
                                    echo form_input('product_items', (isset($_POST['product_items']) ? $_POST['product_items'] : ($product ? $product->product_items : '')), 'class="form-control selec_clear" default-attrib data-tab="product_items" data-key="section_id" data-id="section" id="product_items" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%" ng-model="prod.product_item" ng-change="getProdName();getProductMargin();getGstDetails();"');
                                    ?>
                                 <div class="input-group-addon no-print hidden">
                                    <a href="<?php echo site_url('system_settings/AddProduct_para/product_items'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group all">
                              <?= lang("Type", "type") ?>
                              <div class="input-group col-md-12">
                                 <?php
                                    echo form_input('type_id', (isset($_POST['type_id']) ? $_POST['type_id'] : ($product ? $product->type_id : '')), 'class="form-control selec_clear" default-attrib data-tab="type" id="type_id" data-key="product_items_id" data-id="product_items" placeholder="' . lang("select") . " " . lang("type") . '" required="required" style="width:100%" ng-model="prod.type" ng-change="getProdName();getProductMargin();getGstDetails();"');
                                    ?>
                                 <div class="input-group-addon no-print hidden">
                                    <a href="<?php echo site_url('system_settings/AddProduct_para/type'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group all">
                              <?= lang("Brands", "brands") ?>
                              <div class="input-group col-md-12">
                                 <?php
                                    echo form_input('brands', (isset($_POST['brands']) ? $_POST['brands'] : ($product ? $product->brands : '')), 'class="form-control selec_clear" default-attrib data-tab="brands" id="brands" data-key="type_id" data-id="type_id" placeholder="' . lang("select") . " " . lang("brands") . '" required="required" ng-model="prod.brands_id" ng-change="getProductMargin();getGstDetails();"  style="width:100%"');
                                    ?>
                                 <div class="input-group-addon no-print hidden">
                                    <a href="<?php echo site_url('system_settings/AddProduct_para/brands'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group standard">
                              <?= lang("Design", "design") ?>
                              <div class="input-group col-md-12">
                                 <?php
                                    echo form_input('design', (isset($_POST['design']) ? $_POST['design'] : ($product ? $product->design : '')), 'class="form-control selec_clear" default-attrib data-tab="design" id="design" data-key="brands_id" data-id="brands" placeholder="' . lang("select") . " " . lang("design") . '" required="required" style="width:100%" ng-model="prod.design" ng-change="getGstDetails();"');
                                    ?>
                                 <div class="input-group-addon no-print hidden">
                                    <a href="<?php echo site_url('system_settings/AddProduct_para/design'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group standard">
                              <?= lang("Style", "style_changed") ?>
                              <div class="input-group col-md-12">
                                 <?php
                                    echo form_input('style', (isset($_POST['style']) ? $_POST['style'] : ($product ? $product->style : '')), 'class="form-control selec_clear" default-attrib data-tab="style" id="style" data-key="design_id" data-id="design" placeholder="' . lang("select") . " " . lang("style") . '" required="required" style="width:100%" ng-model="prod.style" ng-change="getGstDetails();"');
                                    ?>
                                 <div class="input-group-addon no-print hidden">
                                    <a href="<?php echo site_url('system_settings/AddProduct_para/style'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group standard">
                              <?= lang("Pattern", "pattern_changed") ?>
                              <div class="input-group col-md-12">
                                 <?php
                                    echo form_input('pattern', (isset($_POST['pattern']) ? $_POST['pattern'] : ($product ? $product->pattern : '')), 'class="form-control selec_clear" default-attrib data-tab="pattern" data-key="style_id" data-id="style" id="pattern" placeholder="' . lang("select") . " " . lang("pattern") . '" required="required" style="width:100%" ng-model="prod.pattern" ng-change="getGstDetails();"');
                                    ?>
                                 <div class="input-group-addon no-print hidden">
                                    <a href="<?php echo site_url('system_settings/AddProduct_para/pattern'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group standard">
                              <?= lang("Fitting", "fitting_changed") ?>
                              <div class="input-group col-md-12">
                                 <?php
                                    echo form_input('fitting', (isset($_POST['fitting']) ? $_POST['fitting'] : ($product ? $product->fitting : '')), 'class="form-control selec_clear" default-attrib data-tab="fitting" data-key="pattern_id" data-id="pattern" id="fitting" placeholder="' . lang("select") . " " . lang("fitting") . '" required="required" style="width:100%"  ng-model="prod.fitting"  ng-change="getGstDetails();"');
                                    ?>
                                 <div class="input-group-addon no-print hidden">
                                    <a href="<?php echo site_url('system_settings/AddProduct_para/fitting'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group standard">
                              <?= lang("Fabric", "fabric_changed") ?>
                              <div class="input-group col-md-12">
                                 <?php
                                    echo form_input('fabric', (isset($_POST['fabric']) ? $_POST['fabric'] : ($product ? $product->fabric : '')), 'class="form-control selec_clear" default-attrib data-tab="fabric" id="fabric" data-key="fitting_id" data-id="fitting" placeholder="' . lang("select") . " " . lang("fabric") . '" required="required" style="width:100%" ng-model="prod.fabric" ng-change="getGstDetails();" ');
                                    ?>
                                 <div class="input-group-addon no-print hidden">
                                    <a href="<?php echo site_url('system_settings/AddProduct_para/fabric'); ?>?{{paraurl}}&popup=1" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-md-6 col-md-offset-1">
                           <div class="form-group all">
                              <?= lang("product_name", "name") ?>
                              <?= form_input('name', (isset($_POST['name']) ? $_POST['name'] : ($product ? $product->name : '')), 'class="form-control selec_clear" id="name" required="required" readonly ng-model="prod.name"'); ?>
                           </div>
                           <div class="form-group all">
                              <?= lang("product_code", "code") ?>
                              <?= form_input('code', (isset($_POST['code']) ? $_POST['code'] : ($product ? $product->code : '')), 'class="form-control selec_clear" id="code"  required="required" readonly ng-model="prod.barcode" ') ?>
                           </div>
                           <?= form_hidden('barcode_symbology', (isset($_POST['barcode_symbology']) ? $_POST['barcode_symbology'] : ($product ? $product->barcode_symbology : 'code128')), 'class="form-control selec_clear" id="barcode_symbology"  required="required" readonly ') ?>
                           <?php if ($Settings->tax1) { ?>
                           <div class="form-group standard">
                              <?= lang("product_tax", "tax_rate") ?>
                              <?php
                                 $tr[""] = "";
                                 foreach ($tax_rates as $tax) {
                                     $tr[$tax->id] = $tax->name;
                                 }
                                 echo form_dropdown('tax_rate', $tr, (isset($_POST['tax_rate']) ? $_POST['tax_rate'] : ($product ? $product->tax_rate : $Settings->default_tax_rate)), 'class="form-control selec_clear select" id="tax_rate" placeholder="' . lang("select") . ' ' . lang("product_tax") . '" style="width:100%"');
                                 ?>
                           </div>
                           <div class="form-group standard">
                              <?= lang("tax_method", "tax_method") ?>
                              <?php
                                 $tm = array('0' => lang('inclusive'), '1' => lang('exclusive'));
                                 echo form_dropdown('tax_method', $tm, (isset($_POST['tax_method']) ? $_POST['tax_method'] : ($product ? $product->tax_method : '')), 'class="form-control selec_clear select" id="tax_method" placeholder="' . lang("select") . ' ' . lang("tax_method") . '" style="width:100%"');
                                 ?>
                           </div>
                           <?php } ?>
                           <div class="form-group standard">
                              <?= lang("supplier", "supplier") ?>
                              <div class="row" id="supplier-con">
                                 <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php echo form_input('supplier', (isset($_POST['supplier']) ? $_POST['supplier'] : ''), 'class="form-control selec_clear hidden" id="supplier12" placeholder="" style="width:100%;"'); ?>
                                    <?php echo form_input('supplier_name', (isset($_POST['supplier']) ? $_POST['supplier'] : ''), 'class="form-control selec_clear" id="supplier_name" readonly placeholder="" style="width:100%;"'); ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                    </div>
                     <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                     <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                  </div>
                </div>
                  <!-- <div class="row setup-content" id="step-2">
                     <div class="col-md-12">
                        <div class="row m-3">
                        <div class="col-md-5">
                            <div class="form-group standard">
                                <?= lang("product_image", "product_image") ?> max (<?= byte_format($this->Settings->iwidth * $this->Settings->iheight) ?>)
                                <input id="xyz_product_img" type="file" name="product_image" style="display:none"/>
                                <button class="btn btn-primary" id="addProductImage">Add Product Image</button>
                            </div>
                            <div class="form-group all" ng-init="prod.store_id = '<?= (isset($_POST['store_id']) ? $_POST['store_id'] : ($product ? $product->store_id : '')) ?>'">
                                <?= lang("product_image", "product_image") ?> max (<?= byte_format($this->Settings->iwidth * $this->Settings->iheight) ?>)
                                <div class="input-group">
                                <input id="product_image" type="file" name="product_image" data-show-upload="false" data-show-preview="true" accept="image/*" multiple="multiple" class="form-control selec_clear file">
                                </div>
                            </div>
                            <div id="img-details"></div>
                        </div>
                            
                        </div>
                        <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                        <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                     </div>
                  </div> -->
                  <div class="row setup-content" id="step-2">
                     <div class="col-md-12" >
                        <div style="padding: 10px;">
                        <!-- <button class="btn btn-primary">Add More</button> -->
                        <div id="variation_1" class="row" style="padding-bottom: 10px;border: 1px solid #000; padding: 10px">

                        <div class="form-group col-md-2">
                                <?= lang("color", "color") ?>
                            <div class="input-group">
                                <?php
                                echo form_input('color', (isset($_POST['color']) ? $_POST['color'] : ($product ? $product->color : '')), 'class="form-control"');
                                ?>
                                <div class="input-group-addon no-print">
                                    <a href="<?php echo site_url('system_settings/AddProduct_para/color'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-2">
                                <?= lang("size", "size") ?>
                            <div class="input-group">
                                <?php
                                echo form_input('size', (isset($_POST['size']) ? $_POST['size'] : ($product ? $product->size : '')), 'class="form-control"');
                                ?>
                                <div class="input-group-addon no-print">
                                    <a href="<?php echo site_url('system_settings/AddProduct_para/size'); ?>?{{paraurl}}" data-toggle="modal" data-target="#myModal" class="external"><i class="fa fa-plus-circle" style="font-size: 26px" id="addIcon"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-2">
                                <?= lang("quantity", "quantity") ?>
                            <div class="input-group">
                                <?php
                                echo form_input('quantity', (isset($_POST['quantity']) ? $_POST['quantity'] : ($product ? $product->quantity : '')), 'class="form-control"');
                                ?>
                            </div>
                        </div>

                        <div class="form-group col-md-2">
                                <?= lang("price", "price") ?>
                            <div class="input-group">
                                <?php
                                echo form_input('price', (isset($_POST['price']) ? $_POST['price'] : ($product ? $product->price : '')), 'class="form-control"');
                                ?>
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                                <?= lang("description", "description") ?>
                            <div class="input-group">
                                <?php
                                echo form_input('description', (isset($_POST['description']) ? $_POST['description'] : ($product ? $product->description : '')), 'class="form-control"');
                                ?>
                            </div>
                        </div>
                        </div>
                        <div class="row" style="padding-bottom: 10px;">
                        <div class="form-group col-md-12">
                            <?= lang("product_image", "product_image") ?> max (<?= byte_format($this->Settings->iwidth * $this->Settings->iheight) ?>)
                            <div class="input-group">
                            <input id="product_image" type="file" name="product_image" data-show-upload="false" data-show-preview="true" accept="image/*" multiple="multiple" class="form-control selec_clear file">
                            </div>
                        </div>
                       </div>

                       <button type="button" class="btn btn-primary" id="addmorevariation">Add More Variations</button>

                       <!-- <div class="row" style="padding-bottom: 10px;">
                           <div class="col-md-3">
                               <?php echo form_dropdown('color[]', $color, (isset($_POST['color']) ? $_POST['color'] : ($product ? $product->color : '')), 'class="form-control" select2 id="color" placeholder="' . lang("select") . " " . lang("color") . '" style="width:100%"'); ?>
                           </div>
               
                           <div class="col-md-2">
                           <?php echo form_dropdown('size[]', $size, (isset($_POST['size']) ? $_POST['size'] : ($product ? $product->color : '')), 'class="form-control" select2 id="color" placeholder="' . lang("select") . " " . lang("size") . '" style="width:100%"'); ?>
                           </div>

                           <div class="col-md-2">
                               <input type="text" name="quantity[]" placeholder="Enter quantity" onlyno class="form-control"/>
                           </div>
                           <div class="col-md-2">
                               <input type="text" name="price[]" placeholder="Enter price" onlyno class="form-control"/>
                           </div>
                           <div class="col-md-2">
                              <a  id="addVariations" class="tip" title="<?= lang('add_more_variations') ?>">
                              <i class="fa fa-2x fa-times-circle removeVariation"></i></a>
                           </div>
                           <div class="col-md-12" style="padding-top: 10px;">
                              <textarea name="description[]"></textarea>
                           </div>
                       </div> -->
                    </div>
                           <button class="btn btn-primary prevBtn pull-left" type="button">Previous</button>
                           <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                     </div>
                  </div>
                </div>
            </form>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-primary" id="addItemManually"><?= lang('submit') ?></button>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript" src="<?= $assets ?>js/bootstrapValidator.min.js"></script>
<script type="text/javascript">
   $(document).ready(function () {
       var su = $('#list_suppliers').val();
       var supp_code = $('#list_suppliers option:selected').text();
       $('#supplier_name').attr('placeholder', supp_code);
       $('#supplier12').val(su);
       $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
           placeholder: "<?= lang('select_category_to_load') ?>", data: [
               {id: '', text: '<?= lang('select_category_to_load') ?>'}
           ]
       });
       $('#category').change(function () {
           var v = $(this).val();
           $('#modal-loading').show();
           if (v) {
               $.ajax({
                   type: "get",
                   async: false,
                   url: "<?= site_url('products/getSubCategories') ?>/" + v,
                   dataType: "json",
                   success: function (scdata) {
                       if (scdata != null) {
                           $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                               placeholder: "<?= lang('select_category_to_load') ?>",
                               data: scdata
                           });
                       }
                   },
                   error: function () {
                       bootbox.alert('<?= lang('ajax_error') ?>');
                       $('#modal-loading').hide();
                   }
               });
           } else {
               $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
                   placeholder: "<?= lang('select_category_to_load') ?>",
                   data: [{id: '', text: '<?= lang('select_category_to_load') ?>'}]
               });
           }
           $('#modal-loading').hide();
       });
       $('#code').bind('keypress', function (e) {
           if (e.keyCode == 13) {
               e.preventDefault();
               return false;
           }
       });
       $('#color').change(function () {
           var v = $(this).val();
           $.ajax({
               type: "get",
               async: false,
               url: "<?= site_url('products/getColorCode') ?>/" + v,
               dataType: "json",
               success: function (scdata) {
                   //                        alert(scdata);
                   if (scdata != null) {
                       var code = scdata.code;
                       $('#codet').val(code);
                       var quantity = $('#colorqty').val();
                       $('#colorcode').val(code + quantity);
                       $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                           placeholder: "<?= lang('select_category_to_load') ?>",
                           data: scdata
                       });
                   }
               },
               error: function () {
                   bootbox.alert('<?= lang('ajax_error') ?>');
                   $('#modal-loading').hide();
               }
           });
       });
       $('#colorqty').change(function () {
           $('#colorcode').attr("value", "");
           var codet = $('#codet').val();
           //        var quantity = $('#colorqty').val();
           var qty = $('#colorqty').val();
           $('#colorcode').attr("value", codet + qty);
       })
   
       $('#wh_qtys').change(function () {
           var v = $('#addproduct').serialize();
           var qty = $('#wh_qtys').val();
           $('#modal-loading').show();
           $.ajax({
               type: "get",
               data: v,
               async: false,
               url: "<?= site_url('products/getProSquentity') ?>?" + v,
               dataType: "json",
               success: function (scdata) {
                   if (scdata != 0) {
                       if (scdata.no_of_pic != '0') {
                           var sqty = parseInt(qty) / parseInt(scdata.no_of_pic);
                           $('#squantity').val(Number(Math.round(sqty)));
                       }
   //                                                                            var sqty = parseInt(qty) / parseInt(scdata.no_of_pic);
   //                                                                            $('#squantity').val(Number(Math.round(sqty)));
                   }
   //                                                                                        else{
   //                                                                                            $('#squantity').val('');
   //                                                                                        }
               },
               error: function () {
                   bootbox.alert('<?= lang('ajax_error') ?>');
                   $('#modal-loading').hide();
               }
           });
           $('#modal-loading').hide();
       });

       $("#addmorevariation").click(function(){

           //$("#variation_1")

           //$("#xyz_product_img").click();
       })
   });
   $(document).on('click', '#addManually', function (e) {
       $(".selec_clear").select2("val", "");
       $('#wh_qtys').val('');
   //                                                             $('#wh_qtys').text();
   
       //        $('#mModal').appendTo("body").modal('show');
       return false;
   });
   $('#addItemManually').click(function () {
       var v = $('#addproduct').serialize();
       var flag = 1;
       $('#modal-loading').show();
       var qty = $('#wh_qtys').val();
       if (qty == '') {
           $('#wh_qtys').addClass('has-error');
           flag = 0;
       }
   
       if (flag == 1) {
           $('.selec_clear').removeClass('has-error');
           $.ajax({
               type: "get",
               //data: v,
               async: false,
               url: "<?= site_url('products/saveAddOrderProduct') ?>?" + v,
               dataType: "json",
               success: function (scdata) {
                   if (scdata.status == 1) {
                       $.each(scdata.data, function (k, val) {
                           var mid = val.id,
                                   mcode = val.code,
                                   mname = val.name,
                                   mqty = val.quantity,
                                   sqty = val.squantity,
                                   unit_price = val.cost,
                                   design = val.design_name,
                                   brandname = val.brandname,
                                   colorname = val.colorname,
                                   designname = val.designname,
                                   stylename = val.stylename,
                                   patternname = val.patternname,
                                   fittingname = val.fittingname,
                                   fabricname = val.fabricname,
                                   rate = val.rate,
                                   uname = val.uname;
                           quitems[mid] = {"id": mid, "item_id": mid, "label": mname, "row": {"id": mid, "code": mcode, "page": 0, "cost": unit_price, "name": mname, "quantity": mqty, "squantity": sqty, "design_name": design, "real_unit_cost": unit_price, "price": unit_price, "unit_price": unit_price, "real_unit_price": unit_price, "tax_rate": 0, "tax_method": 0, "qty": mqty, "type": "manual", "discount": 0, "serial": "", "option": "", "brandname": brandname, "colorname": colorname, "designname": designname, "stylename": stylename, "patternname": patternname, "fittingname": fittingname, "fabricname": fabricname, "rate": rate, "uname": uname}, "tax_rate": 0, "options": false};
                           localStorage.setItem('quitems', JSON.stringify(quitems));
                           loadItems();
                       });
                       $('#mModal').modal('hide');
                       $('#mcode').val('');
                       $('#mname').val('');
                       $('#mtax').val('');
                       $('#mquantity').val('');
                       $('#mdiscount').val('');
                       $('#mprice').val('');
                       return false;
                   } else {
                       alert('Please Select All required fields');
                   }
               },
           });
           $('#modal-loading').hide();
       }
   });
</script>
<script>
   $(document).ready(function () {
   //            $('#user_id').append('<option value="" selected="selected">Select Order By</option>');
       if (($('#list_suppliers').val() == 0) || $('#list_suppliers').val() == null) {
           $('#list_suppliers').append('<option value="" selected="selected">Select Supplier Name</option>');
       }
   })
   $('#store').change(function () {
       // $('#companies').select2('val', 8);
       $('#list_suppliers').removeAttr('readonly', '');
       var v = $(this).val();
       $('#modal-loading').show();
       if (v) {
           $.ajax({
               type: "get",
               async: false,
               data: {store: v},
               url: "<?= site_url('purchases/getBiller') ?>",
               dataType: "json",
               success: function (scdata) {
   
   //                        $('#user_id').html('');
                   $('#list_suppliers').html('');
                   $('#list_suppliers').removeAttr('readonly', '');
                   //                    $('#list_suppliers').append('<option value="">Please Select Supplier Code</option>');
                   $.each(scdata, function (k, val) {
                       var opt = $('<option />');
                       opt.val(val.id);
                       opt.text(val.code + ' - ' + val.company);
                       $('#list_suppliers').append(opt);
                   })
                   $('#modal-loading').hide();
               },
               error: function () {
                   alert('No supplier found');
                   $('#list_suppliers').attr('readonly', '');
                   $('#modal-loading').hide();
               }
           });
   //                $.ajax({
   //                    type: "get",
   //                    async: false,
   //                    data: {store: v},
   //                    url: "<?= site_url('purchases/getEmployees') ?>",
   //                    dataType: "json",
   //                    success: function (scdata) {
   //
   //                        $('#user_id').html('');
   //                        $('#user_id').removeAttr('readonly', '');
   //                        $('#user_id').append('<option value="">Please Select Order By</option>');
   //                        $.each(scdata, function (k, val) {
   //                            var opt = $('<option />');
   //                            opt.val(val.id);
   //                            opt.text(val.fname + ' ' + val.mname + ' ' + val.lname);
   //                            $('#user_id').append(opt);
   //                        })
   //                    }, error: function () {
   //                        //                    $('#booked_by').addClass('hidden');
   //                        $('#user_id').attr('readonly', '');
   //                        $('#modal-loading').hide();
   //                    }
   //                })
   
   
           /*$.ajax({
               type: "get",
               async: false,
               data: {term: 1,
                   tab: 'department',
                   id: 8,
                   key: 'store_id',
                   limit: 10
               },
               url: "<?= site_url('products/getIdAttributes') ?>",
               dataType: "json",
               success: function (scdata) {
                   if (scdata) {
                       return {results: scdata.results};
                   } else {
                       return {results: [{id: '', text: 'No Match Found'}]};
                   }
               }, error: function () {
                   //                    $('#booked_by').addClass('hidden');
                   $('#user_id').attr('readonly', '');
                   $('#modal-loading').hide();
               }
           })*/
       }
   });
   $('#list_suppliers').change(function () {
       var su = $('#list_suppliers').val();
       var supp_code = $('#list_suppliers option:selected').text();
       $('#supplier_name').attr('placeholder', supp_code);
       $('#supplier12').val(su);
   });
</script>
<script type="text/javascript">
   $(document).ready(function () {
       $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
           placeholder: "<?= lang('select_category_to_load') ?>", data: [
               {id: '', text: '<?= lang('select_category_to_load') ?>'}
           ]
       });
       $('#category').change(function () {
           var v = $(this).val();
           $('#modal-loading').show();
           if (v) {
               $.ajax({
                   type: "get",
                   async: false,
                   url: "<?= site_url('products/getSubCategories') ?>/" + v,
                   dataType: "json",
                   success: function (scdata) {
                       if (scdata != null) {
                           $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                               placeholder: "<?= lang('select_category_to_load') ?>",
                               data: scdata
                           });
                       }
                   },
                   error: function () {
                       bootbox.alert('<?= lang('ajax_error') ?>');
                       $('#modal-loading').hide();
                   }
               });
           } else {
               $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
                   placeholder: "<?= lang('select_category_to_load') ?>",
                   data: [{id: '', text: '<?= lang('select_category_to_load') ?>'}]
               });
           }
           $('#modal-loading').hide();
       });
       $('#code').bind('keypress', function (e) {
           if (e.keyCode == 13) {
               e.preventDefault();
               return false;
           }
       });
       $('#color').change(function () {
           var v = $(this).val();
           $.ajax({
               type: "get",
               async: false,
               url: "<?= site_url('products/getColorCode') ?>/" + v,
               dataType: "json",
               success: function (scdata) {
                   //                        alert(scdata);
                   if (scdata != null) {
                       var code = scdata.code;
                       $('#codet').val(code);
                       var quantity = $('#colorqty').val();
                       $('#colorcode').val(code + quantity);
                       $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                           placeholder: "<?= lang('select_category_to_load') ?>",
                           data: scdata
                       });
                   }
               },
               error: function () {
                   bootbox.alert('<?= lang('ajax_error') ?>');
                   $('#modal-loading').hide();
               }
           });
       });
       $('#colorqty').change(function () {
           $('#colorcode').attr("value", "");
           var codet = $('#codet').val();
           //        var quantity = $('#colorqty').val();
           var qty = $('#colorqty').val();
           $('#colorcode').attr("value", codet + qty);
       })
   //            $('#wh_qtys').change(function () {
   //                
   //                var v = $('#addproduct').serialize();
   //                var qty = $('#wh_qtys').val();
   //                $('#modal-loading').show();
   //                $.ajax({
   //                    type: "get",
   //                    data: v,
   //                    async: false,
   //                    url: "<?= site_url('products/getProSquentity') ?>?" + v,
   //                    dataType: "json",
   //                    success: function (scdata) {
   //                        if (scdata != 0) {
   //                            var sqty = parseInt(qty) / parseInt(scdata.no_of_pic);
   //                            $('#squantity').val(Number(Math.round(sqty)));
   //                        }
   //                        //                else{
   //                        //                    $('#squantity').val('');
   //                        //                }
   //                    },
   //                    error: function () {
   //                        bootbox.alert('<?= lang('ajax_error') ?>');
   //                        $('#modal-loading').hide();
   //                    }
   //                });
   //                $('#modal-loading').hide();
   //            });
   });
</script>
<script>
   var autocomplete;
       
       var componentForm = {
         locality: 'long_name', // city
         administrative_area_level_1: 'long_name', //state
         administrative_area_level_2: 'long_name', //state
         country: 'long_name', // country
         Zip_Code: 'short_name' //pincode
       };
       var options = {
         types: ['(cities)'],
           // types: ['geocode'],
           componentRestrictions: {country: "in"}
        };
       
       initAutocomplete();
       
           function initAutocomplete() {
               autocomplete = new google.maps.places.Autocomplete(
               document.getElementById('city'), options );
               // autocomplete.setFields(['address_component']);
               autocomplete.addListener('place_changed', fillInAddress);
           }
           function fillInAddress() {
               var place = autocomplete.getPlace();
               console.log(place);
               var latitude = place.geometry.location.lat();
               var longitude = place.geometry.location.lng();
               $("#auto_address_latitude").val(latitude);
               $("#auto_address_longitude").val(longitude);
               for (var i = 0; i < place.address_components.length; i++) {
                   var addressType = place.address_components[i].types[0];
                   var val = place.address_components[i][componentForm[addressType]];
                   if (addressType == "administrative_area_level_2") {
                       // console.log(addressType+" " +val);
                       $("#city").val(val);
       
                   }
                   else if (addressType == "administrative_area_level_1") {
                       // console.log(addressType+" " +val);
                       $("#state").val(val);
       
                   }
                   else if (addressType == "country") {
                       // console.log(addressType+" " +val);
                       $("#country").val(val);
                       
                   }
                   else if (addressType == "Zip_Code") {
                       console.log(val);
                       $("#Zip_Code").val(val);
       
                   }
               }
           }
   
   
       $(document).ready(function () {
   
   
           /* $('#save').click(function(e){
   
               e.preventDefault();
                   return false;
   
               var bootstrapValidator = $('#form').data('bootstrapValidator');
               
               var result = bootstrapValidator.isValid();
   
               alert(result);
   
               if( result == false )
               {s
                   alert('false');
                   e.preventDefault();
                   return false;
               }
   
           }); */
   
           $('.align').fileinput({
               maxImageWidth: 300,
               maxImageHeight: 100
           });
   
   
           $.ajax({
               type: "get",
               async: false,
               url: "<?= site_url('suppliers/getNewSupplierCode') ?>",
               dataType: "json",
               success: function (data) {
                   if (data != null) {
                       $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                           placeholder: "<?= lang('select_category_to_load') ?>",
                           data: data
                       });
                       // alert(data.id);
                       $('#Supplier_Code').val('SUP' + data.id);
                   }
               },
               error: function () {
                   bootbox.alert('<?= lang('ajax_error') ?>');
                   $('#modal-loading').hide();
               }
           });
   
   
   
   
   
           var navListItems = $('div.setup-panel div a'),
                   allWells = $('.setup-content'),
                   allNextBtn = $('.nextBtn'),
                   allPrevBtn = $('.prevBtn');
   
           allWells.hide();
   
           // console.log($('div.setup-panel div a'));
   
           navListItems.click(function (e) {
               e.preventDefault();
               var $target = $($(this).attr('href')),
                       $item = $(this);
   
               if (!$item.hasClass('disabled')) {
                   navListItems.removeClass('btn-primary').addClass('btn-default');
                   $item.addClass('btn-primary');
                   allWells.hide();
                   $target.show();
   //          $target.find('input:eq(0)').focus();
               }
           });
   
           allPrevBtn.click(function () {
               var curStep = $(this).closest(".setup-content"),
                       curStepBtn = curStep.attr("id"),
                       prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a");
   
               prevStepWizard.removeAttr('disabled').trigger('click');
           });
   
           allNextBtn.click(function () {
               var curStep = $(this).closest(".setup-content"),
                       curStepBtn = curStep.attr("id"),
                       nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                       curInputs = curStep.find("input[type='text'],input[type='file'],input[type='url'],input[type='email'],fieldset > input[type='text']"),
                       isValid = true;
               
                       console.log(curInputs);
                       console.log(curStepBtn);
               $(".form-group").removeClass("has-error");
            //    for (var i = 0; i < curInputs.length; i++) {
            //        if (!curInputs[i].validity.valid) {
            //            isValid = false;
            //            $(curInputs[i]).closest(".form-group").addClass("has-error");
            //        }
            //    }
   
               if (isValid)
               {
   
                   nextStepWizard.removeAttr('disabled').trigger('click');
                   if(curStepBtn == "step-3")
                   {
                       // $("#form").submit();
                       document.getElementById("form").submit()
   
                   }
               }
           });
   
           $('div.setup-panel div a.btn-primary').trigger('click');
       });
</script>
