

<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel">Add New <?= ucwords(str_replace("_", " ", $tab)) ?></h4>
        </div>
        <?php
//        $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'addParaForm');
//        echo form_open_multipart("customers/add", $attrib);
        ?>
        <form data-toggle="validator" role="form" id="addParaForm">
            <div class="modal-body">
                <p><?= lang('enter_info'); ?></p>
                <div class="form-group">
                    <label class="control-label" for="name">Name</label>
                    <div class="controls"> 
                        <?php echo form_input('para_name', '', 'class="form-control tip" id="name" required data-bv-notempty="true"'); ?>
                    </div>
                </div>
                <?php if ($tab == "department") { ?>
                    <div class="form-group">
                        <label class="control-label" for="parmstore_id">Store</label>
                        <div class="controls"> 
                            <?php echo form_input('store_id', '', 'class="form-control product_para_' . $tab . '" id="parmstore_id" data-tab="companies" placeholder="select store" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($tab == "section") {
                    ?>
                    <div class="form-group">
                        <label class="control-label" for="parmstore_id">Store</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control product_para_' . $tab . '" id="parmstore_id" data-tab="companies" data-id="0" placeholder="select store" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmdepartment_id">Department</label>
                        <div class="controls"> 
                            <?php
                            echo form_input('department_id', '', 'class="form-control product_para_' . $tab . '" default-attrib data-tab="department" data-key="store_id" data-id="parmstore_id" id="parmdepartment" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($tab == "product_items") { ?>
                    <div class="form-group">
                        <label class="control-label" for="parmstore_id">Store</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control product_para_' . $tab . '" id="parmstore_id" data-tab="companies" data-id="0" placeholder="select store" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmdepartment_id">Department</label>
                        <div class="controls"> 
                            <?php
                            echo form_input('', '', 'class="form-control product_para_' . $tab . '" default-attrib data-tab="department" data-key="store_id" data-id="parmstore_id" id="parmdepartment" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmsection_id">Section</label>
                        <div class="controls"> 
                            <?php echo form_input('section_id', '', 'class="form-control product_para_' . $tab . '" default-attrib data-tab="section" id="parmsection" data-key="department_id" data-id="parmdepartment" placeholder="select section" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($tab == "type") { ?>
                    <div class="form-group">
                        <label class="control-label" for="parmstore_id">Store</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control product_para_' . $tab . '" id="parmstore_id" data-tab="companies" data-id="0" placeholder="select store" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmdepartment_id">Department</label>
                        <div class="controls"> 
                            <?php
                            echo form_input('', '', 'class="form-control product_para_' . $tab . '" default-attrib data-tab="department" data-key="store_id" data-id="parmstore_id" id="parmdepartment" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmsection_id">Section</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control" default-attrib data-tab="section" id="parmsection" data-key="department_id" data-id="parmdepartment" placeholder="' . lang("select") . " " . lang("section") . '"  required="required" style="width:100%"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmproduct_items_id">Products Item</label>
                        <div class="controls"> 
                            <?php echo form_input('product_items_id', '', 'class="form-control" default-attrib data-tab="product_items" data-key="section_id" data-id="parmsection" id="parmproduct_items" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%" '); ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($tab == "brands") { ?>
                    <div class="form-group">
                        <label class="control-label" for="parmstore_id">Store</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control product_para_' . $tab . '" id="parmstore_id" data-tab="companies" data-id="0" placeholder="select store" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmdepartment_id">Department</label>
                        <div class="controls"> 
                            <?php
                            echo form_input('', '', 'class="form-control product_para_' . $tab . '" default-attrib data-tab="department" data-key="store_id" data-id="parmstore_id" id="parmdepartment" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmsection_id">Section</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control  product_para_' . $tab . '" default-attrib data-tab="section" id="parmsection" data-key="department_id" data-id="parmdepartment" placeholder="' . lang("select") . " " . lang("section") . '"  required="required" style="width:100%"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="parmproduct_items_id">Products Item</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control  product_para_' . $tab . '" default-attrib data-tab="product_items" data-key="section_id" data-id="parmsection" id="parmproduct_items" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%" '); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmtype_id">Type</label>
                        <div class="controls">
                            <?php
                            echo form_input('type_id', '', 'class="form-control" default-attrib data-tab="type" id="parmtype_id" data-key="product_items_id" data-id="parmproduct_items" placeholder="' . lang("select") . " " . lang("type") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>



                <?php } ?>
                <?php if ($tab == "design") { ?>
                    <div class="form-group">
                        <label class="control-label" for="parmstore_id">Store</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control product_para_' . $tab . '" id="parmstore_id" data-tab="companies" data-id="0" placeholder="select store" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmdepartment_id">Department</label>
                        <div class="controls"> 
                            <?php
                            echo form_input('', '', 'class="form-control product_para_' . $tab . '" default-attrib data-tab="department" data-key="store_id" data-id="parmstore_id" id="parmdepartment" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmsection_id">Section</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control  product_para_' . $tab . '" default-attrib data-tab="section" id="parmsection" data-key="department_id" data-id="parmdepartment" placeholder="' . lang("select") . " " . lang("section") . '"  required="required" style="width:100%"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="parmproduct_items_id">Products Item</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control  product_para_' . $tab . '" default-attrib data-tab="product_items" data-key="section_id" data-id="parmsection" id="parmproduct_items" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%" '); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmtype_id">Type</label>
                        <div class="controls">
                            <?php
                            echo form_input('', '', 'class="form-control" default-attrib data-tab="type" id="parmtype_id" data-key="product_items_id" data-id="parmproduct_items" placeholder="' . lang("select") . " " . lang("type") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmbrands_id">Brand</label>
                        <div class="controls"> 
                            <?php echo form_input('brands_id', '', 'class="form-control product_para_' . $tab . '" id="parmbrands_id" data-tab="brands" data-key="type_id" data-id="parmtype_id" placeholder="select brands" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($tab == "style") { ?>
                    <div class="form-group">
                        <label class="control-label" for="parmstore_id">Store</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control product_para_' . $tab . '" id="parmstore_id" data-tab="companies" data-id="0" placeholder="select store" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmdepartment_id">Department</label>
                        <div class="controls"> 
                            <?php
                            echo form_input('', '', 'class="form-control product_para_' . $tab . '" default-attrib data-tab="department" data-key="store_id" data-id="parmstore_id" id="parmdepartment" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmsection_id">Section</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control  product_para_' . $tab . '" default-attrib data-tab="section" id="parmsection" data-key="department_id" data-id="parmdepartment" placeholder="' . lang("select") . " " . lang("section") . '"  required="required" style="width:100%"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="parmproduct_items_id">Products Item</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control  product_para_' . $tab . '" default-attrib data-tab="product_items" data-key="section_id" data-id="parmsection" id="parmproduct_items" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%" '); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmtype_id">Type</label>
                        <div class="controls">
                            <?php
                            echo form_input('', '', 'class="form-control" default-attrib data-tab="type" id="parmtype_id" data-key="product_items_id" data-id="parmproduct_items" placeholder="' . lang("select") . " " . lang("type") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmbrands_id">Brand</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control product_para_' . $tab . '" id="parmbrands_id" data-tab="brands" data-key="type_id" data-id="parmtype_id" placeholder="select brands" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmdesign_id">Design</label>
                        <div class="controls"> 
                            <?php echo form_input('design_id', '', 'class="form-control " id="parmdesign_id"  data-tab="design" data-key="brands_id" data-id="parmbrands_id" placeholder="select design" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($tab == "pattern") { ?>
                    <div class="form-group">
                        <label class="control-label" for="parmstore_id">Store</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control product_para_' . $tab . '" id="parmstore_id" data-tab="companies" data-id="0" placeholder="select store" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmdepartment_id">Department</label>
                        <div class="controls"> 
                            <?php
                            echo form_input('', '', 'class="form-control product_para_' . $tab . '" default-attrib data-tab="department" data-key="store_id" data-id="parmstore_id" id="parmdepartment" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmsection_id">Section</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control  product_para_' . $tab . '" default-attrib data-tab="section" id="parmsection" data-key="department_id" data-id="parmdepartment" placeholder="' . lang("select") . " " . lang("section") . '"  required="required" style="width:100%"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="parmproduct_items_id">Products Item</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control  product_para_' . $tab . '" default-attrib data-tab="product_items" data-key="section_id" data-id="parmsection" id="parmproduct_items" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%" '); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmtype_id">Type</label>
                        <div class="controls">
                            <?php
                            echo form_input('', '', 'class="form-control" default-attrib data-tab="type" id="parmtype_id" data-key="product_items_id" data-id="parmproduct_items" placeholder="' . lang("select") . " " . lang("type") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmbrands_id">Brand</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control product_para_' . $tab . '" id="parmbrands_id" data-tab="brands" data-key="type_id" data-id="parmtype_id" placeholder="select brands" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmdesign_id">Design</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control " id="parmdesign_id"  data-tab="design" data-key="brands_id" data-id="parmbrands_id" placeholder="select design" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmstyle_id">Style</label>
                        <div class="controls"> 
                            <?php echo form_input('style_id', '', 'class="form-control product_para_' . $tab . '" id="parmstyle_id"  data-tab="style" data-key="design_id" data-id="parmdesign_id" placeholder="select style" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($tab == "fitting") { ?>
                    <div class="form-group">
                        <label class="control-label" for="parmstore_id">Store</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control product_para_' . $tab . '" id="parmstore_id" data-tab="companies" data-id="0" placeholder="select store" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="parmdepartment_id">Department</label>
                        <div class="controls"> 
                            <?php
                            echo form_input('', '', 'class="form-control product_para_' . $tab . '" default-attrib data-tab="department" data-key="store_id" data-id="parmstore_id" id="parmdepartment" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="section_id">Section</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control  product_para_' . $tab . '" default-attrib data-tab="section" id="parmsection" data-key="department_id" data-id="parmdepartment" placeholder="' . lang("select") . " " . lang("section") . '"  required="required" style="width:100%"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="product_items_id">Products Item</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control  product_para_' . $tab . '" default-attrib data-tab="product_items" data-key="section_id" data-id="parmsection" id="parmproduct_items" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%" '); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="type_id">Type</label>
                        <div class="controls">
                            <?php
                            echo form_input('', '', 'class="form-control" default-attrib data-tab="type" id="parmtype_id" data-key="product_items_id" data-id="parmproduct_items" placeholder="' . lang("select") . " " . lang("type") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="brands_id">Brand</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control product_para_' . $tab . '" id="parmbrands_id" data-tab="brands" data-key="type_id" data-id="parmtype_id" placeholder="select brands" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="design_id">Design</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control " id="parmdesign_id"  data-tab="design" data-key="brands_id" data-id="parmbrands_id" placeholder="select design" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="style_id">Style</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control product_para_' . $tab . '" id="parmstyle_id"  data-tab="style" data-key="design_id" data-id="parmdesign_id" placeholder="select style" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="pattern_id">Pattern</label>
                        <div class="controls"> 
                            <?php echo form_input('pattern_id', '', 'class="form-control product_para_' . $tab . '" id="parmpattern_id"  data-tab="pattern" data-key="style_id" data-id="parmstyle_id" placeholder="select pattern" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($tab == "fabric") { ?>
                    <div class="form-group">
                        <label class="control-label" for="parmstore_id">Store</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control product_para_' . $tab . '" id="parmstore_id" data-tab="companies" data-id="0" placeholder="select store" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="department_id">Department</label>
                        <div class="controls"> 
                            <?php
                            echo form_input('', '', 'class="form-control product_para_' . $tab . '" default-attrib data-tab="department" data-key="store_id" data-id="parmstore_id" id="parmdepartment" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="section_id">Section</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control  product_para_' . $tab . '" default-attrib data-tab="section" id="parmsection" data-key="department_id" data-id="parmdepartment" placeholder="' . lang("select") . " " . lang("section") . '"  required="required" style="width:100%"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="product_items_id">Products Item</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control  product_para_' . $tab . '" default-attrib data-tab="product_items" data-key="section_id" data-id="parmsection" id="parmproduct_items" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%" '); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="type_id">Type</label>
                        <div class="controls">
                            <?php
                            echo form_input('', '', 'class="form-control" default-attrib data-tab="type" id="parmtype_id" data-key="product_items_id" data-id="parmproduct_items" placeholder="' . lang("select") . " " . lang("type") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="brands_id">Brand</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control product_para_' . $tab . '" id="parmbrands_id" data-tab="brands" data-key="type_id" data-id="parmtype_id" placeholder="select brands" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="design_id">Design</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control " id="parmdesign_id"  data-tab="design" data-key="brands_id" data-id="parmbrands_id" placeholder="select design" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="style_id">Style</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control product_para_' . $tab . '" id="parmstyle_id"  data-tab="style" data-key="design_id" data-id="parmdesign_id" placeholder="select style" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="pattern_id">Pattern</label>
                        <div class="controls"> 
                            <?php echo form_input('', '', 'class="form-control product_para_' . $tab . '" id="parmpattern_id"  data-tab="pattern" data-key="style_id" data-id="parmstyle_id" placeholder="select pattern" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="fitting_id">Fitting</label>
                        <div class="controls"> 
                            <?php echo form_input('fitting_id', '', 'class="form-control product_para_' . $tab . '" id="parmfitting_id" data-tab="fitting" data-key="pattern_id" data-id="parmpattern_id" placeholder="select fitting" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($tab == "color") { ?>
                    <div class="form-group">
                        <label class="control-label" for="code">Code</label>
                        <div class="controls"> 
                            <?php echo form_input('code', '', 'class="form-control tip" id="code" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($tab == "size") { ?>
                    <div class="form-group">
                        <label class="control-label" for="code">Code</label>
                        <div class="controls"> 
                            <?php echo form_input('code', '', 'class="form-control tip" id="code" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="department_id">Department</label>
                        <!--                        <div class="controls"> 
                        <?php echo form_input('department_id', '', 'class="form-control"  data-tab="department" id="parmdepartment_id" placeholder="select department" required data-bv-notempty="true"'); ?>
                                                </div>-->
                        <div class="controls"> 
                            <?php
                            echo form_input('department_id', '', 'class="form-control" default-attrib data-tab="department"  id="parmdepartment" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%"');
                            ?>
                            <?php // echo form_input('department_id', '', 'class="form-control product_para_' . $tab . '" id="department_id"  data-tab="department" placeholder="select department" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="section_id">Section</label>
                        <!--                        <div class="controls"> 
                        <?php echo form_input('section_id', '', 'class="form-control"  data-key="department_id" data-id="parmsection_id" id="section" data-tab="section" placeholder="select section" required data-bv-notempty="true"'); ?>
                                                </div>-->
                        <div class="controls"> 
                            <?php
                            echo form_input('section_id', '', 'class="form-control" default-attrib data-tab="section" id="parmsection" data-key="department_id" data-id="parmdepartment"  required="required" style="width:100%"');
                            ?>
                            <?php // echo form_input('section_id', '', 'class="form-control product_para_' . $tab  .'" id="section_id" data-tab="section" placeholder="select section" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="product_items_id">Products Item</label>
                        <!--                        <div class="controls"> 
                        <?php echo form_input('product_items_id', '', 'class="form-control"  data-key="section_id" data-id="parmsection" id="parmproduct_items_id" data-tab="product_items" placeholder="select product items" required data-bv-notempty="true"'); ?>
                                                </div>-->
                        <div class="controls"> 
                            <?php
                            echo form_input('product_items_id', '', 'class="form-control" default-attrib data-tab="product_items" data-key="section_id" data-id="parmsection" id="parmproduct_items" required="required" style="width:100%"');
                            ?>
                            <?php // echo form_input('product_items_id', '', 'class="form-control product_para_' . $tab  .'" id="product_items_id" data-tab="product_items" placeholder="select product items" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="form-group">
                    <label class="control-label" for="customer_group">Status</label>
                    <div class="controls"> 
                        <?php echo form_dropdown('status', array('Active' => "Active", 'Deactive' => "Deactive"), '', 'class="form-control tip select" id="status" data-bv-notempty="true"'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?php echo form_submit('addPara', "Add", 'class="btn btn-primary"'); ?>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript" src="<?= $assets ?>js/validator.js"></script>
<script type="text/javascript">

    $(document).ready(function () {
        
//        $("#addParaForm").validator();
//        $("#addParaForm").submit(function (e) {
        $("#addParaForm").unbind().submit(function (e) {

            var product_margin = '<?= $tab ?>';

            if ($('#name').val() == "") {
                return;
            }
//            if($('#store_id').val() == ""){
//                return;
//            }
//            if($('#department_id').val() == ""){
//                return;
//            }
//            if($('#section_id').val() == ""){
//                return;
//            }
//            if($('#product_items_id').val() == ""){
//                return;
//            }
//            if($('#type_id').val() == ""){
//                return;
//            }
//            if($('#brands_id').val() == ""){
//                return;
//            }
//            if($('#design_id').val() == ""){
//                return;
//            }
//            if($('#style_id').val() == ""){
//                return;
//            }
//            if($('#pattern_id').val() == ""){
//                return;
//            }
//            if($('#fitting_id').val() == ""){
//                return;
//            }
//            if($('#code').val() == ""){
//                return;
//            }
            var form = $("#addParaForm");
            var $data = $(this).serialize();

            e.preventDefault();
            $.ajax({
                url: "<?= site_url('system_settings/AddAjaxProduct_para/' . $tab . "/?v=1") ?>",
                type: "get", async: false,
                data: $data,
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    if (data.s === "true") {
                        bootbox.alert('Parameter Added Successfully!');
//                        redirect('system_settings/products_para');
//                        window.location.href = site_url + "system_settings/products_para";
                        document.getElementById("addParaForm").reset();
//                        $('#myModal').hide(); 
                        $('#myModal').modal('toggle');
                        oTable['<?= $tab ?>'].fnDraw();

                    } else if (data.s == "ex") {
                        bootbox.alert('Parameter Already Exists !');

                    } else {
                        bootbox.alert('Parameter Adding Failed !');
                    }
                }
            });
            return false;
        });

//        $("#addParaForm").click(function () {
////            alert();
//            $("#addParaForm").submit();
//            $("#section_id").focus();
//            return false;
//        });


        pp = $(".product_para_<?= $tab ?>");
        pp.select2({
//            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site_url + "products/getIdAttribute",
                    data: {
                        term: pp.val(),
                        tab: pp.data('tab')
                    },
                    dataType: "json",
                    
                    success: function (data) {
                        callback(data);
                    }
              
                });
            },
            ajax: {
                url: site_url + "products/getIdAttributes",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        tab: pp.data('tab'),
                        limit: 10
                    };
                },
                results: function (data, page) {
//                    alert();
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'No Match Found'}]};
                    }
                }
            }
        });
        var obj = ['parmdepartment', 'parmsection', 'parmproduct_items', 'parmtype_id', 'parmbrands_id', 'parmdesign_id', 'parmstyle_id', 'parmpattern_id', 'parmfitting_id'];
        var objarr = [];
        var i = 0;
        $.each(obj, function (k, v) {
            objarr[i] = myselect2(v);
            objarr[i]();
            i++;
        });
        function myselect2(id) {
            var pp = $("#" + id);
            function get() {
                pp.select2({
//                    minimumInputLength: 1,
                    data: [],
                    initSelection: function (element, callback) {
                        $.ajax({
                            type: "get", async: false,
                            url: site_url + "products/getIdAttribute",
                            data: {
                                term: pp.val(),
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                            },
                            dataType: "json",
                            success: function (data) {
                                callback(data[0]);
                            }
                        });
                    },
                    ajax: {
                        url: site_url + "products/getIdAttributes",
                        dataType: 'json',
                        quietMillis: 15,
                        data: function (term, page) {
                            return {
                                term: term,
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                                limit: 10
                            };
                        },
                        results: function (data, page) {
                            if (data.results != null) {
                                return {results: data.results};
                            } else {
                                return {results: [{id: '', text: 'No Match Found'}]};
                            }
                        }
                    }
                });
            }
            return get;
        }
    });
</script>
