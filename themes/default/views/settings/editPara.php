<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel">Update <?= ucwords(str_replace("_", " ", $tab)) ?></h4>
        </div>
        <?php
//        $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'addParaForm');
//        echo form_open_multipart("customers/add", $attrib);
        ?>
        <form data-toggle="validator" role="form" id="editParaForm">
            <div class="modal-body">
                <p><?= lang('enter_info'); ?></p>
                <input type="hidden" name="id" value="<?= $d->id ?>" />
                <div class="form-group">
                    <label class="control-label" for="para_name">Name</label>
                    <div class="controls"> 
                        <?php echo form_input('para_name', $d->name, 'class="form-control tip" id="para_name" data-bv-notempty="true"'); ?>
                    </div>
                </div>
                <?php if ($tab == "department") { ?>
                    <div class="form-group">
                        <label class="control-label" for="store_id">Store</label>
                        <div class="controls"> 
                            <?php echo form_input('store_id', $d->store_id, 'class="form-control product_para_' . $tab . '" id="store_id" data-tab="companies" placeholder="select store" data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($tab == "per") { ?>
                    <div class="form-group" >
                        <div class="checkbox-inline">
                            <input type="checkbox" class="checkbox" name="decimal_req" id="decimal_req"  <?= $d->decimal_req ? 'checked':''?> value="1">
                            <label for="decimal_req" class="padding05">Decimal Required</label>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($tab == "section") { ?>
                    <div class="form-group">
                        <label class="control-label" for="store_id">Store</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->store_id, 'class="form-control product_para_' . $tab . '" id="store_id" data-tab="companies" data-id="0" placeholder="select store" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="department_id">Department</label>
                        <div class="controls"> 
                            <?php
                            echo form_input('department_id', $d->department_id, 'class="form-control product_para_' . $tab . '" default-attrib data-tab="department" data-key="store_id" data-id="store_id" id="department" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>

                <?php } ?>
                <?php if ($tab == "product_items") { ?>

                    <div class="form-group">
                        <label class="control-label" for="store_id">Store</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->store_id, 'class="form-control product_para_' . $tab . '" id="store_id" data-tab="companies" data-id="0" placeholder="select store" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="department_id">Department</label>
                        <div class="controls"> 
                            <?php
                            echo form_input('', $d->department_id, 'class="form-control product_para_' . $tab . '" default-attrib data-tab="department" data-key="store_id" data-id="store_id" id="department" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="section_id">Section</label>
                        <div class="controls"> 
                            <?php echo form_input('section_id', $d->section_id, 'class="form-control product_para_' . $tab . '" default-attrib data-tab="section" id="section" data-key="department_id" data-id="department" placeholder="select section" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>

                <?php } ?>
                <?php if ($tab == "type") { ?>

                    <div class="form-group">
                        <label class="control-label" for="store_id">Store</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->store_id, 'class="form-control product_para_' . $tab . '" id="store_id" data-tab="companies" data-id="0" placeholder="select store" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="department_id">Department</label>
                        <div class="controls"> 
                            <?php
                            echo form_input('', $d->department_id, 'class="form-control product_para_' . $tab . '" default-attrib data-tab="department" data-key="store_id" data-id="store_id" id="department" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="section_id">Section</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->section_id, 'class="form-control" default-attrib data-tab="section" id="section" data-key="department_id" data-id="department" placeholder="' . lang("select") . " " . lang("section") . '"  required="required" style="width:100%"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="product_items_id">Products Item</label>
                        <div class="controls"> 
                            <?php echo form_input('product_items_id', $d->product_items_id, 'class="form-control" default-attrib data-tab="product_items" data-key="section_id" data-id="section" id="product_items" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%" '); ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($tab == "brands") { ?>
                    <div class="form-group">
                        <label class="control-label" for="store_id">Store</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->store_id, 'class="form-control product_para_' . $tab . '" id="store_id" data-tab="companies" data-id="0" placeholder="select store" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="department_id">Department</label>
                        <div class="controls"> 
                            <?php
                            echo form_input('', $d->department_id, 'class="form-control product_para_' . $tab . '" default-attrib data-tab="department" data-key="store_id" data-id="store_id" id="department" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="section_id">Section</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->section_id, 'class="form-control  product_para_' . $tab . '" default-attrib data-tab="section" id="section" data-key="department_id" data-id="department" placeholder="' . lang("select") . " " . lang("section") . '"  required="required" style="width:100%"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="product_items_id">Products Item</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->product_items_id, 'class="form-control  product_para_' . $tab . '" default-attrib data-tab="product_items" data-key="section_id" data-id="section" id="product_items" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%" '); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="type_id">Type</label>
                        <div class="controls">
                            <?php
                            echo form_input('type_id', $d->type_id, 'class="form-control" default-attrib data-tab="type" id="type_id" data-key="product_items_id" data-id="product_items" placeholder="' . lang("select") . " " . lang("type") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($tab == "design") { ?>
                    <div class="form-group">
                        <label class="control-label" for="store_id">Store</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->store_id, 'class="form-control product_para_' . $tab . '" id="store_id" data-tab="companies" data-id="0" placeholder="select store" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="department_id">Department</label>
                        <div class="controls"> 
                            <?php
                            echo form_input('', $d->department_id, 'class="form-control product_para_' . $tab . '" default-attrib data-tab="department" data-key="store_id" data-id="store_id" id="department" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="section_id">Section</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->section_id, 'class="form-control  product_para_' . $tab . '" default-attrib data-tab="section" id="section" data-key="department_id" data-id="department" placeholder="' . lang("select") . " " . lang("section") . '"  required="required" style="width:100%"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="product_items_id">Products Item</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->product_items_id, 'class="form-control  product_para_' . $tab . '" default-attrib data-tab="product_items" data-key="section_id" data-id="section" id="product_items" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%" '); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="type_id">Type</label>
                        <div class="controls">
                            <?php
                            echo form_input('', $d->type_id, 'class="form-control" default-attrib data-tab="type" id="type_id" data-key="product_items_id" data-id="product_items" placeholder="' . lang("select") . " " . lang("type") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="brands_id">Brand</label>
                        <div class="controls"> 
                            <?php echo form_input('brands_id', $d->brands_id, 'class="form-control product_para_' . $tab . '" id="brands_id" data-tab="brands" data-key="type_id" data-id="type_id" placeholder="select brands" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($tab == "style") { ?>
                    <div class="form-group">
                        <label class="control-label" for="store_id">Store</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->store_id, 'class="form-control product_para_' . $tab . '" id="store_id" data-tab="companies" data-id="0" placeholder="select store" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="department_id">Department</label>
                        <div class="controls"> 
                            <?php
                            echo form_input('', $d->department_id, 'class="form-control product_para_' . $tab . '" default-attrib data-tab="department" data-key="store_id" data-id="store_id" id="department" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="section_id">Section</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->section_id, 'class="form-control  product_para_' . $tab . '" default-attrib data-tab="section" id="section" data-key="department_id" data-id="department" placeholder="' . lang("select") . " " . lang("section") . '"  required="required" style="width:100%"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="product_items_id">Products Item</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->product_items_id, 'class="form-control  product_para_' . $tab . '" default-attrib data-tab="product_items" data-key="section_id" data-id="section" id="product_items" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%" '); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="type_id">Type</label>
                        <div class="controls">
                            <?php
                            echo form_input('', $d->type_id, 'class="form-control" default-attrib data-tab="type" id="type_id" data-key="product_items_id" data-id="product_items" placeholder="' . lang("select") . " " . lang("type") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="brands_id">Brand</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->brands_id, 'class="form-control product_para_' . $tab . '" id="brands_id" data-tab="brands" data-key="type_id" data-id="type_id" placeholder="select brands" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="design_id">Design</label>
                        <div class="controls"> 
                            <?php echo form_input('design_id', $d->design_id, 'class="form-control " id="design_id"  data-tab="design" data-key="brands_id" data-id="brands_id" placeholder="select design" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($tab == "pattern") { ?>
                    <div class="form-group">
                        <label class="control-label" for="store_id">Store</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->store_id, 'class="form-control product_para_' . $tab . '" id="store_id" data-tab="companies" data-id="0" placeholder="select store" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="department_id">Department</label>
                        <div class="controls"> 
                            <?php
                            echo form_input('', $d->department_id, 'class="form-control" default-attrib data-tab="department" data-key="store_id" data-id="store_id" id="department" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="section_id">Section</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->section_id, 'class="form-control  product_para_' . $tab . '" default-attrib data-tab="section" id="section" data-key="department_id" data-id="department" placeholder="' . lang("select") . " " . lang("section") . '"  required="required" style="width:100%"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="product_items_id">Products Item</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->product_items_id, 'class="form-control" default-attrib data-tab="product_items" data-key="section_id" data-id="section" id="product_items" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%" '); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="type_id">Type</label>
                        <div class="controls">
                            <?php
                            echo form_input('', $d->type_id, 'class="form-control" default-attrib data-tab="type" id="type_id" data-key="product_items_id" data-id="product_items" placeholder="' . lang("select") . " " . lang("type") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="brands_id">Brand</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->brands_id, 'class="form-control" id="brands_id" data-tab="brands" data-key="type_id" data-id="type_id" placeholder="select brands" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="design_id">Design</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->design_id, 'class="form-control " id="design_id"  data-tab="design" data-key="brands_id" data-id="brands_id" placeholder="select design" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="style_id">Style</label>
                        <div class="controls"> 
                            <?php echo form_input('style_id', $d->style_id, 'class="form-control " id="style_id"  data-tab="style" data-key="design_id" data-id="design_id" placeholder="select style" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <!--                    <div class="form-group">
                                            <label class="control-label" for="style_id">Style</label>
                                            <div class="controls"> 
                    <?php // echo form_input('style_id', $d->style_id, 'class="form-control product_para_' . $tab . '" id="style_id" data-tab="style" placeholder="select style" data-bv-notempty="true"'); ?>
                                            </div>
                                        </div>-->
                <?php } ?>
                <?php if ($tab == "fitting") { ?>
                    <div class="form-group">
                        <label class="control-label" for="store_id">Store</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->store_id, 'class="form-control product_para_' . $tab . '" id="store_id" data-tab="companies" data-id="0" placeholder="select store" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="department_id">Department</label>
                        <div class="controls"> 
                            <?php
                            echo form_input('', $d->department_id, 'class="form-control" default-attrib data-tab="department" data-key="store_id" data-id="store_id" id="department" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="section_id">Section</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->section_id, 'class="form-control" default-attrib data-tab="section" id="section" data-key="department_id" data-id="department" placeholder="' . lang("select") . " " . lang("section") . '"  required="required" style="width:100%"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="product_items_id">Products Item</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->product_items_id, 'class="form-control" default-attrib data-tab="product_items" data-key="section_id" data-id="section" id="product_items" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%" '); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="type_id">Type</label>
                        <div class="controls">
                            <?php
                            echo form_input('', $d->type_id, 'class="form-control" default-attrib data-tab="type" id="type_id" data-key="product_items_id" data-id="product_items" placeholder="' . lang("select") . " " . lang("type") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="brands_id">Brand</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->brands_id, 'class="form-control " id="brands_id" data-tab="brands" data-key="type_id" data-id="type_id" placeholder="select brands" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="design_id">Design</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->design_id, 'class="form-control " id="design_id"  data-tab="design" data-key="brands_id" data-id="brands_id" placeholder="select design" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="style_id">Style</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->style_id, 'class="form-control" id="style_id"  data-tab="style" data-key="design_id" data-id="design_id" placeholder="select style" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="pattern_id">Pattern</label>
                        <div class="controls"> 
                            <?php echo form_input('pattern_id', $d->pattern_id, 'class="form-control" id="pattern_id"  data-tab="pattern" data-key="style_id" data-id="style_id" placeholder="select pattern" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>

                <?php } ?>
                <?php if ($tab == "fabric") { ?>
                    <div class="form-group">
                        <label class="control-label" for="store_id">Store</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->store_id, 'class="form-control product_para_' . $tab . '" id="store_id" data-tab="companies" data-id="0" placeholder="select store" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="department_id">Department</label>
                        <div class="controls"> 
                            <?php
                            echo form_input('', $d->department_id, 'class="form-control" default-attrib data-tab="department" data-key="store_id" data-id="store_id" id="department" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="section_id">Section</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->section_id, 'class="form-control" default-attrib data-tab="section" id="section" data-key="department_id" data-id="department" placeholder="' . lang("select") . " " . lang("section") . '"  required="required" style="width:100%"'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="product_items_id">Products Item</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->product_items_id, 'class="form-control" default-attrib data-tab="product_items" data-key="section_id" data-id="section" id="product_items" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%" '); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="type_id">Type</label>
                        <div class="controls">
                            <?php
                            echo form_input('', $d->type_id, 'class="form-control" default-attrib data-tab="type" id="type_id" data-key="product_items_id" data-id="product_items" placeholder="' . lang("select") . " " . lang("type") . '" required="required" style="width:100%" ');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="brands_id">Brand</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->brands_id, 'class="form-control " id="brands_id" data-tab="brands" data-key="type_id" data-id="type_id" placeholder="select brands" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="design_id">Design</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->design_id, 'class="form-control " id="design_id"  data-tab="design" data-key="brands_id" data-id="brands_id" placeholder="select design" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="style_id">Style</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->style_id, 'class="form-control" id="style_id"  data-tab="style" data-key="design_id" data-id="design_id" placeholder="select style" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="pattern_id">Pattern</label>
                        <div class="controls"> 
                            <?php echo form_input('', $d->pattern_id, 'class="form-control" id="pattern_id"  data-tab="pattern" data-key="style_id" data-id="style_id" placeholder="select pattern" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="fitting_id">Fitting</label>
                        <div class="controls"> 
                            <?php echo form_input('fitting_id', $d->fitting_id, 'class="form-control product_para_' . $tab . '" id="fitting_id"  data-tab="fitting" data-key="pattern_id" data-id="pattern_id" placeholder="select fitting" data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($tab == "color") { ?>
                    <div class="form-group">
                        <label class="control-label" for="code">Code</label>
                        <div class="controls"> 
                            <?php echo form_input('code', $d->code, 'class="form-control tip" id="code" required data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($tab == "size") { ?>
                    <div class="form-group">
                        <label class="control-label" for="code">Code</label>
                        <div class="controls"> 
                            <?php echo form_input('code', $d->code, 'class="form-control tip" id="code" data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="department_id">Department</label>
                        <div class="controls">
                            <?php
                            echo form_input('department_id', $d->department_id, 'class="form-control " default-attrib data-tab="department"  id="department" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%"');
                            ?>
                            <?php // echo form_input('department_id', $d->department_id, 'class="form-control product_para_' . $tab . '"  data-tab="department" id="department_id" placeholder="select department" data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="section_id">Section</label>
                        <div class="controls"> 
                            <?php
                            echo form_input('section_id', $d->section_id, 'class="form-control " default-attrib data-tab="section" id="section" data-key="department_id" data-id="department"  required="required" style="width:100%"');
                            ?>
                            <?php // echo form_input('section_id', $d->section_id, 'class="form-control product_para_' . $tab . '"  data-key="department_id" data-id="department" id="section_id" data-tab="section" placeholder="select section" data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="product_items_id">Products Item</label>
                        <div class="controls">
                            <?php
                            echo form_input('product_items_id', $d->product_items_id, 'class="form-control " default-attrib data-tab="product_items" data-key="section_id" data-id="section" id="product_items" required="required" style="width:100%"');
                            ?>
                            <?php // echo form_input('product_items_id', $d->product_items_id, 'class="form-control product_para_' . $tab . '"   data-key="section_id" data-id="section" id="product_items_id" data-tab="product_items" placeholder="select product items" data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="form-group">
                    <label class="control-label" for="customer_group">Status</label>
                    <div class="controls"> 
                        <?php echo form_dropdown('status', array('Active' => "Active", 'Deactive' => "Deactive"), $d->status, 'class="form-control tip select" id="status" data-bv-notempty="true"'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?php echo form_submit('editPara', "Update", 'class="btn btn-primary"'); ?>
            </div>
        </form>
    </div>

</div>
<script type="text/javascript" src="<?= $assets ?>js/bootstrapValidator.min.js"></script>

<script type="text/javascript">


    $(document).ready(function (e) {
        $('#editParaForm').bootstrapValidator({
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            }, excluded: [':disabled']
        });
        $('select.select').select2({minimumResultsForSearch: 6});
        fields = $('.modal-content').find('.form-control');
        $.each(fields, function () {
            var id = $(this).attr('id');
            var iname = $(this).attr('name');
            var iid = '#' + id;
            if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
                $("label[for='" + id + "']").append(' *');
                $(document).on('change', iid, function () {
                    $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
                });
            }
        });
    });
    /**
     * Comment
     */
    $(document).ready(function () {
        var department = '<?= $tab ?>';
        $("#editParaForm").unbind().submit(function (e) {
            e.preventDefault();
            var form = $("#addParaForm");
            var $data = $(this).serialize();
//            $data += "&name=<?= $this->security->get_csrf_token_name() ?>&value=<?= $this->security->get_csrf_hash() ?>";
            $.ajax({
                url: "<?= site_url('system_settings/EditAjaxProduct_para/' . $tab . "/?v=1") ?>",
                type: "get", async: false,
                data: $data,
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    if (data.s === "true") {
                        $("#myModal").modal('hide');
                        bootbox.alert('Parameter Updated Successfully!');
                        oTable[department].fnDraw();
                        document.getElementById("editParaForm").reset();

                    } else if (data.s == "ex") {
                        bootbox.alert('Parameter Already Exists !');

                    } else {
                        bootbox.alert('Parameter Updating Failed!');
                    }
                }
            });
            return false;
        });

        pp = $(".product_para_<?= $tab ?>");

        pp.select2({
//            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site_url + "products/getIdAttribute",
                    data: {
                        term: pp.val(),
                        tab: pp.data('tab')
                    },
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site_url + "products/getIdAttributes",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        tab: pp.data('tab'),
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'No Match Found'}]};
                    }
                }
            }
        });
        var obj = ['department', 'section', 'product_items', 'type_id', 'brands_id', 'design_id', 'style_id', 'pattern_id', 'fitting_id'];
        var objarr = [];
        var i = 0;
        $.each(obj, function (k, v) {

            objarr[i] = myselect2(v);
            objarr[i]();
            i++;
        });
        function myselect2(id) {
            var pp = $("#" + id);
            function get() {
                pp.select2({
//                    minimumInputLength: 1,
                    data: [],
                    initSelection: function (element, callback) {
                        $.ajax({
                            type: "get", async: false,
                            url: site_url + "products/getIdAttribute",
                            data: {
                                term: pp.val(),
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                            },
                            dataType: "json",
                            success: function (data) {

                                callback(data[0]);
                            }
                        });
                    },
                    ajax: {
                        url: site_url + "products/getIdAttributes",
                        dataType: 'json',
                        quietMillis: 15,
                        data: function (term, page) {
                            return {
                                term: term,
                                tab: pp.data('tab'),
                                id: $("#" + pp.data('id')).val(),
                                key: pp.data('key'),
                                limit: 10
                            };
                        },
                        results: function (data, page) {

                            if (data.results != null) {
                                return {results: data.results};
                            } else {
                                return {results: [{id: '', text: 'No Match Found'}]};
                            }
                        }
                    }
                });
            }
            return get;
        }
    });
</script>
