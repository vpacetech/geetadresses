<script>

    var oTable = ['offers'];
    $(document).ready(function () {
        oTable['offers'] = $('#CGData').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('system_settings/get_offers') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [{
                    "bSortable": false, "mRender": checkbox}, null, null, null, null, null, {"mRender": setStatus}, {"bSortable": false}],
            "fnCreatedRow": function (nRow, aData, iDataIndex) {
                $('td:eq(6) > label', nRow).attr('onclick', "changeStatus('id'," + aData[0] + ",'offers')");
            },
        }).dtFilter([
            {column_number: 1, filter_default_label: "[<?= lang('offer_type'); ?>]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?= lang('offer_name'); ?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?= lang('offer_preference'); ?>]", filter_type: "text", data: []},
            {column_number: 4, filter_default_label: "[<?= lang('offer_start_date'); ?>]", filter_type: "text", data: []},
            {column_number: 5, filter_default_label: "[<?= lang('offer_end_date'); ?>]", filter_type: "text", data: []},
            {column_number: 6, filter_default_label: "[<?= lang('status'); ?>]", filter_type: "text", data: []},
        ], "footer");
    });

</script>
<?= form_open('system_settings/offer_actions', 'id="action-form"') ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-building"></i><?= $page_title ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip"
                                                                                  data-placement="left"
                                                                                  title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right" class="tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li><a href="<?php echo site_url('system_settings/addoffer'); ?>" ><i class="fa fa-plus"></i> <?= lang('add_offer') ?></a>
                        </li>
                        <li><a href="#" id="excel" data-action="export_excel"><i
                                    class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?></a></li>
                        <li><a href="#" id="pdf" data-action="export_pdf"><i
                                    class="fa fa-file-pdf-o"></i> <?= lang('export_to_pdf') ?></a></li>
                        <li class="divider"></li>
                        <li><a href="#" id="delete" data-action="delete"><i
                                    class="fa fa-trash-o"></i> <?= lang('delete_offer') ?></a></li>

                        
<!--                        <li><a href="#" id="delete" data-action="delete"><i
                                    class="fa fa-trash-o"></i> <?= lang('delete_offer') ?></a></li>-->
<!--                        <li><a href="#" class="bpo" title="<b><?= $this->lang->line("delete_offer") ?></b>"
                               data-content="<p><?= lang('r_u_sure') ?></p><button type='button' class='btn btn-danger' id='delete' data-action='delete'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button>" data-html="true" data-placement="left"><i class="fa fa-trash-o"></i> <?= lang('delete_offer') ?></a></li>-->
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?php echo $this->lang->line("list_results"); ?></p>

                <div class="table-responsive">
                    <table id="CGData" class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th style="min-width:30px; width: 30px; text-align: center;">
                                    <input class="checkbox checkth" type="checkbox" name="check"/>
                                </th>
                                <th><?php echo $this->lang->line("offer_type"); ?></th>
                                <th><?php echo $this->lang->line("offer_name"); ?></th>
                                <th><?php echo $this->lang->line("offer_preference"); ?></th>
                                <th><?php echo $this->lang->line("offer_start_date"); ?></th>
                                <th><?php echo $this->lang->line("offer_end_date"); ?></th>
                                <th><?php echo $this->lang->line("status"); ?></th>
                                <th style="width:65px;"><?php echo $this->lang->line("actions"); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="8" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                            </tr>

                        </tbody>
                        <tfoot class="dtFilter">
                            <tr class="active">
                                <th style="min-width:30px; width: 30px; text-align: center;">
                                    <input class="checkbox checkft" type="checkbox" name="check"/>
                                </th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th style="width:85px;" class="text-center"><?= lang("actions"); ?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>

        </div>
    </div>
</div>

<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>
<?= form_close() ?>
<script language="javascript">
    $(document).ready(function () {

        $('#delete').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#excel').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#pdf').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

    });
</script>

<script>
deleteParcelSent = function (tab, id) {
       var table_design_display = 'offers';
        bootbox.confirm("Are you sure to delete?", function (r) {
            if (r == true) {
                $.ajax({
                    url: "<?= site_url() ?>" + "System_setting/deleteParcelSent/" + tab + "/" + id,
                    type: 'GET',
                    data: {},
                    dataType: 'json',
                    success: function (data, textStatus, jqXHR) {
                        if (data.s == "true") {
                            bootbox.alert('Parcel Sent Voucher Deleted Successfully!');
                            oTable[table_design_display].fnDraw();
                        } else {
                            bootbox.alert('Deleting Failed!');
                        }
                    }
                });
            }
        });
    };

</script>