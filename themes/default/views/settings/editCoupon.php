<style>
    .close{
        font-size: 19px !important;
    }
    .heading{
        margin-top:14px !important;
        border-top: 1px solid black;
    }
    .heading1{
        padding-top: 7px;
    }
    .modal-header{
        border-bottom: none !important;
    }
</style>
<?php echo $modal_js ?>
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
        </div>
        <?php
        $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'addFormx');
        echo form_open_multipart("system_settings/editCoupon/" . $coupon->id, $attrib);
        ?>
        <div class="modal-body">
            <div >
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                    <h2>
                        <?= lang('edit_coupon') ?>
                    </h2>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                    <?= lang('store', 'store') ?>
                </div>
                <div class="col-lg-9 col-sm-9 col-md-9 col-xs-12">
                    <?php
                    $soters = array();
                    $soters[''] = "Select Store";
                    foreach ($store as $row) {
                        $soters[$row->id] = $row->name;
                    }
                    echo form_dropdown('store', $soters, $coupon->store, 'class="form-control select2 commonselect" data-bv-notempty="true" data-tab="department" data-key="store_id" data-id="department" id="store" placeholder="' . lang("select") . ' ' . lang("store") . '"')
                    ?>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                    <?= lang('Department', 'department') ?>
                </div>
                <div class="col-lg-9 col-sm-9 col-md-9 col-xs-12">
                    <?php
                    $soters = array();
                    $soters[''] = "Select Department";
                    foreach ($department as $row) {
                        $soters[$row->id] = $row->name;
                    }
                    echo form_dropdown('department', $soters, $coupon->department, 'class="form-control select2 commonselect" data-bv-notempty="true" data-tab="product_items" data-key="section_id" data-id="product_item" id="department" placeholder="' . lang("select") . ' ' . lang("department") . '"')
                    ?>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                    <?= lang('product_item', 'product_item') ?>
                </div>
                <div class="col-lg-9 col-sm-9 col-md-9 col-xs-12">
                    <?php
                    $soters = array();
                    $soters[''] = "Select Product Item";
                    foreach ($pro_item as $row) {
                        $soters[$row->id] = $row->name;
                    }
                    echo form_dropdown('product_item', $soters, $coupon->product_item, 'class="form-control select2 commonselect" data-bv-notempty="true" data-tab="type" data-key="product_items_id" data-id="type" id="product_item" placeholder="' . lang("select") . ' ' . lang("product_item") . '"')
                    ?>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                    <?= lang('Type', 'type') ?>
                </div>
                <div class="col-lg-9 col-sm-9 col-md-9 col-xs-12">
                    <?php
                    $soters = array();
                    $soters[''] = "Select Type";
                    foreach ($type as $row) {
                        $soters[$row->id] = $row->name;
                    }
                    echo form_dropdown('type', $soters, $coupon->type, 'class="form-control select2 commonselect" data-bv-notempty="true" data-tab="brands" data-key="type_id" data-id="brand" id="type" placeholder="' . lang("select") . ' ' . lang("type") . '"')
                    ?>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                    <?= lang('Brand', 'brand') ?>
                </div>
                <div class="col-lg-9 col-sm-9 col-md-9 col-xs-12">
                    <?php
                    $soters = array();
                    $soters[''] = "Select Brand";
                    foreach ($brand as $row) {
                        $soters[$row->id] = $row->name;
                    }
                    echo form_dropdown('brand', $soters, $coupon->brand, 'class="form-control select2" data-bv-notempty="true" id="brand" placeholder="' . lang("select") . ' ' . lang("brand") . '"')
                    ?>
                </div>
            </div>


            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                    <?= lang('coupon_name', 'coupon_name') ?>
                </div>
                <div class="col-lg-9 col-sm-9 col-md-9 col-xs-12">
                    <?php echo form_input('coupon_name', set_value('coupon_name', $coupon->name), 'class="form-control tip" id="coupon_name" data-bv-notempty="true"'); ?>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                    <?= lang('offers', 'offers') ?>
                </div>
                <div class="col-lg-9 col-sm-9 col-md-9 col-xs-12">
                    <?php
                    $offer = array();
                    $offer[''] = "Select Offer";
                    foreach ($offers as $row) {
                        $offer[$row->id] = $row->offer_name;
                    }
                    echo form_dropdown('offers', $offer, $coupon->offer, 'class="form-control select2" data-bv-notempty="true" id="offers" placeholder="' . lang("select") . ' ' . lang("offers") . '"')
                    ?>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" >
                    <?= lang('start_date', 'start_dates') ?> 
                    <?php echo form_input('start_date', $this->sma->hrsd($coupon->start_date), 'class="form-control tip" id="start_date" readonly data-bv-notempty="false"'); ?>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" >
                    <?= lang('end_date', 'end_dates') ?>
                    <?php echo form_input('end_date', $this->sma->hrsd($coupon->end_date), 'class="form-control tip" id="end_date" readonly data-bv-notempty="false"'); ?>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                    <?= lang('offer_description', 'offer_description') ?>
                </div>
                <div class="col-lg-9 col-sm-9 col-md-9 col-xs-12">
                    <?php echo form_textarea('offer_description', set_value('offer_description', strip_tags($coupon->description)), 'class="form-control tip"  id="offer_description" data-bv-notempty="true"  style="margin-top: 10px; height: 100px;"'); ?>
                </div>
            </div> 
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border"><?= lang('coupon_usages') ?></legend>
                    <div class="form-group">
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <div class="radio">
                                <label><input type="radio" name="coupon_usages"  id="use_unlimited_no_of_times" <?= $coupon->coupon_usages == "Use Unlimited Number of Times" ? "checked" : "" ?> value="<?= lang('use_unlimited_no_of_times') ?>"> <?= lang('use_unlimited_no_of_times') ?></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <div class="radio">
                            <label><input type="radio"  name="coupon_usages" id="use_fixed_no_of_times"  <?= $coupon->coupon_usages == "Use Fixed Number of Times" ? "checked" : "" ?> value="<?= lang('use_fixed_no_of_times') ?>"> <?= lang('use_fixed_no_of_times') ?></label> 
                        </div>
                    </div> 
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <?php
                        $dis = "";
                        if ($coupon->coupon_usages == "Use Unlimited Number of Times") {
                            $dis = "disabled";
                        }
                        ?>
                        <?php echo form_input('times_usage', set_value('times_usage', $coupon->use_fixed_times), 'class="form-control tip" id="times_usages" required="required"  data-bv-notempty="true" ' . $dis . '
                            data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Only Numbers Allowed" 
                               data-bv-stringlength="true"'); ?>
                    </div>
                </fieldset>
            </div> 
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border"><?= lang('coupon_generater') ?></legend>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="radio">
                            <label><input type="radio" name="coupon_generater"  id="specify_no_of_coupons" <?= $coupon->generat_coupon == "Specify Number of Coupons" ? "checked" : "" ?> value="<?= lang('specify_no_of_coupons') ?>" <?= $offer->applicable_cust_type == "Applicable Only For Seleted Groups" ? "checked" : "" ?> > <?= lang('specify_no_of_coupons') ?></label>
                        </div>
                    </div> 
                    <div class="col-lg-1 col-md-1 col-sm-1">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <?php // echo lang('no_of_coupons', 'no_of_coupons', 'style="font-weight: 0 !important;"')   ?>
                        <span for="no_of_coupons">Number of Coupons * </span>
                    </div> 
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <?php echo form_input('no_of_coupons', set_value('no_of_coupons', $coupon->number_of_coupon), 'class="form-control tip"  id="no_of_coupons" required="required"  data-bv-notempty="true"  
                            data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Only Numbers Allowed"
                               data-bv-stringlength="true"'); ?>
                    </div>
                    <div class="form-group">
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                            <div class="radio">
                                <label><input type="radio" name="coupon_generater" id="customer_specific_coupons" <?= $coupon->generat_coupon == "Customer Specify Coupons" ? "checked" : "" ?> value="Customer Specify Coupons"> <?= lang('customer_specific_coupons') ?></label>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div> 

            <!--            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                            <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
            <?= lang('end_date', 'end_date') ?>s
                            </div>
                            <div class="col-lg-9 col-sm-9 col-md-9 col-xs-12">
                               
            <?php echo form_input('end_date', "", 'class="form-control tip" id="end_date" data-bv-notempty="true"  readonly'); ?>
                            </div>
                        </div>-->


        </div>
        <div class="modal-footer" style="clear:both;text-align: left;">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                <div class="col-md-4 col-md-offset-5"><?php echo form_submit('update_coupon', $this->lang->line("submit"), 'id="add_transfer" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>

                </div>
            </div>
        </div>




        <?php echo form_close(); ?>

        <!---->
    </div><!--end body div-->

    <!--end foter div-->

</div>

<script type="text/javascript" src="<?= $assets ?>js/common.js"></script>
<script type="text/javascript">
    $('#offers').change(function () {
        var v = $(this).val();
        $.ajax({
            type: "get",
            async: false,
            url: "<?= site_url('System_settings/get_offer_byId') ?>/" + v,
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $("#start_date").val(data.start_date);
                    $("#end_date").val(data.end_date);
                }
            },
            error: function () {
                bootbox.alert('<?= lang('ajax_error') ?>');
                $('#modal-loading').hide();
            }
        });
    });

    $(document).on('ifChecked', '#use_fixed_no_of_times', function (e) {
        $('#times_usages').prop("disabled", "");
    });

    $(document).on('ifUnchecked', '#use_fixed_no_of_times', function (e) {
        $('#times_usages').prop('disabled', "disabled");
    });

    $(document).on('ifChecked', '#specify_no_of_coupons', function (e) {
        $('#no_of_coupons').prop("disabled", "");
    });

    $(document).on('ifUnchecked', '#specify_no_of_coupons', function (e) {
        $('#no_of_coupons').prop('disabled', "disabled");
    });


</script>
