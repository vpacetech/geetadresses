
<style>
    .btton
    {
        padding: 5px;
        width:100%;
    }
    .form-group {
        margin-bottom: 6px !important;
    }

</style>

<div class="container-fluid" style="background-color: #ffff;padding: 10px">
    <?php
    $attrib = array('data-toggle' => 'validator', 'class' => 'form-horizontal', 'role' => 'form');
    echo form_open_multipart("system_settings/editOffer/" . $offer->id, $attrib);
    ?>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-3 col-xs-12"><?= lang('offer_type', 'offer_type') ?></label>
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                    <?php
                    $offer_type[''] = "Select Offer Type";
                    $offer_type['Discount On Invoice Amount'] = "Discount On Invoice Amount";
                    $offer_type['Discount On Attribute'] = "Discount On Attribute";
                    $offer_type['Discount On Other'] = "Discount On Other";
                    echo form_dropdown('offer_type', $offer_type, $offer->offertype, 'class="form-control select" id="offer_type" placeholder="' . lang("select") . " " . lang("offer_type") . '"')
                    ?>
                </div>

            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-3 col-xs-12"><?= lang('offer_name', 'offer_name') ?></label>
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                    <?php echo form_input('offer_name', set_value('offer_name', htmlspecialchars($offer->offer_name)), 'class="form-control tip" id="offer_name" data-bv-notempty="true"'); ?>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                    <div class="checkbox">
                        <label><input type="checkbox" name="active" id="enrolllimit" <?= $offer->status = "Active" ? "checked" : "" ?>  value="1"><?= lang('active') ?></label>
                        <label><input type="checkbox" name="apllicable_for_coupons" id="apllicable_for_coupons" <?= $offer->if_coupon_applocable == "True" ? "checked" : "" ?> value="True"><?= lang('apllicable_for_coupons') ?></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-3 col-xs-12"><?= lang('offer_preference', 'offer_preference') ?></label>
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                    <?php echo form_input('offer_preference', set_value('offer_preference', $offer->preference), 'class="form-control tip" id="offer_preference" required="required"  data-bv-notempty="true" 
                            data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Only Numbers Allowed" 
                               data-bv-stringlength="true"'); ?>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">

                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">

                        <label class="control-label col-lg-6 col-md-6 col-sm-6 col-xs-12"><?= lang('offer_start_date', 'offer_start_date') ?></label>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <?php echo form_input('offer_start_date', set_value('offer_start_date', $this->sma->hrld($offer->start_date)), 'class="form-control input-tip datetime" id="offer_start_date" data-bv-notempty="true"'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-lg-4 col-md-4 col-sm-4 col-xs-12"><?= lang('offer_end_date', 'offer_end_date') ?></label>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <?php echo form_input('offer_end_date', set_value('offer_end_date', $this->sma->hrld($offer->end_date)), 'class="form-control input-tip datetime" id="offer_end_date" data-bv-notempty="true"'); ?>
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">

                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <div class="checkbox">
                        <label><input type="checkbox" name="isrepeated" id="isrepeated" <?= $offer->is_repeat == "True" ? "checked" : "" ?> value="True"><?= lang('isrepeated') ?></label>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border"><?= lang('repeatdays') ?></legend>
                        <div class="form-group">
                            <?php
                            $days = json_decode($offer->repeated_days, TRUE);
                            ?>  
                            <div class="checkbox">
                                <label><input type="checkbox" name="sunday" id="sunday" <?= $days['sunday'] == "1" ? "checked" : "" ?>  value="1"><?= lang('sunday') ?></label>
                                <label><input type="checkbox" name="monday" id="monday" <?= $days['monday'] == "1" ? "checked" : "" ?> value="1"><?= lang('monday') ?></label>
                                <label><input type="checkbox" name="tuesday" id="tuesday" <?= $days['tuesday'] == "1" ? "checked" : "" ?> value="1"><?= lang('tuesday') ?></label>
                                <label><input type="checkbox" name="wendesday" id="wendesday" <?= $days['wendesday'] == "1" ? "checked" : "" ?> value="1"><?= lang('wendesday') ?></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label><input type="checkbox" name="thurday" id="thurday" <?= $days['thurday'] == "1" ? "checked" : "" ?> value="1"><?= lang('thurday') ?></label>
                                <label><input type="checkbox" name="friday" id="friday" <?= $days['friday'] == "1" ? "checked" : "" ?> value="1"><?= lang('friday') ?></label>
                                <label><input type="checkbox" name="saturday" id="saturday" <?= $days['saturday'] == "1" ? "checked" : "" ?> value="1"><?= lang('saturday') ?></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3 col-md-3 col-sm-3 col-xs-12"><?= lang('start_time', 'start_time') ?></label>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <?php echo form_input('start_time', set_value('start_time', $offer->start_time), 'class="form-control input-tip time" id="start_time" data-masked-input="99:99" maxlength="5" placeholder="HH:SS" data-bv-notempty="true"'); ?>
                            </div>
                            <label class="control-label col-lg-3 col-md-3 col-sm-3 col-xs-12"><?= lang('end_time', 'end_time') ?></label>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <?php echo form_input('end_time', set_value('end_time', $offer->end_time), 'class="form-control input-tip time" id="end_time" data-masked-input="99:99" maxlength="5" placeholder="HH:SS" data-bv-notempty="true"'); ?>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                </div>


            </div>
            <div class="form-group">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border"><?= lang('select_store') ?></legend>
                        <div class="form-group">
                            <div class="radio">
                                <label><input type="radio" name="select_store" id="applicable_to_all_stores" <?= $offer->applicable_store_type == 'Applicable To All Stores' ? "checked" : "" ?> value="<?= lang('applicable_to_all_stores') ?>"> <?= lang('applicable_to_all_stores') ?></label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="radio">
                                <label><input type="radio" name="select_store" id="applicable_seleted_stores" <?= $offer->applicable_store_type == 'Applicable Only For Seleted Stores' ? "checked" : "" ?> value="<?= lang('applicable_seleted_stores') ?>"> <?= lang('applicable_seleted_stores') ?></label>
                            </div>
                        </div>

                        <div class="form-group">
                            <?php
                            $offersoter = json_decode($offer->sotres);
                            ?>

                            <select name='store[]' class="form-control select2" id="store" multiple id="created" placeholder="<?= lang('select_store') ?>" <?= $offer->applicable_store_type == 'Applicable To All Stores' ? "disabled" : "" ?> >
                                <?php foreach ($store as $usr) { ?>
                                    <option value="<?= $usr->id ?>" 
                                    <?php
                                    foreach ($offersoter as $row) {
                                        if ($usr->id == $row) {
                                            echo 'selected';
                                        }
                                    }
                                    ?>
                                            ><?= $usr->name ?></option>
                                        <?php } ?>
                            </select>
                        </div>
                    </fieldset>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border"><?= lang('select_customer_groups') ?></legend>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <div class="form-group">
                                <div class="radio">
                                    <label><input type="radio" name="select_customer_groups" id="applicable_To_All_customer_groups" value="<?= lang('applicable_To_All_customer_groups') ?>" <?= $offer->applicable_cust_type == "Applicable To All Customer Groups" ? "checked" : "" ?> > <?= lang('applicable_To_All_customer_groups') ?> </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="radio">
                                    <label><input type="radio" name="select_customer_groups" id="applicable_seleted_groups" value="<?= lang('applicable_seleted_groups') ?>" <?= $offer->applicable_cust_type == "Applicable Only For Seleted Groups" ? "checked" : "" ?>> <?= lang('applicable_seleted_groups') ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <!--<button class="btn btn-default">Customer Groups </button>-->
                        </div>
                        <div class="form-group">
                            <?php $custGroups = json_decode($offer->cust_groups); ?>

                            <select name='customer_groups[]' class="form-control select2" id="customer_groups" multiple id="customer_groups" placeholder="<?= lang('select') . ' ' . lang('customer_groups') ?>" <?= $offer->applicable_cust_type == "Applicable To All Customer Groups" ? "disabled" : "" ?> >
                                <?php foreach ($custgroup as $row) { ?>
                                    <option value="<?= $row->id ?>" 
                                    <?php
                                    foreach ($custGroups as $rowsel) {
                                        if ($row->id == $rowsel) {
                                            echo 'selected';
                                        }
                                    }
                                    ?> ><?= $row->name ?></option>
                                        <?php } ?>
                            </select>
                        </div>
                    </fieldset>
                </div>
            </div>

          
            <!--            <div class="form-group">
                            <label class="control-label col-lg-2 col-md-2 col-sm-2 col-xs-12"><?= lang('offer_disc_category', 'offer_disc_category') ?></label>
                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
            <?php $categorys = json_decode($offer->categorys); ?>
            
                                <select name='offer_disc_category[]' class="form-control select2" id="store" multiple id="categorys" placeholder="<?= lang('offer_disc_category') ?>">
            <?php foreach ($department as $row) { ?>
                                                    <option value="<?= $row->id ?>" 
                <?php
                foreach ($categorys as $rowsel) {
                    if ($row->id == $rowsel) {
                        echo 'selected';
                    }
                }
                ?> ><?= $row->name ?></option>
            <?php } ?>
                                </select>
                            </div>
                        </div>-->
                
            <fieldset class="scheduler-border dis_attributes">
                <div class="col-md-12">
                    <div class="col-md-5">
                        <div class="form-group all">
                            <?= lang("Department", "department"); ?>
                            <?php
                            foreach ($department as $row) {
                                $departments[''] = 'Select Department';
                                $departments[$row->id] = $row->name;
                            }
                            echo form_dropdown('department', $departments, (isset($_POST['department']) ? $_POST['department'] : ($offer ? $offer->department : '')), 'class="form-control"   id="department" placeholder="' . lang("select") . " " . lang("department") . '" required="required" style="width:100%"');
                            ?>
                        </div>
                        <div class="form-group all">
                            <?= lang("Section", "section") ?> <label>*</label>
                            <?php echo form_input('section', (isset($_POST['section']) ? $_POST['section'] : ($offer ? $offer->section : '')), 'class="form-control" default-attrib data-tab="section" id="section" data-key="department_id" data-id="department" placeholder="' . lang("select") . " " . lang("section") . '" '); ?>
                        </div>
                        <div class="form-group all">
                            <?= lang("Product_items", "product_items") ?>
                            <?php echo form_input('product_items', (isset($_POST['product_items']) ? $_POST['product_items'] : ($offer ? $offer->product_items : '')), 'class="form-control" default-attrib data-tab="product_items" data-key="section_id" data-id="section" id="product_items" placeholder="' . lang("select") . " " . lang("product_items") . '" required="required" style="width:100%"'); ?>
                        </div>
                    </div>
                    <div class="col-md-6 col-md-offset-1">
                        <div class="form-group all">
                            <?= lang("Type", "type") ?> <label>*</label>
                            <?php echo form_input('type_id',  ($offer ? $offer->type_id : ''), 'class="form-control" default-attrib data-tab="type" id="type_id" data-key="product_items_id" data-id="product_items" placeholder="' . lang("select") . " " . lang("type") . '" required="required" style="width:100%" '); ?>
                        </div>
                        <div class="form-group all">
                            <?= lang("Brands", "brands") ?>
                            <?php echo form_input('brands', (isset($_POST['brands']) ? $_POST['brands'] : ($offer ? $offer->brands : '')), 'class="form-control" default-attrib data-tab="brands" id="brands" data-key="type_id" data-id="type_id" placeholder="' . lang("select") . " " . lang("brands") . '" required="required"'); ?>
                        </div>
                    </div>
                </div>
            </fieldset>

            <fieldset class="scheduler-border dis_invoice">
                <div id="discount_level">
                    <div class="form-group">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">Sr. No.</div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">Invoice Amount</div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">Discount Amount</div>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">is Percent</div>
                    </div>
                    <hr>
                    <?php
                    $alldisc_data = array();
                    $discount_level = json_decode($offer->discount_level);
                    $invoice_amount = json_decode($discount_level->invoice_amount);
                    $discAmount = json_decode($discount_level->discAmount);
                    $ispercent = json_decode($discount_level->ispercent);
                    $col_id = 0;
                    foreach ($invoice_amount as $row) {
                        $alldisc_data[] = array(
                            'invAmount' => $invoice_amount[$col_id],
                            'discAmount' => $discAmount[$col_id],
                            'ispercent' => $ispercent[$col_id],
                        );
                        $col_id++;
                    }
                    if ($alldisc_data . count() == 0) {
                        $alldisc_data[] = array(
                            'invAmount' => '',
                            'discAmount' => '',
                            'ispercent' => '',
                        );
                    }
                    ?>

                    <div ng-controller="offers" ng-init='invoice_item = (<?= json_encode($alldisc_data) ?>)'>
                        <div class="form-group"  ng-repeat="ci in invoice_item">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">{{$index + 1}}</div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><input type="text" ng-model="ci.invAmount" name="invAmount[]" class="form-control input-tip time" id="invAmount" data-bv-notempty="false" data-bv-regexp="true" data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Only Numbers Allowed" ></div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><input type="text" ng-model="ci.discAmount" name="discAmount[]" class="form-control input-tip time" id="discAmount" data-bv-notempty="false" data-bv-regexp="true" data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Only Numbers Allowed" ></div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <div class=" col-lg-3 col-md-3 col-sm-3 col-xs-12" style="padding-top: 1px !important;">
                                    <input type="checkbox" class='iCheck-helper' name="ispercent[]" ng-model="ci.ispercent">
                                    <md-checkbox ng-model="data.cb1" aria-label="Checkbox 1">
                                        <!--           {{ ci.invAmount }}-->
                                    </md-checkbox>
                                </div>
                                <!--<i class="fa fa-2x fa-plus-circle col-lg-6 col-md-6 col-sm-6 col-xs-12 adddiscountLevel"  onclick="adddiscountLevel()"></i>-->
                                <a><i class="fa fa-2x fa-minus-circle adddiscountLevel" ng-if="$index < invoice_item.length - 1" ng-click="invoice_item.splice($index, 1)"></i></a>
                                <a><i class="fa fa-2x fa-plus-circle adddiscountLevel" ng-if="$index == invoice_item.length - 1" ng-click="invoice_item.push({invamount: 0, disc: 0})"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>

    </div>
    <div class="row text-center">
        <?php echo form_submit('add_offer', lang('save'), 'class="btn btn-primary"'); ?>
        <!--<button type="button" class="btn btn-danger"  onclick="this.form.reset();">Cancel</button>-->  	
    </div>
    <?php echo form_close(); ?>
</div><!--end container fluid----->
<script>

    $(document).on('ifUnchecked', '#isrepeated', function (e) {
        $('#sunday').attr("disabled", 'disabled');
        $('#monday').attr("disabled", 'disabled');
        $('#tuesday').attr("disabled", 'disabled');
        $('#wendesday').attr("disabled", 'disabled');
        $('#thurday').attr("disabled", 'disabled');
        $('#friday').attr("disabled", 'disabled');
        $('#saturday').attr("disabled", 'disabled');
        $('#start_time').attr("disabled", 'disabled');
        $('#end_time').attr("disabled", 'disabled');
    });
    $(document).on('ifChecked', '#isrepeated', function (e) {
        $('#sunday').removeAttr("disabled");
        $('#monday').removeAttr("disabled");
        $('#tuesday').removeAttr("disabled");
        $('#wendesday').removeAttr("disabled");
        $('#thurday').removeAttr("disabled");
        $('#friday').removeAttr("disabled");
        $('#saturday').removeAttr("disabled");
        $('#start_time').removeAttr("disabled");
        $('#end_time').removeAttr("disabled");
    });
   

    $(document).ready(function () {
        $('.dis_invoice').slideUp();
        $('.dis_attributes').slideUp();
        $('#offer_type').trigger('change');
        
    });
    $(document).on('change', '#offer_type', function (e) {
        var offertype = $(this).val();
        if (offertype == 'Discount On Invoice Amount') {
            $('.dis_attributes').slideUp();
            $('.dis_invoice').slideDown();
        } else if (offertype == 'Discount On Attribute') {
            $('.dis_attributes').slideDown();
            $('.dis_invoice').slideUp();
        } else {
            $('.dis_invoice').slideUp();
            $('.dis_attributes').slideUp();
        }
    });

    $(document).on('ifUnchecked', '#applicable_to_all_stores', function (e) {
        $('#store').removeAttr("disabled");
    });
    $(document).on('ifChecked', '#applicable_to_all_stores', function (e) {
        $('#store').attr('disabled', "disabled");
    });
    $(document).on('ifUnchecked', '#applicable_To_All_customer_groups', function (e) {
        $('#customer_groups').removeAttr("disabled");
    });
    $(document).on('ifChecked', '#applicable_To_All_customer_groups', function (e) {
        $('#customer_groups').attr('disabled', "disabled");
    });
    function enableEdit() {
        $('#customercode').removeAttr('readonly');
    }

    var i = 1;
    function adddiscountLevel() {
        data = '<div class="form-group">' +
                '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">' + ++i + '</div>' +
                '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><input type="text" name="invAmount[]" class="form-control input-tip time" id="invAmount" data-bv-notempty="true" data-bv-regexp="true" data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Only Numbers Allowed" ></div>' +
                '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><input type="text" name="discAmount[]" class="form-control input-tip time" id="discAmount" data-bv-notempty="true" data-bv-regexp="true" data-bv-regexp-regexp="^[0-9]+$" data-bv-regexp-message="Only Numbers Allowed" ></div>' +
                '<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">' +
                '<div class="checkbox col-lg-3 col-md-3 col-sm-3 col-xs-12"><label><input type="checkbox" name="ispercent[]" id="ispercent" value="1"></label>' +
                '</div><i class="fa fa-2x fa-plus-circle col-lg-6 col-md-6 col-sm-6 col-xs-12 adddiscountLevel" onclick="adddiscountLevel()"></i>' +
                '</div>' +
                '</div>';
        $('#discount_level').append(data);
    }

    $("#autogenerate").click(function () {
        $.ajax({
            type: "get",
            async: false,
            url: "<?= site_url('suppliers/getOfferByID') ?>",
            dataType: "json",
            success: function (data) {
                if (data != null) {
                    $('#loyalityno').val('GC00' + data.id + '@10');
                }
            },
        });
    });

</script>

