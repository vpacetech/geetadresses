<style>
    .close{
        font-size: 19px !important;
    }
    .heading{
        margin-top:14px !important;
        border-top: 1px solid black;
    }
    .heading1{
        padding-top: 7px;
    }
    .modal-header{
        border-bottom: none !important;
    }
</style>
<?php echo $modal_js ?>
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <div class="text-center">
                <h2><?= lang('edit_position') ?></h2>
            </div>
        </div>

        <?php
        $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'addFormx');
        echo form_open_multipart("system_settings/editPosition/" . $data->id, $attrib);
        ?>
        <div class="modal-body">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group" >
                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                    <?= lang('position', 'positionname') ?>
                </div>
                <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                    <?php echo form_input('positionname', $data->name, 'class="form-control" id="positionname" data-bv-notempty="true"'); ?>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                    <?= lang('incentiverate', 'incentiverate') ?>
                </div>
                <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                    <?php echo form_input('incentiverate', $data->rate, 'class="form-control" id="incentiverate" data-bv-notempty="true" data-bv-regexp="true"
                               data-bv-regexp-regexp="^[0-9]\d*(\.\d+)?$" data-bv-regexp-message="Only Numbers Allowed"'); ?>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                    <?= lang('incentivetype', 'incentivetype') ?>
                </div>
                <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                    <?php $type = array('Percentage' => 'Percentage', 'Fixed' => 'Fixed') ?>
                    <?php echo form_dropdown('incentivetype', $type, $data->incentivetype, 'class="form-control" id="incentivetype" data-bv-notempty="true"'); ?>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                    <?= lang('user_group', 'usergroup') ?>
                </div>
                <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                    <?php
                    $ug = array();
                    $ug[""] = "";
                    foreach ($usergroup as $row) {
                        $ug[$row->id] = $row->name;
                    }
                    ?>
                    <?php echo form_dropdown('usergroup', $ug, $data->usergroup, 'class="form-control" id="usergroup" data-bv-notempty="true"'); ?>
                </div>
            </div>

        </div>
        <div class="modal-footer" style="clear:both;text-align: left;">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 ">
                <div class="col-md-4 col-md-offset-5">
                    <?php echo form_submit('update_position', $this->lang->line("submit"), 'id="add_transfer" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>

                </div>
            </div>
        </div>

        <?php echo form_close(); ?>

    </div><!--end body div-->

    <!--end foter div-->

</div>