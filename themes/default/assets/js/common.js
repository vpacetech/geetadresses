
$('.commonselect').change(function () {

    var v = $(this).val();
    var a = $(this).data('id');
    var tables = $(this).data('tab');
    $("#" + a).select2("val", "");
    if (v) {
        $.ajax({
            async: false,
            data: {
                term: v,
                tab: $(this).data('tab'),
                id: $("#" + $(this).data('id')).val(),
                key: $(this).data('key'),
            },
            type: 'GET',
            url: site.base_url + "common/commonDropdownData",
            dataType: "json",
            success: function (scdata) {
                if (scdata) {
                    $('#' + a).html('');
                    $('#' + a).append('<option value="">Please select ' + tables + '</option>');
                    $.each(scdata, function (k, val) {
                        var opt = $('<option />');
                        opt.val(val.id);
                        opt.text(val.name);
                        $('#' + a).append(opt);
                    })
                    $('#modal-loading').hide();
                } else {
                    $('#' + a).html('');
//                    $('#' + a).append('<option value="">Against Order no is not Department</option>');
                }
            },
            error: function () {
                $('#modal-loading').hide();
            }
        });
    }
})
