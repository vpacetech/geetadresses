
app = angular.module('inv', ['ui.select2']);
app.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
    }]);
app.controller('receipt', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
        $scope.design = [{}];
        $scope.designx = {
            allowClear: true,
            ajax: {
                url: site_url + "sales/getDesignCodeA",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'No Match Found'}]};
                    }
                }
            }
        };
        $scope.prodcode = {
            allowClear: true,
            ajax: {
                url: site_url + "products/getAllProdCode",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'No Match Found'}]};
                    }
                }
            }
        };
        $scope.designstatus = {
            allowClear: true
        };
    }]);
app.controller('purchaseCode', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
        $scope.pur = {};

        getDiscountme = function () {
            $scope.pur.supplier_id = $("#supplier_id").val();
            $scope.getDiscount();
        };
        $scope.getDiscount = function () {
            $scope.loading = 1;
            $http.get(site_url + "Purchases/getPurDiscount?v=1&" + $.param($scope.pur)).then(function (d) {
//                alert(JSON.stringify(d));
                $scope.pur.discount = d.data.discount + "%";
                $scope.loading = 0;
            });
        };
    }]);
app.controller('employee', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
        $scope.attachment = [{identityprof: ''}];
        $scope.references = [{referencesname: ''}];


    }]);
app.controller('offers', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
        $scope.invoice_item = [{invamount: 0, disc: 0}];
        //$scope.references = [{referencesname: ''}];


    }]);
app.controller('item_history', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
        $scope.code = 0;
        $scope.history = [];
        $scope.loading = 0;
        $scope.getItemHistory = function () {
            $scope.loading = 1;
            $http.get(site_url + "reports/getItemHistoryReport/?v=1&code=" + $scope.code).success(function (data) {
                $scope.history = data;
                $scope.loading = 0;
            });
        };
    }]);
app.controller('addProducts', ['$scope', '$rootScope', '$http', 'Data', function ($scope, $rootScope, $http, Data) {

        $scope.prod = {barcode: "", colorassorted: [], colorassoarr: [], colortype: "Single", sizeangle: "", sizetype: "Single", singlesize: "", multisizef: "", protype: "standard", qty: 1, colorqty: 0, addupgstmrp: 0, gstno: 0, addupgst: false, chksecqty: false};
        $scope.paraurl = '';
        $scope.loading = 0;
        $scope.setProdName = function (name) {
            $scope.prod.name = name;
        }

        $scope.getProdName = function () {
            $scope.loading = 1;
            Data.getProductName($.param($scope.prod)).then(function (d) {
                if (d.data.name != 'NZ') {
                    $scope.prod.name = d.data.name;
                } else {
                    $scope.prod.name = '.';
                }
                $scope.loading = 0;
            });
        };

        $scope.getGstDetails = function () {
            $scope.loading = 1;
            Data.getGstDetails($.param($scope.prod)).then(function (d) {
                $scope.prod.hsnno = d.data.hsncode ? d.data.hsncode : "";
                $scope.prod.gstno = parseFloat(d.data.cgst) + parseFloat(d.data.sgst) ? parseFloat(d.data.cgst) + parseFloat(d.data.sgst) : 0;
                if ($scope.prod.addupgst && $scope.prod.price > 1000) {
                    $scope.prod.addupgstmrp = parseFloat(d.data.addupgst) ? parseFloat(d.data.addupgst) : 0;
                } else {
                    $scope.prod.addupgstmrp = 0;
                }
                $scope.prod.cess = parseFloat(d.data.cess) ? parseFloat(d.data.cess) : 0;
                $scope.getProductMargin();
            });
            $scope.loading = 0;
        };

        $scope.getProdBarcode = function () {
            $scope.loading = 1;
            Data.getProductBarcode($.param($scope.prod)).then(function (d) {
//                alert(JSON.stringify(d));
                $scope.prod.barcode = d.data.name;
                $scope.loading = 0;
            });
        };
        

        $scope.getQty = function (id) {
            if ($scope.prod.qty !== $scope.prod.colorqty) {
                $(id).css({"border": "1px solid #f00"});
                $(id).siblings('p').removeClass('hide');
            } else {
                $(id).css({"border": "1px solid #CCCCCC"});
                $(id).siblings('p').addClass('hide');
            }
        }
        
        $scope.$watch(function (scope) {
            return scope.prod.colortype;
        }, function (newValue, oldValue) {
            if ($scope.prod.qty != 0) {
                if (newValue == "Single") {
                    $('.colorsingle').removeClass("hide");
                    $('.colorassorted').addClass("hide");
                    $scope.prod.colorassoarr = [];
                    $scope.prod.colorassorted = [];
                    $("#mulcolor").select2("val", "");
                    $scope.prod.colorqty = $scope.prod.qty;
                } else {
                    $('.colorsingle').addClass("hide");
                    $('.colorassorted').removeClass("hide");
                    $scope.prod.colorqty = 0;
                    if ($scope.prod.colorassorted.length != 0) {
                        var x = parseInt($scope.prod.qty) / parseInt($scope.prod.colorassorted.length);
                        $scope.prod.colorassoarr = [];
                        $scope.prod.assortedcolorcode = [];
                        $('.select2-search-choice > div').each(function (i) {

                            $http.get(site_url + "Products/getcolor/?code=" + $(this).html()).success(function (data) {
                                $scope.prod.assortedcolorcode.push({colorassorted: data + x});
                                $scope.prod.colorassoarr[i]["colorcode"] = data + x;
                            });
                        });
                        var colors = $(".multiplevalues").select2("val");
                        angular.forEach(newValue, function (v, k) {
//                            $http.get(site_url + "Products/getcolor/?code=" + colors[k]).success(function (data) {
////                                $scope.prod.assortedcolorcode.push({colorassorted: data + x});
//                                $scope.prod.colorassoarr[k]["colorcode"] = data + x;
//                            });
                            $scope.prod.colorassoarr.push({qty: x});
                        });
                    }
                }
            } else {
                if (newValue != "" || oldValue != "") {
                    bootbox.alert('Please enter Qty first.');
                    $('.colorassorted').addClass("hide");
                }
            }
        });
        $scope.getQtyCal = function (id, qty) {
            var tot = 0;
            angular.forEach($scope.prod.colorassorted, function (v, k) {
                tot += parseInt($scope.prod.colorassoarr[k].qty);
            });

//            if (tot <= $scope.prod.qty) {
            var $qty = 0;
            var j = 0;
            angular.forEach($scope.prod.colorassorted, function (v, k) {
                if (j <= id) {
                    $qty += parseInt($scope.prod.colorassoarr[j].qty);
                }
                j++;
            });
            j--;
            var x = parseInt($scope.prod.qty - $qty) / parseInt((j - id));
            var i = 0;
            angular.forEach($scope.prod.colorassorted, function (v, k) {
                if (i > id) {
                    $scope.prod.colorassoarr[i] = {qty: x};
                }
                i++;
            });

            $scope.prod.assortedcolorcode = [];
            $('.select2-search-choice > div').each(function (i) {

                $http.get(site_url + "Products/getcolor/?code=" + $(this).html()).success(function (data) {
                    $scope.prod.assortedcolorcode.push({colorassorted: data + x});
                    $scope.prod.colorassoarr[i]["colorcode"] = data + x;
                });
            });
            var colors = $(".multiplevalues").select2("val");
            angular.forEach($scope.prod.colorassorted, function (v, k) {

                var x = $scope.prod.colorassoarr[k].qty;
//                setTimeout(function () {
//                $('.select2-search-choice > div').each(function (i) {
//                    alert($(this).html());
//                });
//                $http.get(site_url + "Products/getcolor/?code=" + v).success(function (data) {
////                    $scope.prod.assortedcolorcode.push({colorcode: data + x});
//                    $scope.prod.colorassoarr[k]["colorcode"] = data + x;
//                });
//                }, 800);
            });

//            } else {
//                bootbox.alert('Please ensure that the Product Qty and addion of assorted color qty should be same.');
//            }
        }
        $scope.$watch(function (scope) {
            return scope.prod.qty;
        }, function (newValue, oldValue) {
            if ($scope.prod.colortype == "Single") {
                $scope.prod.colorqty = newValue;
                $scope.prod.colorassorted = [];
            } else {
                if ($scope.prod.colorassorted.length != 0) {
                    var x = parseInt($scope.prod.qty) / parseInt($scope.prod.colorassorted.length);
                    $scope.prod.colorassoarr = [];
                    $scope.prod.assortedcolorcode = [];
                    $('.select2-search-choice > div').each(function (i) {

                        $http.get(site_url + "Products/getcolor/?code=" + $(this).html()).success(function (data) {
                            $scope.prod.assortedcolorcode.push({colorassorted: data + x});
                            $scope.prod.colorassoarr[i]["colorcode"] = data + x;
                        });
                    });
                    var colors = $(".multiplevalues").select2("val");
                    angular.forEach($scope.prod.colorassorted, function (v, k) {
//                        $http.get(site_url + "Products/getcolor/?code=" + colors[k]).success(function (data) {
////                            $scope.prod.assortedcolorcode.push({colorassorted: data + x});
//                            $scope.prod.colorassoarr[k]["colorcode"] = data + x;
//                        });
                        $scope.prod.colorassoarr.push({qty: x});
                    });
                }
            }
        });
        $scope.$watch(function (scope) {
            return scope.prod.colorqty;
        }, function (newValue, oldValue) {
            if ($scope.prod.colortype == "Single") {
                $scope.prod.qty = newValue;
            }
        });

        $scope.$watch(function (scope) {
            return scope.prod.cost;
        }, function (newValue, oldValue) {
            $scope.getGstDetails();
        });
        
        $scope.$watch(function (scope) {
            return scope.prod.chksecqty;
        }, function (newValue, oldValue) {
            if ($scope.prod.chksecqty) {
                Data.getProSecQty($.param($scope.prod)).then(function (d) {
                    var qty = $('#wh_qtys').val();
                    if (d.data != '0') {
                        var sqty = parseInt(qty) / parseInt(d.data.no_of_pic);
                        $('#squantity').val(Number(Math.round(sqty)));
                    } else {
                        $('#squantity').val('');
                    }
                });
            } else {
                $('#squantity').val('');
            }

        });


        $scope.$watch(function (scope) {
            return scope.prod.roundup;
        }, function (newValue, oldValue) {
            if ($scope.prod.roundup) {
                $scope.prod.price = Math.floor($scope.prod.price);
                var rem = $scope.prod.price % 5;
                if (rem > 0) {
                    $scope.prod.price = $scope.prod.price - rem + 5;
                } 
                $scope.prod.singlerate = $scope.prod.price;
            } else {
                $scope.getProductMargin();
            }
        });

//        $scope.$watch(function (scope) {
//            return scope.prod.roundup;
//        }, function (newValue, oldValue) {
//            if ($scope.prod.roundup) {
//                var rem = $scope.prod.price % 10;
//                if (rem < 5) {
//                    $scope.prod.price = $scope.prod.price - rem;
//
//                } else {
//                    $scope.prod.price = $scope.prod.price - rem + 10;
//                }
//                $scope.prod.singlerate = $scope.prod.price;
//            } else {
//                $scope.getProductMargin();
//            }
//        });

        $scope.$watch(function (scope) {
            return scope.prod.addupgst;
        }, function (newValue, oldValue) {
            if ($scope.prod.addupgst) {
                $scope.getGstDetails();
            } else {
                $scope.getGstDetails();
            }
        });


        $scope.$watchCollection(function (scope) {
            return scope.prod;
        }, function (newValue, oldValue) {
            var str = $.param($scope.prod);
            var u = decodeURIComponent(str);
            $scope.paraurl = u;
        });

        $scope.$watch(function (scope) {
            return scope.prod.colorassorted;
        }, function (newValue, oldValue) {
            var x = parseInt($scope.prod.qty) / parseInt(newValue.length);
            $scope.prod.colorassoarr = [];
            $scope.prod.assortedcolorcode = [];
            var colors = $(".multiplevalues").select2("val");
            $('.select2-search-choice > div').each(function (i) {

                $http.get(site_url + "Products/getcolor/?code=" + $(this).html()).success(function (data) {
                    $scope.prod.assortedcolorcode.push({colorassorted: data + x});
                    $scope.prod.colorassoarr[i]["colorcode"] = data + x;
                });
            });
            angular.forEach(newValue, function (v, k) {

//                $http.get(site_url + "Products/getcolor/?code=" + colors[k]).success(function (data) {
//                    $scope.prod.assortedcolorcode.push({colorassorted: data + x});
//                    $scope.prod.colorassoarr[k]["colorcode"] = data + x;
//                });
                $scope.prod.colorassoarr.push({qty: x});
            });
        });
        $scope.$watch(function (scope) {
            return scope.prod.sizetype;
        }, function (newValue, oldValue) {
            if (newValue == "Single") {
                $scope.prod.multisizef = "";
                $scope.prod.multisizet = "";
                $("#multisizef").select2("val", "");
                $("#multisizet").select2("val", "");
            } else if (newValue == "Multiple") {
                $scope.prod.singlesize = "";
                $("#size").select2("val", "");
            }

        });
        $scope.getProductMargin = function () {
            Data.getProductMargin($.param($scope.prod)).then(function (d) {
                $scope.prod.margin = d.data.margin;
                var margin = 0;
                var gst = 0;
                var cess = 0;
                var addgst = 0;
                if ($scope.prod.margin != 0) {
                    margin = ($scope.prod.cost * $scope.prod.margin) / 100;
                    $scope.prod.price = parseFloat($scope.prod.cost) + parseFloat(margin);
                    if ($.isNumeric($scope.prod.gstno)) {
                        gst = (parseFloat($scope.prod.cost) * parseFloat($scope.prod.gstno)) / 100;
                    }
                    if ($.isNumeric($scope.prod.addupgstmrp)) {
                        addgst = (parseFloat($scope.prod.cost) * parseFloat($scope.prod.addupgstmrp)) / 100;
                    }
                    if ($.isNumeric($scope.prod.cess)) {
                        cess = (parseFloat($scope.prod.cost) * parseFloat($scope.prod.cess)) / 100;
                    }
                    $scope.prod.price = Math.round(parseFloat($scope.prod.price) + parseFloat(gst) + parseFloat(addgst) + parseFloat(cess));
                    $scope.prod.singlerate = $scope.prod.price;
                } else {
//                    alert();
                    if ($scope.prod.gstno != 0) {
                        gst = ($scope.prod.cost * $scope.prod.gstno) / 100;
                    }
                    if ($scope.prod.addupgstmrp != 0) {
                        addgst = ($scope.prod.cost * $scope.prod.addupgstmrp) / 100;
                    }
                    if ($.isNumeric($scope.prod.cess)) {
                        cess = (parseFloat($scope.prod.cost) * parseFloat($scope.prod.cess)) / 100;
                    }
                    $scope.prod.price = Math.round(parseFloat($scope.prod.cost) + parseFloat(gst) + parseFloat(addgst) + parseFloat(cess));
                    $scope.prod.singlerate = $scope.prod.price;
                }
                if ($scope.prod.price < 1000 && $scope.prod.addupgstmrp > 0) {
                    $scope.prod.addupgstmrp = 0;
                    $scope.getProductMargin();
                }
            });
        }
    }]);
app.controller('editProducts', ['$scope', '$rootScope', '$http', 'Data', function ($scope, $rootScope, $http, Data) {

        $scope.prod = {};
        $scope.loading = 0;
        $scope.getProdName = function () {
            $scope.loading = 1;
            Data.getProductName($.param($scope.prod)).then(function (d) {
//                alert(JSON.stringify(d));
                $scope.prod.name = d.data.name;
                $scope.loading = 0;
            });
        };

//        $scope.getproDetails = function () {
//            var $id = $('#editOrderporid').attr('data-item-id');
//            alert($id);
//            Data.getProDetails($id).then(function (d) {
//                $scope.prod = d;
//            });
//        }

        $scope.getGstDetails = function () {
            $scope.loading = 1;

            Data.getGstDetails($.param($scope.prod)).then(function (d) {
//                alert(JSON.stringify(d));
                if (d != "") {
                    $scope.prod.hsnno = d.data.hsncode;
                    $scope.prod.gstno = parseFloat(d.data.cgst) + parseFloat(d.data.sgst);

                } else {
                    $scope.prod.hsnno = "";
                    $scope.prod.gstno = 0;
                    $scope.prod.addupgstmrp = 0;
                }
            });
        };

        $scope.$watch(function (scope) {
            return scope.prod.cost;
        }, function (newValue, oldValue) {
            $scope.getGstDetails();
        });
        $scope.getProdBarcode = function () {
            $scope.loading = 1;
            Data.getProductBarcode($.param($scope.prod)).then(function (d) {
//                alert(JSON.stringify(d));
                $scope.prod.barcode = d.data.name;
                $scope.loading = 0;
            });
        };
        $scope.getQty = function (id) {
            if ($scope.prod.quantity !== $scope.prod.colorqty) {
                $(id).css({"border": "1px solid #f00"});
                $(id).siblings('p').removeClass('hide');
            } else {
                $(id).css({"border": "1px solid #CCCCCC"});
                $(id).siblings('p').addClass('hide');
            }
        }

        $scope.$watchCollection(function (scope) {
            return scope.prod;
        }, function (newValue, oldValue) {
            var str = $.param($scope.prod);
            var u = decodeURIComponent(str);
            $scope.paraurl = u;
        });

        $scope.$watch(function (scope) {
            return scope.prod.colortype;
        }, function (newValue, oldValue) {
            if ($scope.prod.quantity != 0) {
                if (newValue == "Single") {
                    $('.colorsingle').removeClass("hide");
                    $('.colorassorted').addClass("hide");
                    $scope.prod.colorassoarr = [];
                    $scope.prod.colorassorted = [];
                    $("#mulcolor").select2("val", "");
                    $scope.prod.colorqty = $scope.prod.quantity;
                } else {
                    $('.colorsingle').addClass("hide");
                    $('.colorassorted').removeClass("hide");
//                    $scope.prod.colorqty = 0;
                    if ($scope.prod.colorassorted.length != 0) {
                        var x = parseInt($scope.prod.quantity) / parseInt($scope.prod.colorassorted.length);
                        $scope.prod.colorassoarr = [];
                        angular.forEach(newValue, function (v, k) {
                            $scope.prod.colorassoarr.push({quantity: x});
                        });
                    }
                }
            } else {
                if (newValue != "" || oldValue != "") {
//                    bootbox.alert('Please enter Qty first.');
                    $('.colorassorted').addClass("hide");
                }
            }
        });
        $scope.getQtyCal = function (id, qty) {
            var tot = 0;
            angular.forEach($scope.prod.colorassorted, function (v, k) {
                tot += parseInt($scope.prod.colorassoarr[k].qty);
            });
            if (tot <= $scope.prod.quantity) {
                var $qty = 0;
                var j = 0;
                angular.forEach($scope.prod.colorassorted, function (v, k) {
                    if (j <= id) {
                        $qty += parseInt($scope.prod.colorassoarr[j].qty);
                    }
                    j++;
                });
                j--;
                var x = parseInt($scope.prod.quantity - $qty) / parseInt((j - id));
                var i = 0;
                angular.forEach($scope.prod.colorassorted, function (v, k) {
                    if (i > id) {
                        $scope.prod.colorassoarr[i] = {quantity: x};
                    }
                    i++;
                });
            } else {
                bootbox.alert('Please ensure that the Product Qty and addion of assorted color qty should be same.');
            }
        }
        $scope.$watch(function (scope) {
            return scope.prod.qty;
        }, function (newValue, oldValue) {
            if ($scope.prod.colortype == "Single") {
                $scope.prod.colorqty = newValue;
                $scope.prod.colorassorted = [];
            } else {
                if ($scope.prod.colorassorted.length != 0) {
                    var x = parseInt($scope.prod.quantity) / parseInt($scope.prod.colorassorted.length);
                    $scope.prod.colorassoarr = [];
                    angular.forEach($scope.prod.colorassorted, function (v, k) {
                        $scope.prod.colorassoarr.push({quantity: x});
                    });
                }
            }
        });
        $scope.$watch(function (scope) {
            return scope.prod.colorqty;
        }, function (newValue, oldValue) {
            if ($scope.prod.colortype == "Single") {
                $scope.prod.quantity = newValue;
            }
        });
        $scope.$watch(function (scope) {
            return scope.prod.colorassorted;
        }, function (newValue, oldValue) {
            var x = parseInt($scope.prod.quantity) / parseInt(newValue.length);
            $scope.prod.colorassoarr = [];
            angular.forEach(newValue, function (v, k) {
                $scope.prod.colorassoarr.push({quantity: x});
            });
        });

        $scope.getProductMargin1 = function () {
            Data.getProductMargin($.param($scope.prod)).then(function (d) {
                $scope.prod.margin = d.data.margin;
                var margin = 0;
                if ($scope.prod.sizetype == "Single") {
                    if ($scope.prod.margin != 0) {
                        margin = ($scope.prod.cost * $scope.prod.margin) / 100;
                        $scope.prod.price = parseFloat($scope.prod.cost) + parseFloat(margin);
                    }
                } else {
                    if ($scope.prod.margin != 0) {
                        margin = ($scope.prod.cost * $scope.prod.margin) / 100;
                        $scope.prod.price = parseFloat($scope.prod.cost) + parseFloat(margin);
                    }
                }
            });
        };
        
         $scope.getProductMargin = function () {
            Data.getProductMargin($.param($scope.prod)).then(function (d) {
                $scope.prod.margin = d.data.margin;
//                alert(JSON.stringify(d.data));
                var margin = 0;
                var gst = 0;
                var cess = 0;
                var addgst = 0;
                if ($scope.prod.margin != 0) {
                    margin = ($scope.prod.cost * $scope.prod.margin) / 100;
                    $scope.prod.price = parseFloat($scope.prod.cost) + parseFloat(margin);
                    if ($.isNumeric($scope.prod.gstno)) {
                        gst = (parseFloat($scope.prod.cost) * parseFloat($scope.prod.gstno)) / 100;
                    }
                    if ($.isNumeric($scope.prod.addupgstmrp)) {
                        addgst = (parseFloat($scope.prod.cost) * parseFloat($scope.prod.addupgstmrp)) / 100;
                    }
                    if ($.isNumeric($scope.prod.cess)) {
                        cess = (parseFloat($scope.prod.cost) * parseFloat($scope.prod.cess)) / 100;
                    }
                    $scope.prod.price = Math.round(parseFloat($scope.prod.price) + parseFloat(gst) + parseFloat(addgst) + parseFloat(cess));
                    $scope.prod.singlerate = $scope.prod.price;
                } else {
                    if ($scope.prod.gstno != 0) {
                        gst = ($scope.prod.price * $scope.prod.gstno) / 100;
                    }
                    if ($scope.prod.addupgstmrp != 0) {
                        addgst = ($scope.prod.price * $scope.prod.addupgstmrp) / 100;
                    }
                    if ($.isNumeric($scope.prod.cess)) {
                        cess = (parseFloat($scope.prod.cost) * parseFloat($scope.prod.cess)) / 100;
                    }
                    $scope.prod.price = Math.round(parseFloat($scope.prod.price) + parseFloat(gst) + parseFloat(addgst) + parseFloat(cess));
                    $scope.prod.singlerate = $scope.prod.price;
                }
                if ($scope.prod.price < 1000 && $scope.prod.addupgstmrp > 0) {
                    $scope.prod.addupgstmrp = 0;
                    $scope.getProductMargin();
                }
            });
        }
        $scope.$watch(function (scope) {
            return scope.prod.department;
        }, function (newValue, oldValue) {
            $scope.prod.dept = newValue;
            if (newValue != oldValue) {
                $scope.prod.dept = newValue;
            }
        });
        $scope.$watch(function (scope) {
            return scope.prod.type_id;
        }, function (newValue, oldValue) {
            $scope.prod.type = newValue;
            if (newValue != oldValue) {
                $scope.prod.type = newValue;
            }
        });
        $scope.$watch(function (scope) {
            return scope.prod.section;
        }, function (newValue, oldValue) {
            $scope.prod.section_id = newValue;
            if (newValue != oldValue) {
                $scope.prod.section_id = newValue;
            }
        });
        $scope.$watch(function (scope) {
            return scope.prod.product_items;
        }, function (newValue, oldValue) {
            $scope.prod.product_item = newValue;
            if (newValue != oldValue) {
                $scope.prod.product_item = newValue;
            }
        });
        $scope.$watch(function (scope) {
            return scope.prod.brands;
        }, function (newValue, oldValue) {
            $scope.prod.brands_id = newValue;
            if (newValue != oldValue) {
                $scope.prod.brands_id = newValue;
            }
        });
    }]);