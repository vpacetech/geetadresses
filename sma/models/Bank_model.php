<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bank_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function create_bank($data) {
        unset($data['add_bank']);
        $data['create_date'] = date('Y-m-d h:i:s');
        $rs = $this->db->insert('bank', $data);
        if ($rs) {
            return true;
        } else {
            return false;
        }
    }

    public function create_payee($data) {
        unset($data['add_bank']);
        $data['create_date'] = date('Y-m-d h:i:s');
        $rs = $this->db->insert('payee', $data);
        if ($rs) {
            return true;
        } else {
            return false;
        }
    }

    public function getBankData($id) {
        $rs = $this->db->select('*')
                        ->where('id', $id)
                        ->from('bank')
                        ->get()->result();
        return $rs[0];
    }

    public function getPayeeData($id) {
        $rs = $this->db->select('*')
                        ->where('id', $id)
                        ->from('payee')
                        ->get()->result();
//        echo"<pre>";
//        print_r($rs);
//        echo"</pre>";
//        die();
        return $rs[0];
    }

    public function getPayeeDetails($id) {
        $rs = $this->db->select('payee.*,companies.name as comp_name, bank.bank_name')
                        ->join('companies', 'companies.id = payee.store_id', 'left')
                        ->join('bank', 'bank.id = payee.bank_id', 'left')
                        ->where('payee.id', $id)
                        ->from('payee')
                        ->get()->result();
        return $rs[0];
    }

    public function update_bank($id, $data) {
        unset($data['add_bank']);
        $this->db->where('id', $id);
        $x = $this->db->update('bank', $data);
        if ($x) {
            return true;
        } else {
            return false;
        }
    }

    public function update_payee($id, $data) {
        unset($data['add_bank']);
        $this->db->where('id', $id);
        $x = $this->db->update('payee', $data);
        if ($x) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteBank($id) {
        if ($this->db->delete('bank', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deletePayee($id) {
        if ($this->db->delete('payee', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getLatestBankCode() {
        $rs = $this->db->select('id')
                        ->from('bank')
                        ->limit('1')
                        ->order_by('id', DESC)
                        ->get()->row();
        $next_id = $rs->id + 1;

        return $next_id;
    }

    public function getifsc($id) {
        $rs = $this->db->select('*')
                        ->where('id', $id)
                        ->from('bank')
                        ->get()->result();
        return $rs;
    }

}
