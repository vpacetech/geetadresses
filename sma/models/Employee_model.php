<?php

class Employee_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function addEmployee($data = array()) {
        if ($this->db->insert('employee', $data)) {
            $cid = $this->db->insert_id();
            return $cid;
        }
        return false;
    }

    public function addincentive($id, $data = array(), $empincentive = null) {
        if ($this->db->insert('employee_sale_insentive', $data)) {
            $this->db->where('id', $id);
            $this->db->update('employee', array('incentive' => $empincentive));
            return TRUE;
        }
        return false;
    }

    public function updateEmployee($id, $data = array()) {
        $this->db->where('id', $id);
        $rs = $this->db->update('employee', $data);
        if ($rs) {
            return $rs;
        }
        return false;
    }

    public function getEmployeeByID($id) {
        $q = $this->db->get_where('employee', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getUserIdByEmpID($id) {
//        $this->db->select('id');
        $q = $this->db->get_where('users', array('emplyee_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteEmployee($id) {
        if (($this->db->delete('employee', array('id' => $id))) && ($this->db->delete('users', array('emplyee_id' => $id)))) {
            return true;
        }
        return FALSE;
    }

    public function getEmployee($id) {
        $this->db->where('id', $id)->from('employee');
        return $this->db->count_all_results();
    }

    public function check_email_user($email) {
        $q = $this->db->get_where('users', array('email' => $email), 1)->result();
        if ($q) {
            return TRUE;
        } else {
            $q = $this->db->get_where('employee', array('email' => $email), 1)->result();
            if ($q) {
                return TRUE;
            }
        }
        return FALSE;
    }

}
