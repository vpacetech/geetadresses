<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function updateLogo($photo) {
        $logo = array('logo' => $photo);
        if ($this->db->update('settings', $logo)) {
            return true;
        }
        return false;
    }

    public function updateLoginLogo($photo) {
        $logo = array('logo2' => $photo);
        if ($this->db->update('settings', $logo)) {
            return true;
        }
        return false;
    }

    public function getSettings() {
        $q = $this->db->get('settings');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getDateFormats() {
        $q = $this->db->get('date_format');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function updateSetting($data) {
        $this->db->where('setting_id', '1');
        if ($this->db->update('settings', $data)) {
            return true;
        }
        return false;
    }

    public function addTaxRate($data) {
        if ($this->db->insert('tax_rates', $data)) {
            return true;
        }
        return false;
    }

    public function updateTaxRate($id, $data = array()) {
        $this->db->where('id', $id);
        if ($this->db->update('tax_rates', $data)) {
            return true;
        }
        return false;
    }

    public function getAllTaxRates() {
        $q = $this->db->get('tax_rates');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getTaxRateByID($id) {
        $q = $this->db->get_where('tax_rates', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addWarehouse($data) {
        if ($this->db->insert('warehouses', $data)) {
            return true;
        }
        return false;
    }

    public function updateWarehouse($id, $data = array()) {
        $this->db->where('id', $id);
        if ($this->db->update('warehouses', $data)) {
            return true;
        }
        return false;
    }

    public function getAllWarehouses() {
        $q = $this->db->get('warehouses');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getWarehouseByID($id) {
        $q = $this->db->get_where('warehouses', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteTaxRate($id) {
        if ($this->db->delete('tax_rates', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteInvoiceType($id) {
        if ($this->db->delete('invoice_types', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteWarehouse($id) {
        if ($this->db->delete('warehouses', array('id' => $id)) && $this->db->delete('warehouses_products', array('warehouse_id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function addCustomerGroup($data) {
        if ($this->db->insert('customer_groups', $data)) {
            return true;
        }
        return false;
    }

    public function updateCustomerGroup($id, $data = array()) {
        $this->db->where('id', $id);
        if ($this->db->update('customer_groups', $data)) {
            return true;
        }
        return false;
    }

    public function getAllCustomerGroups() {
        $q = $this->db->get('customer_groups');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCustomerGroupByID($id) {
        $q = $this->db->get_where('customer_groups', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteCustomerGroup($id) {
        if ($this->db->delete('customer_groups', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getGroups() {
        $this->db->where('id >', 4);
        $q = $this->db->get('groups');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getGroupByID($id) {
        $q = $this->db->get_where('groups', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getGroupPermissions($id) {
        $q = $this->db->get_where('permissions', array('group_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function GroupPermissions($id) {
        $q = $this->db->get_where('permissions', array('group_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->result_array();
        }
        return FALSE;
    }

    public function updatePermissions($id, $data = array()) {
        if ($this->db->update('permissions', $data, array('group_id' => $id)) && $this->db->update('users', array('show_price' => $data['products-price'], 'show_cost' => $data['products-cost']), array('group_id' => $id))) {
            return true;
        }
        return false;
    }

    public function addGroup($data) {
        if ($this->db->insert("groups", $data)) {
            $gid = $this->db->insert_id();
            $this->db->insert('permissions', array('group_id' => $gid));
            return $gid;
        }
        return false;
    }

    public function updateGroup($id, $data = array()) {
        $this->db->where('id', $id);
        if ($this->db->update("groups", $data)) {
            return true;
        }
        return false;
    }

    public function getAllCurrencies() {
        $q = $this->db->get('currencies');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCurrencyByID($id) {
        $q = $this->db->get_where('currencies', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addCurrency($data) {
        if (empty($data['auto_update'])) {
            $data['auto_update'] = 0;
        }
        if ($this->db->insert("currencies", $data)) {
            return true;
        }
        return false;
    }

    public function updateCurrency($id, $data = array()) {
        $this->db->where('id', $id);
        if ($this->db->update("currencies", $data)) {
            return true;
        }
        return false;
    }

    public function deleteCurrency($id) {
        if ($this->db->delete("currencies", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getAllCategories() {
        $q = $this->db->get("categories");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllSubCategories() {
        $q = $this->db->get("subcategories");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSubcategoryDetails($id) {
        $this->db->select("subcategories.code as code, subcategories.name as name, categories.name as parent")
                ->join('categories', 'categories.id = subcategories.category_id', 'left')
                ->group_by('subcategories.id');
        $q = $this->db->get_where("subcategories", array('subcategories.id' => $id));
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSubCategoriesByCategoryID($category_id) {
        $q = $this->db->get_where("subcategories", array('category_id' => $category_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCategoryByID($id) {
        $q = $this->db->get_where("categories", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSubCategoryByID($id) {
        $q = $this->db->get_where("subcategories", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addCategory($name, $code, $photo) {
        if ($this->db->insert("categories", array('code' => $code, 'name' => $name, 'image' => $photo))) {
            return true;
        }
        return false;
    }

    public function addSubCategory($category, $name, $code, $photo) {
        if ($this->db->insert("subcategories", array('category_id' => $category, 'code' => $code, 'name' => $name, 'image' => $photo))) {
            return true;
        }
        return false;
    }

    public function updateCategory($id, $data = array(), $photo) {
        $categoryData = array('code' => $data['code'], 'name' => $data['name']);
        if ($photo) {
            $categoryData['image'] = $photo;
        }
        $this->db->where('id', $id);
        if ($this->db->update("categories", $categoryData)) {
            return true;
        }
        return false;
    }

    public function updateSubCategory($id, $data = array(), $photo) {
        $categoryData = array(
            'category_id' => $data['category'],
            'code' => $data['code'],
            'name' => $data['name'],
        );
        if ($photo) {
            $categoryData['image'] = $photo;
        }
        $this->db->where('id', $id);
        if ($this->db->update("subcategories", $categoryData)) {
            return true;
        }
        return false;
    }

    public function deleteCategory($id) {
        if ($this->db->delete("categories", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteSubCategory($id) {
        if ($this->db->delete("subcategories", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getPaypalSettings() {
        $q = $this->db->get('paypal');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updatePaypal($data) {
        $this->db->where('id', '1');
        if ($this->db->update('paypal', $data)) {
            return true;
        }
        return FALSE;
    }

    public function getSkrillSettings() {
        $q = $this->db->get('skrill');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateSkrill($data) {
        $this->db->where('id', '1');
        if ($this->db->update('skrill', $data)) {
            return true;
        }
        return FALSE;
    }

    public function checkGroupUsers($id) {
        $q = $this->db->get_where("users", array('group_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteGroup($id) {
        if ($this->db->delete('groups', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function addVariant($data) {
        if ($this->db->insert('variants', $data)) {
            return true;
        }
        return false;
    }

    public function updateVariant($id, $data = array()) {
        $this->db->where('id', $id);
        if ($this->db->update('variants', $data)) {
            return true;
        }
        return false;
    }

    public function getAllVariants() {
        $q = $this->db->get('variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getVariantByID($id) {
        $q = $this->db->get_where('variants', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteVariant($id) {
        if ($this->db->delete('variants', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getAllOffer() {
        $q = $this->db->get('offers');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getActivetedOffer() {
        $this->db->where('status', 'Active');
        $q = $this->db->get('offers');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getActivetedOfferwithapplyoffer() {
        $this->db->where('status', 'Active');
        $this->db->where('if_coupon_applocable', 'True');
        $q = $this->db->get('offers');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getOfferByID($id) {
        $q = $this->db->get_where('offers', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            $data = $q->row();
            $data->start_date = $this->sma->hrsd($data->start_date);
            $data->end_dates = $data->end_date;
            $data->end_date = $this->sma->hrsd($data->end_date);
            
            return $data;
        }
        return FALSE;
    }

    public function add_coupon($data) {
        if ($this->db->insert('coupon', $data)) {
            $cid = $this->db->insert_id();
            return $cid;
        }

        return false;
    }

    public function update_coupon($id, $data) {
        $this->db->where('id', $id);
        if ($this->db->update('coupon', $data)) {
            return true;
        }
        return false;
    }

    public function getCouponByID($id) {
        $q = $this->db->get_where('coupon', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteCoupon($id) {
        if ($this->db->delete('coupon', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteoffer($id) {
        if ($this->db->delete('offers', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getofferbypos() {
        $this->db->where('start_date <=', date('Y-m-d'));
        $this->db->where('end_date >=', date('Y-m-d'));
        $this->db->where('status', 'Active');
        $q = $this->db->get('offers');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getParamDetails($tab, $id) {
        $t = $this->db->dbprefix($tab);
        $d = $this->db->dbprefix('department');
        $sec = $this->db->dbprefix('section');
        $pi = $this->db->dbprefix('product_items');
        $type = $this->db->dbprefix('type');
        $brands = $this->db->dbprefix('brands');
        $style = $this->db->dbprefix('style');
        $design = $this->db->dbprefix('design');
        $pattern = $this->db->dbprefix('pattern');
        $fitting = $this->db->dbprefix('fitting');

        if ($tab == "department") {
            return $this->db->select('*')
                            ->from($tab)
                            ->where('id', $id)
                            ->get()->row();
        } else if ($tab == "section") {
            return $this->db->select("$t.*,$d.store_id")
                            ->from($tab)
                            ->join("department", "department.id = $tab.department_id")
                            ->where("$t.id", $id)
                            ->get()->row();
        } else if ($tab == "product_items") {
            $r = $this->db->select("$t.*,$d.store_id,$sec.department_id")
                            ->from($tab)
                            ->join("section", "section.id = $tab.section_id")
                            ->join("department", "department.id = $sec.department_id")
                            ->where("$t.id", $id)
                            ->get()->row();
            return $r;
        } else if ($tab == "type") {
            $r = $this->db->select("$t.*,$d.store_id,$sec.department_id,$pi.section_id")
                            ->from($tab)
                            ->join("product_items", "product_items.id = $tab.product_items_id")
                            ->join("section", "section.id = product_items.section_id")
                            ->join("department", "department.id = section.department_id")
                            ->where("$t.id", $id)
                            ->get()->row();
//            echo "<pre>";
//            print_r($r);
//            echo "</pre>";
//            die();
            return $r;
        } else if ($tab == "brands") {
            $r = $this->db->select("$t.*,$d.store_id,$sec.department_id,$pi.section_id, $type.product_items_id")
                            ->from($tab)
                            ->join("type", "type.id = $tab.type_id")
                            ->join("product_items", "product_items.id = type.product_items_id")
                            ->join("section", "section.id = product_items.section_id")
                            ->join("department", "department.id = section.department_id")
                            ->where("$t.id", $id)
                            ->get()->row();

            return $r;
        } else if ($tab == "design") {
            $r = $this->db->select("$t.*,$d.store_id,$sec.department_id,$pi.section_id, $type.product_items_id, $brands.type_id")
                            ->from($tab)
                            ->join("brands", "brands.id = $tab.brands_id")
                            ->join("type", "type.id = brands.type_id")
                            ->join("product_items", "product_items.id = type.product_items_id")
                            ->join("section", "section.id = product_items.section_id")
                            ->join("department", "department.id = section.department_id")
                            ->where("$t.id", $id)
                            ->get()->row();

            return $r;
        } else if ($tab == "style") {
            $r = $this->db->select("$t.*,$d.store_id,$sec.department_id,$pi.section_id, $type.product_items_id, $brands.type_id,$design.brands_id")
                            ->from($tab)
                            ->join("design", "design.id = $tab.design_id")
                            ->join("brands", "brands.id = design.brands_id")
                            ->join("type", "type.id = brands.type_id")
                            ->join("product_items", "product_items.id = type.product_items_id")
                            ->join("section", "section.id = product_items.section_id")
                            ->join("department", "department.id = section.department_id")
                            ->where("$t.id", $id)
                            ->get()->row();

            return $r;
        } else if ($tab == "pattern") {
            $r = $this->db->select("$t.*,$d.store_id,$sec.department_id,$pi.section_id, $type.product_items_id, $brands.type_id,$design.brands_id,$style.design_id")
                            ->from($tab)
                            ->join("style", "style.id = $tab.style_id")
                            ->join("design", "design.id = style.design_id")
                            ->join("brands", "brands.id = design.brands_id")
                            ->join("type", "type.id = brands.type_id")
                            ->join("product_items", "product_items.id = type.product_items_id")
                            ->join("section", "section.id = product_items.section_id")
                            ->join("department", "department.id = section.department_id")
                            ->where("$t.id", $id)
                            ->get()->row();
            return $r;
        } else if ($tab == "fitting") {
            $r = $this->db->select("$t.*,$d.store_id,$sec.department_id,$pi.section_id, $type.product_items_id, $brands.type_id,$design.brands_id,$style.design_id, $pattern.style_id")
                            ->from($tab)
                            ->join("pattern", "pattern.id = $tab.pattern_id")
                            ->join("style", "style.id = pattern.style_id")
                            ->join("design", "design.id = style.design_id")
                            ->join("brands", "brands.id = design.brands_id")
                            ->join("type", "type.id = brands.type_id")
                            ->join("product_items", "product_items.id = type.product_items_id")
                            ->join("section", "section.id = product_items.section_id")
                            ->join("department", "department.id = section.department_id")
                            ->where("$t.id", $id)
                            ->get()->row();
            return $r;
        } else if ($tab == "fabric") {
            $r = $this->db->select("$t.*,$d.store_id,$sec.department_id,$pi.section_id, $type.product_items_id, $brands.type_id,$design.brands_id,$style.design_id, $pattern.style_id, $fitting.pattern_id")
                            ->from($tab)
                            ->join("fitting", "fitting.id = $tab.fitting_id")
                            ->join("pattern", "pattern.id = fitting.pattern_id")
                            ->join("style", "style.id = pattern.style_id")
                            ->join("design", "design.id = style.design_id")
                            ->join("brands", "brands.id = design.brands_id")
                            ->join("type", "type.id = brands.type_id")
                            ->join("product_items", "product_items.id = type.product_items_id")
                            ->join("section", "section.id = product_items.section_id")
                            ->join("department", "department.id = section.department_id")
                            ->where("$t.id", $id)
                            ->get()->row();
            return $r;
        } else {

            return $this->db->select('*')
                            ->from($tab)
                            ->where('id', $id)
                            ->get()->row();
        }
    }

    public function add_position($data) {
        if ($this->db->insert('position', $data)) {
            $cid = $this->db->insert_id();
            return $cid;
        }
        return false;
    }

    public function getPostionByID($id) {
        $q = $this->db->get_where('position', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function update_position($id, $data) {
        $this->db->where('id', $id);
        if ($this->db->update('position', $data)) {
            return true;
        }
        return false;
    }

    public function getPosition() {
        $q = $this->db->get('position');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

}
