<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Quotes_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getProductNamesFromProducts($term, $warehouse_id, $customer_id, $store_id, $limit = 5) {
        $this->db->select('products.id, products.code, products.name, products.type, warehouses_products.quantity, products.price,products.cost, products.tax_rate, products.tax_method,design.name as design_name,products.squantity')
                ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
                ->join('design', 'products.design = design.id')
                ->where('products.supplier1', $customer_id)
                ->where('products.store_id', $store_id)
                ->group_by('products.id');
        $this->db->where("(sma_products.name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR  concat(sma_products.name, ' (', code, ')') LIKE '%" . $term . "%')");
        $this->db->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

//23/01/2018 Pavan

    public function getProductNames($term, $warehouse_id, $customer_id, $store_id, $limit = 5) {
        $this->db->select('sma_quotes_products.id, sma_quotes_products.code, sma_quotes_products.name, sma_quotes_products.type, sma_warehouses_products.quantity, sma_quotes_products.price, sma_quotes_products.cost, sma_quotes_products.tax_rate, sma_quotes_products.tax_method, sma_design.name as design_name, sma_quotes_products.squantity , sma_color.name as colorname, sma_design.name as designname, sma_style.name as stylename, sma_pattern.name as patternname, sma_fitting.name as fittingname, sma_fabric.name as fabricname, IF(sma_quotes_products.singlerate!= "", sma_quotes_products.singlerate, IF(sma_quotes_products.mrprate!= "", sma_quotes_products.mrprate, CONCAT(sma_quotes_products.mulratef, " - ", sma_quotes_products.mulratet))) AS rate, sma_per.name as uname', FALSE)
                ->join('sma_warehouses_products', 'sma_warehouses_products.product_id=sma_quotes_products.id', 'left')
                ->join('sma_design', 'sma_quotes_products.design = sma_design.id')
                ->join('sma_color', 'sma_color.id =  sma_quotes_products.color', 'left')
                ->join('sma_style', 'sma_style.id =  sma_quotes_products.style', 'left')
                ->join('sma_pattern', 'sma_pattern.id =  sma_quotes_products.pattern', 'left')
                ->join('sma_fitting', 'sma_fitting.id =  sma_quotes_products.fitting', 'left')
                ->join('sma_fabric', 'sma_fabric.id =  sma_quotes_products.fabric', 'left')
                ->join('sma_per', 'sma_per.id =  sma_quotes_products.sunit', 'left')
                ->where('quotes_products.supplier1', $customer_id)
                ->where('quotes_products.store_id', $store_id)
                ->group_by('quotes_products.id');
        $this->db->where("(sma_quotes_products.name LIKE '%" . $term . "%' OR sma_quotes_products.code LIKE '%" . $term . "%' OR  concat(sma_quotes_products.name, ' (', sma_quotes_products.code, ')') LIKE '%" . $term . "%')");
        $this->db->limit($limit);
        $q = $this->db->get('sma_quotes_products');
//        echo $this->db->last_query();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

//Vishal: Remove Company/Supplier id from the query
    public function getProductNameInStore($term, $warehouse_id, $customer_id, $store_id, $limit = 5) {
        $this->db->select('sma_quotes_products.id, sma_quotes_products.code, sma_quotes_products.name, sma_quotes_products.type, sma_warehouses_products.quantity, sma_quotes_products.price, sma_quotes_products.cost, sma_quotes_products.tax_rate, sma_quotes_products.tax_method, sma_design.name as design_name, sma_quotes_products.squantity , sma_color.name as colorname, sma_design.name as designname, sma_style.name as stylename, sma_pattern.name as patternname, sma_fitting.name as fittingname, sma_fabric.name as fabricname, IF(sma_quotes_products.singlerate!= "", sma_quotes_products.singlerate, IF(sma_quotes_products.mrprate!= "", sma_quotes_products.mrprate, CONCAT(sma_quotes_products.mulratef, " - ", sma_quotes_products.mulratet))) AS rate, sma_per.name as uname', FALSE)
                ->join('sma_warehouses_products', 'sma_warehouses_products.product_id=sma_quotes_products.id', 'left')
                ->join('sma_design', 'sma_quotes_products.design = sma_design.id')
                ->join('sma_color', 'sma_color.id =  sma_quotes_products.color', 'left')
                ->join('sma_style', 'sma_style.id =  sma_quotes_products.style', 'left')
                ->join('sma_pattern', 'sma_pattern.id =  sma_quotes_products.pattern', 'left')
                ->join('sma_fitting', 'sma_fitting.id =  sma_quotes_products.fitting', 'left')
                ->join('sma_fabric', 'sma_fabric.id =  sma_quotes_products.fabric', 'left')
                ->join('sma_per', 'sma_per.id =  sma_quotes_products.sunit', 'left')
                // ->where('quotes_products.supplier1', $customer_id)
                ->where('quotes_products.store_id', $store_id)
                ->group_by('quotes_products.id');
        $this->db->where("(sma_quotes_products.name LIKE '%" . $term . "%' OR sma_quotes_products.code LIKE '%" . $term . "%' OR  concat(sma_quotes_products.name, ' (', sma_quotes_products.code, ')') LIKE '%" . $term . "%')");
        $this->db->limit($limit);
        $q = $this->db->get('sma_quotes_products');
//        echo $this->db->last_query();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

//    public function getProductNames($term, $warehouse_id, $customer_id, $store_id, $limit = 5) {
//        $this->db->select('quotes_products.id, quotes_products.code, quotes_products.name, quotes_products.type, warehouses_products.quantity, quotes_products.price,quotes_products.cost, quotes_products.tax_rate, quotes_products.tax_method,design.name as design_name,quotes_products.squantity')
//                ->join('warehouses_products', 'warehouses_products.product_id=quotes_products.id', 'left')
//                ->join('design', 'quotes_products.design = design.id')
//                ->where('quotes_products.supplier1', $customer_id)
//                ->where('quotes_products.store_id', $store_id)
//                ->group_by('quotes_products.id');
//        $this->db->where("(sma_quotes_products.name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR  concat(sma_quotes_products.name, ' (', code, ')') LIKE '%" . $term . "%')");
//        $this->db->limit($limit);
//        $q = $this->db->get('quotes_products');
//        echo $this->db->last_query();
//        if ($q->num_rows() > 0) {
//            foreach (($q->result()) as $row) {
//                $data[] = $row;
//            }
//            return $data;
//        }
//    }

    public function getProductByCode($code) {
//        $q = $this->db->get_where('products', array('code' => $code), 1);
        $q = $this->db->get_where('sma_quotes_products', array('code' => $code), 1);
//        if ($q->num_rows() > 0) {
//            return $q->row();
//        } else {
//            $qs = $this->db->get_where('sma_quotes_products', array('code' => $code), 1)->get();
//            if ($qs->num_rows() > 0) {
//                return $qs->row();
//            }
//        }


        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getWHProduct($id) {
        $this->db->select('products.id, code, name, warehouses_products.quantity, cost, tax_rate')
                ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
                ->group_by('products.id');
        $q = $this->db->get_where('products', array('warehouses_products.product_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getItemByID($id) {
        $q = $this->db->get_where('quote_items', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllQuoteItemsWithDetails($quote_id) {
        $this->db->select('quote_items.id, quote_items.product_name, quote_items.product_code, quote_items.quantity, quote_items.serial_no, quote_items.tax, quote_items.unit_price, quote_items.val_tax, quote_items.discount_val, quote_items.gross_total, products.details');
        $this->db->join('products', 'products.id=quote_items.product_id', 'left');
        $this->db->order_by('id', 'asc');
        $q = $this->db->get_where('quotes_items', array('quote_id' => $quote_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getQuoteByID($id) {
        $q = $this->db->get_where('quotes', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllQuoteItems($quote_id) {
        $this->db->select('quote_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, quotes_products.unit, quotes_products.details as details, product_variants.name as variant,quotes_products.squantity as sec_qty,quotes_products.sunit,sma_per.name as unit_name,sma_design.name as design_name')
                ->join('quotes_products', 'quotes_products.id=quote_items.product_id', 'left')
                ->join('product_variants', 'product_variants.id=quote_items.option_id', 'left')
                ->join('tax_rates', 'tax_rates.id=quote_items.tax_rate_id', 'left')
                ->join('sma_per', 'quotes_products.sunit = sma_per.id', 'left')
                ->join('sma_design', 'quotes_products.design = sma_design.id', 'left')
                ->group_by('quote_items.id')
                ->order_by('id', 'asc');
        $q = $this->db->get_where('quote_items', array('quote_id' => $quote_id));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function viewAllQuoteItems($quote_id) {
        $q = $this->db->query("SELECT 
sma_quotes_products.color,
MIN(sma_size.name) AS from_size,
MAX(sma_size.name) AS to_size,
COUNT(DISTINCT sma_quotes_products.color) AS total_colors,
SUM(sma_quote_items.quantity) AS total_quantity,
sma_quote_items.*,
sma_tax_rates.code AS tax_code,
sma_tax_rates.name AS tax_name,
sma_tax_rates.rate AS tax_rate,
sma_quotes_products.unit,
sma_quotes_products.details AS details,
sma_product_variants.name AS variant,
sma_quotes_products.squantity AS sec_qty,
sma_quotes_products.sunit,
sma_per.name as unit_name
FROM
sma_quote_items 
LEFT JOIN sma_quotes_products 
ON sma_quotes_products.id = sma_quote_items.product_id 
LEFT JOIN sma_product_variants 
ON sma_product_variants.id = sma_quote_items.option_id 
LEFT JOIN sma_tax_rates 
ON sma_tax_rates.id = sma_quote_items.tax_rate_id 
LEFT JOIN sma_size 
ON sma_size.id = sma_quotes_products.size 
LEFT JOIN sma_per ON sma_per.id = sma_quotes_products.sunit
WHERE quote_id ='$quote_id' GROUP BY sma_quotes_products.fabric,sma_quotes_products.fitting,sma_quotes_products.pattern,sma_quotes_products.style,sma_quote_items.unit_price
ORDER BY id ASC ");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function addQuote($data = array(), $items = array()) {
        $rs = $this->db->insert('quotes', $data);

        $quote_id = $this->db->insert_id();
        if ($this->site->getReference('qu') == $data['reference_no']) {
            $this->site->updateReference('qu');
        }
        foreach ($items as $item) {
            $item['quote_id'] = $quote_id;
            $this->db->insert('quote_items', $item);
            $rs = $this->db->select('*')
                            ->from('sma_quotes_products')
                            ->where('code', $item['product_code'])
                            ->get()->result();

            if (empty($rs)) {
                $rss = $this->db->select('*')
                                ->from('sma_products')
                                ->where('code', $item['product_code'])
                                ->get()->row();
                unset($rss->id);
                $ins = $this->db->insert('sma_quotes_products', $rss);
            }
        }
        if ($rs) {
            return true;
        } else {
            return FALSE;
        }
    }

    public function updateQuote($id, $data, $items = array()) {
        if ($this->db->update('quotes', $data, array('id' => $id)) && $this->db->delete('quote_items', array('quote_id' => $id))) {
            foreach ($items as $item) {
                $item['quote_id'] = $id;
                $this->db->insert('quote_items', $item);
            }
            return true;
        }
        return false;
    }

    public function deleteQuote($id) {
        if ($this->db->delete('quote_items', array('quote_id' => $id)) && $this->db->delete('quotes', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getProductByName($name) {
        $q = $this->db->get_where('products', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getWarehouseProductQuantity($warehouse_id, $product_id) {
        $q = $this->db->get_where('warehouses_products', array('warehouse_id' => $warehouse_id, 'product_id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductComboItems($pid, $warehouse_id) {
        $this->db->select('products.id as id, combo_items.item_code as code, combo_items.quantity as qty, products.name as name, products.type as type, warehouses_products.quantity as quantity')
                ->join('products', 'products.code=combo_items.item_code', 'left')
                ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
                ->where('warehouses_products.warehouse_id', $warehouse_id)
                ->group_by('combo_items.id');
        $q = $this->db->get_where('combo_items', array('combo_items.product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function getProductOptions($product_id, $warehouse_id) {
        $this->db->select('product_variants.id as id, product_variants.name as name, product_variants.price as price, product_variants.quantity as total_quantity, warehouses_products_variants.quantity as quantity')
                ->join('warehouses_products_variants', 'warehouses_products_variants.option_id=product_variants.id', 'left')
                //->join('warehouses', 'warehouses.id=product_variants.warehouse_id', 'left')
                ->where('product_variants.product_id', $product_id)
                ->where('warehouses_products_variants.warehouse_id', $warehouse_id)
                ->where('warehouses_products_variants.quantity >', 0)
                ->group_by('product_variants.id');
        $q = $this->db->get('product_variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPurchasedItems($product_id, $warehouse_id, $option_id = NULL) {
        $orderby = ($this->Settings->accounting_method == 1) ? 'asc' : 'desc';
        $this->db->select('id, quantity, quantity_balance, net_unit_cost, item_tax');
        $this->db->where('product_id', $product_id)->where('warehouse_id', $warehouse_id)->where('quantity_balance !=', 0);
        if ($option_id) {
            $this->db->where('option_id', $option_id);
        }
        $this->db->group_by('id');
        $this->db->order_by('date', $orderby);
        $this->db->order_by('purchase_id', $orderby);
        $q = $this->db->get('purchase_items');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getMaxOrderId() {
        $Q = $this->db->select('max(id) as id')->from('quotes')->get()->row();
        return sprintf('%04d', (intval($Q->id) + 1));
        ;
    }

    public function getQuoteListById($id) {
        $q = $this->db->get_where('vw_orderlist', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

}
