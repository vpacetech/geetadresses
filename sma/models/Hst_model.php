<?php

class Hst_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function save($data = array()) {
        if ($this->db->insert('hsncodes', $data)) {
            return true;
        }
        return false;
    }

    public function delete($id) {
        if ($this->db->delete('hsncodes', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function updateGst($id, $data) {
        if ($this->db->update('hsncodes', $data, array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getHsnByID($id) {
        $q = $this->db->get_where('hsncodes', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

}
