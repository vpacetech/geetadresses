<?php

class Gst_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function save($data = array()) {
        if ($this->db->insert('gst', $data)) {
            return true;
        }
        return false;
    }

    public function delete($id) {
        if ($this->db->delete('gst', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function updateGst($id, $data) {
        if ($this->db->update('gst', $data, array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getGstByID($id) {
        $q = $this->db->get_where('gst', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getgstdetaislByID($id) {
        $gst = $this->db->dbprefix('gst');
        $dept = $this->db->dbprefix('department');
        $pro = $this->db->dbprefix('product_items');
        $type = $this->db->dbprefix('type');
        $brands = $this->db->dbprefix('brands');
        $design = $this->db->dbprefix('design');
        $style = $this->db->dbprefix('style');
        $size = $this->db->dbprefix('size');

        $r = $this->db->select("$gst.id,$gst.id as gstid,$dept.name as department,$pro.name as product_items,"
                        . " $type.name as type, $brands.name as brands,$design.name as design,$style.name as style,$size.name as size,"
                        . "IF(mrp > 0, mrp, CONCAT(mrpfrom, '-', mrpto)),cgst,sgst,hsncode,cess", FALSE)
                ->from('gst')
                ->join('department', 'gst.department = department.id', 'left')
                ->join('product_items', 'gst.product_items = product_items.id', 'left')
                ->join('type', 'gst.type_id = type.id', 'left')
                ->join('brands', 'gst.brands = brands.id', 'left')
                ->join('design', 'gst.design = design.id', 'left')
                ->join('style', 'gst.style = style.id', 'left')
                ->join('size', 'gst.size = size.id', 'left')
                ->where('gst.id', $id)
                ->get()
                ->row();

        return $r;
    }

    public function getgstdetaisl() {
//        echo '<pre>';
//        print_r($_GET);
//        echo '</pre>';
//        die();
        $wstore_id =  $this->input->get('store_id') ?  $this->input->get('store_id') : $this->input->get('store_id');
        $wdeptment =  $this->input->get('dept') ?  $this->input->get('dept') : $this->input->get('department');
        $wproduct_item =  $this->input->get('product_item') ?  $this->input->get('product_item') : $this->input->get('product_items');
        $wtype =  $this->input->get('type_id') ?  $this->input->get('type_id') : $this->input->get('type');
        $wbrands_id =  $this->input->get('brands_id') ?  $this->input->get('brands_id') : $this->input->get('brands');
        $wdesign =  $this->input->get('design') ?  $this->input->get('design') : $this->input->get('design');
        
        $gst = $this->db->dbprefix('gst');
        $dept = $this->db->dbprefix('department');
        $pro = $this->db->dbprefix('product_items');
        $type = $this->db->dbprefix('type');
        $brands = $this->db->dbprefix('brands');
        $design = $this->db->dbprefix('design');
        $style = $this->db->dbprefix('style');
        $size = $this->db->dbprefix('size');
        $this->db->select("$gst.*");
        $this->db->from('gst');
        $this->db->join('companies', 'gst.store_id = companies.id', 'left');
        $this->db->join('department', 'gst.department = department.id', 'left');
        $this->db->join('product_items', 'gst.product_items = product_items.id', 'left');
        $this->db->join('type', 'gst.type_id = type.id', 'left');
        $this->db->join('brands', 'gst.brands = brands.id', 'left');
//        $this->db->join('design', 'gst.design = design.id', 'left');
//        $this->db->join('style', 'gst.style = style.id', 'left');
//        $this->db->join('size', 'gst.size = size.id', 'left');
//        $this->db->join('pattern', 'gst.pattern = pattern.id', 'left');
//        $this->db->join('fitting', 'gst.fitting = fitting.id', 'left');
//        $this->db->join('fabric', 'gst.fabric = fabric.id', 'left');

        $this->db->where('companies.id', $wstore_id);
        $this->db->where('department.id', $wdeptment);
        $this->db->where('product_items.id', $wproduct_item);
        $this->db->where('type.id', $wtype);
        $this->db->where('brands.id', $wbrands_id);
//        $this->db->where('design.id', $wdesign);

        $r = $this->db->get()->row();
       
        return $r;
    }

    public function getGSTtoHSN($hsn) {
        $q = $this->db->get_where('gst', array('hsncode' => $hsn), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    
    public function getGstIDHSNCode($id, $hsncode=null) {
        
        $q = $this->db->get_where('gst', array('id !=' => $id, 'hsncode' =>$hsncode), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

}
