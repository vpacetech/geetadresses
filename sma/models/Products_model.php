<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Products_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getAllProducts() {
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getProductOptions($pid) {
        $q = $this->db->get_where('product_variants', array('product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductOptionsWithWH($pid) {
        $this->db->select($this->db->dbprefix('product_variants') . '.*, ' . $this->db->dbprefix('warehouses') . '.name as wh_name, ' . $this->db->dbprefix('warehouses') . '.id as warehouse_id, ' . $this->db->dbprefix('warehouses_products_variants') . '.quantity as wh_qty')
                ->join('warehouses_products_variants', 'warehouses_products_variants.option_id=product_variants.id', 'left')
                ->join('warehouses', 'warehouses.id=warehouses_products_variants.warehouse_id', 'left')
                ->group_by(array('' . $this->db->dbprefix('product_variants') . '.id', '' . $this->db->dbprefix('warehouses_products_variants') . '.warehouse_id'))
                ->order_by('product_variants.id');
        $q = $this->db->get_where('product_variants', array('product_variants.product_id' => $pid, 'warehouses_products_variants.quantity !=' => NULL));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getSizeByID($id) {

        $q = $this->db->get_where('size', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;

    }
    
    public function deleteSize($id){
        if ($this->db->delete('size', array('id' => $id))) {
            return true;
        }
    }

    public function getDepartment(){
        return $this->db->select('id,name')->from('department')->get()->result();
    }

    public function getDeptName($department_id){
        return $this->db->select('id,name')->from('department')->where('id',$department_id)->get()->row();
    }
    public function getProdName($product_items_id){
        return $this->db->select('id,name')->from('product_items')->where('id',$product_items_id)->get()->row();
    }
    public function getProduct(){
        return $this->db->select('id,name')->from('product_items')->get()->result();
    }

    public function getSection(){
        return $this->db->select('id,name')->from('section')->get()->result();
    }

    public function updateSize($id, $data) {

        $this->db->where('id', $id);
        if ($this->db->update('size', $data)) {
            return true;
        }
        return false;
    }

    public function addSize($post){
        
        if ( $this->db->insert('size',$post) ) {
            return 1;
        }
        
        return 0;
    }

    
    public function addChallan($data = array(), $items = array()) {
        if ($this->db->insert('product_challan', $data)) {
            $challan_no = $this->db->insert_id();

            foreach ($items as $item) {
                $item['challan_no'] = $challan_no;
                $this->db->insert('product_challan_items', $item);
            }
            return true;
        }
        return false;
    }

    public function getChallanByID($id) {
        $q = $this->db->get_where('product_challan', array('challan_no' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllChallanByID($id) {
        $this->db->select('product_challan.*, companies.company as customer_name')
                ->join('companies', 'product_challan.customer_id=companies.id', 'left')
                ->group_by('product_challan.challan_no')
                ->order_by('challan_no', 'asc');
        $q = $this->db->get_where('product_challan', array('challan_no' => $id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllChallanItems($challan_no) {
        $this->db->select('product_challan_items.*')
                ->join('products', 'products.id=product_challan_items.product_id', 'left')
                ->group_by('product_challan_items.id')
                ->order_by('id', 'asc');
        $q = $this->db->get_where('product_challan_items', array('challan_no' => $challan_no));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllChallanItemsWithDetails($challan_no) {
        $this->db->select('product_challan_items.id, product_challan_items.challan_no, product_challan_items.product_code, product_challan_items.quantity,products.details,products.name as product_name');
        $this->db->join('products', 'products.id=product_challan_items.product_id', 'left');
        $this->db->order_by('id', 'asc');
        $q = $this->db->get_where('product_challan_items', array('challan_no' => $challan_no));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function updateChallan($id, $data, $items = array()) {
        if ($this->db->update('product_challan', $data, array('challan_no' => $id)) && $this->db->delete('product_challan_items', array('challan_no' => $id))) {
            foreach ($items as $item) {
                $item['challan_no'] = $id;
                $this->db->insert('product_challan_items', $item);
            }
            return true;
        }
        return false;
    }

    public function getProductComboItems($pid) {
        $this->db->select($this->db->dbprefix('products') . '.id as id, ' . $this->db->dbprefix('products') . '.code as code, ' . $this->db->dbprefix('combo_items') . '.quantity as qty, ' . $this->db->dbprefix('products') . '.name as name, ' . $this->db->dbprefix('combo_items') . '.unit_price as price')->join('products', 'products.code=combo_items.item_code', 'left')->group_by('combo_items.id');
        $q = $this->db->get_where('combo_items', array('product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function getProductByID($id) {
        $q = $this->db->get_where('products', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function has_purchase($product_id, $warehouse_id = NULL) {
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get_where('purchase_items', array('product_id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function getProductDetails($id) {
        $this->db->select($this->db->dbprefix('products') . '.code, ' . $this->db->dbprefix('products') . '.name, ' . $this->db->dbprefix('categories') . '.code as category_code, cost, price, quantity, alert_quantity')
                ->join('categories', 'categories.id=products.category_id', 'left');
        $q = $this->db->get_where('products', array('products.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductDetail($id) {
        $this->db->select($this->db->dbprefix('products') . '.*, ' . $this->db->dbprefix('tax_rates') . '.code as tax_rate_code, ' . $this->db->dbprefix('categories') . '.code as category_code, ' . $this->db->dbprefix('subcategories') . '.code as subcategory_code')
                ->join('tax_rates', 'tax_rates.id=products.tax_rate', 'left')
                ->join('categories', 'categories.id=products.category_id', 'left')
                ->join('subcategories', 'subcategories.id=products.subcategory_id', 'left');
        $q = $this->db->get_where('products', array('products.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductByCategoryID($id) {

        $q = $this->db->get_where('products', array('category_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return true;
        }

        return FALSE;
    }

    public function getAllWarehousesWithPQ($product_id) {
        $this->db->select('' . $this->db->dbprefix('warehouses') . '.*, ' . $this->db->dbprefix('warehouses_products') . '.quantity, ' . $this->db->dbprefix('warehouses_products') . '.rack')
                ->join('warehouses_products', 'warehouses_products.warehouse_id=warehouses.id', 'left')
                ->where('warehouses_products.product_id', $product_id)
                ->group_by('warehouses.id');
        $q = $this->db->get('warehouses');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductPhotos($id) {
        $q = $this->db->get_where("product_photos", array('product_id' => $id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getProductByCode($code) {

        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function addProduct($data, $items, $warehouse_qty, $product_attributes, $photos) {

        if ($this->db->insert('products', $data)) {

            $product_id = $this->db->insert_id();

            $this->site->updateReference('br');
            if ($items) {
                foreach ($items as $item) {
                    $item['product_id'] = $product_id;
                    $this->db->insert('combo_items', $item);
                }
            }

            if ($data['type'] == 'combo' || $data['type'] == 'bundle') {
                $warehouses = $this->site->getAllWarehouses();
                foreach ($warehouses as $warehouse) {
                    $this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse->id, 'quantity' => 0));
                }
            }
            $tax_rate = $this->site->getTaxRateByID($data['tax_rate']);

            if ($warehouse_qty && !empty($warehouse_qty)) {

                foreach ($warehouse_qty as $wh_qty) {
                    if (isset($wh_qty['quantity']) && !empty($wh_qty['quantity'])) {
                        $this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $wh_qty['warehouse_id'], 'quantity' => $wh_qty['quantity'], 'rack' => $wh_qty['rack']));
                        if (!$product_attributes) {
                            $tax_rate_id = $tax_rate ? $tax_rate->id : NULL;
                            $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : NULL;
                            $unit_cost = $data['cost'];
                            if ($tax_rate) {
                                if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                                    if ($data['tax_method'] == '0') {
                                        $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                        $net_item_cost = $data['cost'] - $pr_tax_val;
                                        $item_tax = $pr_tax_val * $wh_qty['quantity'];
                                    } else {
                                        $net_item_cost = $data['cost'];
                                        $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                        $unit_cost = $data['cost'] + $pr_tax_val;
                                        $item_tax = $pr_tax_val * $wh_qty['quantity'];
                                    }
                                } else {
                                    $net_item_cost = $data['cost'];
                                    $item_tax = $tax_rate->rate;
                                }
                            } else {
                                $net_item_cost = $data['cost'];
                                $item_tax = 0;
                            }

                            $subtotal = (($net_item_cost * $wh_qty['quantity']) + $item_tax);

                            $item = array(
                                'product_id' => $product_id,
                                'product_code' => $data['code'],
                                'product_name' => $data['name'],
                                'net_unit_cost' => $net_item_cost,
                                'unit_cost' => $unit_cost,
                                'quantity' => $wh_qty['quantity'],
                                'quantity_balance' => $wh_qty['quantity'],
                                'item_tax' => $item_tax,
                                'tax_rate_id' => $tax_rate_id,
                                'tax' => $tax,
                                'subtotal' => $subtotal,
                                'warehouse_id' => $wh_qty['warehouse_id'],
                                'date' => date('Y-m-d'),
                                'status' => 'received',
                            );
                            $this->db->insert('purchase_items', $item);
                            $this->site->syncProductQty($product_id, $wh_qty['warehouse_id']);
                        }
                    }
                }
            }

            if ($product_attributes) {
                foreach ($product_attributes as $pr_attr) {
                    $pr_attr_details = $this->getPrductVariantByPIDandName($product_id, $pr_attr['name']);

                    $pr_attr['product_id'] = $product_id;
                    $variant_warehouse_id = $pr_attr['warehouse_id'];
                    unset($pr_attr['warehouse_id']);
                    if ($pr_attr_details) {
                        $option_id = $pr_attr_details->id;
                    } else {
                        $this->db->insert('product_variants', $pr_attr);
                        $option_id = $this->db->insert_id();
                    }
                    if ($pr_attr['quantity'] != 0) {
                        $this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $variant_warehouse_id, 'quantity' => $pr_attr['quantity']));

                        $tax_rate_id = $tax_rate ? $tax_rate->id : NULL;
                        $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : NULL;
                        $unit_cost = $data['cost'];
                        if ($tax_rate) {
                            if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                                if ($data['tax_method'] == '0') {
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                    $net_item_cost = $data['cost'] - $pr_tax_val;
                                    $item_tax = $pr_tax_val * $pr_attr['quantity'];
                                } else {
                                    $net_item_cost = $data['cost'];
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                    $unit_cost = $data['cost'] + $pr_tax_val;
                                    $item_tax = $pr_tax_val * $pr_attr['quantity'];
                                }
                            } else {
                                $net_item_cost = $data['cost'];
                                $item_tax = $tax_rate->rate;
                            }
                        } else {
                            $net_item_cost = $data['cost'];
                            $item_tax = 0;
                        }

                        $subtotal = (($net_item_cost * $pr_attr['quantity']) + $item_tax);
                        $item = array(
                            'product_id' => $product_id,
                            'product_code' => $data['code'],
                            'product_name' => $data['name'],
                            'net_unit_cost' => $net_item_cost,
                            'unit_cost' => $unit_cost,
                            'quantity' => $pr_attr['quantity'],
                            'option_id' => $option_id,
                            'quantity_balance' => $pr_attr['quantity'],
                            'item_tax' => $item_tax,
                            'tax_rate_id' => $tax_rate_id,
                            'tax' => $tax,
                            'subtotal' => $subtotal,
                            'warehouse_id' => $variant_warehouse_id,
                            'date' => date('Y-m-d'),
                            'status' => 'received',
                        );
                        $this->db->insert('purchase_items', $item);
                    }

                    $this->site->syncVariantQty($option_id, $variant_warehouse_id);
                }
            }

            if ($photos) {
                foreach ($photos as $photo) {
                    $this->db->insert('product_photos', array('product_id' => $product_id, 'photo' => $photo));
                }
            }

            return true;
        }

        return false;
    }

    public function getPrductVariantByPIDandName($product_id, $name) {
        $q = $this->db->get_where('product_variants', array('product_id' => $product_id, 'name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addAjaxProduct($data) {

        if ($this->db->insert('products', $data)) {
            $product_id = $this->db->insert_id();
            return $this->getProductByID($product_id);
        }

        return false;
    }

    public function add_products($products = array()) {
        if (!empty($products)) {
            foreach ($products as $product) {
                $variants = explode('|', $product['variants']);
                unset($product['variants']);
                if ($this->db->insert('products', $product)) {
                    $product_id = $this->db->insert_id();
                    foreach ($variants as $variant) {
                        if ($variant && trim($variant) != '') {
                            $vat = array('product_id' => $product_id, 'name' => trim($variant));
                            $this->db->insert('product_variants', $vat);
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    public function getProductNames($term, $limit = 5) {
        $this->db->select('' . $this->db->dbprefix('products') . '.id, code, ' . $this->db->dbprefix('products') . '.name as name, ' . $this->db->dbprefix('products') . '.price as price, ' . $this->db->dbprefix('product_variants') . '.name as vname')
                ->where("type != 'combo' AND "
                        . "(" . $this->db->dbprefix('products') . ".name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR
                concat(" . $this->db->dbprefix('products') . ".name, ' (', code, ')') LIKE '%" . $term . "%')");
        $this->db->join('product_variants', 'product_variants.product_id=products.id', 'left')
                ->where('' . $this->db->dbprefix('product_variants') . '.name', NULL)
                ->group_by('products.id')->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getProductNamesCombo($term, $get, $limit = 5) {

        unset($get['term']);
        $this->db->select('' . $this->db->dbprefix('products') . '.id, code, ' . $this->db->dbprefix('products') . '.name as name, ' . $this->db->dbprefix('products') . '.price as price, ' . $this->db->dbprefix('product_variants') . '.name as vname')
                ->where("type != 'combo' AND "
                        . "(" . $this->db->dbprefix('products') . ".name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR
                concat(" . $this->db->dbprefix('products') . ".name, ' (', code, ')') LIKE '%" . $term . "%')");
        $this->db->join('product_variants', 'product_variants.product_id=products.id', 'left')
                ->where('' . $this->db->dbprefix('product_variants') . '.name', NULL)
                ->where($get)
                ->group_by('products.id')->limit($limit);
        $q = $this->db->get('products');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function updateProduct($id, $data, $items, $warehouse_qty, $product_attributes, $photos, $update_variants) {
//        echo "<pre>";
//        print_r($warehouse_qty);
//        echo "</pre>";
//        die();
        if ($this->db->update('products', $data, array('id' => $id))) {

            if ($items) {
                $this->db->delete('combo_items', array('product_id' => $id));
                foreach ($items as $item) {
                    $item['product_id'] = $id;
                    $this->db->insert('combo_items', $item);
                }
            }

            $tax_rate = $this->site->getTaxRateByID($data['tax_rate']);

//            if ($warehouse_qty && !empty($warehouse_qty)) {
//                foreach ($warehouse_qty as $wh_qty) {
//                    $this->db->update('warehouses_products', array('rack' => $wh_qty['rack']), array('product_id' => $id, 'warehouse_id' => $wh_qty['warehouse_id']));
//                }
//            }


            if ($update_variants) {
                $this->db->update_batch('product_variants', $update_variants, 'id');
            }


            if ($photos) {
                foreach ($photos as $photo) {
                    $this->db->insert('product_photos', array('product_id' => $id, 'photo' => $photo));
                }
            }


            if ($product_attributes) {
                foreach ($product_attributes as $pr_attr) {

                    $pr_attr['product_id'] = $id;
                    $variant_warehouse_id = $pr_attr['warehouse_id'];
                    unset($pr_attr['warehouse_id']);
                    $this->db->insert('product_variants', $pr_attr);
                    $option_id = $this->db->insert_id();

                    if ($pr_attr['quantity'] != 0) {
                        $this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $id, 'warehouse_id' => $variant_warehouse_id, 'quantity' => $pr_attr['quantity']));

                        $tax_rate_id = $tax_rate ? $tax_rate->id : NULL;
                        $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : NULL;
                        $unit_cost = $data['cost'];
                        if ($tax_rate) {
                            if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                                if ($data['tax_method'] == '0') {
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                    $net_item_cost = $data['cost'] - $pr_tax_val;
                                    $item_tax = $pr_tax_val * $pr_attr['quantity'];
                                } else {
                                    $net_item_cost = $data['cost'];
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                    $unit_cost = $data['cost'] + $pr_tax_val;
                                    $item_tax = $pr_tax_val * $pr_attr['quantity'];
                                }
                            } else {
                                $net_item_cost = $data['cost'];
                                $item_tax = $tax_rate->rate;
                            }
                        } else {
                            $net_item_cost = $data['cost'];
                            $item_tax = 0;
                        }

                        $subtotal = (($net_item_cost * $pr_attr['quantity']) + $item_tax);
                        $item = array(
                            'product_id' => $id,
                            'product_code' => $data['code'],
                            'product_name' => $data['name'],
                            'net_unit_cost' => $net_item_cost,
                            'unit_cost' => $unit_cost,
                            'quantity' => $pr_attr['quantity'],
                            'option_id' => $option_id,
                            'quantity_balance' => $pr_attr['quantity'],
                            'item_tax' => $item_tax,
                            'tax_rate_id' => $tax_rate_id,
                            'tax' => $tax,
                            'subtotal' => $subtotal,
                            'warehouse_id' => $variant_warehouse_id,
                            'date' => date('Y-m-d'),
                            'status' => 'received',
                        );
                        $this->db->insert('purchase_items', $item);
                    }
                }
            }


//            $this->site->syncQuantity(NULL, NULL, NULL, $id);
            return true;
        } else {
            return false;
        }
    }

    public function updateProductOptionQuantity($option_id, $warehouse_id, $quantity, $product_id) {
        if ($option = $this->getProductWarehouseOptionQty($option_id, $warehouse_id)) {
            if ($this->db->update('warehouses_products_variants', array('quantity' => $quantity), array('option_id' => $option_id, 'warehouse_id' => $warehouse_id))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return TRUE;
            }
        } else {
            if ($this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $quantity))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return TRUE;
            }
        }
        return FALSE;
    }

    public function getPurchasedItemDetails($product_id, $warehouse_id, $option_id = NULL) {
        $q = $this->db->get_where('purchase_items', array('product_id' => $product_id, 'option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPurchasedItemDetailsWithOption($product_id, $warehouse_id, $option_id) {
        $q = $this->db->get_where('purchase_items', array('product_id' => $product_id, 'purchase_id' => NULL, 'transfer_id' => NULL, 'warehouse_id' => $warehouse_id, 'option_id' => $option_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updatePrice($data = array()) {

        if ($this->db->update_batch('products', $data, 'code')) {
            return true;
        } else {
            return false;
        }
    }

    public function deleteProduct($id) {
        if ($this->db->delete('products', array('id' => $id)) && $this->db->delete('warehouses_products', array('product_id' => $id)) && $this->db->delete('warehouses_products_variants', array('product_id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteQuoteProduct($id) {
        if ($this->db->delete('sma_quotes_products', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteProductChallan($id) {
        if ($this->db->delete('product_challan', array('challan_no' => $id)) && $this->db->delete('product_challan_items', array('challan_no' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function totalCategoryProducts($category_id) {
        $q = $this->db->get_where('products', array('category_id' => $category_id));

        return $q->num_rows();
    }

    public function getSubcategoryByID($id) {
        $q = $this->db->get_where('subcategories', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getCategoryByCode($code) {
        $q = $this->db->get_where('categories', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getSubcategoryByCode($code) {

        $q = $this->db->get_where('subcategories', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getTaxRateByName($name) {
        $q = $this->db->get_where('tax_rates', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getSubCategories() {
        $this->db->select('id as id, name as text');
        $q = $this->db->get("subcategories");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return FALSE;
    }

    public function getSubCategoriesForCategoryID($category_id) {
        $this->db->select('id as id, name as text');
        $q = $this->db->get_where("subcategories", array('category_id' => $category_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return FALSE;
    }

    public function getSubCategoriesByCategoryID($category_id) {
        $q = $this->db->get_where("subcategories", array('category_id' => $category_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return FALSE;
    }

    public function getAdjustmentByID($id) {
        $q = $this->db->get_where('adjustments', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function syncAdjustment($data = array()) {
        if (!empty($data)) {

            if ($purchase_item = $this->getPurchasedItemDetails($data['product_id'], $data['warehouse_id'], $data['option_id'])) {
                $quantity_balance = $data['type'] == 'subtraction' ? $purchase_item->quantity_balance - $data['quantity'] : $purchase_item->quantity_balance + $data['quantity'];
                $this->db->update('purchase_items', array('quantity_balance' => $quantity_balance), array('id' => $purchase_item->id));
            } else {
                $pr = $this->site->getProductByID($data['product_id']);
                $item = array(
                    'product_id' => $data['product_id'],
                    'product_code' => $pr->code,
                    'product_name' => $pr->name,
                    'net_unit_cost' => 0,
                    'unit_cost' => 0,
                    'quantity' => 0,
                    'option_id' => $data['option_id'],
                    'quantity_balance' => $data['type'] == 'subtraction' ? (0 - $data['quantity']) : $data['quantity'],
                    'item_tax' => 0,
                    'tax_rate_id' => 0,
                    'tax' => '',
                    'subtotal' => 0,
                    'warehouse_id' => $data['warehouse_id'],
                    'date' => date('Y-m-d'),
                    'status' => 'received',
                );
                $this->db->insert('purchase_items', $item);
            }

            $this->site->syncProductQty($data['product_id'], $data['warehouse_id']);
            if ($data['option_id']) {
                $this->site->syncVariantQty($data['option_id'], $data['warehouse_id'], $data['product_id']);
            }
        }
    }

    public function reverseAdjustment($id) {
        if ($adjustment = $this->getAdjustmentByID($id)) {

            if ($purchase_item = $this->getPurchasedItemDetails($adjustment->product_id, $adjustment->warehouse_id, $adjustment->option_id)) {
                $quantity_balance = $adjustment->type == 'subtraction' ? $purchase_item->quantity_balance + $adjustment->quantity : $purchase_item->quantity_balance - $adjustment->quantity;
                $this->db->update('purchase_items', array('quantity_balance' => $quantity_balance), array('id' => $purchase_item->id));
            }

            $this->site->syncProductQty($adjustment->product_id, $adjustment->warehouse_id);
            if ($adjustment->option_id) {
                $this->site->syncVariantQty($adjustment->option_id, $adjustment->warehouse_id, $adjustment->product_id);
            }
        }
    }

    public function addAdjustment($data) {
        if ($this->db->insert('adjustments', $data)) {
            $this->syncAdjustment($data);
            return true;
        }
        return false;
    }

    public function updateAdjustment($id, $data) {
        $this->reverseAdjustment($id);
        if ($this->db->update('adjustments', $data, array('id' => $id))) {
            $this->syncAdjustment($data);
            return true;
        }
        return false;
    }

    public function deleteAdjustment($id) {
        $this->reverseAdjustment($id);
        if ($this->db->delete('adjustments', array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function getProductQuantity($product_id, $warehouse) {
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse), 1);
        if ($q->num_rows() > 0) {
            return $q->row_array(); //$q->row();
        }
        return FALSE;
    }

    public function addQuantity($product_id, $warehouse_id, $quantity, $rack = NULL) {

        if ($this->getProductQuantity($product_id, $warehouse_id)) {
            if ($this->updateQuantity($product_id, $warehouse_id, $quantity, $rack)) {
                return TRUE;
            }
        } else {
            if ($this->insertQuantity($product_id, $warehouse_id, $quantity, $rack)) {
                return TRUE;
            }
        }

        return FALSE;
    }

    public function insertQuantity($product_id, $warehouse_id, $quantity, $rack = NULL) {
        if ($this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $quantity, 'rack' => $rack))) {
            $this->site->syncProductQty($product_id, $warehouse_id);
            return true;
        }
        return false;
    }

    public function updateQuantity($product_id, $warehouse_id, $quantity, $rack = NULL) {
        $data = $rack ? array('quantity' => $quantity, 'rack' => $rack) : $data = array('quantity' => $quantity);
        if ($this->db->update('warehouses_products', $data, array('product_id' => $product_id, 'warehouse_id' => $warehouse_id))) {
            $this->site->syncProductQty($product_id, $warehouse_id);
            return true;
        }
        return false;
    }

    public function products_count($type = null, $barcode = null) {
        if ($type) {
            $this->db->where('type', $type);
        }
        if ($barcode != "") {
            $this->db->like('code', $barcode);
        }
        $this->db->from('products');
        return $this->db->count_all_results();
    }

    public function fetch_products($type, $limit, $start, $barcode = null) {
        $this->db->limit($limit, $start);
        if ($type) {
            $this->db->where('type', $type);
        }
        $this->db->order_by("id", "asc");
        if ($barcode != "") {
            $this->db->like('code', $barcode);
        }
        $query = $this->db->get("products");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getProductWarehouseOptionQty($option_id, $warehouse_id) {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function syncVariantQty($option_id) {
        $wh_pr_vars = $this->getProductWarehouseOptions($option_id);
        $qty = 0;
        foreach ($wh_pr_vars as $row) {
            $qty += $row->quantity;
        }
        if ($this->db->update('product_variants', array('quantity' => $qty), array('id' => $option_id))) {
            return TRUE;
        }
        return FALSE;
    }

    public function getProductWarehouseOptions($option_id) {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function setRack($data) {
        if ($this->db->update('warehouses_products', array('rack' => $data['rack']), array('product_id' => $data['product_id'], 'warehouse_id' => $data['warehouse_id']))) {
            return TRUE;
        }
        return FALSE;
    }

    public function getSoldQty($id) {
        $this->db->select("date_format(" . $this->db->dbprefix('sales') . ".date, '%Y-%M') month, SUM( " . $this->db->dbprefix('sale_items') . ".quantity ) as sold, SUM( " . $this->db->dbprefix('sale_items') . ".subtotal ) as amount")
                ->from('sales')
                ->join('sale_items', 'sales.id=sale_items.sale_id', 'left')
                ->group_by("date_format(" . $this->db->dbprefix('sales') . ".date, '%Y-%m')")
                ->where($this->db->dbprefix('sale_items') . '.product_id', $id)
                //->where('DATE(NOW()) - INTERVAL 1 MONTH')
                ->where('DATE_ADD(curdate(), INTERVAL 1 MONTH)')
                ->order_by("date_format(" . $this->db->dbprefix('sales') . ".date, '%Y-%m') desc")->limit(3);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPurchasedQty($id) {
        $this->db->select("date_format(" . $this->db->dbprefix('purchases') . ".date, '%Y-%M') month, SUM( " . $this->db->dbprefix('purchase_items') . ".quantity ) as purchased, SUM( " . $this->db->dbprefix('purchase_items') . ".subtotal ) as amount")
                ->from('purchases')
                ->join('purchase_items', 'purchases.id=purchase_items.purchase_id', 'left')
                ->group_by("date_format(" . $this->db->dbprefix('purchases') . ".date, '%Y-%m')")
                ->where($this->db->dbprefix('purchase_items') . '.product_id', $id)
                //->where('DATE(NOW()) - INTERVAL 1 MONTH')
                ->where('DATE_ADD(curdate(), INTERVAL 1 MONTH)')
                ->order_by("date_format(" . $this->db->dbprefix('purchases') . ".date, '%Y-%m') desc")->limit(3);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllVariants() {
        $q = $this->db->get('variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getMargin() {
        $_POST = $_GET;
//        $this->db->select('discount');
//        $q = $this->db->get_where('product_discount', array('store_id' => $_POST['companies'],
//            'department_id' => $_POST['department'],
//            'section_id' => $_POST['section'],
//            'product_item_id' => $_POST['product_items'],
//            'type_id' => $_POST['type'],
//            'brands_id' => $_POST['brands'],
//            'status' => 'Active',
//        ));
//
//        $this->db->select('margin');
//        $margin = $this->db->get_where('product_margin', array('store_id' => $_POST['companies'],
//            'department_id' => $_POST['department'],
//            'section_id' => $_POST['section'],
//            'product_item_id' => $_POST['product_items'],
//            'type_id' => $_POST['type'],
//            'brands_id' => $_POST['brands'],
//            'status' => 'Active',
//        ));

        $margin = $this->db->select('margin')
                        ->where('department_id', $_POST['department'])
                        ->where('section_id', $_POST['section'])
                        ->where('product_item_id', $_POST['product_items'])
                        ->where('type_id', $_POST['type'])
                        ->where('brands_id', $_POST['brands'])
                        ->where('status', 'Active')
                        ->from('product_margin')
                        ->get()->result();
        $discount = $this->db->select('discount')
                        ->where('department_id', $_POST['department'])
                        ->where('section_id', $_POST['section'])
                        ->where('product_item_id', $_POST['product_items'])
                        ->where('type_id', $_POST['type'])
                        ->where('brands_id', $_POST['brands'])
                        ->where('status', 'Active')
                        ->from('product_discount')
                        ->get()->result();

        $rs = array('margin' => $margin[0]->margin, 'discount' => $discount[0]->discount);

        return $rs;
    }

    public function deleteParcelRecv($id) {
        if ($this->db->delete('product_received_voucher', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteParcelSend($id) {
        if ($this->db->delete('parcel_sent_voucher', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getviewParcelRev($id) {
        $com = $this->db->dbprefix('companies');
        $tab1 = $this->db->dbprefix('product_received_voucher');
        $this->db->select("product_received_voucher.id,companies.name as sender,product_received_voucher.sender_address,product_received_voucher.transport,product_received_voucher.veh_no,product_received_voucher.hamal,product_received_voucher.lr_no,product_received_voucher.bk_date,cc.name as receiver, product_received_voucher.goods_desc, product_received_voucher.parcel_type, product_received_voucher.delivery");
        $this->db->join('companies', 'companies.id = product_received_voucher.sender');
        $this->db->join('companies cc', 'cc.id = product_received_voucher.receiver');
        $this->db->where('product_received_voucher.id', $id);
        $rs = $this->db->get('product_received_voucher')->row();
    }

    public function getviewParcelSend($id) {
        $com = $this->db->dbprefix('companies');
        $tab1 = $this->db->dbprefix('product_received_voucher');
        $this->db->select("parcel_sent_voucher.id,cc.name as receiver,parcel_sent_voucher.receiver_address,parcel_sent_voucher.transport,parcel_sent_voucher.veh_no,parcel_sent_voucher.through as hamal,parcel_sent_voucher.lr_no,parcel_sent_voucher.book_date, parcel_sent_voucher.goods_desc, parcel_sent_voucher.parcel_type,companies.name as store");
        $this->db->join('companies', 'companies.id = parcel_sent_voucher.booked_by', "left");
        $this->db->join('companies cc', 'cc.id = parcel_sent_voucher.receiver', "left");
        $this->db->where('parcel_sent_voucher.id', $id);
        return $this->db->get('parcel_sent_voucher')->row();
    }

    public function getColorCode($id) {
        $this->db->select('name, code');
        $this->db->where('id', $id);
        $rs = $this->db->get('color')->row();
        return $rs;
    }

    public function getcolor($id) {
        $res = $this->db->get_where('sma_color', array('id' => $id))->row();
        return $res->code;
    }

    function getProductIds($id = null) {
        if ($id == null) {
            $term = $this->input->get('term', TRUE);
        } else {
            $term = $id;
        }
        $rows = $this->site->getProductIds($term);
        return $rows;
//        $rows['results'] = $this->site->getProductIds($term);
//        echo json_encode($rows);
    }

    public function getOffers() {
        $rs = $this->db->select('*')
                        ->where('status', 'Active')
                        ->from('offers')
                        ->get()->result();
        return $rs;
    }

    public function getCoupons() {
        $rs = $this->db->select('sma_coupon.*,sma_offers.offer_name')
                        ->where('sma_coupon.status', 'Active')
                        ->join('sma_offers', 'sma_offers.id = sma_coupon.offer')
                        ->from('sma_coupon')
                        ->get()->result();
        return $rs;
    }

    public function getProductsFromBarcode() {
        $rs = $this->db->select('*')
                        ->from('products')
                        ->get()->result();
        return $rs;
    }

// public function updateSeasonalDiscount($id, $data) {
//        if ($this->db->update('product_seasonal_discount', $data, array('id' => $id))) {
//            return true;
//        }
//        return false;
//    }

    public function check_exist() {
        $data = array(
            'store' => $this->input->post('store_id') != "" ? $this->input->post('store_id') : $_GET['store_id'],
            'department' => $this->input->post('department') != "" ? $this->input->post('department') : $_GET['department'],
            'product_item' => $this->input->post('product_items') != "" ? $this->input->post('product_items') : $_GET['product_items'],
            'section' => $this->input->post('section') != "" ? $this->input->post('section') : $_GET['section'],
            'type' => $this->input->post('type_id') != "" ? $this->input->post('type_id') : $_GET['type_id'],
            'brand' => $this->input->post('brands') != "" ? $this->input->post('brands') : $_GET['brands'],
            'design' => $this->input->post('design') != "" ? $this->input->post('design') : $_GET['design'],
            'style' => $this->input->post('style') != "" ? $this->input->post('style') : $_GET['style'],
            'pattern' => $this->input->post('pattern') != "" ? $this->input->post('pattern') : $_GET['pattern'],
            'fitting' => $this->input->post('fitting') != "" ? $this->input->post('fitting') : $_GET['fitting'],
            'fabric' => $this->input->post('fabric') != "" ? $this->input->post('fabric') : $_GET['fabric'],
        );
        $rs = $this->db->get_where('product_box', $data);
        if ($rs->num_rows() > 0) {
            return $rs->row();
        } else {
            return FALSE;
        }
    }

    public function getSec_quentity() {

        $data = array(
            'store' => $this->input->post('store_id') != "" ? $this->input->post('store_id') : $this->input->get('store_id'),
            'department' => $this->input->post('department') != "" ? $this->input->post('department') : $this->input->get('dept'),
            'product_item' => $this->input->post('product_items') != "" ? $this->input->post('product_items') : $this->input->get('product_item'),
            'section' => $this->input->post('section') != "" ? $this->input->post('section') : $this->input->get('section_id'),
            'type' => $this->input->post('type_id') != "" ? $this->input->post('type_id') : $this->input->get('type'),
            'brand' => $this->input->post('brands') != "" ? $this->input->post('brands') : $this->input->get('brands_id'),
            'design' => $this->input->post('design') != "" ? $this->input->post('design') : $this->input->get('design'),
            'style' => $this->input->post('style') != "" ? $this->input->post('style') : $this->input->get('style'),
            'pattern' => $this->input->post('pattern') != "" ? $this->input->post('pattern') : $this->input->get('pattern'),
            'fitting' => $this->input->post('fitting') != "" ? $this->input->post('fitting') : $this->input->get('fitting'),
            'fabric' => $this->input->post('fabric') != "" ? $this->input->post('fabric') : $this->input->get('fabric'),
        );
        $rs = $this->db->get_where('product_box', $data);
        if ($rs->num_rows() > 0) {
            return $rs->row();
        } else {
            return FALSE;
        }
    }

    public function save_box() {
        $noofpic = intval($this->input->post('p_quantity')) / intval($this->input->post('squantity'));
        $data = array(
            'store' => $this->input->post('store_id'),
            'department' => $this->input->post('department'),
            'product_item' => $this->input->post('product_items'),
            'section' => $this->input->post('section'),
            'type' => $this->input->post('type_id'),
            'brand' => $this->input->post('brands'),
            'design' => $this->input->post('design'),
            'style' => $this->input->post('style'),
            'pattern' => $this->input->post('pattern'),
            'fitting' => $this->input->post('fitting'),
            'fabric' => $this->input->post('fabric'),
            'no_of_pic' => $noofpic,
            'no_of_box' => 1,
        );
        $rs = $this->db->insert('product_box', $data);
        if ($rs) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function save_box_using_get() {
        $noofpic = intval($this->input->get('p_quantity')) / intval($this->input->get('squantity'));
        $data = array(
            'store' => $this->input->get('store_id'),
            'department' => $this->input->get('department'),
            'product_item' => $this->input->get('product_items'),
            'section' => $this->input->get('section'),
            'type' => $this->input->get('type_id'),
            'brand' => $this->input->get('brands'),
            'design' => $this->input->get('design'),
            'style' => $this->input->get('style'),
            'pattern' => $this->input->get('pattern'),
            'fitting' => $this->input->get('fitting'),
            'fabric' => $this->input->get('fabric'),
            'no_of_pic' => $noofpic,
            'no_of_box' => 1,
        );
        $rs = $this->db->insert('product_box', $data);
        if ($rs) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getboxbyid($id) {
//       return $this->db->get_where('product_box', array('id', $id))->row();
        $q = $this->db->get_where('product_box', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addProductWithoutBarcode($data, $items, $warehouse_qty, $product_attributes, $photos) {
        $this->db->insert('purchase_return_product_without_barcode', $data);

        $product_id = $this->db->insert_id();
        $this->site->updateReference('br');
        if ($items) {
            foreach ($items as $item) {
                $item['product_id'] = $product_id;
                $this->db->insert('combo_items', $item);
            }
        }

        if ($data['type'] == 'combo' || $data['type'] == 'bundle') {
            $warehouses = $this->site->getAllWarehouses();
            foreach ($warehouses as $warehouse) {
                $this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse->id, 'quantity' => 0));
            }
        }
        $tax_rate = $this->site->getTaxRateByID($data['tax_rate']);
        if ($warehouse_qty && !empty($warehouse_qty)) {
            foreach ($warehouse_qty as $wh_qty) {
                if (isset($wh_qty['quantity']) && !empty($wh_qty['quantity'])) {
                    $this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $wh_qty['warehouse_id'], 'quantity' => $wh_qty['quantity'], 'rack' => $wh_qty['rack']));
                    if (!$product_attributes) {
                        $tax_rate_id = $tax_rate ? $tax_rate->id : NULL;
                        $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : NULL;
                        $unit_cost = $data['cost'];
                        if ($tax_rate) {
                            if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                                if ($data['tax_method'] == '0') {
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                    $net_item_cost = $data['cost'] - $pr_tax_val;
                                    $item_tax = $pr_tax_val * $wh_qty['quantity'];
                                } else {
                                    $net_item_cost = $data['cost'];
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                    $unit_cost = $data['cost'] + $pr_tax_val;
                                    $item_tax = $pr_tax_val * $wh_qty['quantity'];
                                }
                            } else {
                                $net_item_cost = $data['cost'];
                                $item_tax = $tax_rate->rate;
                            }
                        } else {
                            $net_item_cost = $data['cost'];
                            $item_tax = 0;
                        }

                        $subtotal = (($net_item_cost * $wh_qty['quantity']) + $item_tax);

                        $item = array(
                            'product_id' => $product_id,
                            'product_code' => $data['code'],
                            'product_name' => $data['name'],
                            'net_unit_cost' => $net_item_cost,
                            'unit_cost' => $unit_cost,
                            'quantity' => $wh_qty['quantity'],
                            'quantity_balance' => $wh_qty['quantity'],
                            'item_tax' => $item_tax,
                            'tax_rate_id' => $tax_rate_id,
                            'tax' => $tax,
                            'subtotal' => $subtotal,
                            'warehouse_id' => $wh_qty['warehouse_id'],
                            'date' => date('Y-m-d'),
                            'status' => 'received',
                        );
                        $this->db->insert('purchase_items', $item);
                        $this->site->syncProductQty($product_id, $wh_qty['warehouse_id']);
                    }
                }
            }
        }

        if ($product_attributes) {
            foreach ($product_attributes as $pr_attr) {
                $pr_attr_details = $this->getPrductVariantByPIDandName($product_id, $pr_attr['name']);

                $pr_attr['product_id'] = $product_id;
                $variant_warehouse_id = $pr_attr['warehouse_id'];
                unset($pr_attr['warehouse_id']);
                if ($pr_attr_details) {
                    $option_id = $pr_attr_details->id;
                } else {
                    $this->db->insert('product_variants', $pr_attr);
                    $option_id = $this->db->insert_id();
                }
                if ($pr_attr['quantity'] != 0) {
                    $this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $variant_warehouse_id, 'quantity' => $pr_attr['quantity']));

                    $tax_rate_id = $tax_rate ? $tax_rate->id : NULL;
                    $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : NULL;
                    $unit_cost = $data['cost'];
                    if ($tax_rate) {
                        if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                            if ($data['tax_method'] == '0') {
                                $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                $net_item_cost = $data['cost'] - $pr_tax_val;
                                $item_tax = $pr_tax_val * $pr_attr['quantity'];
                            } else {
                                $net_item_cost = $data['cost'];
                                $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                $unit_cost = $data['cost'] + $pr_tax_val;
                                $item_tax = $pr_tax_val * $pr_attr['quantity'];
                            }
                        } else {
                            $net_item_cost = $data['cost'];
                            $item_tax = $tax_rate->rate;
                        }
                    } else {
                        $net_item_cost = $data['cost'];
                        $item_tax = 0;
                    }

                    $subtotal = (($net_item_cost * $pr_attr['quantity']) + $item_tax);
                    $item = array(
                        'product_id' => $product_id,
                        'product_code' => $data['code'],
                        'product_name' => $data['name'],
                        'net_unit_cost' => $net_item_cost,
                        'unit_cost' => $unit_cost,
                        'quantity' => $pr_attr['quantity'],
                        'option_id' => $option_id,
                        'quantity_balance' => $pr_attr['quantity'],
                        'item_tax' => $item_tax,
                        'tax_rate_id' => $tax_rate_id,
                        'tax' => $tax,
                        'subtotal' => $subtotal,
                        'warehouse_id' => $variant_warehouse_id,
                        'date' => date('Y-m-d'),
                        'status' => 'received',
                    );
                    $this->db->insert('purchase_items_without_barcode', $item);
                }

                $this->site->syncVariantQty($option_id, $variant_warehouse_id);
            }
        }

        if ($photos) {
            foreach ($photos as $photo) {
                $this->db->insert('product_photos', array('product_id' => $product_id, 'photo' => $photo));
            }
        }

        return $product_id;
    }

    public function addUnlabelledProduct($data, $items, $warehouse_qty, $product_attributes, $photos) {
        $this->db->insert('unlabelled_products', $data);
        
        $product_id = $this->db->insert_id();
        $this->site->updateReference('br');
        if ($items) {
            foreach ($items as $item) {
                $item['product_id'] = $product_id;
                $this->db->insert('combo_items', $item);
            }
        }
        if ($data['type'] == 'combo' || $data['type'] == 'bundle') {
            $warehouses = $this->site->getAllWarehouses();
            foreach ($warehouses as $warehouse) {
                $this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse->id, 'quantity' => 0));
            }
        }
        $tax_rate = $this->site->getTaxRateByID($data['tax_rate']);
        if ($warehouse_qty && !empty($warehouse_qty)) {
            foreach ($warehouse_qty as $wh_qty) {
                if (isset($wh_qty['quantity']) && !empty($wh_qty['quantity'])) {
                    $this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $wh_qty['warehouse_id'], 'quantity' => $wh_qty['quantity'], 'rack' => $wh_qty['rack']));
                    if (!$product_attributes) {
                        $tax_rate_id = $tax_rate ? $tax_rate->id : NULL;
                        $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : NULL;
                        $unit_cost = $data['cost'];
                        if ($tax_rate) {
                            if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                                if ($data['tax_method'] == '0') {
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                    $net_item_cost = $data['cost'] - $pr_tax_val;
                                    $item_tax = $pr_tax_val * $wh_qty['quantity'];
                                } else {
                                    $net_item_cost = $data['cost'];
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                    $unit_cost = $data['cost'] + $pr_tax_val;
                                    $item_tax = $pr_tax_val * $wh_qty['quantity'];
                                }
                            } else {
                                $net_item_cost = $data['cost'];
                                $item_tax = $tax_rate->rate;
                            }
                        } else {
                            $net_item_cost = $data['cost'];
                            $item_tax = 0;
                        }

                        $subtotal = (($net_item_cost * $wh_qty['quantity']) + $item_tax);

                        $item = array(
                            'product_id' => $product_id,
                            'product_code' => $data['code'],
                            'product_name' => $data['name'],
                            'net_unit_cost' => $net_item_cost,
                            'unit_cost' => $unit_cost,
                            'quantity' => $wh_qty['quantity'],
                            'quantity_balance' => $wh_qty['quantity'],
                            'item_tax' => $item_tax,
                            'tax_rate_id' => $tax_rate_id,
                            'tax' => $tax,
                            'subtotal' => $subtotal,
                            'warehouse_id' => $wh_qty['warehouse_id'],
                            'date' => date('Y-m-d'),
                            'status' => 'received',
                        );
                        $this->db->insert('purchase_items', $item);
                        $this->site->syncProductQty($product_id, $wh_qty['warehouse_id']);
                    }
                }
            }
        }

        if ($product_attributes) {
            foreach ($product_attributes as $pr_attr) {
                $pr_attr_details = $this->getPrductVariantByPIDandName($product_id, $pr_attr['name']);

                $pr_attr['product_id'] = $product_id;
                $variant_warehouse_id = $pr_attr['warehouse_id'];
                unset($pr_attr['warehouse_id']);
                if ($pr_attr_details) {
                    $option_id = $pr_attr_details->id;
                } else {
                    $this->db->insert('product_variants', $pr_attr);
                    $option_id = $this->db->insert_id();
                }
                if ($pr_attr['quantity'] != 0) {
                    $this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $variant_warehouse_id, 'quantity' => $pr_attr['quantity']));

                    $tax_rate_id = $tax_rate ? $tax_rate->id : NULL;
                    $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : NULL;
                    $unit_cost = $data['cost'];
                    if ($tax_rate) {
                        if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                            if ($data['tax_method'] == '0') {
                                $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                $net_item_cost = $data['cost'] - $pr_tax_val;
                                $item_tax = $pr_tax_val * $pr_attr['quantity'];
                            } else {
                                $net_item_cost = $data['cost'];
                                $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                $unit_cost = $data['cost'] + $pr_tax_val;
                                $item_tax = $pr_tax_val * $pr_attr['quantity'];
                            }
                        } else {
                            $net_item_cost = $data['cost'];
                            $item_tax = $tax_rate->rate;
                        }
                    } else {
                        $net_item_cost = $data['cost'];
                        $item_tax = 0;
                    }

                    $subtotal = (($net_item_cost * $pr_attr['quantity']) + $item_tax);
                    $item = array(
                        'product_id' => $product_id,
                        'product_code' => $data['code'],
                        'product_name' => $data['name'],
                        'net_unit_cost' => $net_item_cost,
                        'unit_cost' => $unit_cost,
                        'quantity' => $pr_attr['quantity'],
                        'option_id' => $option_id,
                        'quantity_balance' => $pr_attr['quantity'],
                        'item_tax' => $item_tax,
                        'tax_rate_id' => $tax_rate_id,
                        'tax' => $tax,
                        'subtotal' => $subtotal,
                        'warehouse_id' => $variant_warehouse_id,
                        'date' => date('Y-m-d'),
                        'status' => 'received',
                    );
                    $this->db->insert('purchase_items_without_barcode', $item);
                }

                $this->site->syncVariantQty($option_id, $variant_warehouse_id);
            }
        }

        if ($photos) {
            foreach ($photos as $photo) {
                $this->db->insert('product_photos', array('product_id' => $product_id, 'photo' => $photo));
            }
        }

        return $product_id;
    }

    public function addOrderProduct($data, $items, $warehouse_qty, $product_attributes, $photos) {


        $data['code'] = $data['code'] . '_';
        $this->db->insert('sma_quotes_products', $data);
        $product_id = $this->db->insert_id();
        // echo $product_id."asd"; exit;
        $this->site->updateReference('br');
        if ($items) {
            foreach ($items as $item) {
                $item['product_id'] = $product_id;
                $this->db->insert('combo_items', $item);
            }
        }
        $tax_rate = $this->site->getTaxRateByID($data['tax_rate']);
        if ($product_attributes) {
            foreach ($product_attributes as $pr_attr) {
                $pr_attr_details = $this->getPrductVariantByPIDandName($product_id, $pr_attr['name']);
                $pr_attr['product_id'] = $product_id;
                $variant_warehouse_id = $pr_attr['warehouse_id'];
                unset($pr_attr['warehouse_id']);
                if ($pr_attr_details) {
                    $option_id = $pr_attr_details->id;
                } else {
                    $this->db->insert('product_variants', $pr_attr);
                    $option_id = $this->db->insert_id();
                }
                if ($pr_attr['quantity'] != 0) {
                    $this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $variant_warehouse_id, 'quantity' => $pr_attr['quantity']));

                    $tax_rate_id = $tax_rate ? $tax_rate->id : NULL;
                    $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : NULL;
                    $unit_cost = $data['cost'];
                    if ($tax_rate) {
                        if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                            if ($data['tax_method'] == '0') {
                                $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                $net_item_cost = $data['cost'] - $pr_tax_val;
                                $item_tax = $pr_tax_val * $pr_attr['quantity'];
                            } else {
                                $net_item_cost = $data['cost'];
                                $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                $unit_cost = $data['cost'] + $pr_tax_val;
                                $item_tax = $pr_tax_val * $pr_attr['quantity'];
                            }
                        } else {
                            $net_item_cost = $data['cost'];
                            $item_tax = $tax_rate->rate;
                        }
                    } else {
                        $net_item_cost = $data['cost'];
                        $item_tax = 0;
                    }

                    $subtotal = (($net_item_cost * $pr_attr['quantity']) + $item_tax);
                    $item = array(
                        'product_id' => $product_id,
                        'product_code' => $data['code'],
                        'product_name' => $data['name'],
                        'net_unit_cost' => $net_item_cost,
                        'unit_cost' => $unit_cost,
                        'quantity' => $pr_attr['quantity'],
                        'option_id' => $option_id,
                        'quantity_balance' => $pr_attr['quantity'],
                        'item_tax' => $item_tax,
                        'tax_rate_id' => $tax_rate_id,
                        'tax' => $tax,
                        'subtotal' => $subtotal,
                        'warehouse_id' => $variant_warehouse_id,
                        'date' => date('Y-m-d'),
                        'status' => 'received',
                    );
                    $this->db->insert('purchase_items_without_barcode', $item);
                }
                $this->site->syncVariantQty($option_id, $variant_warehouse_id);
            }
        }
        if ($photos) {
            foreach ($photos as $photo) {
                $this->db->insert('product_photos', array('product_id' => $product_id, 'photo' => $photo));
            }
        }
        return $product_id;
    }

    public function addProductPurchase($data, $items, $warehouse_qty, $product_attributes, $photos) {
        if ($this->db->insert('products', $data)) {
            $product_id = $this->db->insert_id();

            $this->site->updateReference('br');
            if ($items) {
                foreach ($items as $item) {
                    $item['product_id'] = $product_id;
                    $this->db->insert('combo_items', $item);
                }
            }

            if ($data['type'] == 'combo' || $data['type'] == 'bundle') {
                $warehouses = $this->site->getAllWarehouses();
                foreach ($warehouses as $warehouse) {
                    $this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse->id, 'quantity' => 0));
                }
            }
            $tax_rate = $this->site->getTaxRateByID($data['tax_rate']);
            if ($warehouse_qty && !empty($warehouse_qty)) {
                foreach ($warehouse_qty as $wh_qty) {
                    if (isset($wh_qty['quantity']) && !empty($wh_qty['quantity'])) {
                        $this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $wh_qty['warehouse_id'], 'quantity' => $wh_qty['quantity'], 'rack' => $wh_qty['rack']));
                        if (!$product_attributes) {
                            $tax_rate_id = $tax_rate ? $tax_rate->id : NULL;
                            $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : NULL;
                            $unit_cost = $data['cost'];
                            if ($tax_rate) {
                                if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                                    if ($data['tax_method'] == '0') {
                                        $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                        $net_item_cost = $data['cost'] - $pr_tax_val;
                                        $item_tax = $pr_tax_val * $wh_qty['quantity'];
                                    } else {
                                        $net_item_cost = $data['cost'];
                                        $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                        $unit_cost = $data['cost'] + $pr_tax_val;
                                        $item_tax = $pr_tax_val * $wh_qty['quantity'];
                                    }
                                } else {
                                    $net_item_cost = $data['cost'];
                                    $item_tax = $tax_rate->rate;
                                }
                            } else {
                                $net_item_cost = $data['cost'];
                                $item_tax = 0;
                            }

                            $subtotal = (($net_item_cost * $wh_qty['quantity']) + $item_tax);

                            $item = array(
                                'product_id' => $product_id,
                                'product_code' => $data['code'],
                                'product_name' => $data['name'],
                                'net_unit_cost' => $net_item_cost,
                                'unit_cost' => $unit_cost,
                                'quantity' => $wh_qty['quantity'],
                                'quantity_balance' => $wh_qty['quantity'],
                                'item_tax' => $item_tax,
                                'tax_rate_id' => $tax_rate_id,
                                'tax' => $tax,
                                'subtotal' => $subtotal,
                                'warehouse_id' => $wh_qty['warehouse_id'],
                                'date' => date('Y-m-d'),
                                'status' => 'received',
                            );
                            $this->db->insert('purchase_items', $item);
                            $this->site->syncProductQty($product_id, $wh_qty['warehouse_id']);
                        }
                    }
                }
            }

            if ($product_attributes) {
                foreach ($product_attributes as $pr_attr) {
                    $pr_attr_details = $this->getPrductVariantByPIDandName($product_id, $pr_attr['name']);

                    $pr_attr['product_id'] = $product_id;
                    $variant_warehouse_id = $pr_attr['warehouse_id'];
                    unset($pr_attr['warehouse_id']);
                    if ($pr_attr_details) {
                        $option_id = $pr_attr_details->id;
                    } else {
                        $this->db->insert('product_variants', $pr_attr);
                        $option_id = $this->db->insert_id();
                    }
                    if ($pr_attr['quantity'] != 0) {
                        $this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $variant_warehouse_id, 'quantity' => $pr_attr['quantity']));

                        $tax_rate_id = $tax_rate ? $tax_rate->id : NULL;
                        $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : NULL;
                        $unit_cost = $data['cost'];
                        if ($tax_rate) {
                            if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                                if ($data['tax_method'] == '0') {
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                    $net_item_cost = $data['cost'] - $pr_tax_val;
                                    $item_tax = $pr_tax_val * $pr_attr['quantity'];
                                } else {
                                    $net_item_cost = $data['cost'];
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                    $unit_cost = $data['cost'] + $pr_tax_val;
                                    $item_tax = $pr_tax_val * $pr_attr['quantity'];
                                }
                            } else {
                                $net_item_cost = $data['cost'];
                                $item_tax = $tax_rate->rate;
                            }
                        } else {
                            $net_item_cost = $data['cost'];
                            $item_tax = 0;
                        }

                        $subtotal = (($net_item_cost * $pr_attr['quantity']) + $item_tax);
                        $item = array(
                            'product_id' => $product_id,
                            'product_code' => $data['code'],
                            'product_name' => $data['name'],
                            'net_unit_cost' => $net_item_cost,
                            'unit_cost' => $unit_cost,
                            'quantity' => $pr_attr['quantity'],
                            'option_id' => $option_id,
                            'quantity_balance' => $pr_attr['quantity'],
                            'item_tax' => $item_tax,
                            'tax_rate_id' => $tax_rate_id,
                            'tax' => $tax,
                            'subtotal' => $subtotal,
                            'warehouse_id' => $variant_warehouse_id,
                            'date' => date('Y-m-d'),
                            'status' => 'received',
                        );
                        $this->db->insert('purchase_items', $item);
                    }

                    $this->site->syncVariantQty($option_id, $variant_warehouse_id);
                }
            }

            if ($photos) {
                foreach ($photos as $photo) {
                    $this->db->insert('product_photos', array('product_id' => $product_id, 'photo' => $photo));
                }
            }
            return $product_id;
        }
        return false;
    }

    public function updateSecQty($id, $data) {
        if ($this->db->update('product_box', $data, array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getOrderProductByID($id) {
        $this->db->select('sma_quotes_products.*,sma_design.name as design_name');
        $this->db->join('sma_design', 'quotes_products.design = sma_design.id');
        $q = $this->db->get_where('sma_quotes_products', array('sma_quotes_products.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getUnlabelledProductByID($id) {
        $sp = $this->db->dbprefix('unlabelled_products');
        $q = $this->db->select($sp . '.*,sma_brands.name as brandname,  sma_color.name as colorname,sma_design.name as designname, sma_style.name as stylename, sma_pattern.name as patternname,sma_fitting.name as fittingname, sma_fabric.name as fabricname ,IF(' . $sp . '.singlerate!= "",' . $sp . '.singlerate, IF(' . $sp . '.mrprate!= "" ,' . $sp . '.mrprate,  CONCAT(' . $sp . '.mulratef, " - ", ' . $sp . '.mulratet))) AS rate, sma_per.name as uname', FALSE)
                ->join('sma_brands', "sma_brands.id =  $sp.brands", 'left')
                ->join('sma_color', "sma_color.id =  $sp.color", 'left')
                ->join('sma_design', "sma_design.id =  $sp.design", 'left')
                ->join('sma_style', "sma_style.id =  $sp.style", 'left')
                ->join('sma_pattern', "sma_pattern.id =  $sp.pattern", 'left')
                ->join('sma_fitting', "sma_fitting.id =  $sp.fitting", 'left')
                ->join('sma_fabric', "sma_fabric.id =  $sp.fabric", 'left')
                ->join('sma_per', "sma_per.id =  $sp.sunit", 'left')
                ->from('sma_unlabelled_products')
                ->where("$sp.id", $id)
                ->get();
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

//    public function getUnlabelledProductByID($id) {
//        $q = $this->db->get_where('unlabelled_products', array('id' => $id), 1);
//        if ($q->num_rows() > 0) {
//            return $q->row();
//        }
//        return FALSE;
//    }

    public function getOrderProductByIDPurchase($id) {
        $q = $this->db->get_where('sma_purchase_return_product_without_barcode', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getOrderProductByIDPurchaseDetails($id) {
        $sp = $this->db->dbprefix('purchase_return_product_without_barcode');

        $q = $this->db->select($sp . '.*,sma_brands.name as brandname,  sma_color.name as colorname,sma_design.name as designname, sma_style.name as stylename, sma_pattern.name as patternname,sma_fitting.name as fittingname, sma_fabric.name as fabricname ,IF(' . $sp . '.singlerate!= "",' . $sp . '.singlerate, IF(' . $sp . '.mrprate!= "" ,' . $sp . '.mrprate,  CONCAT(' . $sp . '.mulratef, " - ", ' . $sp . '.mulratet))) AS rate, sma_per.name as uname', FALSE)
                ->join('sma_brands', "sma_brands.id =  $sp.brands", 'left')
                ->join('sma_color', "sma_color.id =  $sp.color", 'left')
                ->join('sma_design', "sma_design.id =  $sp.design", 'left')
                ->join('sma_style', "sma_style.id =  $sp.style", 'left')
                ->join('sma_pattern', "sma_pattern.id =  $sp.pattern", 'left')
                ->join('sma_fitting', "sma_fitting.id =  $sp.fitting", 'left')
                ->join('sma_fabric', "sma_fabric.id =  $sp.fabric", 'left')
                ->join('sma_per', "sma_per.id =  $sp.sunit", 'left')
                ->from('sma_purchase_return_product_without_barcode')
                ->where("$sp.id", $id)
                ->get();
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

//    public function getProductsFromReference($ref) {
//        $rs = $this->db->select('sma_quote_items.*')
//                ->join('sma_quotes','sma_quotes.id = sma_quote_items.quote_id')
//                ->where('sma_quotes.reference_no',$ref)
//                ->from('sma_quote_items')->get()->result();
//        if($rs){
//        return $rs;
//        }else{
//            return false;
//        }
//    }
    public function getProductsFromReference($ref) {
        $rs = $this->db->select('sma_quotes.id as quid,sma_quotes.reference_no as refn, sma_quotes_products.*,sma_quote_items.quantity, sma_brands.name as brandname,  sma_color.name as colorname,sma_design.name as designname, sma_style.name as stylename, sma_pattern.name as patternname,sma_fitting.name as fittingname, sma_fabric.name as fabricname ,IF(sma_quotes_products.singlerate!= "",sma_quotes_products.singlerate, IF(sma_quotes_products.mrprate!= "" ,sma_quotes_products.mrprate,  CONCAT(sma_quotes_products.mulratef, " - ", sma_quotes_products.mulratet))) AS rate, sma_per.name as uname', FALSE)
                        ->join('sma_quote_items', 'sma_quote_items.product_code = sma_quotes_products.code')
                        ->join('sma_quotes', 'sma_quotes.id = sma_quote_items.quote_id')
                        ->join('sma_brands', 'sma_brands.id =  sma_quotes_products.brands', 'left')
                        ->join('sma_color', 'sma_color.id =  sma_quotes_products.color', 'left')
                        ->join('sma_design', 'sma_design.id =  sma_quotes_products.design', 'left')
                        ->join('sma_style', 'sma_style.id =  sma_quotes_products.style', 'left')
                        ->join('sma_pattern', 'sma_pattern.id =  sma_quotes_products.pattern', 'left')
                        ->join('sma_fitting', 'sma_fitting.id =  sma_quotes_products.fitting', 'left')
                        ->join('sma_fabric', 'sma_fabric.id =  sma_quotes_products.fabric', 'left')
                        ->join('sma_per', 'sma_per.id =  sma_quotes_products.sunit', 'left')
                        ->order_by('sma_quotes_products.gst', 'left')
                        ->where('sma_quotes.reference_no', $ref)
                        ->from('sma_quotes_products')->get()->result();
        if ($rs) {
            return $rs;
        } else {
            return false;
        }
    }

    public function getProductsFromReference12($ref) {
        $rs = $this->db->select('sma_quotes_products.*,sma_quote_items.quantity')
                        ->join('sma_quote_items', 'sma_quote_items.product_code = sma_quotes_products.code')
                        ->join('sma_quotes', 'sma_quotes.id = sma_quote_items.quote_id')
                        ->where('sma_quotes.reference_no', $ref)
                        ->from('sma_quotes_products')->get()->result();

        if ($rs) {

            return $rs;
        } else {
            return false;
        }
    }

    public function getProductsFromSupBill($ref) {
        $rs = $this->db->select('sma_products.*, sma_brands.name as brandname, sma_color.name as colorname, sma_design.name as designname, sma_style.name as stylename, sma_pattern.name as patternname, sma_fitting.name as fittingname, sma_fabric.name as fabricname, IF(sma_products.singlerate!= "", sma_products.singlerate, IF(sma_products.mrprate!= "", sma_products.mrprate, CONCAT(sma_products.mulratef, " - ", sma_products.mulratet))) AS rate, sma_per.name as uname', FALSE)
                        ->join('sma_purchase_items', 'sma_purchase_items.product_id = sma_products.id')
                        ->join('sma_purchases', 'sma_purchases.id    = sma_purchase_items.purchase_id')
                        ->join('sma_brands', 'sma_brands.id =  sma_products.brands', 'left')
                        ->join('sma_color', 'sma_color.id =  sma_products.color', 'left')
                        ->join('sma_design', 'sma_design.id =  sma_products.design', 'left')
                        ->join('sma_style', 'sma_style.id =  sma_products.style', 'left')
                        ->join('sma_pattern', 'sma_pattern.id =  sma_products.pattern', 'left')
                        ->join('sma_fitting', 'sma_fitting.id =  sma_products.fitting', 'left')
                        ->join('sma_fabric', 'sma_fabric.id =  sma_products.fabric', 'left')
                        ->join('sma_per', 'sma_per.id =  sma_products.sunit', 'left')
                        ->where('sma_purchases.reference_no', $ref)
//                        ->from('sma_products')->get();
//                        echo $this->db->last_query();
                        ->from('sma_products')->get()->result();
        if ($rs) {
            return $rs;
        } else {
            return false;
        }
    }

//    public function getProductsFromSupBill($ref) {
//        $rs = $this->db->select('sma_quotes_products.*, sma_brands.name as brandname,  sma_color.name as colorname,sma_design.name as designname, sma_style.name as stylename, sma_pattern.name as patternname,sma_fitting.name as fittingname, sma_fabric.name as fabricname ,IF(sma_quotes_products.singlerate!= "",sma_quotes_products.singlerate, IF(sma_quotes_products.mrprate!= "" ,sma_quotes_products.mrprate,  CONCAT(sma_quotes_products.mulratef, " - ", sma_quotes_products.mulratet))) AS rate, sma_per.name as uname', FALSE)
//                        ->join('sma_purchase_items', 'sma_purchase_items.product_code = sma_quotes_products.code')
//                        ->join('sma_purchases', 'sma_purchases.id = sma_purchase_items.purchase_id')
//                        ->join('sma_brands', 'sma_brands.id =  sma_quotes_products.brands')
//                        ->join('sma_color', 'sma_color.id =  sma_quotes_products.color')
//                        ->join('sma_design', 'sma_design.id =  sma_quotes_products.design')
//                        ->join('sma_style', 'sma_style.id =  sma_quotes_products.style')
//                        ->join('sma_pattern', 'sma_pattern.id =  sma_quotes_products.pattern')
//                        ->join('sma_fitting', 'sma_fitting.id =  sma_quotes_products.fitting')
//                        ->join('sma_fabric', 'sma_fabric.id =  sma_quotes_products.fabric')
//                        ->join('sma_per', 'sma_per.id =  sma_quotes_products.sunit')
//                        ->where('sma_purchases.reference_no', $ref)
////                        ->from('sma_quotes_products')->get();
//                        ->from('sma_quotes_products')->get()->result();
//        if ($rs) {
//            return $rs;
//        } else {
//            return false;
//        }
//    }
//    public function getProductsFromSupBill($ref) {
//        $rs = $this->db->select('sma_quotes_products.*,sma_purchase_items.quantity')
//                        ->join('sma_purchase_items', 'sma_purchase_items.product_code = sma_quotes_products.code')
//                        ->join('sma_purchases', 'sma_purchases.id = sma_purchase_items.purchase_id')
//                        ->where('sma_purchases.reference_no', $ref)
//                        ->from('sma_quotes_products')->get()->result();
//        if ($rs) {
//            return $rs;
//        } else {
//            return false;
//        }
//    }

    public function getPurchaseFromSupBill($code) {
        $rs = $this->db->select('*')
                        ->where('reference_no', $code)
                        ->from('sma_purchases')->get()->row();
        return $rs;
    }

    public function getOrdProductsFromBarcode($code) {
        $rs = $this->db->select('sma_quotes_products.*')
                        ->where('sma_quotes_products.code', $code)
                        ->from('sma_quotes_products')->get()->result();
        if ($rs) {
            return $rs;
        } else {
            return false;
        }
    }

    public function getOrderProductByIDs($id) {

        $this->db->select('sma_quotes_products.*,sma_design.name as design_name,  sma_brands.name as brandname,  sma_color.name as colorname,sma_design.name as designname, sma_style.name as stylename, sma_pattern.name as patternname,sma_fitting.name as fittingname, sma_fabric.name as fabricname ,IF(sma_quotes_products.singlerate!= "",sma_quotes_products.singlerate, IF(sma_quotes_products.mrprate!= "" ,sma_quotes_products.mrprate,  CONCAT(sma_quotes_products.mulratef, " - ", sma_quotes_products.mulratet))) AS rate, sma_per.name as uname', FALSE);
//        $this->db->join('sma_design', 'quotes_products.design = sma_design.id')
        $this->db->join('sma_brands', 'sma_brands.id =  sma_quotes_products.brands')
                ->join('sma_color', 'sma_color.id =  sma_quotes_products.color')
                ->join('sma_design', 'sma_design.id =  sma_quotes_products.design', 'left')
                ->join('sma_style', 'sma_style.id =  sma_quotes_products.style', 'left')
                ->join('sma_pattern', 'sma_pattern.id =  sma_quotes_products.pattern', 'left')
                ->join('sma_fitting', 'sma_fitting.id =  sma_quotes_products.fitting', 'left')
                ->join('sma_fabric', 'sma_fabric.id =  sma_quotes_products.fabric', 'left')
                ->join('sma_per', 'sma_per.id =  sma_quotes_products.sunit', 'left');
        $q = $this->db->get_where('sma_quotes_products', array('sma_quotes_products.id' => $id), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductsFromBarcodes($barcode) {
        $rs = $this->db->select('*')
                        ->from('products')
                        ->where('code', $barcode)
                        ->get()->row();
        return $rs;
    }

    public function getProductSizeAndQty($barcode) {
        $data = $this->getProductsFromBarcodes($barcode);
        $parm = array(
            'store_id' => $data->store_id,
            'department' => $data->department,
            'section' => $data->section,
            'product_items' => $data->product_items,
            'type_id' => $data->type_id,
            'brands' => $data->brands,
            'design' => $data->design,
            'style' => $data->style,
            'pattern' => $data->pattern,
            'fitting' => $data->fitting,
            'fabric' => $data->fabric,
            'color' => $data->color,
            'unit' => $data->unit,
        );
        $g = array(
            'size', 'color'
        );

        $this->db->group_by($g);
        $this->db->select('sma_products.*, size.name as prosize, sum(quantity) Qty', FALSE);
        $this->db->join('size', 'size.id =sma_products.size');
        $q = $this->db->get_where('sma_products', $parm);
//        echo $this->db->last_query();
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function getCouponsCurrnet() {
        $rs = $this->db->select('sma_coupon.*,sma_offers.offer_name')
                        ->where('sma_coupon.status', 'Active')
//                        ->where('sma_coupon.start_date >', date('Y-m-d 23:59:00'))
//                        ->where('sma_coupon.end_date <', date('Y-m-d 23:59:00'))
                        ->join('sma_offers', 'sma_offers.id = sma_coupon.offer')
                        ->from('sma_coupon')
                        ->get()->result();
//        echo $this->db->last_query();
        return $rs;
    }

    public function productAllDiscount($id) {

        $p = $this->db->dbprefix('products');
        $pd = $this->db->dbprefix('product_discount');
        $purItem = $this->db->dbprefix('purchase_items');
        $pur = $this->db->dbprefix('purchases');
        $data = array();

        $r = $this->db->select("$pd.discount")
                        ->from("$p")
                        ->join($pd, "$p.store_id=$pd.store_id AND $p.department=$pd.department_id AND $p.section=$pd.section_id AND  $p.product_items=$pd.product_item_id AND $p.type_id = $pd.type_id AND $p.brands= $pd.brands_id")
                        ->where("$p.id", $id)
                        ->get()->row();
        if ($r) {
            $data['product_discount'] = $r->discount . '%';
        }

        $purchase = $this->db->select("$pur.*,$purItem.product_id,$purItem.product_code,$purItem.product_name")
                        ->from("$purItem")
                        ->join($pur, "$pur.id= $purItem.purchase_id")
                        ->where("$purItem.product_id", $id)
                        ->get()->row();

        if ($purchase) {
            $data['purchase_discount'] = $purchase->order_discount_id;
        }

//        echo '<pre>';
//        print_r($purchase);
//        print_r($data);
//        echo '</pre>';
//        die();
    }

}
