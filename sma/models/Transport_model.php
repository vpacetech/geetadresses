<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Transport_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function create_transport($data) {
        $data['create_date'] = date('Y-m-d h:i:s');
        $rs = $this->db->insert('transport', $data);
      
        if ($rs) { return true;} else {return false;}
    }
    public function getTransportData($id){
        $rs = $this->db->select('*')
                ->where('id',$id)
                ->from('transport')
                ->get()->result();
        return $rs[0];
    }
public function update_transport($id,$data){
   $this->db->where('id', $id);
        if ($this->db->update('transport', $data)) {
            return true;
        }
        return false;
}

public function deleteTransport($id){
        if ($this->db->delete('transport', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }
    
    public function getLatestTransport(){
        $rs = $this->db->select('id')
                ->order_by('id','desc')
                ->from('transport')
                ->limit('1')
                ->get()->result();
        $r = $rs[0]->id + 1000;
        return $r;
    }
}
