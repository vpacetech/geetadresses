<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Companies_model extends CI_Model {

    

    public function __construct() {
        parent::__construct();
        // $this->$_supplier_data = array();

    }

    public function getAllBillerCompanies() {
        $q = $this->db->get_where('companies', array('group_name' => 'biller', 'status' => 'Active'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllCustomerCompanies() {
        $q = $this->db->get_where('companies', array('group_name' => 'customer'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllCustomerActiveCompanies() {
        $q = $this->db->get_where('companies', array('group_name' => 'customer', 'status' => 'Active'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllUserCompanies() {
        $rs = $this->db->select('users.*')
                        ->join('groups', 'users.group_id=groups.id')
                        ->join('companies', 'companies.id = users.biller_id')
                        ->group_by('users.id')
                        ->where('company_id', NULL)
                        ->where('active', 1)
                        ->from('users')->get()->result();

        if ($rs) {
            return $rs;
        } else {
            return FALSE;
        }
    }

//    public function getAllUsers() {
//        $rs = $this->db->select('*')
//                        ->where('active', 1)
//                        ->where('emplyee_id !=', null)
//                        ->from('users')->get()->result();
//        if ($rs) {
//            return $rs;
//        } else {
//            return FALSE;
//        }
//    }
    public function getAllUsers() {
        $rs = $this->db->select('users.*, employee.fname as first_name, employee.lname as last_name')
                        ->where('users.active', 1)
                        ->where('employee.status', 'Active')
                        ->from('users')
                        ->join('employee', 'employee.id =users.emplyee_id')->get()->result();
 
        if ($rs) {
            return $rs;
        } else {
            return FALSE;
        }
    }

    public function getAllSupplierCompanies() {
        $q = $this->db->get_where('companies', array('group_name' => 'supplier', 'status' => 'Active'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllCustomerGroups() {
        $q = $this->db->get('customer_groups');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCompanyUsers($company_id) {
        $q = $this->db->get_where('users', array('company_id' => $company_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCompanyByID($id) {
        $q = $this->db->get_where('companies', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    
    public function getDepartmentByID($id) {
        $q = $this->db->get_where('department', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCompanyByEmail($email) {
        $q = $this->db->get_where('companies', array('email' => $email), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    
    public function addCompany($data = array()) {

        
        if ($this->db->insert('companies', $data)) {
            // $cid = $this->db->insert_id();
            // echo "<pre>";print_r($this->db->last_query());exit;
            $cid = 1;
            return true;
        }
        // echo "<pre>";print_r($this->db->last_query());exit;
        // echo "<pre>";print_r($this->db->last_query());exit;
        return false;
    }

    public function addDepartment($data = array()) {

        
        if ($this->db->insert('department', $data)) {
            // $cid = $this->db->insert_id();
            $cid = 1;
            return true;
        }
        // echo "<pre>";print_r($this->db->last_query());exit;
        return false;
    }

    public function getSection($data = array()) {

        return $this->db->select('name,id')->get('section')->result();
    }

    public function savenewsupplier_general($post)
    {   


        // $_SESSION['sid'] = $this->db->select('supplier_id')->get('supplierinfo')->row();

        // echo "<pre>";print_r($s_id);
        // print_r($_SESSION['sid']);


        
        if( $post['IGST'] == 'on' )
        {
            $post['IGST'] = 1;
        }
        else
        {
            $post['IGST'] = 0;
        }
        
        // echo "<pre>";print_r($post);exit;
        $supplier_general = array(
            // 'supplier_id' => $_SESSION['sid'],
            'gstno' => $post['gstno'],
            'panno' => $post['panno'],
            'igst' => $post['IGST']
        );

        $_SESSION['supplier_data']['supplier_general'] = $supplier_general;

        // $this->supplier_data['supplier_general'] = $supplier_general;



        // $this->db->insert('suppliergeneral',$data);
        
        // return true;
    }
    
    public function savenewsupplier_supplierinfo($post)
    {
        if( $post['Preferred_Supplier'] == 'on' )
        {
            $post['Preferred_Supplier'] = 'yes';
            // $post['Is_Active'] = 1;
            
        }
        else
        {
            $post['Preferred_Supplier'] = 'no';
        }
        
        if( $post['Is_Active'] == 'on' )
        {
            $post['Is_Active'] = 'yes';
        }
        else
        {
            $post['Is_Active'] = 'no';
        }
        
        $supplier_info = array(
            'Supplier_Code'      =>     $post['Supplier_Code'],
            // 'supplier_id'        =>     $_SESSION['sid'],
            'Preferred_Supplier' =>     $post['Preferred_Supplier'],
            'Is_Active'          =>     $post['Is_Active'],
            'store'              =>     $post['store'],
            'Supplier_Name'      =>     $post['Supplier_Name'],
            'Supplier_Discount'  =>     $post['Supplier_Discount'],
            'aadhar'             =>     $post['aadhar'],
            'pan'                =>     $post['pan']
        );
        
        $_SESSION['supplier_data']['supplier_info'] = $supplier_info;

        // echo "<pre>";print_r($_SESSION['supplier_data']['supplier_info']['Supplier_Code']);

        // $this->supplier_data['supplier_info'] = $supplier_info;
        
        // echo "g";
        
        // $this->db->insert('supplieinfo',$data);
        
        
    }
    
    
    public function savenewsupplier_contact($post)
    {
        $supplier_contact = array(
            'first_Name'    =>  $post['first_Name'],
            // 'supplier_id'   =>  $_SESSION['sid'],
            'last_Name'     =>  $post['last_Name'],   
            'telephone_no'  =>  $post['telephone_no'],
            'fax_no'        =>  $post['fax_no'],
            'mobile_No'     =>  $post['mobile_No'],
            'website'       =>  $post['website'],
            'mail'          =>  $post['mail']
        );
        
        $_SESSION['supplier_data']['supplier_contact'] = $supplier_contact;

        // $this->supplier_data['supplier_contact'] = $supplier_contact;
        // $this->db->insert('suppliercontactinfo',$data);
        // echo "<pre>";print_r($this->supplier_data['$supplier_contact']);exit;
        
        
    }
    
    public function savenewsupplier_address($post)
    {
        $supplier_address = array(
            'Address1'      =>       $post['Address1'],
            // 'supplier_id'   =>       $_SESSION['sid'],
            'Address2'      =>       $post['Address2'],   
            'city'          =>       $post['city'],
            'state'         =>       $post['state'],
            'country'       =>       $post['country'],
            'Zip_Code'      =>       $post['Zip_Code']        
        );
        
        $_SESSION['supplier_data']['supplier_address'] = $supplier_address;

        // $this->supplier_data['supplier_address'] = $supplier_address;
        // echo "<pre>";print_r($this->supplier_data['$supplier_address']);exit;



        // $this->db->insert('supplieraddress',$data);
    }

    public function savenewsupplier_payment($post)
    {
        $supplier_payment = array(
            // 'supplier_id'  => $_SESSION['sid'],
            'payment_days'      =>       $post['payment']
        );
        
        $_SESSION['supplier_data']['supplier_payment'] = $supplier_payment;

        // $this->supplier_data['supplier_payment'] = $supplier_payment;
        // echo "<pre>";print_r($this->supplier_data['supplier_payment']);exit;



        // $this->db->insert('supplieraddress',$data);
    }

    public function savenewsupplier_referedby($post)
    {
        $supplier_reffer = array(
            'refered_by'                   =>       $post['refered'],
            // 'supplier_id'               =>       $_SESSION['sid'],
            'percentage_commision'      =>       $post['percentage_commision']
            
        );
        
        $_SESSION['supplier_data']['supplier_reffer'] = $supplier_reffer;

        // $this->supplier_data['supplier_reffer'] = $supplier_reffer;
        // echo "<pre>";print_r($_SESSION['supplier_data']['supplier_reffer']);

        // $this->db->insert('supplieraddress',$data);
    }

    public function savenewsupplier_photo($post)
    {
        $supplier_photo = array(
            // 'supplier_id' => $_SESSION['sid'],
            'photo'       => $post['photo']            
        );
        
        $_SESSION['supplier_data']['supplier_photo'] = $supplier_photo;

        // $this->supplier_data['supplier_photo'] = $supplier_photo;


        
        
        $this->db->insert('supplierinfo',   $_SESSION['supplier_data']['supplier_info']);
        
        $s_id = $this->db->select('supplier_id')->get('supplierinfo')->row();
        

        $_SESSION['supplier_data']['supplier_general']['supplier_id'] = $s_id->supplier_id;
        $_SESSION['supplier_data']['supplier_address']['supplier_id'] = $s_id->supplier_id;
        $_SESSION['supplier_data']['supplier_payment']['supplier_id'] = $s_id->supplier_id;
        $_SESSION['supplier_data']['supplier_contact']['supplier_id'] = $s_id->supplier_id;
        $_SESSION['supplier_data']['supplier_reffer']['supplier_id'] = $s_id->supplier_id;
        $_SESSION['supplier_data']['supplier_photo']['supplier_id'] = $s_id->supplier_id;
        
        echo "<pre>";print_r($_SESSION['supplier_data']['supplier_general']['supplier_id']);


        $this->db->insert('suppliergeneral',$_SESSION['supplier_data']['supplier_general']);
        $this->db->insert('supplieraddress',$_SESSION['supplier_data']['supplier_address']);
        $this->db->insert('supplierpayment',$_SESSION['supplier_data']['supplier_payment']);
        $this->db->insert('suppliercontact',$_SESSION['supplier_data']['supplier_contact']);
        $this->db->insert('supplierref',    $_SESSION['supplier_data']['supplier_reffer']);
        $this->db->insert('supplierphoto',  $_SESSION['supplier_data']['supplier_photo']);
        // $this->db->insert('supplieraddress',$data);

        unset($_SESSION['supplier_data']);
    }

    
    
    public function addBanks($data = array()) {
        if ($this->db->insert('user_banks', $data)) {
            $cid = $this->db->insert_id();
            return $cid;
        }
        return false;
    }

    public function add_customer($data = array()) {
        if ($this->db->insert('customerdetails', $data)) {
            $cid = $this->db->insert_id();
            return $cid;
        }
        return false;
    }

    public function update_customer($id, $data = array()) {
        $this->db->where('cust_id', $id);
        if ($this->db->update('customerdetails', $data)) {
            $cid = $this->db->insert_id();
            return $cid;
        }
        return false;
    }

    public function updateCompany($id, $data = array()) {
        $this->db->where('id', $id);
        if ($this->db->update('companies', $data)) {
            return true;
        }
        return false;
    }
    
    public function updateDepartment($id, $data = array()) {
        $this->db->where('id', $id);
        if ($this->db->update('department', $data)) {
            return true;
        }
        return false;
    }

    public function addCompanies($data = array()) {
        if ($this->db->insert_batch('companies', $data)) {
            return true;
        }
        return false;
    }

    public function deleteCustomer($id) {
        if ($this->getCustomerSales($id)) {
            return false;
        }
        if ($this->db->delete('companies', array('id' => $id, 'group_name' => 'customer')) && $this->db->delete('users', array('company_id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteSupplier($id) {
        if ($this->getSupplierPurchases($id)) {
            return false;
        }
        if ($this->db->delete('companies', array('id' => $id, 'group_name' => 'supplier')) && $this->db->delete('users', array('company_id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteBiller($id) {
        if ($this->getBillerSales($id)) {
            return false;
        }

        if ($this->db->delete('companies', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getBillerSuggestions($term, $limit = 10) {
        $this->db->select("id, company as text");
        $this->db->where(" (id LIKE '%" . $term . "%' OR name LIKE '%" . $term . "%' OR company LIKE '%" . $term . "%') ");
        $q = $this->db->get_where('companies', array('group_name' => 'biller'), $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getCustomerSuggestions($term, $limit = 10) {
        $this->db->select("id, CONCAT(company, ' (', name, ')') as text", FALSE);
        $this->db->where(" (id LIKE '%" . $term . "%' OR name LIKE '%" . $term . "%' OR company LIKE '%" . $term . "%' OR email LIKE '%" . $term . "%' OR phone LIKE '%" . $term . "%') ");
        $q = $this->db->get_where('companies', array('group_name' => 'customer', 'status' => 'Active'), $limit);

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getSupplierSuggestions($term, $limit = 10) {
//        CONCAT(name, ' ( ', code, ' )')
        $this->db->select("id, company as text", FALSE);
        $this->db->where('status', 'Active');
        $this->db->where(" (id LIKE '%" . $term . "%' OR name LIKE '%" . $term . "%' OR company LIKE '%" . $term . "%' OR email LIKE '%" . $term . "%' OR phone LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%') ");
        $q = $this->db->get_where('companies', array('group_name' => 'supplier'), $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getCustomerSales($id) {
        $this->db->where('customer_id', $id)->from('sales');
        return $this->db->count_all_results();
    }

    public function getBillerSales($id) {
        $this->db->where('biller_id', $id)->from('sales');
        return $this->db->count_all_results();
    }

    public function getSupplierPurchases($id) {
        $this->db->where('supplier_id', $id)->from('purchases');
        return $this->db->count_all_results();
    }

    public function getNewSupplierCode() {
        $rs = $this->db->select('max(id) as id')
                        ->from('companies')
                        ->where('group_name', 'supplier')
                        ->limit('1')
                        ->order_by('id', 'DESC')
                        ->get()->row();

        $rs->id = $rs->id + 1;

        $rs->id = rand(1000, 9999);
        return $rs;
    }

    public function getNewCustomerCode() {
        $rs = $this->db->select('max(id) as id')
                        ->from('companies')
                        ->where('group_name', 'customer')
                        ->limit('1')
                        ->order_by('id', 'DESC')
                        ->get()->row();
        $rs->id = $rs->id + 1;
        return $rs;
    }

    public function getCustomerByID($id) {
        $this->db->where('companies.id', $id);
        $this->db->select('companies.*, dateofbirth,anniversarydate,isloyatlity,loyalityno,enrolldate,referby,ispoint,billaddressline1,billaddressline2');
        $this->db->select('billcity,billstate,billcountry,billzipcode,billtelephoneno,billmobileno,billemail,enrolllimit,creaditlimit,fabalance,sincedate,loyalitypoint,createdby,loyalty_percent');
        $this->db->join('customerdetails', 'companies.id = customerdetails.cust_id', 'left');
        $data = $this->db->get('companies');
        if ($data->num_rows() > 0) {
            return $data->row();
        }
        return FALSE;
    }

    public function add_offer($data = array()) {
        if ($this->db->insert('offers', $data)) {
            $cid = $this->db->insert_id();
            return $cid;
        }
        return false;
    }

    public function update_offer($id, $data = array()) {
        $this->db->where('id', $id);
        if ($this->db->update('offers', $data)) {
            return TRUE;
        }
        return false;
    }

    public function getOfferByID($id) {
        $q = $this->db->get_where('offers', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

//    public function get_supplier_list($id) {
//        $q = $this->db->get_where('offers', array('id' => $id), 1);
//        if ($q->num_rows() > 0) {
//            return $q->row();
//        }
//        return FALSE;
//    }
}

//test