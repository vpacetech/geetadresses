<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Purchases_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function getProductNames($term, $limit = 5) {


        $this->db->select('sma_quotes_products.*,sma_design.name as design_name,  sma_brands.name as brandname,  sma_color.name as colorname,sma_design.name as designname, sma_style.name as stylename, sma_pattern.name as patternname,sma_fitting.name as fittingname, sma_fabric.name as fabricname ,IF(sma_quotes_products.singlerate!= "",sma_quotes_products.singlerate, IF(sma_quotes_products.mrprate!= "" ,sma_quotes_products.mrprate,  CONCAT(sma_quotes_products.mulratef, " - ", sma_quotes_products.mulratet))) AS rate, sma_per.name as uname', FALSE);
        $this->db->from('sma_quotes_products');
        $this->db->join('sma_brands', 'sma_brands.id = sma_quotes_products.brands')
                ->join('sma_color', 'sma_color.id = sma_quotes_products.color')
                ->join('sma_design', 'sma_design.id = sma_quotes_products.design')
                ->join('sma_style', 'sma_style.id = sma_quotes_products.style')
                ->join('sma_pattern', 'sma_pattern.id = sma_quotes_products.pattern')
                ->join('sma_fitting', 'sma_fitting.id = sma_quotes_products.fitting')
                ->join('sma_fabric', 'sma_fabric.id =  sma_quotes_products.fabric')
                ->join('sma_per', 'sma_per.id =  sma_quotes_products.sunit');

        $this->db->where("(sma_quotes_products.name LIKE '%" . $term . "%' OR sma_quotes_products.code LIKE '%" . $term . "%' OR  concat(sma_quotes_products.name, ' (', sma_quotes_products.code, ')') LIKE '%" . $term . "%')");
        $this->db->limit($limit);
        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

//    public function getProductNames($term, $limit = 5) {
//        $this->db->where("(name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR  concat(name, ' (', code, ')') LIKE '%" . $term . "%')");
//        $this->db->limit($limit);
////        $q = $this->db->get('products');
////        changed this on 28 aug 2017 to add products from orders only
//        $q = $this->db->get('quotes_products');
//
//        if ($q->num_rows() > 0) {
//            foreach (($q->result()) as $row) {
//                $data[] = $row;
//            }
//            return $data;
//        }
//        return FALSE;
//    }

    public function getProductNamesAccordingSupplier($term, $sup, $limit = 5) {
        $products = $this->db->dbprefix('products');
        $brands = $this->db->dbprefix('brands');
        $color = $this->db->dbprefix('color');
        $design = $this->db->dbprefix('design');
        $style = $this->db->dbprefix('style');
        $pattern = $this->db->dbprefix('pattern');
        $fitting = $this->db->dbprefix('fitting');
        $fabric = $this->db->dbprefix('fabric');
        $per = $this->db->dbprefix('per');
        $this->db->select("$products.*,$design.name as design_name,$brands.name as brandname,$color.name as colorname,$design.name as designname, $style.name as stylename, $pattern.name as patternname,$fitting.name as fittingname, $fabric.name as fabricname ,IF($products.singlerate!= '',$products.singlerate, IF($products.mrprate!= '' ,$products.mrprate,  CONCAT($products.mulratef, ' - ', $products.mulratet))) AS rate, $per.name as uname", FALSE);
        $this->db->from('products');
        $this->db->join('brands', 'brands.id = products.brands', 'LEFT')
                ->join('color', 'color.id = products.color', 'LEFT')
                ->join('design', 'design.id = products.design', 'LEFT')
                ->join('style', 'style.id = products.style', 'LEFT')
                ->join('pattern', 'pattern.id = products.pattern', 'LEFT')
                ->join('fitting', 'fitting.id = products.fitting', 'LEFT')
                ->join('fabric', 'fabric.id =  products.fabric', 'LEFT')
                ->join('per', 'per.id =  products.sunit', 'LEFT');
        $this->db->where("($products.name LIKE '%" . $term . "%' OR $products.code LIKE '%" . $term . "%' OR  concat($products.name, ' (', $products.code, ')') LIKE '%" . $term . "%')");
        $this->db->limit($limit);
        $this->db->where("$products.supplier1", $sup);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

//    public function getProductNamesAccordingSupplier($term, $sup, $limit = 5) {
//        $this->db->where("(name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR  concat(name, ' (', code, ')') LIKE '%" . $term . "%')");
//        $this->db->limit($limit);
//        $this->db->where('supplier1', $sup);
//        $q = $this->db->get('products');
//        if ($q->num_rows() > 0) {
//            foreach (($q->result()) as $row) {
//                $data[] = $row;
//            }
//            return $data;
//        }
//        return FALSE;
//    }

    public function getAllProducts() {
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductByID($id) {
        $q = $this->db->get_where('products', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getStoreByID($id) {
        $q = $this->db->get_where('products', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getStoreByFromPurchase($id) {
        $this->db->select('store_id');
        $q = $this->db->get_where('purchases', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductsByCode($code) {
        $this->db->select('*')->from('products')->like('code', $code, 'both');
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getProductByCode($code) {
        $q = $this->db->get_where('quotes_products', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            $q = $this->db->get_where('products', array('code' => $code), 1);
            if ($q->num_rows() > 0) {
                return $q->row();
            }
            return FALSE;
        }
        return FALSE;
    }

    public function getProductByCodeFromProducts($code) {
        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getUnlabelledProductByCode($code) {
        $q = $this->db->get_where('unlabelled_products', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductByName($name) {
//        $q = $this->db->get_where('products', array('name' => $name), 1);
        $q = $this->db->get_where('quotes_products', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateProductQuantity($product_id, $quantity, $warehouse_id, $product_cost) {
        if ($this->addQuantity($product_id, $warehouse_id, $quantity)) {
            $this->site->syncProductQty($product_id, $warehouse_id);
            return true;
        }
        return false;
    }

    public function calculateAndUpdateQuantity($item_id, $product_id, $quantity, $warehouse_id, $product_cost) {
        if ($this->updatePrice($product_id, $product_cost) && $this->calculateAndAddQuantity($item_id, $product_id, $warehouse_id, $quantity)) {
            return true;
        }
        return false;
    }

    public function calculateAndAddQuantity($item_id, $product_id, $warehouse_id, $quantity) {

        if ($this->getProductQuantity($product_id, $warehouse_id)) {
            $quantity_details = $this->getProductQuantity($product_id, $warehouse_id);
            $product_quantity = $quantity_details['quantity'];
            $item_details = $this->getItemByID($item_id);
            $item_quantity = $item_details->quantity;
            $after_quantity = $product_quantity - $item_quantity;
            $new_quantity = $after_quantity + $quantity;
            if ($this->updateQuantity($product_id, $warehouse_id, $new_quantity)) {
                return TRUE;
            }
        } else {

            if ($this->insertQuantity($product_id, $warehouse_id, $quantity)) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function addQuantity($product_id, $warehouse_id, $quantity) {

        if ($this->getProductQuantity($product_id, $warehouse_id)) {
            $warehouse_quantity = $this->getProductQuantity($product_id, $warehouse_id);
            $old_quantity = $warehouse_quantity['quantity'];
            $new_quantity = $old_quantity + $quantity;

            if ($this->updateQuantity($product_id, $warehouse_id, $new_quantity)) {
                return TRUE;
            }
        } else {

            if ($this->insertQuantity($product_id, $warehouse_id, $quantity)) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function insertQuantity($product_id, $warehouse_id, $quantity) {
        $productData = array(
            'product_id' => $product_id,
            'warehouse_id' => $warehouse_id,
            'quantity' => $quantity
        );
        if ($this->db->insert('warehouses_products', $productData)) {
            $this->site->syncProductQty($product_id, $warehouse_id);
            return true;
        }
        return false;
    }

    public function updateQuantity($product_id, $warehouse_id, $quantity) {
        if ($this->db->update('warehouses_products', array('quantity' => $quantity), array('product_id' => $product_id, 'warehouse_id' => $warehouse_id))) {
            $this->site->syncProductQty($product_id, $warehouse_id);
            return true;
        }
        return false;
    }

    public function getProductQuantity($product_id, $warehouse) {
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse), 1);
        if ($q->num_rows() > 0) {
            return $q->row_array(); //$q->row();
        }

        return FALSE;
    }

    public function updatePrice($id, $unit_cost) {

        if ($this->db->update('products', array('cost' => $unit_cost), array('id' => $id))) {
            return true;
        }

        return false;
    }

    public function getAllPurchases() {
        $q = $this->db->get('purchases');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

//    public function getAllPurchaseItems($purchase_id) {
//        $this->db->select('purchase_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, products.unit, products.details as details, product_variants.name as variant,products.squantity,products.sunit')
//                ->join('products', 'products.id=purchase_items.product_id', 'left')
//                ->join('product_variants', 'product_variants.id=purchase_items.option_id', 'left')
//                ->join('tax_rates', 'tax_rates.id=purchase_items.tax_rate_id', 'left')
//                ->group_by('purchase_items.id')
//                ->order_by('id', 'asc');
//        $q = $this->db->get_where('purchase_items', array('purchase_id' => $purchase_id));
//        if ($q->num_rows() > 0) {
//            foreach (($q->result()) as $row) {
//                $data[] = $row;
//            }
//           
//            return $data;
//        }
//        return FALSE;
//    }


    public function getAllPurchaseItems($purchase_id) {
        $this->db->select('purchase_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, quotes_products.unit, quotes_products.details as details, product_variants.name as variant,quotes_products.squantity,quotes_products.sunit,quotes.date as ordered_date, products.squantity as sqty')
                ->join('quotes_products', 'quotes_products.id=purchase_items.product_id', 'left')
                ->join('product_variants', 'product_variants.id=purchase_items.option_id', 'left')
                ->join('tax_rates', 'tax_rates.id=purchase_items.tax_rate_id', 'left')
                ->join('purchases', 'purchases.id = purchase_items.purchase_id')
                ->join('quotes', 'quotes.reference_no = purchases.ag_order_no', 'left')
                ->join('products', 'products.id = purchase_items.product_id')
                ->group_by('purchase_items.id')
                ->order_by('id', 'asc');

        $q = $this->db->get_where('purchase_items', array('purchase_id' => $purchase_id));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function getAllPurchaseReturnItems($purchase_id) {
        $this->db->select('sma_purchase_return_items.*, sma_tax_rates.code as tax_code, sma_tax_rates.name as tax_name, sma_tax_rates.rate as tax_rate, sma_products.unit, sma_products.details as details, sma_product_variants.name as variant,sma_products.squantity, sma_products.sunit, sma_brands.name as brandname,  sma_color.name as colorname,sma_design.name as designname, sma_style.name as stylename, sma_pattern.name as patternname,sma_fitting.name as fittingname, sma_fabric.name as fabricname, IF(sma_products.singlerate!= "",sma_products.singlerate, IF(sma_products.mrprate!= "" ,sma_products.mrprate, CONCAT(sma_products.mulratef, " - ", sma_products.mulratet))) AS rate, sma_per.name as uname', FALSE)
                ->join('sma_products', 'sma_products.id=sma_purchase_return_items.product_id', 'left')
                ->join('sma_product_variants', 'sma_product_variants.id=sma_purchase_return_items.option_id', 'left')
                ->join('sma_tax_rates', 'sma_tax_rates.id=sma_purchase_return_items.tax_rate_id', 'left')
                ->join('sma_brands', 'sma_brands.id =  sma_products.brands', 'left')
                ->join('sma_color', 'sma_color.id =  sma_products.color', 'left')
                ->join('sma_design', 'sma_design.id =  sma_products.design', 'left')
                ->join('sma_style', 'sma_style.id =  sma_products.style', 'left')
                ->join('sma_pattern', 'sma_pattern.id =  sma_products.pattern', 'left')
                ->join('sma_fitting', 'sma_fitting.id =  sma_products.fitting', 'left')
                ->join('sma_fabric', 'sma_fabric.id =  sma_products.fabric', 'left')
                ->join('sma_per', 'sma_per.id =  sma_products.sunit', 'left')
                ->group_by('purchase_return_items.id')
                ->order_by('purchase_return_items.id', 'asc');
        $q = $this->db->get_where('purchase_return_items', array('purchase_return_items.purchase_id' => $purchase_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

//    public function getAllPurchaseReturnItems($purchase_id) {
//        $this->db->select('purchase_return_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, products.unit, products.details as details, product_variants.name as variant,products.squantity,products.sunit')
//                ->join('products', 'products.id=purchase_return_items.product_id', 'left')
//                ->join('product_variants', 'product_variants.id=purchase_return_items.option_id', 'left')
//                ->join('tax_rates', 'tax_rates.id=purchase_return_items.tax_rate_id', 'left')
//                ->group_by('purchase_return_items.id')
//                ->order_by('id', 'asc');
//        $q = $this->db->get_where('purchase_return_items', array('purchase_id' => $purchase_id));
//        if ($q->num_rows() > 0) {
//            foreach (($q->result()) as $row) {
//                $data[] = $row;
//            }
//            return $data;
//        }
//        return FALSE;
//    }

    public function purchase_date($purchase_id) {
        $rs = $this->db->select('sma_purchases.date')
                        ->from('sma_purchases')
                        ->join('sma_purchases_return', 'sma_purchases_return.reference_no = sma_purchases.reference_no')
                        ->where('sma_purchases_return.id', $purchase_id)->get()->row();
        return $rs->date;
    }

    public function getAllInStockDamageItems($purchase_id) {
        $this->db->select('in_stock_damage_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, products.unit, products.details as details, product_variants.name as variant,products.squantity,products.sunit')
                ->join('products', 'products.id=in_stock_damage_items.product_id', 'left')
                ->join('product_variants', 'product_variants.id=in_stock_damage_items.option_id', 'left')
                ->join('tax_rates', 'tax_rates.id=in_stock_damage_items.tax_rate_id', 'left')
                ->group_by('in_stock_damage_items.id')
                ->order_by('id', 'asc');
        $q = $this->db->get_where('in_stock_damage_items', array('purchase_id' => $purchase_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllSupDamageItems($purchase_id) {
        $this->db->select('sup_damage_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, products.unit, products.details as details, product_variants.name as variant,products.squantity,products.sunit')
                ->join('products', 'products.id=sup_damage_items.product_id', 'left')
                ->join('product_variants', 'product_variants.id=sup_damage_items.option_id', 'left')
                ->join('tax_rates', 'tax_rates.id=sup_damage_items.tax_rate_id', 'left')
                ->group_by('sup_damage_items.id')
                ->order_by('id', 'asc');
        $q = $this->db->get_where('sup_damage_items', array('purchase_id' => $purchase_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllPurchaseReturnWithoutBarcodeItems($purchase_id) {
        $this->db->select('purchase_return_items_without_barcode.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, sma_purchase_return_product_without_barcode.unit, sma_purchase_return_product_without_barcode.details as details, product_variants.name as variant,sma_purchase_return_product_without_barcode.squantity,sma_purchase_return_product_without_barcode.sunit')
                ->join('sma_purchase_return_product_without_barcode', 'sma_purchase_return_product_without_barcode.id=purchase_return_items_without_barcode.product_id', 'left')
                ->join('product_variants', 'product_variants.id=purchase_return_items_without_barcode.option_id', 'left')
                ->join('tax_rates', 'tax_rates.id=purchase_return_items_without_barcode.tax_rate_id', 'left')
                ->group_by('purchase_return_items_without_barcode.id')
                ->order_by('id', 'asc');
        $q = $this->db->get_where('purchase_return_items_without_barcode', array('purchase_id' => $purchase_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getUnlabelledBarcodeItems($purchase_id) {
        $this->db->select('unlabelled_damage_return_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, unlabelled_products.unit, unlabelled_products.details as details, product_variants.name as variant,unlabelled_products.squantity,unlabelled_products.sunit')
                ->join('unlabelled_products', 'unlabelled_products.id=unlabelled_damage_return_items.product_id', 'left')
                ->join('product_variants', 'product_variants.id=unlabelled_damage_return_items.option_id', 'left')
                ->join('tax_rates', 'tax_rates.id=unlabelled_damage_return_items.tax_rate_id', 'left')
                ->group_by('unlabelled_damage_return_items.id')
                ->order_by('id', 'asc');
        $q = $this->db->get_where('unlabelled_damage_return_items', array('purchase_id' => $purchase_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getItemByID($id) {
        $q = $this->db->get_where('purchase_items', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTaxRateByName($name) {
        $q = $this->db->get_where('tax_rates', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPurchaseByID($id) {
        $q = $this->db->get_where('purchases', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getLrNoByPurchaseID($id) {
        $q = $this->db->get_where('purchases_lrno', array('purchase_id' => $id));
        if ($q->num_rows() > 0) {
            return $q->result_array();
        }
        return FALSE;
    }

    public function getAgOrderNoByPurchaseID($id) {
        $q = $this->db->get_where('purchases_ag_order', array('purchase_id' => $id));
        if ($q->num_rows() > 0) {
            return $q->result_array();
        }
        return FALSE;
    }

    public function getPurchaseReturnByID($id) {
        $q = $this->db->get_where('purchases_return', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getInStockDamageByID($id) {
        $q = $this->db->get_where('in_stock_damage', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSupDamageByID($id) {
        $q = $this->db->get_where('sup_damage', array('id' => $id), 1);
        if ($q->num_rows() > 0) {

            return $q->row();
        }
        return FALSE;
    }

    public function getPurchaseReturnByWithoutBarcodeID($id) {
        $q = $this->db->get_where('purchases_return_without_barcode', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductOptionByID($id) {
        $q = $this->db->get_where('product_variants', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductWarehouseOptionQty($option_id, $warehouse_id) {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPurchaseReturnWithoutBarcodeByID($id) {
        $q = $this->db->get_where('purchases_return_without_barcode', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getUnlabelledByID($id) {
        $q = $this->db->get_where('unlabelled_damage_return', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductComboItems($pid, $warehouse_id) {
        $this->db->select('products.id as id, combo_items.item_code as code, combo_items.quantity as qty, products.name as name, products.type as type, warehouses_products.quantity as quantity')
                ->join('products', 'products.code=combo_items.item_code', 'left')
                ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
                ->where('warehouses_products.warehouse_id', $warehouse_id)
                ->group_by('combo_items.id');
        $q = $this->db->get_where('combo_items', array('combo_items.product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function addProductOptionQuantity($option_id, $warehouse_id, $quantity, $product_id) {
        if ($option = $this->getProductWarehouseOptionQty($option_id, $warehouse_id)) {
            $nq = $option->quantity + $quantity;
            if ($this->db->update('warehouses_products_variants', array('quantity' => $nq), array('option_id' => $option_id, 'warehouse_id' => $warehouse_id))) {
                return TRUE;
            }
        } else {
            if ($this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $quantity))) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function resetProductOptionQuantity($option_id, $warehouse_id, $quantity, $product_id) {
        if ($option = $this->getProductWarehouseOptionQty($option_id, $warehouse_id)) {
            $nq = $option->quantity - $quantity;
            if ($this->db->update('warehouses_products_variants', array('quantity' => $nq), array('option_id' => $option_id, 'warehouse_id' => $warehouse_id))) {
                return TRUE;
            }
        } else {
            $nq = 0 - $quantity;
            if ($this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $nq))) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function getOverSoldCosting($product_id) {
        $q = $this->db->get_where('costing', array('overselling' => 1));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getMulProdBarcode($dept, $store) {
        $name = $this->site->get_store($store)->barcode_prefix;
        if (isset($get['dept']) && $get['dept'] != "") {
            $dt = $this->site->getParaById('name', 'id', $get['dept'], 'department');
            if ($dt) {
                $name .= strtoupper($dt->name[0]);
            }
        }
        $name .= $this->getProCode($store);
        $name = trim($name);
        return $name;
    }

    function getProdBarcode($dept, $store) {
        $get = $this->input->get();

        $name = $this->site->get_store($store)->barcode_prefix;

        if (isset($dept) && $dept != "") {
            $dt = $this->site->getParaById('name', 'id', $dept, 'department');
            if ($dt) {
                $name .= strtoupper($dt->name[0]);
            }
        }
        $name .= $this->getProCode();

        $name = trim($name);
        return $name;
    }

    public function getProCode($store = null) {
        $get = (!empty($_GET) ) ? $this->input->get() : array();
        if (empty($get)) {
            $get['store_id'] = $store != null ? $store : "";
        }
        $id = 0;
        $dt = "";
        $code = "";
        $code = $this->site->getReference('br');
        if ($_GET['protype'] == "combo" || $_GET['protype'] == "bundle") {
            $id = sprintf('%08d', $code);
        } else {
            $id = sprintf('%09d', $code);
        }
        return ($id);
    }

    public function addPurchase($data, $items) {
        $data['created_on'] = date('y-m-d h:i:s');
        $data['updated_at'] = date('y-m-d h:i:s');
        $allagordno =$data['ag_order_no'];
        $alllr_no =$data['lr_no'];
        // $allitems = $items;
        
        
        $data['ag_order_no'] = $allagordno[0];
        $data['lr_no'] = $alllr_no[0];
        // $items = $allitems[$data['ag_order_no']];
        

        if ($this->db->insert('purchases', $data)) {
            $purchase_id = $this->db->insert_id();
            $this->site->updateReference('pur');
            $this->db->update('quotes', array('status' => 'sent'), array('reference_no' => $data['ag_order_no']));
            if ($this->site->getReference('po') == $data['reference_no']) {
                $this->site->updateReference('po');
            }

            
            $labeller_id = 0;
            $incentive_rate = 0;
            $labellerData = [];
            $labellerData['purchase_id'] = $purchase_id;
            $labellerData['labeller_id'] = $labeller_id;
            $labellerData['incentive_rate'] = $incentive_rate;
            

            /*

            Storing multiple against order no and lr no

            */

            for($i = 0; $i < count($allagordno); $i++)
            {
                
                $pur_ag_ord = [];
                $pur_ag_ord['purchase_id'] = $purchase_id;
                $pur_ag_ord['ag_order_no'] = $allagordno[$i];
                $pur_ag_ord['created_at'] = $savetime;
                $pur_ag_ord['updated_at'] = $savetime;

                $this->db->insert('purchases_ag_order', $pur_ag_ord);
            }

            for($i = 0; $i < count($alllr_no); $i++)
            {
                $savetime = date("Y-m-d H:i:s", time());
                $pur_lr_no = [];
                $pur_lr_no['purchase_id'] = $purchase_id;
                $pur_lr_no['lr_no'] = $alllr_no[$i];
                $pur_lr_no['created_at'] = $savetime;
                $pur_lr_no['updated_at'] = $savetime;

                $this->db->insert('purchase_lrno', $pur_lr_no);
            }


            foreach ($items as $item) {
                $item['purchase_id'] = $purchase_id;
                $rs = $this->db->select('*')
                                ->from('sma_products')
                                ->where('code', $item['product_code'])
                                ->get()->result();
                if (empty($rs)) {
                    $rss = $this->db->select('*')
                                    ->from('sma_quotes_products')
                                    ->where('code', $item['product_code'])
                                    ->get()->row();
                    unset($rss->id);
                    if (empty($rss->type)) {
                        $rss->type = 'standard';
                    }
                    $rss->quantity = $item['quantity'];
                    $barcode = $this->getProdBarcode($rss->department, $rss->store_id);
                    $rss->code = $barcode;
                    $rss->batchdate = date('Y-m-d h:i:s', now());
                    $rss->cost = $item['real_unit_cost'];
                    $this->db->insert('sma_products', $rss);
                    $ins_id = $this->db->insert_id();
                    $item['product_id'] = $ins_id;
                    $item['product_code'] = $barcode;
                    $this->db->insert('purchase_items', $item);
                    $this->site->updateReference('br');
                    $ins_into_wh = $this->db->insert('warehouses_products', array('product_id' => $ins_id, 'warehouse_id' => '1', 'quantity' => $item['quantity'], 'rack' => ''));
                } else {
                    $barcode = $this->getProdBarcode($rs[0]->department, $rs[0]->store_id);
                    $rs[0]->cost = $item['real_unit_cost'];
                    $rs[0]->code = $barcode;
                    unset($rs[0]->id);
                    $this->db->insert('sma_products', $rs[0]);
                    $ins_id = $this->db->insert_id();
                    $item['product_id'] = $ins_id;
                    $item['product_code'] = $barcode;
                    $this->db->insert('purchase_items', $item);
                    $this->site->updateReference('br');
                }
            }
            if ($data['status'] == 'received') {
                $this->site->syncQuantity(NULL, $purchase_id);
            }

        }
        else
            return false;
        
        return true;
    }

    public function updatePurchase($id, $data, $items = array()) {
//        echo "<pre>";
//        print_r($items);
//        echo "</pre>";
//        die();
        $opurchase = $this->getPurchaseByID($id);
        $oitems = $this->getAllPurchaseItems($id);

        if ($this->db->update('purchases', $data, array('id' => $id)) && $this->db->delete('purchase_items', array('purchase_id' => $id))) {
            $purchase_id = $id;
            foreach ($items as $item) {
                $item['purchase_id'] = $id;
                $this->db->insert('purchase_items', $item);

                $rs = $this->db->select('*')
                                ->from('sma_products')
                                ->where('code', $item['product_code'])
                                ->get()->result();
                if (empty($rs)) {
                    $rss = $this->db->select('*')
                                    ->from('sma_quotes_products')
                                    ->where('code', $item['product_code'])
                                    ->get()->row();
                    unset($rss->id);
                    $ins = $this->db->insert('sma_products', $rss);
                }
            }
            if ($opurchase->status == 'received') {
                $this->site->syncQuantity(NULL, NULL, $oitems);
            }
            if ($data['status'] == 'received') {
                $this->site->syncQuantity(NULL, $id);
            }



            $this->site->syncPurchasePayments($id);
            return true;
        }

        return false;
    }

    public function updatePurchaseReturn($id, $data, $items = array()) {

        $opurchase = $this->getPurchaseByID($id);
        $oitems = $this->getAllPurchaseReturnItems($id);
        if ($this->db->update('purchases_return', $data, array('id' => $id)) && $this->db->delete('purchase_return_items', array('purchase_id' => $id))) {
            $purchase_id = $id;

            foreach ($items as $item) {
                $item['purchase_id'] = $id;
                $item['status'] = 'returned';
                $this->db->insert('purchase_return_items', $item);
            }
            if ($opurchase->status == 'received') {
                $this->site->syncQuantity(NULL, NULL, $oitems);
            }
            if ($data['status'] == 'received') {
                $this->site->syncQuantity(NULL, $id);
            }
            $this->site->syncPurchasePayments($id);
            return true;
        }

        return false;
    }

    public function updateInStockDamage($id, $data, $items = array()) {

        $opurchase = $this->getPurchaseByID($id);
        $oitems = $this->getAllPurchaseReturnItems($id);
        if ($this->db->update('in_stock_damage', $data, array('id' => $id)) && $this->db->delete('in_stock_damage_items', array('purchase_id' => $id))) {
            $purchase_id = $id;

            foreach ($items as $item) {
                $item['purchase_id'] = $id;
                $item['status'] = 'returned';
                $this->db->insert('in_stock_damage_items', $item);
            }
            if ($opurchase->status == 'received') {
                $this->site->syncQuantity(NULL, NULL, $oitems);
            }
            if ($data['status'] == 'received') {
                $this->site->syncQuantity(NULL, $id);
            }
            $this->site->syncPurchasePayments($id);
            return true;
        }

        return false;
    }

    public function updateSupDamage($id, $data, $items = array()) {
        $opurchase = $this->getPurchaseByID($id);
        $oitems = $this->getAllSupDamageItems($id);
        if ($this->db->update('sup_damage', $data, array('id' => $id)) && $this->db->delete('sup_damage_items', array('purchase_id' => $id))) {
            $purchase_id = $id;
            foreach ($items as $item) {
                $item['purchase_id'] = $id;
                $item['status'] = 'returned';
                $this->db->insert('sup_damage_items', $item);
            }
            if ($opurchase->status == 'received') {
                $this->site->syncQuantity(NULL, NULL, $oitems);
            }
            if ($data['status'] == 'received') {
                $this->site->syncQuantity(NULL, $id);
            }
            $this->site->syncPurchasePayments($id);
            return true;
        }
        return false;
    }

    public function updatePurchaseReturnWithoutBarcode($id, $data, $items = array()) {
        $opurchase = $this->getPurchaseReturnByWithoutBarcodeID($id);
        $oitems = $this->getAllPurchaseReturnWithoutBarcodeItems($id);
        if ($this->db->update('purchases_return_without_barcode', $data, array('id' => $id)) && $this->db->delete('purchase_return_items_without_barcode', array('purchase_id' => $id))) {
            $purchase_id = $id;

            foreach ($items as $item) {
                $item['purchase_id'] = $id;
                $item['status'] = 'returned';
                $this->db->insert('purchase_return_items_without_barcode', $item);
            }
            if ($opurchase->status == 'received') {
                $this->site->syncQuantity(NULL, NULL, $oitems);
            }
            if ($data['status'] == 'received') {
                $this->site->syncQuantity(NULL, $id);
            }
            $this->site->syncPurchasePayments($id);
            return true;
        }

        return false;
    }

    public function updateUnlabelledWithoutBarcode($id, $data, $items = array()) {
        $opurchase = $this->getUnlabelledByID($id);
        $oitems = $this->getUnlabelledBarcodeItems($id);
        if ($this->db->update('unlabelled_damage_return', $data, array('id' => $id)) && $this->db->delete('unlabelled_damage_return_items', array('purchase_id' => $id))) {
            $purchase_id = $id;
            foreach ($items as $item) {
                $item['purchase_id'] = $id;
                $item['status'] = 'returned';
                $this->db->insert('unlabelled_damage_return_items', $item);
            }
            if ($opurchase->status == 'received') {
                $this->site->syncQuantity(NULL, NULL, $oitems);
            }
            if ($data['status'] == 'received') {
                $this->site->syncQuantity(NULL, $id);
            }
            $this->site->syncPurchasePayments($id);
            return true;
        }

        return false;
    }

    public function deletePurchase($id) {
        $purchase_items = $this->site->getAllPurchaseItems($id);

        if ($this->db->delete('purchase_items', array('purchase_id' => $id)) && $this->db->delete('purchases', array('id' => $id))) {
            $this->db->delete('payments', array('purchase_id' => $id));
            $this->site->syncQuantity(NULL, NULL, $purchase_items);
            return true;
        }
        return FALSE;
    }

    public function deletePurchaseReturnWithBarcode($id) {
//        $purchase_items = $this->site->getAllPurchaseItems($id);
        if ($this->db->delete('purchases_return', array('id' => $id))) {
            $this->site->syncQuantity(NULL, NULL, $purchase_items);
            return true;
        }
        return FALSE;
    }

    public function deleteInStockDamage($id) {
        if ($this->db->delete('in_stock_damage', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteSupDamage($id) {
        if ($this->db->delete('sup_damage', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deletePurchaseReturnWithoutBarcode($id) {

        if ($this->db->delete('purchases_return_without_barcode', array('id' => $id))) {

            $this->site->syncQuantity(NULL, NULL, $purchase_items);
            return true;
        }
        return FALSE;
    }

    public function deleteUnlabelledDamage($id) {

        if ($this->db->delete('unlabelled_damage_return', array('id' => $id))) {
            $product = $this->db->get_where('unlabelled_damage_return_items', array('id' => $id))->result();
            $this->db->delete('unlabelled_damage_return_items', array('purchase_id' => $id));
            foreach ($product as $row) {
                $this->db->delete('unlabelled_products', array('id' => $row->product_id));
            }
            $this->site->syncQuantity(NULL, NULL, $purchase_items);
            return true;
        }
        return FALSE;
    }

    public function convertChallantoBill($id) {
        if ($this->db->update('purchases_return_without_barcode', array('challan' => '0'), array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function convertpurchase($id) {
        $purno = 'PURCHASE' . $this->site->getReference('pur');
        if ($this->db->update('purchases', array('pur_challan' => '0', 'purchase_no' => $purno), array('id' => $id))) {
            $this->site->updateReference('pur');
            return true;
        }
        return FALSE;
    }

    public function convertpurchasereturn($id) {
        if ($this->db->update('purchases_return', array('challan' => '0'), array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getWarehouseProductQuantity($warehouse_id, $product_id) {
        $q = $this->db->get_where('warehouses_products', array('warehouse_id' => $warehouse_id, 'product_id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPurchasePayments($purchase_id) {
        $this->db->order_by('id', 'asc');
        $q = $this->db->get_where('payments', array('purchase_id' => $purchase_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getPaymentByID($id) {
        $q = $this->db->get_where('payments', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getPaymentsForPurchase($purchase_id) {
        $this->db->select('payments.date, payments.paid_by, payments.amount, payments.reference_no, users.first_name, users.last_name, type')
                ->join('users', 'users.id=payments.created_by', 'left');
        $q = $this->db->get_where('payments', array('purchase_id' => $purchase_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function addPayment($data = array()) {
        if ($this->db->insert('payments', $data)) {
            if ($this->site->getReference('pay') == $data['reference_no']) {
                $this->site->updateReference('pay');
            }
            $this->site->syncPurchasePayments($data['purchase_id']);
            return true;
        }
        return false;
    }

    public function updatePayment($id, $data = array()) {
        if ($this->db->update('payments', $data, array('id' => $id))) {
            $this->site->syncPurchasePayments($data['purchase_id']);
            return true;
        }
        return false;
    }

    public function deletePayment($id) {
        $opay = $this->getPaymentByID($id);
        if ($this->db->delete('payments', array('id' => $id))) {
            $this->site->syncPurchasePayments($opay->purchase_id);
            return true;
        }
        return FALSE;
    }

    public function getProductOptions($product_id) {
        $q = $this->db->get_where('product_variants', array('product_id' => $product_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductVariantByName($name, $product_id) {
        $q = $this->db->get_where('product_variants', array('name' => $name, 'product_id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getExpenseByID($id) {
        $q = $this->db->get_where('expenses', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addCashTransfer($data = array()) {
        if ($this->db->insert('pos_cash_transfer', $data)) {
            $this->site->updateReference('ctran');
            return true;
        }
        return false;
    }

    public function getCashTransferByID($id) {
        $q = $this->db->get_where('pos_cash_transfer', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateCashTransfer($id, $data = array()) {
        if ($this->db->update('pos_cash_transfer', $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function deleteCashTransfer($id) {
        if ($this->db->delete('pos_cash_transfer', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function addExpense($data = array()) {
        if ($this->db->insert('expenses', $data)) {
            if ($this->site->getReference('ex') == $data['reference']) {
                $this->site->updateReference('ex');
            }
            return true;
        }
        return false;
    }

    public function updateExpense($id, $data = array()) {
        if ($this->db->update('expenses', $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function deleteExpense($id) {
        if ($this->db->delete('expenses', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getQuoteByID($id) {
        $q = $this->db->get_where('quotes', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllQuoteItems($quote_id) {
        $q = $this->db->get_where('quote_items', array('quote_id' => $quote_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getviewParcelRev($id) {
        $com = $this->db->dbprefix('companies');
        $tab1 = $this->db->dbprefix('product_received_voucher');
        $this->db->select("product_received_voucher.id,companies.name as sender,product_received_voucher.sender_address,product_received_voucher.transport,product_received_voucher.veh_no,product_received_voucher.hamal,product_received_voucher.lr_no,product_received_voucher.bk_date,cc.name as receiver, product_received_voucher.goods_desc, product_received_voucher.parcel_type, product_received_voucher.delivery");
        $this->db->join('companies', 'companies.id = product_received_voucher.sender');
        $this->db->join('companies cc', 'cc.id = product_received_voucher.sender');
        $this->db->where('product_received_voucher.id', $id);
        return $this->db->get('product_received_voucher')->row();
    }

    public function get_product_received_voucher($id) {
//        companies.name as sender,product_received_voucher.sender_address,product_received_voucher.veh_no,product_received_voucher.hamal,product_received_voucher.lr_no,product_received_voucher.bk_date,product_received_voucher.no_of_bales,transport.transport_name,cc.name as receiver, product_received_voucher.goods_desc, product_received_voucher.parcel_type, product_received_voucher.delivery
        $this->db->select("product_received_voucher.*,comp.name as store_name, companies.company as sender, tt.transport_name, tt.code, CONCAT(sma_employee.fname, ' ',sma_employee.lname)  receiver", FALSE);
        $this->db->join('companies', 'companies.id = product_received_voucher.sender', 'left');
        $this->db->join('employee', 'employee.id = product_received_voucher.receiver', 'left');
        $this->db->join('transport tt', 'tt.id = product_received_voucher.transport', 'left');
        $this->db->join('companies comp', 'comp.id = product_received_voucher.store_id', 'left');
//        $this->db->join('transport','transport.id = product_received_voucher.transport')
        $this->db->where('product_received_voucher.id', $id);

//        $this->db->select('product_received_voucher.*');
        $q = $this->db->get_where('product_received_voucher', array('product_received_voucher.id' => $id));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data[0];
        }
        return FALSE;
    }

    public function get_parcel_sent_voucher($id) {
        $this->db->select('parcel_sent_voucher.*,cc.company receiver,tt.transport_name, tt.code,concat(sma_employee.fname, " " , sma_employee.lname) as store,comp.name as store_name', FALSE);
        $this->db->join('companies cc', 'cc.id = parcel_sent_voucher.receiver', 'left');
        $this->db->join('companies comp', 'comp.id = parcel_sent_voucher.store_id', 'left');
        $this->db->join('transport tt', 'tt.id = parcel_sent_voucher.transport', 'left');
        $this->db->join('employee', "employee.id=parcel_sent_voucher.booked_by", "left");

        $this->db->where('parcel_sent_voucher.id', $id);
        $q = $this->db->get_where('parcel_sent_voucher', array('parcel_sent_voucher.id' => $id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data[0];
        }
        return FALSE;
    }

    public function deleteVouchers($table, $id) {
        if ($this->db->delete($table, array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getBillAmount($id) {

        $total = 0;
        $dates = "";
        foreach ($id as $i) {
            $r = $this->db->select('grand_total, date')
                    ->where('id', $i)
                    ->from('purchases')
                    ->get();
            if ($r->num_rows() > 0) {

                $total = $r->row()->grand_total + $total;
                $dates[] = $r->row()->date;
            }
        }

        $data = array(
            'total' => $total,
            'dates' => implode(', ', $dates)
        );
        return $data;
    }

    public function getBillAmountbyedit($id) {

        $total = 0;
        $dates = "";

        $r = $this->db->select('grand_total, date')
                ->where('id', $id)
                ->from('purchases')
                ->get();
        if ($r->num_rows() > 0) {

            $total = $r->row()->grand_total + $total;
            $dates[] = $r->row()->date;
        }


        $data = array(
            'total' => $total,
            'dates' => implode(', ', $dates)
        );
        return $data;
    }

    public function getAmmount($id) {
        $rs = $this->db->select('grand_total')
                        ->where('id', $id)
                        ->from('purchases')
                        ->get()->result();
        return $rs[0]->grand_total;
    }

    public function get_sup_id_from_purchase($id) {
        $rs = $this->db->select('supplier_id')
                        ->where('id', $id)
                        ->from('purchases')
                        ->get()->row();
        return $rs->supplier_id;
    }

    public function get_sup_id_from_instock_damage($id) {
        $rs = $this->db->select('supplier_id')
                        ->where('id', $id)
                        ->from('in_stock_damage')
                        ->get()->row();
        return $rs->supplier_id;
    }

    public function get_sup_id_from_sup_damage($id) {
        $rs = $this->db->select('supplier_id')
                        ->where('id', $id)
                        ->from('sup_damage')
                        ->get()->row();
        return $rs->supplier_id;
    }

    public function addPurchaseReturn($data, $items) {
        if ($this->db->insert('purchases_return', $data)) {
            $purchase_id = $this->db->insert_id();
            if ($this->site->getReference('po') == $data['reference_no']) {
                $this->site->updateReference('po');
            }
            foreach ($items as $item) {
                $item['purchase_id'] = $purchase_id;
                $this->db->insert('purchase_return_items', $item);
            }
            if ($data['status'] == 'received') {
                $this->site->syncQuantity(NULL, $purchase_id);
            }
            return true;
        }
        return false;
    }

    public function getPurchaseReturnNo($id) {
        $store = $this->site->get_store($id);
        $code = $store->barcode_prefix;
        $r = $this->get_purchaseReturnNo()->id;
        $num_str = $code . 'P' . sprintf("%06d", $r + 1);
        return $num_str;
    }

    public function addUnlabelledDamageReturn($data, $items) {
        if (empty($data['supplier'])) {
            $data['supplier'] = 'none';
        }
        if ($this->db->insert('unlabelled_damage_return', $data)) {
            $purchase_id = $this->db->insert_id();
            if ($this->site->getReference('po') == $data['reference_no']) {
                $this->site->updateReference('po');
            }
            foreach ($items as $item) {
                $item['purchase_id'] = $purchase_id;
                $this->db->insert('unlabelled_damage_return_items', $item);
            }
            if ($data['status'] == 'received') {
                $this->site->syncQuantity(NULL, $purchase_id);
            }

            $purchaseData = $data;
            $purchaseData['challan'] = '1';
            $purchaseData['created_on'] = date('Y-m-d H:i:s');
            $purchaseData['damage_id'] = $purchase_id;
            $this->db->insert('purchases_return_without_barcode', $purchaseData);
            $purchaseReturn_id = $this->db->insert_id();
            foreach ($items as $item) {
                $item['purchase_id'] = $purchaseReturn_id;
                $this->db->insert('purchase_return_items_without_barcode', $item);

                $unlabelItem = (array) $this->db->select('*')->get_where('unlabelled_products', array('id' => $item['product_id']))->row();
                unset($unlabelItem['id']);
                $this->db->insert('purchase_return_product_without_barcode', $unlabelItem);
            }
            if ($data['status'] == 'received') {
                $this->site->syncQuantity(NULL, $purchase_id);
            }
            return true;
        }
        return false;
    }

    public function addInStockDamage($data, $items) {
        if ($this->db->insert('in_stock_damage', $data)) {
            $purchase_id = $this->db->insert_id();
            if ($this->site->getReference('po') == $data['reference_no']) {
                $this->site->updateReference('po');
            }
            foreach ($items as $item) {
                $item['purchase_id'] = $purchase_id;
                $this->db->insert('in_stock_damage_items', $item);
            }
            if ($data['status'] == 'received') {
                $this->site->syncQuantity(NULL, $purchase_id);
            }
            return true;
        }
        return false;
    }

    public function addSupDamage($data, $items) {
        if ($this->db->insert('sup_damage', $data)) {
            $purchase_id = $this->db->insert_id();
            if ($this->site->getReference('po') == $data['reference_no']) {
                $this->site->updateReference('po');
            }
            foreach ($items as $item) {
                $item['purchase_id'] = $purchase_id;
                $this->db->insert('sup_damage_items', $item);
            }
            if ($data['status'] == 'received') {
                $this->site->syncQuantity(NULL, $purchase_id);
            }
            return true;
        }
        return false;
    }

    public function addPurchaseReturnWithoutBarcode($data, $items) {

        $this->db->insert('purchases_return', $data);

        $purchase_id = $this->db->insert_id();
//            $this->db->update('quotes', array('status' => 'sent'), array('reference_no' => $data['ag_order_no']));
        if ($this->site->getReference('po') == $data['reference_no']) {
            $this->site->updateReference('po');
        }
        foreach ($items as $item) {
            $item['purchase_id'] = $purchase_id;
            $this->db->insert('purchase_return_items', $item);
            echo"<pre>";
            print_r($this->db->last_query());
            echo"</pre>";
            die();
        }
        if ($data['status'] == 'received') {
            $this->site->syncQuantity(NULL, $purchase_id);
        }
        return true;
    }

    public function savePurchaseReturnWithoutBarcode($data, $items) {
        if ($this->db->insert('purchases_return_without_barcode', $data)) {
            $purchase_id = $this->db->insert_id();
            if ($this->site->getReference('po') == $data['reference_no']) {
                $this->site->updateReference('po');
            }
            foreach ($items as $item) {
                $item['purchase_id'] = $purchase_id;
                $this->db->insert('purchase_return_items_without_barcode', $item);
            }
            if ($data['status'] == 'received') {
                $this->site->syncQuantity(NULL, $purchase_id);
            }
            return true;
        }
        return false;
    }

    public function getProductByCodeFromReturn($code) {
        $q = $this->db->get_where('purchase_return_product_without_barcode', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductReturnedByCode($code) {
        $q = $this->db->get_where('purchase_return_product_without_barcode', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSupIdFromPurchaseReturnWithoutBarcode($id) {
        $rs = $this->db->select('supplier_id')
                        ->where('id', $id)
                        ->from('purchases_return_without_barcode')
                        ->get()->row();
        return $rs->supplier_id;
    }

    public function getSupIdFromUnlabelledBarcode($id) {
        $rs = $this->db->select('supplier_id')
                        ->where('id', $id)
                        ->from('unlabelled_damage_return')
                        ->get()->row();
        return $rs->supplier_id;
    }

    public function getReferenceSupDamage($purchase_id) {
        $rs = $this->db->select('sup_damage.reference_no')
                        ->join('sup_damage', 'sup_damage.id = sup_damage_items.purchase_id')
                        ->from('sup_damage_items')
                        ->get()->row();
        return $rs->reference_no;
    }

    public function addDamageReturn($data, $items) {
        $purchaseReturn = $data;
        if ($this->db->insert('sma_damage_sold_return', $data)) {
            $purchase_id = $this->db->insert_id();
            if ($this->site->getReference('po') == $data['reference_no']) {
                $this->site->updateReference('po');
            }
            foreach ($items as $item) {
                $item['purchase_id'] = $purchase_id;
                $this->db->insert('sma_damage_sold_return_items', $item);
            }
            if ($data['status'] == 'received') {
                $this->site->syncQuantity(NULL, $purchase_id);
            }
            $purchaseReturn['purchase_return_no'] = $this->getPurchaseReturnNo($data['store_id']);
            $purchaseReturn['delivery_date'] =  date('Y-m-d H:i:s', now());
            $purchaseReturn['challan'] = '1';
            $purchaseReturn['damage_id'] = $purchase_id;
            if ($this->db->insert('purchases_return', $purchaseReturn)) {
                $pruchaseRetrunId = $this->db->insert_id();
                foreach ($items as $item) {
                    $item['purchase_id'] = $pruchaseRetrunId;
                    $this->db->insert('purchase_return_items', $item);
                }
            }
            return true;
        }
        return false;
    }

    public function getAllDamageReturnItems($purchase_id) {
        $this->db->select('sma_damage_sold_return_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, products.unit, products.details as details, product_variants.name as variant,products.squantity,products.sunit')
                ->join('products', 'products.id=sma_damage_sold_return_items.product_id', 'left')
                ->join('product_variants', 'product_variants.id=sma_damage_sold_return_items.option_id', 'left')
                ->join('tax_rates', 'tax_rates.id=sma_damage_sold_return_items.tax_rate_id', 'left')
                ->group_by('sma_damage_sold_return_items.id')
                ->order_by('id', 'asc');
        $q = $this->db->get_where('sma_damage_sold_return_items', array('purchase_id' => $purchase_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getDamageReturnByID($id) {
        $q = $this->db->get_where('sma_damage_sold_return', array('id' => $id), 1);
        if ($q->num_rows() > 0) {

            return $q->row();
        }
        return FALSE;
    }

    public function updateDamageReturn($id, $data, $items = array()) {
        $opurchase = $this->getPurchaseByID($id);
        $oitems = $this->getAllDamageReturnItems($id);
        if ($this->db->update('sma_damage_sold_return', $data, array('id' => $id)) && $this->db->delete('sma_damage_sold_return_items', array('purchase_id' => $id))) {
            $purchase_id = $id;
            foreach ($items as $item) {
                $item['purchase_id'] = $id;
                $item['status'] = 'returned';
                $this->db->insert('sma_damage_sold_return_items', $item);
            }
            if ($opurchase->status == 'received') {
                $this->site->syncQuantity(NULL, NULL, $oitems);
            }
            if ($data['status'] == 'received') {
                $this->site->syncQuantity(NULL, $id);
            }
            $this->site->syncPurchasePayments($id);
            return true;
        }
        return false;
    }

    public function getOrderProductByID($id) {
        $q = $this->db->get_where('sma_quotes_products', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateOderItem($id, $data) {
        $this->db->where('id', $id);
        $Q = $this->db->update('sma_quotes_products', $data);
        $this->db->where('product_id', $id);
        $Q = $this->db->update('sma_quote_items', array('quantity' => $data['quantity']));
        if ($Q) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function purchasedetails($id) {
        $q = $this->db->get_where('purchases', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function purchasedetailsByIDdeatails($id) {
        $q = $this->db->get_where('vw_purchase', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_purchaseReturnNo() {
        $r = $this->db->select('max(id) as id')->get('purchases_return')->row();
        return $r;
    }

    public function add_WithoutBarcodePayment($data = array()) {
        if ($this->db->insert('recive_payments', $data)) {
            if ($this->site->getReference('pay') == $data['reference_no']) {
                $this->site->updateReference('pay');
            }
            $this->site->syncPurchaseReturnWithoutBarcodePayments($data['purchase_id']);
            return true;
        }
        return false;
    }
    public function add_PurchaseReturnChallanPayment($data = array()) {
        if ($this->db->insert('recive_payments', $data)) {
            if ($this->site->getReference('pay') == $data['reference_no']) {
                $this->site->updateReference('pay');
            }
            $this->site->syncPurchaseReturnWithBarcodePayments($data['purchase_return_barcode']);
            return true;
        }
        return false;
    }

}
