<?php defined('BASEPATH') OR exit('No direct script access allowed');

$config = array(
/*     'supplier/general' => array(

        array(   
                
        'field' => 'gstno',
        'label' => 'gstno',
        'rules' => 'trim|required'
        )
    ), */
    'supplier/addSupplier' => array(

        array(   

        'field' => 'Supplier_Code',
        'label' => 'Supplier_Code',
        'rules' => 'trim|required'
        ),
        array(   

        'field' => 'Supplier_Name',
        'label' => 'Supplier_Name',
        'rules' => 'trim|required'
        ),
        array(   

        'field' => 'Supplier_Discount',
        'label' => 'Supplier_Discount',
        'rules' => 'trim|numeric|greater_than_equal_to[0]|less_than_equal_to[100]'
        ),
        array(   

        'field' => 'first_Name',
        'label' => 'first_Name',
        'rules' => 'trim|alpha'
        ),
        array(

        'field' => 'last_Name',
        'label' => 'last_Name',
        'rules' => 'trim|alpha'
        ),
        array(

        'field' => 'telephone_no',
        'label' => 'telephone_no',
        'rules' => 'trim|numeric'
        ),
        array(

        'field' => 'fax_no',
        'label' => 'fax_no',
        'rules' => 'trim|alpha_numeric'
        ),
        array(

        'field' => 'mobile_No',
        'label' => 'mobile_No',
        'rules' => 'trim|required|numeric|exact_length[10]'
        ),
        array(

        'field' => 'website',
        'label' => 'website',
        'rules' => 'trim|valid_url'
        ),
        array(   

        'field' => 'Address1',
        'label' => 'Address1',
        'rules' => 'trim|required'
        ),
        array(

        'field' => 'city',
        'label' => 'city',
        'rules' => 'trim|required'
        ),
        array(

        'field' => 'country',
        'label' => 'country',
        'rules' => 'trim|required'
        ),
        array(

        'field' => 'Zip_Code',
        'label' => 'Zip_Code',
        'rules' => 'trim|required|numeric|exact_length[6]'
        ),        
        array(
        'field' => 'payment',
        'label' => 'payment',
        'rules' => 'trim|required|numeric'
        ),
/*         array(
        'field' => 'photo',
        'label' => 'photo',
        'rules' => 'trim|required'
        ), */
        array(
        'field' => 'percentage_commision',
        'label' => 'percentage_commision',
        'rules' => 'trim|numeric'
        )
    ),

    'employee/saveemployee' => array(

        array(
            'field' => 'dojoin',
            'label' => 'date of joining',
            'rules' => 'trim|required'            
        ),
    
        array(
            'field' => 'fname',
            'label' => 'first_name',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'lname',
            'label' => 'last_name',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'dob',
            'label' => 'date of birth',
            'rules' => 'trim|required'
        ),
        
        array(
            'field' => 'maritalstatus',
            'label' => 'marital status',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'anniversary',
            'label' => 'anniversary',
            'rules' => 'trim'
        ),
        array(
            'field' => 'no_of_dependends',
            'label' => 'Number of dependents',
            'rules' => 'trim|is_natural'
        ),
        array(
            'field' => 'gender',
            'label' => 'Gender',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'address',
            'label' => 'address(Home)',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'mobileno',
            'label' => 'mobileno',
            'rules' => 'trim|required|exact_length[10]'
        ),
        // array(
        //     'field' => 'identity[]',
        //     'label' => 'Identity',
        //     'rules' => 'trim|required'
        // ),
        // array(
        //     'field' => 'references[]',
        //     'label' => 'References',
        //     'rules' => 'trim|required'
        // ),
        array(
            'field' => 'department',
            'label' => 'Department',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'department',
            'label' => 'Department',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'refered',
            'label' => 'Refered',
            'rules' => 'trim'
        ),
        array(
            'field' => 'section',
            'label' => 'Section',
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'salary',
            'label' => 'Salary',
            'rules' => 'trim|required'
        )
    ),

    'auth/login' => array(
        array(
            'field' => 'identity',
            'label' => lang('username'),
            'rules' => 'required'
        ),
        array(
            'field' => 'password',
            'label' => lang('password'),
            'rules' => 'required'
        )
    ),
    'auth/create_user' => array(
        array(
            'field' => 'first_name',
            'label' => lang('first_name'),
            'rules' => 'required'
        ),
        array(
            'field' => 'last_name',
            'label' => lang('last_name'),
            'rules' => 'required'
        ),
        array(
            'field' => 'username',
            'label' => lang('username'),
            'rules' => 'required|alpha_dash'
        ),
        array(
            'field' => 'email',
            'label' => lang('email'),
            'rules' => 'required|valid_email'
        ),
        array(
            'field' => 'phone',
            'label' => lang('phone'),
            'rules' => 'required'
        ),
        array(
            'field' => 'company',
            'label' => lang('company'),
            'rules' => 'required'
        ),
        array(
            'field' => 'gender',
            'label' => lang('gender'),
            'rules' => 'required'
        ),
        array(
            'field' => 'password',
            'label' => lang('password'),
            'rules' => 'required|min_length[7]|max_length[20]'
        ),
        array(
            'field' => 'confirm_password',
            'label' => lang('confirm_password'),
            'rules' => 'required|min_length[7]|max_length[20]|matches[password]'
        ),
    ),
    'auth/edit_user' => array(
        array(
            'field' => 'first_name',
            'label' => lang('first_name'),
            'rules' => 'required'
        ),
        array(
            'field' => 'last_name',
            'label' => lang('last_name'),
            'rules' => 'required'
        ),
        array(
            'field' => 'username',
            'label' => lang('username'),
            'rules' => 'required|alpha_dash'
        ),
        array(
            'field' => 'email',
            'label' => lang('email'),
            'rules' => 'required|valid_email'
        ),
        array(
            'field' => 'phone',
            'label' => lang('phone'),
            'rules' => 'required'
        ),
        array(
            'field' => 'company',
            'label' => lang('company'),
            'rules' => 'required'
        ),
        array(
            'field' => 'gender',
            'label' => lang('gender'),
            'rules' => 'required'
        ),
    ),
    'products/add' => array(
        array(
            'field' => 'code',
            'label' => lang('product_code'),
            'rules' => 'trim|min_length[2]|max_length[20]|required|alpha_numeric'
        ),
        array(
            'field' => 'name',
            'label' => lang('product_name'),
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'category',
            'label' => lang('cname'),
            'rules' => 'required'
        ),
        array(
            'field' => 'barcode_symbology',
            'label' => lang('barcode_symbology'),
            'rules' => 'required'
        ),
        array(
            'field' => 'unit',
            'label' => lang('product_unit'),
            'rules' => 'required'
        ),
        array(
            'field' => 'price',
            'label' => lang('product_price'),
            'rules' => 'required|numeric'
        )
    ),
/*     'companies/addSupplier' => array(
        array(
            'field' => 'suppliername',
            'label' => lang('suppliername'),
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'store_id',
            'label' => lang('store_id'),
            'rules' => 'trim|required'
        ),
        array(
            'field' => 'address1',
            'label' => lang('address'),
            'rules' => 'required'
        ),
        array(
            'field' => 'city',
            'label' => lang('city'),
            'rules' => 'required'
        ),
        array(
            'field' => 'state',
            'label' => lang('state'),
            'rules' => 'required'
        ),
        array(
            'field' => 'country',
            'label' => lang('country'),
            'rules' => 'required'
        ),
        array(
            'field' => 'mobileno',
            'label' => lang('mobileno'),
            'rules' => 'required'
        )
    ), */
    'companies/add_user' => array(
        array(
            'field' => 'first_name',
            'label' => lang('first_name'),
            'rules' => 'required'
        ),
        array(
            'field' => 'last_name',
            'label' => lang('last_name'),
            'rules' => 'required'
        ),
        array(
            'field' => 'email',
            'label' => lang('email'),
            'rules' => 'required|valid_email'
        )
    ),
    'suppliers/savesupplierbank' => array(
        array(
            'field' => 'bank_name',
            'label' => lang('bank_name'),
            'rules' => 'required'
        ),
        array(
            'field' => 'bank_branch',
            'label' => lang('bank_branch'),
            'rules' => 'required'
        ),
        array(
            'field' => 'acc_no',
            'label' => lang('acc_no'),
            'rules' => 'required'
        ),
        array(
            'field' => 'ifsc',
            'label' => lang('ifsc'),
            'rules' => 'required'
        )
    ),
);

