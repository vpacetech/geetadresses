<?php

class Hsn extends MY_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->lang->load('products', $this->Settings->language);
        $this->load->library('form_validation');
        $this->load->model('Hst_model', 'hsn');
        $this->load->model('companies_model');
    }

    function index($action = NULL) {
        $this->sma->checkPermissions('index');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('hsn')));
        $meta = array('page_title' => lang('hsn'), 'bc' => $bc);
        $this->page_construct('hsn/index', $meta, $this->data);
    }

    function gethsn() {
        $this->sma->checkPermissions('index');
        $this->load->library('datatables');
        $edit_link = anchor('hsn/edit/$1', '<i class="fa fa-pencil"></i> ' . 'Edit', 'data-toggle="modal" data-target="#myModal"');
//        $view_link = anchor('purchases/viewParcelRev/$1', '<i class="fa fa-eye"></i> ' . 'View', 'data-toggle="modal" data-target="#myModal"');
        $delete_link = "<a href='#' class='tip po' title='<b> Delete </b>' data-content=\"<p>"
                . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' id='a__$1' href='" . site_url('hsn/delete/$1') . "'>"
                . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
                . "Delete </a>";
        $action = '<div class="text-center"><div class="btn-group text-left">'
                . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
                . 'Actions' . ' <i style="color:#fff" class="fa fa-caret-down"></i></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li>' . $edit_link . '</li>
			<li>' . $delete_link . '</li>
			</ul>
		</div></div>';

        $rs = $this->datatables->select("id,particulars,hsncode,taxrate", false)
//                . "IF(mrp!='',mrp, CONCAT(mrpfrom, ' - ', mrpto)) AS rate", false)
                ->from("hsncodes")
                ->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    function addhsn($warehouse_id = 0) {
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();
        $this->data['tab'] = $tab;
        $this->load->view($this->theme . 'hsn/add', $this->data);
    }

    function edit($id = 0) {
        $this->data['hsndata'] = $this->hsn->getHsnByID($id);
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();
        $this->data['tab'] = $tab;
        $this->load->view($this->theme . 'hsn/edit', $this->data);
    }

    public function savehsn() {
        $data = array(
            'particulars' => $this->input->post('particulars'),
            'hsncode' => $this->input->post('hsncode'),
            'taxrate' => $this->input->post('taxrate')
        );

        $x = $this->db->insert("hsncodes", $data);
        if ($x) {
            $this->session->set_flashdata('message', lang("hsn_added"));
            redirect("hsn");
        } else {
            $this->session->set_flashdata('error', lang("hsn_failed"));
            redirect("hsn");
        }
    }

    public function updatehsn($id) {
        $data = array(
            'particulars' => $this->input->post('particulars'),
            'hsncode' => $this->input->post('hsncode'),
            'taxrate' => $this->input->post('taxrate')
        );
        $this->db->where('id', $id);
        $x = $this->db->update("hsncodes", $data);
        if ($x) {
            $this->session->set_flashdata('message', lang("hsn_added"));
            redirect("hsn");
        } else {
            $this->session->set_flashdata('error', lang("hsn_failed"));
            redirect("hsn");
        }
    }

    public function delete($id) {
        $sid = $this->hsn->delete($id);
        if ($sid) {
            echo $this->lang->line("gst_deleted");
        }
    }

    function hsn_actions() {
        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    $error = false;
                    foreach ($_POST['val'] as $id) {
                        if (!$this->hsn->delete($id)) {
                            $error = true;
                        }
                    }
                    if ($error) {
                        $this->session->set_flashdata('warning', lang('hsn_x_deleted'));
                    } else {
                        $this->session->set_flashdata('message', $this->lang->line("gst_deleted"));
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel' || $this->input->post('form_action') == 'export_pdf') {
                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('hsn'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('Id'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('particulars'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('hsncode'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('taxrate'));
                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $customer = $this->hsn->getHsnByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $customer->id);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $customer->particulars);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $customer->hsncode);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $customer->taxrate);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'HSNCodeList_' . date('Y_m_d_H_i_s');
                    if ($this->input->post('form_action') == 'export_pdf') {
                        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
                        $this->excel->getDefaultStyle()->applyFromArray($styleArray);
                        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                        require_once(APPPATH . "third_party" . DIRECTORY_SEPARATOR . "MPDF" . DIRECTORY_SEPARATOR . "mpdf.php");
                        $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                        $rendererLibrary = 'MPDF';
                        $rendererLibraryPath = APPPATH . 'third_party' . DIRECTORY_SEPARATOR . $rendererLibrary;
                        if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                            die('Please set the $rendererName: ' . $rendererName . ' and $rendererLibraryPath: ' . $rendererLibraryPath . ' values' .
                                    PHP_EOL . ' as appropriate for your directory structure');
                        }

                        header('Content-Type: application/pdf');
                        header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
                        return $objWriter->save('php://output');
                    }
                    if ($this->input->post('form_action') == 'export_excel') {
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                        return $objWriter->save('php://output');
                    }

                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_gst_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

}
