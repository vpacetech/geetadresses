<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Transport extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('transportation', $this->Settings->language);

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $this->load->model('transport_model');
        $this->load->library('ion_auth');
    }

    function index() {
        $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('list').' '.lang('transport')));
        $meta = array('page_title' => lang('add_transport'), 'bc' => $bc);
        $this->page_construct('transportation/index', $meta, $this->data);
    }

    function Add() {
        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang("access_denied"));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->data['title'] = "Create Transport";
        $this->form_validation->set_rules('username', lang("username"), 'trim|is_unique[users.username]');
//        $this->form_validation->set_rules('email', lang("email"), 'trim|is_unique[users.email]');
        if ($this->form_validation->run() == true) {
            $email = strtolower($this->input->post('email'));
            $additional_data = array(
                'code' => $this->input->post('code'),
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'email' => $this->input->post('email'),
                'gender' => $this->input->post('gender'),
                'phone' => $this->input->post('phone'),
                'transport_name' => $this->input->post('trans_name'),
                'address' => $this->input->post('address'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('postal_code'),
                'country' => $this->input->post('country'),
                'mobile' => $this->input->post('mobile'),
                'landline' => $this->input->post('landline'),
                'fax' => $this->input->post('fax'),
                'gst_no' => $this->input->post('gst_no'),
                'igst' => $this->input->post('igst'),
                // 'status' => $this->input->post('status'),
                'status' => "Active",
            );
        }
        if ($this->form_validation->run() == true && $this->transport_model->create_transport($additional_data)) {
            $this->session->set_flashdata('message', $this->lang->line("transp_added"));
            redirect("transport");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('error')));
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('transportation'), 'page' => lang('transportation')), array('link' => '#', 'page' => lang('add_transport')));
            $meta = array('page_title' => lang('add_transport'), 'bc' => $bc);
            $this->data['latest_transport'] = $this->transport_model->getLatestTransport();
            $this->page_construct('transportation/add', $meta, $this->data);
        }
    }

    function edit($id = NULL) {
        $this->data['ed_tr'] = $this->transport_model->getTransportData($id);
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('transportation'), 'page' => lang('transportation')), array('link' => '#', 'page' => lang('edit_transport')));
        $meta = array('page_title' => lang('edit_transfer_quantity'), 'bc' => $bc);
        $this->page_construct('transportation/edit', $meta, $this->data);
    }

    function update_transport($id) {
        $data = array(
            'code' => $this->input->post('code'),
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'email' => $this->input->post('email'),
            'gender' => $this->input->post('gender'),
            'phone' => $this->input->post('phone'),
            'transport_name' => $this->input->post('trans_name'),
            'address' => $this->input->post('address'),
            'city' => $this->input->post('city'),
            'state' => $this->input->post('state'),
            'postal_code' => $this->input->post('postal_code'),
            'country' => $this->input->post('country'),
            'mobile' => $this->input->post('mobile'),
            'landline' => $this->input->post('landline'),
            'fax' => $this->input->post('fax'),
               'gst_no' => $this->input->post('gst_no'),
                'igst' => $this->input->post('igst'),
            'status' => $this->input->post('status'),
        );
        if ($this->transport_model->update_transport($id,$data)) {
            $this->session->set_flashdata('message', $this->lang->line("transp_updated"));
            redirect("transport");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('error')));
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('transportation'), 'page' => lang('transportation')), array('link' => '#', 'page' => lang('add_transport')));
            $meta = array('page_title' => lang('add_transport'), 'bc' => $bc);
            $this->page_construct('transportation/add', $meta, $this->data);
        }
    }

    function pdf($transfer_id = NULL, $view = NULL, $save_bufffer = NULL) {
        if ($this->input->get('id')) {
            $transfer_id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $transfer = $this->transport_model->getTransferByID($transfer_id);
        $this->sma->view_rights($transfer->created_by);
        $this->data['rows'] = $this->transport_model->getAllTransferItems($transfer_id, $transfer->status);
        $this->data['from_warehouse'] = $this->site->getWarehouseByID($transfer->from_warehouse_id);
        $this->data['to_warehouse'] = $this->site->getWarehouseByID($transfer->to_warehouse_id);
        $this->data['transfer'] = $transfer;
        $this->data['tid'] = $transfer_id;
        $this->data['created_by'] = $this->site->getUser($transfer->created_by);
        $name = lang("transfer") . "_" . str_replace('/', '_', $transfer->transfer_no) . ".pdf";
        $html = $this->load->view($this->theme . 'transfers/pdf', $this->data, TRUE);
        if ($view) {
            $this->load->view($this->theme . 'transfers/pdf', $this->data);
        } elseif ($save_bufffer) {
            return $this->sma->generate_pdf($html, $name, $save_bufffer);
        } else {
            $this->sma->generate_pdf($html, $name);
        }
    }

    function email($transfer_id = NULL) {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $transfer_id = $this->input->get('id');
        }
        $transfer = $this->transport_model->getTransferByID($transfer_id);
        $this->form_validation->set_rules('to', lang("to") . " " . lang("email"), 'trim|required|valid_email');
        $this->form_validation->set_rules('subject', lang("subject"), 'trim|required');
        $this->form_validation->set_rules('cc', lang("cc"), 'trim');
        $this->form_validation->set_rules('bcc', lang("bcc"), 'trim');
        $this->form_validation->set_rules('note', lang("message"), 'trim');

        if ($this->form_validation->run() == true) {
            $this->sma->view_rights($transfer->created_by);
            $to = $this->input->post('to');
            $subject = $this->input->post('subject');
            if ($this->input->post('cc')) {
                $cc = $this->input->post('cc');
            } else {
                $cc = NULL;
            }
            if ($this->input->post('bcc')) {
                $bcc = $this->input->post('bcc');
            } else {
                $bcc = NULL;
            }

            $this->load->library('parser');
            $parse_data = array(
                'reference_number' => $transfer->transfer_no,
                'site_link' => base_url(),
                'site_name' => $this->Settings->site_name,
                'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo . '" alt="' . $this->Settings->site_name . '"/>'
            );
            $msg = $this->input->post('note');
            $message = $this->parser->parse_string($msg, $parse_data);
            //$name = lang("transfer") . "_" . str_replace('/', '_', $transfer->transfer_no) . ".pdf";
            //$file_content = $this->pdf($transfer_id, NULL, 'S');
            //$attachment = array('file' => $file_content, 'name' => $name, 'mime' => 'application/pdf');
            $attachment = $this->pdf($transfer_id, NULL, 'S'); //delete_files($attachment);
        } elseif ($this->input->post('send_email')) {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->session->set_flashdata('error', $this->data['error']);
            redirect($_SERVER["HTTP_REFERER"]);
        }


        if ($this->form_validation->run() == true && $this->sma->send_email($to, $subject, $message, NULL, NULL, $attachment, $cc, $bcc)) {
            delete_files($attachment);
            $this->session->set_flashdata('message', lang("email_sent"));
            redirect("transfers");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            if (file_exists('./themes/' . $this->theme . '/views/email_templates/transfer.html')) {
                $transfer_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/transfer.html');
            } else {
                $transfer_temp = file_get_contents('./themes/default/views/email_templates/transfer.html');
            }
            $this->data['subject'] = array('name' => 'subject',
                'id' => 'subject',
                'type' => 'text',
                'value' => $this->form_validation->set_value('subject', 'Tranfer Order (' . $transfer->transfer_no . ') from ' . $transfer->from_warehouse_name),
            );
            $this->data['note'] = array('name' => 'note',
                'id' => 'note',
                'type' => 'text',
                'value' => $this->form_validation->set_value('note', $transfer_temp),
            );
            $this->data['warehouse'] = $this->site->getWarehouseByID($transfer->to_warehouse_id);

            $this->data['id'] = $transfer_id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'transfers/email', $this->data);
        }
    }
    
    function delete($id = NULL) {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->transport_model->deleteTransport($id)) {
            if ($this->input->is_ajax_request()) {
                echo lang("transport_deleted");
                  die();
            }
            $this->session->set_flashdata('transport', lang('transport_deleted'));
            redirect('transport');
        }
    }

    function getTransport() {
        $this->sma->checkPermissions('index');

//        $detail_link = anchor('transport/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('transfer_details'), 'data-toggle="modal" data-target="#myModal"');
        $email_link = anchor('transport/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_transfer'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('transport/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_transport'));
        $pdf_link = anchor('transport/pdf/$1', '<i class="fa fa-file-pdf-o"></i> ' . lang('download_pdf'));
        $delete_link = "<a href='#' class='tip po' title='<b>" . lang("delete_transport") . "</b>' data-content=\"<p>"
                . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' id='a__$1' href='" . site_url('transport/delete/$1') . "'>"
                . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
                . lang('delete_transport') . "</a>";
        $action = '<div class="text-center"><div class="btn-group text-left">'
                . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
                . lang('actions') . ' <span class="caret"></span></button>
        <ul class="dropdown-menu pull-right" role="menu">
            
            <li>' . $edit_link . '</li>
            <li>' . $delete_link . '</li>
        </ul>
    </div></div>';

        $this->load->library('datatables');

        $this->datatables
                ->select("id,code,create_date, first_name, last_name, email,phone,transport_name, address, city, mobile,gst_no,IF(igst = 1 , 'Yes', 'No') as igst, status")
                ->from('transport');
//            ->edit_column("fname", "$1 ($2)", "fname, fcode")
//            ->edit_column("tname", "$1 ($2)", "tname, tcode");


        $this->datatables->add_column("Actions", $action, "id");
//            ->unset_column('fcode')
//            ->unset_column('tcode');
        echo $this->datatables->generate();
    }
    
      function transport_actions()
    {
        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {
            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    foreach ($_POST['val'] as $id) {
                        $this->transport_model->deleteTransport($id);
                    }
                    $this->session->set_flashdata('message', lang("transport_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel' || $this->input->post('form_action') == 'export_pdf') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('transport'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('phone'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('transport_name'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('email'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('address'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('city'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('state'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('country'));
                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('postal_code'));
                    $this->excel->getActiveSheet()->SetCellValue('K1', lang('mobile'));
                    $this->excel->getActiveSheet()->SetCellValue('L1', lang('gst_no'));
                    $this->excel->getActiveSheet()->SetCellValue('M1', lang('igst'));
                    $this->excel->getActiveSheet()->SetCellValue('N1', lang('status'));
                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $tansport = $this->transport_model->getTransportData($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($tansport->create_date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $tansport->first_name.' '.$tansport->last_name);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $tansport->phone);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $tansport->transport_name.' - '.$tansport->code);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $tansport->email);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $tansport->address);
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $tansport->city);
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $tansport->state);
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, $tansport->country);
                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $tansport->postal_code);
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, $tansport->mobile);
                        $this->excel->getActiveSheet()->SetCellValue('L' . $row, $tansport->gst_no);
                        $this->excel->getActiveSheet()->SetCellValue('M' . $row, $tansport->igst == 1 ? "Yes" : "NO");
                        $this->excel->getActiveSheet()->SetCellValue('N' . $row, $tansport->status);
                        $row++;
                    }
                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'tansport_' . date('Y_m_d_H_i_s');
                    if ($this->input->post('form_action') == 'export_pdf') {
                        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
                        $this->excel->getDefaultStyle()->applyFromArray($styleArray);
                        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                        require_once(APPPATH . "third_party" . DIRECTORY_SEPARATOR . "MPDF" . DIRECTORY_SEPARATOR . "mpdf.php");
                        $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                        $rendererLibrary = 'MPDF';
                        $rendererLibraryPath = APPPATH . 'third_party' . DIRECTORY_SEPARATOR . $rendererLibrary;
                        if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                            die('Please set the $rendererName: ' . $rendererName . ' and $rendererLibraryPath: ' . $rendererLibraryPath . ' values' .
                                PHP_EOL . ' as appropriate for your directory structure');
                        }

                        header('Content-Type: application/pdf');
                        header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
                        return $objWriter->save('php://output');
                    }
                    if ($this->input->post('form_action') == 'export_excel') {
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                        return $objWriter->save('php://output');
                    }

                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_transport_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }


}
