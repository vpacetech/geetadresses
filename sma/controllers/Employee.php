<?php
    
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->upload_path = 'assets/uploads/';
        $this->image_types = 'jpg|jpeg|png';
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->lang->load('employee', $this->Settings->language);
        $this->load->library('form_validation');
        $this->load->model('companies_model');
        $this->load->model('employee_model');
        $this->load->model('Settings_model');
    }

    function index($action = NULL) {
        $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['action'] = $action;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('employee')));
        $meta = array('page_title' => lang('employee'), 'bc' => $bc);
        $this->page_construct('employee/index', $meta, $this->data);
    }

    public function show()
    {
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['action'] = $action;
        $this->page_construct('employee/show', $meta , $data);
    }

    function AddEmployee() {
//        $this->sma->checkPermissions('add');
        $this->data['store'] = $this->companies_model->getAllBillerCompanies();
        $this->data['department'] = $this->site->department();
        $this->data['employees'] = $this->site->getAllEmployees();
//        $this->data['position'] = $this->Settings_model->getGroups();
        $this->data['position'] = $this->Settings_model->getPosition();
        $this->data['section'] = $this->companies_model->getSection();
        // echo "<pre>";print_r($this->data['section']);exit;
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['action'] = $action;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('add_employee')));
        $meta = array('page_title' => lang('add_employee'), 'bc' => $bc);
        $this->page_construct('employee/AddEmployee', $meta, $this->data);
    }

    function getEmployees() {
        $this->sma->checkPermissions('index');
        $this->load->library('datatables');
//        $emp = $this->db->dbprefix('employee');
//        $dept = $this->db->dbprefix('department');
//        $store = $this->db->dbprefix('companies');
//        $groups = $this->db->dbprefix('position');
//        $rs = $this->datatables->select("$emp.id,$emp.id AS emp_id,$emp.photo, date_format($emp.joining_date, '%d/%m/%Y') as joining_date,CONCAT($emp.fname,' ',$emp.mname,' ',$emp.lname) AS emp_name,$emp.mobile,$store.name as store,$dept.name AS dept_name,$groups.name desigation,$emp.msd,em.fname as refered_by,$emp.incentive,$emp.status", false)
        
//                ->from("employee")
//                ->join("department", "employee.department = department.id", "left")
//                ->join("companies", "employee.store = companies.id", "left")
//                ->join("position", "employee.desigation = position.id", "left")
//                ->join("employee as em", "em.id = employee.refered_by", "left")
        $rs = $this->datatables->select("id,emp_id,photo,joining_date,emp_name,mobile,store,dept_name,desigation,msd,refered_by,incentive,status,gender", false)
                ->from("vw_emp")
                ->add_column("Actions", "<center><a class=\"tip\" title='" . $this->lang->line("edit_employee") . "' href='"
                . site_url('employee/edit/$1') . "''><i class=\"fa fa-edit\">&nbsp;</i></a>"
                . "<a href='#' class='tip po' title='<b>"
                . $this->lang->line("delete_employee") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='"
                . site_url('employee/delete/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no')
                . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\">&nbsp;</i></a>"
                . "<a class=\"tip\" title='" . $this->lang->line("incentive") . " ' href='" . site_url('employee/incentive/$1') . "' data-toggle='modal' data-target='#myModal'>"
                . "<i class=\"fa fa-inr\">&nbsp;</i></a></center>", "id");
        echo $this->datatables->generate();
//        $rs = $this->db->select('id,emp_id,photo,joining_date,emp_name,mobile,store,dept_name,desigation,msd,refered_by,incentive,status')
//                ->from("vw_emp")
//                ->get();
//        echo $this->db->last_query();
    }

    public function incentive($id) {
        $this->data['modal_js'] = $this->site->modal_js();
        $this->data['empdata'] = $this->employee_model->getEmployeeByID($id);

        $this->load->view($this->theme . 'employee/addincentive', $this->data);
    }

    public function addincentive() {
        $_POST = $_GET;

        $id = $_POST['emp_id'];

        $empdata = $this->employee_model->getEmployeeByID($id);
        $type = $this->input->post('type');
        $amount = $this->input->post('amount');
        $data = array(
            'emp_id' => $id,
            'note' => $this->input->post('note'),
            'datetime' => $this->sma->fld(trim($this->input->post('date'))),
        );
        if ($type == "subtraction") {
            $data['pay_incentive'] = $amount;
            $empincentive = floatval($empdata->incentive) - floatval($amount);
        } else {
            $data['insentive_amount'] = $amount;
            $empincentive = floatval($empdata->incentive) + floatval($amount);
        }
        if ($this->employee_model->addincentive($id, $data, $empincentive)) {
            $this->data['success'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->session->set_flashdata('message', $this->lang->line("employee_incentive_added"));
//            $ref = isset($_SERVER["HTTP_REFERER"]) ? explode('?', $_SERVER["HTTP_REFERER"]) : NULL;
//            redirect('employee');
            echo json_encode(array(array('status' => '1', 'msg' => 'success')));
        }
    }


    
    function saveemployee() {
        // echo $this->upload_path; exit;
        /* echo "<pre>"; *//* print_r($_FILES); */
        // print_r($_POST);exit;
        $this->sma->checkPermissions(false, true);
        // $this->form_validation->set_rules('fname', $this->lang->line("fname"), 'required');
        // $this->form_validation->set_rules('mname', $this->lang->line("mname"), 'required');
        // $this->form_validation->set_rules('lname', $this->lang->line("lname"), 'required');
        // $this->form_validation->set_rules('dojoin', $this->lang->line("dojoin"), 'required');
        // $this->form_validation->set_rules('maritalstatus', $this->lang->line("maritalstatus"), 'required');
        // $this->form_validation->set_rules('gender', $this->lang->line("gender"), 'required');
        // $this->form_validation->set_rules('desigation', $this->lang->line("desigation"), 'required');
//        $this->form_validation->set_rules('address1', $this->lang->line("address1"), 'required');
//        $this->form_validation->set_rules('city', $this->lang->line("city"), 'required');
//        $this->form_validation->set_rules('state', $this->lang->line("state"), 'required');
//        $this->form_validation->set_rules('country', $this->lang->line("country"), 'required');
//        $this->form_validation->set_rules('zipcode', $this->lang->line("zipcode"), 'required');
        // $this->form_validation->set_rules('mobileno', $this->lang->line("mobileno"), 'required');
        // $this->form_validation->set_rules('email', $this->lang->line("email"), 'required');
//        $this->form_validation->set_rules('location', $this->lang->line("location"), 'required');
//        $this->form_validation->set_rules('department', $this->lang->line("department"), 'required');
//        $this->form_validation->set_rules('desigation', $this->lang->line("desigation"), 'required');
//        $this->form_validation->set_rules('shift', $this->lang->line("shift"), 'required');
//        $this->form_validation->set_rules('bank_name', $this->lang->line("bank_name"), 'required');
//        $this->form_validation->set_rules('acc_no', $this->lang->line("acc_no"), 'required');
//        $this->form_validation->set_rules('neft', $this->lang->line("neft"), 'required');
//        $this->form_validation->set_rules('bank_branch', $this->lang->line("bank_branch"), 'required');
        if ($this->form_validation->run('employee/saveemployee') == true) {

            // $documentFileCount = count($_FILES['document']['name']);

            if (isset($_FILES['photo']) && !empty($_FILES['photo']['name']) ) 
            {
                $config['upload_path'] = $this->upload_path . 'employment/document';
                $config['allowed_types'] = $this->image_types;
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                $this->upload->initialize($config);

                if ( $this->upload->do_upload('photo') ) {
                    $photo = $this->upload->file_name;
                    // $photo = $_FILES['photo']['name'];
                } else {
                    $$photoError = $this->upload->display_errors();
                }

                // echo $photo;exit;
            }
            
            $check_identity = count(array_filter($_FILES['identity']['name']));
            // echo $check_identity;
            // echo count($_FILES['identity']['name']);exit;

            if ( $check_identity ) 
            {
                // echo "<pre>";print_r($_FILES['identity']);exit;
                $identity_proof = array();
                for ( $i=0; $i < count($_FILES['identity']['name']) ; $i++ ) 
                { 
                    $_FILES['userFile']['name'] = $_FILES['identity']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['identity']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['identity']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['identity']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['identity']['size'][$i];
    
                    $config['upload_path'] = $this->upload_path . 'employment/document';
                    $config['allowed_types'] = $this->image_types;
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config); 

                    // echo $_FILES['identity']['name'][$i];
                    
                    if ( $this->upload->do_upload('userFile') ) {
                        $identity_proof[$i] = $this->upload->file_name;
                        // $photo = $_FILES['photo']['name'];
                        // echo $identity_proof[$i];echo " ";
                    } else {
                        $$identityError = $this->upload->display_errors();
                    }

                }
                // echo "hell";
                $identity_proof_json = json_encode($identity_proof);
                // echo "<pre>";print_r($identity_proof);
                // echo $identity_proof_json; exit;
            }

            $check_references = count(array_filter($_FILES['references']['name']));
            // echo $check_references;exit;

            if ( $check_references ) 
            {
                // echo "<pre>";print_r($_FILES['references']);exit;

                $references = array();
                for ( $i=0; $i < count($_FILES['references']['name']) ; $i++ ) 
                { 
                    $_FILES['userFile']['name'] = $_FILES['references']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['references']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['references']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['references']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['references']['size'][$i];
    
                    $config['upload_path'] = $this->upload_path . 'employment/document';
                    $config['allowed_types'] = $this->image_types;
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config); 

                    // echo $_FILES['references']['name'][$i];
                    
                    if ( $this->upload->do_upload('userFile') ) {
                        $references[$i] = $this->upload->file_name;
                        // $photo = $_FILES['photo']['name'];
                        // echo $references[$i];echo " ";
                    } else {
                        $$referencesError = $this->upload->display_errors();
                    }

                }
                $references_json = json_encode($references);
                // echo "hell";
                // echo "<pre>";print_r($references);
                // echo $references_json; exit;
            }

            $check_yr = count(array_filter($_POST['yr']));
            $checkquali = count(array_filter($_POST['quali']));

            // echo $check_yr;
            // echo $checkquali;
            // if ($check) {
            //     echo "hell";
            //     // ec
            // }

            if ( $check_yr && $checkquali ) 
            {   
                // echo "<pre>";print_r($_POST);

                // print_r(count(array_filter($_POST['yr'])));exit;
                // $quali = array();
                // $special_training = array();
                // $qualification = array();
                // $year = array();
                // echo "<pre>";print_r(count($_POST['quali'][0]));exit;
               
                for ($i=0; $i < count($_POST['quali']) ; $i++) 
                { 
                    $qualification[$i] = $_POST['quali'][$i];
                    $year[$i] = $_POST['yr'][$i];
                    
                    $quali[$i]['quali'] = $qualification[$i];
                    $quali[$i]['yr']    = $year[$i];
                }

                // for ($i=0; $i < count($_POST['special_training']) ; $i++) 
                // { 
                //     $st[$i] = $_POST['special_training'][$i];

                //     $special_training[$i]['special_training'] = $st[$i];
                // }




                // echo json_encode($quali);
                // echo "<pre>";print_r($quali);exit;
            }

            $check_year = count(array_filter($_POST['year']));
            $check_company = count(array_filter($_POST['company']));
            $check_position = count(array_filter($_POST['position']));
                
            // echo $check_year;
            // echo $check_company;
            // echo $check_position;
            // exit;

            if ( ($check_year != 0) and ($check_company != 0) and ($check_position !=0 ) ) 
            {
                // echo "<pre>";print_r($_POST['']);exit;
                $year = array();
                $company = array();
                $position = array();
                $ref_of_comp = array();

                for ($i=0; $i < count($_POST['company']) ; $i++) 
                { 
                    $year[$i]['year']     = $_POST['year'][$i];
                    $company[$i]['company']  = $_POST['company'][$i];
                    $position[$i]['position'] = $_POST['position'][$i];
                    $ref_of_comp[$i]['reference'] = ((isset($_POST['reference'][$i])) ? $_POST['reference'][$i] : "");

                    $exp[$i]['year'] = $year[$i]['year'];
                    $exp[$i]['company'] = $company[$i]['company'];
                    $exp[$i]['postion'] = $position[$i]['position'];
                    $exp[$i]['reference'] = $ref_of_comp[$i]['reference'];
                }
                // echo "<pre>";
                // echo json_encode($exp);
                // print_r($exp);exit;
            }


            // echo "<pre>";print_r($_POST);exit;

            // exit;
/*             if ($documentFileCount > 0) {
                for ($i = 0; $i < $documentFileCount; $i++) {
                    $_FILES['userFile']['name'] = $_FILES['document']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['document']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['document']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['document']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['document']['size'][$i];
                    $config['upload_path'] = $this->upload_path . 'employment/document';
                    $config['allowed_types'] = $this->image_types;
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('userFile')) {
                        $identityFiles[$i]['file'] = $this->upload->file_name;
                    } else {
                        $$identityError = $this->upload->display_errors();
                    }
                    $identityFiles[$i]['docname'] = $_POST['docname'][$i];
                }
            }
            $referencesFileCount = count($_FILES['references']['name']);
            if ($referencesFileCount > 0) {
                for ($i = 0; $i < $referencesFileCount; $i++) {
                    $_FILES['userFile']['name'] = $_FILES['references']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['references']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['references']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['references']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['references']['size'][$i];
                    $config['upload_path'] = $this->upload_path . 'employment/references';
                    $config['allowed_types'] = $this->image_types;
                    $config['encrypt_name'] = TRUE;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('userFile')) {
                        $referencesFiles[$i]['file'] = $this->upload->file_name;
                    } else {
                        $referencesError = $this->upload->display_errors();
                    }
                    $referencesFiles[$i]['name'] = $_POST['refname'][$i];
                }
            }
            $photoFiles = "";
            if ($this->input->post('supplierPhoto') == "") {
                $getpath = $this->session->userdata('camFile');
                if ($getpath != "") {
                    if (file_exists($getpath)) {
                        chmod($getpath, 777);
                        unlink($getpath);
                        $this->session->unset_userdata('camFile');
                    }
                }
                if ($_FILES['photo']['size'] > 0) {
                    $this->load->library('upload');
                    $config['upload_path'] = $this->upload_path;
                    $config['allowed_types'] = $this->image_types;
                    $config['max_size'] = $this->allowed_file_size;
                    $config['overwrite'] = FALSE;
                    $config['encrypt_name'] = TRUE;
                    $config['max_filename'] = 25;
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('photo')) {
                        $error = $this->upload->display_errors();
                        $this->session->set_flashdata('error', $error);
                        redirect("employee/AddEmployee");
                    }
                    $photoFiles = $this->upload->file_name;
                    copy($this->upload_path . $photoFiles, FCPATH . 'assets/uploads/avatars/thumbs/' . $photoFiles);
                } else {
//                    $this->form_validation->set_rules('photo', lang("photo"), 'required');
                }
            } else {
                $photoFiles = $this->input->post('supplierPhoto');
            } */


            // $quali = $this->input->post('quali');
            // $yr = $this->input->post('yr');
            // $special_training = $this->input->post('special_training');
            // $qualification = array(
            //     'quali' => $quali,
            //     'yr' => $yr
            // );
                
            // echo "<pre>";print_r($this->sma->fld(trim($this->input->post('dojoin'))));exit;

            $data = array(
                'joining_date' => $this->sma->fld(trim($this->input->post('dojoin'))),
                'fname' => $this->input->post('fname'),
                'mname' => $this->input->post('mname'),
                'lname' => $this->input->post('lname'),
                'dob' => $this->sma->fld(trim($this->input->post('dob'))),
                'marital_status' => $this->input->post('maritalstatus'),
                'gender' => $this->input->post('gender'),
                // 'smreq' => $this->input->post('smreq') == "1" ? "True" : "False",
                // 'smsapprove' => $this->input->post('smsapprove') == "1" ? "True" : "False",
                // 'smsapprove' => $this->input->post('smsapprove') == "1" ? "True" : "False",
                // 'notemp' => $this->input->post('notemp') == "1" ? "True" : "False",
                // 'invattach' => $this->input->post('invattach'),
                'address' => $this->input->post('address'),
                // 'address2' => $this->input->post('address2'),
                // 'city' => $this->input->post('city'),
                // 'state' => $this->input->post('state'),
                // 'country' => $this->input->post('country'),
                // 'zipcode' => $this->input->post('zipcode'),
                'tele_ph' => $this->input->post('telephoneno'),
                // 'faxno' => $this->input->post('faxno'),
                'mobile' => $this->input->post('mobileno'),
                // 'email' => $this->input->post('email'),
                // 'website' => $this->input->post('website'),
                // 'scp' => $this->input->post('scp'),
                // 'cqp' => $this->input->post('cqp'),
                // 'hlsda' => $this->input->post('hlsda') == "1" ? "True" : "False",
                // 'msd' => $this->input->post('msd'),
                // 'store' => $this->input->post('location'),
                'department' => $this->input->post('department'),
                // 'desigation' => $this->input->post('desigation'),
                'section' => $this->input->post('section'),
                'shift' => $this->input->post('shift'),
                // 'bank_name' => $this->input->post('bank_name'),
                // 'acc_no' => $this->input->post('acc_no'),
                // 'neft' => $this->input->post('neft'),
                // 'bank_branch' => $this->input->post('bank_branch'),
                // 'opening_balance' => $this->input->post('opening_balance'),
                'refered_by' => $this->input->post('refered'),
                'qualification' => json_encode($quali),
                'special_training' => json_encode($this->input->post('special_training')),
                'identity_proof' => $identity_proof_json,
                'references' => $references_json,
                'photo' => $photo,
                'working_exp' => json_encode($exp),
                'salary_of_emp' => $this->input->post('salary'),
                'domicile' => $this->input->post('domicile'),
                'alternet_mob' => $this->input->post('alternet_mob'),
                'position_of_emp' => $this->input->post('position_of_emp'),
                'ref_emp_income' => $this->input->post('ref_emp_income') . " in " . $this->input->post('ref_emp_income_days') . " days",
                'customer_income' => $this->input->post('customer_income') . " in " . $this->input->post('customer_income_days'). " days",
                // 'create_date' => date('Y-m-d H:i:s'),
                'status' => "Active"
            );
            
        } elseif ($this->input->post('add_employee')) {
            $this->session->set_flashdata('error', validation_errors());
            $this->AddEmployee();
        }
        
        if ($this->form_validation->run() == true && $sid = $this->employee_model->addEmployee($data)) {
            $this->load->library('ion_auth');
            list($username, $domain) = explode("@", $this->input->post('email'));
            $email = strtolower($this->input->post('email'));
            $password = $this->input->post('password');
            $active = '1';
            $notify = '1';
            $data['group_id'] = $this->site->getEmpPositions($this->input->post('position_of_emp'))->usergroup;
            $data['emplyee_id'] = $sid;
            $data['first_name'] = $this->input->post('fname');
            $data['last_name'] = $this->input->post('lname');
            $data['avatar'] = $photoFiles;
            $data['phone'] = $this->input->post('mobileno');
            if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $data, $active, $notify)) {
//                $this->session->set_flashdata('message', $this->lang->line("user_added"));
//                redirect("customers");
            }
            $this->data['success'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->session->set_flashdata('message', $this->lang->line("employee_added"));
            $ref = isset($_SERVER["HTTP_REFERER"]) ? explode('?', $_SERVER["HTTP_REFERER"]) : NULL;
            redirect('employee');
        } else {
            echo "<pre>err";print_r(validation_errors());exit;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->AddEmployee();
        }
    }

    function addPostion()
    {
        $this->sma->checkPermissions(false, true);
        $this->data['tax_rates'] = $this->site->getAllTaxRates();
        // $this->data['logos'] = $this->getLogoList();
        // $this->data['logos'] = ["test1.jpg","test2.jpg"];
        $this->data['logos'] = [];
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme . 'employee/addPostion', $this->data);
    }

    function edit($id = NULL) {
//        $this->sma->checkPermissions(false, true);
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $company_details = $this->employee_model->getEmployeeByID($id);
//        if ($this->input->post('email') != $company_details->email) {
//            $this->form_validation->set_rules('code', lang("email_address"), 'is_unique[companies.email]');
//        }
        $this->form_validation->set_rules('fname', $this->lang->line("fname"), 'required');
        $this->form_validation->set_rules('mname', $this->lang->line("mname"), 'required');
        $this->form_validation->set_rules('lname', $this->lang->line("lname"), 'required');
//        $this->form_validation->set_rules('dojoin', $this->lang->line("dojoin"), 'required');
        if ($this->form_validation->run() == true) {
//            echo "<pre>";
//            print_r($_POST);
//            print_r($_FILES);
//            echo "</pre>";
//            die();
            $data = array(
                'joining_date' => $this->sma->fld(trim($this->input->post('dojoin'))),
                'fname' => $this->input->post('fname'),
                'mname' => $this->input->post('mname'),
                'lname' => $this->input->post('lname'),
                'dob' => $this->sma->fld(trim($this->input->post('dob'))),
                'marital_status' => $this->input->post('maritalstatus'),
                'gender' => $this->input->post('gender'),
                'smreq' => $this->input->post('smreq') == "1" ? "True" : "False",
                'smsapprove' => $this->input->post('smsapprove') == "1" ? "True" : "False",
                'smsapprove' => $this->input->post('smsapprove') == "1" ? "True" : "False",
                'notemp' => $this->input->post('notemp') == "1" ? "True" : "False",
                'invattach' => $this->input->post('invattach'),
                'address' => $this->input->post('address1'),
                'address2' => $this->input->post('address2'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'country' => $this->input->post('country'),
                'zipcode' => $this->input->post('zipcode'),
                'tele_ph' => $this->input->post('telephoneno'),
                'faxno' => $this->input->post('faxno'),
                'mobile' => $this->input->post('mobileno'),
                'email' => $this->input->post('email'),
                'website' => $this->input->post('website'),
                'scp' => $this->input->post('scp'),
                'cqp' => $this->input->post('cqp'),
                'hlsda' => $this->input->post('hlsda') == "1" ? "True" : "False",
                'msd' => $this->input->post('msd'),
                'store' => $this->input->post('location'),
                'department' => $this->input->post('department'),
                'desigation' => $this->input->post('desigation'),
                'shift' => $this->input->post('shift'),
                'bank_name' => $this->input->post('bank_name'),
                'opening_balance' => $this->input->post('opening_balance'),
                'acc_no' => $this->input->post('acc_no'),
                'neft' => $this->input->post('neft'),
                'refered_by' => $this->input->post('refered_by'),
                'bank_branch' => $this->input->post('bank_branch'),
                'create_date' => date('Y-m-d H:i:s')
            );
            $documentFileCount = count($_FILES['document']['name']);
//            $identityFiles = json_decode($company_details->identity_proof, TRUE);
            if ($documentFileCount > 0) {
                for ($i = 0; $i < $documentFileCount; $i++) {
                    if ($_FILES['document']['name'][$i] != "") {
                        $_FILES['userFile']['name'] = $_FILES['document']['name'][$i];
                        $_FILES['userFile']['type'] = $_FILES['document']['type'][$i];
                        $_FILES['userFile']['tmp_name'] = $_FILES['document']['tmp_name'][$i];
                        $_FILES['userFile']['error'] = $_FILES['document']['error'][$i];
                        $_FILES['userFile']['size'] = $_FILES['document']['size'][$i];
                        $config['upload_path'] = $this->upload_path . 'employment/document';
                        $config['allowed_types'] = $this->image_types;
                        $config['encrypt_name'] = TRUE;
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        if ($this->upload->do_upload('userFile')) {
                            $identityFiles[$i]['file'] = $this->upload->file_name;
                        } else {
                            $$identityError = $this->upload->display_errors();
                        }
                        $identityFiles[$i]['docname'] = $_POST['docname'][$i];
                    } else {
                        $identityFiles[$i]['docname'] = $_POST['docname'][$i];
                        $identityFiles[$i]['file'] = $_POST['predocname'][$i];
                    }
                }
                $data['identity_proof'] = json_encode($identityFiles);
//                $docfiles = $this->input->post('prefiles');
//                if (!empty($docfiles)) {
//                    $files = array_merge($docfiles, $identityFiles);
//                } else {
//                    $files = $identityFiles;
//                }
            }


            $referencesFileCount = count($_FILES['references']['name']);
//            $referencesFiles = json_decode($company_details->references, TRUE) ;
            if ($referencesFileCount > 0) {
                $referencesFiles = array();
                for ($i = 0; $i < $referencesFileCount; $i++) {
                    if ($_FILES['references']['name'][$i] != "") {
                        $_FILES['userFile']['name'] = $_FILES['references']['name'][$i];
                        $_FILES['userFile']['type'] = $_FILES['references']['type'][$i];
                        $_FILES['userFile']['tmp_name'] = $_FILES['references']['tmp_name'][$i];
                        $_FILES['userFile']['error'] = $_FILES['references']['error'][$i];
                        $_FILES['userFile']['size'] = $_FILES['references']['size'][$i];
                        $config['upload_path'] = $this->upload_path . 'employment/references';
                        $config['allowed_types'] = $this->image_types;
                        $config['encrypt_name'] = TRUE;
                        $this->load->library('upload', $config);
                        $this->upload->initialize($config);
                        if ($this->upload->do_upload('userFile')) {
                            $referencesFiles[$i]['file'] = $this->upload->file_name;
                            $referencesFiles[$i]['name'] = $_POST['refname'][$i];
                        } else {
                            $referencesError = $this->upload->display_errors();
                        }
                    } else {
                        echo '00';
                        $referencesFiles[$i]['name'] = $_POST['refname'][$i];
                        $referencesFiles[$i]['file'] = $_POST['prereffile'][$i];
                    }
                }
                $data['references'] = json_encode($referencesFiles);
//                $prerefattach = $this->input->post('prereffiles');
//                if (!empty($prerefattach)) {
//                    $reffiles = array_merge($prerefattach, $referencesFiles);
//                } else {
//                    $reffiles = $referencesFiles;
//                }
            }


            $photoFiles = "";
            if ($this->input->post('supplierPhoto') == "") {
                $getpath = $this->session->userdata('camFile');
                if ($getpath != "") {
                    if (file_exists($getpath)) {
                        chmod($getpath, 777);
                        unlink($getpath);
                        $this->session->unset_userdata('camFile');
                    }
                }
                if ($_FILES['photo']['size'] > 0) {
                    $this->load->library('upload');
                    $config['upload_path'] = $this->upload_path;
                    $config['allowed_types'] = $this->image_types;
                    $config['max_size'] = $this->allowed_file_size;
                    $config['overwrite'] = FALSE;
                    $config['encrypt_name'] = TRUE;
                    $config['max_filename'] = 25;
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('photo')) {
                        $error = $this->upload->display_errors();
                        $this->session->set_flashdata('error', $error);
                        redirect("employee");
                    }
                    $photoFiles = $this->upload->file_name;
                    copy($this->upload_path . $photoFiles, FCPATH . 'assets/uploads/avatars/thumbs/' . $photoFiles);
                    $data['photo'] = $photoFiles;
                } else {
//                    $this->form_validation->set_rules('photo', lang("photo"), 'required');
                }
            } else if ($this->input->post('supplierPhoto') != "") {
                $photoFiles = $this->input->post('supplierPhoto');
                $data['photo'] = $photoFiles;
            }
            $quali = $this->input->post('quali');
            $yr = $this->input->post('yr');
            $special_training = $this->input->post('special_training');
            $qualification = array(
                'quali' => $quali,
                'yr' => $yr
            );
            $data['special_training'] = json_encode($special_training);
            $data['qualification'] = json_encode($qualification);
        } elseif ($this->input->post('edit_employee')) {
            $this->session->set_flashdata('error', validation_errors());
//            redirect($_SERVER["HTTP_REFERER"]);
            redirect('employee');
        }
        if ($this->form_validation->run() == true && $this->employee_model->updateEmployee($id, $data)) {
            if ($this->input->post('password') != "") {
                $usersInfo = $this->employee_model->getUserIdByEmpID($id);
                $this->load->library('ion_auth');
                $usersdata = array('password' => $this->input->post('password'));
                $this->ion_auth->update($usersInfo->id, $usersdata);
            }
            $image = $data['photo'];
            $data = array(
                'first_name' => $this->input->post('fname'),
                'last_name' => $this->input->post('lname'),
            );
            if ($image != "") {
                $data['avatar'] = $image;
            }

            $data['group_id'] = $this->site->getEmpPositions($this->input->post('desigation'))->usergroup;

            $data1 = array();
            $data1['emplyee_id'] = $id;


            $this->db->update('users', $data, $data1);

            $this->session->set_flashdata('message', $this->lang->line("employee_updated"));
            redirect('employee');
        } else {

            $this->data['employee'] = $company_details;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['store'] = $this->companies_model->getAllBillerCompanies();
            $this->data['department'] = $this->site->department();
            $this->data['section'] = $this->site->sectionBydeptId($company_details->department);
//            $this->data['position'] = $this->Settings_model->getGroups();
            $this->data['position'] = $this->Settings_model->getPosition();
            $this->data['employees'] = $this->site->getAllEmployees();
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['action'] = $action;

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('edit_employee')));
            $meta = array('page_title' => lang('edit_employee'), 'bc' => $bc);
            $this->page_construct('employee/EditEmployee', $meta, $this->data);
        }
    }

    function users($company_id = NULL) {
        $this->sma->checkPermissions(false, true);
        if ($this->input->get('id')) {
            $company_id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();
        $this->data['company'] = $this->companies_model->getCompanyByID($company_id);
        $this->data['users'] = $this->companies_model->getCompanyUsers($company_id);
        $this->load->view($this->theme . 'suppliers/users', $this->data);
    }

    function add_user($company_id = NULL) {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $company_id = $this->input->get('id');
        }
        $company = $this->companies_model->getCompanyByID($company_id);

        $this->form_validation->set_rules('email', $this->lang->line("email_address"), 'is_unique[users.email]');
        $this->form_validation->set_rules('password', $this->lang->line('password'), 'required|min_length[8]|max_length[20]|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', $this->lang->line('confirm_password'), 'required');

        if ($this->form_validation->run('companies/add_user') == true) {
            $active = $this->input->post('status');
            $notify = $this->input->post('notify');
            list($username, $domain) = explode("@", $this->input->post('email'));
            $email = strtolower($this->input->post('email'));
            $password = $this->input->post('password');
            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'phone' => $this->input->post('phone'),
                'gender' => $this->input->post('gender'),
                'company_id' => $company->id,
                'company' => $company->company,
                'group_id' => 3
            );
            $this->load->library('ion_auth');
        } elseif ($this->input->post('add_user')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('suppliers');
        }

        if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data, $active, $notify)) {
            $this->session->set_flashdata('message', $this->lang->line("user_added"));
            redirect("suppliers");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['company'] = $company;
            $this->load->view($this->theme . 'suppliers/add_user', $this->data);
        }
    }

    function import_csv() {
        $this->sma->checkPermissions();
        $this->load->helper('security');
        $this->form_validation->set_rules('csv_file', $this->lang->line("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (DEMO) {
                $this->session->set_flashdata('warning', $this->lang->line("disabled_in_demo"));
                redirect($_SERVER["HTTP_REFERER"]);
            }

            if (isset($_FILES["csv_file"])) /* if($_FILES['userfile']['size'] > 0) */ {

                $this->load->library('upload');

                $config['upload_path'] = 'assets/uploads/csv/';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = '2000';
                $config['overwrite'] = TRUE;

                $this->upload->initialize($config);

                if (!$this->upload->do_upload('csv_file')) {

                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect("suppliers");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen("assets/uploads/csv/" . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5001, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);

                $keys = array('company', 'name', 'email', 'phone', 'address', 'city', 'state', 'postal_code', 'country', 'vat_no', 'cf1', 'cf2', 'cf3', 'cf4', 'cf5', 'cf6');

                $final = array();

                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }
                $rw = 2;
                foreach ($final as $csv) {
                    if ($this->companies_model->getCompanyByEmail($csv['email'])) {
                        $this->session->set_flashdata('error', $this->lang->line("check_employee_email") . " (" . $csv['email'] . "). " . $this->lang->line("supplier_already_exist") . " (" . $this->lang->line("line_no") . " " . $rw . ")");
                        redirect("suppliers");
                    }
                    $rw++;
                }
                foreach ($final as $record) {
                    $record['group_id'] = 4;
                    $record['group_name'] = 'supplier';
                    $data[] = $record;
                }
                //$this->sma->print_arrays($data);
            }
        } elseif ($this->input->post('import')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('customers');
        }

        if ($this->form_validation->run() == true && !empty($data)) {
            if ($this->companies_model->addCompanies($data)) {
                $this->session->set_flashdata('message', $this->lang->line("employee_added"));
                redirect('suppliers');
            }
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'suppliers/import', $this->data);
        }
    }

    function delete($id = NULL) {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->employee_model->deleteEmployee($id)) {
            echo $this->lang->line("employee_deleted");
        } else {
            $this->session->set_flashdata('warning', lang('employee_x_deleted_have_purchases'));
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : site_url('welcome')) . "'; }, 0);</script>");
        }
    }

    function suggestions($term = NULL, $limit = NULL) {
        // $this->sma->checkPermissions('index');
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        $limit = $this->input->get('limit', TRUE);
        $rows['results'] = $this->companies_model->getSupplierSuggestions($term, $limit);
        echo json_encode($rows);
    }

    function getSupplier($id = NULL) {
        // $this->sma->checkPermissions('index');
        $row = $this->companies_model->getCompanyByID($id);
        echo json_encode(array(array('id' => $row->id, 'text' => $row->company)));
    }

    function supplier_actions() {
        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    $error = false;
                    foreach ($_POST['val'] as $id) {
                        if (!$this->companies_model->deleteSupplier($id)) {
                            $error = true;
                        }
                    }
                    if ($error) {
                        $this->session->set_flashdata('warning', lang('suppliers_x_deleted_have_purchases'));
                    } else {
                        $this->session->set_flashdata('message', $this->lang->line("suppliers_deleted"));
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel' || $this->input->post('form_action') == 'export_pdf') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('company'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('email'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('phone'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('address'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('city'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('state'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('postal_code'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('country'));
                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('vat_no'));
                    $this->excel->getActiveSheet()->SetCellValue('K1', lang('scf1'));
                    $this->excel->getActiveSheet()->SetCellValue('L1', lang('scf2'));
                    $this->excel->getActiveSheet()->SetCellValue('M1', lang('scf3'));
                    $this->excel->getActiveSheet()->SetCellValue('N1', lang('scf4'));
                    $this->excel->getActiveSheet()->SetCellValue('O1', lang('scf5'));
                    $this->excel->getActiveSheet()->SetCellValue('P1', lang('scf6'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $customer = $this->site->getCompanyByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $customer->company);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $customer->name);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $customer->email);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $customer->phone);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $customer->address);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $customer->city);
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $customer->state);
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $customer->postal_code);
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, $customer->country);
                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $customer->vat_no);
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, $customer->cf1);
                        $this->excel->getActiveSheet()->SetCellValue('L' . $row, $customer->cf2);
                        $this->excel->getActiveSheet()->SetCellValue('M' . $row, $customer->cf3);
                        $this->excel->getActiveSheet()->SetCellValue('N' . $row, $customer->cf4);
                        $this->excel->getActiveSheet()->SetCellValue('O' . $row, $customer->cf5);
                        $this->excel->getActiveSheet()->SetCellValue('P' . $row, $customer->cf6);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'suppliers_' . date('Y_m_d_H_i_s');
                    if ($this->input->post('form_action') == 'export_pdf') {
                        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
                        $this->excel->getDefaultStyle()->applyFromArray($styleArray);
                        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                        require_once(APPPATH . "third_party" . DIRECTORY_SEPARATOR . "MPDF" . DIRECTORY_SEPARATOR . "mpdf.php");
                        $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                        $rendererLibrary = 'MPDF';
                        $rendererLibraryPath = APPPATH . 'third_party' . DIRECTORY_SEPARATOR . $rendererLibrary;
                        if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                            die('Please set the $rendererName: ' . $rendererName . ' and $rendererLibraryPath: ' . $rendererLibraryPath . ' values' .
                                    PHP_EOL . ' as appropriate for your directory structure');
                        }

                        header('Content-Type: application/pdf');
                        header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
                        return $objWriter->save('php://output');
                    }
                    if ($this->input->post('form_action') == 'export_excel') {
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                        return $objWriter->save('php://output');
                    }

                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_supplier_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function checkuserEmail() {
        $email = $this->input->get('email');
        $data = $this->employee_model->check_email_user($email);
        if ($data) {
            echo 1;
        } else {
            echo 0;
        }
    }

    function employee_actions() {
        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    $error = false;
                    foreach ($_POST['val'] as $id) {
                        if (!$this->companies_model->deleteSupplier($id)) {
                            $error = true;
                        }
                    }
                    if ($error) {
                        $this->session->set_flashdata('warning', lang('suppliers_x_deleted_have_purchases'));
                    } else {
                        $this->session->set_flashdata('message', $this->lang->line("suppliers_deleted"));
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel' || $this->input->post('form_action') == 'export_pdf') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('employee'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('emp_id'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('emp_join_date'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('emp_name'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('emp_phone'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('store'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('department'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('desigation'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('msd'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('refered_by'));
                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('incentive'));
                    $this->excel->getActiveSheet()->SetCellValue('K1', lang('status'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $empployee = $this->employee_model->getEmployeeByID($id);
                        $empployee1 = $this->employee_model->getEmployeeByID($empployee->refered_by);
                        $store = $this->site->get_store($empployee->store);
                        $dept = $this->site->departmentsById($empployee->department);
                        $posi = $this->site->get_positionById($empployee->desigation);

                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $empployee->id);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $this->sma->hrsd($empployee->joining_date));
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $empployee->fname . ' ' . $empployee->mname . ' ' . $empployee->lname);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $empployee->mobile);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $store->name);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $dept->name);
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $posi->name);
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $empployee->msd);
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, $empployee1->fname);
                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $empployee->incentive);
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, $empployee->status);
                        $row++;
                    }
                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                    
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'employee_' . date('Y_m_d_H_i_s');
                    if ($this->input->post('form_action') == 'export_pdf') {
                        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
                        $this->excel->getDefaultStyle()->applyFromArray($styleArray);
                        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                        require_once(APPPATH . "third_party" . DIRECTORY_SEPARATOR . "MPDF" . DIRECTORY_SEPARATOR . "mpdf.php");
                        $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                        $rendererLibrary = 'MPDF';
                        $rendererLibraryPath = APPPATH . 'third_party' . DIRECTORY_SEPARATOR . $rendererLibrary;
                        if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                            die('Please set the $rendererName: ' . $rendererName . ' and $rendererLibraryPath: ' . $rendererLibraryPath . ' values' .
                                    PHP_EOL . ' as appropriate for your directory structure');
                        }

                        header('Content-Type: application/pdf');
                        header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
                        return $objWriter->save('php://output');
                    }
                    if ($this->input->post('form_action') == 'export_excel') {
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                        return $objWriter->save('php://output');
                    }

                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_supplier_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

}
