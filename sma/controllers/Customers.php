<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends MY_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->lang->load('customers', $this->Settings->language);
        $this->load->library('form_validation');
        $this->load->model('companies_model');
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size = '5024';
    }

    function index($action = NULL) {
        $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['action'] = $action;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('customers')));
        $meta = array('page_title' => lang('customers'), 'bc' => $bc);
        $this->page_construct('customers/index', $meta, $this->data);
    }

    function addCustomer() {
        $this->sma->checkPermissions('add');
        $this->data['custgroup'] = $this->companies_model->getAllCustomerGroups();
//        $this->data['store'] = $this->companies_model->getAllBillerCompanies();
        $this->data['user'] = $this->site->getActivatedUsers();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['action'] = $action;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('add_customer')));
        $meta = array('page_title' => lang('add_customer'), 'bc' => $bc);
        $this->page_construct('customers/addCustomer', $meta, $this->data);
    }

    function getCustomers() {
        $this->sma->checkPermissions('index');
        $this->load->library('datatables');
        $this->datatables
                ->select("id, code,logo, name, email, phone, city, customer_group_name, status")
                ->from("companies")
                ->where('group_name', 'customer')
                ->add_column("Actions", "<center><a class=\"tip\" title='" . $this->lang->line("add_bank") . "' href='" . site_url('suppliers/addnewsupplierbank/$1') . "'><i class=\"fa fa-bank\"></i></a> <a class=\"tip\" title='" . $this->lang->line("edit_customer") . "' href='" . site_url('customers/edit/$1') . "'><i class=\"fa fa-edit\"></i></a> "
//                        . "<a class=\"tip\" title='" . $this->lang->line("list_users") . "' href='" . site_url('customers/users/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-users\"></i></a> "
//                        . "<a class=\"tip\" title='" . $this->lang->line("add_user") . "' href='" . site_url('customers/add_user/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-plus-circle\"></i></a> "
                        . "<a href='#' class='tip po' title='<b>" . $this->lang->line("delete_customer") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('customers/delete/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></center>", "id");
        //->unset_column('id');
        echo $this->datatables->generate();
    }

    function add() {
        $this->sma->checkPermissions(false, true);
//        $this->form_validation->set_rules('supliercode', $this->lang->line("customercode"), 'is_unique[companies.code]');
        $this->form_validation->set_rules('mobileno', lang("phone"), 'min_length[10]|max_length[10]');
        
        
        if ($this->form_validation->run('companies/add') == true) {
          
            $cg = $this->site->getCustomerGroupByID($this->input->post('customer_groups'));
            $data = array(
                'group_id' => '3',
                'group_name' => 'customer',
                'customer_group_id' => $this->input->post('customer_groups'),
                'customer_group_name' => $cg->name,
                'code' => $this->input->post('supliercode'),
                'name' => $this->input->post('customerFname') . " " . $this->input->post('customerLname'),
                'vat_no' => $this->input->post('vat_no'),
//                'cstno' => $this->input->post('cst_no'),
                'tinno' => $this->input->post('tin_no'),
                'panno' => $this->input->post('pan_no'),
                'adhar_no' => $this->input->post('adhar_no'),
                // 'bank_name' => $this->input->post('bank_name'),
                // 'acc_no' => $this->input->post('acc_no'),
                // 'neft' => $this->input->post('neft'),
                // 'bank_branch' => $this->input->post('bank_branch'),
                'address' => $this->input->post('addressline1'),
                'address2' => $this->input->post('addressline2'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'country' => $this->input->post('country'),
                'postal_code' => $this->input->post('zipcode'),
                'telphoneno' => $this->input->post('telephoneno'),
                'opening_balance' => $this->input->post('opening_balance'),
                'phone' => $this->input->post('mobileno'),
                'email' => $this->input->post('email'),
                'logo' => $this->input->post('supplierPhoto'),
                'status' => $this->input->post('isactive') == '1' ? "Active" : "Deactive",
//                'password' => $this->input->post('password'),
            );
           

            if ($this->input->post('supplierPhoto') == "") {
                $getpath = $this->session->userdata('camFile');
                if ($getpath != "") {
                    if (file_exists($getpath)) {
                        chmod($getpath, 777);
                        unlink($getpath);
                        $this->session->unset_userdata('camFile');
                    }
                }
                if ($_FILES['photo']['size'] > 0) {
                    $this->load->library('upload');
                    $config['upload_path'] = $this->upload_path;
                    $config['allowed_types'] = $this->image_types;
                    $config['max_size'] = $this->allowed_file_size;
                    $config['overwrite'] = FALSE;
                    $config['encrypt_name'] = FALSE;
                    $config['max_filename'] = 25;
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('photo')) {
                        $error = $this->upload->display_errors();
                        $this->session->set_flashdata('error', $error);
                        redirect("customers/addCustomer");
                    }
                    $file = $this->upload->file_name;
                    $data['logo'] = $file;
                } else {
                    $this->form_validation->set_rules('photo', lang("photo"), 'required');
                }
            } else {
                $data['logo'] = $this->input->post('supplierPhoto');
            }
            $referencesFileCount = count($_FILES['attachment']['name']);
            $referencesFiles = array();
            if ($referencesFileCount > 0) {
                for ($i = 0; $i < $referencesFileCount; $i++) {
                    $_FILES['userFile']['name'] = $_FILES['attachment']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['attachment']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['attachment']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['attachment']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['attachment']['size'][$i];
                    $config['upload_path'] = $this->upload_path;
                    $config['allowed_types'] = $this->file_types;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('userFile')) {
//                        $referencesFiles[] = $this->upload->data();
                        $referencesFiles[] = $this->upload->file_name;
                    } else {
                        $referencesError = $this->upload->display_errors();
                    }
                }
                $data['attachments'] = json_encode($referencesFiles);
            }
            if ($this->form_validation->run() == true && $cid = $this->companies_model->addCompany($data)) {
                $otherdata = array(
                    'cust_id' => $cid,
                    'dateofbirth' => $this->sma->fld(trim($this->input->post('dateofbirth'))),
                    'anniversarydate' => $this->sma->fld(trim($this->input->post('anniversarydate'))),
                    'isloyatlity' => $this->input->post('isloyatlity') == '1' ? "True" : "False",
                    'loyalityno' => $this->input->post('loyalityno'),
                    'enrolldate' => $this->sma->fld(trim($this->input->post('enrolldate'))),
                    'loyalitypoint' => $this->input->post('loyalitypoint'),
                    'loyalty_percent' => $this->input->post('loyalty_percent'),
                    'referby' => $this->input->post('referby'),
                    'ispoint' => $this->input->post('ispoint') == '1' ? "True" : "False",
                    'billaddressline1' => $this->input->post('billaddressline1'),
                    'billaddressline2' => $this->input->post('billaddressline2'),
                    'billcity' => $this->input->post('billcity'),
                    'billstate' => $this->input->post('billstate'),
                    'billcountry' => $this->input->post('billcountry'),
                    'billzipcode' => $this->input->post('billzipcode'),
                    'billtelephoneno' => $this->input->post('billtelephoneno'),
                    'billmobileno' => $this->input->post('billmobileno'),
                    'billemail' => $this->input->post('billemail'),
                    'enrolllimit' => $this->input->post('enrolllimit'),
                    'creaditlimit' => $this->input->post('creaditlimit'),
//                    'fabalance' => $this->input->post('fabalance'),
                    'createdby' => $this->input->post('store'),
                    'sincedate' => $this->sma->fld(trim($this->input->post('sincedate'))),
                );
                $custid = $this->companies_model->add_customer($otherdata);
            }
        } elseif ($this->input->post('add_customer')) {
           
            $this->session->set_flashdata('error', validation_errors());
        }

        if ($this->form_validation->run() == true) {
            $this->session->set_flashdata('message', $this->lang->line("customer_added"));
            $ref = isset($_SERVER["HTTP_REFERER"]) ? explode('?', $_SERVER["HTTP_REFERER"]) : NULL;
            redirect('customers');
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->addCustomer();
        }
    }

    function edit($id = NULL) {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $company_details = $this->companies_model->getCustomerByID($id);

//        if ($this->input->post('email') != $company_details->email) {
        $this->form_validation->set_rules('email', lang("email_address"), 'required');
//        }
//        $this->form_validation->set_rules('mobileno', lang("phone"), 'min_length[10]|max_length[10]');
        if ($this->form_validation->run() == true) {
            $cg = $this->site->getCustomerGroupByID($this->input->post('customer_groups'));
            $data = array(
                'group_id' => '3',
                'group_name' => 'customer',
                'customer_group_id' => $this->input->post('customer_groups'),
                'customer_group_name' => $cg->name,
//                'company' => $this->input->post('store'),
                'code' => $this->input->post('supliercode'),
                'name' => $this->input->post('customerFname') . " " . $this->input->post('customerLname'),
//                'vat_no' => $this->input->post('vat_no'),
                'adhar_no' => $this->input->post('adhar_no'),
//                'cstno' => $this->input->post('cst_no'),
                'tinno' => $this->input->post('tin_no'),
                'panno' => $this->input->post('pan_no'),
                // 'bank_name' => $this->input->post('bank_name'),
                // 'acc_no' => $this->input->post('acc_no'),
                // 'neft' => $this->input->post('neft'),
                // 'bank_branch' => $this->input->post('bank_branch'),
                'address' => $this->input->post('addressline1'),
                'address2' => $this->input->post('addressline2'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'country' => $this->input->post('country'),
                'postal_code' => $this->input->post('zipcode'),
                'telphoneno' => $this->input->post('telephoneno'),
                'opening_balance' => $this->input->post('opening_balance'),
                'phone' => $this->input->post('mobileno'),
                'email' => $this->input->post('email'),
                'logo' => $this->input->post('supplierPhoto'),
                'status' => $this->input->post('isactive') == '1' ? "Active" : "Deactive",
            );
            if ($this->input->post('supplierPhoto') == "") {
                $getpath = $this->session->userdata('camFile');
                if ($getpath != "") {
                    if (file_exists($getpath)) {
                        chmod($getpath, 777);
                        unlink($getpath);
                        $this->session->unset_userdata('camFile');
                    }
                }
                if ($_FILES['photo']['size'] > 0) {
                    $this->load->library('upload');
                    $config['upload_path'] = $this->upload_path;
                    $config['allowed_types'] = $this->image_types;
                    $config['max_size'] = $this->allowed_file_size;
                    $config['overwrite'] = FALSE;
                    $config['encrypt_name'] = TRUE;
                    $config['max_filename'] = 25;
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('photo')) {
                        $error = $this->upload->display_errors();
                        $this->session->set_flashdata('error', $error);
                        redirect("customers/addCustomer");
                    }
                    $file = $this->upload->file_name;
                    $data['logo'] = $file;
                }
            } else if ($this->input->post('supplierPhoto') != "") {
                $data['logo'] = $this->input->post('supplierPhoto');
            }
            $referencesFileCount = count($_FILES['attachment']['name']);
            $referencesFiles = array();
            if ($referencesFileCount > 0) {
                for ($i = 0; $i < $referencesFileCount; $i++) {
                    $_FILES['userFile']['name'] = $_FILES['attachment']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['attachment']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['attachment']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['attachment']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['attachment']['size'][$i];
                    $config['upload_path'] = $this->upload_path;
                    $config['allowed_types'] = $this->file_types;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('userFile')) {
//                        $referencesFiles[] = $this->upload->data();
                        $referencesFiles[] = $this->upload->file_name;
                    } else {
                        $referencesError = $this->upload->display_errors();
                    }
                }
                $preattach = $this->input->post('prefiles');
                if (!empty($preattach)) {
                    $files = array_merge($preattach, $referencesFiles);
                } else {
                    $files = $referencesFiles;
                }

                $data['attachments'] = json_encode($files);
//                $data['attachments'] = json_encode($referencesFiles);
            }
            $cid = $this->companies_model->updateCompany($id, $data);
            $otherdata = array(
                'dateofbirth' => $this->sma->fld(trim($this->input->post('dateofbirth'))),
                'anniversarydate' => $this->sma->fld(trim($this->input->post('anniversarydate'))),
                'isloyatlity' => $this->input->post('isloyatlity') == '1' ? "True" : "False",
                'loyalityno' => $this->input->post('loyalityno'),
                'enrolldate' => $this->sma->fld(trim($this->input->post('enrolldate'))),
                'loyalitypoint' => $this->input->post('loyalitypoint'),
                'loyalty_percent' => $this->input->post('loyalty_percent'),
                'referby' => $this->input->post('referby'),
                'ispoint' => $this->input->post('ispoint') == '1' ? "True" : "False",
                'billaddressline1' => $this->input->post('billaddressline1'),
                'billaddressline2' => $this->input->post('billaddressline2'),
                'billcity' => $this->input->post('billcity'),
                'billstate' => $this->input->post('billstate'),
                'billcountry' => $this->input->post('billcountry'),
                'billzipcode' => $this->input->post('billzipcode'),
                'billtelephoneno' => $this->input->post('billtelephoneno'),
                'billmobileno' => $this->input->post('billmobileno'),
                'billemail' => $this->input->post('billemail'),
                'enrolllimit' => $this->input->post('enrolllimit'),
                'creaditlimit' => $this->input->post('creaditlimit'),
//                'fabalance' => $this->input->post('fabalance'),
                'createdby' => $this->input->post('store'),
                'sincedate' => $this->sma->fld(trim($this->input->post('sincedate'))),
            );
            $custid = $this->companies_model->update_customer($id, $otherdata);
            if ($cid) {
                $this->session->set_flashdata('message', $this->lang->line("customer_edit"));
                $ref = isset($_SERVER["HTTP_REFERER"]) ? explode('?', $_SERVER["HTTP_REFERER"]) : NULL;
                redirect('customers');
            }
        }


//        if ($this->form_validation->run() == true && $this->companies_model->updateCompany($id, $data)) {
//            $this->session->set_flashdata('message', $this->lang->line("customer_updated"));
//            redirect($_SERVER["HTTP_REFERER"]);
//        } 
        else {

            $this->data['customer'] = $company_details;
            $this->data['custgroup'] = $this->companies_model->getAllCustomerGroups();
//            $this->data['store'] = $this->companies_model->getAllBillerCompanies();
            $this->data['user'] = $this->site->getActivatedUsers();
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('edit_customer')));
            $meta = array('page_title' => lang('edit_customer'), 'bc' => $bc);
            $this->page_construct('customers/editCustomer', $meta, $this->data);
        }
    }

    function users($company_id = NULL) {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $company_id = $this->input->get('id');
        }


        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();
        $this->data['company'] = $this->companies_model->getCompanyByID($company_id);
        $this->data['users'] = $this->companies_model->getCompanyUsers($company_id);
        $this->load->view($this->theme . 'customers/users', $this->data);
    }

    function add_user($company_id = NULL) {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $company_id = $this->input->get('id');
        }
        $company = $this->companies_model->getCompanyByID($company_id);

        $this->form_validation->set_rules('email', $this->lang->line("email_address"), 'is_unique[users.email]');
        $this->form_validation->set_rules('password', $this->lang->line('password'), 'required|min_length[8]|max_length[20]|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', $this->lang->line('confirm_password'), 'required');

        if ($this->form_validation->run('companies/add_user') == true) {
            $active = $this->input->post('status');
            $notify = $this->input->post('notify');
            list($username, $domain) = explode("@", $this->input->post('email'));
            $email = strtolower($this->input->post('email'));
            $password = $this->input->post('password');
            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'phone' => $this->input->post('phone'),
                'gender' => $this->input->post('gender'),
                'company_id' => $company->id,
                'company' => $company->company,
                'group_id' => 3
            );
            $this->load->library('ion_auth');
        } elseif ($this->input->post('add_user')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('customers');
        }

        if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data, $active, $notify)) {
            $this->session->set_flashdata('message', $this->lang->line("user_added"));
            redirect("customers");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['company'] = $company;
            $this->load->view($this->theme . 'customers/add_user', $this->data);
        }
    }

    function import_csv() {
        $this->sma->checkPermissions();
        $this->load->helper('security');
        $this->form_validation->set_rules('csv_file', $this->lang->line("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (DEMO) {
                $this->session->set_flashdata('warning', $this->lang->line("disabled_in_demo"));
                redirect($_SERVER["HTTP_REFERER"]);
            }

            if (isset($_FILES["csv_file"])) /* if($_FILES['userfile']['size'] > 0) */ {

                $this->load->library('upload');

                $config['upload_path'] = 'assets/uploads/csv/';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = '2000';
                $config['overwrite'] = TRUE;

                $this->upload->initialize($config);

                if (!$this->upload->do_upload('csv_file')) {

                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect("customers");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen("assets/uploads/csv/" . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5001, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);

                $keys = array('company', 'name', 'email', 'phone', 'address', 'city', 'state', 'postal_code', 'country', 'vat_no', 'cf1', 'cf2', 'cf3', 'cf4', 'cf5', 'cf6');

                $final = array();
                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }
                $rw = 2;
                foreach ($final as $csv) {
                    if ($this->companies_model->getCompanyByEmail($csv['email'])) {
                        $this->session->set_flashdata('error', $this->lang->line("check_customer_email") . " (" . $csv['email'] . "). " . $this->lang->line("customer_already_exist") . " (" . $this->lang->line("line_no") . " " . $rw . ")");
                        redirect("customers");
                    }
                    $rw++;
                }
                foreach ($final as $record) {
                    $record['group_id'] = 3;
                    $record['group_name'] = 'customer';
                    $record['customer_group_id'] = 1;
                    $record['customer_group_name'] = 'General';
                    $data[] = $record;
                }
                //$this->sma->print_arrays($data);
            }
        } elseif ($this->input->post('import')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('customers');
        }

        if ($this->form_validation->run() == true && !empty($data)) {
            if ($this->companies_model->addCompanies($data)) {
                $this->session->set_flashdata('message', $this->lang->line("customers_added"));
                redirect('customers');
            }
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'customers/import', $this->data);
        }
    }

    function delete($id = NULL) {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->input->get('id') == 1) {
            $this->session->set_flashdata('error', lang('customer_x_deleted'));
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : site_url('welcome')) . "'; }, 0);</script>");
        }

        if ($this->companies_model->deleteCustomer($id)) {
            echo $this->lang->line("customer_deleted");
        } else {
            $this->session->set_flashdata('warning', lang('customer_x_deleted_have_sales'));
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : site_url('welcome')) . "'; }, 0);</script>");
        }
    }

    function suggestions($term = NULL, $limit = NULL) {
        // $this->sma->checkPermissions('index');
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
//        if (strlen($term) < 1) {
//            return FALSE;
//        }
        
        $limit = $this->input->get('limit', TRUE);
        $rows['results'] = $this->companies_model->getCustomerSuggestions($term, $limit);
        
        echo json_encode($rows);
    }

    function getCustomer($id = NULL) {
        // $this->sma->checkPermissions('index');
        $row = $this->companies_model->getCompanyByID($id);
        
        echo json_encode(array(array('id' => $row->id, 'text' => ($row->company != '' ? $row->company : $row->name))));
    }

    function get_award_points($id = NULL) {
        $this->sma->checkPermissions('index');
        $row = $this->companies_model->getCompanyByID($id);
        echo json_encode(array('ca_points' => $row->award_points));
    }

    function customer_actions() {
        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    $error = false;
                    foreach ($_POST['val'] as $id) {
                        if (!$this->companies_model->deleteCustomer($id)) {
                            $error = true;
                        }
                    }
                    if ($error) {
                        $this->session->set_flashdata('warning', lang('customers_x_deleted_have_sales'));
                    } else {
                        $this->session->set_flashdata('message', $this->lang->line("customers_deleted"));
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel' || $this->input->post('form_action') == 'export_pdf') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('code'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('email'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('phone'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('address'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('city'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('state'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('postal_code'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('country'));
                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('pan_no'));
                    $this->excel->getActiveSheet()->SetCellValue('K1', lang('adhar_no'));
                    $this->excel->getActiveSheet()->SetCellValue('L1', lang('cg'));
//                    $this->excel->getActiveSheet()->SetCellValue('L1', lang('ccf1'));
//                    $this->excel->getActiveSheet()->SetCellValue('M1', lang('ccf2'));
//                    $this->excel->getActiveSheet()->SetCellValue('N1', lang('ccf3'));
//                    $this->excel->getActiveSheet()->SetCellValue('O1', lang('ccf4'));
//                    $this->excel->getActiveSheet()->SetCellValue('P1', lang('ccf5'));
//                    $this->excel->getActiveSheet()->SetCellValue('Q1', lang('ccf6'));
                    $this->excel->getActiveSheet()->SetCellValue('M1', lang('loyalitypoint'));
                    $this->excel->getActiveSheet()->SetCellValue('N1', lang('csd'));
                    $this->excel->getActiveSheet()->SetCellValue('O1', lang('creditlimit'));
                    $this->excel->getActiveSheet()->SetCellValue('P1', lang('status'));
                    
                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $customer = $this->companies_model->getCustomerByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $customer->code);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $customer->name);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $customer->email);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $customer->phone);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $customer->address);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $customer->city);
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $customer->state);
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $customer->postal_code);
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, $customer->country);
                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $customer->panno);
                        $this->excel->getActiveSheet()->SetCellValue('k' . $row, $customer->adhar_no);
                        $this->excel->getActiveSheet()->SetCellValue('L' . $row, $customer->customer_group_name);
                        $this->excel->getActiveSheet()->SetCellValue('M' . $row, $customer->loyalitypoint);
                        $this->excel->getActiveSheet()->SetCellValue('N' . $row, $customer->sincedate);
                        $this->excel->getActiveSheet()->SetCellValue('O' . $row, $customer->creaditlimit);
                        $this->excel->getActiveSheet()->SetCellValue('P' . $row, $customer->status);
//                        $this->excel->getActiveSheet()->SetCellValue('l' . $row, $customer->cf1);
//                        $this->excel->getActiveSheet()->SetCellValue('M' . $row, $customer->cf2);
//                        $this->excel->getActiveSheet()->SetCellValue('N' . $row, $customer->cf3);
//                        $this->excel->getActiveSheet()->SetCellValue('O' . $row, $customer->cf4);
//                        $this->excel->getActiveSheet()->SetCellValue('P' . $row, $customer->cf5);
//                        $this->excel->getActiveSheet()->SetCellValue('Q' . $row, $customer->cf6);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'customers_' . date('Y_m_d_H_i_s');
                    if ($this->input->post('form_action') == 'export_pdf') {
                        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
                        $this->excel->getDefaultStyle()->applyFromArray($styleArray);
                        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                        require_once(APPPATH . "third_party" . DIRECTORY_SEPARATOR . "MPDF" . DIRECTORY_SEPARATOR . "mpdf.php");
                        $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                        $rendererLibrary = 'MPDF';
                        $rendererLibraryPath = APPPATH . 'third_party' . DIRECTORY_SEPARATOR . $rendererLibrary;
                        if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                            die('Please set the $rendererName: ' . $rendererName . ' and $rendererLibraryPath: ' . $rendererLibraryPath . ' values' .
                                    PHP_EOL . ' as appropriate for your directory structure');
                        }

                        header('Content-Type: application/pdf');
                        header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
                        return $objWriter->save('php://output');
                    }
                    if ($this->input->post('form_action') == 'export_excel') {
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                        return $objWriter->save('php://output');
                    }

                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_customer_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

}
