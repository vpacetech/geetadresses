<?php

class Gst extends MY_Controller {

    function __construct() {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->lang->load('products', $this->Settings->language);
        $this->load->library('form_validation');
        $this->load->model('Gst_model');
        $this->load->model('companies_model');
    }

    function index($action = NULL) {
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['action'] = $action;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('gst')));
        $meta = array('page_title' => lang('gst'), 'bc' => $bc);
        $this->page_construct('gst/index', $meta, $this->data);
    }

    function getgst() {
        $this->sma->checkPermissions('index');
        $this->load->library('datatables');
        $gst = $this->db->dbprefix('gst');
        $dept = $this->db->dbprefix('department');
        $pro = $this->db->dbprefix('product_items');
        $type = $this->db->dbprefix('type');
        $brands = $this->db->dbprefix('brands');
        $design = $this->db->dbprefix('design');
        $style = $this->db->dbprefix('style');
        $size = $this->db->dbprefix('size');

        $rs = $this->datatables->select("$gst.id,$dept.name as department,$pro.name as product_items,"
                        . " $type.name as type, $brands.name as brands,$design.name as design,$style.name as style,$size.name as size,"
                        . "IF(mrp > 0, mrp, CONCAT(mrpfrom, '-', mrpto)),cgst,sgst,hsncode,cess, addupgst", false)
//                . "IF(mrp!='',mrp, CONCAT(mrpfrom, ' - ', mrpto)) AS rate", false)
                ->from("gst")
                ->join("department", "gst.department = department.id", 'left')
                ->join("product_items", "gst.product_items = product_items.id", 'left')
                ->join("type", "gst.type_id = type.id", 'left')
                ->join("brands", "gst.brands = brands.id", 'left')
                ->join("design", "gst.design = design.id", 'left')
                ->join("style", "gst.style = style.id", 'left')
                ->join("size", "gst.size = size.id", 'left')
                ->add_column("Actions", "<center><a class=\"tip\" title='" . $this->lang->line("edit_gst") . "' href='"
                . site_url('gst/edit/$1') . "''><i class=\"fa fa-edit\"></i></a>"
                . "<a href='#' class='tip po' title='<b>"
                . $this->lang->line("delete_gst") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='"
                . site_url('gst/delete/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no')
                . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></center>", "$gst.id");
        echo $this->datatables->generate();
    }

    function add() {
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['action'] = $action;
        $this->data['data'] = $this->input->get();
        $this->data['store'] = $this->site->getAllCompaniesName('biller');
        $this->data['product_para'] = $this->site->getAllParameters('color');
        
        $this->load->view($this->theme . 'gst/add', $this->data);
    }

    function addgst() {
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['action'] = $action;
        $this->data['store'] = $this->site->getAllCompaniesName('biller');
        $this->data['product_para'] = $this->site->getAllParameters('color');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('add_gst')));
        $meta = array('page_title' => lang('add_gst'), 'bc' => $bc);
        $this->page_construct('gst/add_1', $meta, $this->data);
    }

    function savepopup() {
        $data = array(
            'store_id' => $this->input->get('store_idgst'),
            'department' => $this->input->get('departmentgst'),
            'section' => $this->input->get('sectiongst'),
            'product_items' => $this->input->get('product_itemsgst'),
            'type_id' => $this->input->get('type_idgst'),
            'brands' => $this->input->get('brandsgst'),
            'design' => $this->input->get('designgst'),
            'style' => $this->input->get('stylegst'),
            'pattern' => $this->input->get('patterngst'),
            'fitting' => $this->input->get('fittinggst'),
            'fabric' => $this->input->get('fabricgst'),
            'color' => $this->input->get('colorgst'),
            'size' => $this->input->get('sizegst'),
            'mrp' => $this->input->get('mrpgst'),
            'mrpfrom' => $this->input->get('mrpfromgst'),
            'mrpto' => $this->input->get('mrpuptogst'),
            'cgst' => $this->input->get('cgst'),
            'sgst' => $this->input->get('sgst'),
            'hsncode' => $this->input->get('hsncodegst'),
            'cess' => $this->input->get('cessgst'),
            'addupgst' => $this->input->get('addupgst'),
            'status' => 'Active',
        );
        if ($this->Gst_model->getGSTtoHSN($this->input->get('hsncodegst'))) {
            echo json_encode(array('ex' => 'true'));
        } else {
            if ($sid = $this->Gst_model->save($data)) {
                echo json_encode(array('s' => 'true'));
            } else {
                echo json_encode(array('s' => 'False.'));
            }
        }
    }

    function save() {
        $this->form_validation->set_rules('store', 'Store', 'required');
        $this->form_validation->set_rules('department', 'Department', 'required');
        $this->form_validation->set_rules('product_item', 'Product Item', 'required');
        $this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('brand', 'Brand', 'required');
        $this->form_validation->set_rules('design', 'Design', 'required');
        $this->form_validation->set_rules('cgst', 'CGST', 'required');
        $this->form_validation->set_rules('sgst', 'SGST', 'required');
        $this->form_validation->set_rules('hsncode', 'HSN Code', 'required|is_unique[sma_gst.hsncode]');
        $this->form_validation->set_rules('cess', 'Cess', 'required');
        if ($this->form_validation->run() == true) {

            $data = array(
                'store_id' => $this->input->post('store'),
                'department' => $this->input->post('department'),
                'section' => $this->input->post('section'),
                'product_items' => $this->input->post('product_item'),
                'type_id' => $this->input->post('type'),
                'brands' => $this->input->post('brand'),
                'design' => $this->input->post('design'),
                'style' => $this->input->post('style'),
                'pattern' => $this->input->post('pattern'),
                'fitting' => $this->input->post('fitting'),
                'fabric' => $this->input->post('fabric'),
                'color' => $this->input->post('color'),
                'size' => $this->input->post('size'),
                'mrp' => $this->input->post('mrp'),
                'mrpfrom' => $this->input->post('mrpfrom'),
                'mrpto' => $this->input->post('mrpupto'),
                'cgst' => $this->input->post('cgst'),
                'sgst' => $this->input->post('sgst'),
                'hsncode' => $this->input->post('hsncode'),
                'cess' => $this->input->post('cess'),
                'addupgst' => $this->input->post('addupgst'),
                
                'status' => 'Active',
            );
        } elseif ($this->input->post('add_gst')) {

            $this->session->set_flashdata('error', validation_errors());
            redirect('gst');
        }

        if ($this->form_validation->run() == true && $sid = $this->Gst_model->save($data)) {
            $this->session->set_flashdata('message', $this->lang->line("gst_added"));
            $ref = isset($_SERVER["HTTP_REFERER"]) ? explode('?', $_SERVER["HTTP_REFERER"]) : NULL;
            redirect('gst');
        } else {

            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->addgst();
        }
    }

    function delete($id) {
        $sid = $this->Gst_model->delete($id);
        if ($sid) {
            echo $this->lang->line("gst_deleted");
        }
    }

    function edit($id) {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $gst_details = $this->Gst_model->getGstByID($id);
        
        $gstdup = $this->Gst_model->getGstIDHSNCode($id, $this->input->post('hsncode'));
        if ($gstdup) {
            $this->form_validation->set_rules('hsncode', 'HSN Code', 'required|is_unique[sma_gst.hsncode]');
        } else {
            $this->form_validation->set_rules('hsncode', 'HSN Code', 'required');
        }
        $this->form_validation->set_rules('store', 'Store', 'required');
        $this->form_validation->set_rules('department', 'Department', 'required');
        $this->form_validation->set_rules('product_item', 'Product Item', 'required');
        $this->form_validation->set_rules('type', 'Type', 'required');
        $this->form_validation->set_rules('brand', 'Brand', 'required');
        $this->form_validation->set_rules('design', 'Design', 'required');
        $this->form_validation->set_rules('cgst', 'CGST', 'required');
        $this->form_validation->set_rules('sgst', 'SGST', 'required');
        $this->form_validation->set_rules('cess', 'Cess', 'required');
        if ($this->form_validation->run() == true) {
            $data = array(
                'store_id' => $this->input->post('store'),
                'department' => $this->input->post('department'),
                'section' => $this->input->post('section'),
                'product_items' => $this->input->post('product_item'),
                'type_id' => $this->input->post('type'),
                'brands' => $this->input->post('brand'),
                'design' => $this->input->post('design'),
                'style' => $this->input->post('style'),
                'pattern' => $this->input->post('pattern'),
                'fitting' => $this->input->post('fitting'),
                'fabric' => $this->input->post('fabric'),
                'color' => $this->input->post('color'),
                'size' => $this->input->post('size'),
                'mrp' => $this->input->post('mrp'),
                'mrpfrom' => $this->input->post('mrpfrom'),
                'mrpto' => $this->input->post('mrpupto'),
                'cgst' => $this->input->post('cgst'),
                'sgst' => $this->input->post('sgst'),
                'hsncode' => $this->input->post('hsncode'),
                'cess' => $this->input->post('cess'),
                'addupgst' => $this->input->post('addupgst'),
            );
        } elseif ($this->input->post('edit_gst')) {
            $this->session->set_flashdata('error', validation_errors());
//            redirect($_SERVER["HTTP_REFERER"]);
            redirect('gst');
        }
        if ($this->form_validation->run() == true && $this->Gst_model->updateGst($id, $data)) {
            $this->session->set_flashdata('message', $this->lang->line("gst_updated"));
            redirect('gst');
        } else {
            $this->data['gst'] = $gst_details;

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['store'] = $this->companies_model->getAllBillerCompanies();
            $this->data['departments'] = $this->site->getattributesbyid('department', 'store_id', $gst_details->store_id);
            $this->data['sections'] = $this->site->getattributesbyid('section', 'department_id', $gst_details->department);
            $this->data['pro_item'] = $this->site->getattributesbyid('product_items', 'section_id', $gst_details->section);
            $this->data['type'] = $this->site->getattributesbyid('type', 'product_items_id', $gst_details->product_items);
            $this->data['brand'] = $this->site->getattributesbyid('brands', 'type_id', $gst_details->type_id);
            $this->data['design'] = $this->site->getattributesbyid('design', 'brands_id', $gst_details->brands);
            $this->data['style'] = $this->site->getattributesbyid('style', 'design_id', $gst_details->design);
            $this->data['pattern'] = $this->site->getattributesbyid('pattern', 'style_id', $gst_details->style);
            $this->data['fitting'] = $this->site->getattributesbyid('fitting', 'pattern_id', $gst_details->pattern);
            $this->data['fabric'] = $this->site->getattributesbyid('fabric', 'fitting_id', $gst_details->fitting);
            $this->data['size'] = $this->site->getSizebyid($gst_details->department, $gst_details->product_items);
            $this->data['product_para'] = $this->site->getAllParameters('color');
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('edit_gst')));
            $meta = array('page_title' => lang('edit_gst'), 'bc' => $bc);
            $this->page_construct('gst/edit', $meta, $this->data);
        }
    }

    function gst_actions() {
        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    $error = false;
                    foreach ($_POST['val'] as $id) {
                        if (!$this->Gst_model->delete($id)) {
                            $error = true;
                        }
                    }
                    if ($error) {
                        $this->session->set_flashdata('warning', lang('gst_x_deleted'));
                    } else {
                        $this->session->set_flashdata('message', $this->lang->line("gst_deleted"));
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel' || $this->input->post('form_action') == 'export_pdf') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('gst'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('gst_id'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('department'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('product_item'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('type'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('brand'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('design'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('style'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('size'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('mrp'));
                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('cgst'));
                    $this->excel->getActiveSheet()->SetCellValue('K1', lang('sgst'));
                    $this->excel->getActiveSheet()->SetCellValue('L1', lang('hsncode'));
                    $this->excel->getActiveSheet()->SetCellValue('M1', lang('cess'));


                    $row = 2;
                    foreach ($_POST['val'] as $id) {

                        $customer = $this->Gst_model->getgstdetaislByID($id);

                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $customer->gstid);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $customer->department);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $customer->product_items);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $customer->type);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $customer->brands);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $customer->design);
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $customer->style);
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $customer->size);
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, $customer->mrp);
                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $customer->cgst);
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, $customer->sgst);
                        $this->excel->getActiveSheet()->SetCellValue('L' . $row, $customer->hsncode);
                        $this->excel->getActiveSheet()->SetCellValue('M' . $row, $customer->cess);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'GST_' . date('Y_m_d_H_i_s');
                    if ($this->input->post('form_action') == 'export_pdf') {
                        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
                        $this->excel->getDefaultStyle()->applyFromArray($styleArray);
                        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                        require_once(APPPATH . "third_party" . DIRECTORY_SEPARATOR . "MPDF" . DIRECTORY_SEPARATOR . "mpdf.php");
                        $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                        $rendererLibrary = 'MPDF';
                        $rendererLibraryPath = APPPATH . 'third_party' . DIRECTORY_SEPARATOR . $rendererLibrary;
                        if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                            die('Please set the $rendererName: ' . $rendererName . ' and $rendererLibraryPath: ' . $rendererLibraryPath . ' values' .
                                    PHP_EOL . ' as appropriate for your directory structure');
                        }

                        header('Content-Type: application/pdf');
                        header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
                        return $objWriter->save('php://output');
                    }
                    if ($this->input->post('form_action') == 'export_excel') {
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                        return $objWriter->save('php://output');
                    }

                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_gst_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function hsn($warehouse_id = 0) {
        $this->sma->checkPermissions('index');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('hsn')));
        $meta = array('page_title' => lang('hsn'), 'bc' => $bc);
        $this->page_construct('hsn/index', $meta, $this->data);
    }

    function getGstRate() {
        $data = $this->Gst_model->getgstdetaisl();
        echo json_encode($data);
    }

}
