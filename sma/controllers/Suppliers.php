<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Suppliers extends MY_Controller {
    
    function __construct() {
        parent::__construct();
        
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        
        // echo "<pre>";print_r($this->Settings->language);exit();
        $this->config->load('form_validation');
        $this->load->library('upload');

        // $this->load->library('form_validation');

        // $this->lang->load('suppliers', $this->Settings->language);
        // $this->load->library('form_validation');
        $this->load->model('companies_model');
        $this->upload_path = 'assets/uploads/supplier';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size = '1024';
    }

    function index($action = NULL) {
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['action'] = $action;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('suppliers')));
        $meta = array('page_title' => lang('suppliers'), 'bc' => $bc);
        // echo "<pre>";print_r($action);exit;
        // echo "<pre>";print_r($meta);exit;
        $this->page_construct('suppliers/index', $meta, $this->data);
    }

    public function list_new_supplier($action = NULL)
    {
        $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['action'] = $action;

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('suppliers')));
        $meta = array('page_title' => lang('suppliers'), 'bc' => $bc);

        $this->page_construct('suppliers/list_new_supplier', $meta, $this->data);

    }

    public function addSupplier() {

        // echo "<pre>";print_r("hello");exit; 
        $this->sma->checkPermissions('add');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
//        $this->data['supplier_list'] = $this->companies_model->get_supplier_list();
        $this->data['suppliers'] = $this->site->getAllCompaniesUser('supplier');
//        $this->data['store'] = $this->site->getAllCompaniesUser('biller');
        $this->data['store'] = $this->site->getAllCompaniesName('biller');
       
//        $this->data['action'] = $action;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('add_supplier')));
        $meta = array('page_title' => lang('suppliers'), 'bc' => $bc);

        // echo "<pre>";print_r($bc);print_r($meta);exit();
        // echo "<pre>";print_r(lang('add_supplier');exit();
        
        // $this->page_construct('suppliers/addNewSupplier', $meta, $this->data);
        $this->page_construct('suppliers/addSupplier', $meta, $this->data);

    }



    public function addnewsupplier()
    {       


        if( isset($_SESSION['supplier_data']) )
        {
            unset($_SESSION['supplier_data']);
        }
        // $this->sma->checkPermissions('add');
        // $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
//        $this->data['supplier_list'] = $this->companies_model->get_supplier_list();
        $this->data['suppliers'] = $this->site->getAllCompaniesUser('supplier');
//        $this->data['store'] = $this->site->getAllCompaniesUser('biller');
        $this->data['store'] = $this->site->getAllCompaniesName('biller');
       
//        $this->data['action'] = $action;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('addnewsupplier')));
        $meta = array('page_title' => lang('suppliers'), 'bc' => $bc);

        // echo "<pre>";print_r($bc);print_r($meta);exit();
        // echo "<pre>";print_r(lang('add_supplier');exit();
        
        $this->page_construct('suppliers/addNewSupplier', $meta, $this->data);

    }


    public function addnewsupplierbank($uid)
    {   
        // echo $uid;exit;
        // $this->sma->checkPermissions('add');
        // $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
//        $this->data['supplier_list'] = $this->companies_model->get_supplier_list();
        $this->data['banks'] = $this->site->getAllUserBanks($uid);
        $this->data['uid'] = $uid;
//        $this->data['store'] = $this->site->getAllCompaniesUser('biller');
        // $this->data['store'] = $this->site->getAllCompaniesName('biller');
       
//        $this->data['action'] = $action;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('addnewsupplier')));
        $meta = array('page_title' => lang('suppliers'), 'bc' => $bc);

        // echo "<pre>";print_r($bc);print_r($meta);exit();     
        // echo "<pre>";print_r(lang('add_supplier');exit();
        
        $this->page_construct('user_banks/index', $meta, $this->data);
    }

    public function savesupplierbank()
    {
        $x= $this->input->post('isprimary') ? 1 : 0;
        $data = [
            'user_id' => $this->input->post('user_id'),
            'name' => $this->input->post('bank_name'),
            'branch' => $this->input->post('bank_branch'),
            'ifsc' => $this->input->post('ifsc'),
            'account_number' => $this->input->post('acc_no'),
            'isprimary' => $x,
            'isverified' => 0,
            'user_type'=>'supplier'
        ];
        
        
        if ($this->form_validation->run('suppliers/savesupplierbank') == true && $sid = $this->companies_model->addBanks($data)) {
            $this->session->set_flashdata('message', $this->lang->line("supplier bank added"));
            $ref = isset($_SERVER["HTTP_REFERER"]) ? explode('?', $_SERVER["HTTP_REFERER"]) : NULL;
            redirect('suppliers/addnewsupplierbank/'.$this->input->post('user_id'));
        } else {
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            // echo "<pre>"; print_r($this->data['error']);exit;
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('add_supplier_bank')));
            $meta = array('page_title' => lang('suppliers'), 'bc' => $bc);
            $this->page_construct('user_banks/index', $meta, $this->data);
        }
    }
    
    public function print()
    {        $this->page_construct('suppliers/add_new_supplier', $meta, $this->data);

        // $com = $this->db->select('code')->get('sma_companies')->result();
        // $this->load->library('datatables');
/*         $rs = $this->datatables
        ->select("sma_supplierinfo.supplier_id,"
                . "sma_supplierinfo.Supplier_Code as supplier_code,"
                . "sma_supplierinfo.store as store_name,"
                . "sma_suppliercontact.mail as email,"
                . "sma_suppliercontact.mobile_No as phone,"
                . "sma_supplieraddress.city,"
                . "sma_supplieraddress.country,"
                . "sma_suppliergeneral.gstno,"
                . "sma_supplierinfo.Supplier_Discount as discount,"
                . "IF(sma_suppliergeneral.igst = 1 , 'Yes', 'No') as igst,"
                . "sma_supplierref.refered_by,"
                . "sma_supplierinfo.Is_Active")
        ->join('sma_supplierinfo cc','cc.supplier_id = sma_suppliercontact.supplier_id')
        ->join('sma_supplierinfo cc1','cc1.supplier_id = sma_suppliergeneral.supplier_id')
        ->join('sma_supplierinfo cc2','cc2.supplier_id = sma_supplierref.supplier_id')
        ->from("sma_supplierinfo,sma_suppliercontact,sma_suppliergeneral,sma_supplierref,sma_supplieraddress")
        ->add_column("Actions", "<center><a class=\"tip\" title='" . $this->lang->line("add_bank") . "' href='" . site_url('suppliers/addnewsupplierbanks/$1') . "'>"
        . "<i class=\"fa fa-bank\"></i></a> <a class=\"tip\" title='" . $this->lang->line("edit_supplier") . "' href='" . site_url('suppliers/edit/$1') . "'>"
                . "<i class=\"fa fa-edit\"></i></a> <a class=\"tip\" title='" . $this->lang->line("list_users") . "' href='" . site_url('suppliers/users/$1') . "' data-toggle='modal' data-target='#myModal'>"
                . "<a href='#' class='tip po' title='<b>" . $this->lang->line("delete_supplier") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('suppliers/delete/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'>"
                . "<i class=\"fa fa-trash-o\"></i></a></center>", "sma_companies.id"); */
        // $r = $this->db->select('supplier_id')->get('supplierinfo')->row();

//         $rs = $this->db->select('sma_supplierinfo.supplier_id,
//         sma_supplierinfo.Supplier_Code as supplier_code,
//         sma_supplierinfo.store as store_name,
//         sma_suppliercontact.mail as email,
//         sma_suppliercontact.mobile_No as phone,
//         sma_supplieraddress.city,
//         sma_supplieraddress.country,
//         sma_suppliergeneral.gstno,
//         sma_supplierinfo.Supplier_Discount as discount,
//         IF(sma_suppliergeneral.igst = 1 , "Yes", "No") as igst,
//         sma_supplierref.refered_by,
//         sma_supplierinfo.Is_Active')
//         ->join('sma_suppliercontact','sma_supplierinfo.supplier_id = sma_suppliercontact.supplier_id','inner join')
//         ->join('sma_supplieraddress','sma_supplierinfo.supplier_id = sma_supplieraddress.supplier_id','inner join')
//         ->join('sma_suppliergeneral','sma_supplierinfo.supplier_id = sma_suppliergeneral.supplier_id','inner join')
//         ->join('sma_supplierref','sma_supplierinfo.supplier_id = sma_supplierref.supplier_id ')
//  ->from('sma_supplierinfo')->get()->result();


        // $this->datatables->generate();

        // echo "<pre>";print_r($rs);exit;   
    }

    function getSuppliers() {


        $this->sma->checkPermissions('index');
        $this->load->library('datatables');
        $rs = $this->datatables
                ->select("sma_companies.id,"
                        . "sma_companies.code as supplier_code,"
                        . "store.name as store_name,"
                        . "sma_companies.company,"
                        . "sma_companies.email,"
                        . "sma_companies.phone,"
                        . "sma_companies.city,"
                        . "sma_companies.country,"
                        . "sma_companies.gstno,"
                        . "sma_companies.discount,"
                        . "IF(sma_companies.igst = 1 , 'Yes', 'No') as igst,"
                        . "cc.company as cname,"
                        . "sma_companies.status")
                ->join('sma_companies cc','cc.id = sma_companies.refered_by','left')
                ->join('companies store','sma_companies.store_id = store.id','left')
                ->from("sma_companies")
                ->where('sma_companies.group_name', 'supplier')
                ->add_column("Actions", "<center><a class=\"tip\" title='" . $this->lang->line("add_bank") . "' href='" . site_url('suppliers/addnewsupplierbank/$1') . "'>"
                . "<i class=\"fa fa-bank\"></i></a> <a class=\"tip\" title='" . $this->lang->line("edit_supplier") . "' href='" . site_url('suppliers/edit/$1') . "'>"
                        . "<i class=\"fa fa-edit\"></i></a> <a class=\"tip\" title='" . $this->lang->line("list_users") . "' href='" . site_url('suppliers/users/$1') . "' data-toggle='modal' data-target='#myModal'>"
                        . "<a href='#' class='tip po' title='<b>" . $this->lang->line("delete_supplier") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('suppliers/delete/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'>"
                        . "<i class=\"fa fa-trash-o\"></i></a></center>", "sma_companies.id");


        // echo "<pre>";print_r($rs);exit;
        echo $this->datatables->generate();

    }
    

/*     function data()
    {
        $this->load->library('datatables');
        $rs = $this->datatables
                ->select("sma_companies.id,"
                        . "sma_companies.code as supplier_code,"
                        . "store.name as store_name,"
                        . "sma_companies.company,"
                        . "sma_companies.email,"
                        . "sma_companies.phone,"
                        . "sma_companies.city,"
                        . "sma_companies.country,"
                        . "sma_companies.gstno,"
                        . "sma_companies.discount,"
                        . "IF(sma_companies.igst = 1 , 'Yes', 'No') as igst,"
                        . "cc.company as cname,"
                        . "sma_companies.status")
                ->join('sma_companies cc','cc.id = sma_companies.refered_by','left')
                ->join('companies store','sma_companies.store_id = store.id','left')
                ->from("sma_companies")
                ->where('sma_companies.group_name', 'supplier')
                ->add_column("Actions", "<center><a class=\"tip\" title='" . $this->lang->line("add_bank") . "' href='" . site_url('suppliers/addnewsupplierbank/$1') . "'>"
                . "<i class=\"fa fa-bank\"></i></a> <a class=\"tip\" title='" . $this->lang->line("edit_supplier") . "' href='" . site_url('suppliers/edit/$1') . "'>"
                        . "<i class=\"fa fa-edit\"></i></a> <a class=\"tip\" title='" . $this->lang->line("list_users") . "' href='" . site_url('suppliers/users/$1') . "' data-toggle='modal' data-target='#myModal'>"
                        . "<a href='#' class='tip po' title='<b>" . $this->lang->line("delete_supplier") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('suppliers/delete/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'>"
                        . "<i class=\"fa fa-trash-o\"></i></a></center>", "sma_companies.id");


        echo "<pre>";print_r($rs);exit;
        echo $this->datatables->generate();
    } */

/*     public function get_supplier_list()
    {
        
        $this->sma->checkPermissions(false,true);
        $this->load->library('datatables');

        $rs = $this->datatables->select('sma_supplierinfo.supplier_id,
                                   sma_supplierinfo.Supplier_Code as supplier_code,
                                   sma_supplierinfo.store as store_name,
                                   sma_suppliercontact.mail as email,
                                   sma_suppliercontact.mobile_No as phone,
                                   sma_supplieraddress.city,
                                   sma_supplieraddress.country,
                                   sma_suppliergeneral.gstno,
                                   sma_supplierinfo.Supplier_Discount as discount,
                                   IF(sma_suppliergeneral.igst = 1 , "Yes", "No") as igst,
                                   sma_supplierref.refered_by,
                                   sma_supplierinfo.Is_Active')
                                   ->join('sma_suppliercontact','sma_supplierinfo.supplier_id = sma_suppliercontact.supplier_id','inner join')
                                   ->join('sma_supplieraddress','sma_supplierinfo.supplier_id = sma_supplieraddress.supplier_id','inner join')
                                   ->join('sma_suppliergeneral','sma_supplierinfo.supplier_id = sma_suppliergeneral.supplier_id','inner join')
                                   ->join('sma_supplierref','sma_supplierinfo.supplier_id = sma_supplierref.supplier_id ')
                                   ->from("sma_companies")
                            ->from('sma_supplierinfo')
                            ->add_column("Actions", "<center><a class=\"tip\" title='" . $this->lang->line("add_bank") . "' href='" . site_url('suppliers/addnewsupplierbanks/$1') . "'>"
                            . "<i class=\"fa fa-bank\"></i></a> <a class=\"tip\" title='" . $this->lang->line("edit_supplier") . "' href='" . site_url('suppliers/edit/$1') . "'>"
                                    . "<i class=\"fa fa-edit\"></i></a> <a class=\"tip\" title='" . $this->lang->line("list_users") . "' href='" . site_url('suppliers/users/$1') . "' data-toggle='modal' data-target='#myModal'>"
                                    . "<a href='#' class='tip po' title='<b>" . $this->lang->line("delete_supplier") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('suppliers/delete/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'>"
                                    . "<i class=\"fa fa-trash-o\"></i></a></center>", "sma_companies.id"); */


                                // echo "<pre>";print_r($rs);exit;
/*         $rs = $this->datatables
        ->select("sma_supplierinfo.supplier_id,"
                . "sma_supplierinfo.Supplier_Code as supplier_code,"
                . "sma_supplierinfo.store as store_name,"
                . "sma_suppliercontact.mail as email,"
                . "sma_suppliercontact.mobile_No as phone,"
                . "sma_supplieraddress.city,"
                . "sma_supplieraddress.country,"
                . "sma_suppliergeneral.gstno,"
                . "sma_supplierinfo.Supplier_Discount as discount,"
                . "IF(sma_suppliergeneral.igst = 1 , 'Yes', 'No') as igst,"
                . "sma_supplierref.refered_by,"
                . "sma_supplierinfo.Is_Active")
        ->join('sma_supplierinfo cc','cc.supplier_id = sma_suppliercontact.supplier_id')
        ->join('sma_supplierinfo cc1','cc1.supplier_id = sma_suppliergeneral.supplier_id')
        ->join('sma_supplierinfo cc2','cc2.supplier_id = sma_supplierref.supplier_id')
        ->from("sma_supplierinfo,sma_suppliercontact,sma_suppliergeneral,sma_supplierref,sma_supplieraddress")
        ->add_column("Actions", "<center><a class=\"tip\" title='" . $this->lang->line("add_bank") . "' href='" . site_url('suppliers/addnewsupplierbanks/$1') . "'>"
        . "<i class=\"fa fa-bank\"></i></a> <a class=\"tip\" title='" . $this->lang->line("edit_supplier") . "' href='" . site_url('suppliers/edit/$1') . "'>"
                . "<i class=\"fa fa-edit\"></i></a> <a class=\"tip\" title='" . $this->lang->line("list_users") . "' href='" . site_url('suppliers/users/$1') . "' data-toggle='modal' data-target='#myModal'>"
                . "<a href='#' class='tip po' title='<b>" . $this->lang->line("delete_supplier") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('suppliers/delete/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'>"
                . "<i class=\"fa fa-trash-o\"></i></a></center>", "sma_companies.id"); */
        // $r = $this->db->select('supplier_id')->get('supplierinfo')->row();

        // $this->datatables->generate();
/*         echo $this->datatables->generate();

    } */

/*     public function addnewsupplierbanks($supplier_id)
    {
        $this->data['banks'] = $this->site->getAllSupplierBanks($supplier_id);
        $this->data['uid'] = $supplier_id;

        if ($this->data['banks'] == false) {
            echo "<pre>";print_r("helo");exit;
        }
        echo "<pre>";print_r($this->data['banks']);exit;

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('addnewsupplier')));
        $meta = array('page_title' => lang('suppliers'), 'bc' => $bc);

        $this->page_construct('suppliers/supplier_bank', $meta, $this->data);

    } */

    public function savenewsupplier_supplierinfo()
    {
        // print_r($_FILES['aadhar']['name']);exit;
        // echo "hello";
        // $data['sup'] = $_POST; 
        // echo $sup;
        $this->sma->checkPermissions(false, true);
        
        // echo $_FILES['pan']['name'];exit;

        if ( $this->form_validation->run('supplier/supplierinfo') == false ) 
        {
            echo "invalid";
        }
        else 
        {
               if( !empty($_FILES['aadhar']) )
               {
                   $_FILES['userFile']['name'] = $_FILES['aadhar']['name'];
                   $_FILES['userFile']['type'] = $_FILES['aadhar']['type'];
                   $_FILES['userFile']['tmp_name'] = $_FILES['aadhar']['tmp_name'];
                   $_FILES['userFile']['error'] = $_FILES['aadhar']['error'];
                   $_FILES['userFile']['size'] = $_FILES['aadhar']['size'];
                   $config['upload_path'] = $this->upload_path;
                   $config['allowed_types'] = $this->file_types;
                   $this->upload->initialize($config);
                   if ($this->upload->do_upload('userFile'))
                   {
                        $_POST['aadhar'] = $_FILES['aadhar']['name'];
                   }
                   else
                   {
                        $referencesError = $this->upload->display_errors();       
                   }
    
               } 
               else
               {
                    $_POST['aadhar'] = "";
               }
               if( !empty($_FILES['pan']) )
               {
                   $_FILES['userFile']['name'] = $_FILES['pan']['name'];
                   $_FILES['userFile']['type'] = $_FILES['pan']['type'];
                   $_FILES['userFile']['tmp_name'] = $_FILES['pan']['tmp_name'];
                   $_FILES['userFile']['error'] = $_FILES['pan']['error'];
                   $_FILES['userFile']['size'] = $_FILES['pan']['size'];
                   $config['upload_path'] = $this->upload_path;
                   $config['allowed_types'] = $this->file_types;
                   $this->upload->initialize($config);
                   if ($this->upload->do_upload('userFile'))
                   {
                        $_POST['pan'] = $_FILES['pan']['name'];
                   }
                   else
                   {
                        $referencesError = $this->upload->display_errors();       
                   }
               }
               else
               {
                    $_POST['pan'] = "";
               }
            
/*             if(!empty($referencesFiles))    
            {
                $_POST['attachment'] = $referencesFiles[0]; 
            }
            else 
            {
                $_POST['attachment'] = ""; 
            }
 */            
            $this->companies_model->savenewsupplier_supplierinfo($_POST);

            // echo "DONE";

        }
        
    }

    public function savenewsupplier_general()
    {
        $this->sma->checkPermissions(false, true);

/*         if ( $this->form_validation->run() == false ) 
        {
            echo "invalid";
        } */
/*         else 
        {
            // echo "<pre>";print_r($_POST);exit;
            // echo gettype($_POST['IGST']);
        }
*/      $this->companies_model->savenewsupplier_general($_POST);
        echo "<pre>";print_r($_SESSION['supplier_data']);
        // echo "DONE";
    }
    

    public function savenewsupplier_contact()
    {
        $this->sma->checkPermissions(false, true);

        
        if ( $this->form_validation->run('supplier/Contact_form') == false ) 
        {
            echo "invalid";
            // echo validation_errors();
        }
        else 
        {
            $this->companies_model->savenewsupplier_contact($_POST);
        }
        
    }
    
    public function savenewsupplier_address()
    {
        $this->sma->checkPermissions(false, true);
        
        if ($this->form_validation->run('supplier/Addressinfo') == false) 
        {
            echo "invalid";
            
        }
        else 
        {
            // echo "<pre>";print_r($_POST);exit;
            $this->companies_model->savenewsupplier_address($_POST);
            echo "done";
        }
    }

    public function savenewsupplier_photo()
    {
        $this->sma->checkPermissions(false, true);
        
/*         if( isset($_FILES) && !empty($_FILES))
        {
            echo "<pre>";print_r($_FILES);exit;        
        } */
        
        if ( $this->form_validation->run('supplier/photo') == false ) 
        {
            echo "invalid";
        }
        else 
        {

            $_FILES['userFile']['name'] = $_FILES['photo']['name'];
            $_FILES['userFile']['type'] = $_FILES['photo']['type'];
            $_FILES['userFile']['tmp_name'] = $_FILES['photo']['tmp_name'];
            $_FILES['userFile']['error'] = $_FILES['photo']['error'];
            $_FILES['userFile']['size'] = $_FILES['photo']['size'];
            $config['upload_path'] = $this->upload_path;
            $config['allowed_types'] = $this->file_types;
            $this->upload->initialize($config);
            if ($this->upload->do_upload('userFile'))
            {
                $_POST['photo'] = $_FILES['photo']['name'];
                
                $this->companies_model->savenewsupplier_photo($_POST);
            }
            else
            {
                $referencesError = $this->upload->display_errors();       
            }
        }
    }
    public function savenewsupplier_referedby()
    {
        // echo "<pre>";print_r($_POST);
        $this->sma->checkPermissions(false, true);
        
        if ($this->form_validation->run('supplier/referedby') == false) 
        {
            echo "invalid";
        }
        else 
        {
            $this->companies_model->savenewsupplier_referedby($_POST);
        }
    }
    public function savenewsupplier_bank()
    {
        $this->sma->checkPermissions(false, true);
        
        if ($this->form_validation->run('supplier/photo') == false) 
        {
            echo "invalid";
        }
        else 
        {
            $this->companies_model->savenewsupplier_photo($_POST);
        }
    }
    public function savenewsupplier_payment()
    {
        $this->sma->checkPermissions(false, true);
        
        if ($this->form_validation->run('supplier/payment') == false) 
        {
            echo "invalid";
        }
        else 
        {
            $this->companies_model->savenewsupplier_payment($_POST);
        }
    
    }
    
    function savesupplier() {

        // echo "<pre>";print_r($_POST);exit;



        $this->sma->checkPermissions(false, true);
//        $this->form_validation->set_rules('email', $this->lang->line("email_address"), 'is_unique[companies.email]');
        // $this->form_validation->set_rules('email', $this->lang->line("email_address"), 'required');

/*         if ( !empty($_FILES['aadhar']['name']) ) 
        {
            // echo $_FILES['aadhar']['name'];
            $aadhar = $_FILES['aadhar']['name'];
        }
        if ( !empty($_FILES['pan']['name']) ) 
        {
            echo $_FILES['aadhar']['name'];
        }
        if ( !empty($_FILES['photo']['name']) ) 
        {
            echo $_FILES['aadhar']['name'];
        } */

        if ($this->form_validation->run('supplier/addSupplier') == true) {
            $data = array(
                'group_id' => '4',
                'group_name' => 'supplier',
                'name' => $this->input->post('first_Name') . " " . $this->input->post('last_Name'),
                'company' => $this->input->post('Supplier_Name'),
                'code' => $this->input->post('Supplier_Code'),
                'aadhar' => $_FILES['aadhar']['name'] == "" ? "" : $_FILES['aadhar']['name'],
                'pan' => $_FILES['pan']['name'] == "" ? "" : $_FILES['pan']['name'],
                'supplier_photo' => $_FILES['photo']['name'] == "" ? "" : $_FILES['photo']['name'],
                // 'vat_no' => $this->input->post('vat_no'),
                'address' => $this->input->post('Address1'),
                'address2' => $this->input->post('Address2'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('Zip_Code'),
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('mobile_No'),
                'email' => $this->input->post('mail'),
                // 'cf1' => $this->input->post('customefild1'),
                // 'cf2' => $this->input->post('customefild2'),
                // 'cf3' => $this->input->post('customefild3'),
                // 'cf4' => $this->input->post('customefild4'),
                // 'cf5' => $this->input->post('customefild5'),
                // 'cf6' => $this->input->post('customefild6'),
                'discount' => $this->input->post('Supplier_Discount'),
                'conactpname' => $this->input->post('first_Name') . " " . $this->input->post('first_Name'),
                'panno' => $this->input->post('panno'),
                'gstno' => $this->input->post('gstno'),
                'tinno' => "",
                'openingbalance' => "",
                'website' => $this->input->post('website'),
                'telphoneno' => $this->input->post('telephone_no'),
                'faxno' => $this->input->post('fax_no'),
                'preferredsupplier' => $this->input->post('Preferred_Supplier') == 'on' ? '1' : '0',
                'noofdays' => $this->input->post('payment'),
                'bank_name' => "",
                'opening_balance' => "",
                'acc_no' => "",
                'neft' => "",
                'store_id' => $this->input->post('store'),
                'refered_by' => $this->input->post('refered'),
                'bank_branch' => "", 
                'status' => $this->input->post('Is_Active') == 'on' ? "Active" : "Deactive",
            );
/*             if ($this->input->post('supplierPhoto') == "") {
                $getpath = $this->session->userdata('camFile');
                if ($getpath != "") {
                    if (file_exists($getpath)) {
                        chmod($getpath, 777);
                        unlink($getpath);
                        $this->session->unset_userdata('camFile');
                    }
                }
                if ($_FILES['photo']['size'] > 0) {
                    $this->load->library('upload');
                    $config['upload_path'] = $this->upload_path;
                    $config['allowed_types'] = $this->image_types;
                    $config['max_size'] = $this->allowed_file_size;
                    $config['overwrite'] = FALSE;
//                    $config['encrypt_name'] = TRUE;
                    $config['max_filename'] = 25;
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('photo')) {
                        $error = $this->upload->display_errors();
                        $this->session->set_flashdata('error', $error);
                        redirect("suppliers/addSupplier");
                    }
                    $file = $this->upload->file_name;
                    $data['logo'] = $file;
                } else {
                    $this->form_validation->set_rules('photo', lang("photo"), 'required');
                }
            } else {
                $data['logo'] = $this->input->post('supplierPhoto');
            } */

            if ( !empty($_FILES['aadhar']['name']) ) 
            {
                $_FILES['userFile']['name'] = $_FILES['aadhar']['name'];
                $_FILES['userFile']['type'] = $_FILES['aadhar']['type'];
                $_FILES['userFile']['tmp_name'] = $_FILES['aadhar']['tmp_name'];
                $_FILES['userFile']['error'] = $_FILES['aadhar']['error'];
                $_FILES['userFile']['size'] = $_FILES['aadhar']['size'];
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->file_types;
                // $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('userFile')) {
//                        $referencesFiles[] = $this->upload->data();
                    $referencesFiles[] = $this->upload->file_name;
                } else {
                    $referencesError = $this->upload->display_errors();
                }
            }

            if ( !empty($_FILES['pan']['name']) ) 
            {
                $_FILES['userFile']['name'] = $_FILES['pan']['name'];
                $_FILES['userFile']['type'] = $_FILES['pan']['type'];
                $_FILES['userFile']['tmp_name'] = $_FILES['pan']['tmp_name'];
                $_FILES['userFile']['error'] = $_FILES['pan']['error'];
                $_FILES['userFile']['size'] = $_FILES['pan']['size'];
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->file_types;
                // $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('userFile')) {
//                        $referencesFiles[] = $this->upload->data();
                    $referencesFiles[] = $this->upload->file_name;
                } else {
                    $referencesError = $this->upload->display_errors();
                }
            }
            
            if ( !empty($_FILES['photo']['name']) ) 
            {
                $_FILES['userFile']['name'] = $_FILES['photo']['name'];
                $_FILES['userFile']['type'] = $_FILES['photo']['type'];
                $_FILES['userFile']['tmp_name'] = $_FILES['photo']['tmp_name'];
                $_FILES['userFile']['error'] = $_FILES['photo']['error'];
                $_FILES['userFile']['size'] = $_FILES['photo']['size'];
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->file_types;
                // $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if ($this->upload->do_upload('userFile')) {
//                        $referencesFiles[] = $this->upload->data();
                    $referencesFiles[] = $this->upload->file_name;
                } else {
                    $referencesError = $this->upload->display_errors();
                }
            }
            
            // echo "<pre>";print_r($data);exit;
/*             $referencesFileCount = count($_FILES['attachment']['name']);
            $referencesFiles = array();
            if ($referencesFileCount > 0) {
                for ($i = 0; $i < $referencesFileCount; $i++) {
                    $_FILES['userFile']['name'] = $_FILES['attachment']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['attachment']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['attachment']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['attachment']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['attachment']['size'][$i];
                    $config['upload_path'] = $this->upload_path;
                    $config['allowed_types'] = $this->file_types;
//                    $config['overwrite'] = FALSE;
//                    $config['encrypt_name'] = FALSE;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('userFile')) {
//                        $referencesFiles[] = $this->upload->data();
                        $referencesFiles[] = $this->upload->file_name;
                    } else {
                        $referencesError = $this->upload->display_errors();
                    }
                }
                $data['attachments'] = json_encode($referencesFiles);
            } */
        } elseif ($this->input->post('add_supplier')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('suppliers');
        }
        if ($this->form_validation->run('supplier/addSupplier') == true &&  $this->companies_model->addCompany($data) == true) {

            // $this->session->unset_userdata('camFile');
            $this->session->set_flashdata('message', $this->lang->line("supplier_added"));
            $ref = isset($_SERVER["HTTP_REFERER"]) ? explode('?', $_SERVER["HTTP_REFERER"]) : NULL;
            // echo "<pre>";print_r("hi");exit;
            redirect('suppliers');
        } else {
            // echo "<pre>"; print_r(validation_errors());exit;
            $this->data['suppliers'] = $this->site->getAllCompaniesUser('supplier');
            $this->data['store'] = $this->site->getAllCompaniesName('biller');
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
//          $this->data['action'] = $action;
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('addnewsupplier')));
            $meta = array('page_title' => lang('suppliers'), 'bc' => $bc);
            $this->page_construct('suppliers/addNewSupplier', $meta, $this->data);
//            $this->session->set_flashdata('message', (validation_errors() ? validation_errors() : $this->session->flashdata('error')));
//            $ref = isset($_SERVER["HTTP_REFERER"]) ? explode('?', $_SERVER["HTTP_REFERER"]) : NULL;
//            redirect('suppliers/addSupplier');
        }
    }

    function savecamImage() {
        if ($_FILES['webcam']['tmp_name'] != "") {
            $this->load->library('upload');
            $config['upload_path'] = $this->upload_path;
            $config['allowed_types'] = $this->image_types;
            $config['max_size'] = $this->allowed_file_size;
            $config['overwrite'] = FALSE;
//            $config['encrypt_name'] = TRUE;
            $config['max_filename'] = 25;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('webcam')) {
                $error = $this->upload->display_errors();
            }
            $file = $this->upload->file_name;
            echo $file;
            $this->session->set_userdata('camFile', FCPATH . $this->upload_path . $file);
        }
    }

    function add() {
        $this->sma->checkPermissions(false, true);
        $this->form_validation->set_rules('email', $this->lang->line("email_address"), 'is_unique[companies.email]');
        if ($this->form_validation->run('companies/add') == true) {

            $data = array('name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'group_id' => '4',
                'group_name' => 'supplier',
                'company' => $this->input->post('company'),
                'address' => $this->input->post('address'),
                'code' => $this->input->post('code'),
                'vat_no' => $this->input->post('vat_no'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('postal_code'),
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('phone'),
                'cf1' => $this->input->post('cf1'),
                'cf2' => $this->input->post('cf2'),
                'cf3' => $this->input->post('cf3'),
                'cf4' => $this->input->post('cf4'),
                'cf5' => $this->input->post('cf5'),
                'cf6' => $this->input->post('cf6'),
                'discount' => $this->input->post('discount'),
            );
        } elseif ($this->input->post('add_supplier')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('suppliers');
        }
        if ($this->form_validation->run() == true && $sid = $this->companies_model->addCompany($data)) {
            $this->session->set_flashdata('message', $this->lang->line("supplier_added"));
            $ref = isset($_SERVER["HTTP_REFERER"]) ? explode('?', $_SERVER["HTTP_REFERER"]) : NULL;
            redirect($ref[0] . '?supplier=' . $sid);
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->load->view($this->theme . 'suppliers/add', $this->data);
        }
    }
    
    function edit($id = NULL) {
        $this->sma->checkPermissions(false, true);
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $company_details = $this->companies_model->getCompanyByID($id);

//        if ($this->input->post('email') != $company_details->email) {
//            $this->form_validation->set_rules('email', lang("email_address"), 'required');
//        }
//        

        if ($this->input->post('edit_supplier')) {

            $data = array(
                'group_id' => '4',
                'group_name' => 'supplier',
                'name' => $this->input->post('fname') . " " . $this->input->post('lname'),
                'company' => $this->input->post('suppliername'),
                'discount' => $this->input->post('discount'),
                'code' => $this->input->post('supliercode'),
                'vat_no' => $this->input->post('vat_no'),
                'igst' => $this->input->post('igst'),
                'address' => $this->input->post('address1'),
                'address2' => $this->input->post('address2'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('zip_code'),
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('mobileno'),
                'email' => $this->input->post('email'),
                'cf1' => $this->input->post('customefild1'),
                'cf2' => $this->input->post('customefild2'),
                'cf3' => $this->input->post('customefild3'),
                'cf4' => $this->input->post('customefild4'),
                'cf5' => $this->input->post('customefild5'),
                'cf6' => $this->input->post('customefild6'),
//                'discount' => $this->input->post('discount'),
                'conactpname' => $this->input->post('fname') . " " . $this->input->post('lname'),
                'panno' => $this->input->post('pan_no'),
                'gstno' => $this->input->post('gstno'),
                'tinno' => $this->input->post('tin_no'),
                'website' => $this->input->post('website'),
                'preferredsupplier' => $this->input->post('preferredsupplier') == 1 ? '1' : '0',
                'noofdays' => $this->input->post('noofdays'),
                // 'bank_name' => $this->input->post('bank_name'),
                // 'opening_balance' => $this->input->post('opening_balance'),
                // 'acc_no' => $this->input->post('acc_no'),
                // 'neft' => $this->input->post('neft'),
                // 'bank_branch' => $this->input->post('bank_branch'),
                'faxno' => $this->input->post('faxno'),
                'store_id' => $this->input->post('store_id'),
                'refered_by' => $this->input->post('refered_by'),
                'telphoneno' => $this->input->post('telephoneno'),
                'status' => $this->input->post('isactive') == '1' ? "Active" : "Deactive",
            );

/*             if ($_FILES['photo']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
               $config['overwrite'] = FALSE;
               $config['encrypt_name'] = FALSE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('photo')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect("suppliers/addSupplier");
                }
                $file = $this->upload->file_name;
                $data['logo'] = $file;
            }
            if ($this->input->post('supplierPhoto') != "") {
                $data['logo'] = $this->input->post('supplierPhoto');
            } */


            $referencesFileCount = count($_FILES['attachment']['name']);
            $referencesFiles = array();
            if ($referencesFileCount > 0) {
                for ($i = 0; $i < $referencesFileCount; $i++) {
                    $_FILES['userFile']['name'] = $_FILES['attachment']['name'][$i];
                    $_FILES['userFile']['type'] = $_FILES['attachment']['type'][$i];
                    $_FILES['userFile']['tmp_name'] = $_FILES['attachment']['tmp_name'][$i];
                    $_FILES['userFile']['error'] = $_FILES['attachment']['error'][$i];
                    $_FILES['userFile']['size'] = $_FILES['attachment']['size'][$i];
                    $config['upload_path'] = $this->upload_path;
                    $config['allowed_types'] = $this->file_types;
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    if ($this->upload->do_upload('userFile')) {
//                        $referencesFiles[] = $this->upload->data();
                        $referencesFiles[] = $this->upload->file_name;
                    } else {
                        $referencesError = $this->upload->display_errors();
                    }
                }
                
                $preattach = $this->input->post('prefiles');
                if(!empty($preattach)){
                $files = array_merge($preattach, $referencesFiles);
                }else{
                   $files =  $referencesFiles;
                }
                    
                $data['attachments'] = json_encode($files);
                
            }
        } elseif ($this->input->post('edit_supplier')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->input->post('edit_supplier') && $this->companies_model->updateCompany($id, $data)) {
            $this->session->set_flashdata('message', $this->lang->line("supplier_updated"));
//            redirect($_SERVER["HTTP_REFERER"]);
            redirect('suppliers');
        } else {
            $this->data['supplier'] = $company_details;
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
//             $this->data['suppliers'] = $this->site->getAllCompaniesName('supplier');
            $this->data['suppliers'] = $this->site->getAllCompaniesUser('supplier');
             
//             $this->data['store'] = $this->site->getAllCompaniesUser('biller');
             $this->data['store'] = $this->site->getAllCompaniesName('biller');
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('edit_supplier')));
            $meta = array('page_title' => lang('suppliers'), 'bc' => $bc);
            $this->page_construct('suppliers/editSupplier', $meta, $this->data);
        }
    }

    function users($company_id = NULL) {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $company_id = $this->input->get('id');
        }


        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();
        $this->data['company'] = $this->companies_model->getCompanyByID($company_id);
        $this->data['users'] = $this->companies_model->getCompanyUsers($company_id);
        $this->load->view($this->theme . 'suppliers/users', $this->data);
    }

    function add_user($company_id = NULL) {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $company_id = $this->input->get('id');
        }
        $company = $this->companies_model->getCompanyByID($company_id);

        $this->form_validation->set_rules('email', $this->lang->line("email_address"), 'is_unique[users.email]');
        $this->form_validation->set_rules('password', $this->lang->line('password'), 'required|min_length[8]|max_length[20]|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', $this->lang->line('confirm_password'), 'required');

        if ($this->form_validation->run('companies/add_user') == true) {
            $active = $this->input->post('status');
            $notify = $this->input->post('notify');
            list($username, $domain) = explode("@", $this->input->post('email'));
            $email = strtolower($this->input->post('email'));
            $password = $this->input->post('password');
            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'phone' => $this->input->post('phone'),
                'gender' => $this->input->post('gender'),
                'company_id' => $company->id,
                'company' => $company->company,
                'group_id' => 3
            );
            $this->load->library('ion_auth');
        } elseif ($this->input->post('add_user')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('suppliers');
        }

        if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data, $active, $notify)) {
            $this->session->set_flashdata('message', $this->lang->line("user_added"));
            redirect("suppliers");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['company'] = $company;
            $this->load->view($this->theme . 'suppliers/add_user', $this->data);
        }
    }

    function import_csv() {
        $this->sma->checkPermissions();
        $this->load->helper('security');
        $this->form_validation->set_rules('csv_file', $this->lang->line("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (DEMO) {
                $this->session->set_flashdata('warning', $this->lang->line("disabled_in_demo"));
                redirect($_SERVER["HTTP_REFERER"]);
            }

            if (isset($_FILES["csv_file"])) /* if($_FILES['userfile']['size'] > 0) */ {

                $this->load->library('upload');

                $config['upload_path'] = 'assets/uploads/csv/';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = '2000';
                $config['overwrite'] = TRUE;

                $this->upload->initialize($config);

                if (!$this->upload->do_upload('csv_file')) {

                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect("suppliers");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen("assets/uploads/csv/" . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5001, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);

                $keys = array('company', 'name', 'email', 'phone', 'address', 'city', 'state', 'postal_code', 'country', 'vat_no', 'cf1', 'cf2', 'cf3', 'cf4', 'cf5', 'cf6');

                $final = array();

                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }
                $rw = 2;
                foreach ($final as $csv) {
                    if ($this->companies_model->getCompanyByEmail($csv['email'])) {
                        $this->session->set_flashdata('error', $this->lang->line("check_supplier_email") . " (" . $csv['email'] . "). " . $this->lang->line("supplier_already_exist") . " (" . $this->lang->line("line_no") . " " . $rw . ")");
                        redirect("suppliers");
                    }
                    $rw++;
                }
                foreach ($final as $record) {
                    $record['group_id'] = 4;
                    $record['group_name'] = 'supplier';
                    $data[] = $record;
                }
                //$this->sma->print_arrays($data);
            }
        } elseif ($this->input->post('import')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('customers');
        }

        if ($this->form_validation->run() == true && !empty($data)) {
            if ($this->companies_model->addCompanies($data)) {
                $this->session->set_flashdata('message', $this->lang->line("suppliers_added"));
                redirect('suppliers');
            }
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'suppliers/import', $this->data);
        }
    }

    function delete($id = NULL) {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->companies_model->deleteSupplier($id)) {
            echo $this->lang->line("supplier_deleted");
        } else {
            $this->session->set_flashdata('warning', lang('supplier_x_deleted_have_purchases'));
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : site_url('welcome')) . "'; }, 0);</script>");
        }
    }

    function suggestions($term = NULL, $limit = NULL) {
        // $this->sma->checkPermissions('index');
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        $limit = $this->input->get('limit', TRUE);
        $rows['results'] = $this->companies_model->getSupplierSuggestions($term, $limit);
        echo json_encode($rows);
    }

    function getSupplier($id = NULL) {
        // $this->sma->checkPermissions('index');
        $row = $this->companies_model->getCompanyByID($id);
        echo json_encode(array(array('id' => $row->id, 'text' => $row->company)));
    }

    function supplier_actions() {

        // echo "<pre>";print_r($_POST['val']);exit;
        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    $error = false;
                    foreach ($_POST['val'] as $id) {
                        if (!$this->companies_model->deleteSupplier($id)) {
                            $error = true;
                        }
                    }
                    if ($error) {
                        $this->session->set_flashdata('warning', lang('suppliers_x_deleted_have_purchases'));
                    } else {
                        $this->session->set_flashdata('message', $this->lang->line("suppliers_deleted"));
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel' || $this->input->post('form_action') == 'export_pdf') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('company'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('code'));                   
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('email'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('phone'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('address'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('city'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('state'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('postal_code'));
                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('country'));
                    $this->excel->getActiveSheet()->SetCellValue('K1', lang('gst_no'));
                    $this->excel->getActiveSheet()->SetCellValue('L1', lang('pan_no'));
                    $this->excel->getActiveSheet()->SetCellValue('M1', lang('Discount'));
                    $this->excel->getActiveSheet()->SetCellValue('N1', lang('store'));
                    $this->excel->getActiveSheet()->SetCellValue('O1', lang('igst'));
                    $this->excel->getActiveSheet()->SetCellValue('P1', lang('opening_balance'));
                    $this->excel->getActiveSheet()->SetCellValue('Q1', lang('status'));
//                    $this->excel->getActiveSheet()->SetCellValue('N1', lang('scf2'));
//                    $this->excel->getActiveSheet()->SetCellValue('O1', lang('scf3'));
//                    $this->excel->getActiveSheet()->SetCellValue('P1', lang('scf4'));
//                    $this->excel->getActiveSheet()->SetCellValue('Q1', lang('scf5'));
//                    $this->excel->getActiveSheet()->SetCellValue('R1', lang('scf6'));
//                    $this->excel->getActiveSheet()->SetCellValue('S1', lang('Discount'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $supplier = $this->site->getCompanyBywithstoreID($id);
                        
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $supplier->company);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $supplier->name);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $supplier->code);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $supplier->email);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $supplier->phone);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $supplier->address);
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $supplier->city);
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $supplier->state);
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, $supplier->postal_code);
                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $supplier->country);
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, $supplier->gstno);
                        $this->excel->getActiveSheet()->SetCellValue('L' . $row, $supplier->panno);
                        $this->excel->getActiveSheet()->SetCellValue('M' . $row, $supplier->discount.' %');
                        $this->excel->getActiveSheet()->SetCellValue('N' . $row, $supplier->storename);
                        $this->excel->getActiveSheet()->SetCellValue('O' . $row, $supplier->igst);
                        $this->excel->getActiveSheet()->SetCellValue('P' . $row, $supplier->opening_balance);
                        $this->excel->getActiveSheet()->SetCellValue('Q' . $row, $supplier->status);
//                        
//                        $this->excel->getActiveSheet()->SetCellValue('M' . $row, $customer->cf1);
//                        $this->excel->getActiveSheet()->SetCellValue('N' . $row, $customer->cf2);
//                        $this->excel->getActiveSheet()->SetCellValue('O' . $row, $customer->cf3);
//                        $this->excel->getActiveSheet()->SetCellValue('P' . $row, $customer->cf4);
//                        $this->excel->getActiveSheet()->SetCellValue('Q' . $row, $customer->cf5);
//                        $this->excel->getActiveSheet()->SetCellValue('R' . $row, $customer->cf6);
                        
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'suppliers_' . date('Y_m_d_H_i_s');
                    if ($this->input->post('form_action') == 'export_pdf') {
                        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
                        $this->excel->getDefaultStyle()->applyFromArray($styleArray);
                        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                        require_once(APPPATH . "third_party" . DIRECTORY_SEPARATOR . "MPDF" . DIRECTORY_SEPARATOR . "mpdf.php");
                        $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                        $rendererLibrary = 'MPDF';
                        $rendererLibraryPath = APPPATH . 'third_party' . DIRECTORY_SEPARATOR . $rendererLibrary;
                        if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                            die('Please set the $rendererName: ' . $rendererName . ' and $rendererLibraryPath: ' . $rendererLibraryPath . ' values' .
                                    PHP_EOL . ' as appropriate for your directory structure');
                        }

                        header('Content-Type: application/pdf');
                        header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
                        return $objWriter->save('php://output');
                    }
                    if ($this->input->post('form_action') == 'export_excel') {
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                        return $objWriter->save('php://output');
                    }

                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_supplier_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function getNewSupplierCode() {
        $data = $this->companies_model->getNewSupplierCode();
        echo json_encode($data);
    }
    
    function getNewCustomerCode() {
        $data = $this->companies_model->getNewCustomerCode();
        echo json_encode($data);
    }

    function CheckCode() {
        $data = $this->companies_model->getNewSupplierCode($id);
        echo json_encode($data);
    }

}

//test