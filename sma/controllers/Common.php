<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends MY_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
        if ($this->Customer) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->lang->load('purchases', $this->Settings->language);
        $this->load->library('form_validation');
        $this->load->model('purchases_model');
        $this->load->model('companies_model');
        $this->load->model('Site');
        

        $this->digital_upload_path = 'files/';
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size = '1024';
        $this->data['logo'] = true;
    }

    function index($warehouse_id = NULL) {
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        if ($this->Owner || $this->Admin) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        } else {
            $this->data['warehouses'] = NULL;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : NULL;
        }
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('purchases')));
        $meta = array('page_title' => lang('purchases'), 'bc' => $bc);
        $this->page_construct('purchases/index', $meta, $this->data);
    }

    public function getLrNoOld() {
        $rss = $this->db->select('lr_no')->from('purchases')->where('lr_no!=', '')->get()->result();
        $i = 0;
        foreach ($rss as $r) {
            $data[$i] = $r->lr_no;
            $i++;
        }
        $pvr = $this->db->dbprefix('product_received_voucher');
        $trans = $this->db->dbprefix('transport');
        $pur = $this->db->dbprefix('purchases');
        $lr_parcel_rec = $this->db->select($pvr . '.id' . ',' . $pvr . '.lr_no,' . $trans . '.code,' . 'CONCAT_WS(" - ",' . $trans . '.code,' . $pvr . '.lr_no)AS lrno', false)
                        ->from('product_received_voucher')
                        ->where_not_in('CONCAT_WS(" - ", sma_transport.code, sma_product_received_voucher.lr_no)', $data)
                        ->where($pvr . '.sender', $_GET['supplier_id'])
                        ->join('transport', 'product_received_voucher.transport = transport.id')
                        ->get()->result();
        echo json_encode($lr_parcel_rec);
    }


    // Get all LR No: 
    // written by: vpace vishal
    // getLrNoNew function fetches all lr no for selected supplier. No filters applied

    public function getLrNo() {
        // $rss = $this->db->select('lr_no')->from('purchases')->where('lr_no!=', '')->get()->result();
        // $i = 0;
        // foreach ($rss as $r) {
        //     $data[$i] = $r->lr_no;
        //     $i++;
        // }
        $pvr = $this->db->dbprefix('product_received_voucher');
        $trans = $this->db->dbprefix('transport');
        $pur = $this->db->dbprefix('purchases');
        $lr_parcel_rec = $this->db->select($pvr . '.id' . ',' . $pvr . '.lr_no,' . $trans . '.code,' . 'CONCAT_WS(" - ",' . $trans . '.code,' . $pvr . '.lr_no)AS lrno', false)
                        ->from('product_received_voucher')
                        // ->where_not_in('CONCAT_WS(" - ", sma_transport.code, sma_product_received_voucher.lr_no)', $data)
                        ->where($pvr . '.sender', $_GET['supplier_id'])
                        ->join('transport', 'product_received_voucher.transport = transport.id')
                        ->get()->result();
        echo json_encode($lr_parcel_rec);
    }

    public function getAgainstOrderNoAccordingSupplierOld() {
        $rss = $this->db->select('ag_order_no')->from('purchases')->where('ag_order_no!=', '')->get()->result();
        $i = 0;
        foreach ($rss as $r) {
            $data[$i] = $r->ag_order_no;
            $i++;
        }
        $this->db->select('quotes.id,quotes.reference_no,SUM(`sma_quote_items`.`quantity`) AS total_qty');
        $this->db->where('quotes.reference_no!=', '');
        $this->db->where_not_in('quotes.reference_no', $data);
        $this->db->where('quotes.customer_id', $_GET['supplier_id']);
//        $this->db->join('purchases', 'purchases.id = quotes.id', 'right outer');
        $this->db->join('purchases', 'purchases.id = quotes.id', 'left');
        $this->db->join('quote_items', 'quote_items.quote_id = quotes.id');
        $this->db->group_by('quote_items.quote_id');
        $q = $this->db->get('quotes');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $datas[] = $row;
            }
            echo json_encode($datas);
        } else {
//            echo json_encode(array(array('id' => 0, 'label' => lang('no_match_found'))));
        }
    }


    public function getAgainstOrderNoAccordingSupplier() {
        // $rss = $this->db->select('ag_order_no')->from('purchases')->where('ag_order_no!=', '')->get()->result();
        // $i = 0;
        // foreach ($rss as $r) {
        //     $data[$i] = $r->ag_order_no;
        //     $i++;
        // }
        $this->db->select('quotes.id,quotes.reference_no,SUM(`sma_quote_items`.`quantity`) AS total_qty');
        $this->db->where('quotes.reference_no!=', '');
        // $this->db->where_not_in('quotes.reference_no', $data);
        $this->db->where('quotes.customer_id', $_GET['supplier_id']);
//        $this->db->join('purchases', 'purchases.id = quotes.id', 'right outer');
        $this->db->join('purchases', 'purchases.id = quotes.id', 'left');
        $this->db->join('quote_items', 'quote_items.quote_id = quotes.id');
        $this->db->group_by('quote_items.quote_id');
        $q = $this->db->get('quotes');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $datas[] = $row;
            }
            echo json_encode($datas);
        } else {
//            echo json_encode(array(array('id' => 0, 'label' => lang('no_match_found'))));
        }
    }

    public function getSupplierBillNos() {
        $pvr = $this->db->dbprefix('product_received_voucher');
        $trans = $this->db->dbprefix('transport');
        // $pur = $this->db->dbprefix('purchases');
        $lr_parcel_rec = $this->db->select($pvr . '.id' . ',' . $pvr . '.purchase_bill_no, '. $pvr . '.lr_no, '. $trans . '.code,' . 'CONCAT_WS(" - ",' . $trans . '.code,' . $pvr . '.lr_no)AS lrno', false)
                        ->from('product_received_voucher')
                        ->join('transport', 'product_received_voucher.transport = transport.id')
                        ->where($pvr . '.sender', $_GET['supplier_id'])

                        ->where($pvr . '.status', "Active")
                        ->get()->result();
        echo json_encode($lr_parcel_rec);
    }

    public function getAgOrderNosFromParcelVoucer() {
        $pvr = $this->db->dbprefix('received_parcel_ag_order');
        $lr_parcel_rec = $this->db->select($pvr . '.id' . ',' . $pvr . '.received_voucher_id, ' . $pvr . '.ag_order_no', false)
                        ->from('received_parcel_ag_order')
                        ->where($pvr . '.received_voucher_id', $_GET['parcel_received_voucher_id'])
                        ->get()->result();
        echo json_encode($lr_parcel_rec);
    }

    public function getProductsFromOrderNoForListing() {
        $quote_id = $_GET['quote_id'];
        if ($quote_id) {
            $this->data['quote'] = $this->purchases_model->getQuoteByID($quote_id);

            $items = $this->purchases_model->getAllQuoteItems($quote_id);
            $c = rand(100000, 9999999);
            foreach ($items as $item) {
                $row = $this->site->getProductByID($item->product_id);

                if ($row->type == 'combo') {
//                        $combo_items = $this->purchases_model->getProductComboItems($row->id, $warehouse_id);
                    foreach ($combo_items as $citem) {
                        $crow = $this->site->getProductByID($citem->product_id);
                        if (!$crow) {
                            $crow = json_decode('{}');
                            $crow->quantity = 0;
                        } else {
                            unset($crow->details, $crow->product_details);
                        }
                        $crow->discount = $item->discount ? $item->discount : '0';
                        $crow->cost = $crow->cost ? $crow->cost : 0;
                        $crow->tax_rate = $item->tax_rate_id;
                        $crow->real_unit_cost = $crow->cost ? $crow->cost : 0;
                        $crow->expiry = '';
                        $options = $this->purchases_model->getProductOptions($crow->id);

                        $ri = $this->Settings->item_addition ? $crow->id : $c;
                        if ($crow->tax_rate) {
                            $tax_rate = $this->site->getTaxRateByID($crow->tax_rate);
                            $pr[$ri] = array('id' => $c, 'item_id' => $crow->id, 'label' => $crow->name . " (" . $crow->code . ")", 'row' => $crow, 'tax_rate' => $tax_rate, 'options' => $options);
                        } else {
                            $pr[$ri] = array('id' => $c, 'item_id' => $crow->id, 'label' => $crow->name . " (" . $crow->code . ")", 'row' => $crow, 'tax_rate' => false, 'options' => $options);
                        }
                        $c++;
                    }
                } elseif ($row->type == 'standard') {
                    if (!$row) {
                        $row = json_decode('{}');
                        $row->quantity = 0;
                    } else {
                        unset($row->details, $row->product_details);
                    }

                    $row->id = $item->product_id;
                    $row->code = $item->product_code;
                    $row->name = $item->product_name;
                    $row->qty = $item->quantity;
                    $row->option = $item->option_id;
                    $row->discount = $item->discount ? $item->discount : '0';
                    $row->cost = $row->cost ? $row->cost : 0;
                    $row->tax_rate = $item->tax_rate_id;
                    $row->expiry = '';
                    $row->real_unit_cost = $row->cost ? $row->cost : 0;
                    $options = $this->purchases_model->getProductOptions($row->id);

                    $ri = $this->Settings->item_addition ? $row->id : $c;
                    if ($row->tax_rate) {
                        $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                        $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'tax_rate' => $tax_rate, 'options' => $options);
                    } else {
                        $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'tax_rate' => false, 'options' => $options);
                    }
                    $c++;
                }
            }
            echo json_encode($pr);
        }
    }

    public function commonDropdownData() {
        $tab = $this->input->get('tab');
        $key = $this->input->get('key');
        $id = $this->input->get('term');
        $data = $this->Site->getattributesbyid($tab, $key, $id);
        echo json_encode($data);
    }

    public function getSizebyid() {
        $dept = $this->input->get('department');
        $pro_item = $this->input->get('pro_item');
        $data = $this->Site->getSizebyid($dept, $pro_item);
        echo json_encode($data);
    }

    public function supplierDiscount() {
        $rs = $this->db->select('discount')
                        ->where('id', $_GET['supplier_id'])
                        ->from('companies')
                        ->get()->row();
        echo json_encode($rs->discount . '%');
    }
    
    public function checksupplierGst() {
        $rs = $this->db->select('gstno')
                        ->where('id', $_GET['supplier_id'])
                        ->from('companies')
                        ->get()->row();
        if($rs){
            if($rs->gstno != ""){
                echo json_encode(array('result' => TRUE));
            }else{
                echo json_encode(array('result' => FALSE));
            }
        }else{
            echo json_encode(array('result' => FALSE));
        }
//        echo json_encode($rs->discount);
    }

    public function getSupBillPurchase() {
        $challan = $this->input->get('challan');
        $rs = $this->db->select('reference_no')
                        ->where('supplier_id', $_GET['supplier_id'])
                        ->where('pur_challan', $challan)
                        ->from('sma_purchases')
                        ->order_by('id', 'DESC')
                        ->get()->result();
        echo json_encode($rs);
    }

}
