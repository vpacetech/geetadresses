<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Product_para
 *
 * @author Pravinkumar
 */
class Product_para extends MY_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function Department() {
        if ($_GET['t'] == "get") {
            $rs = $this->db->select('id,name,status')->get('department')->result_array();
            $jTableResult = array();
            $jTableResult['Result'] = "OK";
            $jTableResult['Records'] = $rs;
            print json_encode($jTableResult);
        }
        if ($_GET['t'] == "save") {
            $_POST['create_date'] = date('Y-m-d h:i:s');
            $i = $this->db->insert('department', $_POST);
            $rs = $this->db->select('id,name,status')->get_where('department', array('id' => $this->db->insert_id()))->result_array();
            $jTableResult = array();
            $jTableResult['Result'] = "OK";
            $jTableResult['Records'] = $rs;
            print json_encode($jTableResult);
        }
        if ($_GET['t'] == "update") {
            $id = $_POST['id'];
            unset($_POST['id']);
            $i = $this->db->update('department', $_POST, array('id' => $id));
//            $rs = $this->db->select('id,name,status')->get_where('department', array('id' => $this->db->insert_id()))->result_array();
            $jTableResult = array();
            $jTableResult['Result'] = "OK";
//            $jTableResult['Records'] = $rs;
            print json_encode($jTableResult);
        }
        if ($_GET['t'] == "delete") {
            $i = $this->db->delete('department', array('id' => $_POST['id']));
//            $rs = $this->db->select('id,name,status')->get_where('department', array('id' => $this->db->insert_id()))->result_array();
            $jTableResult = array();
            $jTableResult['Result'] = "OK";
//            $jTableResult['Records'] = $rs;
            print json_encode($jTableResult);
        }
    }

}
