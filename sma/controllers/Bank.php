<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bank extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('bank', $this->Settings->language);

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        $this->load->model('bank_model');
        $this->load->model('companies_model');
        $this->load->library('ion_auth');
    }

    function index() {
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('add_bank')));
        $meta = array('page_title' => lang('add_bank'), 'bc' => $bc);
        $this->page_construct('bank/index', $meta, $this->data);
    }

    function Payee() {
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('payee')), array('link' => '#', 'page' => lang('list_payee')));
        $meta = array('page_title' => lang('list_payee'), 'bc' => $bc);
        $this->page_construct('bank/payee_list', $meta, $this->data);
    }

    function Add() {
        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang("access_denied"));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->data['title'] = "Create Bank";
        $this->form_validation->set_rules('username', lang("username"), 'trim|is_unique[users.username]');
//        $this->form_validation->set_rules('email', lang("email"), 'trim|is_unique[users.email]');
        if ($this->form_validation->run() == true) {
            $email = strtolower($this->input->post('email'));
        }
        if ($this->form_validation->run() == true && $this->bank_model->create_bank($this->input->post())) {
            $this->session->set_flashdata('message', $this->lang->line("bank_added"));
            redirect("bank");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('error')));
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('bank'), 'page' => lang('bank')), array('link' => '#', 'page' => lang('add_bank')));
            $meta = array('page_title' => lang('add_bank'), 'bc' => $bc);
            $this->data['bank_code'] = $this->bank_model->getLatestBankCode();
            $this->page_construct('bank/add', $meta, $this->data);
        }
    }

    function AddPayee() {
        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang("access_denied"));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->data['title'] = "Add Payee";
        $this->form_validation->set_rules('account_no', lang("account_no"), 'required');
        if ($this->form_validation->run() == true && $this->bank_model->create_payee($this->input->post())) {
            $this->session->set_flashdata('message', $this->lang->line("payee_added"));
            redirect("bank/Payee");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('error')));
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('bank/payee'), 'page' => lang('payee')), array('link' => '#', 'page' => lang('add_payee')));
            $meta = array('page_title' => lang('add_payee'), 'bc' => $bc);
            $this->data['store'] = $this->companies_model->getAllBillerCompanies();
            $this->data['bank'] = $this->site->getBankData();
            $this->page_construct('bank/addPayee', $meta, $this->data);
        }
    }

    function edit($id = NULL) {
        $this->data['ed_bank'] = $this->bank_model->getBankData($id);
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('bank'), 'page' => lang('bank')), array('link' => '#', 'page' => lang('edit_bank')));
        $meta = array('page_title' => lang('edit_bank'), 'bc' => $bc);
        $this->page_construct('bank/edit', $meta, $this->data);
    }

    function update_bank($id) {
        if ($this->bank_model->update_bank($id, $this->input->post())) {
            $this->session->set_flashdata('message', $this->lang->line("bank_updated"));
            redirect("bank");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('error')));
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('bank'), 'page' => lang('bank')), array('link' => '#', 'page' => lang('add_bank')));
            $meta = array('page_title' => lang('add_bank'), 'bc' => $bc);
            $this->page_construct('bank/add', $meta, $this->data);
        }
    }

    function editPayee($id = NULL) {

        $this->data['ed_payee'] = $this->bank_model->getPayeeData($id);

        $this->data['ifsc'] = $this->bank_model->getifsc($this->data['ed_payee']->bank_id);

        $this->data['store'] = $this->companies_model->getAllBillerCompanies();
        $this->data['bank'] = $this->site->getBankDataToEditPayee();

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('bank/payee'), 'page' => lang('payee')), array('link' => '#', 'page' => lang('edit_payee')));
        $meta = array('page_title' => lang('edit_bank'), 'bc' => $bc);
        $this->page_construct('bank/editPayee', $meta, $this->data);
    }

    function update_payee($id) {
//        $data = $this->input->post();
//        unset($data['add_bank']);
//        
        if ($this->bank_model->update_payee($id, $this->input->post())) {
            $this->session->set_flashdata('message', $this->lang->line("bankPayee_updated"));
            redirect("bank/Payee");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('error')));
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('bank'), 'page' => lang('payee')), array('link' => '#', 'page' => lang('add_bank')));
            $meta = array('page_title' => lang('add_bank'), 'bc' => $bc);

            $this->page_construct('bank/editPayee', $meta, $this->data);
        }
    }

    function pdf($bank_id = NULL, $view = NULL, $save_bufffer = NULL) {
        if ($this->input->get('id')) {
            $bank_id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bank = $this->bank_model->getBankData($bank_id);
        $this->sma->view_rights($bank->created_by);
        $this->data['rows'] = $this->bank_model->getAllTransferItems($bank_id, $bank->status);
        $this->data['from_warehouse'] = $this->site->getWarehouseByID($bank->from_warehouse_id);
        $this->data['to_warehouse'] = $this->site->getWarehouseByID($bank->to_warehouse_id);
        $this->data['transfer'] = $bank;
        $this->data['tid'] = $bank_id;
        $this->data['created_by'] = $this->site->getUser($bank->created_by);
        $name = lang("transfer") . "_" . str_replace('/', '_', $bank->bank_no) . ".pdf";
        $html = $this->load->view($this->theme . 'bank/pdf', $this->data, TRUE);
        if ($view) {
            $this->load->view($this->theme . 'bank/pdf', $this->data);
        } elseif ($save_bufffer) {
            return $this->sma->generate_pdf($html, $name, $save_bufffer);
        } else {
            $this->sma->generate_pdf($html, $name);
        }
    }

    function email($bank_id = NULL) {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $bank_id = $this->input->get('id');
        }
        $bank = $this->bank_model->getBankData($bank_id);
        $this->form_validation->set_rules('to', lang("to") . " " . lang("email"), 'trim|required|valid_email');
        $this->form_validation->set_rules('subject', lang("subject"), 'trim|required');
        $this->form_validation->set_rules('cc', lang("cc"), 'trim');
        $this->form_validation->set_rules('bcc', lang("bcc"), 'trim');
        $this->form_validation->set_rules('note', lang("message"), 'trim');

        if ($this->form_validation->run() == true) {
            $this->sma->view_rights($bank->created_by);
            $to = $this->input->post('to');
            $subject = $this->input->post('subject');
            if ($this->input->post('cc')) {
                $cc = $this->input->post('cc');
            } else {
                $cc = NULL;
            }
            if ($this->input->post('bcc')) {
                $bcc = $this->input->post('bcc');
            } else {
                $bcc = NULL;
            }

            $this->load->library('parser');
            $parse_data = array(
                'reference_number' => $bank->bank_no,
                'site_link' => base_url(),
                'site_name' => $this->Settings->site_name,
                'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo . '" alt="' . $this->Settings->site_name . '"/>'
            );
            $msg = $this->input->post('note');
            $message = $this->parser->parse_string($msg, $parse_data);
            //$name = lang("transfer") . "_" . str_replace('/', '_', $bank->bank_no) . ".pdf";
            //$file_content = $this->pdf($bank_id, NULL, 'S');
            //$attachment = array('file' => $file_content, 'name' => $name, 'mime' => 'application/pdf');
            $attachment = $this->pdf($bank_id, NULL, 'S'); //delete_files($attachment);
        } elseif ($this->input->post('send_email')) {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->session->set_flashdata('error', $this->data['error']);
            redirect($_SERVER["HTTP_REFERER"]);
        }


        if ($this->form_validation->run() == true && $this->sma->send_email($to, $subject, $message, NULL, NULL, $attachment, $cc, $bcc)) {
            delete_files($attachment);
            $this->session->set_flashdata('message', lang("email_sent"));
            redirect("bank");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            if (file_exists('./themes/' . $this->theme . '/views/email_templates/transfer.html')) {
                $bank_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/transfer.html');
            } else {
                $bank_temp = file_get_contents('./themes/default/views/email_templates/transfer.html');
            }
            $this->data['subject'] = array('name' => 'subject',
                'id' => 'subject',
                'type' => 'text',
                'value' => $this->form_validation->set_value('subject', 'Tranfer Order (' . $bank->bank_no . ') from ' . $bank->from_warehouse_name),
            );
            $this->data['note'] = array('name' => 'note',
                'id' => 'note',
                'type' => 'text',
                'value' => $this->form_validation->set_value('note', $bank_temp),
            );
            $this->data['warehouse'] = $this->site->getWarehouseByID($bank->to_warehouse_id);

            $this->data['id'] = $bank_id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'bank/email', $this->data);
        }
    }

    function delete($id = NULL) {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->bank_model->deleteBank($id)) {
            if ($this->input->is_ajax_request()) {
                echo lang("bank_deleted");
                die();
            }
            $this->session->set_flashdata('bank', lang('bank_deleted'));
            redirect('bank');
        }
    }

    function delete_payee($id = NULL) {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        if ($this->bank_model->deletePayee($id)) {
            if ($this->input->is_ajax_request()) {
                echo lang("payee_deleted");
                die();
            }
            $this->session->set_flashdata('bank', lang('payee_deleted'));
            redirect('bank');
        }
    }

    function getBank() {
        $this->sma->checkPermissions('index');

//        $detail_link = anchor('bank/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('transfer_details'), 'data-toggle="modal" data-target="#myModal"');
        $email_link = anchor('bank/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_transfer'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('bank/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_bank'));
        $pdf_link = anchor('bank/pdf/$1', '<i class="fa fa-file-pdf-o"></i> ' . lang('download_pdf'));
        $delete_link = "<a href='#' class='tip po' title='<b>" . lang("delete_bank") . "</b>' data-content=\"<p>"
                . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' id='a__$1' href='" . site_url('bank/delete/$1') . "'>"
                . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
                . lang('delete_bank') . "</a>";
        $action = '<div class="text-center"><div class="btn-group text-left">'
                . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
                . lang('actions') . ' <span class="caret"></span></button>
        <ul class="dropdown-menu pull-right" role="menu">
            
            <li>' . $edit_link . '</li>
            <li>' . $delete_link . '</li>
        </ul>
    </div></div>';
        $this->load->library('datatables');
        $this->datatables
                ->select("id,bank_code,bank_name,ifsc_code,address_1, city,state,pin_code,mobile,email,status")
                ->from('bank');
        $this->datatables->add_column("Actions", $action, "id");
        echo $this->datatables->generate();
    }

    function getPayee() {
        $this->sma->checkPermissions('index');

//        $detail_link = anchor('bank/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('transfer_details'), 'data-toggle="modal" data-target="#myModal"');
        $email_link = anchor('bank/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_transfer'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('bank/editPayee/$1', '<i class="fa fa-edit"></i> ' . lang('edit'));
        $pdf_link = anchor('bank/pdf/$1', '<i class="fa fa-file-pdf-o"></i> ' . lang('delete') . ' ' . lang('download_pdf'));
        $delete_link = "<a href='#' class='tip po' title='<b>" . lang('delete') . ' ' . lang("payee") . "</b>' data-content=\"<p>"
                . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' id='a__$1' href='" . site_url('bank/delete_payee/$1') . "'>"
                . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
                . lang('delete') . "</a>";
        $action = '<div class="text-center"><div class="btn-group text-left">'
                . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
                . lang('actions') . ' <span class="caret"></span></button>
        <ul class="dropdown-menu pull-right" role="menu">
            
            <li>' . $edit_link . '</li>
            <li>' . $delete_link . '</li>
        </ul>
    </div></div>';

        $payee = $this->db->dbprefix('payee');

        $this->load->library('datatables');
        $this->datatables->select('sma_payee.id,sma_payee.create_date,sma_companies.name,sma_bank.bank_name,sma_payee.ifsc_code,sma_payee.account_no,sma_payee.opening_balance,sma_payee.status')
                ->join('sma_bank', 'sma_payee.bank_id = sma_bank.id')
                ->join('sma_companies', 'sma_payee.store_id = sma_companies.id')
                ->from('sma_payee');

        $this->datatables->add_column("Actions", $action, "sma_payee.id");
        echo $this->datatables->generate();
    }

    function bank_actions() {
        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {
            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    foreach ($_POST['val'] as $id) {
                        $this->bank_model->deleteBank($id);
                    }
                    $this->session->set_flashdata('message', lang("bank_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel' || $this->input->post('form_action') == 'export_pdf') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('bank'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('bank_code'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('bank_name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('ifsc'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('address_1'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('city'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('state'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('country'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('pin_code'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('mobile'));
                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('email'));
                    $this->excel->getActiveSheet()->SetCellValue('K1', lang('status'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $bank = $this->bank_model->getBankData($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $bank->bank_code);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $bank->bank_name);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $bank->ifsc_code);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $bank->address_1);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $bank->city);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $bank->state);
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $bank->country);
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $bank->pin_code);
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, $bank->mobile);
                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $bank->email);
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, $bank->status);
                        $row++;
                    }
                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'bank_' . date('Y_m_d_H_i_s');
                    if ($this->input->post('form_action') == 'export_pdf') {
                        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
                        $this->excel->getDefaultStyle()->applyFromArray($styleArray);
                        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                        require_once(APPPATH . "third_party" . DIRECTORY_SEPARATOR . "MPDF" . DIRECTORY_SEPARATOR . "mpdf.php");
                        $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                        $rendererLibrary = 'MPDF';
                        $rendererLibraryPath = APPPATH . 'third_party' . DIRECTORY_SEPARATOR . $rendererLibrary;
                        if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                            die('Please set the $rendererName: ' . $rendererName . ' and $rendererLibraryPath: ' . $rendererLibraryPath . ' values' .
                                    PHP_EOL . ' as appropriate for your directory structure');
                        }

                        header('Content-Type: application/pdf');
                        header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
                        return $objWriter->save('php://output');
                    }
                    if ($this->input->post('form_action') == 'export_excel') {
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                        return $objWriter->save('php://output');
                    }

                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_bank_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function payee_actions() {
        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {
            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    foreach ($_POST['val'] as $id) {
                        $this->bank_model->deletePayee($id);
                    }
                    $this->session->set_flashdata('message', lang("payee_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel' || $this->input->post('form_action') == 'export_pdf') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('bank'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('added_on'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('store_name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('bank_name'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('ifsc_code'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('account_no'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('opening_balance'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $bank = $this->bank_model->getPayeeDetails($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $bank->create_date);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $bank->comp_name);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $bank->bank_name);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $bank->ifsc_code);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $bank->account_no);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $bank->opening_balance);
                        $row++;
                    }
                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'payee_' . date('Y_m_d_H_i_s');
                    if ($this->input->post('form_action') == 'export_pdf') {
                        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
                        $this->excel->getDefaultStyle()->applyFromArray($styleArray);
                        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                        require_once(APPPATH . "third_party" . DIRECTORY_SEPARATOR . "MPDF" . DIRECTORY_SEPARATOR . "mpdf.php");
                        $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                        $rendererLibrary = 'MPDF';
                        $rendererLibraryPath = APPPATH . 'third_party' . DIRECTORY_SEPARATOR . $rendererLibrary;
                        if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                            die('Please set the $rendererName: ' . $rendererName . ' and $rendererLibraryPath: ' . $rendererLibraryPath . ' values' .
                                    PHP_EOL . ' as appropriate for your directory structure');
                        }

                        header('Content-Type: application/pdf');
                        header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
                        return $objWriter->save('php://output');
                    }
                    if ($this->input->post('form_action') == 'export_excel') {
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                        return $objWriter->save('php://output');
                    }

                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_bank_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function getifsc($id) {
        if ($this->input->get('bankid')) {
            $id = $this->input->get('bankid');
        }
        $data = $this->bank_model->getifsc($id);
        echo json_encode($data);
    }

}
