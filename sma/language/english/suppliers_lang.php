<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Suppliers
 * 
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 * 
 * You can translate this file to your language. 
 * For instruction on new language setup, please visit the documentations. 
 * You also can share your language files by emailing to saleem@tecdiary.com 
 * Thank you 
 */


$lang['opening_bal']                        = "Opening Balance";
$lang['list_new_suppliers']                 = "list new supplier"; 
$lang['addsupplier']                        = "Add Supplier";
$lang['addnewsupplier']                     = "Add New Supplier";
$lang['edit_supplier']                      = "Edit Supplier";
$lang['delete_supplier']                    = "Delete Supplier";
$lang['delete_suppliers']                   = "Delete Suppliers";
$lang['supplier_added']                     = "Supplier successfully added";
$lang['suppliers_added']                    = "Suppliers successfully added";
$lang['supplier_updated']                   = "Supplier successfully updated";
$lang['supplier_deleted']                   = "Supplier successfully deleted";
$lang['suppliers_deleted']                  = "Suppliers successfully deleted";
$lang['import_by_csv']                      = "Add Suppliers by CSV";
$lang['edit_profile']                       = "Edit User";
$lang['delete_user']                        = "Delete User";
$lang['no_supplier_selected']               = "No supplier selected. Please select at least one supplier.";
$lang['pw_not_same']                        = "Confirm password is not match with your password";
$lang['user_added']                         = "Supplier user successfully added";
$lang['user_deleted']                       = "Supplier user successfully deleted";
$lang['supplier_x_deleted_have_purchases']  = "Action failed! supplier have purchases";
$lang['suppliers_x_deleted_have_purchases'] = "Some suppliers cannot be deleted as they have purchases";
$lang['check_supplier_email']               = "Please check supplier email";
$lang['supplier_already_exist']             = "Supplier already exists with the same email address";
$lang['line_no']                            = "Line Number";
$lang['first_6_required']                   = "First Six (6) columns are required and others are optional.";
$lang['supliercode']                         = "Supplier Code";
$lang['suppliername']                       = "Supplier Name";
$lang['searchcode']                         = "Search Code";
$lang['searchcode']                         = "Search Code";
$lang['general']                            = "General";
$lang['vat_no']                             = "VAT No";
$lang['tin_no']                             = "TIN No";
$lang['cst_no']                             = "CST No";
$lang['pan_no']                             = "PAN No";
$lang['address']                            = "Address";
$lang['address1']                           = "Address Line 1";
$lang['address2']                           = "Address Line 2";
$lang['city']                               = "City";
$lang['country']                            = "Country";
$lang['state']                              = "State";
$lang['zip_code']                           = "Zip Code";
$lang['financial_account']                  = "Financial Account";
$lang['opening_balance']                    = "Opening Balance";
$lang['cr']                                 = "Cr";
$lang['change']                             = "Change";
$lang['define_my_own_Code']                 = " Define my own Code ";
$lang['preferred_supplier']                 = "Preferred Supplier";
$lang['is_active']                          = "Is Active";
$lang['fname']                              = "First Name";
$lang['lname']                              = "Last Name";
$lang['telephoneno']                        = "Telephone No";
$lang['faxnumber']                          = "Fax No";
$lang['mobileno']                           = "Mobile No";
$lang['website']                            = "Website";
$lang['email']                              = "E-mail";
$lang['phtot']                              = "Photo";
$lang['customefild1']                       = "Supplier Custome Field 1";
$lang['customefild2']                       = "Supplier Custome Field 2";
$lang['customefild3']                       = "Supplier Custome Field 3";
$lang['customefild4']                       = "Supplier Custome Field 4";
$lang['customefild5']                       = "Supplier Custome Field 5";
$lang['customefild6']                       = "Supplier Custome Field 6";
$lang['attachment']                         = "Attachments";
$lang['refered_by']                         = "Refered By";
$lang['select_store']                       = "Select Store";
$lang['ifsc']                               = "IFSC";
$lang['store']                              = "store";