<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Customers
 * 
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 * 
 * You can translate this file to your language. 
 * For instruction on new language setup, please visit the documentations. 
 * You also can share your language files by emailing to saleem@tecdiary.com 
 * Thank you 
 */


$lang['add_customer']                   = "Add Customer";
$lang['edit_customer']                  = "Edit Customer";
$lang['delete_customer']                = "Delete Customer";
$lang['delete_customers']               = "Delete Customers";
$lang['customer_added']                 = "Customer successfully added";
$lang['customer_edit']                 = "Customer updated successfully";
$lang['customers_added']                = "Customers successfully added";
$lang['customer_updated']               = "Customer successfully updated";
$lang['customer_deleted']               = "Customer successfully deleted";
$lang['customers_deleted']              = "Customers successfully deleted";
$lang['import_by_csv']                  = "Add Customers by CSV";
$lang['edit_profile']                   = "Edit User";
$lang['delete_user']                    = "Delete User";
$lang['no_customer_selected']           = "No customer selected. Please select at least one customer.";
$lang['pw_not_same']                    = "Confirm password is not match with your password";
$lang['user_added']                     = "Customer user successfully added";
$lang['user_deleted']                   = "Customer user successfully deleted";
$lang['customer_x_deleted']             = "Action failed! this customer can't be deleted";
$lang['customer_x_deleted_have_sales']  = "Action failed! customer have sales";
$lang['customers_x_deleted_have_sales'] = "Some customers cannot be deleted as they have sales";
$lang['check_customer_email']           = "Please check customer email";
$lang['customer_already_exist']         = "Customer already exists with the same email address";
$lang['line_no']                        = "Line Number";
$lang['first_6_required']               = "First Six (6) columns are required and others are optional.";
$lang['vat_no']                         = "VAT No";
$lang['tin_no']                         = "TIN No";
$lang['cst_no']                         = "CST No";
$lang['pan_no']                         = "PAN No";
$lang['dob']                            = "Date of Birth";
$lang['fname']                          = "First Name";
$lang['lname']                          = "Last Name";
$lang['anniversarydate']                = "Anniversary Date";
$lang['loyalityno']                     = "Loyalty Number";
$lang['enrolldate']                     = "Enrollment Date";
$lang['loyalitypoint']                  = "Loyalty Points";
$lang['loyalitypoints']                 = "Loyalty Points";
$lang['referby']                        = "Referred By";
$lang['bank_name']                      = "Bank Name";
$lang['acc_no']                         = "Account Number";
$lang['neft']                           = "NEFT";
$lang['bank_branch']                    = "Bank Branch";
$lang['address1']                       = "Address Line 1";
$lang['address2']                       = "Address Line 2";
$lang['city']                           = "City";
$lang['state']                          = "State";
$lang['country']                        = "Country";
$lang['zipcode']                        = "Zip Code";
$lang['telephoneno']                    = "Telephone No";
$lang['mobileno']                       = "Mobile No";
$lang['email']                          = "E Mail";
$lang['creditlimit']                    = "Credit Limit";
$lang['fbalance']                       = "Balance";
$lang['csd']                            = "Customer Since Date";
$lang['cg']                             = "Customer Groups";
$lang['attachment']                     = "Attachment";
$lang['photo']                          = "Photo";
$lang['created']                        = "Created By";
$lang['define_my_own_Code']             = "Define My Own Code";
$lang['customercode']                   = "Customer Code";
$lang['username']                   = "User Name";
$lang['loyalty_percent']                   = "Loyalty in %";
    
