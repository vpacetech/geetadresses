<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Orders
 * 
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 * 
 * You can translate this file to your language. 
 * For instruction on new language setup, please visit the documentations. 
 * You also can share your language files by emailing to saleem@tecdiary.com 
 * Thank you 
 */


$lang['add_quote']                      = "Add Order";
$lang['edit_quote']                     = "Edit Order";
$lang['delete_quote']                   = "Delete Order";
$lang['delete_quotes']                  = "Delete Orders";
$lang['quote_added']                    = "Order Successfully Added";
$lang['quote_updated']                  = "Order Successfully Updated";
$lang['quote_deleted']                  = "Order Successfully Deleted";
$lang['quotes_deleted']                 = "Orders Successfully Deleted";
$lang['quote_details']                  = "Order Details";
$lang['email_quote']                    = "Email Order";
$lang['view_quote_details']             = "View Order Details";
$lang['quote_no']                       = "Order No";
$lang['send_email']                     = "Send Email";
$lang['quote_items']                    = "Order Items";
$lang['no_quote_selected']              = "No quotation selected. Please select at least one quotation.";
$lang['stamp_sign']                     = "Stamp &amp; Signature";
$lang['create_sale']                    = "Create Sale";
$lang['create_purchase']                = "Create Purchase";


$lang['srno']                           = "Sr. No";
$lang['dept']                           = "Dept";
$lang['product']                        = "Product";
$lang['type']                           = "Type";
$lang['brand']                          = "Brand";
$lang['dno']                            = "D. No";
$lang['style']                          = "Style";
$lang['fitting']                        = "Fitting";
$lang['pattern']                        = "Pattern";
$lang['fabric']                         = "Fabric";
$lang['color']                          = "Color";
$lang['purrate']                        = "pur. Rate";
$lang['mrp']                            = "MRP";
$lang['fromsize']                       = "From Size";
$lang['tosize']                         = "To Size";
$lang['nocolor']                        = "No of Color";
$lang['qty']                            = "Qty";

$lang['purchase_order_date']            = "Purchase order date";
$lang['estimate_dilievery_date']        = "Estimate Delivery Date";
$lang['through_transport']              = "Through Transport";
$lang['reveived']                       = "Received";
$lang['order_date']                     = "Order Date";
$lang['add_product_for_order']          = "Add Product For Order";
$lang['product_qty']                    = "Quantity";
$lang['transport']                      = "Transport";
$lang['design']                         = "Design";
$lang['purchase_price']                 = "Purchase Price";
$lang['squantity']                      = "Secondary Quantity";
$lang['re-order_quote']                 = "Re-Order";
$lang['primary_quantity']               = "Primary Quantity";
$lang['totatl_qty']                     = "Total Quantity";
$lang['from_size']                      = "From Size";                
$lang['to_size']                        = "To Size";                
$lang['no_of_colors']                   = "Number Of Colors";   

$lang['hsncode']                        = "HSN Code";
$lang['gstno']                          = "Tax Rate (%)";
$lang['hsn']                            = "HSN";
$lang['taxrate']                        = "Tax Rate";
$lang['particulars']                    = "Particulars";
$lang['delete_hsn']                     = "Delete HSN";
$lang['add_hsn']                        = "Add HSN";
$lang['hsn_added']                      = "HSN Successfully Added";
$lang['hsn_failed']                     = "HSN Not Added";
$lang['gst_deleted']                    = "HSN Successfully Deleted";
$lang['hsn_x_deleted']                  = "HSN Successfully Deleted";

$lang['total_primary_quantity']         = "Total Pri Qty.";
$lang['total_sec_qty']                  = "Total Sec. Qty";
$lang['grand_total']                    = "Grand Total";
