<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['transportation']     = "Transportation";
$lang['transportation_list']     = "Transportation";
$lang['add_transport']     = "Add Transport";
$lang['trans_name']     = "Transport Name";
$lang['transp_added']     = "Transport Added Successfully";
$lang['mobile']     = "Operator's Mobile";
$lang['delete_transport']     = "Delete Transport";
$lang['edit_transport']     = "Edit Transport";
$lang['transp_updated']     = "Transport Updated successfully";
$lang['transport_deleted']     = "Transport Deleted successfully";
$lang['no_transport_selected']     = "No Transport Selected";
$lang['landline']     = "Landline no.";
$lang['fax']     = "Fax no.";
$lang['phone']     = "Owner's contact no";
$lang['t_code']     = "Transport Code";
$lang['define_my_own_Code']     = "Define My Own Code";
$lang['transport']     = "Transport";


