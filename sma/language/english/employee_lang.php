<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Suppliers
 * 
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 * 
 * You can translate this file to your language. 
 * For instruction on new language setup, please visit the documentations. 
 * You also can share your language files by emailing to saleem@tecdiary.com 
 * Thank you 
 */

$lang['refbyemp']                                = "Reffered By Employee";
$lang['sal']                                = "Salary";
$lang['address']                            = "Address(Home)";
$lang['domicile']                           = "Domicile";
$lang['anniversary']                       = "Anniversary";
$lang['no_of_dependends']                   = "Number of dependents";
$lang['employee']                           = "Employees";
$lang['add_employee']                       = "Add Employee";
$lang['edit_employee']                      = "Edit Employee";
$lang['delete_employee']                    = "Delete Employee";
$lang['delete_employee']                   = "Delete Employee";
$lang['employee_added']                     = "Employee successfully added";
$lang['employee_added']                    = "Employee successfully added";
$lang['employee_updated']                   = "Employee successfully updated";
$lang['employee_deleted']                   = "Employee successfully deleted";
$lang['suppliers_deleted']                  = "Employee successfully deleted";
$lang['import_by_csv']                      = "Add Suppliers by CSV";
$lang['edit_profile']                       = "Edit User";
$lang['delete_user']                        = "Delete User";
$lang['no_supplier_selected']               = "No Employee selected. Please select at least one supplier.";
$lang['pw_not_same']                        = "Confirm password is not match with your password";
$lang['user_added']                         = "Employee user successfully added";
$lang['user_deleted']                       = "Employee user successfully deleted";
$lang['employee_x_deleted_have_purchases']  = "Action failed! Employee have purchases";
$lang['employee_x_deleted_have_purchases'] = "Some Employee cannot be deleted as they have purchases";
$lang['check_employee_email']               = "Please check Employee email";
$lang['employee_already_exist']             = "Employee already exists with the same email address";
$lang['line_no']                            = "Line Number";
$lang['first_6_required']                   = "First Six (6) columns are required and others are optional.";


$lang['emp_id']                             = "Employee Id";
$lang['emp_photo']                          = "Photo";
$lang['emp_join_date']                      = "Joining Date";
$lang['emp_name']                           = "Employee Name";
$lang['emp_email_address']                  = "Employee Email";
$lang['emp_phone']                          = "Employee Mobile";
$lang['delete_employee']                    = "Delete Employee";
$lang['fname']                              = "First Name";
$lang['mname']                              = "Middle Name";
$lang['lname']                              = "Last Name";
$lang['dob']                                = "Date of Birth";
$lang['dojoin']                             = "Date of Joining";
$lang['maritalstatus']                      = "Marital Status";
$lang['gender']                             = "Gender";
//$lang['location']                           = "Business Location";
$lang['location']                           = "Store Name";
$lang['department']                         = "Department";
$lang['desigation']                         = "Employee Desigation";
$lang['smsreq']                             = "Can Request Reports via SMS";
$lang['smsapprove']                         = "Can approve access request via sms";
$lang['notemp']                             = "Not an employee";
$lang['invattach']                          = "Can be attached to invoice appointment";
$lang['address1']                           = "Address Line 1";
$lang['address2']                           = "Address Line 2";
$lang['city']                               = "City";
$lang['state']                              = "State";
$lang['country']                            = "Country";
$lang['zipcode']                            = "Zip Code";
$lang['telephonenos']                       = "Telephone No(Home)";
$lang['mobileno']                           = "Mobile No";
$lang['email']                              = "E-Mail";
$lang['website']                            = "Website";
$lang['shift']                              = "Working Shift";
$lang['scp']                                = "Sales Commission Percent(%)";
$lang['cqp']                                = "Commission Quick Position";
$lang['hlsda']                              = "Has Limited Spot Discount Authority";
$lang['msd']                                = "Maximum Spot Discount(%)";
$lang['photo']                              = "Photo";
$lang['faxno']                              = "Fax Number";
$lang['bank_name']                          = "Bank Name";
$lang['acc_no']                             = "Account Number";
$lang['neft']                               = "NEFT";
$lang['bank_branch']                        = "Bank Branch";
$lang['store']                              = "Store Name";
$lang['position']                           = "Position";
$lang['alternet_mob']                       = "Alternet Mobile Number";
$lang['section']                            = "Section";
$lang['incentive']                          = "Incentive";
$lang['totalincentive']                     = "Total Incentive";
$lang['employee_incentive_added']           = "Employee Incentive Sucessfully Added";

