<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Stores
 * 
 * Last edited:
 * 30th April 2015
 *
 * Package:
 * Stock Manage Advance v3.0
 * 
 * You can translate this file to your language. 
 * For instruction on new language setup, please visit the documentations. 
 * You also can share your language files by emailing to saleem@tecdiary.com 
 * Thank you 
 */


$lang['add_biller']                     = "Add Store";
$lang['edit_biller']                    = "Edit Store";
$lang['delete_biller']                  = "Delete Store";
$lang['delete_billers']                 = "Delete Stores";
$lang['biller_added']                   = "Store Successfully Added";
$lang['biller_updated']                 = "Store Successfully Updated";
$lang['biller_deleted']                 = "Store Successfully Deleted";
$lang['billers_deleted']                = "Stores Successfully Deleted";
$lang['no_biller_selected']             = "No store selected. Please select at least one store.";
$lang['invoice_footer']                 = "Invoice Footer";
$lang['biller_x_deleted_have_sales']    = "Action failed! biller have sales";
$lang['billers_x_deleted_have_sales']   = "Some billers cannot be deleted as they have sales";
$lang['biller_x_deleted_is_active']     = "Store can not be deleted as it is active now";
$lang['propriter_name']     = "testing";


