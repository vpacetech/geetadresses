<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['bank']     = "Bank";
$lang['bank_list']     = "Bank";
$lang['add_bank']     = "Add Bank";
$lang['bank_name']     = "Bank Name";
$lang['bank_added']     = "Bank Added Successfully";
$lang['mobile']     = "Mobile";
$lang['delete_bank']     = "Delete Bank";
$lang['delete_bankPayee']     = "Delete BankPayee";
$lang['edit_bank']     = "Edit Bank";
$lang['edit_bankPayee']     = "Edit BankPayee";
$lang['bank_updated']     = "Bank Updated successfully";
$lang['bankPayee_updated']     = "Bank Payee Updated successfully";
$lang['bank_deleted']     = "Bank Deleted successfully";
$lang['no_bank_selected']     = "No Bank Selected";
$lang['landline']     = "Landline no.";
$lang['fax']     = "Fax no.";
$lang['phone']     = "Phone no.";
$lang['create_date']     = "Added On";


$lang['ifsc']     = "IFSC Code";
$lang['ifsc_code']     = "IFSC Code";
$lang['address_1']     = "Address 1";
$lang['address_2']     = "Address 2";
$lang['bank_code']     = "Bank Code";
$lang['web_site']     = "WebSite";
$lang['pin_code']     = "Pin Code";
$lang['add_payee']     = "Add Payee";
$lang['edit_payee']     = "Edit Payee";
$lang['account_no']     = "Account No.";
$lang['select_store']     = "Select Store";
$lang['list_payee']     = "List Payee";
$lang['define_my_own_code']     = "Define My Own Code";
$lang['delete_payee']     = "Delete Payee";
$lang['payee']     = "Payee";
$lang['payee_deleted']     = "Payee Deleted";
$lang['no_bank_selected']     = "No Payee Selected";


